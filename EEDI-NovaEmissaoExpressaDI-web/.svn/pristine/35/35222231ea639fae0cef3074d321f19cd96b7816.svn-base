package br.com.bradseg.eedi.novaemissaoexpressadi.validador;

import java.util.List;

import javax.inject.Named;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.novaemissaoexpressadi.cep.facade.CepServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.Constantes;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.ConstantesMensagemErro;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.BeneficiariosVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.CorretorVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.DependenteVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.EnderecoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.FormaPagamento;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PagamentoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PlanoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PropostaVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.RepresentanteLegalVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TitularVO;

import com.google.common.base.Strings;

/**
 * Classe responsavel por validra se todos os dados necess�rios para o cadastro de uma proposta foram preenchidos e est�o v�lidos de acordo com as regras.
 * 
 */
@Scope("prototype")
@Named("NovaPropostaValidator")
public class NovaPropostaValidator {

	@Autowired
	protected PropostaServiceFacade propostaServiceFacade;
	
	@Autowired
	protected CepServiceFacade cepServiceFacade;

	/**
	 * Valida se todas as informa��es necess�rias para finalizar uma proposta foram preenchidas e est�o de acordo com as regras do sistema.
	 * 
	 * - Validar os dados dos benefici�rios;
	 * - Validar os dados do pagamento;
	 * 
	 * @param proposta - informa��es da proposta.
	 */
	public void validate(PropostaVO proposta) {
		Validador validador = new Validador();

		validarDadosCorretor(validador, proposta.getCorretor());
		validarDadosPlano(validador, proposta.getPlano());
		validarDadosPagamento(validador, proposta.getPagamento());
		if(!proposta.getPagamento().getFormaPagamento().equals("0")){
			validarDadosBeneficiarios(validador, proposta.getBeneficiarios(), proposta.getPlano(), proposta.getPagamento());
		}

		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}
	}

	private void validarDadosCorretor(Validador validador, CorretorVO corretor) {
		boolean cpdPreenchido = validador.obrigatorio(corretor.getCpd(), "CPD/Sucursal");		
		if(cpdPreenchido){
			validador.obrigatorio(corretor.getSucursal(), "CPD/Sucursal");	
		}
		validador.obrigatorio(corretor.getAssistenteProducao(), "Assistente de Produ��o");	
	}

	/**
	 * Metodo responsavel por validar os dados do plano.
	 * 
	 * @param validador - mensagens de erro.
	 * @param plano - informa��es do plano.
	 */
	private void validarDadosPlano(Validador validador, PlanoVO plano) {
		validador.obrigatorio(plano.getCodigo(), "Plano");		
	}

	/**
	 * Metodo responsavel por validar os dados dos benefici�rios.
	 * 
	 * - Validar os dados do representante legal (quando o titular form menor de idade);
	 * - Validar os dados do titular;
	 * - Validar os dados dos dependentes; 
	 * 
	 * @param validador - mensagens de erro.
	 * @param beneficiarios - informa��es dos benecifi�rios.
	 */
	private void validarDadosBeneficiarios(Validador validador, BeneficiariosVO beneficiarios, PlanoVO plano, PagamentoVO pagamento) {
		validador.obrigatorio(beneficiarios.isTitularMenorIdade(), "O benefici�rio titular � menor de idade");
		if(beneficiarios.isTitularMenorIdade()){
			validarDadosRepresentanteLegal(validador, beneficiarios.getRepresentanteLegal());
		}
		if(beneficiarios.getTitular().getGrauParentescoTitularProponente() != null){
			if(beneficiarios.getTitular().getGrauParentescoTitularProponente().equals("1") || beneficiarios.getTitular().getGrauParentescoTitularProponente().equals("2")){
				if(pagamento.getContaCorrente().getDataNascimento() != null){
					validador.verdadeiro(ValidadorDataNascimento.validaDataNascimentoRespFinanceiro(pagamento.getContaCorrente().getDataNascimento(), 
						beneficiarios.getTitular().getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO, beneficiarios.getTitular().getCpf());
				}else{
					validador.verdadeiro(ValidadorDataNascimento.validaDataNascimentoRespFinanceiro(pagamento.getCartaoCredito().getDataNascimento(), 
						beneficiarios.getTitular().getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO, beneficiarios.getTitular().getCpf());
				}
			
			}else if(beneficiarios.getTitular().getGrauParentescoTitularProponente().equals("3") || beneficiarios.getTitular().getGrauParentescoTitularProponente().equals("4")){
				
				if(pagamento.getFormaPagamento().equals("3")){
					validador.verdadeiro(ValidadorDataNascimento.validaDataNascimentoRespFinanceiroFilho(beneficiarios.getTitular().getDataNascimento(), 
						pagamento.getContaCorrente().getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO_MENOS, beneficiarios.getTitular().getCpf());	
				}else{
					validador.verdadeiro(ValidadorDataNascimento.validaDataNascimentoRespFinanceiroFilho(beneficiarios.getTitular().getDataNascimento(), 
						pagamento.getCartaoCredito().getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO_MENOS, beneficiarios.getTitular().getCpf());
				}
			}
		}
		validarDadosTitular(validador, beneficiarios.getTitular(), beneficiarios.isTitularMenorIdade());
		validarDadosDependentes(validador, beneficiarios.getDependentes(),pagamento, beneficiarios.getTitular());
		
		if(beneficiarios.getTitular().getGrauParentescoTitularProponente().equals("19")){
			String cpfOutPoint = String.valueOf(beneficiarios.getTitular().getCpf()).replaceAll("[.,;\\-\\s]", "");
			if(!pagamento.getContaCorrente().getCpf().equals("")){
				validador.verdadeiro(ValidadorDataNascimento.cpfProprio(pagamento.getContaCorrente().getCpf(), cpfOutPoint), ConstantesMensagemErro.MSG_CPF_PROPRIO_IGUAIS_OBRIGATORIO);
			}else{
				validador.verdadeiro(ValidadorDataNascimento.cpfProprio(pagamento.getCartaoCredito().getCpfCartao(), cpfOutPoint), ConstantesMensagemErro.MSG_CPF_PROPRIO_IGUAIS_OBRIGATORIO);
			}
		}
	}

	/**
	 * Metodo responsavel por validar os dados do representante legal.
	 * 
	 * @param validador - mensagens de erro.
	 * @param representanteLegal - informa��es do representante legal.
	 */
	private void validarDadosRepresentanteLegal(Validador validador, RepresentanteLegalVO representanteLegal) {
		validador.nome(representanteLegal.getNome(), "Nome do Representante Legal");	
		validador.cpf(representanteLegal.getCpf(), "CPF do Representante Legal");	
		boolean nascimentoPreenchido = validador.obrigatorio(representanteLegal.getDataNascimento(), "Nascimento do Representante Legal");
		if(nascimentoPreenchido){
			validador.verdadeiro(ValidadorDataNascimento.validarDataMaxima(representanteLegal.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA, "Representante Legal");
			validador.falso(ValidadorDataNascimento.validarSeMenorDeIdade(representanteLegal.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA, "Representante Legal");
		}
		validador.email(representanteLegal.getEmail(), "E-mail do Representante Legal");
		validarEndereco(validador, representanteLegal.getEndereco(), "Representante Legal");
		validador.obrigatorio(representanteLegal.getTelefones().get(0).getTipoTelefone(), "Tipo de Telefone do Representante Legal");
		validador.obrigatorio(representanteLegal.getTelefones().get(0).getDddEnumero(), "N� Telefone/Ramal do Representante Legal");
	}
	
	/**
	 * Metodo responsavel por validar os dados do titular.
	 * 
	 * @param validador - mensagens de erro.
	 * @param titular - informa��es do titular.
	 * @param indicativoTitularMenorIdade - indicativo do titular ser menor de idade.
	 */
	private void validarDadosTitular(Validador validador, TitularVO titular, boolean indicativoTitularMenorIdade) {
		validador.nomeCompleto(titular.getNomeMae(), "Nome da M�e do titular");	
		validador.nomeCompleto(titular.getNome(), "Nome do titular");	
		validador.cpf(titular.getCpf(), "CPF do Titular");	
		boolean nascimentoPreenchido = validador.obrigatorio(titular.getDataNascimento(), "Nascimento do Titular");
		if(nascimentoPreenchido){
			validador.verdadeiro(ValidadorDataNascimento.validarDataMaxima(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA, Constantes.STRING_TITULAR);
			if(indicativoTitularMenorIdade){
				validador.verdadeiro(ValidadorDataNascimento.validarSeMenorDeIdade(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_TITULAR_DATA_NASCIMENTO_MENOR_DE_DEZOITO, Constantes.STRING_TITULAR);
			}
			else{
				validador.falso(ValidadorDataNascimento.validarSeMenorDeIdade(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA, Constantes.STRING_TITULAR);
			}
		}
		validador.email(titular.getEmail(), "E-mail do Titular");
		validador.obrigatorio(titular.getSexo(), "Sexo do Titular");
		validador.obrigatorio(titular.getEstadoCivil(), "Estado Civil do Titular");
		validarEndereco(validador, titular.getEndereco(), Constantes.STRING_TITULAR);
		validador.verdadeiro((!Strings.isNullOrEmpty(titular.getTelefones().get(0).getDddEnumero()) || !Strings.isNullOrEmpty(titular.getTelefones().get(1).getDddEnumero()) || !Strings.isNullOrEmpty(titular.getTelefones().get(2).getDddEnumero())), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_TITULAR_TELEFONE_OBRIGATORIO);
	}
	
	/**
	 * Metodo responsavel por validar os dados dos dependentes.
	 * 
	 * - Validar os dados de todos os dependentes.
	 * 
	 * @param validador - mensagens de erro.
	 * @param dependentes - informa��es dos dependentes.
	 */
	private void validarDadosDependentes(Validador validador, List<DependenteVO> dependentes, PagamentoVO responsavelFinanceiro, TitularVO titularInf) {
		if(!dependentes.isEmpty()){
			int contadorDependente = 1;
			for (DependenteVO dependente : dependentes) {
				validador.verdadeiro((!Strings.isNullOrEmpty(dependente.getNomeMae()) || !Strings.isNullOrEmpty(dependente.getCpf())), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DEPENDENTE_NOME_MAE_OU_CPF_OBRIGATORIO, contadorDependente);
				if(!Strings.isNullOrEmpty(dependente.getNomeMae())){
					validador.nomeCompleto(dependente.getNomeMae(), "Nome da M�e do Dependente");	
				}
				
				if(dependente.getGrauParentescoBeneficiario().equals("1")){				
					validador.grauParentescoequals(dependente.getGrauParentescoBeneficiario(), titularInf.getGrauParentescoTitularProponente(), "Pai");
				}else if(dependente.getGrauParentescoBeneficiario().equals("2")){
					validador.grauParentescoequals(dependente.getGrauParentescoBeneficiario(), titularInf.getGrauParentescoTitularProponente(), "M�e");
				}else if(dependente.getGrauParentescoBeneficiario().equals("5")){
					validador.grauParentescoequals(dependente.getGrauParentescoBeneficiario(), titularInf.getGrauParentescoTitularProponente(), "C�njuge");
				}
				
				if(!Strings.isNullOrEmpty(dependente.getCpf())){
					validador.cpf(dependente.getCpf(), "CPF do Dependente");	
				}
				validador.nomeCompleto(dependente.getNome(), "Nome do Dependente");	
				boolean nascimentoPreenchido = validador.obrigatorio(dependente.getDataNascimento(), "Nascimento do Dependente");	
				if(nascimentoPreenchido){
					validador.verdadeiro(ValidadorDataNascimento.validarDataMaxima(dependente.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA, "Dependente");
					//validador.falso(ValidadorDataNascimento.validarSeMenorDeIdade(dependente.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA, "Dependente[" + contadorDependente + "]");
				}
				if(dependente.getGrauParentescoBeneficiario().equals("1") || dependente.getGrauParentesco().equals("2")){
					if(responsavelFinanceiro.getContaCorrente().getDataNascimento() != null){
						if(validador.verdadeiro(ValidadorDataNascimento.validaDataNascimentoRespFinanceiro(responsavelFinanceiro.getContaCorrente().getDataNascimento(),dependente.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO, dependente.getCpf())){
							break;
						}
					}else{
						if(validador.verdadeiro(ValidadorDataNascimento.validaDataNascimentoRespFinanceiro(responsavelFinanceiro.getCartaoCredito().getDataNascimento(),dependente.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO, dependente.getCpf())){
							break;
						}
					}
				}else if(dependente.getGrauParentescoBeneficiario().equals("3") || dependente.getGrauParentescoBeneficiario().equals("4") ){
					if(responsavelFinanceiro.getContaCorrente().getDataNascimento() != null){
						if(ValidadorDataNascimento.validaDataNascimentoRespFinanceiroFilho(dependente.getDataNascimento(),responsavelFinanceiro.getContaCorrente().getDataNascimento()) == false){
							validador.verdadeiro(false, ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO_MENOS, dependente.getCpf());
							break;
						}
					}else{
						if(ValidadorDataNascimento.validaDataNascimentoRespFinanceiroFilho(dependente.getDataNascimento(),responsavelFinanceiro.getCartaoCredito().getDataNascimento()) == false){
							validador.verdadeiro(false, ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO_MENOS, dependente.getCpf());
							break;
						}
					}
					
				}
				validador.obrigatorio(dependente.getGrauParentesco(), "Parentesco do Dependente");	
				validador.obrigatorio(dependente.getSexo(), "Sexo do Dependente");	
				validador.obrigatorio(dependente.getEstadoCivil(), "Estado Civil do Dependente");	
				contadorDependente++;
			}
		}			
	}

	/**
	 * Metodo responsavel por validar os dados do endere�o.
	 * 
	 * @param validador - mensagens de erro.
	 * @param endereco - informa��es do endere�o.
	 * @param tipoPessoa - tipo de pessoa a qual o endere�o pertence.
	 */
	private void validarEndereco(Validador validador, EnderecoVO endereco, String tipoPessoa) {
		boolean cepPreenchido = validador.obrigatorio(endereco.getCep(), "Cep " + tipoPessoa);
		if(cepPreenchido){
			boolean enderecoPreenchido = validador.falso(Strings.isNullOrEmpty(endereco.getLogradouro()) && Strings.isNullOrEmpty(endereco.getBairro()) && Strings.isNullOrEmpty(endereco.getCidade()) && Strings.isNullOrEmpty(endereco.getEstado()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_CEP_NAO_ENCONTRADO, "Cep " + tipoPessoa);
			if(cepPreenchido && enderecoPreenchido){
				validador.obrigatorio(endereco.getLogradouro(), "Logradouro " + tipoPessoa);
				validador.obrigatorio(endereco.getNumero(), "N�mero do Endere�o " + tipoPessoa);
				validador.obrigatorio(endereco.getComplemento(), "Complemento " + tipoPessoa);
				validador.obrigatorio(endereco.getBairro(), "Bairro " + tipoPessoa);
			}
		}
	}
	
	/**
	 * Metodo responsavel por validar os dados do pagamento.
	 * 
	 * - Validar o tipod e cobran�a;
	 * - Validar os dados banc�rios do proponente;
	 * 
	 * @param validador - mensagens de erro.
	 * @param pagamento - informa��es do pagamento.
	 */
	private void validarDadosPagamento(Validador validador, PagamentoVO pagamento) {
		validador.obrigatorio(pagamento.getTipoCobranca(), "Tipo de Cobran�a");	
		boolean tipoCobrancaPreenchido = validador.obrigatorio(pagamento.getFormaPagamento(), "Forma de Pagamento");
		
		if(tipoCobrancaPreenchido) {
			if(FormaPagamento.DEBITO_AUTOMATICO.getCodigo().equals(Integer.parseInt(pagamento.getFormaPagamento()))) {
				boolean dataCobrancaPreenchida = validador.obrigatorio(pagamento.getDataCobranca(), "Data de Cobran�a");
				if(dataCobrancaPreenchida){
					validador.verdadeiro(pagamento.getDataCobranca().isAfter(new LocalDate().plusDays(14)) && pagamento.getDataCobranca().isBefore(new LocalDate().plusDays(46)), ConstantesMensagemErro.MSG_ERRO_PAGAMENTO_DATA_COBRANCA_ENTRE_15_A_45_DIAS);
				}
				boolean bancoPreenchido = validador.obrigatorio(pagamento.getContaCorrente().getBanco(), "Banco");	
				if(bancoPreenchido){
					validador.obrigatorio(pagamento.getContaCorrente().getNumeroAgencia(), "Ag�ncia do Proponente");
					validador.obrigatorio(pagamento.getContaCorrente().getNumeroConta(), "N�mero da Conta do Proponente");
					validador.verdadeiro(!Strings.isNullOrEmpty(pagamento.getContaCorrente().getDigitoVerificadorConta()), ConstantesMensagemErro.MSG_ERRO_CAMPO_OBRIGATORIO, "D�gito da Conta do Proponente");
					validador.cpf(pagamento.getContaCorrente().getCpf(), "CPF do Proponente");
					validador.nome(pagamento.getContaCorrente().getNome(), "Nome do Proponente");
					validador.obrigatorio(pagamento.getContaCorrente().getDataNascimento(), "Nascimento do Proponente");
				}
			}
			else if(FormaPagamento.CARTAO_CREDITO.getCodigo().equals(Integer.parseInt(pagamento.getFormaPagamento()))) {
				validarCamposcartaoCredito(validador, pagamento);
			}
		}
	}
	
	public void validarCamposcartaoCredito(Validador validador, PagamentoVO pagamento){
		boolean campoValidadePreenchido = validador.obrigatorio(pagamento.getCartaoCredito().getValidade() , "Validade do cart�o");
		
		if(campoValidadePreenchido) {
			LocalDate dataAtual = new LocalDate();
			
			String dataValidadeString = pagamento.getCartaoCredito().getValidade();
			String dataSeparada[] = new String[2];
			dataSeparada = dataValidadeString.split("/");
			
			Integer mes = Integer.parseInt(dataSeparada[0]);
			Integer ano = Integer.parseInt(dataSeparada[1]);
			
        	if((mes > 12 || mes < 1) && ano <= dataAtual.getYear()) {
        		validador.validaDataValidadeCartao("Validade do Cart�o");
        	} else if(mes > 12 || mes < 1) {
        		validador.validaDataValidadeCartao("Validade do Cart�o");
        	} else if(ano <= dataAtual.getYear()) {
        		validador.validaDataValidadeCartao("Validade do Cart�o");
        	}
		}
		validador.obrigatorio(pagamento.getCartaoCredito().getNomeCartao()  , "Nome do cart�o");
		validador.obrigatorio(pagamento.getCartaoCredito().getCpfCartao(), "Numero do cpf");
		validador.obrigatorio(pagamento.getCartaoCredito().getNumeroCartao(), "N�mero do cart�o");
		validador.obrigatorio(pagamento.getCartaoCredito().getBandeira()    , "Bandeira do cart�o");
	}
}