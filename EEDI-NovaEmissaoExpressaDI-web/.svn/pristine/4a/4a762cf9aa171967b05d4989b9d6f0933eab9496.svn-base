package br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.facade;

import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONPopulator;
import org.apache.struts2.json.JSONUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.vo.AccessTokenVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.vo.BandeirasVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.vo.PaymentTokenVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.vo.RetornoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.Constantes;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PropostaVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoCobranca;

/**
 * Classe responsavel por disponibilizar os metodos de neg�cio referentes a odontoprev.
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class OdontoprevServiceFacadeImpl implements OdontoprevServiceFacade {

	@Autowired
	private URL urlOdontoprev;
	// 100.168.100.137

	/**
	 * Contexto antigo do MIP
	 */
	//private static String METODO_LISTAR_BANDEIRAS = "/mip/api/v1/cartao/bandeiras/EEDI";
	//private static String METODO_OBTER_ACCESS_TOKEN = "/mip/api/v1/captura/ticket/acessar/EEDI/BDAXXXXXXXXXXXX";
	//private static String METODO_VALIDAR_DADOS_CARTAO_CREDITO = "/mip/api/v1/captura/ticket/guardar";
	//private static String METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/mip/api/v1/captura/ticket/cobrar";
	//private static String METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/mip/api/v1/cobranca/cancelar/parcela";
	//private static String METODO_VERIFICAR_CORRETOR_CARTAO_CREDITO = "/mip/api/v1/config/cartaohabilitado?login=";
	//	private static String METODO_VERIFICAR_CORRETOR_CARTAO_CREDITO = "/mip/api/v1/config/cartaohabilitado";
	
	/**
	 * Novo contexto de mapeamento do MIP
	 * Data de altera��o: 17/06/2019
	 */
	private static String METODO_LISTAR_BANDEIRAS = "/BSMO-OdontoPrev/v2/mip/1.0/cartao/bandeiras/EEDI";
	private static String METODO_OBTER_ACCESS_TOKEN = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/acessar/EEDI/BDAXXXXXXXXXXXX";
	private static String METODO_VALIDAR_DADOS_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/guardar";
	private static String METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/cobrar";
	private static String METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/cancelar/parcela";
	private static String METODO_VERIFICAR_CORRETOR_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/config/cartaohabilitado?login=null";

	/**
	 * Implementado o Endpoint da Braspag para valida��o do cart�o de cr�dito
	 */
	private static String ENDPOINT_BRASPAG_PAYMENT_TOKEN_DEV = "https://transactionsandbox.pagador.com.br/post/api/public/v1/card";
	private static String ENDPOINT_BRASPAG_PAYMENT_TOKEN_PRD = "https://pagador.com.br/post/api/public/v1/card";

	private static final Logger LOGGER = LoggerFactory.getLogger(OdontoprevServiceFacadeImpl.class);

	private static final String POST = "POST";
	private static final String GET = "GET";
	
	/**
	 * Metodo responsavel por listar as bandeiras para pagamento com cart�o de cr�dito.
	 * 
	 * @return List<String> - lista de bandeiras.
	 */
	public List<String> listarBandeiras() {

		try {
			BandeirasVO bandeiras = (BandeirasVO) realizarChamadaOdontoprev(METODO_LISTAR_BANDEIRAS, BandeirasVO.class,
					GET, null);
			
			if (null == bandeiras) {
				return new ArrayList<String>();
			} else {
				if (0 == bandeiras.getSucesso()) {
					LOGGER.error("listarBandeiras - Problema na comunica��o com a Odontoprev: " + bandeiras.getMensagem());
					throw new BusinessException("listarBandeiras - Problema na comunica��o com a Odontoprev: " + bandeiras.getMensagem());
				}
				return bandeiras.getBandeiras();
			}
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DO METODO DE LISTAR BANDEIRAS: " + e.getMessage());
			return new ArrayList<String>();
		}
	}

	public boolean verificarCorretorHabilitado(String numeroCorretor) {
		boolean corretorPermiteCartao = false;
		
		try {
			RetornoVO retorno = (RetornoVO) realizarChamadaOdontoprevCartao(METODO_VERIFICAR_CORRETOR_CARTAO_CREDITO,
					RetornoVO.class, GET, numeroCorretor);
			if (retorno != null && retorno.isHabilitado()) {
				corretorPermiteCartao = true;
			}
		} catch (Exception e) {
			LOGGER.error("verificarCorretorHabilitado - Erro ao chamar servico para verificar corretor logado");
		}
		return corretorPermiteCartao;
	}

	/**
	 * Metodo responsavel por obter o access token para pagamento com cart�o de cr�dito
	 * 
	 * @return String - access token.
	 */
	public String obterAccessToken() {
		try {
			AccessTokenVO accessToken = (AccessTokenVO) realizarChamadaOdontoprev(METODO_OBTER_ACCESS_TOKEN,
					AccessTokenVO.class, GET, null);

			if (null == accessToken) {
				return "";
			} else {
				if (0 == accessToken.getSucesso()) {
					LOGGER.error("obterAccessToken - Erro ao obter Access Token: " + accessToken.getMensagem());
					throw new IntegrationException("obterAccessToken - Problema na comunica��o com a Odontoprev. ");
				}
				return accessToken.getAccessToken();
			}
		} catch (Exception e) {	
			LOGGER.error("ERRO NA CHAMADA DO METODO DE OBTER O ACCESS TOKEN: " + e.getMessage());
			throw new IntegrationException(e.getMessage());
		}
	}

	public RetornoVO debitarPrimeiraParcelaCartaoDeCredito(PropostaVO proposta) {

		RetornoVO retorno = null;
		try {
			LOGGER.error(String.format("INICANDO METODO DE DEBITO 1 PARCELA [%s]", proposta.getCodigo()));
			
			StringBuilder url = new StringBuilder(METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO);
			retorno = (RetornoVO) realizarChamadaOdontoprev(url.toString(), RetornoVO.class, POST,
					preencherParametros(proposta, true, false));

			LOGGER.error("FINALIZANDO METODO DE DEBITO 1 PARCELA");
			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("debitarPrimeiraParcelaCartaoDeCredito - Problema na comunica��o com a Odontoprev MIP:" + retorno.getMensagem());
				//throw new BusinessException("debitarPrimeiraParcelaCartaoDeCredito - retorno vazio do MIP: " + retorno.getMensagem());
			}
			LOGGER.error(String.format("Retorno metodo debitarPrimeiraParcelaCartaoDeCredito MIP: [%s] [%s]", retorno.getSucesso(), retorno.getMensagem()));
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DEBITAR 1 PARCELA no MIP: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}
		return retorno;
	}

	public RetornoVO validarDadosCartaoCredito(PropostaVO proposta) {
		
		RetornoVO retorno = null;
		LOGGER.error("validarDadosCartaoCredito - Endereco url odontoprev: " + urlOdontoprev.toString());
		
		try {
			LOGGER.error(String.format("INICANDO METODO DE VALIDAR DADOS CARTAO [%s]", proposta.getCodigo()));

			StringBuilder url = new StringBuilder(METODO_VALIDAR_DADOS_CARTAO_CREDITO);
			retorno = (RetornoVO) realizarChamadaOdontoprev(url.toString(), RetornoVO.class, POST,
					preencherParametros(proposta, false, true));

			LOGGER.error("FINALIZANDO METODO DE validarDadosCartaoCredito");
			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("validarDadosCartaoCredito - Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
				throw new BusinessException("validarDadosCartaoCredito - BusinessException - Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}
			LOGGER.error(String.format("Retorno metodo validarDadosCartaoCredito: [%s] [%s]", retorno.getSucesso(), retorno.getMensagem()));
		} catch (Exception e) {
			LOGGER.error("validarDadosCartaoCredito - ERRO NA CHAMADA VALIDAR DADOS CARTAO: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}

		return retorno;
	}

	public void cancelarDebitoPrimeiraParcelaCartaoDeCredito(PropostaVO proposta) {

		try {
			LOGGER.error("INICANDO METODO DE CANCELAMENTO DE DEBITO 1 PARCELA");

			StringBuilder url = new StringBuilder(METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO);
			RetornoVO retorno = (RetornoVO) realizarChamadaOdontoprev(url.toString(), RetornoVO.class, POST,
					preencherParametrosCancelamento(proposta, false, false));

			LOGGER.error("FINALIZANDO METODO DE CANCELAMENTO DE DEBITO 1 PARCELA");
			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("Problema na comunica��o com a Odontoprev:" + retorno.getMensagem());
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DE CANCELAMENTO DE DEBITO 1 PARCELA: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}

	private Object realizarChamadaOdontoprev(String urlServico, Class<?> classe, String tipoEnvio, String parametros) {
		URL url = null;
		HttpURLConnection conexao;
		try {
			LOGGER.error("realizarChamadaOdontoprev - urlodontoprev: " + urlOdontoprev.toString());
			url = new URL(urlOdontoprev.toString() + urlServico);
			
			conexao = (HttpURLConnection) url.openConnection();
			conexao.setRequestMethod(tipoEnvio);
			conexao.setRequestProperty("Accept", "application/json");
			if (null != parametros) {
				conexao.setDoInput(true);
				conexao.setDoOutput(true);
				OutputStream os = conexao.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				writer.write(parametros);
				writer.flush();
				writer.close();
				os.close();
			}
			conexao.connect();

			if (conexao.getResponseCode() != 200) {
				throw new BusinessException(conexao.getResponseCode() + " / " + conexao.getResponseMessage());
			}

			return deserializeJSON(inputStreamToString(conexao.getInputStream()), classe);
		} catch (MalformedURLException e) {
			LOGGER.error("realizarChamadaOdontoprev - ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("MalformedURLException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("realizarChamadaOdontoprev - ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("IOException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("realizarChamadaOdontoprev - ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Exception - Problema na comunica��o com a Odontoprev: " + e.getMessage());
		}
	}

	private Object realizarChamadaOdontoprevCartao(String urlServico, Class<?> classe, String tipoEnvio,
			String parametros) {
		URL url;
		HttpURLConnection conexao;
		try {
			LOGGER.error("realizarChamadaOdontoprevCartao - Endereco url odontoprev: " + urlOdontoprev.toString());
			url = new URL(urlOdontoprev.toString() + urlServico);
			if (parametros != null) {
				parametros = parametros.trim();
			}
			url = new URL(urlOdontoprev.toString() + urlServico + parametros);
			conexao = (HttpURLConnection) url.openConnection();
			conexao.setRequestMethod(GET);
			conexao.setRequestProperty("Accept", "application/json");
			conexao.setConnectTimeout(10000);
			conexao.connect();

			if (conexao.getResponseCode() != 200) {
				LOGGER.error("realizarChamadaOdontoprevCartao - ERRO NA CHAMADA: " + conexao.getResponseCode() + " / " + conexao.getResponseMessage());
				throw new BusinessException(conexao.getResponseCode() + " / " + conexao.getResponseMessage());
			}

			return deserializeJSON(inputStreamToString(conexao.getInputStream()), classe);
		} catch (MalformedURLException e) {
			LOGGER.error("realizarChamadaOdontoprevCartao - MalformedURLException - ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("MalformedURLException - Problema na comunica��o com a OdontoprevCartao: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("realizarChamadaOdontoprevCartao - IOException - ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("IOException - Problema na comunica��o com a OdontoprevCartao: " + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("realizarChamadaOdontoprevCartao - Exception - ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Exception - Problema na comunica��o com a OdontoprevCartao: " + e.getMessage());
		}
	}

	private String preencherParametros(PropostaVO proposta, boolean indicativoDebitoPrimeiraParcela,
			boolean indicativoValidacaoDados) {

		StringBuilder parametros = new StringBuilder();
		parametros.append("sistema=EEDI&").append("proposta=" + proposta.getCodigo());

		if (indicativoDebitoPrimeiraParcela) {
			parametros.append("&")
					.append("paymentToken=" + proposta.getPagamento().getCartaoCredito().getPaymentToken() + "&")
					.append("nome=" + proposta.getPagamento().getCartaoCredito().getNomeCartao() + "&")
					.append("cpf=" + proposta.getPagamento().getCartaoCredito().getCpfCartao() + "&")
					.append("bandeira=" + proposta.getPagamento().getCartaoCredito().getBandeira());

			Double valor = 0.0D;
			
			if (TipoCobranca.MENSAL.getNome().equalsIgnoreCase(proposta.getPagamento().getTipoCobranca())) {
				valor = proposta.getPlano().getValorPlanoVO().getValorMensalTitular()
						* (proposta.getBeneficiarios().getDependentes().size()
						+ Constantes.QUANTIDADE_BENEFICIARIO_TITULAR);
			} else if (TipoCobranca.ANUAL.getNome().equalsIgnoreCase(proposta.getPagamento().getTipoCobranca())) {
				valor = proposta.getPlano().getValorPlanoVO().getValorAnualTitular()
						* (proposta.getBeneficiarios().getDependentes().size()
						+ Constantes.QUANTIDADE_BENEFICIARIO_TITULAR);
			}

			parametros.append("&valor=" + new DecimalFormat("#0.00").format(valor).replace(",", ""));
		}

		if (indicativoValidacaoDados) {
			parametros.append("&")
					.append("paymentToken=" + proposta.getPagamento().getCartaoCredito().getPaymentToken() + "&")
					.append("nome=" + proposta.getPagamento().getCartaoCredito().getNomeCartao() + "&")
					.append("cpf=" + proposta.getPagamento().getCartaoCredito().getCpfCartao() + "&")
					.append("bandeira=" + proposta.getPagamento().getCartaoCredito().getBandeira());
		}

		return parametros.toString();
	}

	private String preencherParametrosCancelamento(PropostaVO proposta, boolean indicativoDebitoPrimeiraParcela,
			boolean indicativoValidacaoDados) {

		StringBuilder parametros = new StringBuilder();
		parametros.append("sistema=EEDI&").append("proposta=" + proposta.getCodigo() + "&").append("numeroParcela=001");

		return parametros.toString();
	}

	private String inputStreamToString(InputStream is) throws IOException {
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

	private Object deserializeJSON(String json, Class<?> classe) {
		Object objeto = null;
		if (json != null && !"".equals(json.trim())) {
			try {
				objeto = classe.newInstance();
				JSONPopulator jsonPopulator = new JSONPopulator();
				Map<?, ?> map = (Map<?, ?>) JSONUtil.deserialize(json);
				jsonPopulator.populateObject(objeto, map);

			} catch (IllegalAccessException e) {
				throw new BusinessException("deserializeJSON - IllegalAccessException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (InstantiationException e) {
				throw new BusinessException("deserializeJSON - InstantiationException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (IllegalArgumentException e) {
				throw new BusinessException("deserializeJSON - IllegalArgumentException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (InvocationTargetException e) {
				throw new BusinessException("deserializeJSON - InvocationTargetException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (NoSuchMethodException e) {
				throw new BusinessException("deserializeJSON - NoSuchMethodException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (IntrospectionException e) {
				throw new BusinessException("deserializeJSON - IntrospectionException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (JSONException e) {
				LOGGER.error("deserializeJSON - JSONException - Problema na comunica��o com a Odontoprev: " + e.getMessage());
				// throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
				return null;
			}
		}
		return objeto;
	}
	
	/**
	 * Metodo responsavel por obter o PAYMENT TOKEN para pagamento com cart�o de cr�dito
	 * 
	 * @param proposta - n�mero da proposta gerada.
	 * @return String - PAYMENT token.
	 */
	public String obterPaymentToken(PropostaVO proposta) {

		PaymentTokenVO retorno = null;
		try {
            SSLContext ctx = SSLContext.getInstance("TLSv1.2");
            ctx.init(null, null, null);
            SSLContext.setDefault(ctx);
        } catch (Exception e) {
            LOGGER.error("OCORREU UM ERRO AO BUSCAR O PAR�METRO PAYMENT_TOKEN" + e.getMessage());
        }
		
		try {
			String acesso = ENDPOINT_BRASPAG_PAYMENT_TOKEN_DEV;
			
			if(!urlOdontoprev.toString().contains("dsv")){
				acesso = ENDPOINT_BRASPAG_PAYMENT_TOKEN_PRD;
			}
			String value = buscaPaymentTokenByBraspag(acesso, proposta);
			if(value != null){
				retorno = new PaymentTokenVO();
				retorno.setPaymentToken(value);
			}				

			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("Problema na comunica��o para obterPaymentToken:" + retorno.getMensagem());
				retorno.setPaymentToken("Par�metro PaymentToken inv�lido.");
			//	throw new BusinessException("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}
			LOGGER.error(String.format("Retorno metodo obterPaymentToken: [%s] [%s]", retorno.getSucesso(), retorno.getMensagem()));
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA obterPaymentToken: " + e.getMessage());
			//throw new BusinessException(e.getMessage());
		}
		return retorno.getPaymentToken();
	}
	
	/**
	 * 
	 * @param url
	 * @param proposta
	 * @return
	 * @throws Exception
	 */
	private String buscaPaymentTokenByBraspag(String url, PropostaVO proposta) throws Exception {

		String USER_AGENT = "Mozilla/5.0";
		int ERROR_CODE_ACCESS = 401;
		
		//String url = ENDPOINT;
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		
		String nome = proposta.getPagamento().getCartaoCredito().getNomeCartao();
		String cartao = proposta.getPagamento().getCartaoCredito().getNumeroCartao();
		String data = proposta.getPagamento().getCartaoCredito().getValidade();
		String token = proposta.getPagamento().getCartaoCredito().getAccessToken();

        String urlParameters = "HolderName="+nome+"&RawNumber="+cartao+"&Expiration="+data+"&AccessToken="+token;

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		
		if(responseCode != ERROR_CODE_ACCESS) {
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			JSONObject jo = new JSONObject(response.toString());
		    String payment = jo.getString("PaymentToken");
		    proposta.getPagamento().getCartaoCredito().setPaymentToken(payment);
		    return payment;
		}else {
		    return null;
		}
	}
}
