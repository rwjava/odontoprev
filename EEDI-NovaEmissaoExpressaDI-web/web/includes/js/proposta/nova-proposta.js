$(document).ready(function() {
	
	FORMA_PAGAMENTO_DEBITO_AUTOMATICO = 3;
	FORMA_PAGAMENTO_CARTAO_CREDITO = 5;
	FORMA_PAGAMENTO_BOLETO_E_CARTAO_CREDITO = 6;
	FORMA_PAGAMENTO_DEBITO_AUTOMATICO_E_CARTAO_CREDITO = 7;
	 
	inicializarTela();
	
	$('.mascara-moeda').formatCurrency({'region': 'pt-BR', 'symbol ' : 'R$'});
	
	$('#finalizar').on('click', function (){
		var accessToken = $('.accessToken').val();
		var paymentToken = $('.paymentToken').val();
		$('#finalizar').hide();
		
		if(accessToken != "" && paymentToken == ""){
			$('.bp-sop-cardnumber').val($('.bp-sop-cardnumber').val().replace(/^\s+|\s+$/g,''));
			sendCardData();
		}
		BootstrapDialog.show({
	        title: 'CONFIRMA&Ccedil;&Atilde;O DE FINALIZA&Ccedil;&Atilde;O DA PROPOSTA',
	        message: novo_finalizar(),
	        closable: false,
	        buttons: [{
	        	id: 'botao-confirmar-proposta',
	            label: 'Confirmar',
	            cssClass: 'btn-warning',
	            action: function(dialogRef){
	            	console.log('action finalizarProposta.do');
	            	
	            	var formaDePagamento = $('#forma_pagamento').val();
	            	
	            	console.log(formaDePagamento);
	            	
	            	if(formaDePagamento == FORMA_PAGAMENTO_CARTAO_CREDITO 
							|| formaDePagamento  == FORMA_PAGAMENTO_BOLETO_E_CARTAO_CREDITO 
							|| formaDePagamento == FORMA_PAGAMENTO_DEBITO_AUTOMATICO_E_CARTAO_CREDITO){
	            		salvarPropostaCartao();
	            	}else{
	            		finalizarProposta();
	            	}
	        		
	            	dialogRef.close();
	            	bloquearTela();
	            	$('#finalizar').show();
	        		setTimeout(function(){$('input[name="finalizar-proposta"]').click();}, 2000);
	            }
	        }, {
	            label: 'Cancelar',
	            action: function(dialogRef){

	                dialogRef.close();
	                desbloquearTela();
	                $('#finalizar').show();
	            }
	        }]
	    });
	});
	
	
	
	$("#tabela_planos").on('click','.radio_plano', function(){
		console.log('planoselecionado: ' + $('.radio_plano:checked').val());
		var planoSelecionado = $('.radio_plano:checked').val();
//		$('#plano_selecionado').val(planoSelecionado);
		obterFormasPagamento(planoSelecionado);
		
		if(planoSelecionado == 15 || planoSelecionado == 20){
			$('input:radio[name="proposta.beneficiarios.titularMenorIdade"][value="true"]').prop('checked', true);
			$('#form_proposta_beneficiarios_titularMenorIdadefalse').attr('disabled', 'disabled');
			$('#representante_legal').show();
			$('#dependentesForm').hide();
		}else{
			$('input:radio[name="proposta.beneficiarios.titularMenorIdade"][value="false"]').prop('checked', true);
			$('#form_proposta_beneficiarios_titularMenorIdadefalse').removeAttr('disabled');
			$('#representante_legal').hide();
			$('#dependentesForm').show();
		}
		
		if(this.value != '0' && $('#codigoProposta').val() == "" && $('.radio_plano:checked').val() != undefined) {
			$('.imprimir_proposta_vazia').show();
	  	}else{
	  	 	$('.imprimir_proposta_vazia').hide();
	  	}

	});
	
	//Oculta blocos formas de pagamento até uma ser selecionada na comboBox
	if($('#forma_pagamento').val() == '0') {
		$('.debito_automatico').hide();
	}
	$('.cartao_credito').hide();
	$('.banco_pagamento').hide();
	
	//Fun��o para exibir blocos de pagamentos
	$('#forma_pagamento').on('change', function() {
				
		if($('#forma_pagamento').val() == '0') {
			$('.banco_pagamento').hide();
			$('.cpf_pagamento').hide();
			$('.cartao_credito').hide();
			
			$("#finalizar").show();	
			//$("#salvar-rascunho").show();
			
		}
		else if($('#forma_pagamento').val() == '3') {
			$('.banco_pagamento').show();
			$('.cartao_credito').hide();
		} else {
			$('.debito_automatico').hide();
			$('.banco_pagamento').hide();
			$('.cpf_pagamento').hide();
		}
		
		if($('#forma_pagamento').val() == 5 || $('#forma_pagamento').val() == 6 || $('#forma_pagamento').val() == 7){
			$('.debito_automatico').hide();
			$('.cartao_credito').show();
			gerarAccessToken();
		}
		
	});

function salvarPropostaCartao(){	
	var codigo = $('#sequencial_proposta').text();
	
	if(codigo != null && codigo != ""){
		$('#form').attr('action', window.baseURL + 'proposta/finalizarPropostaAjax.do?proposta.codigo='+codigo);
	}else{
		$('#form').attr('action', window.baseURL + 'proposta/finalizarPropostaAjax.do');
	}
	
	var actionSalvarRascunhoPropostaCartao = $('#form').prop('action');
	
	$.ajax({			
		type: "POST",
		url: actionSalvarRascunhoPropostaCartao,
		dataType: 'json',
		data: $('#form').serialize(),
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			console.log(data + " esto aqui no success cartão cred");
			if(data.actionErrors.length  > 0){
				montarMengagensErro(data.actionErrors);
			}else{  
				$('input[name="proposta.sequencial"]').val(data.proposta.sequencial);
				$('label[name="proposta.codigo"]').text(data.proposta.codigo);
				$('input[name="proposta.status"]').val(data.proposta.status);
				$('#sequencial_proposta').text(data.proposta.codigo);
				$('#codigo_proposta').val(data.proposta.sequencial);
				$('.status_label').text(data.proposta.status);	
				tratarCartaoCredito(data);
			}

	  },
	  error: function( event, xhr, settings ){
		  console.log('event ' + event);
		  console.log('xhr ' + xhr);
		  console.log('settings ' + settings);
	  }
		
	});
}

function tratarCartaoCredito(data){
	console.log('tratarCartaoCredito');
//	var nomeCartao = $('');
//	var numeroCartao = $('');
//	var validadeCartao = $('');
//	var bandeiraCartao = $('');
	$('#form').attr('action', window.baseURL + 'proposta/validarDadosCartaoCreditoAjax.do');	        			
	var actionValidarDadosCartaoCreditoAjax = $('#form').prop('action');
	
	$.ajax({			
		type: "POST",
		url: actionValidarDadosCartaoCreditoAjax,
		dataType: 'json',
		data: $('#form').serialize(),
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			console.log(data);
			data.actionMessages;
			if(data.actionErrors.length  > 0){
				montarMengagensErro(data.actionErrors);
				$('input[name="proposta.sequencial"]').val(null);
				$('input[name="proposta.status"]').val(null);
				$('label[name="proposta.codigo"]').text(null);
			}else{				
				$('input[name="proposta.sequencial"]').val(data.proposta.sequencial);
				$('label[name="proposta.codigo"]').text(data.proposta.codigo);
				$('input[name="proposta.status"]').val(data.proposta.status);
				$('#sequencial_proposta').text(data.proposta.codigo);
				$('#codigo_proposta').val(data.proposta.sequencial);
				$('.status_label').text(data.proposta.status);	
				montarMensagemSucesso(data.actionMessages);
				tratarExibicaoCamposAposFinalizarProposta();
			}			
//			else{
//				montarMensagemSucesso(data.actionMessages);
//				$('#sequencial_proposta').text(data.proposta.codigo);
//				$('#codigo_proposta').val(data.proposta.sequencial);
//				$('#sequencial_proposta').show();
//				$('.status_label').text(data.proposta.status);	
//				$('input[name="proposta.status"]').val(data.proposta.status);
//				$('.imprimir_proposta_vazia').hide();
//				$('#btn_limpar').show();
//				$('#cancelar-proposta').show();
//				$('#imprimir_proposta_finalizada').show();
//				$('#cancelar-proposta_finalizada').show();
//				$('#salvar-rascunho').hide();
//			}
			
			
		},
	  error: function( event, xhr, settings ){
		  console.log('event ' + event);
		  if(data.actionErrors.length  > 0){
				montarMengagensErro(data.actionErrors);
		  }
	  }
	});	
}
	
$("[name='proposta.beneficiarios.titularMenorIdade']").on('change', function(){
	$('#representante_legal').hide();
	if($(this).val() == 'true'){
		$('#representante_legal').show();
	}
});

$("[name='proposta.plano.codigo']").on('change', function(){
	var planoSelecionado = $('[name="proposta.plano.codigo"]:checked').val();
	$.ajax({
		url: "proposta/alterarPlanoDaProposta.do",
		type: "POST",
		dataType: 'json',
		data:{
			'proposta.sucursalSelecionada.cpdSucursal':$('#sucursalList').val(),
			'proposta.plano.codigo' : planoSelecionado,
			'numeroVidas' : $('#numero_vidas').text()
		},
		beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function (data) {
			$('#mensal-label').text(data.valorTotalMensal);
			$('#anual-label').text(data.valorTotalAnual);
        },  
        error: function(data){
  		  console.log('error ' + data);
  		
  	  }
	});
	
	//$.submitAjax('proposta/alterarPlanoDaProposta.do');
});
	
$("[name='proposta.beneficiarios.titular.endereco.cep']").on('change', function(){
	var cep = $(this).val();
	if (cep.length == 8){
		$("[name='proposta.beneficiarios.titular.endereco.logradouro']").val('');
		//$.submitAjax('proposta/obterEnderecoPorCep.do?cep=' + $(this).val() + '&indicadorCepDoTitular=' + true);
		obterEnderecoCepAjax(cep, 'true');
	}else{
		$('.cep_titular').hide();
	}
});
	
$("[name='proposta.beneficiarios.representanteLegal.endereco.cep']").on('change', function(){
	var cep = $(this).val();
	if (cep.length == 8){
		$("[name='proposta.beneficiarios.representanteLegal.endereco.logradouro']").val('');
		//$.submitAjax('proposta/obterEnderecoPorCep.do?cep=' + $(this).val() + '&indicadorCepDoTitular=' + false);
		obterEnderecoCepAjax(cep, 'false');
	}else{
		$('.cep_representante_legal').hide();
	}
});	
	
$('#adicionar_dependente').click(function(){
	//$.submitAjax('proposta/adicionarDependente.do');
	var quantidadeDependentes = $('#numero_vidas').text();
	var planoSelecionado = $('[name="proposta.plano.codigo"]:checked').val();
	$.executarAjax({
		url: window.baseURL + "proposta/adicionarDependente.do",
		dataType: 'html',	
		data: $('#form').serializeLatin(),
		type: 'POST',
		 success : function(data) {
			 //console.log(data);
			 $('#content').html(data);
			 marcarPlanoJaSelecionado(planoSelecionado);
			
			 console.log('valor mensal :' + $('#valorTotalMensal').val());
			 console.log('valor anual :' + $('#valorTotalAnual').val());
		},
           error: function(e) {
        	   //$('#content').html(e.responseText);
        	   exibirCriticas();
        	   //console.log(e);
           }
	});
});	
	
$("[name='proposta.pagamento.contaCorrente.banco']").on('change', function(){
	var banco = $(this).val();
	$('#cpf_pagamento').val('');
	$('#nome_pagante').val('');
	$('#nascimento_pagante').val('');
	$('#agencia').val('');
	$('#conta').val('');
	$('#digito_conta').val('');
	$('#agencia_conta').val(0);
	if(banco == 0){
		$('.cpf_pagamento').hide();
	}else{
		$('.cpf_pagamento').show();
	}
	$('.dados_debito_automatico').hide();
	$('.dados_debito_automatico_outros_bancos').hide();
});	
	
$("[name='proposta.pagamento.contaCorrente.cpf']").on('blur', function(){
	var cpf = $(this).val();
	$('#nome_pagante').val('');
	$('#nascimento_pagante').val('');
	$('#agencia').val('');
	$('#conta').val('');
	$('#digito_conta').val('');
	$('#agencia_conta').val(0);
	
	if(cpf.length == 11){
		if($("#banco").val() == 237){
			listarContaCorrentePorCpfAjax(cpf);
		}else{
			$('.dados_debito_automatico').show();
			$('.dados_debito_automatico_outros_bancos').show();
		}
	}else{
		$('.dados_debito_automatico').hide();
		$('.dados_debito_automatico_outros_bancos').hide();
	}
});
	
$('[data-telefone]').change(function(){
	mudarMascaraTelefone($(this).attr('data-telefone'));
}).trigger('change');
	
function mudarMascaraTelefone(idTelefone) {		
	var $inputNumeroTelefone = $('#' + idTelefone);
	var mascaraAnterior = $inputNumeroTelefone.inputmask("getemptymask");
	// Regra para nono digito nos telefones moveis
	if($('#tipo_telefone_representante_legal').val() == 'CELULAR'){
		$inputNumeroTelefone.inputmask({ mask: "(99) 9999-9999[9]", greedy: false });
	}else{
		$inputNumeroTelefone.inputmask({ mask: "(99) 9999-9999", greedy: false });
		}
	}
});


function removerDependente(posicao){
	$.submitAjax('proposta/removerDependente.do?numeroDependente=' + posicao);
}

function inicializarTela(){
	var planoSelecionado = $('.radio_plano:checked').val();
	
	$('#nova_proposta').addClass('selected');
	$('#listar_proposta').removeClass('selected');
	
	exibirCriticas();
	inicializarDadosRepresentanteLegal();	
	inicializarDadosTitular();
	inicializarDadosPagamento();
	identificarAmbiente();
	obterFormasPagamento(planoSelecionado);
}

function exibirCriticas(){
	var existeCriticas = $('#hasActionErrors').val();
	if(existeCriticas){
		$('#msgErros').toggle($.parseJSON($('#hasActionErrors').val()));
		$('#msgErros').ScrollTo();
	}else{
		$('#msgErros').hide();
	}
}

//Length modificado dia 05/08/2019
function inicializarDadosRepresentanteLegal(){
	if($("[name='proposta.beneficiarios.titularMenorIdade']:checked").val() == 'true'){
		$('#representante_legal').show();
		
		//cep do representante legal
		// Length do Logradouro modificado por: Gabriel
		if($("[name='proposta.beneficiarios.representanteLegal.endereco.cep']").val().length == 8) {
			if($("[name='proposta.beneficiarios.representanteLegal.endereco.logradouro']").val().length <= 50 || $("[name='proposta.beneficiarios.representanteLegal.endereco.bairro']").val().length > 0 || $("[name='proposta.beneficiarios.representanteLegal.endereco.cidade']").val().length > 0 || $("[name='proposta.beneficiarios.representanteLegal.endereco.estado']").val().length > 0){			
				
				//Verifica se logradouro retorna algo e faz a modifica��o no input do mesmo
				// Length do Logradouro modificado por: Gabriel
				 if($("[name='proposta.beneficiarios.representanteLegal.endereco.logradouro']").val().length <= 50) {
					 $("[name='proposta.beneficiarios.representanteLegal.endereco.logradouro']").css({
						'outline':'0',
						'box-shadow': '0 0 0 0',
						'border': '0 none',
						'background-color': '#F5F5F5',
						'cursor': 'Default',
						'color': '##636161'
					 });
				 }
				 
				//Verifica se bairro retorna algo e faz a modifica��o no input do mesmo
				 if($("[name='proposta.beneficiarios.representanteLegal.endereco.bairro']").val().length > 0) {
					 $("[name='proposta.beneficiarios.representanteLegal.endereco.bairro']").css({
						'outline':'0',
						'box-shadow': '0 0 0 0',
						'border': '0 none',
						'background-color': '#F5F5F5',
						'cursor': 'Default',
						'color': '##636161'
					 });
				 }
				 
				//Exibe o(s) dado(s) retornado(s) pelo CEP
				$('.cep_representante_legal').show();
				
			}
		}
	}
}

function inicializarDadosTitular(){
	//cep do titular
	// Length do Logradouro modificado por: Gabriel
	if($("[name='proposta.beneficiarios.titular.endereco.cep']").val().length == 8){
		 if($("[name='proposta.beneficiarios.titular.endereco.logradouro']").val().length <= 50 || $("[name='proposta.beneficiarios.titular.endereco.bairro']").val().length > 0 || $("[name='proposta.beneficiarios.titular.endereco.cidade']").val().length > 0 || $("[name='proposta.beneficiarios.titular.endereco.estado']").val().length > 0) {	
			 //Verifica se logradouro retorna algo e faz a modifica��o no input do mesmo
			// Length do Logradouro modificado por: Gabriel
			 if($("[name='proposta.beneficiarios.titular.endereco.logradouro']").val().length <= 50) {
				 $("[name='proposta.beneficiarios.titular.endereco.logradouro']").css({
					'outline':'0',
					'box-shadow': '0 0 0 0',
					'border': '0 none',
					'background-color': '#F5F5F5',
					'cursor': 'Default',
					'color': '##636161'
				 });
			 }
			//Verifica se bairro retorna algo e faz a modifica��o no input do mesmo
			 if($("[name='proposta.beneficiarios.titular.endereco.bairro']").val().length > 0) {
				 $("[name='proposta.beneficiarios.titular.endereco.bairro']").css({
					'outline':'0',
					'box-shadow': '0 0 0 0',
					'border': '0 none',
					'background-color': '#F5F5F5',
					'cursor': 'Default',
					'color': '##636161'
				 });
			 }
			 //Exibe o(s) dado(s) retornado(s) do CEP
			$('.cep_titular').show();
		 }
	}
}

function inicializarDadosPagamento(){
	//banco
	inicializarBanco();
	//conta corrente
	inicializarContaCorrente();
}

function inicializarBanco(){
	var banco = $('#banco').val();
	if(banco == 0){
		$('.cpf_pagamento').hide();
		$('#cpf_pagamento').val('');
		$('.dados_debito_automatico').hide();
		$('#nome_pagante').val('');
		$('#nascimento_pagante').val('');
		$('.dados_debito_automatico_outros_bancos').hide();
		$('#agencia').val('');
		$('#conta').val('');
		$('#digito_conta').val('');
	}else{
		$('.cpf_pagamento').show();
	}
}

function inicializarContaCorrente(){
	var cpf = $("[name='proposta.pagamento.contaCorrente.cpf']").val();
	if(cpf.length == 11 && $('#hasActionErrors').val() == 'false'){
		$('.dados_debito_automatico').show();
		var banco = $("#banco").val();
		$('.dados_debito_automatico_outros_bancos').show();
	}else if(cpf.length == 11 && $('#hasActionErrors').val() == 'true'){
		$('.dados_debito_automatico').show();
		$('.dados_debito_automatico_outros_bancos').show();
	}
}

/*******************************************************
 *                FUNCTIONS CREDIT CARD                *
 *******************************************************/

function novo_finalizar(){	
	var plano = "<b>Plano</b>: ";
	var planoSelecionado = false;
	if(undefined == $("input[name='proposta.plano.codigo']:checked").val()){
		plano = plano + "N&atilde;o Selecionado<br/>";
	}else{
		var tr = $("input[name='proposta.plano.codigo']:checked").closest('tr');
	    	plano = plano + tr.find('#t').text() + "<br/>";
	    	planoSelecionado = true;
	}

	var tipoCobranca = "<b>Tipo de Cobran&ccedil;a</b>: ";
	var tipoCobrancaSelecionado = false;
	if(undefined == $("input[name='proposta.pagamento.tipoCobranca']:checked").val()){
		tipoCobranca = tipoCobranca + "N&atilde;o Selecionado<br/>";
		console.log(tipoCobranca);
	}
	else {
		tipoCobranca = tipoCobranca + $("label[for='" + $("input[name='proposta.pagamento.tipoCobranca']:checked").attr('id') + "']").text() + "<br/>";
		tipoCobrancaSelecionado = true;
	}
	
	var valor = "<b>Valor</b>: ";
	if(planoSelecionado && tipoCobrancaSelecionado){
		var tipo_cobranca = $("label[for='" + $("input[name='proposta.pagamento.tipoCobranca']:checked").attr('id') + "']").text();
		if("Mensal" == tipo_cobranca){
			
			console.log('valor total mensal label: ' + $('#mensal-label').text());
			valor = valor + $('#mensal-label').text() + "<br/><br/><br/>";
		}else if("Anual" == tipo_cobranca){
			console.log('valor total anual label: ' + $('#anual-label').text());
			valor = valor + $('#anual-label').text() + "<br/><br/><br/>";
		}	
		$('.mascara-moeda').formatCurrency({'region': 'pt-BR', 'symbol ' : 'R$'});
	}else{
		valor = valor + "R$ 0,00<br/><br/><br/>";
	}
	var resumo = "Resumo da Proposta:<br/><br/>";
	var pergunta = "<i>Deseja confirmar a contrata&ccedil;&atilde;o da proposta com as informa&ccedil;&otilde;es acima?</i>";
	var mensagem_completa = resumo + plano + tipoCobranca + valor + pergunta;
	
	return mensagem_completa;
}


function finalizarProposta(){	
	var codigo = $('#sequencial_proposta').text();

	if(codigo != null && codigo != ""){
		$('#form').attr('action', window.baseURL + 'proposta/finalizarPropostaAjax.do?proposta.codigo='+codigo);	        		
	}else{
		$('#form').attr('action', window.baseURL + 'proposta/finalizarPropostaAjax.do');	        			
	}
	var actionFinalizarPropostaCartao = $('#form').prop('action');
		$.ajax({			
			type: "POST",
			url: actionFinalizarPropostaCartao,
			dataType: 'json',
			data: $('#form').serialize(),
			 beforeSend: function(){
			 	bloquearTela();
			 },
			 complete: function(){
			 	desbloquearTela();
			 },
			success: function(data){
				console.log(data);
				if(data.actionErrors.length  > 0){
					montarMengagensErro(data.actionErrors);
				}else{    	 			
					$('input[name="proposta.sequencial"]').val(data.proposta.sequencial);
					$('label[name="proposta.codigo"]').text(data.proposta.codigo);
					$('input[name="proposta.status"]').val(data.proposta.status);
					$('#sequencial_proposta').text(data.proposta.codigo);
					$('#codigo_proposta').val(data.proposta.sequencial);
					$('.status_label').text(data.proposta.status);	
					montarMensagemSucesso(data.actionMessages);
					tratarExibicaoCamposAposFinalizarProposta();
				}
			},
			error: function( event, xhr, settings ){
			  console.log('event ' + event);
			  console.log('xhr ' + xhr);
			  console.log('settings ' + settings);
			}
	});
}

function atualizarComboFormaPagamento(listaFormaPagamento){
	$('#forma_pagamento').find('option').remove().end()
    .append('<option value="0">Selecione</option>');
	
	$.each( listaFormaPagamento.formasPagamento, function( key, value ) {
		$('#forma_pagamento').append($('<option>', { 
	        value: value.codigo,
	        text : value.descricao
		}));
	});
	console.log('atualizando combo de forma de pagamento');
}

function obterFormasPagamento(planoSelecionado){
	var numeroVidas = $('#numero_vidas').text();
	var planoSelecionado;
	
	console.log(planoSelecionado);
	console.log('obterFormasPagamento\t numero de vidas : ' + numeroVidas);
	$.ajax({
		type: "POST",
		url: window.baseURL + 'proposta/obterFormasPagamento.do',
		dataType: 'json',
		data:{
			'proposta.sucursalSelecionada.cpdSucursal':$('#sucursalList').val(),
			'proposta.plano.codigo' : planoSelecionado,
			'numeroVidas' : $('#numero_vidas').text()
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			//$('#mensal-label').text(data.valorTotalMensal);
			//$('#anual-label').text(data.valorTotalAnual);
			atualizarComboFormaPagamento(data);	
		},
	  error: function(data){
		  console.log('error ' + data);
	  }
	});
}

function tratarExibicaoCamposAposFinalizarProposta(){
	$('#sequencial_proposta').show();
	$('.imprimir_proposta_vazia').hide();
	$('#btn_limpar').show();
	$('#cancelar-proposta').show();
	$('#imprimir_proposta_finalizada').show();
	$('#cancelar-proposta_finalizada').show();
	$('#salvar-rascunho').hide();
	$('#finalizar').hide();
	$('.bloco-corretor').prop('disabled', 'disabled');
}


function gerarAccessToken(){
	console.log('gerarAccessToken');
	var formaPagamento = $('#forma_pagamento').val();
	$.ajax({			
		type: "POST",
		url: window.baseURL + 'proposta/gerarAccessToken.do',
		dataType: 'json',
		data:{
			'proposta.pagamento.formaPagamento':formaPagamento
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			console.log(data);
			if(data.actionErrors.length  > 0){
				montarMengagensErro(data.actionErrors);
				$('.cartao_credito').hide();
		    }
			$('.accessToken').val(data.proposta.pagamento.cartaoCredito.accessToken);
			$('.paymentToken').val(data.proposta.pagamento.cartaoCredito.paymentToken);
			atualizarComboBandeiras(data.bandeiras);
		},
	  error: function( event, xhr, settings ){
		  console.log('event ' + event);
		  console.log('xhr ' + xhr);
		  console.log('settings ' + settings);

	  }
		
	});
}
function marcarPlanoJaSelecionado(planoSelecionado){
	
	if($('#plano_selecionado').val() == undefined ){
		return;
	} 
	
	$("#tabela_planos :radio").each(function() {
	    console.log('plano:: ' + $(this).val());
	    
	    if($(this).val() == planoSelecionado){
	    	$(this).prop('checked', true);
	    	$('#plano_selecionado').val(0);
	    	planoSelecionado = 0;
	    }	    
	});
}

function atualizarComboBandeiras(listaBandeiras){
	$('#bandeiras').find('option').remove().end()
    .append('<option value="0">Selecione</option>');
	
	listaBandeiras.forEach(function(c){
		$('#bandeiras').append($('<option>', { 
	        value: c.codigo,
	        text : c.descricao
	    }));
	});
}

function sendCardData(){
	var options = {
		accessToken: $('.accessToken').val(),
		onSuccess: function(e){
			var paymentToken = e.PaymentToken;
			$('.paymentToken').val(paymentToken);
			$('input[name="proposta.pagamento.cartaoCredito.paymentToken"]').val(paymentToken);
		},
		onError: function(e){
			var errorCode = e.Code;
			var errorMessage = e.Text;
		},
		onInvalid: function(e){
			for(var i = 0; i < e.length; i++){
				var field = e[i].Field;
				var message = e[i].Message;
			}
		},//TODO sempre trocar para production ou sandbox
		environment: "production",
		language: "PT"
		,
		cvvrequired: false
	};
	bpSop_silentOrderPost(options);
}		

function identificarAmbiente(){
	if(location.hostname.indexOf('dsv') != -1 || location.hostname.indexOf('hml') != -1 || location.hostname.indexOf('localhost') != -1){
		$('#ambiente').val('sandbox');
	}else{
		$('#ambiente').val('production');
	}
}
function marcarPlanoJaSelecionado(planoSelecionado){
	
	if($('#plano_selecionado').val() == undefined ){
		return;
	} 
	
	$("#tabela_planos :radio").each(function() {
	    console.log('plano:: ' + $(this).val());
	    
	    if($(this).val() == planoSelecionado){
	    	$(this).prop('checked', true);
	    	$('#plano_selecionado').val(0);
	    	planoSelecionado = 0;
	    }	    
	});
}

function montarMengagensErro(mensagensErro){
	
	$('#serverErrors').text('');
	$.each( mensagensErro, function( key, mensagem ) {
		var descricao = "<li>" + mensagem + "</li>";
		$('#serverErrors').append(descricao);	
	});	
	$('#msgErros').show();
	$('#msgErros').ScrollTo();
}

function montarMensagemSucesso(mensagemSucesso){
	$('#serverErrors').text('');
	$('#msgErros').hide();

	$('#serverActionsMessagesAjax').text('');
	
	mensagemSucesso.forEach(function (mensagem){
		var descricao = "<li>" + mensagem + "</li>";
		$('#serverActionsMessagesAjax').append(descricao);			 
	  });
	$('#msgSucessoAjax').show();
	$('#msgSucessoAjax').ScrollTo();	
}
function exibirCriticas(){
	var existeCriticas = $('#hasActionErrors').val();
	if(existeCriticas){
		$('#msgErros').toggle($.parseJSON($('#hasActionErrors').val()));
		$('#msgErros').ScrollTo();
	}else{
		$('#msgErros').hide();
	}
}
function limparMensagensErro(){
	$('#serverErrors').text('');
	$('#msgErros').hide();
}


/********************************************************************
 *                FUNCTION RESPONSÁVEL POR OBTER CEP                *
 ********************************************************************/
function obterEnderecoCepAjax(cepInformado, indicaCepDoTitular){
	console.log('cepInformado:' + cepInformado);
	console.log('indicaCepDoTitular:' + indicaCepDoTitular);
	
	$.ajax({			
		type: "POST",
		url: window.baseURL + 'proposta/obterEnderecoPorCep.do',
		dataType: 'json',
		data:{
			'cep':cepInformado,
			 indicadorCepDoTitular : indicaCepDoTitular
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			console.log(data);
			$('.cep_titular').show();
			if(indicaCepDoTitular == 'true'){
				$('#bairro-titular').val(data.proposta.beneficiarios.titular.endereco.bairro);
				$('#cidade-titular').text(data.proposta.beneficiarios.titular.endereco.cidade);
				$('#estado-titular').text(data.proposta.beneficiarios.titular.endereco.estado);
				$('#logradouro-titular').val(data.proposta.beneficiarios.titular.endereco.logradouro);
				$('input[name="proposta.beneficiarios.titular.endereco.bairro"]').val(data.proposta.beneficiarios.titular.endereco.bairro);
				$('input[name="proposta.beneficiarios.titular.endereco.cidade"]').val(data.proposta.beneficiarios.titular.endereco.cidade);
				$('input[name="proposta.beneficiarios.titular.endereco.estado"]').val(data.proposta.beneficiarios.titular.endereco.estado);
				$('input[name="proposta.beneficiarios.titular.endereco.logradouro"]').val(data.proposta.beneficiarios.titular.endereco.logradouro);
			
				if(data.proposta.beneficiarios.titular.endereco.logradouro == null || data.proposta.beneficiarios.titular.endereco.logradouro ==""){
					$('#logradouro-titular').attr('disabled', false);
				}else{
					$('#logradouro-titular').attr('disabled', true);
				}

			}else{				
				$('#bairro-representante').val(data.proposta.beneficiarios.representanteLegal.endereco.bairro);
				$('#cidade-representante').text(data.proposta.beneficiarios.representanteLegal.endereco.cidade);
				$('#estado-representante').text(data.proposta.beneficiarios.representanteLegal.endereco.estado);
				$('#logradouro-representante').val(data.proposta.beneficiarios.representanteLegal.endereco.logradouro);
				$('.cep_representante_legal').show();
				$('input[name="proposta.beneficiarios.representanteLegal.endereco.bairro"]').val(data.proposta.beneficiarios.representanteLegal.endereco.bairro);
				$('input[name="proposta.beneficiarios.representanteLegal.endereco.cidade"]').val(data.proposta.beneficiarios.representanteLegal.endereco.cidade);
				$('input[name="proposta.beneficiarios.representanteLegal.endereco.estado"]').val(data.proposta.beneficiarios.representanteLegal.endereco.estado);
				$('input[name="proposta.beneficiarios.representanteLegal.endereco.logradouro"]').val(data.proposta.beneficiarios.representanteLegal.endereco.logradouro);
			
				if(data.proposta.beneficiarios.representanteLegal.endereco.logradouro == null || data.proposta.beneficiarios.representanteLegal.endereco.logradouro ==""){
					$('#logradouro-representante').attr('disabled', false);
				}else{
					$('#logradouro-representante').attr('disabled', true);
				}
			}
		},
	  error: function( event, xhr, settings ){
		  console.log('event ' + event);
		  console.log('xhr ' + xhr);
		  console.log('settings ' + settings);
	  }
	});
}

/****************************************************************************************
 *                FUNCTION RESPONSÁVEL POR LISTAR CONTA CORRENTE POR CPF                *
 ****************************************************************************************/
function listarContaCorrentePorCpfAjax(cpfInformado) {
	$.ajax({
		type: "POST",
		url: window.baseURL + 'proposta/listarContaCorrentePorCPF.do',
		dataType: 'json',
		data:{
			'proposta.pagamento.contaCorrente.cpf':cpfInformado
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			console.log(data);
			$('.dados_debito_automatico').show();
			$('.dados_debito_automatico_outros_bancos').show();
			$('#nome_pagante').val(data.proposta.pagamento.contaCorrente.nome);
			$('#nascimento_pagante').val(data.proposta.pagamento.contaCorrente.dataNascimentoStr);
			$('#agencia').val(data.proposta.pagamento.contaCorrente.numeroAgencia);
			$('#conta').val(data.proposta.pagamento.contaCorrente.numeroConta);
			$('#digito_conta').val(data.proposta.pagamento.contaCorrente.digitoVerificadorConta);
		},
		  error: function( event, xhr, settings ){
			  console.log('event ' + event);
			  console.log('xhr ' + xhr);
			  console.log('settings ' + settings);
		  }
	});
}
