function inicializarPluginAjax() {
	
	// Bloquear a p�gina ao dar submit do form
	$(document).submit(function(){
		$.blockUI({ message: 'Aguarde carregando...' });
	});
	
	// Bloquear a p�gina ao clicar em link do form
	$('a').click(function() {
		$.blockUI({ message: 'Aguarde carregando...' });
	});
	
	// Quando o ajax terminar, vai fechar a tela de bloqueio
	desbloquearTela();
	
	//exibirCriticas();
	
	$.submitAjax = function(action) {
		$.executarAjax({
			url: window.baseURL + action
			, dataType: 'html'
			, data: $('#form').serializeLatin()
			, type: 'POST'
			, success: function(html){
				$('#content').html(html);
			}
			, complete: function(){
				exibirCriticas();
			}
		});
	};
	
	$.executarAjax = function( options ) {
		
		// Monta as opcoes default da chamada ajax
		var defaults = {
			dataType: 'json',
			beforeSend: function(jqXHR){
				jqXHR.overrideMimeType("text/html;charset=iso-8859-1");
				bloquearTela();
			}
		};
		
		// Faz o merge das opcoes informadas com as defaults
		$.extend(defaults, options);
		
		// Executa a chamada ajax
		$.ajax(defaults);
	};
	
	function exibirCriticas(){
		var existeCriticas = $('#hasActionErrors').val();
		if(existeCriticas){
			$('#msgErros').toggle($.parseJSON($('#hasActionErrors').val()));
			$('#msgErros').ScrollTo();
		}else{
			$('#msgErros').hide();
		}
	}
	
	// Kind of encodeURIComponent for ISO-8859-1
	function escapeComponent(str) {  
	    return escape(str).replace(/\+/g, '%2B');
	}
	 
	// Kind of $.param for ISO-8859-1
	$.paramLatin = function( a ) {
	    var s = [];
	     $.each( a, function( i, kv ) {
	        s[ s.length ] = escapeComponent(kv.name) +"="+ escapeComponent(kv.value);
	    });
	     return s.join("&").replace(/%20/g, '+');
	};
	 
	// Kind of $.fn.serialize for ISO-8859-1
	$.fn.serializeLatin = function() {
	    return $.paramLatin( this.serializeArray() );
	};
	
}
$(document).ready(inicializarPluginAjax);