$(document).ready(function(){

	// Bloquear a p�gina ao dar submit do form
	$(document).submit(function(){
		$.blockUI({ message: 'Aguarde carregando...' });
	});
	
	// Bloquear a p�gina ao clicar em link do form
//	$('a').click(function() {
//		$.blockUI({ message: 'Aguarde carregando...' });
//	});
});
/**
 * Bloquear a tela com a mensagem 'Aguarde carregando...'.
 */
function bloquearTela(){
	$.blockUI({ message: 'Aguarde carregando...' });
}

/**
 * Desbloquear a tela ao final da a��o realizada.
 */
function desbloquearTela(){
	$(document).ajaxStop($.unblockUI);
}