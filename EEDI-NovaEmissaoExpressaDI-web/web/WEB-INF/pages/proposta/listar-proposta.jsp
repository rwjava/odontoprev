<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<script type="text/javascript" src="<s:url value='/includes/js/proposta/listar-proposta.js'/>"></script>

<s:form id="form" action="consultarProposta">
	<table class="tabela_interna">
		<tr>
			<th colspan="4">BUSCA DE PROPOSTA</th> 
		</tr>
		<tr>
			<td class="td_label" colspan="1" width="130px;">Tipo de Busca<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:radio name="filtroProposta.tipoBusca" list="tiposBusca" listValue="nome" listKey="codigo"/>
			</td>
		</tr>
		<tr class="buscarPorCodigo" style="display:none;">
			<td class="td_label" colspan="1">C�digo Proposta<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:textfield name="filtroProposta.codigoProposta" size="16" maxlength="15" style="text-transform: uppercase;" />
			</td>
		</tr>
		<tr class="buscarPorCPF" style="display:none;">
			<td class="td_label" colspan="1" >Tipo de CPF<span class="obrigatorio">*</span></td>
			<td colspan="1" width="380px;">
				<s:radio name="filtroProposta.tipoCPF" list="tiposCPFs" listValue="nome" listKey="codigo"/>
			</td>
			<td class="td_label" colspan="1" >CPF<span class="obrigatorio">*</span></td>
			<td colspan="1" >
				<s:textfield name="filtroProposta.cpf" data-mask="cpf" size="12" maxlength="14"  />
			</td>
		</tr>
		<tr class="buscarPorPeriodo" style="display:none;">
			<td class="td_label" colspan="1" >Per�odo<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:textfield name="filtroProposta.filtroPeriodoVO.dataInicio" data-mask="data" size="11" maxlength="11" />
				&nbsp;at�&nbsp;
				<s:textfield name="filtroProposta.filtroPeriodoVO.dataFim" data-mask="data" size="11" maxlength="11" />
				(M�ximo 30 Dias)
			</td>
		</tr>
		<tr class="buscarPorPeriodo" style="display:none;">
			<td class="td_label" >Status<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:select name="filtroProposta.filtroPeriodoVO.status" id="status" list="status" headerKey="0" headerValue="Selecione" listKey="codigo" listValue="nome"/>
			</td>
		</tr>
		<!-- tr class="buscarPorPeriodo" style="display:none;">
			<td class="td_label" colspan="1" >CPD</td>
			<td colspan="1" width="380px;"><s:textfield name="filtroProposta.filtroPeriodoVO.cpd" size="4" maxlength="6"  /></td>
			<td class="td_label" colspan="1" width="100px;">Sucursal</td>
			<td colspan="1" >
				<s:textfield name="filtroProposta.filtroPeriodoVO.sucursal" size="3" maxlength="3"  />
			</td>
		</tr-->
	</table>
	
	<br/><br/><br/>
	
	<table style="width: 974px;">
		<tr>
			<!-- td width="45%" align="right"><input type="button" value="Limpar" id="btn_limpar"/></td>
			<td width="10%"></td-->
			<td align="center"><input type="submit" value="Consultar"/></td>
		</tr>
	</table>
	
	<br/><br/><br/>
	
	<s:if test="propostas != null && propostas.size > 0">
		<table class="tabela_interna">
			<tr>
				<th colspan="4">PROPOSTAS</th>
			</tr>
			<tr>
				<th width="150px;">C�digo</th>
				<th width="450px;">Nome do Benefici�rio</th>
				<th width="250px;">Status</th>
				<th width="250px;">Canal</th>
			</tr>
			<s:iterator value="propostas" var="proposta" status="status">
				<tr>
					<td>
						<s:if test="canal.codigo == 8">
							<a href="visualizarProposta.do?proposta.sequencial=<s:property value='sequencial'/>" >
								<s:property value="codigo"/>
							</a>
						</s:if>
						<s:else>
							<s:property value="codigo"/>
						</s:else>
					</td>
					<td><s:property value="beneficiarios.titular.nome"/></td>
					<td align="center"><s:property value="status"/></td>
					<td align="center"><s:property value="canal.nome"/></td>
				</tr>
			</s:iterator>
		</table>
	</s:if>
	
	<br/><br/>
</s:form>
