<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/bootstrap.min.css'/>" />
<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/bootstrap-dialog.css'/>" />
<script type="text/javascript" src="<s:url value='/includes/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<s:url value='/includes/js/bootstrap-dialog.js'/>"></script>
<script src="http://malsup.github.com/jquery.form.js"></script> 

<script	src="https://www.pagador.com.br/post/scripts/silentorderpost-1.0.min.js"></script>
<script type="text/javascript" src="<s:url value='/includes/js/proposta/nova-proposta.js'/>"></script>

<s:form id="form" action="finalizarProposta">
	<table class="tabela_interna">
		<s:hidden id="plano_selecionado" name="codigoPlano"></s:hidden>
		<s:hidden name="proposta.sequencial" id="sequencial"/>
		<s:hidden name="proposta.status" id="status_proposta" />
		<s:hidden name="proposta.codigo" id="codigo_proposta" />
		
		<tr>
			<th colspan="4">1. PRODUTOR</th>
		</tr>
		<tr>
			<td class="td_label" width="150px;">Nome</td>
			<td width="400px;">
				<s:hidden name="proposta.corretor.produtor.nome"/>
				<s:property value="proposta.corretor.produtor.nome"/>
			</td>
			<td class="td_label" width="200px;">CPF/CNPJ</td>
			<td>
				<s:hidden name="proposta.corretor.produtor.cpfCnpj"/>
				<s:property value="proposta.corretor.produtor.cpfCnpj"/>
			</td>
		</tr>
		<tr>
			<td class="td_label" width="150px;">CPD/Sucursal<span class="obrigatorio">*</span></td>
			<td width="400px;">
				<s:if test="proposta.corretor.cpd == null || proposta.corretor.cpd == 0">
					<s:textfield name="proposta.corretor.cpd" data-type="integer" size="5" maxlength="7" />
				</s:if>
				<s:else>
					<s:hidden name="proposta.corretor.cpd"/>
					<s:property value="proposta.corretor.cpd"/>
				</s:else>
				/
				<s:if test="proposta.corretor.sucursal == null || proposta.corretor.sucursal == 0">
					<s:textfield name="proposta.corretor.sucursal" data-type="integer" size="2" maxlength="3" />
				</s:if>
				<s:else>
					<s:hidden name="proposta.corretor.sucursal"/>
					<s:property value="proposta.corretor.sucursal"/>
				</s:else>
			</td>
			<td class="td_label" width="200px;" ></td>
			<td></td>
		</tr>
	</table>
	<table class="tabela_interna" id="tabela_planos">
		<tr>
			<th colspan="7">2. PLANO</th>
		</tr>
		<tr>
			<th width="13%" rowspan="2">Plano</th>
			<th width="10%" colspan="2">Valor Titular</th>
			<th width="11%" colspan="2">Valor Dependente</th>
			<th width="28%" colspan="2">Per&iacute;odo de Car&ecirc;ncia</th>
		</tr>
		<tr>
			<th>Mensal</th>
			<th>Anual</th>
			<th>Mensal</th>
			<th>Anual</th>
			<th>Mensal</th>
			<th>Anual</th>
		</tr>
		<s:if test="planos == null || planos == 0">
			<tr>
				<td colspan="7" align="center">Nenhum plano vigente encontrado para o canal CORRETORA CALL CENTER PROPRIO</td>
			</tr>
		</s:if>
		<s:else>
			<s:iterator value="planos" var="plano" status="status">
				<tr>
			        <td>
<%-- 			        	<s:if test="codigo == proposta.plano.codigo"> --%>
<%-- 			        		<input class="radio_plano" type="radio" name="proposta.plano.codigo" id="proposta_${status.index}" value="<s:property value='codigo'/>" checked="checked"> --%>
<!-- 			        		<label> -->
<%-- 			        			<s:property value="nome"/> --%>
<!-- 			        		</label> -->
<%-- 			        	</s:if> --%>
			        	
			        		<input class="radio_plano" type="radio"  name="proposta.plano.codigo" value="<s:property value='codigo'/>">
			        		<span id="t"><s:property  value="nome"/></span>
			        	
			        </td>
			        <td>
			        	<fmt:formatNumber value="${plano.valorPlanoVO.valorMensalTitular}" type="currency" currencySymbol="R$"/>
			        </td>
			        <td>
			        	<fmt:formatNumber value="${plano.valorPlanoVO.valorAnualTitular}" type="currency" currencySymbol="R$"/>
			        </td>
			        <td>
			        	<fmt:formatNumber value="${plano.valorPlanoVO.valorMensalDependente}" type="currency" currencySymbol="R$"/>
			        </td>
			        <td>
			        	<fmt:formatNumber value="${plano.valorPlanoVO.valorAnualDependente}" type="currency" currencySymbol="R$"/>
			        </td>
			        <td><s:property value="descricaoCarenciaPeriodoMensal"/></td>
			        <td><s:property value="descricaoCarenciaPeriodoAnual"/></td>
			     </tr>
			</s:iterator>
		</s:else>		
	     <!-- tr>
			<td colspan="7" align="center">
				<a href="#" class="margem_botoes"><img border="0" src="<s:url value='/includes/img/ic_sbox_calculadora.gif'/>">Simular Contrata��o</a>
			</td>
		</tr-->
	</table>
	<table class="tabela_interna">
		<tr>
			<th colspan="2">3. BENEFICI�RIOS</th>
		</tr>
		<tr>
	        <td class="td_label" width="292px;">O benefici�rio titular � menor de idade?<span class="obrigatorio">*</span></td>
	        <td>
	        	<!-- input type="radio" name="proposta.beneficiarios.indicadorTitularMenorIdade" value="true" />Sim
	        	<input type="radio" name="proposta.beneficiarios.indicadorTitularMenorIdade" value="false" />N�o-->
	        	<s:radio name="proposta.beneficiarios.titularMenorIdade" list="#{true:'Sim',false:'N�o'}" />
	        </td>
	    </tr>
	</table>
	<table class="tabela_interna" id="representante_legal" style="display:none;">
		<tr>
			<th colspan="4">Representante Legal</th>
		</tr>
		<tr>
			<td class="td_label" width="127px;">Nome<span class="obrigatorio">*</span></td>
			<td><s:textfield name="proposta.beneficiarios.representanteLegal.nome" size="40" maxlength="100" style="text-transform: uppercase;"/></td>
			<td class="td_label" width="149px;">CPF<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.beneficiarios.representanteLegal.cpf" data-mask="cpf" size="11" maxlength="14" /></td>
		</tr>
		<tr>
			<td class="td_label">Nascimento<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.beneficiarios.representanteLegal.dataNascimento" data-mask="data" size="7" maxlength="10" /></td>
			<td class="td_label" >E-mail<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.beneficiarios.representanteLegal.email" size="30" maxlength="100" style="text-transform: uppercase;"/></td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>ENDERE�O</b></td>
		</tr>
		<tr>
			<td class="td_label">CEP<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:textfield name="proposta.beneficiarios.representanteLegal.endereco.cep" data-mask="cep" size="8" maxlength="11" />
				&nbsp;<input type="button" value="Buscar" id="buscarCepRepresentanteLegal"></td>
			</td>
		</tr>
		<tr class="cep_representante_legal" style="display: none;">
			<td class="td_label">Endere�o<span class="obrigatorio">*</span></td>
			<td>
				<%--Logradouro modificado para n�o permitir a edi��o do campo
					Modificado por: Kaio e Gabriel 05/08.
				--%>
				<input type="hidden" name="proposta.beneficiarios.representanteLegal.endereco.logradouro">
				<s:textfield id="logradouro-representante"
					name="proposta.beneficiarios.representanteLegal.endereco.logradouro"
					size="40" maxlength="50" />
				<!-- s:hidden name="proposta.beneficiarios.representanteLegal.endereco.logradouro"/-->
				<!-- s:property value="proposta.beneficiarios.representanteLegal.endereco.logradouro"/-->
			</td>
			<td class="td_label">N�mero<span class="obrigatorio">*</span></td>
			<td><s:textfield name="proposta.beneficiarios.representanteLegal.endereco.numero" data-type="integer" size="5" maxlength="5" /></td>
		</tr>
		<tr class="cep_representante_legal" style="display: none;">
			<td class="td_label">Complemento</td>
			<td><s:textfield name="proposta.beneficiarios.representanteLegal.endereco.complemento" size="30" style="text-transform: uppercase;"/></td>
			<td class="td_label">Bairro</td>
			<td>
				<!-- s:hidden name="proposta.beneficiarios.representanteLegal.endereco.bairro"/-->
				<!-- s:property value="proposta.beneficiarios.representanteLegal.endereco.bairro"/-->
				<s:textfield name="proposta.beneficiarios.representanteLegal.endereco.bairro" size="21" />
			</td>
		</tr>
		<tr class="cep_representante_legal" style="display: none;">
			<td class="td_label">Cidade</td>
			<td>
				<s:hidden name="proposta.beneficiarios.representanteLegal.endereco.cidade" />
				<label id=cidade-representante>
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.cidade" />
				</label>
				<!-- s:textfield name="proposta.beneficiarios.representanteLegal.endereco.cidade" size="20" /-->
			</td>
			<td class="td_label">Estado</td>
			<td>
				<s:hidden name="proposta.beneficiarios.representanteLegal.endereco.estado" />
				<label id="estado-representante">
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.estado" />
				</label>
				<!-- s:textfield name="proposta.beneficiarios.representanteLegal.endereco.estado" size="3" /-->
			</td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>CONTATO</b></td>
		</tr>
		<tr>
	        <td class="td_label" >Tipo de Telefone<span class="obrigatorio">*</span></td>
			<td>
				<s:select name="proposta.beneficiarios.representanteLegal.telefones[0].tipoTelefone" list="tiposTelefone" listValue="nome" listKey="codigo" headerKey="0" headerValue="Selecione" id="tipo_telefone_representante_legal" data-telefone="telefone_representante_legal" />
			</td>
			<td class="td_label" >N� Telefone / Ramal<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="proposta.beneficiarios.representanteLegal.telefones[0].dddEnumero" id="telefone_representante_legal" data-telefone="telefone_representante_legal" data-mask="telefone" size="11" /> / <s:textfield name="proposta.beneficiarios.representanteLegal.telefones[0].ramal" data-type="integer" size="3" maxlength="4" />
			</td>
	    </tr>
	    
	</table>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">Titular</th>
		</tr>
		<tr>
			<td class="td_label" width="125px;">Nome da M�e<span class="obrigatorio">*</span></td>
			<td><s:textfield name="proposta.beneficiarios.titular.nomeMae" size="40" maxlength="100" style="text-transform: uppercase;" /></td>
			<td class="td_label" width="150px;">CPF<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.beneficiarios.titular.cpf" data-mask="cpf" size="11" maxlength="14" /></td>
		
		</tr>
		<tr>
			<td class="td_label" width="125px;">Nome Completo<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.beneficiarios.titular.nome" size="40" maxlength="100" style="text-transform: uppercase;"/></td>
				<td colspan="1" class="td_label">Grau de Parentesco 
					entre Titular e Respons�vel Financeiro<span class="obrigatorio">*</span></td>
					<td>
				<s:select name="proposta.beneficiarios.titular.grauParentescoTitularProponente" list="grausParentesco" listKey="codigo" listValue="nome" headerKey="0" headerValue="Selecione"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Nascimento<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.beneficiarios.titular.dataNascimento" data-mask="data" size="11" maxlength="11" /></td>
			<td class="td_label" >E-mail<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.beneficiarios.titular.email" size="30" maxlength="100" style="text-transform: uppercase;"/></td>
		</tr>
		<tr>
	        <td class="td_label" >Sexo<span class="obrigatorio">*</span></td>
	        <td>
	        	<s:radio name="proposta.beneficiarios.titular.sexo" list="sexos" listKey="codigo" listValue="nome"/>
	        </td>
	        <td class="td_label" >Estado Civil<span class="obrigatorio">*</span></td>
			<td>
				<s:select name="proposta.beneficiarios.titular.estadoCivil" list="estadosCivis" listKey="codigo" listValue="nome" headerKey="0" headerValue="Selecione"/>
			</td>
	    </tr>
	    <tr>
			<td class="td_label">CNS</td>
			<td ><s:textfield name="proposta.beneficiarios.titular.cns" data-type="integer" size="20" maxlength="20"/></td>
			<td class="td_label" >DNV</td>
			<td ><s:textfield name="proposta.beneficiarios.titular.dnv" data-type="integer" size="20" maxlength="20"/></td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>ENDERE�O</b></td>
		</tr>
		<tr>
			<td class="td_label">CEP<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:textfield name="proposta.beneficiarios.titular.endereco.cep" data-mask="cep" size="8" maxlength="11" />
				&nbsp;<input type="button" value="Buscar" id="buscarCep">
			</td>
				<!-- &nbsp;
				<input type="button" value="Buscar">  -->
		</tr>
		<tr class="cep_titular" style="display: none;">
			<td class="td_label">Logradouro<span class="obrigatorio">*</span></td>
			<td>
				<%--Logradouro modificado para n�o permitir a edi��o do campo
					Modificado por: Kaio e Gabriel 05/08.
				--%>
				<input type="hidden" name="proposta.beneficiarios.titular.endereco.logradouro">
				<s:textfield id="logradouro-titular" name="proposta.beneficiarios.titular.endereco.logradouro"
					size="40" maxlength="50" />
				<!-- s:property value="proposta.beneficiarios.titular.endereco.logradouro"/-->
				<!-- s:textfield name="proposta.beneficiarios.titular.endereco.logradouro" size="40" /-->
			</td>
			<td class="td_label">N�mero<span class="obrigatorio">*</span></td>
			<td><s:textfield name="proposta.beneficiarios.titular.endereco.numero" data-type="integer" size="5" maxlength="5" /></td>
		</tr>
		<tr class="cep_titular" style="display: none;">
			<td class="td_label">Complemento</td>
			<td><s:textfield name="proposta.beneficiarios.titular.endereco.complemento" size="30" style="text-transform: uppercase;"/></td>
			<td class="td_label">Bairro<span class="obrigatorio">*</span></td>
			<td>
				<!-- s:hidden name="proposta.beneficiarios.titular.endereco.bairro"/-->
				<!-- s:property value="proposta.beneficiarios.titular.endereco.bairro"/-->
				<s:textfield name="proposta.beneficiarios.titular.endereco.bairro" size="21"/>
			</td>
		</tr>
		<tr class="cep_titular" style="display: none;">
			<td class="td_label">Cidade</td>
			<td>
				<s:hidden name="proposta.beneficiarios.titular.endereco.cidade" />
				<label id="cidade-titular">
					<s:property value="proposta.beneficiarios.titular.endereco.cidade" />
				</label>
				<!-- s:textfield name="proposta.beneficiarios.titular.endereco.cidade" size="20" /-->
			</td>
			<td class="td_label">Estado</td>
			<td>
				<s:hidden name="proposta.beneficiarios.titular.endereco.estado" />
				<label id="estado-titular">
					<s:property value="proposta.beneficiarios.titular.endereco.estado" />
				</label>
				<!-- s:textfield name="proposta.beneficiarios.titular.endereco.estado" size="3" /-->
			</td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>CONTATO</b><span class="obrigatorio">*</span></td>
		</tr>
		<tr>
	        <td class="td_label" >Tipo de Telefone</td>
			<td>
				Celular
				<s:hidden name="proposta.beneficiarios.titular.telefones[0].tipoTelefone"/>
			</td>
			<td class="td_label" >N� Telefone</td>
			<td>
				<s:textfield name="proposta.beneficiarios.titular.telefones[0].dddEnumero" data-mask="celular" size="11" />
			</td>
	    </tr>
	    <tr>
	        <td class="td_label" >Tipo de Telefone</td>
			<td>
				Comercial
				<s:hidden name="proposta.beneficiarios.titular.telefones[1].tipoTelefone"/>
			</td>
			<td class="td_label" >N� Telefone / Ramal</td>
			<td>
				<s:textfield name="proposta.beneficiarios.titular.telefones[1].dddEnumero" data-mask="telefone" size="11" /> / <s:textfield name="proposta.beneficiarios.titular.telefones[0].ramal" data-type="integer" size="3" maxlength="4" />
			</td>
	    </tr>
	    <tr>
	        <td class="td_label" >Tipo de Telefone</td>
			<td>
				Residencial
				<s:hidden name="proposta.beneficiarios.titular.telefones[2].tipoTelefone"/>
			</td>
			<td class="td_label" >N� Telefone</td>
			<td><s:textfield name="proposta.beneficiarios.titular.telefones[2].dddEnumero" data-mask="telefone" size="11" /></td>
	    </tr>
	</table>
	<table class="tabela_interna" id="dependentesForm">
		<tr>
			<th colspan="4">Dependente(s)</th>
		</tr>
		<s:if test="proposta.beneficiarios.dependentes == null || proposta.beneficiarios.dependentes.size == 0">
			<tr>
				<td colspan="4" align="center">
					<a href="javacsript:void(0);" class="margem_botoes" id="adicionar_dependente">
					<img border="0" src="<s:url value='/includes/img/beneficiario_add.png'/>"> Adicionar Dependente</a>
				</td>
			</tr>
		</s:if>
	</table>
	<s:iterator value="proposta.beneficiarios.dependentes" var="dependente" status="status">
		<table class="tabela_interna" id="dependentes">
			<s:if test="%{#status.index > 0}">
				<tr>
					<td class="td_label" colspan="4" align="center"><b>DEPENDENTE</b></td>
				</tr>
			</s:if>
			<tr>
				<td class="td_label" width="125px;">Nome da M�e</td>
				<td><s:textfield name="proposta.beneficiarios.dependentes[%{#status.index}].nomeMae" size="35" maxlength="100" style="text-transform: uppercase;"/></td>
				
				<td colspan="1" class="td_label">Grau de Parentesco 
					entre Titular e Dependente<span class="obrigatorio">*</span></td>
					<td>
						<s:select name="proposta.beneficiarios.dependentes[%{#status.index}].grauParentesco" list="grausParentesco" listKey="codigo" listValue="nome" headerKey="0" headerValue="Selecione"/>
					</td>
			</tr>
			<tr>
				<td class="td_label" width="125px;">Nome Completo<span class="obrigatorio">*</span></td>
				<td width="380px;"><s:textfield name="proposta.beneficiarios.dependentes[%{#status.index}].nome" size="40" maxlength="100" style="text-transform: uppercase;"/></td>
				<td class="td_label" width="125px;">CPF</td>
				<td><s:textfield name="proposta.beneficiarios.dependentes[%{#status.index}].cpf" data-mask="cpf" size="11" maxlength="14" /></td>
			</tr>
			<tr>
				<td class="td_label">Nascimento<span class="obrigatorio">*</span></td>
				<td ><s:textfield name="proposta.beneficiarios.dependentes[%{#status.index}].dataNascimento" data-mask="data" size="11" maxlength="11" /></td>
				<td class="td_label" >Grau de Parentesco 
					entre Dependente e Resp Financeiro<span class="obrigatorio">*</span></td>
				<td>
					<s:select name="proposta.beneficiarios.dependentes[%{#status.index}].grauParentescoBeneficiario" list="grausParentesco" listKey="codigo" listValue="nome" headerKey="0" headerValue="Selecione"/>
				</td>
			</tr>
			<tr>
		        <td class="td_label" >Sexo<span class="obrigatorio">*</span></td>
		        <td>
		        	<s:radio name="proposta.beneficiarios.dependentes[%{#status.index}].sexo" list="sexos" listKey="codigo" listValue="nome"/>
		        </td>
		        <td class="td_label" >Estado Civil<span class="obrigatorio">*</span></td>
				<td>
					<s:select name="proposta.beneficiarios.dependentes[%{#status.index}].estadoCivil" list="estadosCivis" listKey="codigo" listValue="nome" headerKey="0" headerValue="Selecione"/>
				</td>
		    </tr>
		    <tr>
				<td class="td_label">CNS</td>
				<td ><s:textfield name="proposta.beneficiarios.dependentes[%{#status.index}].cns" data-type="integer" size="20" maxlength="20" /></td>
				<td class="td_label" >DNV</td>
				<td ><s:textfield name="proposta.beneficiarios.dependentes[%{#status.index}].dnv" data-type="integer" size="20" maxlength="20"/></td>
			</tr>
			<s:if test="%{proposta.beneficiarios.dependentes.size() == #status.index + 1}">
				<tr>
					<td colspan="2" align="right">
						<a href="javacsript:void(0);" class="margem_botoes" id="adicionar_dependente"><img border="0" src="<s:url value='/includes/img/beneficiario_add.png'/>"> Adicionar Dependente</a>
					</td>
					<td colspan="2" align="left">
						<a href="javacsript:void(0);" class="margem_botoes" onclick="javascript: removerDependente(<s:property value='#status.index'/>);"><img border="0" src="<s:url value='/includes/img/beneficiario_del.png'/>"> Remover Dependente</a>
					</td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td colspan="4" align="center">
						<a href="javacsript:void(0);" class="margem_botoes" onclick="javascript: removerDependente(<s:property value='#status.index'/>);"><img border="0" src="<s:url value='/includes/img/beneficiario_del.png'/>"> Remover Dependente</a>
					</td>
				</tr>
			</s:else>
		</table>
	</s:iterator>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">4. PAGAMENTO</th>
		</tr>
		<tr>
			<td class="td_label" width="170px;">N�mero de Vidas</td>
			<td colspan="3">
				<s:if test="proposta.beneficiarios.dependentes == null || proposta.beneficiarios.dependentes.size == 0">
					<span id="numero_vidas">1</span>
				</s:if>
				<s:else>
					<span id="numero_vidas"><s:property value="%{proposta.beneficiarios.dependentes.size + 1}"/></span>
				</s:else>
			</td>
		</tr>
		<tr>
			<td class="td_label" width="170px;">Valor Total Mensal</td>
			<td width="368px;">
				<label id="mensal-label" class="mascara-moeda">
					${valorTotalMensal}
				</label>
			</td>
			<td class="td_label" width="140px;">Valor Total Anual</td>
			<td>
				<label id="anual-label" class="mascara-moeda">
					${valorTotalAnual}
				</label>
			</td>
			<s:hidden value="%{valorTotalMensal}" id="valorTotalMensal"></s:hidden>
			<s:hidden value="%{valorTotalAnual}" id="valorTotalAnual"></s:hidden>
		</tr>
		<tr>
			<td class="td_label">Tipo de Cobran�a<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:radio name="proposta.pagamento.tipoCobranca" list="tiposCobranca" listValue="nome" listKey="codigo"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Forma de Pagamento<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<!-- s:radio id="forma_pagamento" name="proposta.pagamento.formaPagamento" list="formasPagamento" listValue="nome" listKey="codigo"/ -->
				<s:hidden value="%{proposta.pagamento.formaPagamento}" id="forma-pagamento-selecionado" />
				<s:select id="forma_pagamento" name="proposta.pagamento.formaPagamento" list="formasPagamento" listKey="codigo" listValue="descricao" headerKey="0" headerValue="Selecione"/>
			</td>
		</tr>
		
		<!--  D�BITO -->
		<tr class="banco_pagamento" style="display:none;">
			<td class="td_label">Data de Cobran�a<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:textfield name="proposta.pagamento.dataCobranca" data-mask="data" size="8" maxlength="11" /> (15 a 45 dias da data atual)
			</td>
		</tr>
		<tr class="banco_pagamento" style="display:none;">
			<td class="td_label">Banco<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:select id="banco" name="proposta.pagamento.contaCorrente.banco" list="bancos" listKey="codigo" listValue="descricao" headerKey="0" headerValue="Selecione"/>
			</td>
		</tr>
		<tr class="cpf_pagamento" style="display:none;">
			<td class="td_label" colspan="4" align="center"><b>PROPONENTE</b></td>
		</tr>
		<tr class="cpf_pagamento" style="display:none;">
			<td class="td_label">CPF<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:textfield name="proposta.pagamento.contaCorrente.cpf" id="cpf_pagamento" data-mask="cpf" size="12" maxlength="14" />
			</td>
		</tr>
		<tr class="dados_debito_automatico" style="display:none;">
			<td class="td_label">Nome<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.pagamento.contaCorrente.nome" id="nome_pagante" size="40" style="text-transform: uppercase;"/></td>
			<td class="td_label">Nascimento<span class="obrigatorio">*</span></td>
			<td ><s:textfield name="proposta.pagamento.contaCorrente.dataNascimento" id="nascimento_pagante" data-mask="data" size="9" maxlength="10" /></td>
		</tr>
		<tr class="dados_debito_automatico_outros_bancos" style="display:none;">
			<td class="td_label">Ag�ncia / Conta<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:textfield name="proposta.pagamento.contaCorrente.numeroAgencia" id="agencia" data-type="integer" size="4" maxlength="4" /> 
				/ 
				<s:textfield name="proposta.pagamento.contaCorrente.numeroConta" id="conta" data-type="integer" size="9" maxlength="9" /> 
				- 
				<s:textfield name="proposta.pagamento.contaCorrente.digitoVerificadorConta" id="digito_conta" size="1" maxlength="1" />
			</td>
		</tr>
		<!-- tr class="dados_debito_automatico_bradesco" style="display:none;">
			<td class="td_label">Ag�ncia / Conta<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:select name="contaCorrenteFormatada" id="agencia_conta" list="contasCorrente" headerKey="0" headerValue="Selecione" listKey="numeroConta" listValue="contaCorrenteFormatada"/>
			</td>
		</tr-->
		
		<!--  FIM D�BITO -->
		
		<!-- CARTAO CREDITO -->
			
			<tr class="cartao_credito">
				<td class="td_label" align="center" colspan="4"><b>CART�O DE CR�DITO</b></td>
			</tr>
			<tr class="cartao_credito">
				<td class="td_label">Nome</td>
				<td><input type="text" size="35"
				id="cartaoCredito.nomeCartao"
				name="proposta.pagamento.cartaoCredito.nomeCartao"
				class="bp-sop-cardholdername" /></td>
				<td class="td_label">CPF<span class="obrigatorio">*</span></td>
				<td>
					<s:textfield name="proposta.pagamento.cartaoCredito.cpfCartao" id="cpf_cartao_credito" data-mask="cpf" size="12" maxlength="14" />
				</td>
			</tr>
			<tr class="cartao_credito">
				<td class="td_label">N�mero</td>
				<td><input type="text" size="18" maxlength="18"
				id="cartaoCredito.numeroCartao"
				name="proposta.pagamento.cartaoCredito.numeroCartao"
				class="bp-sop-cardnumber" onkeypress='return verificaNumero(event)' /></td>
				<td class="td_label">Validade</td>
				<td><input type="text" size="6" id="cartaoCredito.validade"
				name="proposta.pagamento.cartaoCredito.validade"
				class="bp-sop-cardexpirationdate" data-mask="data-cartao" /></td>
			</tr>
			<tr class="cartao_credito">
				<td class="td_label">Bandeira</td>
				<td>
					<s:select id="bandeiras"
					name="proposta.pagamento.cartaoCredito.bandeira" list="bandeiras"
					listKey="codigo" listValue="descricao" headerKey="0"
					headerValue="Selecione" />
				</td>
				<td class="td_label">Data Nascimento</td>
				<td><s:textfield name="proposta.pagamento.cartaoCredito.dataNascimento" 
				data-mask="data" size="7" maxlength="10" /></td>

			</tr>
		
			<!-- FIM CARTAO CREDITO -->
	</table>
	
	<br />
	<br />
	<br />
	
	<table style="width: 974px;">
		<tr>
			<!-- td width="45%" align="right"><input type="button" value="Limpar" id="btn_limpar"/></td>
			<td width="10%"></td-->
			
			<td align="center">
				<input id="finalizar" name="finalizar" type="button" value="Finalizar" />
			</td>
		</tr>
	</table>
	
	<br />
	<br />
	<s:hidden name="proposta.pagamento.contaCorrente.dataNascimento"></s:hidden>
	<s:hidden name="proposta.pagamento.cartaoCredito.accessToken"
		cssClass="accessToken"></s:hidden>
	<s:hidden name="proposta.pagamento.cartaoCredito.paymentToken"
		cssClass="paymentToken"></s:hidden>
	<s:hidden id="ambiente"></s:hidden>
</s:form>
