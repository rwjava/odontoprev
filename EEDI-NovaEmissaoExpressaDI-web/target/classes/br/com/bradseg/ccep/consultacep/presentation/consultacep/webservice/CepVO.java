
package br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cepVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cepVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descricaoLogradouro" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroInicial" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroFinal" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complemento" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bairro" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cidade" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="siglaUf" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cep" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoCep" type="{http" + "://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="cepMunicSubor" type="{http" + "://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="bairroAbrev" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="logradouroAbrev" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="siglaUfIBGE" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeUf" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="municipioIBGE" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unidadeOper" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoLogradouro" type="{http" + "://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cepVO", propOrder = {
    "descricaoLogradouro",
    "numeroInicial",
    "numeroFinal",
    "complemento",
    "bairro",
    "cidade",
    "siglaUf",
    "cep",
    "tipoCep",
    "cepMunicSubor",
    "bairroAbrev",
    "logradouroAbrev",
    "siglaUfIBGE",
    "nomeUf",
    "municipioIBGE",
    "unidadeOper",
    "codigoLogradouro"
})
@XmlSeeAlso({
    CepCompletoVO.class
})
public class CepVO {

    protected String descricaoLogradouro;
    protected String numeroInicial;
    protected String numeroFinal;
    protected String complemento;
    protected String bairro;
    protected String cidade;
    protected String siglaUf;
    protected String cep;
    protected Long tipoCep;
    protected Long cepMunicSubor;
    protected String bairroAbrev;
    protected String logradouroAbrev;
    protected String siglaUfIBGE;
    protected String nomeUf;
    protected String municipioIBGE;
    protected String unidadeOper;
    protected Long codigoLogradouro;

    /**
     * Gets the value of the descricaoLogradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoLogradouro() {
        return descricaoLogradouro;
    }

    /**
     * Sets the value of the descricaoLogradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoLogradouro(String value) {
        this.descricaoLogradouro = value;
    }

    /**
     * Gets the value of the numeroInicial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroInicial() {
        return numeroInicial;
    }

    /**
     * Sets the value of the numeroInicial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroInicial(String value) {
        this.numeroInicial = value;
    }

    /**
     * Gets the value of the numeroFinal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroFinal() {
        return numeroFinal;
    }

    /**
     * Sets the value of the numeroFinal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroFinal(String value) {
        this.numeroFinal = value;
    }

    /**
     * Gets the value of the complemento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * Sets the value of the complemento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplemento(String value) {
        this.complemento = value;
    }

    /**
     * Gets the value of the bairro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Sets the value of the bairro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairro(String value) {
        this.bairro = value;
    }

    /**
     * Gets the value of the cidade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Sets the value of the cidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidade(String value) {
        this.cidade = value;
    }

    /**
     * Gets the value of the siglaUf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiglaUf() {
        return siglaUf;
    }

    /**
     * Sets the value of the siglaUf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiglaUf(String value) {
        this.siglaUf = value;
    }

    /**
     * Gets the value of the cep property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCep() {
        return cep;
    }

    /**
     * Sets the value of the cep property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCep(String value) {
        this.cep = value;
    }

    /**
     * Gets the value of the tipoCep property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTipoCep() {
        return tipoCep;
    }

    /**
     * Sets the value of the tipoCep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTipoCep(Long value) {
        this.tipoCep = value;
    }

    /**
     * Gets the value of the cepMunicSubor property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCepMunicSubor() {
        return cepMunicSubor;
    }

    /**
     * Sets the value of the cepMunicSubor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCepMunicSubor(Long value) {
        this.cepMunicSubor = value;
    }

    /**
     * Gets the value of the bairroAbrev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairroAbrev() {
        return bairroAbrev;
    }

    /**
     * Sets the value of the bairroAbrev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairroAbrev(String value) {
        this.bairroAbrev = value;
    }

    /**
     * Gets the value of the logradouroAbrev property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogradouroAbrev() {
        return logradouroAbrev;
    }

    /**
     * Sets the value of the logradouroAbrev property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogradouroAbrev(String value) {
        this.logradouroAbrev = value;
    }

    /**
     * Gets the value of the siglaUfIBGE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiglaUfIBGE() {
        return siglaUfIBGE;
    }

    /**
     * Sets the value of the siglaUfIBGE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiglaUfIBGE(String value) {
        this.siglaUfIBGE = value;
    }

    /**
     * Gets the value of the nomeUf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeUf() {
        return nomeUf;
    }

    /**
     * Sets the value of the nomeUf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeUf(String value) {
        this.nomeUf = value;
    }

    /**
     * Gets the value of the municipioIBGE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMunicipioIBGE() {
        return municipioIBGE;
    }

    /**
     * Sets the value of the municipioIBGE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMunicipioIBGE(String value) {
        this.municipioIBGE = value;
    }

    /**
     * Gets the value of the unidadeOper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidadeOper() {
        return unidadeOper;
    }

    /**
     * Sets the value of the unidadeOper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidadeOper(String value) {
        this.unidadeOper = value;
    }

    /**
     * Gets the value of the codigoLogradouro property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoLogradouro() {
        return codigoLogradouro;
    }

    /**
     * Sets the value of the codigoLogradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoLogradouro(Long value) {
        this.codigoLogradouro = value;
    }

}
