
package br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cepWebServiceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cepWebServiceResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mensagem" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lista" type="{http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/}cepVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cepWebServiceResponse", propOrder = {
    "mensagem",
    "lista"
})
public class CepWebServiceResponse {

    protected String mensagem;
    @XmlElement(nillable = true)
    protected List<CepVO> lista;

    /**
     * Gets the value of the mensagem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Sets the value of the mensagem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagem(String value) {
        this.mensagem = value;
    }

    /**
     * Gets the value of the lista property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lista property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLista().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CepVO }
     * 
     * 
     */
    public List<CepVO> getLista() {
        if (lista == null) {
            lista = new ArrayList<CepVO>();
        }
        return this.lista;
    }

}
