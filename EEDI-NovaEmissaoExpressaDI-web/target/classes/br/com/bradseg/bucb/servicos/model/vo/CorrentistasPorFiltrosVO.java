
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorrentistasPorFiltrosVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorrentistasPorFiltrosVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bairro" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cep" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cidade" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="complemento" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cpfCnpj" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ddd" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="logradouro" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nome" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numero" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="situacao" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="situacaoContaDestino" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="telefone" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="titularidade" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tpPessoa" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="uf" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorrentistasPorFiltrosVO", propOrder = {
    "bairro",
    "cep",
    "cidade",
    "complemento",
    "cpfCnpj",
    "ddd",
    "logradouro",
    "nome",
    "numero",
    "situacao",
    "situacaoContaDestino",
    "telefone",
    "titularidade",
    "tpPessoa",
    "uf"
})
public class CorrentistasPorFiltrosVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(required = true, nillable = true)
    protected String bairro;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cep;
    @XmlElement(required = true, nillable = true)
    protected String cidade;
    @XmlElement(required = true, nillable = true)
    protected String complemento;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cpfCnpj;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long ddd;
    @XmlElement(required = true, nillable = true)
    protected String logradouro;
    @XmlElement(required = true, nillable = true)
    protected String nome;
    @XmlElement(required = true, nillable = true)
    protected String numero;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer situacao;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer situacaoContaDestino;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long telefone;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer titularidade;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tpPessoa;
    @XmlElement(required = true, nillable = true)
    protected String uf;

    /**
     * Gets the value of the bairro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Sets the value of the bairro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairro(String value) {
        this.bairro = value;
    }

    /**
     * Gets the value of the cep property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCep() {
        return cep;
    }

    /**
     * Sets the value of the cep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCep(Long value) {
        this.cep = value;
    }

    /**
     * Gets the value of the cidade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Sets the value of the cidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidade(String value) {
        this.cidade = value;
    }

    /**
     * Gets the value of the complemento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * Sets the value of the complemento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplemento(String value) {
        this.complemento = value;
    }

    /**
     * Gets the value of the cpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Sets the value of the cpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCpfCnpj(Long value) {
        this.cpfCnpj = value;
    }

    /**
     * Gets the value of the ddd property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDdd() {
        return ddd;
    }

    /**
     * Sets the value of the ddd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDdd(Long value) {
        this.ddd = value;
    }

    /**
     * Gets the value of the logradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * Sets the value of the logradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogradouro(String value) {
        this.logradouro = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the situacao property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSituacao() {
        return situacao;
    }

    /**
     * Sets the value of the situacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSituacao(Integer value) {
        this.situacao = value;
    }

    /**
     * Gets the value of the situacaoContaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSituacaoContaDestino() {
        return situacaoContaDestino;
    }

    /**
     * Sets the value of the situacaoContaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSituacaoContaDestino(Integer value) {
        this.situacaoContaDestino = value;
    }

    /**
     * Gets the value of the telefone property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTelefone() {
        return telefone;
    }

    /**
     * Sets the value of the telefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTelefone(Long value) {
        this.telefone = value;
    }

    /**
     * Gets the value of the titularidade property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTitularidade() {
        return titularidade;
    }

    /**
     * Sets the value of the titularidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTitularidade(Integer value) {
        this.titularidade = value;
    }

    /**
     * Gets the value of the tpPessoa property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTpPessoa() {
        return tpPessoa;
    }

    /**
     * Sets the value of the tpPessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTpPessoa(Integer value) {
        this.tpPessoa = value;
    }

    /**
     * Gets the value of the uf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUf() {
        return uf;
    }

    /**
     * Sets the value of the uf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUf(String value) {
        this.uf = value;
    }

}
