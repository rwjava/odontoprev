
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ContaCorrenteVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="consultarDadosCorrentistaReturn" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ContaCorrenteVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarDadosCorrentistaReturn"
})
@XmlRootElement(name = "consultarDadosCorrentistaResponse")
public class ConsultarDadosCorrentistaResponse {

    @XmlElement(required = true, nillable = true)
    protected ContaCorrenteVO consultarDadosCorrentistaReturn;

    /**
     * Gets the value of the consultarDadosCorrentistaReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ContaCorrenteVO }
     *     
     */
    public ContaCorrenteVO getConsultarDadosCorrentistaReturn() {
        return consultarDadosCorrentistaReturn;
    }

    /**
     * Sets the value of the consultarDadosCorrentistaReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContaCorrenteVO }
     *     
     */
    public void setConsultarDadosCorrentistaReturn(ContaCorrenteVO value) {
        this.consultarDadosCorrentistaReturn = value;
    }

}
