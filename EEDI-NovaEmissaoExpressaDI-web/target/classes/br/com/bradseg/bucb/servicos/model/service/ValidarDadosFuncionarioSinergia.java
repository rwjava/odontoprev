
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ValidarDadosFuncionarioSinergiaEntradaVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validarDadosFuncionarioSinergiaEntradaVO" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ValidarDadosFuncionarioSinergiaEntradaVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validarDadosFuncionarioSinergiaEntradaVO"
})
@XmlRootElement(name = "validarDadosFuncionarioSinergia")
public class ValidarDadosFuncionarioSinergia {

    @XmlElement(required = true, nillable = true)
    protected ValidarDadosFuncionarioSinergiaEntradaVO validarDadosFuncionarioSinergiaEntradaVO;

    /**
     * Gets the value of the validarDadosFuncionarioSinergiaEntradaVO property.
     * 
     * @return
     *     possible object is
     *     {@link ValidarDadosFuncionarioSinergiaEntradaVO }
     *     
     */
    public ValidarDadosFuncionarioSinergiaEntradaVO getValidarDadosFuncionarioSinergiaEntradaVO() {
        return validarDadosFuncionarioSinergiaEntradaVO;
    }

    /**
     * Sets the value of the validarDadosFuncionarioSinergiaEntradaVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidarDadosFuncionarioSinergiaEntradaVO }
     *     
     */
    public void setValidarDadosFuncionarioSinergiaEntradaVO(ValidarDadosFuncionarioSinergiaEntradaVO value) {
        this.validarDadosFuncionarioSinergiaEntradaVO = value;
    }

}
