
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CorrentistaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorrentistaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataNascimento" type="{http" + "://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="nomeCorrentista" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nomeMae" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nomePai" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoTipoPessoa" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoTitular" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroCpfCnpj" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorrentistaVO", propOrder = {
    "dataNascimento",
    "nomeCorrentista",
    "nomeMae",
    "nomePai",
    "codigoTipoPessoa",
    "codigoTitular",
    "numeroCpfCnpj"
})
public class CorrentistaVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataNascimento;
    @XmlElement(required = true, nillable = true)
    protected String nomeCorrentista;
    @XmlElement(required = true, nillable = true)
    protected String nomeMae;
    @XmlElement(required = true, nillable = true)
    protected String nomePai;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoTipoPessoa;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoTitular;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long numeroCpfCnpj;

    /**
     * Gets the value of the dataNascimento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataNascimento() {
        return dataNascimento;
    }

    /**
     * Sets the value of the dataNascimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataNascimento(XMLGregorianCalendar value) {
        this.dataNascimento = value;
    }

    /**
     * Gets the value of the nomeCorrentista property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCorrentista() {
        return nomeCorrentista;
    }

    /**
     * Sets the value of the nomeCorrentista property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCorrentista(String value) {
        this.nomeCorrentista = value;
    }

    /**
     * Gets the value of the nomeMae property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeMae() {
        return nomeMae;
    }

    /**
     * Sets the value of the nomeMae property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeMae(String value) {
        this.nomeMae = value;
    }

    /**
     * Gets the value of the nomePai property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomePai() {
        return nomePai;
    }

    /**
     * Sets the value of the nomePai property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomePai(String value) {
        this.nomePai = value;
    }

    /**
     * Gets the value of the codigoTipoPessoa property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoTipoPessoa() {
        return codigoTipoPessoa;
    }

    /**
     * Sets the value of the codigoTipoPessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoTipoPessoa(Integer value) {
        this.codigoTipoPessoa = value;
    }

    /**
     * Gets the value of the codigoTitular property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoTitular() {
        return codigoTitular;
    }

    /**
     * Sets the value of the codigoTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoTitular(Integer value) {
        this.codigoTitular = value;
    }

    /**
     * Gets the value of the numeroCpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroCpfCnpj() {
        return numeroCpfCnpj;
    }

    /**
     * Sets the value of the numeroCpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroCpfCnpj(Long value) {
        this.numeroCpfCnpj = value;
    }

}
