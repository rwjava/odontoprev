
package br.com.bradseg.bucb.servicos.model.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import br.com.bradseg.bucb.servicos.model.vo.ContaCorrentePorCPFCNPJVO;


/**
 * <p>Java class for ArrayOf_1799424901_nillable_ContaCorrentePorCPFCNPJVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOf_1799424901_nillable_ContaCorrentePorCPFCNPJVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContaCorrentePorCPFCNPJVO" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ContaCorrentePorCPFCNPJVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOf_1799424901_nillable_ContaCorrentePorCPFCNPJVO", propOrder = {
    "contaCorrentePorCPFCNPJVO"
})
public class ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(name = "ContaCorrentePorCPFCNPJVO", nillable = true)
    protected List<ContaCorrentePorCPFCNPJVO> contaCorrentePorCPFCNPJVO;

    /**
     * Gets the value of the contaCorrentePorCPFCNPJVO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contaCorrentePorCPFCNPJVO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContaCorrentePorCPFCNPJVO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContaCorrentePorCPFCNPJVO }
     * 
     * 
     */
    public List<ContaCorrentePorCPFCNPJVO> getContaCorrentePorCPFCNPJVO() {
        if (contaCorrentePorCPFCNPJVO == null) {
            contaCorrentePorCPFCNPJVO = new ArrayList<ContaCorrentePorCPFCNPJVO>();
        }
        return this.contaCorrentePorCPFCNPJVO;
    }

}
