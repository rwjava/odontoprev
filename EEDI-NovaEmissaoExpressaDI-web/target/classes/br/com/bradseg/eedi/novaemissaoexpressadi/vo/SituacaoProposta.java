package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum de situa��o da proposta
 * 
 * @author EEDI
 */
public enum SituacaoProposta {

	PENDENTE(1, "Pendente"), RASCUNHO(2, "Rascunho"), EM_PROCESSAMENTO(3, "Em Processamento"), CRITICADA(4, "Criticada"), CANCELADA(
			5, "Cancelada"), IMPLANTADA(6, "Implantada"), PRE_CANCELADA(7, "Pr�-Cancelada"), TODAS(8, "Todas");

	private Integer codigo;
	private String nome;

	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo C�digo da situa��o
	 * @param nome - Descri��o da situa��o
	 */
	private SituacaoProposta(Integer codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Ot�m c�digo
	 * 
	 * @return C�digo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Define o c�digo
	 * 
	 * @param codigo C�digo
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * Obt�m descri��o
	 * 
	 * @return Descri��o
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Define descri��o
	 * 
	 * @param nome - nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Metodo responsavel por listar as situa��es da proposta.
	 * 
	 * @return List<SituacaoProposta> - lista de situa��es da proposta.
	 */
	public static List<SituacaoProposta> listar(){
		List<SituacaoProposta> lista = new ArrayList<SituacaoProposta>();
		for (SituacaoProposta status : SituacaoProposta.values()) {
			lista.add(status);
		}
		return lista;
	}
	
	public static SituacaoProposta obterSituacaoPropostaPorCodigo(int codigo){
		for (SituacaoProposta status : SituacaoProposta.values()) {
			if(codigo == status.getCodigo()){
				return status;
			}
		}
		return null;
	}
}
