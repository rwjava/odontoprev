package br.com.bradseg.eedi.novaemissaoexpressadi.proposta.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.AgenciaBancariaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.AngariadorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.BancoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.ContaCorrenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FormaPagamento;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Sexo;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TelefoneVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoSucursalSeguradora;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.UnidadeFederativaVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.Constantes;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.Banco;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.DependenteVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.EstadoCivil;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.GrauParentesco;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PropostaVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.SituacaoProposta;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoCobranca;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoTelefone;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.ValorPlanoVO;

import com.google.common.base.Strings;

/**
 * Classe responsavel por converter o objeto proposta do servi�o para o objeto proposta da aplica��o.
 *
 */
@Scope("prototype")
@Named("PropostaConverter")
public class PropostaConverter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaConverter.class);

	/**
	 * Metodo responsavel por converter o objeto aplica��o para o objeto do servi�o.
	 * 
	 * @param propostaAplicacao - informa��es da proposta a serem convertidas.
	 * 
	 * @return PropostaVO - informa��es convertidas para o objeto do servi�o.
	 */
	public br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO converterPropostaAplicacaoParaPropostaServico(PropostaVO propostaAplicacao){
		
		br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO propostaServico = new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO();
		//sucursal
		propostaServico.setSucursalSeguradora(new SucursalSeguradoraVO());
		propostaServico.getSucursalSeguradora().setTipo(TipoSucursalSeguradora.EMISSORA);
		propostaServico.getSucursalSeguradora().setCodigo(propostaAplicacao.getCorretor().getSucursal());
		//corretor
		propostaServico.setCorretor(new CorretorVO());
		propostaServico.getCorretor().setCpd(propostaAplicacao.getCorretor().getCpd().intValue());
		propostaServico.getCorretor().setCpfCnpj(propostaAplicacao.getCorretor().getCpfCnpj());
		propostaServico.getCorretor().setSucursalSeguradora(propostaServico.getSucursalSeguradora());
		propostaServico.setAngariador(new AngariadorVO());
		propostaServico.getAngariador().setCpd(propostaAplicacao.getCorretor().getCpd().intValue());
		propostaServico.getAngariador().setCpfCnpj(propostaAplicacao.getCorretor().getCpfCnpj());
		propostaServico.getAngariador().setSucursalSeguradora(propostaServico.getSucursalSeguradora());
		propostaServico.setCodigoMatriculaAssistente(propostaAplicacao.getCorretor().getAssistenteProducao().toString());
		//plano
		propostaServico.setPlanoVO(new PlanoVO());
		propostaServico.getPlanoVO().setCodigo(propostaAplicacao.getPlano().getCodigo());
		//canal de venda
		propostaServico.setCanalVenda(new CanalVendaVO());
		propostaServico.getCanalVenda().setCodigo(Constantes.CODIGO_CANAL_CORRETORA_CALL_CENTER);

		//beneficiarios
		//#Representante Legal
		if(propostaAplicacao.getBeneficiarios().isTitularMenorIdade()){
			propostaServico.setResponsavelLegal(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.ResponsavelLegalVO());
			propostaServico.getResponsavelLegal().setNome(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getNome());
			propostaServico.getResponsavelLegal().setCpf(br.com.bradseg.eedi.novaemissaoexpressadi.util.Strings.desnormalizarCPF(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getCpf()));
			if(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getDataNascimento() != null){
				propostaServico.getResponsavelLegal().setDataNascimento(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
			}
			propostaServico.getResponsavelLegal().setEmail(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEmail());
			propostaServico.getResponsavelLegal().setEndereco(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.EnderecoVO());
			propostaServico.getResponsavelLegal().getEndereco().setCep(br.com.bradseg.eedi.novaemissaoexpressadi.util.Strings.normalizarCep(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getCep()));
			propostaServico.getResponsavelLegal().getEndereco().setLogradouro(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getLogradouro());
			propostaServico.getResponsavelLegal().getEndereco().setNumero(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getNumero());
			propostaServico.getResponsavelLegal().getEndereco().setComplemento(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getComplemento());
			propostaServico.getResponsavelLegal().getEndereco().setBairro(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getBairro());
			propostaServico.getResponsavelLegal().getEndereco().setCidade(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getCidade());
			propostaServico.getResponsavelLegal().getEndereco().setUnidadeFederativa(new UnidadeFederativaVO());
			propostaServico.getResponsavelLegal().getEndereco().getUnidadeFederativa().setSigla(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getEstado());
			propostaServico.getResponsavelLegal().setTelefone(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TelefoneVO());
			propostaServico.getResponsavelLegal().getTelefone().setTipo(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoTelefone.valueOf(TipoTelefone.obterTipoTelefonePorCodigo(Integer.valueOf(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getTipoTelefone())).name()));
			propostaServico.getResponsavelLegal().getTelefone().setDdd(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getDdd());
			propostaServico.getResponsavelLegal().getTelefone().setNumero(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getNumero());
			if(!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getRamal())){
				propostaServico.getResponsavelLegal().getTelefone().setRamal(Integer.valueOf(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getRamal()));
			}
		}
		//#Titular
		propostaServico.setTitular(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TitularVO());
		propostaServico.getTitular().setNomeMae(propostaAplicacao.getBeneficiarios().getTitular().getNomeMae());
		propostaServico.getTitular().setNome(propostaAplicacao.getBeneficiarios().getTitular().getNome());
		propostaServico.getTitular().setCpf(br.com.bradseg.eedi.novaemissaoexpressadi.util.Strings.desnormalizarCPF(propostaAplicacao.getBeneficiarios().getTitular().getCpf()));
		if(propostaAplicacao.getBeneficiarios().getTitular().getDataNascimento() != null){
			propostaServico.getTitular().setDataNascimento(propostaAplicacao.getBeneficiarios().getTitular().getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		}
		propostaServico.getTitular().setEmail(propostaAplicacao.getBeneficiarios().getTitular().getEmail());
		if(propostaAplicacao.getBeneficiarios().getTitular().getSexo() != null){
			propostaServico.getTitular().setSexo(Sexo.fromValue(propostaAplicacao.getBeneficiarios().getTitular().getSexo()));
		}
		if(propostaAplicacao.getBeneficiarios().getTitular().getEstadoCivil() != null){
			propostaServico.getTitular().setEstadoCivil(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.EstadoCivil.valueOf(EstadoCivil.obterEstadoCivilPorCodigo(Integer.valueOf(propostaAplicacao.getBeneficiarios().getTitular().getEstadoCivil())).name()));
		}
		
		if(propostaAplicacao.getBeneficiarios().getTitular().getGrauParentescoTitularProponente() != null){
			propostaServico.getTitular().setGrauParentescoTitulaProponente(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.GrauParentesco.valueOf(GrauParentesco.obterGrauParentescoPorCodigo(Integer.valueOf(propostaAplicacao.getBeneficiarios().getTitular().getGrauParentescoTitularProponente())).name()));
		}
		if(!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getCns())){
			propostaServico.getTitular().setNumeroCartaoNacionalSaude(Long.valueOf(propostaAplicacao.getBeneficiarios().getTitular().getCns()));
		}
		if(!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getDnv())){
			propostaServico.getTitular().setNumeroDeclaracaoNascidoVivo(Long.valueOf(propostaAplicacao.getBeneficiarios().getTitular().getDnv()));
		}		
		propostaServico.getTitular().setEndereco(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.EnderecoVO());
		propostaServico.getTitular().getEndereco().setCep(br.com.bradseg.eedi.novaemissaoexpressadi.util.Strings.normalizarCep(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getCep()));
		propostaServico.getTitular().getEndereco().setLogradouro(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getLogradouro());
		propostaServico.getTitular().getEndereco().setNumero(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getNumero());
		propostaServico.getTitular().getEndereco().setComplemento(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getComplemento());
		propostaServico.getTitular().getEndereco().setBairro(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getBairro());
		propostaServico.getTitular().getEndereco().setCidade(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getCidade());
		propostaServico.getTitular().getEndereco().setUnidadeFederativa(new UnidadeFederativaVO());
		propostaServico.getTitular().getEndereco().getUnidadeFederativa().setSigla(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getEstado());
		//#Dependentes
		if(propostaAplicacao.getBeneficiarios().getDependentes().size() > 0){
			br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DependenteVO dependenteServico = null;
			for (DependenteVO dependenteAplicacao : propostaAplicacao.getBeneficiarios().getDependentes()) {
				dependenteServico = new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DependenteVO();
				dependenteServico.setNomeMae(dependenteAplicacao.getNomeMae());
				dependenteServico.setNome(dependenteAplicacao.getNome());
				dependenteServico.setCpf(br.com.bradseg.eedi.novaemissaoexpressadi.util.Strings.desnormalizarCPF(dependenteAplicacao.getCpf()));
				if(dependenteAplicacao.getDataNascimento() != null){
					dependenteServico.setDataNascimento(dependenteAplicacao.getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
				}
				if(dependenteAplicacao.getSexo() != null){
					dependenteServico.setSexo(Sexo.fromValue(dependenteAplicacao.getSexo()));
				}
				if(dependenteAplicacao.getEstadoCivil() != null){
					dependenteServico.setEstadoCivil(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.EstadoCivil.valueOf(EstadoCivil.obterEstadoCivilPorCodigo(Integer.valueOf(dependenteAplicacao.getEstadoCivil())).name()));
				}
				if(dependenteAplicacao.getGrauParentesco() != null){
					dependenteServico.setGrauParentesco(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.GrauParentesco.valueOf(GrauParentesco.obterGrauParentescoPorCodigo(Integer.valueOf(dependenteAplicacao.getGrauParentesco())).name()));
				}
				
				if(dependenteAplicacao.getGrauParentescoBeneficiario() != null){
					dependenteServico.setGrauParentescoBeneficiario(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.GrauParentesco.valueOf(GrauParentesco.obterGrauParentescoPorCodigo(Integer.valueOf(dependenteAplicacao.getGrauParentescoBeneficiario())).name()));
				}
				if(!Strings.isNullOrEmpty(dependenteAplicacao.getCns())){
					dependenteServico.setNumeroCartaoNacionalSaude(Long.valueOf(dependenteAplicacao.getCns()));
				}
				if(!Strings.isNullOrEmpty(dependenteAplicacao.getDnv())){
					dependenteServico.setNumeroDeclaracaoNascidoVivo(Long.valueOf(dependenteAplicacao.getDnv()));
				}
				propostaServico.getDependentes().add(dependenteServico);
			}
		}
		//data de ades�o
		propostaServico.setDataAdesao(new LocalDate().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		//forma de pagamento
		if(br.com.bradseg.eedi.novaemissaoexpressadi.vo.FormaPagamento.DEBITO_AUTOMATICO.getCodigo().equals(Integer.parseInt(propostaAplicacao.getPagamento().getFormaPagamento()))){
			propostaServico.setFormaPagamento(FormaPagamento.DEBITO_AUTOMATICO);
		}else if(br.com.bradseg.eedi.novaemissaoexpressadi.vo.FormaPagamento.CARTAO_CREDITO.getCodigo().equals(Integer.parseInt(propostaAplicacao.getPagamento().getFormaPagamento()))){
			propostaServico.setFormaPagamento(FormaPagamento.CARTAO_CREDITO);
		}
		
		//tipo de cobran�a
		if(propostaAplicacao.getPagamento().getTipoCobranca() != null){
			propostaServico.setTipoCobranca(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoCobranca.valueOf(TipoCobranca.obterTipoCobrancaPorCodigo(Integer.valueOf(propostaAplicacao.getPagamento().getTipoCobranca())).name()));
		}
		//data de cobran�a
		if(propostaAplicacao.getPagamento().getDataCobranca() != null){
			propostaServico.setDataInicioCobranca(propostaAplicacao.getPagamento().getDataCobranca().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		}
		//conta corrente
		propostaServico.setProponente(new ProponenteVO());
		propostaServico.getProponente().setCpf(br.com.bradseg.eedi.novaemissaoexpressadi.util.Strings.desnormalizarCPF(propostaAplicacao.getPagamento().getContaCorrente().getCpf()));
		propostaServico.getProponente().setNome(propostaAplicacao.getPagamento().getContaCorrente().getNome());
		if(propostaAplicacao.getPagamento().getContaCorrente().getDataNascimento() != null){
			propostaServico.getProponente().setDataNascimento(propostaAplicacao.getPagamento().getContaCorrente().getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		}
		propostaServico.getProponente().setContaCorrente(new ContaCorrenteVO());
		propostaServico.getProponente().getContaCorrente().setAgenciaBancaria(new AgenciaBancariaVO());
		propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().setBanco(new BancoVO());
		propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().getBanco().setCodigo(Integer.valueOf(propostaAplicacao.getPagamento().getContaCorrente().getBanco()));
		propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().setCodigo(propostaAplicacao.getPagamento().getContaCorrente().getNumeroAgencia());
		propostaServico.getProponente().getContaCorrente().setNumero(propostaAplicacao.getPagamento().getContaCorrente().getNumeroConta());
		propostaServico.getProponente().getContaCorrente().setDigitoVerificador(propostaAplicacao.getPagamento().getContaCorrente().getDigitoVerificadorConta());
		//contato
		if(!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero())){
			propostaServico.getProponente().getTelefones().add(converterTelefoneAplicacaoParaTelefoneServico(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(0)));
		}
		if(!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero())){
			propostaServico.getProponente().getTelefones().add(converterTelefoneAplicacaoParaTelefoneServico(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(1)));
		}
		if(!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(2).getDddEnumero())){
			propostaServico.getProponente().getTelefones().add(converterTelefoneAplicacaoParaTelefoneServico(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(2)));
		}
		
		return propostaServico;
	}
	
	private TelefoneVO converterTelefoneAplicacaoParaTelefoneServico(br.com.bradseg.eedi.novaemissaoexpressadi.vo.TelefoneVO telefoneAplicacao) {
		
		TelefoneVO telefoneServico = new TelefoneVO();
		
		telefoneServico.setTipo(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoTelefone.valueOf(TipoTelefone.obterTipoTelefonePorCodigo(Integer.valueOf(telefoneAplicacao.getTipoTelefone())).name()));
		telefoneServico.setDdd(telefoneAplicacao.getDdd());
		telefoneServico.setNumero(telefoneAplicacao.getNumero());

		return telefoneServico;
	}

	/**
	 * Metodo responsavel por converter a lista de proposta oriunda do servi�o para uma lista de proposta da aplica��o.
	 * 
	 * @param listaPropostaServico - lista de proposta oriundas do servi�o.
	 * 
	 * @return List<PropostaVO> - lista de proposta da aplica��o.
	 */
	public List<PropostaVO> converterListaPropostaServicoParaListaPropostaAplicacao(List<br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO> listaPropostaServico){
		List<PropostaVO> listaPropostaAplicacao = new ArrayList<PropostaVO>();
		
		PropostaVO propostaAplicacao = null;
		
		if(listaPropostaServico != null){
			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO propostaServico : listaPropostaServico) {
				propostaAplicacao = new PropostaVO();
				
				propostaAplicacao.setSequencial(propostaServico.getNumeroSequencial());
				propostaAplicacao.setCodigo(propostaServico.getCodigo());		
				if(null != propostaServico.getTitular().getNome()){
					propostaAplicacao.getBeneficiarios().getTitular().setNome(propostaServico.getTitular().getNome().toUpperCase());
				}
				if(Strings.isNullOrEmpty(propostaServico.getMovimento().getDescricao())){
					propostaAplicacao.setStatus(SituacaoProposta.valueOf(propostaServico.getMovimento().getSituacao().name()).getNome().toUpperCase());
				}else{
					propostaAplicacao.setStatus(SituacaoProposta.valueOf(propostaServico.getMovimento().getSituacao().name()).getNome().toUpperCase() + " / " + propostaServico.getMovimento().getDescricao().toUpperCase());
				}
				propostaAplicacao.getPagamento().getContaCorrente().setCpf(propostaServico.getProponente().getCpf());
				
				propostaAplicacao.getCanal().setCodigo(propostaServico.getCanalVenda().getCodigo());
				propostaAplicacao.getCanal().setNome(propostaServico.getCanalVenda().getNome());
				
				if(propostaServico.getCorretor() != null && propostaServico.getCorretor().getCpfCnpj() != null) {
					propostaAplicacao.setCorretor(new br.com.bradseg.eedi.novaemissaoexpressadi.vo.CorretorVO());
					propostaAplicacao.getCorretor().setCpfCnpj(propostaServico.getCorretor().getCpfCnpj());
				}
				
				listaPropostaAplicacao.add(propostaAplicacao);
			}
		}
		
		return listaPropostaAplicacao;
	}
	
	/**
	 * Metodo responsavel por converter o objeto proposta da aplica��o para o objeto proposta do servi�o.
	 * 
	 * @param propostaServico - informa��es da proposta do servi�o a serem convertidas.
	 * 
	 * @return PropostaVO - informa��es da proposta convertidas para o objeto da aplica��o.
	 */
	public PropostaVO converterPropostaServicoParaPropostaAplicacao(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO propostaServico){
		PropostaVO propostaAplicacao = new PropostaVO();
		
		propostaAplicacao.setSequencial(propostaServico.getNumeroSequencial());
		propostaAplicacao.setCodigo(propostaServico.getCodigo());
		if(!Strings.isNullOrEmpty(propostaServico.getDataAdesao())){
			propostaAplicacao.setDataEmissao(DateTimeFormat.forPattern(Constantes.DATA_FORMATO_DD_MM_YYYY).parseDateTime(propostaServico.getDataAdesao()).toLocalDate());	
		}
		
		//plano
		propostaAplicacao.setPlano(new br.com.bradseg.eedi.novaemissaoexpressadi.vo.PlanoVO());
		propostaAplicacao.getPlano().setCodigo(propostaServico.getPlanoVO().getCodigo());
		propostaAplicacao.getPlano().setNome(propostaServico.getPlanoVO().getNome());
		propostaAplicacao.getPlano().setDescricaoCarenciaPeriodoMensal(propostaServico.getPlanoVO().getDescricaoCarenciaPeriodoMensal());
		propostaAplicacao.getPlano().setDescricaoCarenciaPeriodoAnual(propostaServico.getPlanoVO().getDescricaoCarenciaPeriodoAnual());
		propostaAplicacao.getPlano().setValorPlanoVO(new ValorPlanoVO());
		propostaAplicacao.getPlano().getValorPlanoVO().setValorAnualDependente(propostaServico.getPlanoVO().getValorPlanoVO().getValorAnualDependente());
		propostaAplicacao.getPlano().getValorPlanoVO().setValorAnualTitular(propostaServico.getPlanoVO().getValorPlanoVO().getValorAnualTitular());
		propostaAplicacao.getPlano().getValorPlanoVO().setValorDesconto(propostaServico.getPlanoVO().getValorPlanoVO().getValorDesconto());
		propostaAplicacao.getPlano().getValorPlanoVO().setValorMensalDependente(propostaServico.getPlanoVO().getValorPlanoVO().getValorMensalDependente());
		propostaAplicacao.getPlano().getValorPlanoVO().setValorMensalTitular(propostaServico.getPlanoVO().getValorPlanoVO().getValorMensalTitular());
		propostaAplicacao.getPlano().getValorPlanoVO().setValorTaxa(propostaServico.getPlanoVO().getValorPlanoVO().getValorTaxa());
		
		if(propostaServico.getResponsavelLegal() == null){
			propostaAplicacao.getBeneficiarios().setTitularMenorIdade(false);
		}else{
			propostaAplicacao.getBeneficiarios().setTitularMenorIdade(true);
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setCpf(propostaServico.getResponsavelLegal().getCpf());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setNome(propostaServico.getResponsavelLegal().getNome());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setEmail(propostaServico.getResponsavelLegal().getEmail());			
			try {
				propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setDataNascimento(LocalDate.fromDateFields(new SimpleDateFormat(Constantes.DATA_FORMATO_DD_MM_YYYY).parse(propostaServico.getResponsavelLegal().getDataNascimento())));
			} catch (ParseException e) {
				LOGGER.error(String.format("Erro ao converter data de nascimento de responsavel titular. [%s]", e.getMessage()));
			}
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setCep(propostaServico.getResponsavelLegal().getEndereco().getCep());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setLogradouro(propostaServico.getResponsavelLegal().getEndereco().getLogradouro());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setNumero(propostaServico.getResponsavelLegal().getEndereco().getNumero());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setComplemento(propostaServico.getResponsavelLegal().getEndereco().getComplemento());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setBairro(propostaServico.getResponsavelLegal().getEndereco().getBairro());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setCidade(propostaServico.getResponsavelLegal().getEndereco().getCidade());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setEstado(propostaServico.getResponsavelLegal().getEndereco().getUnidadeFederativa().getSigla());
		
			br.com.bradseg.eedi.novaemissaoexpressadi.vo.TelefoneVO telefoneAplicacao = new br.com.bradseg.eedi.novaemissaoexpressadi.vo.TelefoneVO();
			telefoneAplicacao.setTipoTelefone(br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoTelefone.valueOf(propostaServico.getResponsavelLegal().getTelefone().getTipo().name()).getNome().toUpperCase());
			telefoneAplicacao.setDddEnumero("(" + propostaServico.getResponsavelLegal().getTelefone().getDdd() + ") " + propostaServico.getResponsavelLegal().getTelefone().getNumero());
			if(null != propostaServico.getResponsavelLegal().getTelefone().getRamal()){
				telefoneAplicacao.setRamal(propostaServico.getResponsavelLegal().getTelefone().getRamal().toString());
			}
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setTelefones(new ArrayList<br.com.bradseg.eedi.novaemissaoexpressadi.vo.TelefoneVO>());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().add(telefoneAplicacao);
		}
		
		//titular
		propostaAplicacao.getBeneficiarios().getTitular().setCpf(propostaServico.getTitular().getCpf());
		propostaAplicacao.getBeneficiarios().getTitular().setNomeMae(propostaServico.getTitular().getNomeMae());
		propostaAplicacao.getBeneficiarios().getTitular().setNome(propostaServico.getTitular().getNome());
		try {
			propostaAplicacao.getBeneficiarios().getTitular().setDataNascimento(LocalDate.fromDateFields(new SimpleDateFormat(Constantes.DATA_FORMATO_DD_MM_YYYY).parse(propostaServico.getTitular().getDataNascimento())));
		} catch (ParseException e) {
			LOGGER.error(String.format("Erro ao converter data de nascimento de titular. [%s]", e.getMessage()));
		}
		
		propostaAplicacao.getBeneficiarios().getTitular().setEmail(propostaServico.getTitular().getEmail());
		propostaAplicacao.getBeneficiarios().getTitular().setSexo(br.com.bradseg.eedi.novaemissaoexpressadi.vo.Sexo.valueOf(propostaServico.getTitular().getSexo().name()).getNome().toUpperCase());
		propostaAplicacao.getBeneficiarios().getTitular().setEstadoCivil(br.com.bradseg.eedi.novaemissaoexpressadi.vo.EstadoCivil.valueOf(propostaServico.getTitular().getEstadoCivil().name()).getNome().toUpperCase());
		
		if(null != propostaServico.getTitular().getGrauParentescoTitulaProponente()){
			propostaAplicacao.getBeneficiarios().getTitular().setGrauParentescoTitularProponente(GrauParentesco.valueOf(propostaServico.getTitular().getGrauParentescoTitulaProponente().name()).getNome().toUpperCase());

		}
		//dependenteAplicacao.setGrauParentesco(GrauParentesco.valueOf(dependenteServico.getGrauParentesco().name()).getNome().toUpperCase());
		if(null != propostaServico.getTitular().getNumeroCartaoNacionalSaude()){
			propostaAplicacao.getBeneficiarios().getTitular().setCns(propostaServico.getTitular().getNumeroCartaoNacionalSaude().toString());
		}
		if(null != propostaServico.getTitular().getNumeroDeclaracaoNascidoVivo()){
			propostaAplicacao.getBeneficiarios().getTitular().setDnv(propostaServico.getTitular().getNumeroDeclaracaoNascidoVivo().toString());
		}
		propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setCep(propostaServico.getTitular().getEndereco().getCep());
		propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setLogradouro(propostaServico.getTitular().getEndereco().getLogradouro());
		propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setNumero(propostaServico.getTitular().getEndereco().getNumero());
		propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setComplemento(propostaServico.getTitular().getEndereco().getComplemento());
		propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setBairro(propostaServico.getTitular().getEndereco().getBairro());
		propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setCidade(propostaServico.getTitular().getEndereco().getCidade());
		propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setEstado(propostaServico.getTitular().getEndereco().getUnidadeFederativa().getSigla());
		
		if(propostaServico.getProponente().getTelefones() != null){
			propostaAplicacao.getBeneficiarios().getTitular().setTelefones(new ArrayList<br.com.bradseg.eedi.novaemissaoexpressadi.vo.TelefoneVO>());
			br.com.bradseg.eedi.novaemissaoexpressadi.vo.TelefoneVO telefoneAplicacao = null;
			for (TelefoneVO telefoneServico : propostaServico.getProponente().getTelefones()) {
				telefoneAplicacao = new br.com.bradseg.eedi.novaemissaoexpressadi.vo.TelefoneVO();
				
				telefoneAplicacao.setTipoTelefone(br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoTelefone.valueOf(telefoneServico.getTipo().name()).getNome().toUpperCase());
				telefoneAplicacao.setDddEnumero("(" + telefoneServico.getDdd() + ") " + telefoneServico.getNumero());
				if(null != telefoneServico.getRamal()){
					telefoneAplicacao.setRamal(telefoneServico.getRamal().toString());
				}
				
				propostaAplicacao.getBeneficiarios().getTitular().getTelefones().add(telefoneAplicacao);
			}
		}
		
		//dependentes
		if(propostaServico.getDependentes() != null){
			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DependenteVO dependenteServico : propostaServico.getDependentes()) {
				DependenteVO dependenteAplicacao = new DependenteVO();
				
				dependenteAplicacao.setCpf(dependenteServico.getCpf());
				dependenteAplicacao.setNome(dependenteServico.getNome());
				dependenteAplicacao.setNomeMae(dependenteServico.getNomeMae());				
				try {
					dependenteAplicacao.setDataNascimento(LocalDate.fromDateFields(new SimpleDateFormat(Constantes.DATA_FORMATO_DD_MM_YYYY).parse(dependenteServico.getDataNascimento())));
				} catch (ParseException e) {
					LOGGER.error(String.format("Erro ao converter data de nascimento de titular. [%s]", e.getMessage()));
				}
				dependenteAplicacao.setSexo(br.com.bradseg.eedi.novaemissaoexpressadi.vo.Sexo.valueOf(dependenteServico.getSexo().name()).getNome().toUpperCase());
				dependenteAplicacao.setGrauParentesco(GrauParentesco.valueOf(dependenteServico.getGrauParentesco().name()).getNome().toUpperCase());
				dependenteAplicacao.setEstadoCivil(EstadoCivil.valueOf(dependenteServico.getEstadoCivil().name()).getNome().toUpperCase());
				
				if(null != dependenteServico.getGrauParentescoBeneficiario()){
					dependenteAplicacao.setGrauParentescoBeneficiario(GrauParentesco.valueOf(dependenteServico.getGrauParentescoBeneficiario().name()).getNome().toUpperCase());
				}
				
				if(null != dependenteServico.getNumeroCartaoNacionalSaude()){
					dependenteAplicacao.setCns(dependenteServico.getNumeroCartaoNacionalSaude().toString());
				}
				if(null != dependenteServico.getNumeroDeclaracaoNascidoVivo()){
					dependenteAplicacao.setDnv(dependenteServico.getNumeroDeclaracaoNascidoVivo().toString());
				}
				propostaAplicacao.getBeneficiarios().getDependentes().add(dependenteAplicacao);
			}
		}
		
		//pagamento
		propostaAplicacao.getPagamento().setTipoCobranca(TipoCobranca.valueOf(propostaServico.getTipoCobranca().name()).getNome().toUpperCase());
		propostaAplicacao.getPagamento().setFormaPagamento(String.valueOf(br.com.bradseg.eedi.novaemissaoexpressadi.vo.FormaPagamento.valueOf(propostaServico.getFormaPagamento().name()).getCodigo()));
		if(null != propostaServico.getDataInicioCobranca()){
			try {
				propostaAplicacao.getPagamento().setDataCobranca(LocalDate.fromDateFields(new SimpleDateFormat(Constantes.DATA_FORMATO_DD_MM_YYYY).parse(propostaServico.getDataInicioCobranca())));
			} catch (ParseException e) {
				LOGGER.error(String.format("Erro ao converter data de nascimento de titular. [%s]", e.getMessage()));
			}
		}
		
		if(br.com.bradseg.eedi.novaemissaoexpressadi.vo.FormaPagamento.DEBITO_AUTOMATICO.name().equalsIgnoreCase(propostaAplicacao.getPagamento().getFormaPagamento())) {
			propostaAplicacao.getPagamento().getContaCorrente().setBanco(Banco.obterPorCodigo(propostaServico.getBanco().getCodigo()).getDescricao().toUpperCase());
			propostaAplicacao.getPagamento().getContaCorrente().setCpf(propostaServico.getProponente().getCpf());
			propostaAplicacao.getPagamento().getContaCorrente().setNome(propostaServico.getProponente().getNome().toUpperCase());
			if(null != propostaServico.getDataInicioCobranca()){
				try {
					propostaAplicacao.getPagamento().getContaCorrente().setDataNascimento(LocalDate.fromDateFields(new SimpleDateFormat(Constantes.DATA_FORMATO_DD_MM_YYYY).parse(propostaServico.getProponente().getDataNascimento())));
				} catch (ParseException e) {
					LOGGER.error(String.format("Erro ao converter data de nascimento de titular. [%s]", e.getMessage()));
				}
			}
			
			propostaAplicacao.getPagamento().getContaCorrente().setNumeroAgencia(propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().getCodigo());
			propostaAplicacao.getPagamento().getContaCorrente().setNumeroConta(propostaServico.getProponente().getContaCorrente().getNumero());
			propostaAplicacao.getPagamento().getContaCorrente().setDigitoVerificadorConta(propostaServico.getProponente().getContaCorrente().getDigitoVerificador());
		}
		
		//corretor
		propostaAplicacao.getCorretor().setCpfCnpj(propostaServico.getCorretor().getCpfCnpj());
		propostaAplicacao.getCorretor().setNome(propostaServico.getCorretor().getNome());
		if(null != propostaServico.getCorretor().getCpd()){
			propostaAplicacao.getCorretor().setCpd(propostaServico.getCorretor().getCpd().longValue());
		}
		propostaAplicacao.getCorretor().setSucursal(propostaServico.getSucursalSeguradora().getCodigo());
		if(!Strings.isNullOrEmpty(propostaServico.getCodigoMatriculaAssistente())){
			propostaAplicacao.getCorretor().setAssistenteProducao(Long.valueOf(propostaServico.getCodigoMatriculaAssistente()));
		}
		
		//status
		propostaAplicacao.setStatus(SituacaoProposta.valueOf(propostaServico.getMovimento().getSituacao().name()).getNome().toUpperCase());
		
		//canal
		propostaAplicacao.getCanal().setCodigo(propostaServico.getCanalVenda().getCodigo());
		propostaAplicacao.getCanal().setNome(propostaServico.getCanalVenda().getNome());
		
		return propostaAplicacao;
	}

}
