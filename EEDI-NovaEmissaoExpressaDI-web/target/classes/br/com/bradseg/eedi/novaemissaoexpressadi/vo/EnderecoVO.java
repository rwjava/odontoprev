package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;

/**
 * Classe responsavel pelo transporte de dados de endere�o.
 *
 */
public class EnderecoVO implements Serializable {

	private static final long serialVersionUID = -7473272355694582478L;
	
	private String cep;
	private String logradouro;
	private Long numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private String estado;
	
	/**
	 * Retorna cep.
	 *
	 * @return cep - cep.
	 */
	public String getCep() {
		return cep;
	}
	/**
	 * Especifica cep.
	 *
	 * @param cep - cep.
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}
	/**
	 * Retorna logradouro.
	 *
	 * @return logradouro - logradouro.
	 */
	public String getLogradouro() {
		return logradouro;
	}
	/**
	 * Especifica logradouro.
	 *
	 * @param logradouro - logradouro.
	 */
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	/**
	 * Retorna numero.
	 *
	 * @return numero - numero.
	 */
	public Long getNumero() {
		return numero;
	}
	/**
	 * Especifica numero.
	 *
	 * @param numero - numero.
	 */
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	/**
	 * Retorna complemento.
	 *
	 * @return complemento - complemento.
	 */
	public String getComplemento() {
		return complemento;
	}
	/**
	 * Especifica complemento.
	 *
	 * @param complemento - complemento.
	 */
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	/**
	 * Retorna bairro.
	 *
	 * @return bairro - bairro.
	 */
	public String getBairro() {
		return bairro;
	}
	/**
	 * Especifica bairro.
	 *
	 * @param bairro - bairro.
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	/**
	 * Retorna cidade.
	 *
	 * @return cidade - cidade.
	 */
	public String getCidade() {
		return cidade;
	}
	/**
	 * Especifica cidade.
	 *
	 * @param cidade - cidade.
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	/**
	 * Retorna estado.
	 *
	 * @return estado - estado.
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * Especifica estado.
	 *
	 * @param estado - estado.
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
