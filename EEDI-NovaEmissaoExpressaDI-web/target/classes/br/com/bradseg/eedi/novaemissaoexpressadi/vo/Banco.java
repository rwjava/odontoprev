package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar os bancos.
 *
 */
public enum Banco {

	SANTANDER(33, "Santander", "033 - Santander"), 
	BRADESCO(237, "Bradesco", "237 - Bradesco"), 
	ITAU(341, "Ita�", "341 - Ita�");
	//HSBC(399, "HSBC", "399 - HSBC");

	private Integer codigo;
	private String nome;
	private String descricao;

	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo - codigo do banco.
	 * @param nome - nome do banco.
	 * @param descricao - descricao do banco (codigo - nome).
	 */
	private Banco(Integer codigo, String nome, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.nome = nome;
	}	

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Retorna descricao.
	 *
	 * @return descricao - descricao.
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Metodo responsavel por obter a lista de bancos de acordo com o segmento da sucursal.
	 * 
	 * @param segmento - segmento da sucursal.
	 * @return List<Banco> - lista de bancos.
	 */
	public static List<Banco> obterListaDeBancoPorSegmento(String segmento) {

		List<Banco> listaDeBancos = new ArrayList<Banco>();

		if ("REDE".equalsIgnoreCase(segmento)) {
			listaDeBancos.add(Banco.BRADESCO);
			//listaDeBancos.add(Banco.HSBC);
		} else if ("MERCADO".equalsIgnoreCase(segmento) || "CORPORATE".equalsIgnoreCase(segmento)) {
			listaDeBancos.add(Banco.BRADESCO);
			listaDeBancos.add(Banco.SANTANDER);
			listaDeBancos.add(Banco.ITAU);
		}

		return listaDeBancos;
	}

	/**
	 * Metodo responsavel por listar os bancos.
	 * 
	 * @return List<Banco> - lista de banco.
	 */
	public static List<Banco> listar(){
		List<Banco> lista = new ArrayList<Banco>();
		for (Banco banco : Banco.values()) {
			lista.add(banco);
		}
		return lista;
	}
	
	public static Banco obterPorCodigo(int codigo){
		for (Banco banco : Banco.values()) {
			if(codigo == banco.getCodigo().intValue()){
				return banco;
			}
		}
		return null;
	}
}
