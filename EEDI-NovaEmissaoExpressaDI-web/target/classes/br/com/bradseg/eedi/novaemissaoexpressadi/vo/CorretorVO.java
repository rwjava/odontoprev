package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;

/**
 * Classe responsavel pelo transporte de dados do corretor.
 *
 */
public class CorretorVO implements Serializable {

	private static final long serialVersionUID = 1817789260056445482L;
	
	private String nome;
	private String cpfCnpj;
	private Integer sucursal;
	private Long cpd;
	private Long agenciaProdutora;
	private Long assistenteProducao;
	private ProdutorVO produtor;
	
	/**
	 * Construtor padr�o.
	 */
	public CorretorVO(){
		produtor = new ProdutorVO();
	}
	
	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * Retorna cpfCnpj.
	 *
	 * @return cpfCnpj - cpfCnpj.
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	/**
	 * Especifica cpfCnpj.
	 *
	 * @param cpfCnpj - cpfCnpj.
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	/**
	 * Retorna sucursal.
	 *
	 * @return sucursal - sucursal.
	 */
	public Integer getSucursal() {
		return sucursal;
	}
	/**
	 * Especifica sucursal.
	 *
	 * @param sucursal - sucursal.
	 */
	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}
	/**
	 * Retorna cpd.
	 *
	 * @return cpd - cpd.
	 */
	public Long getCpd() {
		return cpd;
	}
	/**
	 * Especifica cpd.
	 *
	 * @param cpd - cpd.
	 */
	public void setCpd(Long cpd) {
		this.cpd = cpd;
	}
	/**
	 * Retorna agenciaProdutora.
	 *
	 * @return agenciaProdutora - agenciaProdutora.
	 */
	public Long getAgenciaProdutora() {
		return agenciaProdutora;
	}
	/**
	 * Especifica agenciaProdutora.
	 *
	 * @param agenciaProdutora - agenciaProdutora.
	 */
	public void setAgenciaProdutora(Long agenciaProdutora) {
		this.agenciaProdutora = agenciaProdutora;
	}
	/**
	 * Retorna assistenteProducao.
	 *
	 * @return assistenteProducao - assistenteProducao.
	 */
	public Long getAssistenteProducao() {
		return assistenteProducao;
	}
	/**
	 * Especifica assistenteProducao.
	 *
	 * @param assistenteProducao - assistenteProducao.
	 */
	public void setAssistenteProducao(Long assistenteProducao) {
		this.assistenteProducao = assistenteProducao;
	}

	/**
	 * Retorna produtor.
	 *
	 * @return produtor - produtor
	 */
	public ProdutorVO getProdutor() {
		return produtor;
	}

	/**
	 * Especifica produtor.
	 *
	 * @param produtor - produtor
	 */
	public void setProdutor(ProdutorVO produtor) {
		this.produtor = produtor;
	}

}
