package br.com.bradseg.eedi.novaemissaoexpressadi.validador;

import org.joda.time.LocalDate;
import org.joda.time.Period;

import br.com.bradseg.eedi.novaemissaoexpressadi.util.Constantes;

/**
 * Verifica se a data de nascimento � v�lida de acordo com as regras definidas
 */
public class ValidadorDataNascimento {

	/**
	 * Verifica se a data de nascimento � v�lida. Para ser v�lido, a data de nascimento precisa seguir as seguintes regras:
	 * 	- Ser no m�ximo no dia atual;
	 * 	- Possuir no m�nimo 150 anos a partir de hoje;
	 * @param cpfProponente 
	 * @param cpfTitular 
	 * @return verdadeiro se a data for v�lida
	 */
	public static boolean cpfProprio(String cpfProponente, String cpfTitular){
		if(cpfProponente.equals(cpfTitular)){
			return true;
		}else{
			return false;
		}
	}
	public static boolean validarDataMaxima(LocalDate dataNascimento) {
		LocalDate dataAtual = new LocalDate();
		return dataNascimento.isBefore(dataAtual.plusDays(1)) && dataNascimento.isAfter(dataAtual.minusYears(Constantes.ANO_MINIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL));
	}
	
	public static boolean validarSeMenorDeIdade(LocalDate dataNascimento){
		return (new Period(dataNascimento, new LocalDate()).getYears() < 18);
	}
	public static boolean validaDataNascimentoRespFinanceiroFilho(LocalDate dataNascimentoRespFinanceiro , LocalDate dataNascimentoTitular){
		LocalDate dataAtual = new LocalDate();
		int anoNascimentoTitular = dataNascimentoTitular.getYear();
		int anoNascimentoRespFinanceiro = dataNascimentoRespFinanceiro.getYear(); 
		int anoAtual = dataAtual.getYear();
		int idadeAtualRespFinanceiro = anoAtual - anoNascimentoRespFinanceiro;
		int idadeAtualTitular = anoAtual - anoNascimentoTitular;
		
		if(idadeAtualTitular - idadeAtualRespFinanceiro  > Constantes.IDADE_PERMITADA_DIFERENCA_RESPONSAVEL_FINANCEIRO){
			return true;
		}
		
		return false;
	}
	public static boolean validaDataNascimentoRespFinanceiro(LocalDate dataNascimentoRespFinanceiro , LocalDate dataNascimentoTitular){
		LocalDate dataAtual = new LocalDate();
		int anoNascimentoTitular = dataNascimentoTitular.getYear();
		int anoNascimentoRespFinanceiro = dataNascimentoRespFinanceiro.getYear(); 
		int anoAtual = dataAtual.getYear();
		int idadeAtualRespFinanceiro = anoAtual - anoNascimentoRespFinanceiro;
		int idadeAtualTitular = anoAtual - anoNascimentoTitular;
		
		
		if(idadeAtualTitular - idadeAtualRespFinanceiro > Constantes.IDADE_PERMITADA_DIFERENCA_RESPONSAVEL_FINANCEIRO){
			return true;
		}
		
		return false;
		
	}

	
	
}