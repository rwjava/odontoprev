package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar os tipos de telefone.
 *
 */
public enum TipoTelefone {
	
	RESIDENCIAL(1, "Residencial"), COMERCIAL(2, "Comercial"), CELULAR(3, "Celular");
	
	private int codigo;
	private String nome;
	
	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo - codigo.
	 * @param nome - nome.
	 */
	private TipoTelefone(int codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Metodo responsavel por listar os tipos de telefone.
	 * 
	 * @return List<TipoTelefone> - lista de tipo de telefone.
	 */
	public static List<TipoTelefone> listar(){
		List<TipoTelefone> lista = new ArrayList<TipoTelefone>();
		for (TipoTelefone tipoTelefone : TipoTelefone.values()) {
			lista.add(tipoTelefone);
		}
		return lista;
	}
	
	public static TipoTelefone obterTipoTelefonePorCodigo(int codigo){
		for (TipoTelefone tipoTelefone : TipoTelefone.values()) {
			if(codigo == tipoTelefone.getCodigo()){
				return tipoTelefone;
			}
		}
		return TipoTelefone.CELULAR;
	}
	
}
