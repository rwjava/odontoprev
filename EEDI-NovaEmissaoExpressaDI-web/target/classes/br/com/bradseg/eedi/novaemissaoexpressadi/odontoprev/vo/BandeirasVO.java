package br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.vo;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BandeirasVO extends RetornoVO implements Serializable {

	private static final long serialVersionUID = 8085719360060767555L;

	private List<String> bandeiras;

	public BandeirasVO() {
		bandeiras = new ArrayList<String>();
	}

	/**
	 * Retorna bandeiras.
	 *
	 * @return bandeiras - bandeiras
	 */
	public List<String> getBandeiras() {
		return bandeiras;
	}

	/**
	 * Especifica bandeiras.
	 *
	 * @param bandeiras - bandeiras
	 */
	public void setBandeiras(List<String> bandeiras) {
		this.bandeiras = bandeiras;
	}

}
