package br.com.bradseg.eedi.novaemissaoexpressadi.validador;

import org.slf4j.Logger;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.bsad.framework.core.validator.Validators;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.ConstantesMensagemErro;

import com.google.common.collect.Lists;

/**
 * Classe utilizada para auxiliar na valida��o de regras. 
 * 
 */
public class Validador {

	private static final Pattern PADRAO_CARACTERES_NAO_PERMITIDOS_NOME = Pattern.compile("[^'\\-\\.\\s\\pL\\pM\\p{Nl}[\\p{InEnclosedAlphanumerics}&&\\p{So}]]");
	private static final Pattern PADRAO_EMAIL_VALIDO = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	//private static final Pattern PADRAO_DDD_VALIDO = Pattern.compile("(10)|([1-9][1-9])");
	//private static final Pattern PADRAO_NUMERO_TELEFONE_VALIDO = Pattern.compile("[2-9][0-9]{7,8}");

	private List<Message> mensagens = Lists.newArrayList();

	/**
	 * Adiciona uma mensagem de erro na lista de mensagens
	 * 
	 * @param messageKey Chave da mensagem
	 * @param params Par�metros da mensagem
	 */
	public void adicionarErro(String messageKey, Object... params) {
		mensagens.add(new Message(messageKey, Message.ERROR_TYPE, params));
	}

	/**
	 * Indica se existe algum erro na valida��o
	 * @return true se existir erro na valida��o
	 */
	public boolean hasError() {
		return !mensagens.isEmpty();
	}

	/**
	 * Retorna as mensagens de erro
	 * 
	 * @return Mensagens de erro
	 */
	public List<Message> getMessages() {
		return mensagens;
	}

	/**
	 * Valida se uma determinada string foi informada.
	 * 
	 * @param valor Valor String
	 * @param rotulo R�tulo do campo
	 * @return true se a string informada passar na regra de obrigatoriedade
	 */
	public boolean obrigatorio(String valor, String rotulo) {
		if (StringUtils.isBlank(valor) || "0".equals(valor)) {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CAMPO_OBRIGATORIO, rotulo);
		} else {
			return true;
		}
		return false;
	}

	/**
	 * Valida se um determinado objeto foi informado.
	 * 
	 * @param objeto Objeto
	 * @param rotulo R�tulo do campo
	 * @return true se o objeto informado passar na regra de obrigatoriedade
	 */
	public boolean obrigatorio(Object objeto, String rotulo) {
		if (objeto == null || objeto.equals(0)) {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CAMPO_OBRIGATORIO, rotulo);
		} else {
			return true;
		}
		return false;
	}

	/**
	 * Valida se um cpf foi preenchido e se est� de acordo com as regras de cpf v�lido (11 caracteres, d�gito verificador v�lido).
	 * 
	 * @param cpf Valor do cpf
	 * @param rotulo R�tulo do campo
	 * @return true se o cpf passar nas regras
	 */
	public boolean cpf(String cpf, String rotulo) {
		if (obrigatorio(cpf, rotulo)) {
			if (!Validators.value(cpf).isCpf().validate()) {
				adicionarErro(ConstantesMensagemErro.MSG_ERRO_CPF_INVALIDO, rotulo);
			} else {
				return true;
			}
		}
		return false;
	}
	
	public boolean grauParentescoequals(String grauParentescoTitular, String grauParentescoProponente, String rotulo){
		if (grauParentescoTitular.equals(grauParentescoProponente)) {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_GRAUPARENTESCO_IGUAIS, rotulo);
		} else {
			return true;
		}
	return true;
}
	public boolean grauParentescotrueequals(String grauParentescoTitular, String grauParentescoProponente, String rotulo){
		if (grauParentescoTitular.equals(grauParentescoProponente)) {
			return true;
		} else {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CPF_INVALIDO, rotulo);
		}
	return true;
}

/*	public boolean cnpj(String cnpj, String rotulo) {
		if (obrigatorio(cnpj, rotulo)) {
			if (!Validators.value(cnpj).isCnpj().validate()) {
				adicionarErro("msg.erro.cnpj.invalido", rotulo);
			} else {
				return true;
			}
		}
		return false;
	}
	
	public boolean cpfCnpjCorpo(String cpfCnpj, String rotulo) {
		if (obrigatorio(cpfCnpj, rotulo)) {
			cpfCnpj = Strings.removerCaracteresNaoNumericos(cpfCnpj);
			if(cpfCnpj.length() == 8 || cpfCnpj.length() == 11 || cpfCnpj.length() == 14){
				return true;
			}
			return false;
		}
		return false;
	}

	public boolean cpfCnpj(String cpfCnpj, String rotulo) {
		if (obrigatorio(cpfCnpj, rotulo)) {
			cpfCnpj = Strings.removerCaracteresNaoNumericos(cpfCnpj);
			if (Validators.value(cpfCnpj).isCpf().validate()) {
				return cpf(cpfCnpj, rotulo);
			} else {
				return cnpj(cpfCnpj, rotulo);
			}
		}
		return false;
	}*/

	/**
	 * Valida as regras de um nome completo.
	 * <ul>
	 * 	<li>Deve ser preenchido</li>
	 * 	<li>N�o pode possuir caracteres especiais. Por exemplo: @, #, (, %, etc.</li>
	 * 	<li>Precisa ser nome completo (mais de 2 palavras)</li>
	 * 	<li>O primeiro nome precisa ter mais de 2 letras</li>
	 * 	<li>O �ltimo nome precisa ter mais de 2 letras</li>
	 * </ul>
	 * 
	 * @param nome Nome
	 * @param rotulo R�tulo do campo
	 * @return true se o nome completo passar nas regras
	 */
	public boolean nomeCompleto(String nome, String rotulo) {
		if (nome(nome, rotulo)) {
			// Remove os v�rios espa�os em branco transformando em apenas um (ex.: "Teste     da Silva" -> Teste da Silva)
			nome = nome.replaceAll("\\t", " ").replaceAll("\\s+", " ").trim();
			String[] nomes = nome.split("\\s");
			if (nomes.length < 2) {
				adicionarErro(ConstantesMensagemErro.MSG_ERRO_NOME_INCOMLETO, rotulo);
			} else {
				// Nao pode abreviar nenhuma parte do nome
				for (int i = 0; i < nomes.length; i++) {
					//Se for um nome com somente "e", considera como v�lido
					if (nomes[i].length() < 2 && !nomes[i].equalsIgnoreCase("E")) {
						adicionarErro(ConstantesMensagemErro.MSG_ERRO_NOME_INCOMLETO, rotulo);
						return false;
					} else if(!nomes[i].matches("[a-zA-Z��������������������������]*$")){
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Valida as regras de um nome (n�o precisa ser completo). Ex.: Jos�
	 * <ul>
	 * 	<li>Deve ser preenchido</li>
	 * 	<li>N�o pode possuir caracteres especiais. Por exemplo: @, #, (, %, etc.</li>
	 * 	<li>O primeiro nome precisa ter mais de 2 letras</li>
	 * 	<li>O �ltimo nome precisa ter mais de 2 letras</li>
	 * </ul>
	 * @param nome Nome
	 * @param rotulo R�tulo do campo
	 * @return true se o nome passar nas regras
	 */
	public boolean nome(String nome, String rotulo) {
		if (possuiCaracteresValidos(nome, rotulo)) {
			return true;
		}
		return false;
	}

	/**
	 * Valida se o nome possui apenas caracters v�lidos.
	 * 
	 * @param nome Nome
	 * @param rotulo R�tulo do campo
	 * @return true se o nome n�o possuir caracteres especiais
	 */
	public boolean possuiCaracteresValidos(String nome, String rotulo) {
		if (obrigatorio(nome, rotulo)) {
			if (PADRAO_CARACTERES_NAO_PERMITIDOS_NOME.matcher(nome).find()) {
				adicionarErro(ConstantesMensagemErro.MSG_ERRO_NOME_CARACTER_INVALIDO, rotulo);
			} else {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifica se o email informado � v�lido e adiciona mensagem de erro caso contr�rio.
	 * Considera v�lido o seguinte padr�o:
	 * 	xxxxx@xxx.com
	 * 	xxxxx@xxx.com.br
	 * 
	 * @param email Email
	 * @param rotulo R�tulo do campo
	 * @return true se o email for v�lido
	 */
	public boolean email(String email, String rotulo) {
		if(StringUtils.isBlank(email)){
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CAMPO_OBRIGATORIO, rotulo);
		}
		else if (StringUtils.isNotBlank(email) && PADRAO_EMAIL_VALIDO.matcher(email).matches()) {
			return true;
		}else{
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_EMAIL_INVALIDO, rotulo);
		}
		return false;
	}

	/**
	 * Valida se uma determinada express�o � verdadeira.
	 * 
	 * @param expressao Express�o a ser validada
	 * @param messageKey Chave da mensagem
	 * @param params Par�metros da mensagem
	 * @return true se a express�o for verdadeira
	 */
	public boolean verdadeiro(boolean expressao, String messageKey, Object... params) {
		if (expressao) {
			return true;
		}
		adicionarErro(messageKey, params);
		return false;
	}
	

	/**
	 * Valida se uma determinada express�o � falsa.
	 * 
	 * @param expressao Express�o a ser validada
	 * @param messageKey Chave da mensagem
	 * @param params Par�metros da mensagem
	 * @return true se a express�o for falsa
	 */
	public boolean falso(boolean expressao, String messageKey, Object... params) {
		return verdadeiro(!expressao, messageKey, params);
	}
	
	public void validaDataValidadeCartao(String rotulo) {
		adicionarErro("msg.erro.proposta.beneficiario.cartao.validade.invalido", rotulo);
	}

}