package br.com.bradseg.eedi.novaemissaoexpressadi.exception;

import java.util.ArrayList;
import java.util.List;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.message.Message;

/**
 * Classe de exce��o do EEDI para a captura de exce�oes oriundas de servi�os.
 *
 */
public class EEDIBusinessException extends BusinessException {

	private static final long serialVersionUID = 1556128184268873763L;

	private List<Message> lista;
	
	/**
	 * Construtor alternativo.
	 * 
	 * @param messages - lista de mensagens.
	 */
	public EEDIBusinessException(List<Message> messages) {
		super();
        this.setLista(messages);
	}
	
	/**
	 * Construtor alterantivo.
	 * 
	 * @param message - mensagem da exce��o.
	 * @param listException - exce��o oriunda do servi�o.
	 */
	public EEDIBusinessException(String message, List<br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Message> listException) {
		super();
		
		List<Message> lista = new ArrayList<Message>();
		if(listException != null){
			for(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Message elementoLista : listException){
				Message mensagem = new Message();
				mensagem.setKey(elementoLista.getKey());
				mensagem.setMessage(elementoLista.getMessage());
				lista.add(mensagem);			
			}
		}
		this.setLista(lista);
	}
	
	/**
	 * Construtor alterantivo.
	 * 
	 * @param message - mensagem da exce��o.
	 * @param exception - exce��o oriunda do servi�o.
	 */
	public EEDIBusinessException(String message, br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.BusinessException exception) {
		super(message);
		
		List<Message> lista = new ArrayList<Message>();
		if(exception.getMessages() != null){
			for(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Message elementoLista : exception.getMessages()){
				Message mensagem = new Message();
				mensagem.setKey(elementoLista.getKey());
				mensagem.setMessage(elementoLista.getMessage());
				lista.add(mensagem);			
			}
		}
		this.setLista(lista);
	}

	/**
	 * Retorna lista.
	 *
	 * @return lista - lista
	 */
	public List<Message> getLista() {
		return lista;
	}

	/**
	 * Especifica lista.
	 *
	 * @param lista - lista
	 */
	public void setLista(List<Message> lista) {
		this.lista = lista;
	}

}
