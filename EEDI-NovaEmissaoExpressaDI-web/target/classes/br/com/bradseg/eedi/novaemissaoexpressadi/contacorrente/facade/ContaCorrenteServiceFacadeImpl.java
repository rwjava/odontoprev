package br.com.bradseg.eedi.novaemissaoexpressadi.contacorrente.facade;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bucb.servicos.model.service.ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO;
import br.com.bradseg.bucb.servicos.model.service.ContaCorrenteSOAPImpl;
import br.com.bradseg.bucb.servicos.model.service.WebServiceBusinessException;
import br.com.bradseg.bucb.servicos.model.service.WebServiceIntegrationException;
import br.com.bradseg.bucb.servicos.model.vo.ContaCorrentePorCPFCNPJVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.ContaCorrenteVO;

import com.google.common.base.Strings;

/**
 * Classe de neg�cio responsavel por disponibilizar os metodos referentes a conta corrente.
 *
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class ContaCorrenteServiceFacadeImpl implements ContaCorrenteServiceFacade {

	@Autowired
	private ContaCorrenteSOAPImpl contaCorrenteWebService;
	
	/**
	 * Metodo responsavel por listar as contas corrente por cpf dos clientes bradesco.
	 * 
	 * @param cpf - cpf a ser consultado
	 * @return List<ContaCorrenteVO> - lista com as contas corrente.
	 */
	public List<ContaCorrenteVO> listarContaCorrentePorCPF(String cpf) {
		
		try {
			return converterObjetoServicoParaObjetoAplicacao(contaCorrenteWebService.listarCorrentistasPorCpfCnpj(1, Long.valueOf(cpf)));
		} catch (NumberFormatException e) {
			throw new BusinessException(e.getMessage());
		} catch (WebServiceIntegrationException e) {
			throw new IntegrationException(e.getMessage());
		} catch (WebServiceBusinessException e) {
			throw new BusinessException(e.getFaultInfo().getMessage());
		}
	}
	
	private List<ContaCorrenteVO> converterObjetoServicoParaObjetoAplicacao(ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO contasCorrenteServico){
		
		List<ContaCorrenteVO> contasCorrenteAplicacao = null;
		
		if(contasCorrenteServico != null && contasCorrenteServico.getContaCorrentePorCPFCNPJVO() != null && !contasCorrenteServico.getContaCorrentePorCPFCNPJVO().isEmpty()){
			contasCorrenteAplicacao = new ArrayList<ContaCorrenteVO>();
			ContaCorrenteVO contaCorrenteAplicacao = null;
			for (ContaCorrentePorCPFCNPJVO contaCorrenteServico : contasCorrenteServico.getContaCorrentePorCPFCNPJVO()) {
				if(contaCorrenteServico.getAgencia().longValue() != 0){
					contaCorrenteAplicacao = new ContaCorrenteVO();
					
					contaCorrenteAplicacao.setNumeroAgencia(contaCorrenteServico.getAgencia().intValue());
					contaCorrenteAplicacao.setNumeroConta(contaCorrenteServico.getConta());
					contaCorrenteAplicacao.setDigitoVerificadorConta(contaCorrenteServico.getDvConta());
					contaCorrenteAplicacao.setNome(contaCorrenteServico.getNome());
					if(!Strings.isNullOrEmpty(contaCorrenteServico.getDtNascimento())){
						contaCorrenteAplicacao.setDataNascimento(DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(contaCorrenteServico.getDtNascimento()).toLocalDate());	
					}
					
					contasCorrenteAplicacao.add(contaCorrenteAplicacao);
				}else{
					break;
				}
			}
		}
		
		return contasCorrenteAplicacao;
	}

}
