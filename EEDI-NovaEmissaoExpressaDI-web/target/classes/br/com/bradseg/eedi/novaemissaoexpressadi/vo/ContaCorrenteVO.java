package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

/**
 * Classe responsavel pelo transporte de dados da conta corrente.
 *
 */
public class ContaCorrenteVO implements Serializable {

	private static final long serialVersionUID = 7047327400082288350L;
	
	private String banco;
	private Integer numeroAgencia;
	private Long numeroConta;
	private String digitoVerificadorConta;
	private String cpf;
	private String nome;
	private LocalDate dataNascimento;
	private String dataNascimentoStr;
	
	/**
	 * Retorna banco.
	 *
	 * @return banco - banco.
	 */
	public String getBanco() {
		return banco;
	}
	/**
	 * Especifica banco.
	 *
	 * @param banco - banco.
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}
	/**
	 * Retorna numeroAgencia.
	 *
	 * @return numeroAgencia - numeroAgencia.
	 */
	public Integer getNumeroAgencia() {
		return numeroAgencia;
	}
	/**
	 * Especifica numeroAgencia.
	 *
	 * @param numeroAgencia - numeroAgencia.
	 */
	public void setNumeroAgencia(Integer numeroAgencia) {
		this.numeroAgencia = numeroAgencia;
	}
	/**
	 * Retorna numeroConta.
	 *
	 * @return numeroConta - numeroConta.
	 */
	public Long getNumeroConta() {
		return numeroConta;
	}
	/**
	 * Especifica numeroConta.
	 *
	 * @param numeroConta - numeroConta.
	 */
	public void setNumeroConta(Long numeroConta) {
		this.numeroConta = numeroConta;
	}
	/**
	 * Retorna digitoVerificadorConta.
	 *
	 * @return digitoVerificadorConta - digitoVerificadorConta.
	 */
	public String getDigitoVerificadorConta() {
		return digitoVerificadorConta;
	}
	/**
	 * Especifica digitoVerificadorConta.
	 *
	 * @param digitoVerificadorConta - digitoVerificadorConta.
	 */
	public void setDigitoVerificadorConta(String digitoVerificadorConta) {
		this.digitoVerificadorConta = digitoVerificadorConta;
	}
	/**
	 * Retorna cpf.
	 *
	 * @return cpf - cpf.
	 */
	public String getCpf() {
		return cpf;
	}
	/**
	 * Especifica cpf.
	 *
	 * @param cpf - cpf.
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * Retorna dataNascimento.
	 *
	 * @return dataNascimento - dataNascimento.
	 */
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	/**
	 * Especifica dataNascimento.
	 *
	 * @param dataNascimento - dataNascimento.
	 */
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	/**
	 * Retorna a conta corrente formatada.
	 * 
	 * @return String - conta corrente formatada no padr�o: Agencia / Conta - DV.
	 */
	public String getContaCorrenteFormatada(){
		StringBuilder contaCorrenteFormatada = new StringBuilder();
		
		contaCorrenteFormatada.append(this.getNumeroAgencia())
							  .append("/")
							  .append(this.getNumeroConta())
							  .append("-")
							  .append(this.digitoVerificadorConta);
		
		return contaCorrenteFormatada.toString();
	}
	public String getDataNascimentoStr() {
		return dataNascimentoStr;
	}
	public void setDataNascimentoStr(String dataNascimentoStr) {
		this.dataNascimentoStr = dataNascimentoStr;
	}
}
