package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar as formas de pagamento.
 *
 */
public enum FormaPagamento {

	BOLETO_BANCARIO(1, "Boleto Banc�rio"), 
	BOLETO_DEMAIS_DEBITO(2, "1� parcela em boleto e demais via d�bito em conta corrente"), 
	DEBITO_AUTOMATICO(3, "D�bito em conta corrente"),
	DEBITO_DEMAIS_BOLETO(4, "1�. parcela via d�bito em conta corrente e demais em boleto"),
	CARTAO_CREDITO(5, "Cart�o de Cr�dito");
	
	private Integer codigo;
	private String nome;
	
	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo - codigo.
	 * @param nome - nome.
	 */
	private FormaPagamento(Integer codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Busca por codigo.
	 * 
	 * @param codigo - codigo da Forma de Pagamento
	 * @return a forma de pagamento
	 */
	public static FormaPagamento buscaPor(Integer codigo) {
		for (FormaPagamento forma : FormaPagamento.values()) {
			if (forma.getCodigo().equals(codigo)) {
				return forma;
			}
		}
		return null;
	}

	public static List<FormaPagamento> listarFormaPagamento() {
		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();
		lista = obterListaFormaPagamento();

		return lista;
	}
	
	public static List<FormaPagamento> obterListaFormaPagamento() {
		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();
		lista.add(FormaPagamento.DEBITO_AUTOMATICO);
		lista.add(FormaPagamento.CARTAO_CREDITO);

		return lista;
	}
	
	public static List<LabelValueVO> obterLista() {
		List<LabelValueVO> lista = new ArrayList<LabelValueVO>();
		for (FormaPagamento forma : FormaPagamento.values()) {
			lista.add(new LabelValueVO(String.valueOf(forma.getCodigo()), forma.getNome()));
		}
		return lista;
	}

	public static List<LabelValueVO> converterEnumEmLabel(List<FormaPagamento> listaFormaPagamento) {
		List<LabelValueVO> lista = new ArrayList<LabelValueVO>();
		for (FormaPagamento forma : listaFormaPagamento) {
			lista.add(new LabelValueVO(String.valueOf(forma.getCodigo()), forma.getNome()));
		}

		return lista;
	}
}
