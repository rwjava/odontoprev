package br.com.bradseg.eedi.novaemissaoexpressadi.proposta.facade;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.BusinessException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.IntegrationException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaWebService;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoSucursalSeguradora;
import br.com.bradseg.eedi.novaemissaoexpressadi.exception.EEDIBusinessException;
import br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.facade.OdontoprevServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.vo.RetornoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.plano.facade.PlanoServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.proposta.converter.PropostaConverter;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.Constantes;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.ConstantesMensagemErro;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.Strings;
import br.com.bradseg.eedi.novaemissaoexpressadi.validador.ListaPropostaValidator;
import br.com.bradseg.eedi.novaemissaoexpressadi.validador.NovaPropostaValidator;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.CorretorVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.DependenteVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PropostaVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.SituacaoProposta;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoBusca;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoCPF;

/**
 * Classe de neg�cio responsavel por disponibilizar os metodos referentes a proposta.
 *
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class PropostaServiceFacadeImpl implements PropostaServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaServiceFacadeImpl.class);
	
	@Autowired
	private NovaPropostaValidator validadorNovaProposta;
	
	@Autowired
	private ListaPropostaValidator validadorListaProposta;
	
	@Autowired
	private PropostaConverter converterProposta;
	
	@Autowired
	private PropostaWebService propostaWebService;
	
	@Autowired
	private transient PlanoServiceFacade planoServiceFacade;
	
	@Autowired
	private OdontoprevServiceFacade odontoprevServiceFacade;
	
	private static final int RETONO_MIP_SUCCESSO = 1;
	
	/**
	 * Metodo responsavel por finalizar uma proposta.
	 * 
	 * - Validar os dados da proposta conforme as regras de neg�cio;
	 * - Converter o objeto (proposta) aplica��o para o objeto (proposta) servi�o; 
	 * - Chamar o servi�o para finalizar uma proposta;
	 * 
	 * @param proposta - informa��es da proposta a serem gravadas.
	 * 
	 * @return Long - sequencial da proposta gerado.
	 */
	public Long finalizarProposta(PropostaVO proposta){

		validadorNovaProposta.validate(proposta);
		
		try {
			return propostaWebService.finalizar(converterProposta.converterPropostaAplicacaoParaPropostaServico(proposta));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao finalizar uma proposta atrav�s do canal CORRETORA CALL CENTER PROPRIO.");
			throw new EEDIBusinessException("", e.getFaultInfo());			
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao finalizar uma proposta atrav�s do canal CORRETORA CALL CENTER PROPRIO.");
			throw new IntegrationException(e.getMessage());
		}
	}	
	
	/**
	 * Metodo responsavel por adicionar um dependente na proposta.
	 * 
	 * @param proposta - informa��es da proposta.
	 */
	public void adicionarDependente(PropostaVO proposta){
		if(proposta.getBeneficiarios().getDependentes().size() < Constantes.NUMERO_MAXIMO_DEPENDENTES){
			proposta.getBeneficiarios().getDependentes().add(new DependenteVO());
		}else{
			throw new BusinessException(new Message(ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DEPENDENTES_NUMERO_MAXIMO));
		}
	}
	
	/**
	 * Metodo responsavel por consultar as propostas de acordo com o filtro informado.
	 * 
	 * @param filtro - informa��es a serem levadas em considera��o no momento da consulta.
	 * @param corretor - informa��es do corretor.
	 * 
	 * @return List<PropostaVO> - lista com as propostas encontradas.
	 */
	public List<PropostaVO> consultarPropostaPorFiltro(FiltroPropostaVO filtro, CorretorVO corretor){
		
		LoginVO login = new LoginVO();
		login.setId(corretor.getProdutor().getCpfCnpj());
		
		validadorListaProposta.validate(filtro);
		
		try {
			if(TipoBusca.CPF.getCodigo().equals(filtro.getTipoBusca()) && TipoCPF.BENEFICIARIO.getCodigo().equals(filtro.getTipoCPF())){
				List<PropostaVO> propostas;
				propostas = converterProposta.converterListaPropostaServicoParaListaPropostaAplicacao(propostaWebService.listarPropostasPorBeneficiario(Strings.desnormalizarCPF(filtro.getCpf()), Constantes.CODIGO_CANAL_CORRETORA_CALL_CENTER));
				for(PropostaVO proposta : propostas) {
					if(proposta.getCorretor().getCpfCnpj().equals(login.getId())) {
						proposta.setIndicadorCorretorLogado(true);
					}
				}
				return propostas;
			}else{
				br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FiltroPropostaVO filtroServico = new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FiltroPropostaVO();
				if(TipoBusca.PROPOSTA.getCodigo().equals(filtro.getTipoBusca())){
					filtroServico.setCodigoProposta(filtro.getCodigoProposta().toUpperCase());
				}
				else if(TipoBusca.PERIODO.getCodigo().equals(filtro.getTipoBusca())){
					filtroServico.setDataInicio(filtro.getFiltroPeriodoVO().getDataInicio().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
					filtroServico.setDataFim(filtro.getFiltroPeriodoVO().getDataFim().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
					filtroServico.setCanalVenda(new CanalVendaVO());
					filtroServico.getCanalVenda().setCodigo(Constantes.CODIGO_CANAL_CORRETORA_CALL_CENTER);
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.valueOf(SituacaoProposta.obterSituacaoPropostaPorCodigo(filtro.getFiltroPeriodoVO().getStatus()).name()));
					filtroServico.setCorretor(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CorretorVO());
					if(null != corretor.getCpd() && corretor.getCpd() != 0){
						filtroServico.getCorretor().setCpd(corretor.getCpd().intValue());
					}
					filtroServico.getCorretor().setSucursalSeguradora(new SucursalSeguradoraVO());
					if(null != corretor.getSucursal() && corretor.getSucursal() != 0){
						filtroServico.getCorretor().getSucursalSeguradora().setCodigo(corretor.getSucursal());
						filtroServico.getCorretor().getSucursalSeguradora().setTipo(TipoSucursalSeguradora.EMISSORA);
					}
				}
				else if(TipoBusca.CPF.getCodigo().equals(filtro.getTipoBusca()) && TipoCPF.PROPONENTE.getCodigo().equals(filtro.getTipoCPF())){
					filtroServico.setCanalVenda(new CanalVendaVO());
					filtroServico.getCanalVenda().setCodigo(Constantes.CODIGO_CANAL_CORRETORA_CALL_CENTER);
					filtroServico.setCpfProponente(Strings.desnormalizarCPF(filtro.getCpf()));
				}
				
				List<PropostaVO> propostas;
				propostas = converterProposta.converterListaPropostaServicoParaListaPropostaAplicacao(propostaWebService.listarPorFiltro(filtroServico, login));

				for(PropostaVO proposta : propostas) {
					if(proposta.getCorretor().getCpfCnpj().equals(login.getId())) {
						proposta.setIndicadorCorretorLogado(true);
					}
				}
				return propostas;				
			}
		} catch (BusinessException_Exception e) {
			LOGGER.error("consultarPropostaPorFiltro - Erro de neg�cio ao consultar propostas atrav�s do canal CORRETORA CALL CENTER PROPRIO.");
			throw new EEDIBusinessException("", e.getFaultInfo());	
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao consultar propostas atrav�s do canal CORRETORA CALL CENTER PROPRIO.");
			throw new IntegrationException(e.getMessage());
		}
	}
	
	/**
	 * Metodo responsavel por obter uma proposta atraves do sequencial da proposta.
	 * 
	 * @param sequencial - sequencial da proposta.
	 * 
	 * @return PropostaVO - informa��es da proposta encontrada.
	 * @throws BusinessException_Exception 
	 */
	public PropostaVO obterPropostaPorSequencial(Long sequencial){
		
		try {
			return converterProposta.converterPropostaServicoParaPropostaAplicacao(propostaWebService.obterPropostaPorCodigo(sequencial));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao consultar propostas atrav�s do canal CORRETORA CALL CENTER PROPRIO.");
			throw new EEDIBusinessException("", e.getFaultInfo());	
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao consultar propostas atrav�s do canal CORRETORA CALL CENTER PROPRIO.");
			throw new IntegrationException(e.getMessage());
		}
	}
	
	/**
	 * Metodo responsavel por cancelar a proposta atraves do sequencial da proposta.
	 * 
	 * @param sequencialProposta - sequencial da proposta a ser cancelado.
	 */
	public void cancelarProposta(Long sequencialProposta){
		try {
			propostaWebService.cancelar(sequencialProposta);
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao cancelar a proposta atrav�s do canal CALL CENTER PROPRIO.");
			throw new EEDIBusinessException("", e.getFaultInfo());	
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao cancelar a proposta atrav�s do canal CALL CENTER PROPRIO.");
			throw new IntegrationException(e.getMessage());
		}
	}
	
	/**
	 * Metodo responsavel por salvar rascunho de uma proposta.
	 * 
	 * @param proposta - informa��es da proposta a serem gravadas.
	 * @return Long - sequencial da proposta gerado.
	 * @throws BusinessException_Exception
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public Long salvarRascunhoProposta(PropostaVO proposta) {

		validadorNovaProposta.validate(proposta);

		try {
			return propostaWebService.salvarRascunho(converterProposta.converterPropostaAplicacaoParaPropostaServico(proposta));
		} catch (BusinessException_Exception e) {
			LOGGER.error(Constantes.ERRO_DE_NEGOCIO_AO_SALVAR_RASCUNHO_DE_UMA_PROPOSTA_ATRAVES_DO_SITE_DO_PRODUTOR);

			List<Message> listaErros = new ArrayList<Message>();
			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Message mensagemErro : e.getFaultInfo().getMessages()) {
				Message erro = new Message(mensagemErro.getMessage());
				listaErros.add(erro);
			}
			throw new EEDIBusinessException(listaErros);
		} catch (IntegrationException_Exception e) {
			LOGGER.error(Constantes.ERRO_DE_INTEGRACAO_AO_SALVAR_RASCUNHO_DE_UMA_PROPOSTA_ATRAVES_DO_SITE_DO_PRODUTOR);
			throw new IntegrationException(e.getMessage());
		}
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean validarDadosCartaoCredito(PropostaVO proposta) {
		boolean envioMIPComSucesso = false;
		RetornoVO retornoMIP = null;
		PropostaVO propostaParaTratamentoCartaoCredito = obterPropostaPorSequencial(proposta.getSequencial());
		propostaParaTratamentoCartaoCredito.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
		propostaParaTratamentoCartaoCredito.getPagamento().setCartaoCredito(proposta.getPagamento().getCartaoCredito());

		/**
		 * Valida o AcessToken
		 */
		if(proposta.getPagamento().getCartaoCredito().getAccessToken() != null){
	
			/**
			 * Codigo colocado para validar os campos que n�o estavam sendo contemplados
			 * autor: Roberto William
			 * data: 21/06/2019
			 */
			propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setBandeira(proposta.getPagamento().getCartaoCredito().getBandeira());
			propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setNumeroCartao(proposta.getPagamento().getCartaoCredito().getNumeroCartao());
			propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setNomeCartao(proposta.getPagamento().getCartaoCredito().getNomeCartao());
			propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setValidade(proposta.getPagamento().getCartaoCredito().getValidade());
			propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setPaymentToken(odontoprevServiceFacade.obterPaymentToken(proposta));
			
			propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setCpfCartao(proposta.getBeneficiarios().getTitular().getCpf());
			propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setCpfCartao(proposta.getPagamento().getContaCorrente().getCpf());
			if (br.com.bradseg.eedi.novaemissaoexpressadi.vo.FormaPagamento.CARTAO_CREDITO.getCodigo().equals(Integer.parseInt(proposta.getPagamento().getFormaPagamento()))) {
				propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setCpfCartao(proposta.getBeneficiarios().getTitular().getCpf());
				retornoMIP = odontoprevServiceFacade.debitarPrimeiraParcelaCartaoDeCredito(propostaParaTratamentoCartaoCredito);
			}
			if (retornoMIP != null && retornoMIP.getSucesso() == RETONO_MIP_SUCCESSO) {
				envioMIPComSucesso = true;
			}
		}
		return envioMIPComSucesso;
	}
	
}
