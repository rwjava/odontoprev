package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;

/**
 * Classe responsavel pelo transporte de dados do produtor.
 *
 */
public class ProdutorVO implements Serializable {

	private static final long serialVersionUID = 1817789260056445482L;
	
	private String nome;
	private String cpfCnpj;
	
	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * Retorna cpfCnpj.
	 *
	 * @return cpfCnpj - cpfCnpj.
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	/**
	 * Especifica cpfCnpj.
	 *
	 * @param cpfCnpj - cpfCnpj.
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}


}
