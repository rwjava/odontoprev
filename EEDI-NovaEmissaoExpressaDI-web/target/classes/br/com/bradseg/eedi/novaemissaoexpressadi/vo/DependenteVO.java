package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

/**
 * Classe responsavel por transportar os dados do beneficiário dependente.
 *
 */
public class DependenteVO extends PessoaVO {

	private static final long serialVersionUID = -4090451911390720399L;

	private String grauParentesco;
	private String grauParentescoBeneficiario;
	private String cns;
	private String dnv;
	

	/**
	 * Retorna cns.
	 *
	 * @return cns - cns.
	 */
	public String getCns() {
		return cns;
	}
	/**
	 * Especifica cns.
	 *
	 * @param cns - cns.
	 */
	public void setCns(String cns) {
		this.cns = cns;
	}
	/**
	 * Retorna dnv.
	 *
	 * @return dnv - dnv.
	 */
	public String getDnv() {
		return dnv;
	}
	/**
	 * Especifica dnv.
	 *
	 * @param dnv - dnv.
	 */
	public void setDnv(String dnv) {
		this.dnv = dnv;
	}
	public String getGrauParentesco() {
		return grauParentesco;
	}
	public void setGrauParentesco(String grauParentesco) {
		this.grauParentesco = grauParentesco;
	}
	public String getGrauParentescoBeneficiario() {
		return grauParentescoBeneficiario;
	}
	public void setGrauParentescoBeneficiario(String grauParentescoBeneficiario) {
		this.grauParentescoBeneficiario = grauParentescoBeneficiario;
	}


}
