package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar o grau de parentesco.
 *
 */
public enum GrauParentesco {

	PAI(1, "Pai"), MAE(2, "M�e"), FILHA(3, "Filha"), FILHO(4, "Filho"), CONJUGE(5, "Conjuge"), PROPRIO(19, "Proprio");

	private Integer codigo;
	private String nome;

	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo - codigo do grau.
	 * @param nome - nome do grau.
	 */
	private GrauParentesco(Integer codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Metodo responsavel por listar os graus de parentesco.
	 * 
	 * @return List<GrauParentesco> - lista de grau de parentesco.
	 */
	public static List<GrauParentesco> listar(){
		List<GrauParentesco> lista = new ArrayList<GrauParentesco>();
		for (GrauParentesco grauParentesco : GrauParentesco.values()) {
			lista.add(grauParentesco);
		}
		return lista;
	}
	
	public static GrauParentesco obterGrauParentescoPorCodigo(int codigo){
		for (GrauParentesco grauParentesco : GrauParentesco.values()) {
			if(codigo == grauParentesco.getCodigo()){
				return grauParentesco;
			}
		}
		return GrauParentesco.PROPRIO;
	}
}
