package br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PaymentTokenVO extends RetornoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3397024213225407265L;
	
	private String paymentToken;

	/**
	 * Retorna paymentToken.
	 *
	 * @return paymentToken - paymentToken
	 */
	public String getPaymentToken() {
		return paymentToken;
	}

	/**
	 * Especifica paymentToken.
	 *
	 * @param paymentToken - paymentToken
	 */
	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}
}
