package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;

import com.google.common.base.Preconditions;


public class LabelValueVO implements Serializable{

	private static final long serialVersionUID = 4018791822855850943L;

	private String codigo;
	private String descricao;

	public LabelValueVO(String codigo, String descricao) {
		Preconditions.checkNotNull(codigo, "O c�digo n�o pode ser nulo");
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

}