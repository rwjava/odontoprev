package br.com.bradseg.eedi.novaemissaoexpressadi.util;

import br.com.bradseg.bsad.framework.core.text.formatter.MaskFormatter;

public class FormatacaoUtil {

	public static String formataCodigoDaProposta(String codigoProposta) {
		MaskFormatter mf = new MaskFormatter("***###########-#", String.class);
		mf.setValueContainsLiteralCharacters(false);

		return mf.valueToString(codigoProposta);
	}
}
