
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for produtorVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="produtorVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cpd" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cpfCnpj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposta" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}propostaVO" minOccurs="0"/>
 *         &lt;element name="sucursalSeguradora" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}sucursalSeguradoraVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "produtorVO", propOrder = {
    "cpd",
    "cpfCnpj",
    "nome",
    "proposta",
    "sucursalSeguradora"
})
@XmlSeeAlso({
    CorretorMasterVO.class,
    AngariadorVO.class,
    CorretorVO.class
})
public abstract class ProdutorVO {

    protected Integer cpd;
    protected String cpfCnpj;
    protected String nome;
    protected PropostaVO proposta;
    protected SucursalSeguradoraVO sucursalSeguradora;

    /**
     * Gets the value of the cpd property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCpd() {
        return cpd;
    }

    /**
     * Sets the value of the cpd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCpd(Integer value) {
        this.cpd = value;
    }

    /**
     * Gets the value of the cpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Sets the value of the cpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfCnpj(String value) {
        this.cpfCnpj = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the proposta property.
     * 
     * @return
     *     possible object is
     *     {@link PropostaVO }
     *     
     */
    public PropostaVO getProposta() {
        return proposta;
    }

    /**
     * Sets the value of the proposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropostaVO }
     *     
     */
    public void setProposta(PropostaVO value) {
        this.proposta = value;
    }

    /**
     * Gets the value of the sucursalSeguradora property.
     * 
     * @return
     *     possible object is
     *     {@link SucursalSeguradoraVO }
     *     
     */
    public SucursalSeguradoraVO getSucursalSeguradora() {
        return sucursalSeguradora;
    }

    /**
     * Sets the value of the sucursalSeguradora property.
     * 
     * @param value
     *     allowed object is
     *     {@link SucursalSeguradoraVO }
     *     
     */
    public void setSucursalSeguradora(SucursalSeguradoraVO value) {
        this.sucursalSeguradora = value;
    }

}
