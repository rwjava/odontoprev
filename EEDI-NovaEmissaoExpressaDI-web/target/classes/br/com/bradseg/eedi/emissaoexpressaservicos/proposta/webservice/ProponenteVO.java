
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for proponenteVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="proponenteVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}pessoaVO">
 *       &lt;sequence>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="contaCorrente" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}contaCorrenteVO" minOccurs="0"/>
 *         &lt;element name="telefones" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}telefoneVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "proponenteVO", propOrder = {
    "codigo",
    "contaCorrente",
    "telefones"
})
public class ProponenteVO
    extends PessoaVO
{

    protected Long codigo;
    protected ContaCorrenteVO contaCorrente;
    @XmlElement(nillable = true)
    protected List<TelefoneVO> telefones;

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigo(Long value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the contaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link ContaCorrenteVO }
     *     
     */
    public ContaCorrenteVO getContaCorrente() {
        return contaCorrente;
    }

    /**
     * Sets the value of the contaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContaCorrenteVO }
     *     
     */
    public void setContaCorrente(ContaCorrenteVO value) {
        this.contaCorrente = value;
    }

    /**
     * Gets the value of the telefones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the telefones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTelefones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TelefoneVO }
     * 
     * 
     */
    public List<TelefoneVO> getTelefones() {
        if (telefones == null) {
            telefones = new ArrayList<TelefoneVO>();
        }
        return this.telefones;
    }

}
