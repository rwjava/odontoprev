
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for filtroRelatorioAcompanhamentoVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="filtroRelatorioAcompanhamentoVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}filtroPropostaVO">
 *       &lt;sequence>
 *         &lt;element name="codigoAgenciaDebito" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codigoAgenciaProdutora" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codigoSucursalSeguradora" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="matriculaAssistenteBS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="matriculaGerente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pessoa" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}pessoaVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "filtroRelatorioAcompanhamentoVO", propOrder = {
    "codigoAgenciaDebito",
    "codigoAgenciaProdutora",
    "codigoSucursalSeguradora",
    "matriculaAssistenteBS",
    "matriculaGerente",
    "pessoa"
})
public class FiltroRelatorioAcompanhamentoVO
    extends FiltroPropostaVO
{

    protected Integer codigoAgenciaDebito;
    protected Integer codigoAgenciaProdutora;
    protected Integer codigoSucursalSeguradora;
    protected String matriculaAssistenteBS;
    protected String matriculaGerente;
    protected PessoaVO pessoa;

    /**
     * Gets the value of the codigoAgenciaDebito property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoAgenciaDebito() {
        return codigoAgenciaDebito;
    }

    /**
     * Sets the value of the codigoAgenciaDebito property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoAgenciaDebito(Integer value) {
        this.codigoAgenciaDebito = value;
    }

    /**
     * Gets the value of the codigoAgenciaProdutora property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoAgenciaProdutora() {
        return codigoAgenciaProdutora;
    }

    /**
     * Sets the value of the codigoAgenciaProdutora property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoAgenciaProdutora(Integer value) {
        this.codigoAgenciaProdutora = value;
    }

    /**
     * Gets the value of the codigoSucursalSeguradora property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoSucursalSeguradora() {
        return codigoSucursalSeguradora;
    }

    /**
     * Sets the value of the codigoSucursalSeguradora property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoSucursalSeguradora(Integer value) {
        this.codigoSucursalSeguradora = value;
    }

    /**
     * Gets the value of the matriculaAssistenteBS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatriculaAssistenteBS() {
        return matriculaAssistenteBS;
    }

    /**
     * Sets the value of the matriculaAssistenteBS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatriculaAssistenteBS(String value) {
        this.matriculaAssistenteBS = value;
    }

    /**
     * Gets the value of the matriculaGerente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatriculaGerente() {
        return matriculaGerente;
    }

    /**
     * Sets the value of the matriculaGerente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatriculaGerente(String value) {
        this.matriculaGerente = value;
    }

    /**
     * Gets the value of the pessoa property.
     * 
     * @return
     *     possible object is
     *     {@link PessoaVO }
     *     
     */
    public PessoaVO getPessoa() {
        return pessoa;
    }

    /**
     * Sets the value of the pessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link PessoaVO }
     *     
     */
    public void setPessoa(PessoaVO value) {
        this.pessoa = value;
    }

}
