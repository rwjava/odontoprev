
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for responsavelLegalVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="responsavelLegalVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}pessoaVO">
 *       &lt;sequence>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="endereco" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}enderecoVO" minOccurs="0"/>
 *         &lt;element name="telefone" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}telefoneVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responsavelLegalVO", propOrder = {
    "codigo",
    "endereco",
    "telefone"
})
public class ResponsavelLegalVO
    extends PessoaVO
{

    protected Long codigo;
    protected EnderecoVO endereco;
    protected TelefoneVO telefone;

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigo(Long value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the endereco property.
     * 
     * @return
     *     possible object is
     *     {@link EnderecoVO }
     *     
     */
    public EnderecoVO getEndereco() {
        return endereco;
    }

    /**
     * Sets the value of the endereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnderecoVO }
     *     
     */
    public void setEndereco(EnderecoVO value) {
        this.endereco = value;
    }

    /**
     * Gets the value of the telefone property.
     * 
     * @return
     *     possible object is
     *     {@link TelefoneVO }
     *     
     */
    public TelefoneVO getTelefone() {
        return telefone;
    }

    /**
     * Sets the value of the telefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link TelefoneVO }
     *     
     */
    public void setTelefone(TelefoneVO value) {
        this.telefone = value;
    }

}
