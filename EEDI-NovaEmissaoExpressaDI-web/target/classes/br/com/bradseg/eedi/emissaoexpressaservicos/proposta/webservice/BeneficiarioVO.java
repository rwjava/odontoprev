
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for beneficiarioVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="beneficiarioVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}pessoaVO">
 *       &lt;sequence>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="estadoCivil" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}estadoCivil" minOccurs="0"/>
 *         &lt;element name="nomeMae" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroCartaoNacionalSaude" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="numeroCarteiraOdontoprev" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="numeroDeclaracaoNascidoVivo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="proposta" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}propostaVO" minOccurs="0"/>
 *         &lt;element name="sexo" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}sexo" minOccurs="0"/>
 *         &lt;element name="grauParentescoTitulaProponente" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}grauParentescoBeneficiario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beneficiarioVO", propOrder = {
    "codigo",
    "estadoCivil",
    "grauParentescoTitulaProponente",
    "nomeMae",
    "numeroCartaoNacionalSaude",
    "numeroCarteiraOdontoprev",
    "numeroDeclaracaoNascidoVivo",
    "proposta",
    "sexo"
})
@XmlSeeAlso({
    DependenteVO.class,
    TitularVO.class
})
public abstract class BeneficiarioVO
    extends PessoaVO
{

    protected Integer codigo;
    protected EstadoCivil estadoCivil;
    protected GrauParentesco grauParentescoTitulaProponente;
	protected String nomeMae;
    protected Long numeroCartaoNacionalSaude;
    protected Long numeroCarteiraOdontoprev;
    protected Long numeroDeclaracaoNascidoVivo;
    protected PropostaVO proposta;
    protected Sexo sexo;

   public GrauParentesco getGrauParentescoTitulaProponente() {
        return grauParentescoTitulaProponente;
    }

    /**
     * Sets the value of the proposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropostaVO }
     *     
     */
    public void setGrauParentescoTitulaProponente(GrauParentesco value) {
        this.grauParentescoTitulaProponente = value;
    }
    
    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigo(Integer value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the estadoCivil property.
     * 
     * @return
     *     possible object is
     *     {@link EstadoCivil }
     *     
     */
    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Sets the value of the estadoCivil property.
     * 
     * @param value
     *     allowed object is
     *     {@link EstadoCivil }
     *     
     */
    public void setEstadoCivil(EstadoCivil value) {
        this.estadoCivil = value;
    }

    /**
     * Gets the value of the nomeMae property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeMae() {
        return nomeMae;
    }

    /**
     * Sets the value of the nomeMae property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeMae(String value) {
        this.nomeMae = value;
    }

    /**
     * Gets the value of the numeroCartaoNacionalSaude property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroCartaoNacionalSaude() {
        return numeroCartaoNacionalSaude;
    }

    /**
     * Sets the value of the numeroCartaoNacionalSaude property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroCartaoNacionalSaude(Long value) {
        this.numeroCartaoNacionalSaude = value;
    }

    /**
     * Gets the value of the numeroCarteiraOdontoprev property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroCarteiraOdontoprev() {
        return numeroCarteiraOdontoprev;
    }

    /**
     * Sets the value of the numeroCarteiraOdontoprev property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroCarteiraOdontoprev(Long value) {
        this.numeroCarteiraOdontoprev = value;
    }

    /**
     * Gets the value of the numeroDeclaracaoNascidoVivo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroDeclaracaoNascidoVivo() {
        return numeroDeclaracaoNascidoVivo;
    }

    /**
     * Sets the value of the numeroDeclaracaoNascidoVivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroDeclaracaoNascidoVivo(Long value) {
        this.numeroDeclaracaoNascidoVivo = value;
    }

    /**
     * Gets the value of the proposta property.
     * 
     * @return
     *     possible object is
     *     {@link PropostaVO }
     *     
     */
    public PropostaVO getProposta() {
        return proposta;
    }

    /**
     * Sets the value of the proposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropostaVO }
     *     
     */
    public void setProposta(PropostaVO value) {
        this.proposta = value;
    }

    /**
     * Gets the value of the sexo property.
     * 
     * @return
     *     possible object is
     *     {@link Sexo }
     *     
     */
    public Sexo getSexo() {
        return sexo;
    }

    /**
     * Sets the value of the sexo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sexo }
     *     
     */
    public void setSexo(Sexo value) {
        this.sexo = value;
    }

}
