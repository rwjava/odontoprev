
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for subsegmentacao.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="subsegmentacao">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PRIME"/>
 *     &lt;enumeration value="EXCLUSIVE"/>
 *     &lt;enumeration value="CLASSIC"/>
 *     &lt;enumeration value="ESPACO_PRIME"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "subsegmentacao")
@XmlEnum
public enum Subsegmentacao {

    PRIME,
    EXCLUSIVE,
    CLASSIC,
    ESPACO_PRIME;

    public String value() {
        return name();
    }

    public static Subsegmentacao fromValue(String v) {
        return valueOf(v);
    }

}
