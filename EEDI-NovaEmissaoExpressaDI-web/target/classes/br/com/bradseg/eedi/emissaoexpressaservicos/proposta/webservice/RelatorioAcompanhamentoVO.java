
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for relatorioAcompanhamentoVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="relatorioAcompanhamentoVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filtroRelatorioAcompanhamento" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}filtroRelatorioAcompanhamentoVO" minOccurs="0"/>
 *         &lt;element name="itensRelatorioAcompanhamento" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}itemRelatorioAcompanhamentoVO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "relatorioAcompanhamentoVO", propOrder = {
    "filtroRelatorioAcompanhamento",
    "itensRelatorioAcompanhamento",
    "nome"
})
public class RelatorioAcompanhamentoVO {

    protected FiltroRelatorioAcompanhamentoVO filtroRelatorioAcompanhamento;
    @XmlElement(nillable = true)
    protected List<ItemRelatorioAcompanhamentoVO> itensRelatorioAcompanhamento;
    protected String nome;

    /**
     * Gets the value of the filtroRelatorioAcompanhamento property.
     * 
     * @return
     *     possible object is
     *     {@link FiltroRelatorioAcompanhamentoVO }
     *     
     */
    public FiltroRelatorioAcompanhamentoVO getFiltroRelatorioAcompanhamento() {
        return filtroRelatorioAcompanhamento;
    }

    /**
     * Sets the value of the filtroRelatorioAcompanhamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link FiltroRelatorioAcompanhamentoVO }
     *     
     */
    public void setFiltroRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoVO value) {
        this.filtroRelatorioAcompanhamento = value;
    }

    /**
     * Gets the value of the itensRelatorioAcompanhamento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itensRelatorioAcompanhamento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItensRelatorioAcompanhamento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemRelatorioAcompanhamentoVO }
     * 
     * 
     */
    public List<ItemRelatorioAcompanhamentoVO> getItensRelatorioAcompanhamento() {
        if (itensRelatorioAcompanhamento == null) {
            itensRelatorioAcompanhamento = new ArrayList<ItemRelatorioAcompanhamentoVO>();
        }
        return this.itensRelatorioAcompanhamento;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

}
