
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for telefoneVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="telefoneVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ddd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proponente" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}proponenteVO" minOccurs="0"/>
 *         &lt;element name="ramal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="responsavelLegal" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}responsavelLegalVO" minOccurs="0"/>
 *         &lt;element name="tipo" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}tipoTelefone" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "telefoneVO", propOrder = {
    "codigo",
    "ddd",
    "numero",
    "proponente",
    "ramal",
    "responsavelLegal",
    "tipo"
})
public class TelefoneVO {

    protected Integer codigo;
    protected String ddd;
    protected String numero;
    protected ProponenteVO proponente;
    protected Integer ramal;
    protected ResponsavelLegalVO responsavelLegal;
    protected TipoTelefone tipo;

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigo(Integer value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the ddd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDdd() {
        return ddd;
    }

    /**
     * Sets the value of the ddd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDdd(String value) {
        this.ddd = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the proponente property.
     * 
     * @return
     *     possible object is
     *     {@link ProponenteVO }
     *     
     */
    public ProponenteVO getProponente() {
        return proponente;
    }

    /**
     * Sets the value of the proponente property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProponenteVO }
     *     
     */
    public void setProponente(ProponenteVO value) {
        this.proponente = value;
    }

    /**
     * Gets the value of the ramal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRamal() {
        return ramal;
    }

    /**
     * Sets the value of the ramal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRamal(Integer value) {
        this.ramal = value;
    }

    /**
     * Gets the value of the responsavelLegal property.
     * 
     * @return
     *     possible object is
     *     {@link ResponsavelLegalVO }
     *     
     */
    public ResponsavelLegalVO getResponsavelLegal() {
        return responsavelLegal;
    }

    /**
     * Sets the value of the responsavelLegal property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsavelLegalVO }
     *     
     */
    public void setResponsavelLegal(ResponsavelLegalVO value) {
        this.responsavelLegal = value;
    }

    /**
     * Gets the value of the tipo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoTelefone }
     *     
     */
    public TipoTelefone getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTelefone }
     *     
     */
    public void setTipo(TipoTelefone value) {
        this.tipo = value;
    }

}
