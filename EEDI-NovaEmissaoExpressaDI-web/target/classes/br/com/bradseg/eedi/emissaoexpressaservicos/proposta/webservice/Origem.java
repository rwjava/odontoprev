
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for origem.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="origem">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SITE_100_CORRETOR"/>
 *     &lt;enumeration value="CALL_CENTER"/>
 *     &lt;enumeration value="FAMILIA_BRADESCO"/>
 *     &lt;enumeration value="INTERNET_BANKING"/>
 *     &lt;enumeration value="WORKSITE_CALL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "origem")
@XmlEnum
public enum Origem {

    SITE_100_CORRETOR,
    CALL_CENTER,
    FAMILIA_BRADESCO,
    INTERNET_BANKING,
    WORKSITE_CALL;

    public String value() {
        return name();
    }

    public static Origem fromValue(String v) {
        return valueOf(v);
    }

}
