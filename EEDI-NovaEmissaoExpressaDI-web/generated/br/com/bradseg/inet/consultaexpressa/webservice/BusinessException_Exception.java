
package br.com.bradseg.inet.consultaexpressa.webservice;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.5.2
 * 2012-10-30T16:11:34.257-02:00
 * Generated source version: 2.5.2
 */

@WebFault(name = "BusinessException", targetNamespace = "http://webservice.consultaexpressa.inet.bradseg.com.br/")
public class BusinessException_Exception extends Exception {
    
    private br.com.bradseg.inet.consultaexpressa.webservice.BusinessException businessException;

    public BusinessException_Exception() {
        super();
    }
    
    public BusinessException_Exception(String message) {
        super(message);
    }
    
    public BusinessException_Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException_Exception(String message, br.com.bradseg.inet.consultaexpressa.webservice.BusinessException businessException) {
        super(message);
        this.businessException = businessException;
    }

    public BusinessException_Exception(String message, br.com.bradseg.inet.consultaexpressa.webservice.BusinessException businessException, Throwable cause) {
        super(message, cause);
        this.businessException = businessException;
    }

    public br.com.bradseg.inet.consultaexpressa.webservice.BusinessException getFaultInfo() {
        return this.businessException;
    }
}
