
package br.com.bradseg.ccrr.cadastrocorretores.exception.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.ccrr.cadastrocorretores.exception.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WSIntegrationException_QNAME = new QName("http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br", "WSIntegrationException");
    private final static QName _WSBusinessException_QNAME = new QName("http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br", "WSBusinessException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.ccrr.cadastrocorretores.exception.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSBusinessException }
     * 
     */
    public WSBusinessException createWSBusinessException() {
        return new WSBusinessException();
    }

    /**
     * Create an instance of {@link WSIntegrationException }
     * 
     */
    public WSIntegrationException createWSIntegrationException() {
        return new WSIntegrationException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSIntegrationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br", name = "WSIntegrationException")
    public JAXBElement<WSIntegrationException> createWSIntegrationException(WSIntegrationException value) {
        return new JAXBElement<WSIntegrationException>(_WSIntegrationException_QNAME, WSIntegrationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSBusinessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br", name = "WSBusinessException")
    public JAXBElement<WSBusinessException> createWSBusinessException(WSBusinessException value) {
        return new JAXBElement<WSBusinessException>(_WSBusinessException_QNAME, WSBusinessException.class, null, value);
    }

}
