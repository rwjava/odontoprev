
package br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.ccrr.cadastrocorretores.model.vo.ws.WSCorretorVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="obterDadosCorretorPorCpdReturn" type="{http://ws.vo.model.cadastrocorretores.ccrr.bradseg.com.br}WSCorretorVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "obterDadosCorretorPorCpdReturn"
})
@XmlRootElement(name = "obterDadosCorretorPorCpdResponse")
public class ObterDadosCorretorPorCpdResponse {

    @XmlElement(required = true, nillable = true)
    protected WSCorretorVO obterDadosCorretorPorCpdReturn;

    /**
     * Gets the value of the obterDadosCorretorPorCpdReturn property.
     * 
     * @return
     *     possible object is
     *     {@link WSCorretorVO }
     *     
     */
    public WSCorretorVO getObterDadosCorretorPorCpdReturn() {
        return obterDadosCorretorPorCpdReturn;
    }

    /**
     * Sets the value of the obterDadosCorretorPorCpdReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCorretorVO }
     *     
     */
    public void setObterDadosCorretorPorCpdReturn(WSCorretorVO value) {
        this.obterDadosCorretorPorCpdReturn = value;
    }

}
