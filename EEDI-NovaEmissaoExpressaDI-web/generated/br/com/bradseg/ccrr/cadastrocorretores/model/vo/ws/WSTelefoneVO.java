
package br.com.bradseg.ccrr.cadastrocorretores.model.vo.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSTelefoneVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSTelefoneVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numeroDDD" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroTelefone" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSTelefoneVO", propOrder = {
    "numeroDDD",
    "numeroTelefone"
})
public class WSTelefoneVO {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer numeroDDD;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer numeroTelefone;

    /**
     * Gets the value of the numeroDDD property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroDDD() {
        return numeroDDD;
    }

    /**
     * Sets the value of the numeroDDD property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroDDD(Integer value) {
        this.numeroDDD = value;
    }

    /**
     * Gets the value of the numeroTelefone property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroTelefone() {
        return numeroTelefone;
    }

    /**
     * Sets the value of the numeroTelefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroTelefone(Integer value) {
        this.numeroTelefone = value;
    }

}
