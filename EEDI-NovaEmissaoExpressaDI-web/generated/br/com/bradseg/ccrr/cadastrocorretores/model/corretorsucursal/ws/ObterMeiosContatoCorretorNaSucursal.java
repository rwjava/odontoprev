
package br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cpfCnpjCorretor" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sucursal",
    "cpfCnpjCorretor"
})
@XmlRootElement(name = "obterMeiosContatoCorretorNaSucursal")
public class ObterMeiosContatoCorretorNaSucursal {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer sucursal;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cpfCnpjCorretor;

    /**
     * Gets the value of the sucursal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSucursal() {
        return sucursal;
    }

    /**
     * Sets the value of the sucursal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSucursal(Integer value) {
        this.sucursal = value;
    }

    /**
     * Gets the value of the cpfCnpjCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCpfCnpjCorretor() {
        return cpfCnpjCorretor;
    }

    /**
     * Sets the value of the cpfCnpjCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCpfCnpjCorretor(Long value) {
        this.cpfCnpjCorretor = value;
    }

}
