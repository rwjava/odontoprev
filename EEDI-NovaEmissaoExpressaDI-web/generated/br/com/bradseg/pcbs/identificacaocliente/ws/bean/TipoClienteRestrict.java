
package br.com.bradseg.pcbs.identificacaocliente.ws.bean;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoClienteRestrict.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoClienteRestrict">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PROSPECT"/>
 *     &lt;enumeration value="CLIENTE"/>
 *     &lt;enumeration value="INEXISTENTE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoClienteRestrict")
@XmlEnum
public enum TipoClienteRestrict {

    PROSPECT,
    CLIENTE,
    INEXISTENTE;

    public String value() {
        return name();
    }

    public static TipoClienteRestrict fromValue(String v) {
        return valueOf(v);
    }

}
