
package br.com.bradseg.pcbs.identificacaocliente.ws.bean;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for statusClienteRestrict.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="statusClienteRestrict">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="S"/>
 *     &lt;enumeration value="N"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "statusClienteRestrict")
@XmlEnum
public enum StatusClienteRestrict {

    S,
    N;

    public String value() {
        return name();
    }

    public static StatusClienteRestrict fromValue(String v) {
        return valueOf(v);
    }

}
