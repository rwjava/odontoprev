package br.com.bradseg.eedi.novaemissaoexpressadi.web.struts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;

import org.joda.time.LocalDate;
import org.junit.Test;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.novaemissaoexpressadi.test.util.BaseValidatorTest;

public class LocalDateConverterTest extends BaseValidatorTest {

	@Test
	public void deveConverterStringParaDataCorretamente(){
		String data = "01/10/1980";
		LocalDateConverter localDateConverter = new LocalDateConverter();
		String[] values = {data};
		Object objetoDataRetornado = localDateConverter.convertFromString(new HashMap<Object, Object>(), values, LocalDateConverter.class);
		
		assertNotNull(objetoDataRetornado);
		assertEquals(new LocalDate(1980, 10, 1), (LocalDate) objetoDataRetornado);
		
	}
	
	@Test
	public void deveConverterStringParaDataDeInicioDeHorarioDeVerao(){
		String data = "17/09/1983";
		LocalDateConverter localDateConverter = new LocalDateConverter();
		String[] values = {data};
		Object objetoDataRetornado = localDateConverter.convertFromString(new HashMap<Object, Object>(), values, LocalDateConverter.class);
		
		assertNotNull(objetoDataRetornado);
		assertEquals(new LocalDate(1983, 9, 17), (LocalDate) objetoDataRetornado);
	}
	
	@Test(expected = BusinessException.class)
	public void deveRetornarErroQuandoDataEstiverComFormatoInvalido(){
		String data = "01-20-1980";
		LocalDateConverter localDateConverter = new LocalDateConverter();
		String[] values = {data};
		try{
			localDateConverter.convertFromString(new HashMap<Object, Object>(), values, LocalDateConverter.class);
		}catch(BusinessException e){
			assertEquals("msg.erro.formato.data.invalido", e.getMessage());
			throw e;
		}	
	}	

	@Test
	public void deveConverterDataParaString(){
		LocalDate data = new LocalDate(1983, 9, 17);
		LocalDateConverter localDateConverter = new LocalDateConverter();
		String dataConvertida = localDateConverter.convertToString(new HashMap<Object, Object>(), data);
		assertEquals("17/09/1983", dataConvertida);
	}

}
