package br.com.bradseg.eedi.novaemissaoexpressadi.util;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class FormatacaoUtilTest {


	@Test
	public void deveRetornarCodigoDePropostaFormatado(){
		String codigo = "BDA000000536927";
		assertEquals("BDA00000053692-7", FormatacaoUtil.formataCodigoDaProposta(codigo));
	}
}
