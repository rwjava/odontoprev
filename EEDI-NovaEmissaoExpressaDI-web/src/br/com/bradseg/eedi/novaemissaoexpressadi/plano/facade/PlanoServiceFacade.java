package br.com.bradseg.eedi.novaemissaoexpressadi.plano.facade;

import java.util.List;

import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PlanoVO;


/**
 * Interface responsavel por disponibilizar os metodos referentes a plano.
 *
 */
public interface PlanoServiceFacade {
	
	/**
	 * Metodo responsavel por listar os planos vigente para o canal CORRETORA CALL CENTER.
	 * 
	 * @return List<PlanoVO> - lista de planos vigente do canal CORRETORA CALL CENTER.
	 */
	public List<PlanoVO> listarPlanosVigente();

	public PlanoVO obterPlanoPorCodigo(Long codigoPlano);
	
}
