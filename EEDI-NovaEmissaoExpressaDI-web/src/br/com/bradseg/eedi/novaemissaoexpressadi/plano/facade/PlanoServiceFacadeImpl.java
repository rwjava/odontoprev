package br.com.bradseg.eedi.novaemissaoexpressadi.plano.facade;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.BusinessException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.IntegrationException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaWebService;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.Constantes;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PlanoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.ValorPlanoVO;

/**
 * Classe de neg�cio responsavel por disponibilizar os metodos referentes a plano.
 *
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class PlanoServiceFacadeImpl implements PlanoServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanoServiceFacadeImpl.class);
	
	@Autowired
	private PropostaWebService propostaWebService;
	
	/**
	 * Metodo responsavel por listar os planos vigente para o canal CORRETORA CALL CENTER.
	 * 
	 * @return List<PlanoVO> - lista de planos vigente do canal CORRETORA CALL CENTER.
	 */
	public List<PlanoVO> listarPlanosVigente() {
		try {			
			return converterListaDePlanoServicoParaListaDePlanoAplicacao(propostaWebService.listarPlanosVigentePorCanal(Constantes.CODIGO_CANAL_CORRETORA_CALL_CENTER));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao obter os planos vigente para o canal CORRETORA CALL CENTER.");
			throw new BusinessException(e.getMessage());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao obter os planos vigente para o canal CORRETORA CALL CENTER.");
			throw new IntegrationException(e.getMessage());
		}
	}
	
	public List<PlanoVO> converterListaDePlanoServicoParaListaDePlanoAplicacao(List<br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PlanoVO> listaServico){
		List<PlanoVO> listaAplicacao = new ArrayList<PlanoVO>();
		
		if(listaServico != null && !listaServico.isEmpty()){
			PlanoVO planoAplicacao = null;
			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PlanoVO planoServico : listaServico) {
				planoAplicacao = new PlanoVO();
				planoAplicacao.setCodigo(planoServico.getCodigo());
				planoAplicacao.setNome(planoServico.getNome());
				planoAplicacao.setDescricaoCarenciaPeriodoMensal(planoServico.getDescricaoCarenciaPeriodoMensal());
				planoAplicacao.setDescricaoCarenciaPeriodoAnual(planoServico.getDescricaoCarenciaPeriodoAnual());
				planoAplicacao.setValorPlanoVO(new ValorPlanoVO());
				planoAplicacao.getValorPlanoVO().setValorMensalTitular(planoServico.getValorPlanoVO().getValorMensalTitular());
				planoAplicacao.getValorPlanoVO().setValorMensalDependente(planoServico.getValorPlanoVO().getValorMensalDependente());
				planoAplicacao.getValorPlanoVO().setValorAnualTitular(planoServico.getValorPlanoVO().getValorAnualTitular());
				planoAplicacao.getValorPlanoVO().setValorAnualDependente(planoServico.getValorPlanoVO().getValorAnualDependente());
				
				listaAplicacao.add(planoAplicacao);
			}
		}
		
		return listaAplicacao;
	}
	
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano){
		try {
			return converterObjetoPlanoServicoParaObjetoPlanoAplicacao(propostaWebService.obterPlanoPorCodigo(codigoPlano));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao obter o codigo do plano.");
			throw new BusinessException(e.getMessage());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao obter o codigo do plano.");
			throw new IntegrationException(e.getMessage());
		}
			
	}
	
public PlanoVO converterObjetoPlanoServicoParaObjetoPlanoAplicacao(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PlanoVO planoServico){
		
		PlanoVO planoAplicacao = new PlanoVO();
		planoAplicacao = new PlanoVO();
		planoAplicacao.setCodigo(planoServico.getCodigo());
		planoAplicacao.setNome(planoServico.getNome());
		planoAplicacao.setDescricaoCarenciaPeriodoMensal(planoServico.getDescricaoCarenciaPeriodoMensal());
		planoAplicacao.setDescricaoCarenciaPeriodoAnual(planoServico.getDescricaoCarenciaPeriodoAnual());
		ValorPlanoVO valor = new ValorPlanoVO();
		valor.setValorAnualTitular(planoServico.getValorPlanoVO().getValorAnualTitular());
		valor.setValorAnualDependente(planoServico.getValorPlanoVO().getValorAnualDependente());
		valor.setValorMensalTitular(planoServico.getValorPlanoVO().getValorMensalTitular());
		valor.setValorMensalDependente(planoServico.getValorPlanoVO().getValorMensalDependente());
		planoAplicacao.setValorPlanoVO(valor);
		
		return planoAplicacao;
		
	}

}
