package br.com.bradseg.eedi.novaemissaoexpressadi.contacorrente.facade;

import java.util.List;

import br.com.bradseg.eedi.novaemissaoexpressadi.vo.ContaCorrenteVO;

/**
 * Interface de neg�cio responsavel por disponibilizar os metodos referentes a conta corrente.
 *
 */
public interface ContaCorrenteServiceFacade {
	
	/**
	 * Metodo responsavel por listar as contas corrente por cpf dos clientes bradesco.
	 * 
	 * @param cpf - cpf a ser consultado
	 * @return List<ContaCorrenteVO> - lista com as contas corrente.
	 */
	public List<ContaCorrenteVO> listarContaCorrentePorCPF(String cpf);

}
