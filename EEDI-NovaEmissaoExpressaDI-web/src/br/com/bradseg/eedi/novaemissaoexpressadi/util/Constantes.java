package br.com.bradseg.eedi.novaemissaoexpressadi.util;

/**
 * Constantes para auxiliar a aplica��o.
 *
 */
public class Constantes {
	
	public static final Integer CODIGO_CANAL_CORRETORA_CALL_CENTER = Integer.valueOf(8);
	
	public static final int NUMERO_MAXIMO_DEPENDENTES = 8;
	public static final int IDADE_PERMITADA_DIFERENCA_RESPONSAVEL_FINANCEIRO = 10;
	
	public static final int ANO_MINIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL = 100;
	public static final int ANO_MAXIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_DENTE_LEITE = 8;
	
	public static final int ANO_MAXIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_JUNIOR = 18;
	public static final int ANO_MINIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_JUNIOR = 8;
	
	public static final String DATA_FORMATO_DD_MM_YYYY = "dd/MM/yyyy";
	
	public static final String STRING_TITULAR = "Titular";
	
	
	public static final Integer QUANTIDADE_BENEFICIARIO_TITULAR = 1;
	
	public static final String PLANO = "plano";
	
	
	public static final String ERRO_DE_NEGOCIO_AO_SALVAR_RASCUNHO_DE_UMA_PROPOSTA_ATRAVES_DO_SITE_DO_PRODUTOR = "Erro de neg�cio ao salvar rascunho de uma proposta atrav�s do site do PRODUTOR.";
	public static final String ERRO_DE_INTEGRACAO_AO_SALVAR_RASCUNHO_DE_UMA_PROPOSTA_ATRAVES_DO_SITE_DO_PRODUTOR = "Erro de integra��o ao salvar rascunho de uma proposta atrav�s do site do PRODUTOR.";
	
}
