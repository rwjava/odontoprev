package br.com.bradseg.eedi.novaemissaoexpressadi.proposta.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.novaemissaoexpressadi.cep.facade.CepServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.contacorrente.facade.ContaCorrenteServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.exception.EEDIBusinessException;
import br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.facade.OdontoprevServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.plano.facade.PlanoServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.Constantes;
import br.com.bradseg.eedi.novaemissaoexpressadi.util.FormatacaoUtil;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.Banco;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.CorretorVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.EnderecoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.EstadoCivil;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.FormaPagamento;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.GrauParentesco;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.LabelValueVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PlanoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PropostaVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.Sexo;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.SituacaoProposta;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TelefoneVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoBusca;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoCPF;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoCobranca;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.TipoTelefone;

/**
 * Classe responsavel pela apresenta��o das informa��es e realiza��o das a��es referentes a tela.
 *
 */
@Controller
@Scope(value = "request")
public class PropostaAction extends BaseAction {

	private static final long serialVersionUID = 2413271777838545504L;
	
	private PropostaVO proposta = new PropostaVO();
	
	private int numeroDependente;
	
	private double valorTotalMensal;
	
	private double valorTotalAnual;
	
	private boolean indicadorCepDoTitular;
	
	private String contaCorrenteFormatada;
	
	private String cep;
	
	private List<PlanoVO> planos;
	
	private List<Sexo> sexos;
	
	private List<EstadoCivil> estadosCivis;
	
	private List<GrauParentesco> grausParentesco;
	
	private List<Banco> bancos;
	
	private List<TipoCobranca> tiposCobranca;
	
	//private List<FormaPagamento> formasPagamento;
	private List<LabelValueVO> formasPagamento = FormaPagamento.obterLista();
	
	private List<TipoTelefone> tiposTelefone;
	
	private List<ContaCorrenteVO> contasCorrente;
	
	private List<TipoBusca> tiposBusca;
	private FiltroPropostaVO filtroProposta = new FiltroPropostaVO();
	private List<TipoCPF> tiposCPFs;
	private List<SituacaoProposta> status;
	
	private List<PropostaVO> propostas;
	
	private boolean indicativoDeCancelamentoDaProposta;
	
	@Autowired
	private OdontoprevServiceFacade odontoprevServiceFacade;
	
	private List<LabelValueVO> bandeiras = new ArrayList<LabelValueVO>();
	
	private boolean corretorCartaoCreditoHabilitado;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaAction.class);
	
	@Autowired
	private transient PlanoServiceFacade planoServiceFacade;
	
	@Autowired
	private transient CepServiceFacade cepServiceFacade;
	
	@Autowired
	private transient ContaCorrenteServiceFacade contaCorrenteServiceFacade;
	
	@Autowired
	private transient PropostaServiceFacade propostaServiceFacade;
	
	private Long codigoPlano;
	
	private String nomePagante;
	
	private Integer numeroVidas;
	
	private PropostaVO propostaTemporaria;
	
	/**
	 * Metodo responsavel por inicializar todas as informa��es necess�rias para a cria��o de uma nova proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String novaProposta(){
		
		//inicializar todas informa��es necess�rias para a tela
		inicializarTelaNovaProposta();
		
		//setar os dados do corretor
		proposta.setCorretor(obterDadosCorretorLogado());
		
		return SUCCESS;
	}
	
	/**
	 * Metodo responsavel por obter o endere�o a partir do cep informado.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String obterEnderecoPorCep(){
		
		try{
			//inicializar todas informa��es necess�rias para a tela
			//inicializarTelaNovaProposta();
			codigoPlano = (Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO);
			
			EnderecoVO endereco = cepServiceFacade.obterEnderecoPorCep(cep);
			if(indicadorCepDoTitular){
				proposta.getBeneficiarios().getTitular().setEndereco(endereco);
			}else{
				proposta.getBeneficiarios().getRepresentanteLegal().setEndereco(endereco);
			}
		}catch(Exception e){
			addActionError(e.getMessage());
		}
		
		return SUCCESS;
	}
	
	/**
	 * Metodo responsavel por listar as contas corrente dos clientes bradesco.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String listarContaCorrentePorCPF(){
		
		try{
			//inicializar todas informa��es necess�rias para a tela
			inicializarTelaNovaProposta();
			
			contasCorrente = contaCorrenteServiceFacade.listarContaCorrentePorCPF(proposta.getPagamento().getContaCorrente().getCpf());
			ServletActionContext.getRequest().getSession().setAttribute("contasCorrente", contasCorrente);
			if(contasCorrente != null && !contasCorrente.isEmpty()){
				proposta.getPagamento().getContaCorrente().setNome(contasCorrente.get(0).getNome());
				proposta.getPagamento().getContaCorrente().setDataNascimento(contasCorrente.get(0).getDataNascimento());
				proposta.getPagamento().getContaCorrente().setDataNascimentoStr(contasCorrente.get(0).getDataNascimento().toString("dd/MM/yyyy"));				
				proposta.getPagamento().getContaCorrente().setNumeroAgencia(contasCorrente.get(0).getNumeroAgencia());
				proposta.getPagamento().getContaCorrente().setNumeroConta(contasCorrente.get(0).getNumeroConta());
				proposta.getPagamento().getContaCorrente().setDigitoVerificadorConta(contasCorrente.get(0).getDigitoVerificadorConta());
			}
			
		}catch(Exception e){
			addActionError(e.getMessage());
		}
		
		return SUCCESS;
	}
	
	/**
	 * Metodo responsavel por adicionar um dependente na proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String adicionarDependente(){

		try{
			codigoPlano = (Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO);
			//adicionar dependente
			//proposta.setCorretor(obterDadosCorretorLogado());
			propostaServiceFacade.adicionarDependente(proposta);
			if(proposta.getPlano().getCodigo() != null && proposta.getPlano().getCodigo() != 0){
				proposta.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
				calcularValorTotalDaPropostaNova();
			}
		
		}catch(BusinessException e){
			tratarErro(e);
		}
		
		//inicializar todas informa��es necess�rias para a tela
		inicializarTelaNovaProposta();
		
		return SUCCESS;
	}
	
	/**
	 * Metodo responsavel por remover um determinado dependente.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String removerDependente(){

		proposta.getBeneficiarios().getDependentes().remove(numeroDependente);
		
		//inicializar todas informa��es necess�rias para a tela
		inicializarTelaNovaProposta();
		
		return SUCCESS;
	}
	
	/**
	 * Metodo responsavel por alterar o plano da proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String alterarPlanoDaProposta(){
		
		//inicializar todas informa��es necess�rias para a tela
		inicializarTelaNovaProposta();
		
		return SUCCESS;
	}
	
	private void retirarFormatacaoCPFPagante() {
		proposta.getPagamento().getContaCorrente().setCpf(proposta.getPagamento().getContaCorrente().getCpf().replaceAll("[.,;\\-\\s]", ""));
	}

	/**
	 * Metodo responsavel por finalizar uma proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String finalizarProposta(){
		
		try{
		
			//inicializar todas informa��es necess�rias para a tela
			inicializarTelaNovaProposta();
			retirarFormatacaoCPFPagante();
			// indica o CNPJ do logado
			CorretorVO corretor = obterDadosCorretorLogado();
			proposta.getCorretor().setCpfCnpj(corretor.getCpfCnpj());
			
			/**
			 * Chama o M�todo salvarRascunhoProposta
			 */
			
			//finalizar uma proposta
			if(proposta.getSequencial() != null){
				
				propostaTemporaria =  propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());

				if(propostaTemporaria != null){
					proposta.setCodigo(propostaTemporaria.getCodigo());
					preencheDadosCartaoCredito(propostaTemporaria);
				}
				
					proposta.setSequencial(propostaServiceFacade.finalizarProposta(proposta));
				
				proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
				converteDadosCartaoCredito(proposta, propostaTemporaria);			
			}else{
				
				proposta.setSequencial(propostaServiceFacade.finalizarProposta(proposta));
				
				if(isCartaoCredito()){
					propostaTemporaria =  propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
					preencheDadosCartaoCredito(propostaTemporaria);
				}
					proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			}
			
			if(proposta.getCodigo() != null){
				
				if (isCartaoCredito()) {
					converteDadosCartaoCredito(proposta, propostaTemporaria);			
					//return validarDadosCartaoCredito();
				}

				addActionMessage("Proposta N� " + FormatacaoUtil.formataCodigoDaProposta(proposta.getCodigo()) + " finalizada com sucesso.");
				LOGGER.error(String.format("Proposta finalizada: [%s]", proposta.getSequencial()));
			}
				
		}catch(EEDIBusinessException e){
			LOGGER.error("Erro ao  finalizar proposta: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		}catch(BusinessException e){
			LOGGER.error("Erro ao  finalizar proposta: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		}catch (Exception e) {
			LOGGER.error("Erro ao  finalizar proposta: " + e.getMessage());
			addActionError("Erro ao  finalizar proposta: " + e.getMessage());
			return SUCCESS;
		}
		
		return SUCCESS;
	}

	private boolean isCartaoCredito() {
		return FormaPagamento.CARTAO_CREDITO.getCodigo().equals(Integer.parseInt(proposta.getPagamento().getFormaPagamento()));
	}
	
	/**
	 * Metodo responsavel por inicializar todas as informa��es necess�rias para a consulta de proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String listarProposta(){
		
		//inicializar todas informa��es necess�rias para a tela de listar proposta
		inicializarTelaListarProposta();
		
		return SUCCESS;
	}
	
	/**
	 * Metodo responsavel por consultar propostas de acordo com o filtro informado.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String consultarProposta(){
		
		//inicializar todas informa��es necess�rias para a tela de listar proposta
		inicializarTelaListarProposta();
		
		try {			
			propostas = propostaServiceFacade.consultarPropostaPorFiltro(filtroProposta, obterDadosCorretorLogado());
		}catch(EEDIBusinessException e){
			tratarErro(e);
			return ERROR;
		}catch(BusinessException e){
			LOGGER.error("Erro ao consultar");
			tratarErro(e);
			if(e.getMessage().isEmpty()){
				e.setMessage("Nenhuma proposta encontrada para o filtro informado.");
			}
			addActionError(e.getMessage());
			return ERROR;
		}catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	/**
	 * Metodo responsavel por visualizar uma proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String visualizarProposta(){
		
		try {
			inicializarTelaListarProposta();
			
			proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			
			try{
				List<TelefoneVO> listaDeTelefones = new ArrayList<TelefoneVO>();
				listaDeTelefones.addAll(proposta.getBeneficiarios().getTitular().getTelefones());
				proposta.getBeneficiarios().getTitular().setTelefones(new ArrayList<TelefoneVO>());
				proposta.getBeneficiarios().getTitular().getTelefones().add(new TelefoneVO(TipoTelefone.CELULAR));
				proposta.getBeneficiarios().getTitular().getTelefones().add(new TelefoneVO(TipoTelefone.RESIDENCIAL));
				proposta.getBeneficiarios().getTitular().getTelefones().add(new TelefoneVO(TipoTelefone.COMERCIAL));
				
				for(TelefoneVO telefone: listaDeTelefones){
					
					if(TipoTelefone.CELULAR.name().equals(telefone.getTipoTelefone().toUpperCase())){
						proposta.getBeneficiarios().getTitular().getTelefones().set(0, telefone);
					}else if(TipoTelefone.RESIDENCIAL.name().equals(telefone.getTipoTelefone().toUpperCase())){
						proposta.getBeneficiarios().getTitular().getTelefones().set(2, telefone);
					}else if(TipoTelefone.COMERCIAL.name().equals(telefone.getTipoTelefone().toUpperCase())){
						proposta.getBeneficiarios().getTitular().getTelefones().set(1, telefone);
					}
				}
			}catch(Exception e){
				LOGGER.error(String.format("Erro ao obter telefones da proposta [%s]", e.getMessage()));
			}
			
			indicativoDeCancelamentoDaProposta = verificarSePropostaPodeSerCancelada();
			calcularValorTotalDaProposta();
			inicializarTelaNovaProposta();
			codigoPlano = proposta.getPlano().getCodigo();
			//LOGGER.info("Codigo do plano: " + codigoPlano);
			ServletActionContext.getRequest().getSession().setAttribute("plano", codigoPlano);
		}catch(EEDIBusinessException e){
			tratarErro(e);
			return ERROR;
		}catch(BusinessException e){
			tratarErro(e);
			return ERROR;
		}catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	public String salvarRascunhoProposta() {

		try {
			
			if(!isCartaoCredito()){
				LOGGER.error("Inicio do metodo salvar rascunho de proposta");
				// inicializar todas informa��es necess�rias para a tela
				inicializarTelaNovaProposta();
				codigoPlano = (Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO);
				retirarFormatacaoCPFPagante();

				CorretorVO corretor = obterDadosCorretorLogado();
				proposta.getCorretor().setCpfCnpj(corretor.getCpfCnpj());
			}
			
			proposta.setSequencial(propostaServiceFacade.salvarRascunhoProposta(proposta));
			
			PropostaVO propostaTemporaria =  propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			if(propostaTemporaria != null){
				proposta.setCodigo(propostaTemporaria.getCodigo());
				preencheDadosCartaoCredito(propostaTemporaria);
			}
			
			proposta =  propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			converteDadosCartaoCredito(proposta, propostaTemporaria);
			
		/*	if(proposta.getCodigo() != null){
				
				//autor: Roberto William
				//deve ser colocado uma valida��o do cart�o de cr�dito antes de chamar o m�todo de cart�o do MIP
				if (isCartaoCredito()) {
					validarDadosCartaoCredito();
				}
				
				addActionMessage("Proposta N� " + FormatacaoUtil.formataCodigoDaProposta(proposta.getCodigo()) + " salva em rascunho com sucesso.");
				LOGGER.info(String.format("Proposta salva em rascunho: [%s]", proposta.getSequencial()));
			}*/

		} catch (EEDIBusinessException e) {
			LOGGER.error("Erro ao  salvar rascunho de proposta: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (BusinessException e) {
			LOGGER.error("Erro ao  salvar rascunho de proposta: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error("Erro ao  salvar rascunho de proposta: " + e.getMessage());
			addActionError(e.getMessage());
			return SUCCESS;
		}

		return SUCCESS;
		
	}
	
	public String validarDadosCartaoCredito(){		
		LOGGER.error( String.format("Validar dados de cartao de credito para proposta : [%s]" , proposta.getSequencial()));
		boolean envioAoMIPComSucesso = false;
		try{
			//Colocado por Roberto William
			//data: 08/06/2019
			if(proposta.getSequencial()!= null){
				
				envioAoMIPComSucesso = propostaServiceFacade.validarDadosCartaoCredito(proposta);
				
				if(envioAoMIPComSucesso == true){
					proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
					
					addActionMessage("Proposta N� " + codigoPropostaFormatado(proposta.getCodigo()) + " gerada com sucesso.");

				}else if(envioAoMIPComSucesso == false && StringUtils.isEmpty(proposta.getPagamento().getCartaoCredito().getPaymentToken())){
					addActionError(String.format("Cart�o N�o Autorizado - Erro ao validar os dados do cart�o de cr�dito para proposta [%s].", proposta.getSequencial()));
					proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
					LOGGER.error(String.format("Erro na valida��o dos dados do cart�o para proposta [%s].", proposta.getSequencial()));
					return SUCCESS;
				}

			}
		} catch(BusinessException e){
			LOGGER.error(String.format("Erro na valida��o dos dados do cart�o para proposta [%s]. [%s]", proposta.getSequencial(), e.getMessage()));
			proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
			propostaServiceFacade.cancelarProposta(proposta.getSequencial());
			addActionError(String.format("Cart�o N�o Autorizado - Erro ao validar os dados do cart�o de cr�dito para proposta [%s]. [%s]", proposta.getSequencial(), e.getMessage()));
			return SUCCESS;
		} catch(Exception e){
			LOGGER.error(String.format("Erro na valida��o dos dados do cart�o para proposta [%s]. [%s]", proposta.getSequencial(), e.getMessage()));
			proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
			propostaServiceFacade.cancelarProposta(proposta.getSequencial());
			addActionError(String.format("Cart�o N�o Autorizado - Erro ao validar os dados do cart�o de cr�dito para proposta [%s]. [%s]", proposta.getSequencial(), e.getMessage()));
			return SUCCESS;
		}
		
		return carregarProposta(proposta.getSequencial());
	}
	
	
	private boolean verificarSePropostaPodeSerCancelada() {
		boolean retorno = false;
		
		if(proposta.getStatus().contains("CANCELADA")){
			retorno = false;
		}
		else if(null != proposta.getDataEmissao() && new LocalDate().isBefore(proposta.getDataEmissao().plusDays(7))){
			retorno = true;
		}
		return retorno;
	}

	/**
	 * Metodo responsavel por cancelar uma proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String cancelarProposta() {
		try {

			propostaServiceFacade.cancelarProposta(proposta.getSequencial());
			proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			
			//chamar o m�todo do MIP(odontoprev) para realizar o estorno da 1� parcela creditada no cart�o
			if((proposta.getPagamento().getCartaoCredito() != null) && SituacaoProposta.CANCELADA.getNome().toUpperCase().equals(proposta.getStatus().toUpperCase())){
				odontoprevServiceFacade.cancelarDebitoPrimeiraParcelaCartaoDeCredito(proposta);
			}

		}catch(EEDIBusinessException e){
			tratarErro(e);
			return ERROR;
		}catch(BusinessException e){
			tratarErro(e);
			return ERROR;
		}catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		addActionMessage("Proposta N� " + codigoPropostaFormatado(proposta.getCodigo()) + " cancelada com sucesso.");

		return SUCCESS;
	}
	
	private String codigoPropostaFormatado(String codigo) {
		StringBuilder codigoProposta = new StringBuilder(codigo);
		codigoProposta.insert(codigo.length() - 1, '-');
		return codigoProposta.toString();
	}
	
	public String carregarProposta(Long sequencial){
		try {
			inicializarTelaListarProposta();
			proposta = propostaServiceFacade.obterPropostaPorSequencial(sequencial);
			
			indicativoDeCancelamentoDaProposta = verificarSePropostaPodeSerCancelada();
			calcularValorTotalDaPropostaNova();
			inicializarTelaNovaProposta();
			codigoPlano = proposta.getPlano().getCodigo();
			LOGGER.error("Codigo do plano: " + codigoPlano);
			ServletActionContext.getRequest().getSession().setAttribute("plano", codigoPlano);
			
		} catch (EEDIBusinessException e) {
			tratarErro(e);
			addActionError(e.getMessage());
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			addActionError(e.getMessage());
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}
	
	/**
	 * Metodo responsavel pela inicializa��o das informa��es na tela de listar proposta.
	 * 
	 */
	private void inicializarTelaListarProposta(){
		tiposBusca = TipoBusca.listar();
		tiposCPFs = TipoCPF.listar();
		status = SituacaoProposta.listar();
		
	}
	
	/**
	 * Metodo responsavel pela inicializa��o das informa��es na tela de nova proposta.
	 * 
	 * - Listar os planos vigentes para o canal CORRETORA MERCADO;
	 * - Calcular o valor total da proposta de acordo com o plano selecionado;
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void inicializarTelaNovaProposta(){
		if(planos == null){
			planos = planoServiceFacade.listarPlanosVigente();
			ServletActionContext.getRequest().getSession().setAttribute("planos", planos);
		}else{
			planos = (List<PlanoVO>) ServletActionContext.getRequest().getSession().getAttribute("planos");
		}
		//planos = new ArrayList<PlanoVO>();
		calcularValorTotalDaProposta();
		sexos = Sexo.listar();
		estadosCivis = EstadoCivil.listar();
		grausParentesco = GrauParentesco.listar();
		bancos = Banco.listar();
		tiposCobranca = TipoCobranca.listar();
		formasPagamento = new ArrayList<LabelValueVO>();
		//setFormasPagamento(new ArrayList<LabelValueVO>());
		tiposTelefone = TipoTelefone.listar();
		contasCorrente = (List<ContaCorrenteVO>) ServletActionContext.getRequest().getSession().getAttribute("contasCorrente");
		if(contasCorrente == null){
			contasCorrente = new ArrayList<ContaCorrenteVO>();
		}
		if (StringUtils.isNotBlank(proposta.getCodigo())) {
			try {
				tratarTokensCartaoCredito();
			} catch (Exception e) {
				LOGGER.error("Erro na gera��o do token do MIP.");
			}
		}
		/**
		 * desabilitar chamada do CorretorHabilitado
		 * data: 15/07/2019
		 * Autor: Roberto Freire
		 */
		corretorCartaoCreditoHabilitado = odontoprevServiceFacade.verificarCorretorHabilitado(proposta.getCorretor().getCpfCnpj());
	}
	
	private List<LabelValueVO> converterBandeirasParaEnum(List<String> listarBandeiras) {
		if(listarBandeiras == null || listarBandeiras.isEmpty()){
			return null;
		}
		List<LabelValueVO> listaBandeirasEnum = new ArrayList<LabelValueVO>();
		Integer codigoBandeira = 0;
		for(String bandeira : listarBandeiras){
			codigoBandeira++;
			listaBandeirasEnum.add(new LabelValueVO(bandeira, bandeira));
		}
		return listaBandeirasEnum;
	}
	
	public void tratarTokensCartaoCredito() {
		LOGGER.error("Tratar Token cartao de credito");
		if (isCartaoCredito()) {
			if(proposta.getPagamento().getCartaoCredito().getAccessToken() == null){
				proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
			}
			//proposta.getPagamento().getCartaoCredito().setAccessToken("token-mock");
			setBandeiras(converterBandeirasParaEnum(odontoprevServiceFacade.listarBandeiras()));
			LOGGER.error("AcessToken: "+ proposta.getPagamento().getCartaoCredito().getAccessToken());
		} else {
			proposta.getPagamento().getCartaoCredito().setAccessToken("");
			proposta.getPagamento().getCartaoCredito().setPaymentToken("");
		}
		
	}
	
	/**
	 * Preencher dados do Cartao de Credito
	 */
	private void preencheDadosCartaoCredito(PropostaVO propostaTemporaria){
		propostaTemporaria.getPagamento().getCartaoCredito().setAccessToken(this.proposta.getPagamento().getCartaoCredito().getAccessToken());
		propostaTemporaria.getPagamento().getCartaoCredito().setNomeCartao(this.proposta.getPagamento().getCartaoCredito().getNomeCartao());
		propostaTemporaria.getPagamento().getCartaoCredito().setNumeroCartao(this.proposta.getPagamento().getCartaoCredito().getNumeroCartao());
		propostaTemporaria.getPagamento().getCartaoCredito().setBandeira(this.proposta.getPagamento().getCartaoCredito().getBandeira());
		propostaTemporaria.getPagamento().getCartaoCredito().setCpfCartao(this.proposta.getPagamento().getCartaoCredito().getCpfCartao());
		propostaTemporaria.getPagamento().getCartaoCredito().setValidade(this.proposta.getPagamento().getCartaoCredito().getValidade());
		propostaTemporaria.getPagamento().getCartaoCredito().setDataNascimento(this.proposta.getPagamento().getCartaoCredito().getDataNascimento());
	}
	
	private void converteDadosCartaoCredito(PropostaVO propostaNova, PropostaVO propostaTela){
		propostaNova.getPagamento().getCartaoCredito().setAccessToken(propostaTela.getPagamento().getCartaoCredito().getAccessToken());
		propostaNova.getPagamento().getCartaoCredito().setNomeCartao(propostaTela.getPagamento().getCartaoCredito().getNomeCartao());
		propostaNova.getPagamento().getCartaoCredito().setNumeroCartao(propostaTela.getPagamento().getCartaoCredito().getNumeroCartao());
		propostaNova.getPagamento().getCartaoCredito().setBandeira(propostaTela.getPagamento().getCartaoCredito().getBandeira());
		propostaNova.getPagamento().getCartaoCredito().setCpfCartao(propostaTela.getPagamento().getCartaoCredito().getCpfCartao());
		propostaNova.getPagamento().getCartaoCredito().setValidade(propostaTela.getPagamento().getCartaoCredito().getValidade());
		propostaNova.getPagamento().getCartaoCredito().setDataNascimento(propostaTela.getPagamento().getCartaoCredito().getDataNascimento());
	}
	
	public String gerarAccessToken(){
		
		try{
			tratarTokensCartaoCredito();
		} catch (BusinessException e) {
			tratarErro(e);
			return SUCCESS;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return SUCCESS;
	}

	/**
	 * Metodo responsavel pelo calculo do valor total (mensal e anual) da proposta.
	 * 
	 * - Obter o plano selecionado em tela;
	 * - Calcular o valor total (mensal e anual) da proposta de acordo com o n�mero de vidas informado;
	 *  
	 */
	private void calcularValorTotalDaProposta() {
		obterPlanoSeleciondo();
		if(proposta.getSequencial() == null && (planos == null || planos.isEmpty() || proposta.getPlano().getCodigo() == null || proposta.getPlano().getCodigo().longValue() == 0)){
			valorTotalMensal = 0;
			valorTotalAnual = 0;
		}else{
			valorTotalMensal = proposta.getPlano().getValorPlanoVO().getValorMensalTitular() * (proposta.getBeneficiarios().getDependentes().size() + 1);
			valorTotalAnual = proposta.getPlano().getValorPlanoVO().getValorAnualTitular() * (proposta.getBeneficiarios().getDependentes().size() + 1);
		}
	}
	
	private void calcularValorTotalDaPropostaNova() {
		if (proposta.getPlano().getCodigo() == null || proposta.getPlano().getCodigo().longValue() == 0 ){
			valorTotalMensal = 0;
			valorTotalAnual = 0;
		}else if(numeroVidas != null && numeroVidas > 0){
			valorTotalMensal = proposta.getPlano().getValorPlanoVO().getValorMensalTitular() * (numeroVidas );
			valorTotalAnual = proposta.getPlano().getValorPlanoVO().getValorAnualTitular() * (numeroVidas );
		}
		else {
			valorTotalMensal = proposta.getPlano().getValorPlanoVO().getValorMensalTitular() * (proposta.getBeneficiarios().getDependentes().size() + 1);
			valorTotalAnual = proposta.getPlano().getValorPlanoVO().getValorAnualTitular() * (proposta.getBeneficiarios().getDependentes().size() + 1);
		}
	}
	
	/**
	 * Metodo responsavel por obter o plano selecionado em tela.
	 * 
	 * - Marcar o primeiro plano como o default;
	 * - Procurar o plano selecionado na lista de planos vigente.
	 */
	private void obterPlanoSeleciondo() {
		if(planos != null && !planos.isEmpty() && (proposta.getPlano().getCodigo() == null || proposta.getPlano().getCodigo().longValue() == 0)){
			proposta.setPlano(planos.get(0));
		}
		else if(planos != null && !planos.isEmpty()){
			for (PlanoVO plano : planos) {
				if(plano.getCodigo().equals(proposta.getPlano().getCodigo())){
					proposta.setPlano(plano);
					break;
				}
			}
		}
	}
	
	public String obterFormasPagamento() {
		setFormasPagamento(FormaPagamento.converterEnumEmLabel(FormaPagamento.listarFormaPagamento()));
		return SUCCESS;
	}

	/**
	 * Retorna proposta.
	 *
	 * @return proposta - proposta.
	 */
	public PropostaVO getProposta() {
		return proposta;
	}

	/**
	 * Especifica proposta.
	 *
	 * @param proposta - proposta.
	 */
	public void setProposta(PropostaVO proposta) {
		this.proposta = proposta;
	}

	/**
	 * Retorna numeroDependente.
	 *
	 * @return numeroDependente - numeroDependente.
	 */
	public int getNumeroDependente() {
		return numeroDependente;
	}

	/**
	 * Especifica numeroDependente.
	 *
	 * @param numeroDependente - numeroDependente.
	 */
	public void setNumeroDependente(int numeroDependente) {
		this.numeroDependente = numeroDependente;
	}

	/**
	 * Retorna planos.
	 *
	 * @return planos - planos.
	 */
	public List<PlanoVO> getPlanos() {
		return planos;
	}

	/**
	 * Especifica planos.
	 *
	 * @param planos - planos.
	 */
	public void setPlanos(List<PlanoVO> planos) {
		this.planos = planos;
	}

	/**
	 * Retorna valorTotalMensal.
	 *
	 * @return valorTotalMensal - valorTotalMensal.
	 */
	public double getValorTotalMensal() {
		return valorTotalMensal;
	}

	/**
	 * Especifica valorTotalMensal.
	 *
	 * @param valorTotalMensal - valorTotalMensal.
	 */
	public void setValorTotalMensal(double valorTotalMensal) {
		this.valorTotalMensal = valorTotalMensal;
	}

	/**
	 * Retorna valorTotalAnual.
	 *
	 * @return valorTotalAnual - valorTotalAnual.
	 */
	public double getValorTotalAnual() {
		return valorTotalAnual;
	}

	/**
	 * Especifica valorTotalAnual.
	 *
	 * @param valorTotalAnual - valorTotalAnual.
	 */
	public void setValorTotalAnual(double valorTotalAnual) {
		this.valorTotalAnual = valorTotalAnual;
	}

	/**
	 * Retorna cep.
	 *
	 * @return cep - cep.
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * Especifica cep.
	 *
	 * @param cep - cep.
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}

	/**
	 * Retorna indicadorCepDoTitular.
	 *
	 * @return indicadorCepDoTitular - indicadorCepDoTitular.
	 */
	public boolean isIndicadorCepDoTitular() {
		return indicadorCepDoTitular;
	}

	/**
	 * Especifica indicadorCepDoTitular.
	 *
	 * @param indicadorCepDoTitular - indicadorCepDoTitular.
	 */
	public void setIndicadorCepDoTitular(boolean indicadorCepDoTitular) {
		this.indicadorCepDoTitular = indicadorCepDoTitular;
	}

	/**
	 * Retorna contasCorrente.
	 *
	 * @return contasCorrente - contasCorrente.
	 */
	public List<ContaCorrenteVO> getContasCorrente() {
		return contasCorrente;
	}

	/**
	 * Especifica contasCorrente.
	 *
	 * @param contasCorrente - contasCorrente.
	 */
	public void setContasCorrente(List<ContaCorrenteVO> contasCorrente) {
		this.contasCorrente = contasCorrente;
	}

	/**
	 * Retorna contaCorrenteFormatada.
	 *
	 * @return contaCorrenteFormatada - contaCorrenteFormatada.
	 */
	public String getContaCorrenteFormatada() {
		return contaCorrenteFormatada;
	}

	/**
	 * Especifica contaCorrenteFormatada.
	 *
	 * @param contaCorrenteFormatada - contaCorrenteFormatada.
	 */
	public void setContaCorrenteFormatada(String contaCorrenteFormatada) {
		this.contaCorrenteFormatada = contaCorrenteFormatada;
	}

	/**
	 * Retorna sexos.
	 *
	 * @return sexos - sexos.
	 */
	public List<Sexo> getSexos() {
		return sexos;
	}

	/**
	 * Especifica sexos.
	 *
	 * @param sexos - sexos.
	 */
	public void setSexos(List<Sexo> sexos) {
		this.sexos = sexos;
	}

	/**
	 * Retorna estadosCivis.
	 *
	 * @return estadosCivis - estadosCivis.
	 */
	public List<EstadoCivil> getEstadosCivis() {
		return estadosCivis;
	}

	/**
	 * Especifica estadosCivis.
	 *
	 * @param estadosCivis - estadosCivis.
	 */
	public void setEstadosCivis(List<EstadoCivil> estadosCivis) {
		this.estadosCivis = estadosCivis;
	}

	/**
	 * Retorna grausParentesco.
	 *
	 * @return grausParentesco - grausParentesco.
	 */
	public List<GrauParentesco> getGrausParentesco() {
		return grausParentesco;
	}

	/**
	 * Especifica grausParentesco.
	 *
	 * @param grausParentesco - grausParentesco.
	 */
	public void setGrausParentesco(List<GrauParentesco> grausParentesco) {
		this.grausParentesco = grausParentesco;
	}

	/**
	 * Retorna bancos.
	 *
	 * @return bancos - bancos.
	 */
	public List<Banco> getBancos() {
		return bancos;
	}

	/**
	 * Especifica bancos.
	 *
	 * @param bancos - bancos.
	 */
	public void setBancos(List<Banco> bancos) {
		this.bancos = bancos;
	}

	/**
	 * Retorna tiposCobranca.
	 *
	 * @return tiposCobranca - tiposCobranca.
	 */
	public List<TipoCobranca> getTiposCobranca() {
		return tiposCobranca;
	}

	/**
	 * Especifica tiposCobranca.
	 *
	 * @param tiposCobranca - tiposCobranca.
	 */
	public void setTiposCobranca(List<TipoCobranca> tiposCobranca) {
		this.tiposCobranca = tiposCobranca;
	}

	/**
	 * Retorna formasPagamento.
	 *
	 * @return formasPagamento - formasPagamento.
	 */


	/**
	 * Retorna tiposTelefone.
	 *
	 * @return tiposTelefone - tiposTelefone.
	 */
	public List<TipoTelefone> getTiposTelefone() {
		return tiposTelefone;
	}

	/**
	 * Especifica tiposTelefone.
	 *
	 * @param tiposTelefone - tiposTelefone.
	 */
	public void setTiposTelefone(List<TipoTelefone> tiposTelefone) {
		this.tiposTelefone = tiposTelefone;
	}

	/**
	 * Retorna tiposBusca.
	 *
	 * @return tiposBusca - tiposBusca.
	 */
	public List<TipoBusca> getTiposBusca() {
		return tiposBusca;
	}

	/**
	 * Especifica tiposBusca.
	 *
	 * @param tiposBusca - tiposBusca.
	 */
	public void setTiposBusca(List<TipoBusca> tiposBusca) {
		this.tiposBusca = tiposBusca;
	}

	/**
	 * Retorna filtroProposta.
	 *
	 * @return filtroProposta - filtroProposta.
	 */
	public FiltroPropostaVO getFiltroProposta() {
		return filtroProposta;
	}

	/**
	 * Especifica filtroProposta.
	 *
	 * @param filtroProposta - filtroProposta.
	 */
	public void setFiltroProposta(FiltroPropostaVO filtroProposta) {
		this.filtroProposta = filtroProposta;
	}

	/**
	 * Retorna tiposCPFs.
	 *
	 * @return tiposCPFs - tiposCPFs.
	 */
	public List<TipoCPF> getTiposCPFs() {
		return tiposCPFs;
	}

	/**
	 * Especifica tiposCPFs.
	 *
	 * @param tiposCPFs - tiposCPFs.
	 */
	public void setTiposCPFs(List<TipoCPF> tiposCPFs) {
		this.tiposCPFs = tiposCPFs;
	}

	/**
	 * Retorna status.
	 *
	 * @return status - status.
	 */
	public List<SituacaoProposta> getStatus() {
		return status;
	}

	/**
	 * Especifica status.
	 *
	 * @param status - status.
	 */
	public void setStatus(List<SituacaoProposta> status) {
		this.status = status;
	}

	/**
	 * Retorna propostas.
	 *
	 * @return propostas - propostas.
	 */
	public List<PropostaVO> getPropostas() {
		return propostas;
	}

	/**
	 * Especifica propostas.
	 *
	 * @param propostas - propostas.
	 */
	public void setPropostas(List<PropostaVO> propostas) {
		this.propostas = propostas;
	}

	/**
	 * Retorna indicativoDeCancelamentoDaProposta.
	 *
	 * @return indicativoDeCancelamentoDaProposta - indicativoDeCancelamentoDaProposta
	 */
	public boolean isIndicativoDeCancelamentoDaProposta() {
		return indicativoDeCancelamentoDaProposta;
	}

	/**
	 * Especifica indicativoDeCancelamentoDaProposta.
	 *
	 * @param indicativoDeCancelamentoDaProposta - indicativoDeCancelamentoDaProposta
	 */
	public void setIndicativoDeCancelamentoDaProposta(boolean indicativoDeCancelamentoDaProposta) {
		this.indicativoDeCancelamentoDaProposta = indicativoDeCancelamentoDaProposta;
	}

	public List<LabelValueVO> getBandeiras() {
		return bandeiras;
	}

	public void setBandeiras(List<LabelValueVO> bandeiras) {
		this.bandeiras = bandeiras;
	}

	public boolean isCorretorCartaoCreditoHabilitado() {
		return corretorCartaoCreditoHabilitado;
	}

	public void setCorretorCartaoCreditoHabilitado(boolean corretorCartaoCreditoHabilitado) {
		this.corretorCartaoCreditoHabilitado = corretorCartaoCreditoHabilitado;
	}

	public List<LabelValueVO> getFormasPagamento() {
		return formasPagamento;
	}

	public void setFormasPagamento(List<LabelValueVO> formasPagamento) {
		this.formasPagamento = formasPagamento;
	}

	public Long getCodigoPlano() {
		return codigoPlano;
	}

	public void setCodigoPlano(Long codigoPlano) {
		this.codigoPlano = codigoPlano;
	}

	public String getNomePagante() {
		return nomePagante;
	}

	public void setNomePagante(String nomePagante) {
		this.nomePagante = nomePagante;
	}

	public Integer getNumeroVidas() {
		return numeroVidas;
	}

	public void setNumeroVidas(Integer numeroVidas) {
		this.numeroVidas = numeroVidas;
	}

}
