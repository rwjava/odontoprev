package br.com.bradseg.eedi.novaemissaoexpressadi.proposta.action;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.eedi.novaemissaoexpressadi.exception.EEDIBusinessException;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.CorretorVO;

import com.google.common.base.Strings;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe base responsavel pela apresenta��o das informa��es.
 *
 */
public class BaseAction extends ActionSupport {

	private static final long serialVersionUID = 4086490879004687443L;

	//private static final Logger LOGGER = LoggerFactory.getLogger(BaseAction.class);

	/**
	 * Metodo responsavel por tratar os erros oriundos de servi�os e apresenta-los como erros do sistema.
	 * 
	 * @param exception - excess�o disparada por um determinado servi�o.
	 */
	public void tratarErro(Exception exception) {
		if (exception instanceof EEDIBusinessException) {
			EEDIBusinessException eediBusinessException = (EEDIBusinessException) exception;
			adicionarErros(eediBusinessException.getLista());
		} else if (exception instanceof BusinessException) {
			BusinessException businessException = (BusinessException) exception;
			adicionarErros(businessException.getMessages());
		}
	}

	/**
	 * Metodo responsavel por adicionar os erros na pagina.
	 * 
	 * @param messages - lista com os erros a serem apresentandos.
	 */
	private void adicionarErros(List<Message> messages) {
		if (!messages.isEmpty()) {
			for (Message message : messages) {
				addActionError(message.getMessage());
			}
		}
	}

	/**
	 * Metodo responsavel por obter os dados do corretor logado no sistema.
	 * 
	 * @return CorretorVO - informa��es do corretor logado.
	 */
	public CorretorVO obterDadosCorretorLogado() {
		CorretorVO corretor = new CorretorVO();

		HttpSession session = ServletActionContext.getRequest().getSession();
		CorretorVO corretorLogado = (CorretorVO) session.getAttribute("corretor_logado");
		
		if (null == corretorLogado || corretorLogado.getProdutor() == null || StringUtils.isBlank(corretorLogado.getProdutor().getCpfCnpj())) {
			corretor.getProdutor().setCpfCnpj(ServletActionContext.getRequest().getParameter("cd_cgccpf"));
			corretor.getProdutor().setNome(ServletActionContext.getRequest().getParameter("nome"));
			corretor.setCpfCnpj(ServletActionContext.getRequest().getParameter("chave"));
			String cpd = ServletActionContext.getRequest().getParameter("cd_corretor");
			if (!Strings.isNullOrEmpty(cpd)) {
				corretor.setCpd(Long.valueOf(cpd));
			}
			String sucursal = ServletActionContext.getRequest().getParameter("cd_suc");
			if (!Strings.isNullOrEmpty(sucursal)) {
				corretor.setSucursal(Integer.valueOf(sucursal));
			}
			session.setAttribute("corretor_logado", corretor);
		} else {						
			corretor = corretorLogado;
			corretor.setCpfCnpj(ServletActionContext.getRequest().getParameter("proposta.corretor.produtor.cpfCnpj"));
		}

		return corretor;
	}

}
