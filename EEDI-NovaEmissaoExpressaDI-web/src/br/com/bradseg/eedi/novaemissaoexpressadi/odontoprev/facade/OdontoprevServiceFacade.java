package br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.facade;


import java.util.List;

import br.com.bradseg.eedi.novaemissaoexpressadi.odontoprev.vo.RetornoVO;
import br.com.bradseg.eedi.novaemissaoexpressadi.vo.PropostaVO;

/**
 * Interface responsavel por disponibilizar os metodos de neg�cio referentes a odontoprev.
 */
public interface OdontoprevServiceFacade {

	/**
	 * Metodo responsavel por listar as bandeiras para pagamento com cart�o de cr�dito.
	 * 
	 * @return List<String> - lista de bandeiras.
	 */
	public List<String> listarBandeiras();

	/**
	 * Metodo responsavel por obter o access token para pagamento com cart�o de cr�dito
	 * 
	 * @return String - access token.
	 */
	public String obterAccessToken();

	public RetornoVO debitarPrimeiraParcelaCartaoDeCredito(PropostaVO proposta);
	
	public RetornoVO validarDadosCartaoCredito(PropostaVO proposta);

	public void cancelarDebitoPrimeiraParcelaCartaoDeCredito(PropostaVO proposta);
	
	/**
	 * desabilitar chamada do CorretorHabilitado
	 * data: 15/07/2019
	 * Autor: Roberto Freire
	 */
	public boolean verificarCorretorHabilitado(String numeroCorretor);
	
	/**
	 * Metodo responsavel por obter o paymentToken para pagamento com cart�o de cr�dito
	 * 
	 * @return String - payment token.
	 */
	public String obterPaymentToken(PropostaVO proposta);
	
	public RetornoVO consultarDadosCartaoCredito(PropostaVO proposta);
}
