package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

/**
 * Classe responsavel pelo transporte de dados do pagamento.
 *
 */
public class PagamentoVO implements Serializable {

	private static final long serialVersionUID = 232137961041195502L;
	
	private String tipoCobranca;
	private String formaPagamento;
	private ContaCorrenteVO contaCorrente;
	private LocalDate dataCobranca;
	private CartaoCreditoVO cartaoCredito;
	private Integer codigoTipoCobranca;
	
	/**
	 * Construtor padr�o.
	 */
	public PagamentoVO(){
		contaCorrente = new ContaCorrenteVO();
		//formaPagamento = String.valueOf(FormaPagamento.DEBITO_AUTOMATICO.getCodigo());
		setCartaoCredito(new CartaoCreditoVO());
	}

	/**
	 * Retorna tipoCobranca.
	 *
	 * @return tipoCobranca - tipoCobranca.
	 */
	public String getTipoCobranca() {
		return tipoCobranca;
	}

	/**
	 * Especifica tipoCobranca.
	 *
	 * @param tipoCobranca - tipoCobranca.
	 */
	public void setTipoCobranca(String tipoCobranca) {
		this.tipoCobranca = tipoCobranca;
	}

	/**
	 * Retorna formaPagamento.
	 *
	 * @return formaPagamento - formaPagamento.
	 */
	public String getFormaPagamento() {
		return formaPagamento;
	}



	/**
	 * Especifica formaPagamento.
	 *
	 * @param formaPagamento - formaPagamento.
	 */
	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}



	/**
	 * Retorna contaCorrente.
	 *
	 * @return contaCorrente - contaCorrente.
	 */
	public ContaCorrenteVO getContaCorrente() {
		return contaCorrente;
	}

	/**
	 * Especifica contaCorrente.
	 *
	 * @param contaCorrente - contaCorrente.
	 */
	public void setContaCorrente(ContaCorrenteVO contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	/**
	 * Retorna dataCobranca.
	 *
	 * @return dataCobranca - dataCobranca.
	 */
	public LocalDate getDataCobranca() {
		return dataCobranca;
	}

	/**
	 * Especifica dataCobranca.
	 *
	 * @param dataCobranca - dataCobranca.
	 */
	public void setDataCobranca(LocalDate dataCobranca) {
		this.dataCobranca = dataCobranca;
	}

	public CartaoCreditoVO getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCreditoVO cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public Integer getCodigoTipoCobranca() {
		return codigoTipoCobranca;
	}

	public void setCodigoTipoCobranca(Integer codigoTipoCobranca) {
		this.codigoTipoCobranca = codigoTipoCobranca;
	}
	
}
