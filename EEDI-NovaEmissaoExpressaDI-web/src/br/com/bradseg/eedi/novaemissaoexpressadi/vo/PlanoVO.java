package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CanalVendaVO;

public class PlanoVO implements Serializable {
	
	private static final long serialVersionUID = 9048822434809918376L;
	
	protected Long codigo;
    protected Long codigoRegistro;
    protected String codigoResponsavel;
    protected Long codigoResponsavelUltimaAtualizacao;
    protected DateTime dataFimVigencia;
    protected DateTime dataInicioVigencia;
    protected LocalDate dataUltimaAtualizacao;
    protected String descricaoCarenciaPeriodoAnual;
    protected String descricaoCarenciaPeriodoMensal;
    protected List<CanalVendaVO> listaDeCanais;
    protected String nome;
    protected ValorPlanoVO valorPlanoVO;

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigo(Long value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the codigoRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoRegistro() {
        return codigoRegistro;
    }

    /**
     * Sets the value of the codigoRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoRegistro(Long value) {
        this.codigoRegistro = value;
    }

    /**
     * Gets the value of the codigoResponsavel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoResponsavel() {
        return codigoResponsavel;
    }

    /**
     * Sets the value of the codigoResponsavel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoResponsavel(String value) {
        this.codigoResponsavel = value;
    }

    /**
     * Gets the value of the codigoResponsavelUltimaAtualizacao property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoResponsavelUltimaAtualizacao() {
        return codigoResponsavelUltimaAtualizacao;
    }

    /**
     * Sets the value of the codigoResponsavelUltimaAtualizacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoResponsavelUltimaAtualizacao(Long value) {
        this.codigoResponsavelUltimaAtualizacao = value;
    }

    /**
     * Gets the value of the dataFimVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link DateTime }
     *     
     */
    public DateTime getDataFimVigencia() {
        return dataFimVigencia;
    }

    /**
     * Sets the value of the dataFimVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTime }
     *     
     */
    public void setDataFimVigencia(DateTime value) {
        this.dataFimVigencia = value;
    }

    /**
     * Gets the value of the dataInicioVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link DateTime }
     *     
     */
    public DateTime getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    /**
     * Sets the value of the dataInicioVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTime }
     *     
     */
    public void setDataInicioVigencia(DateTime value) {
        this.dataInicioVigencia = value;
    }

    /**
     * Gets the value of the dataUltimaAtualizacao property.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getDataUltimaAtualizacao() {
        return dataUltimaAtualizacao;
    }

    /**
     * Sets the value of the dataUltimaAtualizacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setDataUltimaAtualizacao(LocalDate value) {
        this.dataUltimaAtualizacao = value;
    }

    /**
     * Gets the value of the descricaoCarenciaPeriodoAnual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoCarenciaPeriodoAnual() {
        return descricaoCarenciaPeriodoAnual;
    }

    /**
     * Sets the value of the descricaoCarenciaPeriodoAnual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoCarenciaPeriodoAnual(String value) {
        this.descricaoCarenciaPeriodoAnual = value;
    }

    /**
     * Gets the value of the descricaoCarenciaPeriodoMensal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoCarenciaPeriodoMensal() {
        return descricaoCarenciaPeriodoMensal;
    }

    /**
     * Sets the value of the descricaoCarenciaPeriodoMensal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoCarenciaPeriodoMensal(String value) {
        this.descricaoCarenciaPeriodoMensal = value;
    }

    /**
     * Gets the value of the listaDeCanais property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaDeCanais property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaDeCanais().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CanalVendaVO }
     * 
     * 
     */
    public List<CanalVendaVO> getListaDeCanais() {
        if (listaDeCanais == null) {
            listaDeCanais = new ArrayList<CanalVendaVO>();
        }
        return this.listaDeCanais;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the valorPlanoVO property.
     * 
     * @return
     *     possible object is
     *     {@link ValorPlanoVO }
     *     
     */
    public ValorPlanoVO getValorPlanoVO() {
        return valorPlanoVO;
    }

    /**
     * Sets the value of the valorPlanoVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValorPlanoVO }
     *     
     */
    public void setValorPlanoVO(ValorPlanoVO value) {
        this.valorPlanoVO = value;
    }

}
