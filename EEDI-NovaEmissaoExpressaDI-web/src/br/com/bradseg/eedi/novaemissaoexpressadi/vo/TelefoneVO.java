package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;

import br.com.bradseg.eedi.novaemissaoexpressadi.util.Strings;

/**
 * Classe responsavel por transportar os dados do telefone.
 *
 */
public class TelefoneVO implements Serializable {

	private static final long serialVersionUID = -695965863326392889L;
	
	private String tipoTelefone;
	private String ramal;
	private String dddEnumero;
	
	/**
	 * Construtor padr�o.
	 */
	public TelefoneVO(){
		super();
	}
	
	/**
	 * Construtor alternativo.
	 * 
	 * @param tipoTelefone - tipo de telefone.
	 */
	public TelefoneVO(TipoTelefone tipoTelefone){
		this.tipoTelefone = String.valueOf(tipoTelefone.getCodigo());
	}
	
	/**
	 * Retorna tipoTelefone.
	 *
	 * @return tipoTelefone - tipoTelefone.
	 */
	public String getTipoTelefone() {
		return tipoTelefone;
	}

	/**
	 * Especifica tipoTelefone.
	 *
	 * @param tipoTelefone - tipoTelefone.
	 */
	public void setTipoTelefone(String tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}

	/**
	 * Retorna ddd.
	 *
	 * @return ddd - ddd.
	 */
	public String getDdd() {
		return Strings.normalizarTelefone(getDddEnumero()).substring(0, 2);
	}
	/**
	 * Retorna numero.
	 *
	 * @return numero - numero.
	 */
	public String getNumero() {
		return Strings.normalizarTelefone(getDddEnumero()).substring(2, Strings.normalizarTelefone(getDddEnumero()).length());
	}
	/**
	 * Retorna ramal.
	 *
	 * @return ramal - ramal.
	 */
	public String getRamal() {
		return ramal;
	}
	/**
	 * Especifica ramal.
	 *
	 * @param ramal - ramal.
	 */
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	/**
	 * Retorna dddEnumero.
	 *
	 * @return dddEnumero - dddEnumero.
	 */
	public String getDddEnumero() {
		return dddEnumero;
	}

	/**
	 * Especifica dddEnumero.
	 *
	 * @param dddEnumero - dddEnumero.
	 */
	public void setDddEnumero(String dddEnumero) {
		this.dddEnumero = dddEnumero;
	}
}
