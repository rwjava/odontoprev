package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

/**
 * Classe responsavel por transportar os dados do representante legal.
 *
 */
public class RepresentanteLegalVO extends PessoaVO {

	private static final long serialVersionUID = -7419805670786007037L;

	/**
	 * Construtor padr�o.
	 */
	public RepresentanteLegalVO(){
		getTelefones().add(new TelefoneVO());
	}
}
