package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;




/**
 * Classe responsavel pelo transporte de dados da proposta.
 *
 */
public class PropostaVO implements Serializable {

	private static final long serialVersionUID = 8739020366500747731L;
	
	private Long sequencial;
	private String codigo;
	private CorretorVO corretor;
	private PlanoVO plano;
	private BeneficiariosVO beneficiarios;
	private PagamentoVO pagamento;
	private String status;
	private CanalVO canal;
	private LocalDate dataEmissao;
	private boolean indicadorCorretorLogado;
	private List<DependenteVO> dependentes;
	
	/**
	 * Construtor padr�o.
	 */
	public PropostaVO(){
		beneficiarios = new BeneficiariosVO();
		pagamento = new PagamentoVO();
		plano = new PlanoVO();
		plano.setValorPlanoVO(new ValorPlanoVO());
		canal = new CanalVO();
		corretor = new CorretorVO();
		dependentes = new ArrayList<DependenteVO>();
	}

	/**
	 * Retorna sequencial.
	 *
	 * @return sequencial - sequencial.
	 */
	public Long getSequencial() {
		return sequencial;
	}

	/**
	 * Especifica sequencial.
	 *
	 * @param sequencial - sequencial.
	 */
	public void setSequencial(Long sequencial) {
		this.sequencial = sequencial;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Retorna plano.
	 *
	 * @return plano - plano.
	 */
	public PlanoVO getPlano() {
		return plano;
	}

	/**
	 * Especifica plano.
	 *
	 * @param plano - plano.
	 */
	public void setPlano(PlanoVO plano) {
		this.plano = plano;
	}

	/**
	 * Retorna beneficiarios.
	 *
	 * @return beneficiarios - beneficiarios.
	 */
	public BeneficiariosVO getBeneficiarios() {
		return beneficiarios;
	}

	/**
	 * Especifica beneficiarios.
	 *
	 * @param beneficiarios - beneficiarios.
	 */
	public void setBeneficiarios(BeneficiariosVO beneficiarios) {
		this.beneficiarios = beneficiarios;
	}

	/**
	 * Retorna corretor.
	 *
	 * @return corretor - corretor
	 */
	public CorretorVO getCorretor() {
		return corretor;
	}

	/**
	 * Especifica corretor.
	 *
	 * @param corretor - corretor
	 */
	public void setCorretor(CorretorVO corretor) {
		this.corretor = corretor;
	}

	/**
	 * Retorna pagamento.
	 *
	 * @return pagamento - pagamento.
	 */
	public PagamentoVO getPagamento() {
		return pagamento;
	}

	/**
	 * Especifica pagamento.
	 *
	 * @param pagamento - pagamento.
	 */
	public void setPagamento(PagamentoVO pagamento) {
		this.pagamento = pagamento;
	}

	/**
	 * Retorna status.
	 *
	 * @return status - status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Especifica status.
	 *
	 * @param status - status.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Retorna canal.
	 *
	 * @return canal - canal
	 */
	public CanalVO getCanal() {
		return canal;
	}

	/**
	 * Especifica canal.
	 *
	 * @param canal - canal
	 */
	public void setCanal(CanalVO canal) {
		this.canal = canal;
	}

	/**
	 * Retorna dataEmissao.
	 *
	 * @return dataEmissao - dataEmissao
	 */
	public LocalDate getDataEmissao() {
		return dataEmissao;
	}

	/**
	 * Especifica dataEmissao.
	 *
	 * @param dataEmissao - dataEmissao
	 */
	public void setDataEmissao(LocalDate dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public boolean isIndicadorCorretorLogado() {
		return indicadorCorretorLogado;
	}

	public void setIndicadorCorretorLogado(boolean indicadorCorretorLogado) {
		this.indicadorCorretorLogado = indicadorCorretorLogado;
	}

	public List<DependenteVO> getDependentes() {
		return dependentes;
	}

	public void setDependentes(List<DependenteVO> dependentes) {
		this.dependentes = dependentes;
	}
	
}
