package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar os estados civis.
 *
 */
public enum EstadoCivil {
	
	CASADO(1, "Casado"), DIVORCIADO(3, "Divorciado"), SEPARADO(5, "Separado"), SOLTEIRO(6, "Solteiro"), VIUVO(7,"Vi�vo");

	private int codigo;
	private String nome;
	
	private EstadoCivil(int codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Metodo responsavel por listar os estados civis.
	 * 
	 * @return List<EstadoCivil> - lista de estado civil.
	 */
	public static List<EstadoCivil> listar(){
		List<EstadoCivil> lista = new ArrayList<EstadoCivil>();
		for (EstadoCivil estadoCivil : EstadoCivil.values()) {
			lista.add(estadoCivil);
		}
		return lista;
	}
	
	public static EstadoCivil obterEstadoCivilPorCodigo(int codigo){
		for (EstadoCivil estadoCivil : EstadoCivil.values()) {
			if(codigo == estadoCivil.getCodigo()){
				return estadoCivil;
			}
		}
		return EstadoCivil.SOLTEIRO;
	}
}
