package br.com.bradseg.eedi.novaemissaoexpressadi.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar os tipos de cpfs.
 *
 */
public enum TipoCPF {

	BENEFICIARIO(1, "Beneficiário"), PROPONENTE(2, "Proponente");

	private Integer codigo;
	private String nome;

	private TipoCPF(Integer codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Metodo responsavel por listar os tipos de cpfs.
	 * 
	 * @return List<TipoCPF> - lista de tipos de cpfs.
	 */
	public static List<TipoCPF> listar(){
		List<TipoCPF> lista = new ArrayList<TipoCPF>();
		for (TipoCPF tipoCPF : TipoCPF.values()) {
			lista.add(tipoCPF);
		}
		return lista;
	}
	
	public static TipoCPF obterPorCodigo(Integer codigo) {
		for (TipoCPF tipo : TipoCPF.values()) {
			if (tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}
		return TipoCPF.BENEFICIARIO;
	}

}
