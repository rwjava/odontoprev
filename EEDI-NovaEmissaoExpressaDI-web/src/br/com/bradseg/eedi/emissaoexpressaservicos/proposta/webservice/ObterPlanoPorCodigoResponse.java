
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for obterPlanoPorCodigoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="obterPlanoPorCodigoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="plano" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}planoVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obterPlanoPorCodigoResponse", propOrder = {
    "plano"
})
public class ObterPlanoPorCodigoResponse {

    protected PlanoVO plano;

    /**
     * Gets the value of the plano property.
     * 
     * @return
     *     possible object is
     *     {@link PlanoVO }
     *     
     */
    public PlanoVO getPlano() {
        return plano;
    }

    /**
     * Sets the value of the plano property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanoVO }
     *     
     */
    public void setPlano(PlanoVO value) {
        this.plano = value;
    }

}
