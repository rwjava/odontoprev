
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for emitirPropostaResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="emitirPropostaResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bytesArquivoRelatorio" type="{http" + "://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "emitirPropostaResponse", propOrder = {
    "bytesArquivoRelatorio"
})
public class EmitirPropostaResponse {

    protected byte[] bytesArquivoRelatorio;

    /**
     * Gets the value of the bytesArquivoRelatorio property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBytesArquivoRelatorio() {
        return bytesArquivoRelatorio;
    }

    /**
     * Sets the value of the bytesArquivoRelatorio property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBytesArquivoRelatorio(byte[] value) {
        this.bytesArquivoRelatorio = value;
    }

}
