
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for grauParentesco.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="grauParentesco">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PAI"/>
 *     &lt;enumeration value="MAE"/>
 *     &lt;enumeration value="FILHA"/>
 *     &lt;enumeration value="FILHO"/>
 *     &lt;enumeration value="CONJUGE"/>
 *     &lt;enumeration value="PROPRIO"/>
 *      &lt;enumeration value="OUTRO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "grauParentesco")
@XmlEnum
public enum GrauParentesco {

	PAI,
	MAE,
	FILHA,
    FILHO,
    CONJUGE,
    PROPRIO,
    OUTRO;

    public String value() {
        return name();
    }

    public static GrauParentesco fromValue(String v) {
        return valueOf(v);
    }

}
