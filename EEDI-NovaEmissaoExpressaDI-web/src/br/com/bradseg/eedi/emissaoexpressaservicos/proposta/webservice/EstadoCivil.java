
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for estadoCivil.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="estadoCivil">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CASADO"/>
 *     &lt;enumeration value="DIVORCIADO"/>
 *     &lt;enumeration value="SEPARADO"/>
 *     &lt;enumeration value="SOLTEIRO"/>
 *     &lt;enumeration value="VIUVO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "estadoCivil")
@XmlEnum
public enum EstadoCivil {

    CASADO,
    DIVORCIADO,
    SEPARADO,
    SOLTEIRO,
    VIUVO;

    public String value() {
        return name();
    }

    public static EstadoCivil fromValue(String v) {
        return valueOf(v);
    }

}
