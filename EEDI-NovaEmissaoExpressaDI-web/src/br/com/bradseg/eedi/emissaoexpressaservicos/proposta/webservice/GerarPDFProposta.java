
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gerarPDFProposta complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gerarPDFProposta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoProposta" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="gerarViaOperadora" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="gerarViaCorretor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="gerarBoleto" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gerarPDFProposta", propOrder = {
    "codigoProposta",
    "gerarViaOperadora",
    "gerarViaCorretor",
    "gerarBoleto"
})
public class GerarPDFProposta {

    protected Long codigoProposta;
    protected boolean gerarViaOperadora;
    protected boolean gerarViaCorretor;
    protected boolean gerarBoleto;

    /**
     * Gets the value of the codigoProposta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoProposta() {
        return codigoProposta;
    }

    /**
     * Sets the value of the codigoProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoProposta(Long value) {
        this.codigoProposta = value;
    }

    /**
     * Gets the value of the gerarViaOperadora property.
     * 
     */
    public boolean isGerarViaOperadora() {
        return gerarViaOperadora;
    }

    /**
     * Sets the value of the gerarViaOperadora property.
     * 
     */
    public void setGerarViaOperadora(boolean value) {
        this.gerarViaOperadora = value;
    }

    /**
     * Gets the value of the gerarViaCorretor property.
     * 
     */
    public boolean isGerarViaCorretor() {
        return gerarViaCorretor;
    }

    /**
     * Sets the value of the gerarViaCorretor property.
     * 
     */
    public void setGerarViaCorretor(boolean value) {
        this.gerarViaCorretor = value;
    }

    /**
     * Gets the value of the gerarBoleto property.
     * 
     */
    public boolean isGerarBoleto() {
        return gerarBoleto;
    }

    /**
     * Sets the value of the gerarBoleto property.
     * 
     */
    public void setGerarBoleto(boolean value) {
        this.gerarBoleto = value;
    }

}
