
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GerarPDFProposta_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "gerarPDFProposta");
    private final static QName _ObterPropostaPorCodigoResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterPropostaPorCodigoResponse");
    private final static QName _GerarBoletoResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "gerarBoletoResponse");
    private final static QName _CancelarPropostaPorProtocolo_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "cancelarPropostaPorProtocolo");
    private final static QName _ObterCanalVendaCallCenter_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterCanalVendaCallCenter");
    private final static QName _GerarPDFPropostaResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "gerarPDFPropostaResponse");
    private final static QName _SalvarRascunhoResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "salvarRascunhoResponse");
    private final static QName _CancelarPropostaShoppingSeguros_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "cancelarPropostaShoppingSeguros");
    private final static QName _CancelarPropostaShoppingSegurosResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "cancelarPropostaShoppingSegurosResponse");
    private final static QName _BusinessException_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "BusinessException");
    private final static QName _SalvarRascunho_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "salvarRascunho");
    private final static QName _ListarPlanosVigentePorCanal_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarPlanosVigentePorCanal");
    private final static QName _ObterPorCodigoResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterPorCodigoResponse");
    private final static QName _ObterPropostaPorCodigo_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterPropostaPorCodigo");
    private final static QName _CancelarPropostaPorProtocoloResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "cancelarPropostaPorProtocoloResponse");
    private final static QName _FinalizarResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "finalizarResponse");
    private final static QName _GerarRelatorioAcompanhamento_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "gerarRelatorioAcompanhamento");
    private final static QName _Cancelar_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "cancelar");
    private final static QName _ObterCanalVendaCallCenterResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterCanalVendaCallCenterResponse");
    private final static QName _IntegrationException_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "IntegrationException");
    private final static QName _ListarPorFiltroResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarPorFiltroResponse");
    private final static QName _AtualizarStatusDaPropostaNoShoppingDeSeguros_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "atualizarStatusDaPropostaNoShoppingDeSeguros");
    private final static QName _ListarPropostasPorProponente_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarPropostasPorProponente");
    private final static QName _ObterSituacaoProposta_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterSituacaoProposta");
    private final static QName _ObterPlanoPorCodigo_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterPlanoPorCodigo");
    private final static QName _AtualizarStatusDaPropostaNoShoppingDeSegurosResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "atualizarStatusDaPropostaNoShoppingDeSegurosResponse");
    private final static QName _ObterPorCodigo_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterPorCodigo");
    private final static QName _GerarRelatorioAcompanhamentoResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "gerarRelatorioAcompanhamentoResponse");
    private final static QName _ListarPorFiltro_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarPorFiltro");
    private final static QName _ListarPlanosVigentePorCanalResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarPlanosVigentePorCanalResponse");
    private final static QName _ListarPropostasPorBeneficiarioResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarPropostasPorBeneficiarioResponse");
    private final static QName _ObterTermoDeAdesao_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterTermoDeAdesao");
    private final static QName _Finalizar_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "finalizar");
    private final static QName _ListarParaRelatorioAcompanhamento_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarParaRelatorioAcompanhamento");
    private final static QName _ObterPlanoPorCodigoResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterPlanoPorCodigoResponse");
    private final static QName _CancelarResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "cancelarResponse");
    private final static QName _ListarPropostasPorProponenteResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarPropostasPorProponenteResponse");
    private final static QName _ObterTermoDeAdesaoResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterTermoDeAdesaoResponse");
    private final static QName _GerarBoleto_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "gerarBoleto");
    private final static QName _ObterSituacaoPropostaResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "obterSituacaoPropostaResponse");
    private final static QName _ListarParaRelatorioAcompanhamentoResponse_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarParaRelatorioAcompanhamentoResponse");
    private final static QName _ListarPropostasPorBeneficiario_QNAME = new QName("http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", "listarPropostasPorBeneficiario");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObterSituacaoProposta }
     * 
     */
    public ObterSituacaoProposta createObterSituacaoProposta() {
        return new ObterSituacaoProposta();
    }

    /**
     * Create an instance of {@link ListarPropostasPorProponente }
     * 
     */
    public ListarPropostasPorProponente createListarPropostasPorProponente() {
        return new ListarPropostasPorProponente();
    }

    /**
     * Create an instance of {@link ObterPlanoPorCodigo }
     * 
     */
    public ObterPlanoPorCodigo createObterPlanoPorCodigo() {
        return new ObterPlanoPorCodigo();
    }

    /**
     * Create an instance of {@link ObterPorCodigo }
     * 
     */
    public ObterPorCodigo createObterPorCodigo() {
        return new ObterPorCodigo();
    }

    /**
     * Create an instance of {@link AtualizarStatusDaPropostaNoShoppingDeSegurosResponse }
     * 
     */
    public AtualizarStatusDaPropostaNoShoppingDeSegurosResponse createAtualizarStatusDaPropostaNoShoppingDeSegurosResponse() {
        return new AtualizarStatusDaPropostaNoShoppingDeSegurosResponse();
    }

    /**
     * Create an instance of {@link GerarRelatorioAcompanhamentoResponse }
     * 
     */
    public GerarRelatorioAcompanhamentoResponse createGerarRelatorioAcompanhamentoResponse() {
        return new GerarRelatorioAcompanhamentoResponse();
    }

    /**
     * Create an instance of {@link ListarPorFiltro }
     * 
     */
    public ListarPorFiltro createListarPorFiltro() {
        return new ListarPorFiltro();
    }

    /**
     * Create an instance of {@link ListarPlanosVigentePorCanalResponse }
     * 
     */
    public ListarPlanosVigentePorCanalResponse createListarPlanosVigentePorCanalResponse() {
        return new ListarPlanosVigentePorCanalResponse();
    }

    /**
     * Create an instance of {@link ObterTermoDeAdesao }
     * 
     */
    public ObterTermoDeAdesao createObterTermoDeAdesao() {
        return new ObterTermoDeAdesao();
    }

    /**
     * Create an instance of {@link ListarPropostasPorBeneficiarioResponse }
     * 
     */
    public ListarPropostasPorBeneficiarioResponse createListarPropostasPorBeneficiarioResponse() {
        return new ListarPropostasPorBeneficiarioResponse();
    }

    /**
     * Create an instance of {@link Finalizar }
     * 
     */
    public Finalizar createFinalizar() {
        return new Finalizar();
    }

    /**
     * Create an instance of {@link ListarParaRelatorioAcompanhamento }
     * 
     */
    public ListarParaRelatorioAcompanhamento createListarParaRelatorioAcompanhamento() {
        return new ListarParaRelatorioAcompanhamento();
    }

    /**
     * Create an instance of {@link ObterPlanoPorCodigoResponse }
     * 
     */
    public ObterPlanoPorCodigoResponse createObterPlanoPorCodigoResponse() {
        return new ObterPlanoPorCodigoResponse();
    }

    /**
     * Create an instance of {@link CancelarResponse }
     * 
     */
    public CancelarResponse createCancelarResponse() {
        return new CancelarResponse();
    }

    /**
     * Create an instance of {@link ListarPropostasPorProponenteResponse }
     * 
     */
    public ListarPropostasPorProponenteResponse createListarPropostasPorProponenteResponse() {
        return new ListarPropostasPorProponenteResponse();
    }

    /**
     * Create an instance of {@link ObterTermoDeAdesaoResponse }
     * 
     */
    public ObterTermoDeAdesaoResponse createObterTermoDeAdesaoResponse() {
        return new ObterTermoDeAdesaoResponse();
    }

    /**
     * Create an instance of {@link GerarBoleto }
     * 
     */
    public GerarBoleto createGerarBoleto() {
        return new GerarBoleto();
    }

    /**
     * Create an instance of {@link ObterSituacaoPropostaResponse }
     * 
     */
    public ObterSituacaoPropostaResponse createObterSituacaoPropostaResponse() {
        return new ObterSituacaoPropostaResponse();
    }

    /**
     * Create an instance of {@link ListarParaRelatorioAcompanhamentoResponse }
     * 
     */
    public ListarParaRelatorioAcompanhamentoResponse createListarParaRelatorioAcompanhamentoResponse() {
        return new ListarParaRelatorioAcompanhamentoResponse();
    }

    /**
     * Create an instance of {@link ListarPropostasPorBeneficiario }
     * 
     */
    public ListarPropostasPorBeneficiario createListarPropostasPorBeneficiario() {
        return new ListarPropostasPorBeneficiario();
    }

    /**
     * Create an instance of {@link GerarPDFProposta }
     * 
     */
    public GerarPDFProposta createGerarPDFProposta() {
        return new GerarPDFProposta();
    }

    /**
     * Create an instance of {@link ObterPropostaPorCodigoResponse }
     * 
     */
    public ObterPropostaPorCodigoResponse createObterPropostaPorCodigoResponse() {
        return new ObterPropostaPorCodigoResponse();
    }

    /**
     * Create an instance of {@link GerarBoletoResponse }
     * 
     */
    public GerarBoletoResponse createGerarBoletoResponse() {
        return new GerarBoletoResponse();
    }

    /**
     * Create an instance of {@link CancelarPropostaPorProtocolo }
     * 
     */
    public CancelarPropostaPorProtocolo createCancelarPropostaPorProtocolo() {
        return new CancelarPropostaPorProtocolo();
    }

    /**
     * Create an instance of {@link ObterCanalVendaCallCenter }
     * 
     */
    public ObterCanalVendaCallCenter createObterCanalVendaCallCenter() {
        return new ObterCanalVendaCallCenter();
    }

    /**
     * Create an instance of {@link GerarPDFPropostaResponse }
     * 
     */
    public GerarPDFPropostaResponse createGerarPDFPropostaResponse() {
        return new GerarPDFPropostaResponse();
    }

    /**
     * Create an instance of {@link SalvarRascunhoResponse }
     * 
     */
    public SalvarRascunhoResponse createSalvarRascunhoResponse() {
        return new SalvarRascunhoResponse();
    }

    /**
     * Create an instance of {@link CancelarPropostaShoppingSegurosResponse }
     * 
     */
    public CancelarPropostaShoppingSegurosResponse createCancelarPropostaShoppingSegurosResponse() {
        return new CancelarPropostaShoppingSegurosResponse();
    }

    /**
     * Create an instance of {@link CancelarPropostaShoppingSeguros }
     * 
     */
    public CancelarPropostaShoppingSeguros createCancelarPropostaShoppingSeguros() {
        return new CancelarPropostaShoppingSeguros();
    }

    /**
     * Create an instance of {@link BusinessException }
     * 
     */
    public BusinessException createBusinessException() {
        return new BusinessException();
    }

    /**
     * Create an instance of {@link SalvarRascunho }
     * 
     */
    public SalvarRascunho createSalvarRascunho() {
        return new SalvarRascunho();
    }

    /**
     * Create an instance of {@link ListarPlanosVigentePorCanal }
     * 
     */
    public ListarPlanosVigentePorCanal createListarPlanosVigentePorCanal() {
        return new ListarPlanosVigentePorCanal();
    }

    /**
     * Create an instance of {@link ObterPropostaPorCodigo }
     * 
     */
    public ObterPropostaPorCodigo createObterPropostaPorCodigo() {
        return new ObterPropostaPorCodigo();
    }

    /**
     * Create an instance of {@link ObterPorCodigoResponse }
     * 
     */
    public ObterPorCodigoResponse createObterPorCodigoResponse() {
        return new ObterPorCodigoResponse();
    }

    /**
     * Create an instance of {@link FinalizarResponse }
     * 
     */
    public FinalizarResponse createFinalizarResponse() {
        return new FinalizarResponse();
    }

    /**
     * Create an instance of {@link CancelarPropostaPorProtocoloResponse }
     * 
     */
    public CancelarPropostaPorProtocoloResponse createCancelarPropostaPorProtocoloResponse() {
        return new CancelarPropostaPorProtocoloResponse();
    }

    /**
     * Create an instance of {@link GerarRelatorioAcompanhamento }
     * 
     */
    public GerarRelatorioAcompanhamento createGerarRelatorioAcompanhamento() {
        return new GerarRelatorioAcompanhamento();
    }

    /**
     * Create an instance of {@link Cancelar }
     * 
     */
    public Cancelar createCancelar() {
        return new Cancelar();
    }

    /**
     * Create an instance of {@link ObterCanalVendaCallCenterResponse }
     * 
     */
    public ObterCanalVendaCallCenterResponse createObterCanalVendaCallCenterResponse() {
        return new ObterCanalVendaCallCenterResponse();
    }

    /**
     * Create an instance of {@link IntegrationException }
     * 
     */
    public IntegrationException createIntegrationException() {
        return new IntegrationException();
    }

    /**
     * Create an instance of {@link AtualizarStatusDaPropostaNoShoppingDeSeguros }
     * 
     */
    public AtualizarStatusDaPropostaNoShoppingDeSeguros createAtualizarStatusDaPropostaNoShoppingDeSeguros() {
        return new AtualizarStatusDaPropostaNoShoppingDeSeguros();
    }

    /**
     * Create an instance of {@link ListarPorFiltroResponse }
     * 
     */
    public ListarPorFiltroResponse createListarPorFiltroResponse() {
        return new ListarPorFiltroResponse();
    }

    /**
     * Create an instance of {@link CorretorMasterVO }
     * 
     */
    public CorretorMasterVO createCorretorMasterVO() {
        return new CorretorMasterVO();
    }

    /**
     * Create an instance of {@link PropostaVO }
     * 
     */
    public PropostaVO createPropostaVO() {
        return new PropostaVO();
    }

    /**
     * Create an instance of {@link DependenteVO }
     * 
     */
    public DependenteVO createDependenteVO() {
        return new DependenteVO();
    }

    /**
     * Create an instance of {@link TelefoneVO }
     * 
     */
    public TelefoneVO createTelefoneVO() {
        return new TelefoneVO();
    }

    /**
     * Create an instance of {@link PlanoVO }
     * 
     */
    public PlanoVO createPlanoVO() {
        return new PlanoVO();
    }

    /**
     * Create an instance of {@link AgenciaBancariaVO }
     * 
     */
    public AgenciaBancariaVO createAgenciaBancariaVO() {
        return new AgenciaBancariaVO();
    }

    /**
     * Create an instance of {@link LoginVO }
     * 
     */
    public LoginVO createLoginVO() {
        return new LoginVO();
    }

    /**
     * Create an instance of {@link ItemRelatorioAcompanhamentoVO }
     * 
     */
    public ItemRelatorioAcompanhamentoVO createItemRelatorioAcompanhamentoVO() {
        return new ItemRelatorioAcompanhamentoVO();
    }

    /**
     * Create an instance of {@link BancoVO }
     * 
     */
    public BancoVO createBancoVO() {
        return new BancoVO();
    }

    /**
     * Create an instance of {@link SucursalSeguradoraVO }
     * 
     */
    public SucursalSeguradoraVO createSucursalSeguradoraVO() {
        return new SucursalSeguradoraVO();
    }

    /**
     * Create an instance of {@link FiltroPropostaVO }
     * 
     */
    public FiltroPropostaVO createFiltroPropostaVO() {
        return new FiltroPropostaVO();
    }

    /**
     * Create an instance of {@link AngariadorVO }
     * 
     */
    public AngariadorVO createAngariadorVO() {
        return new AngariadorVO();
    }

    /**
     * Create an instance of {@link ResponsavelLegalVO }
     * 
     */
    public ResponsavelLegalVO createResponsavelLegalVO() {
        return new ResponsavelLegalVO();
    }

    /**
     * Create an instance of {@link ProponenteVO }
     * 
     */
    public ProponenteVO createProponenteVO() {
        return new ProponenteVO();
    }

    /**
     * Create an instance of {@link MovimentoPropostaVO }
     * 
     */
    public MovimentoPropostaVO createMovimentoPropostaVO() {
        return new MovimentoPropostaVO();
    }

    /**
     * Create an instance of {@link ValorPlanoVO }
     * 
     */
    public ValorPlanoVO createValorPlanoVO() {
        return new ValorPlanoVO();
    }

    /**
     * Create an instance of {@link ContaCorrenteVO }
     * 
     */
    public ContaCorrenteVO createContaCorrenteVO() {
        return new ContaCorrenteVO();
    }

    /**
     * Create an instance of {@link DateTime }
     * 
     */
    public DateTime createDateTime() {
        return new DateTime();
    }

    /**
     * Create an instance of {@link TitularVO }
     * 
     */
    public TitularVO createTitularVO() {
        return new TitularVO();
    }

    /**
     * Create an instance of {@link RelatorioAcompanhamentoVO }
     * 
     */
    public RelatorioAcompanhamentoVO createRelatorioAcompanhamentoVO() {
        return new RelatorioAcompanhamentoVO();
    }

    /**
     * Create an instance of {@link PessoaVO }
     * 
     */
    public PessoaVO createPessoaVO() {
        return new PessoaVO();
    }

    /**
     * Create an instance of {@link CorretorVO }
     * 
     */
    public CorretorVO createCorretorVO() {
        return new CorretorVO();
    }

    /**
     * Create an instance of {@link CanalVendaVO }
     * 
     */
    public CanalVendaVO createCanalVendaVO() {
        return new CanalVendaVO();
    }

    /**
     * Create an instance of {@link FiltroRelatorioAcompanhamentoVO }
     * 
     */
    public FiltroRelatorioAcompanhamentoVO createFiltroRelatorioAcompanhamentoVO() {
        return new FiltroRelatorioAcompanhamentoVO();
    }

    /**
     * Create an instance of {@link LocalDate }
     * 
     */
    public LocalDate createLocalDate() {
        return new LocalDate();
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link EnderecoVO }
     * 
     */
    public EnderecoVO createEnderecoVO() {
        return new EnderecoVO();
    }

    /**
     * Create an instance of {@link UnidadeFederativaVO }
     * 
     */
    public UnidadeFederativaVO createUnidadeFederativaVO() {
        return new UnidadeFederativaVO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarPDFProposta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "gerarPDFProposta")
    public JAXBElement<GerarPDFProposta> createGerarPDFProposta(GerarPDFProposta value) {
        return new JAXBElement<GerarPDFProposta>(_GerarPDFProposta_QNAME, GerarPDFProposta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPropostaPorCodigoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterPropostaPorCodigoResponse")
    public JAXBElement<ObterPropostaPorCodigoResponse> createObterPropostaPorCodigoResponse(ObterPropostaPorCodigoResponse value) {
        return new JAXBElement<ObterPropostaPorCodigoResponse>(_ObterPropostaPorCodigoResponse_QNAME, ObterPropostaPorCodigoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarBoletoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "gerarBoletoResponse")
    public JAXBElement<GerarBoletoResponse> createGerarBoletoResponse(GerarBoletoResponse value) {
        return new JAXBElement<GerarBoletoResponse>(_GerarBoletoResponse_QNAME, GerarBoletoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarPropostaPorProtocolo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "cancelarPropostaPorProtocolo")
    public JAXBElement<CancelarPropostaPorProtocolo> createCancelarPropostaPorProtocolo(CancelarPropostaPorProtocolo value) {
        return new JAXBElement<CancelarPropostaPorProtocolo>(_CancelarPropostaPorProtocolo_QNAME, CancelarPropostaPorProtocolo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCanalVendaCallCenter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterCanalVendaCallCenter")
    public JAXBElement<ObterCanalVendaCallCenter> createObterCanalVendaCallCenter(ObterCanalVendaCallCenter value) {
        return new JAXBElement<ObterCanalVendaCallCenter>(_ObterCanalVendaCallCenter_QNAME, ObterCanalVendaCallCenter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarPDFPropostaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "gerarPDFPropostaResponse")
    public JAXBElement<GerarPDFPropostaResponse> createGerarPDFPropostaResponse(GerarPDFPropostaResponse value) {
        return new JAXBElement<GerarPDFPropostaResponse>(_GerarPDFPropostaResponse_QNAME, GerarPDFPropostaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalvarRascunhoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "salvarRascunhoResponse")
    public JAXBElement<SalvarRascunhoResponse> createSalvarRascunhoResponse(SalvarRascunhoResponse value) {
        return new JAXBElement<SalvarRascunhoResponse>(_SalvarRascunhoResponse_QNAME, SalvarRascunhoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarPropostaShoppingSeguros }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "cancelarPropostaShoppingSeguros")
    public JAXBElement<CancelarPropostaShoppingSeguros> createCancelarPropostaShoppingSeguros(CancelarPropostaShoppingSeguros value) {
        return new JAXBElement<CancelarPropostaShoppingSeguros>(_CancelarPropostaShoppingSeguros_QNAME, CancelarPropostaShoppingSeguros.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarPropostaShoppingSegurosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "cancelarPropostaShoppingSegurosResponse")
    public JAXBElement<CancelarPropostaShoppingSegurosResponse> createCancelarPropostaShoppingSegurosResponse(CancelarPropostaShoppingSegurosResponse value) {
        return new JAXBElement<CancelarPropostaShoppingSegurosResponse>(_CancelarPropostaShoppingSegurosResponse_QNAME, CancelarPropostaShoppingSegurosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "BusinessException")
    public JAXBElement<BusinessException> createBusinessException(BusinessException value) {
        return new JAXBElement<BusinessException>(_BusinessException_QNAME, BusinessException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalvarRascunho }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "salvarRascunho")
    public JAXBElement<SalvarRascunho> createSalvarRascunho(SalvarRascunho value) {
        return new JAXBElement<SalvarRascunho>(_SalvarRascunho_QNAME, SalvarRascunho.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPlanosVigentePorCanal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarPlanosVigentePorCanal")
    public JAXBElement<ListarPlanosVigentePorCanal> createListarPlanosVigentePorCanal(ListarPlanosVigentePorCanal value) {
        return new JAXBElement<ListarPlanosVigentePorCanal>(_ListarPlanosVigentePorCanal_QNAME, ListarPlanosVigentePorCanal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPorCodigoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterPorCodigoResponse")
    public JAXBElement<ObterPorCodigoResponse> createObterPorCodigoResponse(ObterPorCodigoResponse value) {
        return new JAXBElement<ObterPorCodigoResponse>(_ObterPorCodigoResponse_QNAME, ObterPorCodigoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPropostaPorCodigo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterPropostaPorCodigo")
    public JAXBElement<ObterPropostaPorCodigo> createObterPropostaPorCodigo(ObterPropostaPorCodigo value) {
        return new JAXBElement<ObterPropostaPorCodigo>(_ObterPropostaPorCodigo_QNAME, ObterPropostaPorCodigo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarPropostaPorProtocoloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "cancelarPropostaPorProtocoloResponse")
    public JAXBElement<CancelarPropostaPorProtocoloResponse> createCancelarPropostaPorProtocoloResponse(CancelarPropostaPorProtocoloResponse value) {
        return new JAXBElement<CancelarPropostaPorProtocoloResponse>(_CancelarPropostaPorProtocoloResponse_QNAME, CancelarPropostaPorProtocoloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinalizarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "finalizarResponse")
    public JAXBElement<FinalizarResponse> createFinalizarResponse(FinalizarResponse value) {
        return new JAXBElement<FinalizarResponse>(_FinalizarResponse_QNAME, FinalizarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarRelatorioAcompanhamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "gerarRelatorioAcompanhamento")
    public JAXBElement<GerarRelatorioAcompanhamento> createGerarRelatorioAcompanhamento(GerarRelatorioAcompanhamento value) {
        return new JAXBElement<GerarRelatorioAcompanhamento>(_GerarRelatorioAcompanhamento_QNAME, GerarRelatorioAcompanhamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cancelar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "cancelar")
    public JAXBElement<Cancelar> createCancelar(Cancelar value) {
        return new JAXBElement<Cancelar>(_Cancelar_QNAME, Cancelar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterCanalVendaCallCenterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterCanalVendaCallCenterResponse")
    public JAXBElement<ObterCanalVendaCallCenterResponse> createObterCanalVendaCallCenterResponse(ObterCanalVendaCallCenterResponse value) {
        return new JAXBElement<ObterCanalVendaCallCenterResponse>(_ObterCanalVendaCallCenterResponse_QNAME, ObterCanalVendaCallCenterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntegrationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "IntegrationException")
    public JAXBElement<IntegrationException> createIntegrationException(IntegrationException value) {
        return new JAXBElement<IntegrationException>(_IntegrationException_QNAME, IntegrationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPorFiltroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarPorFiltroResponse")
    public JAXBElement<ListarPorFiltroResponse> createListarPorFiltroResponse(ListarPorFiltroResponse value) {
        return new JAXBElement<ListarPorFiltroResponse>(_ListarPorFiltroResponse_QNAME, ListarPorFiltroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtualizarStatusDaPropostaNoShoppingDeSeguros }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "atualizarStatusDaPropostaNoShoppingDeSeguros")
    public JAXBElement<AtualizarStatusDaPropostaNoShoppingDeSeguros> createAtualizarStatusDaPropostaNoShoppingDeSeguros(AtualizarStatusDaPropostaNoShoppingDeSeguros value) {
        return new JAXBElement<AtualizarStatusDaPropostaNoShoppingDeSeguros>(_AtualizarStatusDaPropostaNoShoppingDeSeguros_QNAME, AtualizarStatusDaPropostaNoShoppingDeSeguros.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPropostasPorProponente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarPropostasPorProponente")
    public JAXBElement<ListarPropostasPorProponente> createListarPropostasPorProponente(ListarPropostasPorProponente value) {
        return new JAXBElement<ListarPropostasPorProponente>(_ListarPropostasPorProponente_QNAME, ListarPropostasPorProponente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterSituacaoProposta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterSituacaoProposta")
    public JAXBElement<ObterSituacaoProposta> createObterSituacaoProposta(ObterSituacaoProposta value) {
        return new JAXBElement<ObterSituacaoProposta>(_ObterSituacaoProposta_QNAME, ObterSituacaoProposta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPlanoPorCodigo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterPlanoPorCodigo")
    public JAXBElement<ObterPlanoPorCodigo> createObterPlanoPorCodigo(ObterPlanoPorCodigo value) {
        return new JAXBElement<ObterPlanoPorCodigo>(_ObterPlanoPorCodigo_QNAME, ObterPlanoPorCodigo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtualizarStatusDaPropostaNoShoppingDeSegurosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "atualizarStatusDaPropostaNoShoppingDeSegurosResponse")
    public JAXBElement<AtualizarStatusDaPropostaNoShoppingDeSegurosResponse> createAtualizarStatusDaPropostaNoShoppingDeSegurosResponse(AtualizarStatusDaPropostaNoShoppingDeSegurosResponse value) {
        return new JAXBElement<AtualizarStatusDaPropostaNoShoppingDeSegurosResponse>(_AtualizarStatusDaPropostaNoShoppingDeSegurosResponse_QNAME, AtualizarStatusDaPropostaNoShoppingDeSegurosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPorCodigo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterPorCodigo")
    public JAXBElement<ObterPorCodigo> createObterPorCodigo(ObterPorCodigo value) {
        return new JAXBElement<ObterPorCodigo>(_ObterPorCodigo_QNAME, ObterPorCodigo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarRelatorioAcompanhamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "gerarRelatorioAcompanhamentoResponse")
    public JAXBElement<GerarRelatorioAcompanhamentoResponse> createGerarRelatorioAcompanhamentoResponse(GerarRelatorioAcompanhamentoResponse value) {
        return new JAXBElement<GerarRelatorioAcompanhamentoResponse>(_GerarRelatorioAcompanhamentoResponse_QNAME, GerarRelatorioAcompanhamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPorFiltro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarPorFiltro")
    public JAXBElement<ListarPorFiltro> createListarPorFiltro(ListarPorFiltro value) {
        return new JAXBElement<ListarPorFiltro>(_ListarPorFiltro_QNAME, ListarPorFiltro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPlanosVigentePorCanalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarPlanosVigentePorCanalResponse")
    public JAXBElement<ListarPlanosVigentePorCanalResponse> createListarPlanosVigentePorCanalResponse(ListarPlanosVigentePorCanalResponse value) {
        return new JAXBElement<ListarPlanosVigentePorCanalResponse>(_ListarPlanosVigentePorCanalResponse_QNAME, ListarPlanosVigentePorCanalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPropostasPorBeneficiarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarPropostasPorBeneficiarioResponse")
    public JAXBElement<ListarPropostasPorBeneficiarioResponse> createListarPropostasPorBeneficiarioResponse(ListarPropostasPorBeneficiarioResponse value) {
        return new JAXBElement<ListarPropostasPorBeneficiarioResponse>(_ListarPropostasPorBeneficiarioResponse_QNAME, ListarPropostasPorBeneficiarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTermoDeAdesao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterTermoDeAdesao")
    public JAXBElement<ObterTermoDeAdesao> createObterTermoDeAdesao(ObterTermoDeAdesao value) {
        return new JAXBElement<ObterTermoDeAdesao>(_ObterTermoDeAdesao_QNAME, ObterTermoDeAdesao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Finalizar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "finalizar")
    public JAXBElement<Finalizar> createFinalizar(Finalizar value) {
        return new JAXBElement<Finalizar>(_Finalizar_QNAME, Finalizar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarParaRelatorioAcompanhamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarParaRelatorioAcompanhamento")
    public JAXBElement<ListarParaRelatorioAcompanhamento> createListarParaRelatorioAcompanhamento(ListarParaRelatorioAcompanhamento value) {
        return new JAXBElement<ListarParaRelatorioAcompanhamento>(_ListarParaRelatorioAcompanhamento_QNAME, ListarParaRelatorioAcompanhamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterPlanoPorCodigoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterPlanoPorCodigoResponse")
    public JAXBElement<ObterPlanoPorCodigoResponse> createObterPlanoPorCodigoResponse(ObterPlanoPorCodigoResponse value) {
        return new JAXBElement<ObterPlanoPorCodigoResponse>(_ObterPlanoPorCodigoResponse_QNAME, ObterPlanoPorCodigoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "cancelarResponse")
    public JAXBElement<CancelarResponse> createCancelarResponse(CancelarResponse value) {
        return new JAXBElement<CancelarResponse>(_CancelarResponse_QNAME, CancelarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPropostasPorProponenteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarPropostasPorProponenteResponse")
    public JAXBElement<ListarPropostasPorProponenteResponse> createListarPropostasPorProponenteResponse(ListarPropostasPorProponenteResponse value) {
        return new JAXBElement<ListarPropostasPorProponenteResponse>(_ListarPropostasPorProponenteResponse_QNAME, ListarPropostasPorProponenteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTermoDeAdesaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterTermoDeAdesaoResponse")
    public JAXBElement<ObterTermoDeAdesaoResponse> createObterTermoDeAdesaoResponse(ObterTermoDeAdesaoResponse value) {
        return new JAXBElement<ObterTermoDeAdesaoResponse>(_ObterTermoDeAdesaoResponse_QNAME, ObterTermoDeAdesaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GerarBoleto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "gerarBoleto")
    public JAXBElement<GerarBoleto> createGerarBoleto(GerarBoleto value) {
        return new JAXBElement<GerarBoleto>(_GerarBoleto_QNAME, GerarBoleto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterSituacaoPropostaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "obterSituacaoPropostaResponse")
    public JAXBElement<ObterSituacaoPropostaResponse> createObterSituacaoPropostaResponse(ObterSituacaoPropostaResponse value) {
        return new JAXBElement<ObterSituacaoPropostaResponse>(_ObterSituacaoPropostaResponse_QNAME, ObterSituacaoPropostaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarParaRelatorioAcompanhamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarParaRelatorioAcompanhamentoResponse")
    public JAXBElement<ListarParaRelatorioAcompanhamentoResponse> createListarParaRelatorioAcompanhamentoResponse(ListarParaRelatorioAcompanhamentoResponse value) {
        return new JAXBElement<ListarParaRelatorioAcompanhamentoResponse>(_ListarParaRelatorioAcompanhamentoResponse_QNAME, ListarParaRelatorioAcompanhamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPropostasPorBeneficiario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/", name = "listarPropostasPorBeneficiario")
    public JAXBElement<ListarPropostasPorBeneficiario> createListarPropostasPorBeneficiario(ListarPropostasPorBeneficiario value) {
        return new JAXBElement<ListarPropostasPorBeneficiario>(_ListarPropostasPorBeneficiario_QNAME, ListarPropostasPorBeneficiario.class, null, value);
    }

}
