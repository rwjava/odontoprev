
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ListarSegmentacaoProdutoClienteSaidaVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listarSegmentacaoProdutoClienteReturn" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ListarSegmentacaoProdutoClienteSaidaVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listarSegmentacaoProdutoClienteReturn"
})
@XmlRootElement(name = "listarSegmentacaoProdutoClienteResponse")
public class ListarSegmentacaoProdutoClienteResponse {

    @XmlElement(required = true, nillable = true)
    protected ListarSegmentacaoProdutoClienteSaidaVO listarSegmentacaoProdutoClienteReturn;

    /**
     * Gets the value of the listarSegmentacaoProdutoClienteReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ListarSegmentacaoProdutoClienteSaidaVO }
     *     
     */
    public ListarSegmentacaoProdutoClienteSaidaVO getListarSegmentacaoProdutoClienteReturn() {
        return listarSegmentacaoProdutoClienteReturn;
    }

    /**
     * Sets the value of the listarSegmentacaoProdutoClienteReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListarSegmentacaoProdutoClienteSaidaVO }
     *     
     */
    public void setListarSegmentacaoProdutoClienteReturn(ListarSegmentacaoProdutoClienteSaidaVO value) {
        this.listarSegmentacaoProdutoClienteReturn = value;
    }

}
