
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PessoaContaCorrenteVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PessoaContaCorrenteVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bairro" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cep" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cidade" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoAgencia" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoTipoConta" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="compl" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cpfCnpj" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ddd" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="logradouro" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nome" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numero" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroContaCorrente" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroOcorrencias" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="telefone" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="titular" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="titularidade" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="tpConta" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="tpPessoa" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="uf" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PessoaContaCorrenteVO", propOrder = {
    "bairro",
    "cep",
    "cidade",
    "codigoAgencia",
    "codigoTipoConta",
    "compl",
    "cpfCnpj",
    "ddd",
    "logradouro",
    "nome",
    "numero",
    "numeroContaCorrente",
    "numeroOcorrencias",
    "telefone",
    "titular",
    "titularidade",
    "tpConta",
    "tpPessoa",
    "uf"
})
public class PessoaContaCorrenteVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(required = true, nillable = true)
    protected String bairro;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cep;
    @XmlElement(required = true, nillable = true)
    protected String cidade;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoAgencia;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoTipoConta;
    @XmlElement(required = true, nillable = true)
    protected String compl;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cpfCnpj;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long ddd;
    @XmlElement(required = true, nillable = true)
    protected String logradouro;
    @XmlElement(required = true, nillable = true)
    protected String nome;
    @XmlElement(required = true, nillable = true)
    protected String numero;
    @XmlElement(required = true, nillable = true)
    protected String numeroContaCorrente;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long numeroOcorrencias;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long telefone;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long titular;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long titularidade;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long tpConta;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long tpPessoa;
    @XmlElement(required = true, nillable = true)
    protected String uf;

    /**
     * Gets the value of the bairro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Sets the value of the bairro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairro(String value) {
        this.bairro = value;
    }

    /**
     * Gets the value of the cep property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCep() {
        return cep;
    }

    /**
     * Sets the value of the cep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCep(Long value) {
        this.cep = value;
    }

    /**
     * Gets the value of the cidade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Sets the value of the cidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidade(String value) {
        this.cidade = value;
    }

    /**
     * Gets the value of the codigoAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Sets the value of the codigoAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoAgencia(Long value) {
        this.codigoAgencia = value;
    }

    /**
     * Gets the value of the codigoTipoConta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoTipoConta() {
        return codigoTipoConta;
    }

    /**
     * Sets the value of the codigoTipoConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoTipoConta(Long value) {
        this.codigoTipoConta = value;
    }

    /**
     * Gets the value of the compl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompl() {
        return compl;
    }

    /**
     * Sets the value of the compl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompl(String value) {
        this.compl = value;
    }

    /**
     * Gets the value of the cpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Sets the value of the cpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCpfCnpj(Long value) {
        this.cpfCnpj = value;
    }

    /**
     * Gets the value of the ddd property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDdd() {
        return ddd;
    }

    /**
     * Sets the value of the ddd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDdd(Long value) {
        this.ddd = value;
    }

    /**
     * Gets the value of the logradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * Sets the value of the logradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogradouro(String value) {
        this.logradouro = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the numeroContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    /**
     * Sets the value of the numeroContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroContaCorrente(String value) {
        this.numeroContaCorrente = value;
    }

    /**
     * Gets the value of the numeroOcorrencias property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroOcorrencias() {
        return numeroOcorrencias;
    }

    /**
     * Sets the value of the numeroOcorrencias property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroOcorrencias(Long value) {
        this.numeroOcorrencias = value;
    }

    /**
     * Gets the value of the telefone property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTelefone() {
        return telefone;
    }

    /**
     * Sets the value of the telefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTelefone(Long value) {
        this.telefone = value;
    }

    /**
     * Gets the value of the titular property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTitular() {
        return titular;
    }

    /**
     * Sets the value of the titular property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTitular(Long value) {
        this.titular = value;
    }

    /**
     * Gets the value of the titularidade property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTitularidade() {
        return titularidade;
    }

    /**
     * Sets the value of the titularidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTitularidade(Long value) {
        this.titularidade = value;
    }

    /**
     * Gets the value of the tpConta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTpConta() {
        return tpConta;
    }

    /**
     * Sets the value of the tpConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTpConta(Long value) {
        this.tpConta = value;
    }

    /**
     * Gets the value of the tpPessoa property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTpPessoa() {
        return tpPessoa;
    }

    /**
     * Sets the value of the tpPessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTpPessoa(Long value) {
        this.tpPessoa = value;
    }

    /**
     * Gets the value of the uf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUf() {
        return uf;
    }

    /**
     * Sets the value of the uf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUf(String value) {
        this.uf = value;
    }

}
