
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidarDadosFuncionarioSinergiaSaidaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidarDadosFuncionarioSinergiaSaidaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nome" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="situacao" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="indDiretor" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contaSalario" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agenciaSaida" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="dvAgenciaSaida" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contaSaida" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="dvContaSaida" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cpf" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idBuc" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cargo" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="corretorSaida" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="sucursalSinergia" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="filler" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidarDadosFuncionarioSinergiaSaidaVO", propOrder = {
    "nome",
    "situacao",
    "indDiretor",
    "contaSalario",
    "agenciaSaida",
    "dvAgenciaSaida",
    "contaSaida",
    "dvContaSaida",
    "cpf",
    "idBuc",
    "cargo",
    "corretorSaida",
    "sucursalSinergia",
    "filler"
})
public class ValidarDadosFuncionarioSinergiaSaidaVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(required = true, nillable = true)
    protected String nome;
    @XmlElement(required = true, nillable = true)
    protected String situacao;
    @XmlElement(required = true, nillable = true)
    protected String indDiretor;
    @XmlElement(required = true, nillable = true)
    protected String contaSalario;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long agenciaSaida;
    @XmlElement(required = true, nillable = true)
    protected String dvAgenciaSaida;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long contaSaida;
    @XmlElement(required = true, nillable = true)
    protected String dvContaSaida;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cpf;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long idBuc;
    @XmlElement(required = true, nillable = true)
    protected String cargo;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long corretorSaida;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long sucursalSinergia;
    @XmlElement(required = true, nillable = true)
    protected String filler;

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the situacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Sets the value of the situacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSituacao(String value) {
        this.situacao = value;
    }

    /**
     * Gets the value of the indDiretor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndDiretor() {
        return indDiretor;
    }

    /**
     * Sets the value of the indDiretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndDiretor(String value) {
        this.indDiretor = value;
    }

    /**
     * Gets the value of the contaSalario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContaSalario() {
        return contaSalario;
    }

    /**
     * Sets the value of the contaSalario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContaSalario(String value) {
        this.contaSalario = value;
    }

    /**
     * Gets the value of the agenciaSaida property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgenciaSaida() {
        return agenciaSaida;
    }

    /**
     * Sets the value of the agenciaSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgenciaSaida(Long value) {
        this.agenciaSaida = value;
    }

    /**
     * Gets the value of the dvAgenciaSaida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvAgenciaSaida() {
        return dvAgenciaSaida;
    }

    /**
     * Sets the value of the dvAgenciaSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvAgenciaSaida(String value) {
        this.dvAgenciaSaida = value;
    }

    /**
     * Gets the value of the contaSaida property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContaSaida() {
        return contaSaida;
    }

    /**
     * Sets the value of the contaSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContaSaida(Long value) {
        this.contaSaida = value;
    }

    /**
     * Gets the value of the dvContaSaida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaSaida() {
        return dvContaSaida;
    }

    /**
     * Sets the value of the dvContaSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaSaida(String value) {
        this.dvContaSaida = value;
    }

    /**
     * Gets the value of the cpf property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCpf() {
        return cpf;
    }

    /**
     * Sets the value of the cpf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCpf(Long value) {
        this.cpf = value;
    }

    /**
     * Gets the value of the idBuc property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdBuc() {
        return idBuc;
    }

    /**
     * Sets the value of the idBuc property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdBuc(Long value) {
        this.idBuc = value;
    }

    /**
     * Gets the value of the cargo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * Sets the value of the cargo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargo(String value) {
        this.cargo = value;
    }

    /**
     * Gets the value of the corretorSaida property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCorretorSaida() {
        return corretorSaida;
    }

    /**
     * Sets the value of the corretorSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCorretorSaida(Long value) {
        this.corretorSaida = value;
    }

    /**
     * Gets the value of the sucursalSinergia property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSucursalSinergia() {
        return sucursalSinergia;
    }

    /**
     * Sets the value of the sucursalSinergia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSucursalSinergia(Long value) {
        this.sucursalSinergia = value;
    }

    /**
     * Gets the value of the filler property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiller() {
        return filler;
    }

    /**
     * Sets the value of the filler property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiller(String value) {
        this.filler = value;
    }

}
