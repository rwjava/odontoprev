
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContaCorrentePorCPFCNPJVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContaCorrentePorCPFCNPJVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencia" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="agenciaDestino" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="bairro" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cdPosto" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cep" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cidade" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="complemento" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="conta" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="contaDestino" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="dddFone" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="descSegmento" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dscPosto" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dtNascimento" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvAgencia" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvConta" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContaDestino" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="endereco" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estado" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fone" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idCrrtt" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="imaePssoaCrrtt" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="indTragueada" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ipaiPssoaCrrtt" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nome" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numero" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="razao" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="segmento" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="situacao" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="titularidade" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tpConta" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dtAbertura" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContaCorrentePorCPFCNPJVO", propOrder = {
    "agencia",
    "agenciaDestino",
    "bairro",
    "cdPosto",
    "cep",
    "cidade",
    "complemento",
    "conta",
    "contaDestino",
    "dddFone",
    "descSegmento",
    "dscPosto",
    "dtNascimento",
    "dvAgencia",
    "dvConta",
    "dvContaDestino",
    "endereco",
    "estado",
    "fone",
    "idCrrtt",
    "imaePssoaCrrtt",
    "indTragueada",
    "ipaiPssoaCrrtt",
    "nome",
    "numero",
    "razao",
    "segmento",
    "situacao",
    "titularidade",
    "tpConta",
    "dtAbertura"
})
public class ContaCorrentePorCPFCNPJVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long agencia;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long agenciaDestino;
    @XmlElement(required = true, nillable = true)
    protected String bairro;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cdPosto;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cep;
    @XmlElement(required = true, nillable = true)
    protected String cidade;
    @XmlElement(required = true, nillable = true)
    protected String complemento;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long conta;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long contaDestino;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long dddFone;
    @XmlElement(required = true, nillable = true)
    protected String descSegmento;
    @XmlElement(required = true, nillable = true)
    protected String dscPosto;
    @XmlElement(required = true, nillable = true)
    protected String dtNascimento;
    @XmlElement(required = true, nillable = true)
    protected String dvAgencia;
    @XmlElement(required = true, nillable = true)
    protected String dvConta;
    @XmlElement(required = true, nillable = true)
    protected String dvContaDestino;
    @XmlElement(required = true, nillable = true)
    protected String endereco;
    @XmlElement(required = true, nillable = true)
    protected String estado;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long fone;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long idCrrtt;
    @XmlElement(required = true, nillable = true)
    protected String imaePssoaCrrtt;
    @XmlElement(required = true, nillable = true)
    protected String indTragueada;
    @XmlElement(required = true, nillable = true)
    protected String ipaiPssoaCrrtt;
    @XmlElement(required = true, nillable = true)
    protected String nome;
    @XmlElement(required = true, nillable = true)
    protected String numero;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer razao;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer segmento;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer situacao;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer titularidade;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tpConta;
    @XmlElement(required = true, nillable = true)
    protected String dtAbertura;

    /**
     * Gets the value of the agencia property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgencia() {
        return agencia;
    }

    /**
     * Sets the value of the agencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgencia(Long value) {
        this.agencia = value;
    }

    /**
     * Gets the value of the agenciaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgenciaDestino() {
        return agenciaDestino;
    }

    /**
     * Sets the value of the agenciaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgenciaDestino(Long value) {
        this.agenciaDestino = value;
    }

    /**
     * Gets the value of the bairro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Sets the value of the bairro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairro(String value) {
        this.bairro = value;
    }

    /**
     * Gets the value of the cdPosto property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCdPosto() {
        return cdPosto;
    }

    /**
     * Sets the value of the cdPosto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCdPosto(Integer value) {
        this.cdPosto = value;
    }

    /**
     * Gets the value of the cep property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCep() {
        return cep;
    }

    /**
     * Sets the value of the cep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCep(Long value) {
        this.cep = value;
    }

    /**
     * Gets the value of the cidade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Sets the value of the cidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidade(String value) {
        this.cidade = value;
    }

    /**
     * Gets the value of the complemento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * Sets the value of the complemento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplemento(String value) {
        this.complemento = value;
    }

    /**
     * Gets the value of the conta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getConta() {
        return conta;
    }

    /**
     * Sets the value of the conta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setConta(Long value) {
        this.conta = value;
    }

    /**
     * Gets the value of the contaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContaDestino() {
        return contaDestino;
    }

    /**
     * Sets the value of the contaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContaDestino(Long value) {
        this.contaDestino = value;
    }

    /**
     * Gets the value of the dddFone property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDddFone() {
        return dddFone;
    }

    /**
     * Sets the value of the dddFone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDddFone(Long value) {
        this.dddFone = value;
    }

    /**
     * Gets the value of the descSegmento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescSegmento() {
        return descSegmento;
    }

    /**
     * Sets the value of the descSegmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescSegmento(String value) {
        this.descSegmento = value;
    }

    /**
     * Gets the value of the dscPosto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDscPosto() {
        return dscPosto;
    }

    /**
     * Sets the value of the dscPosto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDscPosto(String value) {
        this.dscPosto = value;
    }

    /**
     * Gets the value of the dtNascimento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtNascimento() {
        return dtNascimento;
    }

    /**
     * Sets the value of the dtNascimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtNascimento(String value) {
        this.dtNascimento = value;
    }

    /**
     * Gets the value of the dvAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvAgencia() {
        return dvAgencia;
    }

    /**
     * Sets the value of the dvAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvAgencia(String value) {
        this.dvAgencia = value;
    }

    /**
     * Gets the value of the dvConta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvConta() {
        return dvConta;
    }

    /**
     * Sets the value of the dvConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvConta(String value) {
        this.dvConta = value;
    }

    /**
     * Gets the value of the dvContaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaDestino() {
        return dvContaDestino;
    }

    /**
     * Sets the value of the dvContaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaDestino(String value) {
        this.dvContaDestino = value;
    }

    /**
     * Gets the value of the endereco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * Sets the value of the endereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndereco(String value) {
        this.endereco = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Gets the value of the fone property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFone() {
        return fone;
    }

    /**
     * Sets the value of the fone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFone(Long value) {
        this.fone = value;
    }

    /**
     * Gets the value of the idCrrtt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdCrrtt() {
        return idCrrtt;
    }

    /**
     * Sets the value of the idCrrtt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdCrrtt(Long value) {
        this.idCrrtt = value;
    }

    /**
     * Gets the value of the imaePssoaCrrtt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImaePssoaCrrtt() {
        return imaePssoaCrrtt;
    }

    /**
     * Sets the value of the imaePssoaCrrtt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImaePssoaCrrtt(String value) {
        this.imaePssoaCrrtt = value;
    }

    /**
     * Gets the value of the indTragueada property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndTragueada() {
        return indTragueada;
    }

    /**
     * Sets the value of the indTragueada property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndTragueada(String value) {
        this.indTragueada = value;
    }

    /**
     * Gets the value of the ipaiPssoaCrrtt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpaiPssoaCrrtt() {
        return ipaiPssoaCrrtt;
    }

    /**
     * Sets the value of the ipaiPssoaCrrtt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpaiPssoaCrrtt(String value) {
        this.ipaiPssoaCrrtt = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the razao property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRazao() {
        return razao;
    }

    /**
     * Sets the value of the razao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRazao(Integer value) {
        this.razao = value;
    }

    /**
     * Gets the value of the segmento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSegmento() {
        return segmento;
    }

    /**
     * Sets the value of the segmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSegmento(Integer value) {
        this.segmento = value;
    }

    /**
     * Gets the value of the situacao property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSituacao() {
        return situacao;
    }

    /**
     * Sets the value of the situacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSituacao(Integer value) {
        this.situacao = value;
    }

    /**
     * Gets the value of the titularidade property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTitularidade() {
        return titularidade;
    }

    /**
     * Sets the value of the titularidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTitularidade(Integer value) {
        this.titularidade = value;
    }

    /**
     * Gets the value of the tpConta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTpConta() {
        return tpConta;
    }

    /**
     * Sets the value of the tpConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTpConta(Integer value) {
        this.tpConta = value;
    }

    /**
     * Gets the value of the dtAbertura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtAbertura() {
        return dtAbertura;
    }

    /**
     * Sets the value of the dtAbertura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtAbertura(String value) {
        this.dtAbertura = value;
    }

}
