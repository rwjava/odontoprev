
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.bucb.servicos.model.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.bucb.servicos.model.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultarContaCorrentePorCPFResponse }
     * 
     */
    public ConsultarContaCorrentePorCPFResponse createConsultarContaCorrentePorCPFResponse() {
        return new ConsultarContaCorrentePorCPFResponse();
    }

    /**
     * Create an instance of {@link ArrayOf1799424901NillableContaCorrenteCpfVO }
     * 
     */
    public ArrayOf1799424901NillableContaCorrenteCpfVO createArrayOf1799424901NillableContaCorrenteCpfVO() {
        return new ArrayOf1799424901NillableContaCorrenteCpfVO();
    }

    /**
     * Create an instance of {@link ListarCorrentistasPorAgCc }
     * 
     */
    public ListarCorrentistasPorAgCc createListarCorrentistasPorAgCc() {
        return new ListarCorrentistasPorAgCc();
    }

    /**
     * Create an instance of {@link ObterDadosCorrentistaIDCRRTTResponse }
     * 
     */
    public ObterDadosCorrentistaIDCRRTTResponse createObterDadosCorrentistaIDCRRTTResponse() {
        return new ObterDadosCorrentistaIDCRRTTResponse();
    }

    /**
     * Create an instance of {@link ListarCorrentistasPorCpfCnpjResponse }
     * 
     */
    public ListarCorrentistasPorCpfCnpjResponse createListarCorrentistasPorCpfCnpjResponse() {
        return new ListarCorrentistasPorCpfCnpjResponse();
    }

    /**
     * Create an instance of {@link ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO }
     * 
     */
    public ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO createArrayOf1799424901NillableContaCorrentePorCPFCNPJVO() {
        return new ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO();
    }

    /**
     * Create an instance of {@link ObterIDBucPorContaCorrenteCpfCnpjResponse }
     * 
     */
    public ObterIDBucPorContaCorrenteCpfCnpjResponse createObterIDBucPorContaCorrenteCpfCnpjResponse() {
        return new ObterIDBucPorContaCorrenteCpfCnpjResponse();
    }

    /**
     * Create an instance of {@link ConsultarContaCorrentePorCPFCNPJ }
     * 
     */
    public ConsultarContaCorrentePorCPFCNPJ createConsultarContaCorrentePorCPFCNPJ() {
        return new ConsultarContaCorrentePorCPFCNPJ();
    }

    /**
     * Create an instance of {@link ObterDadosCorrentistaIDCRRTT }
     * 
     */
    public ObterDadosCorrentistaIDCRRTT createObterDadosCorrentistaIDCRRTT() {
        return new ObterDadosCorrentistaIDCRRTT();
    }

    /**
     * Create an instance of {@link ListarSegmentacaoProdutoClienteResponse }
     * 
     */
    public ListarSegmentacaoProdutoClienteResponse createListarSegmentacaoProdutoClienteResponse() {
        return new ListarSegmentacaoProdutoClienteResponse();
    }

    /**
     * Create an instance of {@link ValidarContaCorrente }
     * 
     */
    public ValidarContaCorrente createValidarContaCorrente() {
        return new ValidarContaCorrente();
    }

    /**
     * Create an instance of {@link ListarCorrentistasPorAgCcResponse }
     * 
     */
    public ListarCorrentistasPorAgCcResponse createListarCorrentistasPorAgCcResponse() {
        return new ListarCorrentistasPorAgCcResponse();
    }

    /**
     * Create an instance of {@link ListarSegmentacaoProdutoCliente }
     * 
     */
    public ListarSegmentacaoProdutoCliente createListarSegmentacaoProdutoCliente() {
        return new ListarSegmentacaoProdutoCliente();
    }

    /**
     * Create an instance of {@link ValidarDadosFuncionarioSinergia }
     * 
     */
    public ValidarDadosFuncionarioSinergia createValidarDadosFuncionarioSinergia() {
        return new ValidarDadosFuncionarioSinergia();
    }

    /**
     * Create an instance of {@link ListarCorrentistasPorCpfCnpj }
     * 
     */
    public ListarCorrentistasPorCpfCnpj createListarCorrentistasPorCpfCnpj() {
        return new ListarCorrentistasPorCpfCnpj();
    }

    /**
     * Create an instance of {@link ObterIDBucPorContaCorrenteCpfCnpj }
     * 
     */
    public ObterIDBucPorContaCorrenteCpfCnpj createObterIDBucPorContaCorrenteCpfCnpj() {
        return new ObterIDBucPorContaCorrenteCpfCnpj();
    }

    /**
     * Create an instance of {@link ConsultarDadosCorrentista }
     * 
     */
    public ConsultarDadosCorrentista createConsultarDadosCorrentista() {
        return new ConsultarDadosCorrentista();
    }

    /**
     * Create an instance of {@link ValidarDVContaCorrenteResponse }
     * 
     */
    public ValidarDVContaCorrenteResponse createValidarDVContaCorrenteResponse() {
        return new ValidarDVContaCorrenteResponse();
    }

    /**
     * Create an instance of {@link ObterContaCorrenteFuncionarioPorIDBuc }
     * 
     */
    public ObterContaCorrenteFuncionarioPorIDBuc createObterContaCorrenteFuncionarioPorIDBuc() {
        return new ObterContaCorrenteFuncionarioPorIDBuc();
    }

    /**
     * Create an instance of {@link ValidarDadosFuncionario }
     * 
     */
    public ValidarDadosFuncionario createValidarDadosFuncionario() {
        return new ValidarDadosFuncionario();
    }

    /**
     * Create an instance of {@link ConsultarCorrentistasPorAgenciaContaCorrente }
     * 
     */
    public ConsultarCorrentistasPorAgenciaContaCorrente createConsultarCorrentistasPorAgenciaContaCorrente() {
        return new ConsultarCorrentistasPorAgenciaContaCorrente();
    }

    /**
     * Create an instance of {@link ObterContaCorrenteFuncionarioPorIDBucResponse }
     * 
     */
    public ObterContaCorrenteFuncionarioPorIDBucResponse createObterContaCorrenteFuncionarioPorIDBucResponse() {
        return new ObterContaCorrenteFuncionarioPorIDBucResponse();
    }

    /**
     * Create an instance of {@link ConsultarContaCorrentePorCPFCNPJResponse }
     * 
     */
    public ConsultarContaCorrentePorCPFCNPJResponse createConsultarContaCorrentePorCPFCNPJResponse() {
        return new ConsultarContaCorrentePorCPFCNPJResponse();
    }

    /**
     * Create an instance of {@link ArrayOf1799424901NillableContaCorrenteVO }
     * 
     */
    public ArrayOf1799424901NillableContaCorrenteVO createArrayOf1799424901NillableContaCorrenteVO() {
        return new ArrayOf1799424901NillableContaCorrenteVO();
    }

    /**
     * Create an instance of {@link ValidarDadosFuncionarioResponse }
     * 
     */
    public ValidarDadosFuncionarioResponse createValidarDadosFuncionarioResponse() {
        return new ValidarDadosFuncionarioResponse();
    }

    /**
     * Create an instance of {@link ValidarDVContaCorrente }
     * 
     */
    public ValidarDVContaCorrente createValidarDVContaCorrente() {
        return new ValidarDVContaCorrente();
    }

    /**
     * Create an instance of {@link ValidarDadosContaCorrenteResponse }
     * 
     */
    public ValidarDadosContaCorrenteResponse createValidarDadosContaCorrenteResponse() {
        return new ValidarDadosContaCorrenteResponse();
    }

    /**
     * Create an instance of {@link ValidarDadosContaCorrente }
     * 
     */
    public ValidarDadosContaCorrente createValidarDadosContaCorrente() {
        return new ValidarDadosContaCorrente();
    }

    /**
     * Create an instance of {@link ValidarContaCorrenteResponse }
     * 
     */
    public ValidarContaCorrenteResponse createValidarContaCorrenteResponse() {
        return new ValidarContaCorrenteResponse();
    }

    /**
     * Create an instance of {@link ObterDadosBaseContaCorrentePorCPFResponse }
     * 
     */
    public ObterDadosBaseContaCorrentePorCPFResponse createObterDadosBaseContaCorrentePorCPFResponse() {
        return new ObterDadosBaseContaCorrentePorCPFResponse();
    }

    /**
     * Create an instance of {@link ConsultarContaCorrentePorCPF }
     * 
     */
    public ConsultarContaCorrentePorCPF createConsultarContaCorrentePorCPF() {
        return new ConsultarContaCorrentePorCPF();
    }

    /**
     * Create an instance of {@link ObterContaCorrentePorIdBucResponse }
     * 
     */
    public ObterContaCorrentePorIdBucResponse createObterContaCorrentePorIdBucResponse() {
        return new ObterContaCorrentePorIdBucResponse();
    }

    /**
     * Create an instance of {@link ObterContaCorrentePorIdBuc }
     * 
     */
    public ObterContaCorrentePorIdBuc createObterContaCorrentePorIdBuc() {
        return new ObterContaCorrentePorIdBuc();
    }

    /**
     * Create an instance of {@link ConsultarMeioContatoPessoaContaCorrenteResponse }
     * 
     */
    public ConsultarMeioContatoPessoaContaCorrenteResponse createConsultarMeioContatoPessoaContaCorrenteResponse() {
        return new ConsultarMeioContatoPessoaContaCorrenteResponse();
    }

    /**
     * Create an instance of {@link ArrayOf1799424901NillablePessoaContaCorrenteVO }
     * 
     */
    public ArrayOf1799424901NillablePessoaContaCorrenteVO createArrayOf1799424901NillablePessoaContaCorrenteVO() {
        return new ArrayOf1799424901NillablePessoaContaCorrenteVO();
    }

    /**
     * Create an instance of {@link ValidarDadosFuncionarioSinergiaResponse }
     * 
     */
    public ValidarDadosFuncionarioSinergiaResponse createValidarDadosFuncionarioSinergiaResponse() {
        return new ValidarDadosFuncionarioSinergiaResponse();
    }

    /**
     * Create an instance of {@link ConsultarMeioContatoPessoaContaCorrente }
     * 
     */
    public ConsultarMeioContatoPessoaContaCorrente createConsultarMeioContatoPessoaContaCorrente() {
        return new ConsultarMeioContatoPessoaContaCorrente();
    }

    /**
     * Create an instance of {@link ConsultarCorrentistasPorAgenciaContaCorrenteResponse }
     * 
     */
    public ConsultarCorrentistasPorAgenciaContaCorrenteResponse createConsultarCorrentistasPorAgenciaContaCorrenteResponse() {
        return new ConsultarCorrentistasPorAgenciaContaCorrenteResponse();
    }

    /**
     * Create an instance of {@link ConsultarDadosCorrentistaResponse }
     * 
     */
    public ConsultarDadosCorrentistaResponse createConsultarDadosCorrentistaResponse() {
        return new ConsultarDadosCorrentistaResponse();
    }

    /**
     * Create an instance of {@link ObterDadosBaseContaCorrentePorCPF }
     * 
     */
    public ObterDadosBaseContaCorrentePorCPF createObterDadosBaseContaCorrentePorCPF() {
        return new ObterDadosBaseContaCorrentePorCPF();
    }

}
