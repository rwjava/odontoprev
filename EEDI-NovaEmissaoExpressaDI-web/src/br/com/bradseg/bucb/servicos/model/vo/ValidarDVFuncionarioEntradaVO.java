
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidarDVFuncionarioEntradaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidarDVFuncionarioEntradaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cdvContaCorrente" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoAgencia" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cpfCnpj" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numeroContaCorrente" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="tipoPesquisa" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="razaoContaCorrente" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidarDVFuncionarioEntradaVO", propOrder = {
    "cdvContaCorrente",
    "codigoAgencia",
    "cpfCnpj",
    "numeroContaCorrente",
    "tipoPesquisa",
    "razaoContaCorrente"
})
public class ValidarDVFuncionarioEntradaVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(required = true, nillable = true)
    protected String cdvContaCorrente;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoAgencia;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cpfCnpj;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long numeroContaCorrente;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long tipoPesquisa;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long razaoContaCorrente;

    /**
     * Gets the value of the cdvContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdvContaCorrente() {
        return cdvContaCorrente;
    }

    /**
     * Sets the value of the cdvContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdvContaCorrente(String value) {
        this.cdvContaCorrente = value;
    }

    /**
     * Gets the value of the codigoAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Sets the value of the codigoAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoAgencia(Long value) {
        this.codigoAgencia = value;
    }

    /**
     * Gets the value of the cpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Sets the value of the cpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCpfCnpj(Long value) {
        this.cpfCnpj = value;
    }

    /**
     * Gets the value of the numeroContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    /**
     * Sets the value of the numeroContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroContaCorrente(Long value) {
        this.numeroContaCorrente = value;
    }

    /**
     * Gets the value of the tipoPesquisa property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTipoPesquisa() {
        return tipoPesquisa;
    }

    /**
     * Sets the value of the tipoPesquisa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTipoPesquisa(Long value) {
        this.tipoPesquisa = value;
    }

    /**
     * Gets the value of the razaoContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRazaoContaCorrente() {
        return razaoContaCorrente;
    }

    /**
     * Sets the value of the razaoContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRazaoContaCorrente(Long value) {
        this.razaoContaCorrente = value;
    }

}
