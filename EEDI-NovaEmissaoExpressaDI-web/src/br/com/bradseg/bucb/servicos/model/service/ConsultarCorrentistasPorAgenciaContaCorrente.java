
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ipCliente" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userID" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoAgencia" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroContaCorrente" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ipCliente",
    "userID",
    "codigoAgencia",
    "numeroContaCorrente"
})
@XmlRootElement(name = "consultarCorrentistasPorAgenciaContaCorrente")
public class ConsultarCorrentistasPorAgenciaContaCorrente {

    @XmlElement(required = true, nillable = true)
    protected String ipCliente;
    @XmlElement(required = true, nillable = true)
    protected String userID;
    protected int codigoAgencia;
    protected long numeroContaCorrente;

    /**
     * Gets the value of the ipCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpCliente() {
        return ipCliente;
    }

    /**
     * Sets the value of the ipCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpCliente(String value) {
        this.ipCliente = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the codigoAgencia property.
     * 
     */
    public int getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Sets the value of the codigoAgencia property.
     * 
     */
    public void setCodigoAgencia(int value) {
        this.codigoAgencia = value;
    }

    /**
     * Gets the value of the numeroContaCorrente property.
     * 
     */
    public long getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    /**
     * Sets the value of the numeroContaCorrente property.
     * 
     */
    public void setNumeroContaCorrente(long value) {
        this.numeroContaCorrente = value;
    }

}
