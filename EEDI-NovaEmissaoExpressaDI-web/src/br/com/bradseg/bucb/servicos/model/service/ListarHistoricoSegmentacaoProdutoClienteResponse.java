
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ListarSegmentacaoProdutoClienteSaidaVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listarHistoricoSegmentacaoProdutoClienteReturn" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ListarSegmentacaoProdutoClienteSaidaVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listarHistoricoSegmentacaoProdutoClienteReturn"
})
@XmlRootElement(name = "listarHistoricoSegmentacaoProdutoClienteResponse")
public class ListarHistoricoSegmentacaoProdutoClienteResponse {

    @XmlElement(required = true, nillable = true)
    protected ListarSegmentacaoProdutoClienteSaidaVO listarHistoricoSegmentacaoProdutoClienteReturn;

    /**
     * Gets the value of the listarHistoricoSegmentacaoProdutoClienteReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ListarSegmentacaoProdutoClienteSaidaVO }
     *     
     */
    public ListarSegmentacaoProdutoClienteSaidaVO getListarHistoricoSegmentacaoProdutoClienteReturn() {
        return listarHistoricoSegmentacaoProdutoClienteReturn;
    }

    /**
     * Sets the value of the listarHistoricoSegmentacaoProdutoClienteReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListarSegmentacaoProdutoClienteSaidaVO }
     *     
     */
    public void setListarHistoricoSegmentacaoProdutoClienteReturn(ListarSegmentacaoProdutoClienteSaidaVO value) {
        this.listarHistoricoSegmentacaoProdutoClienteReturn = value;
    }

}
