
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ValidarDVFuncionarioSaidaVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validarContaCorrenteReturn" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ValidarDVFuncionarioSaidaVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validarContaCorrenteReturn"
})
@XmlRootElement(name = "validarContaCorrenteResponse")
public class ValidarContaCorrenteResponse {

    @XmlElement(required = true, nillable = true)
    protected ValidarDVFuncionarioSaidaVO validarContaCorrenteReturn;

    /**
     * Gets the value of the validarContaCorrenteReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ValidarDVFuncionarioSaidaVO }
     *     
     */
    public ValidarDVFuncionarioSaidaVO getValidarContaCorrenteReturn() {
        return validarContaCorrenteReturn;
    }

    /**
     * Sets the value of the validarContaCorrenteReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidarDVFuncionarioSaidaVO }
     *     
     */
    public void setValidarContaCorrenteReturn(ValidarDVFuncionarioSaidaVO value) {
        this.validarContaCorrenteReturn = value;
    }

}
