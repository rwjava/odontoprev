
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cpfCnpj" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="agencia" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="data" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cpfCnpj",
    "agencia",
    "data"
})
@XmlRootElement(name = "listarHistoricoSegmentacaoProdutoCliente")
public class ListarHistoricoSegmentacaoProdutoCliente {

    protected long cpfCnpj;
    protected long agencia;
    protected long data;

    /**
     * Gets the value of the cpfCnpj property.
     * 
     */
    public long getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Sets the value of the cpfCnpj property.
     * 
     */
    public void setCpfCnpj(long value) {
        this.cpfCnpj = value;
    }

    /**
     * Gets the value of the agencia property.
     * 
     */
    public long getAgencia() {
        return agencia;
    }

    /**
     * Sets the value of the agencia property.
     * 
     */
    public void setAgencia(long value) {
        this.agencia = value;
    }

    /**
     * Gets the value of the data property.
     * 
     */
    public long getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     */
    public void setData(long value) {
        this.data = value;
    }

}
