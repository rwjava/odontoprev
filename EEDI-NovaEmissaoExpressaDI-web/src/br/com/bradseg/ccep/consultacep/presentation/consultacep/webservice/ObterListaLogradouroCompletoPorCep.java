
package br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for obterListaLogradouroCompletoPorCep complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="obterListaLogradouroCompletoPorCep">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cepCompleto" type="{http" + "://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obterListaLogradouroCompletoPorCep", propOrder = {
    "cepCompleto"
})
public class ObterListaLogradouroCompletoPorCep {

    protected String cepCompleto;

    /**
     * Gets the value of the cepCompleto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCepCompleto() {
        return cepCompleto;
    }

    /**
     * Sets the value of the cepCompleto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCepCompleto(String value) {
        this.cepCompleto = value;
    }

}
