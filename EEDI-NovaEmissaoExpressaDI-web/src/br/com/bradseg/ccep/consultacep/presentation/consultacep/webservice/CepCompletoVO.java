
package br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cepCompletoVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cepCompletoVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/}cepVO">
 *       &lt;sequence>
 *         &lt;element name="codigoBairro" type="{http" + "://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoCidade" type="{http" + "://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cepCompletoVO", propOrder = {
    "codigoBairro",
    "codigoCidade"
})
public class CepCompletoVO
    extends CepVO
{

    protected Long codigoBairro;
    protected Long codigoCidade;

    /**
     * Gets the value of the codigoBairro property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoBairro() {
        return codigoBairro;
    }

    /**
     * Sets the value of the codigoBairro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoBairro(Long value) {
        this.codigoBairro = value;
    }

    /**
     * Gets the value of the codigoCidade property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoCidade() {
        return codigoCidade;
    }

    /**
     * Sets the value of the codigoCidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoCidade(Long value) {
        this.codigoCidade = value;
    }

}
