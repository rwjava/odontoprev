
package br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for obterListaPorRaizCepResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="obterListaPorRaizCepResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/}cepWebServiceResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obterListaPorRaizCepResponse", propOrder = {
    "_return"
})
public class ObterListaPorRaizCepResponse {

    @XmlElement(name = "return")
    protected CepWebServiceResponse _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link CepWebServiceResponse }
     *     
     */
    public CepWebServiceResponse getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link CepWebServiceResponse }
     *     
     */
    public void setReturn(CepWebServiceResponse value) {
        this._return = value;
    }

}
