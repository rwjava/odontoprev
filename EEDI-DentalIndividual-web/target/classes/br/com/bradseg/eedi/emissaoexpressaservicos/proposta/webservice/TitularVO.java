
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for titularVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="titularVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}beneficiarioVO">
 *       &lt;sequence>
 *         &lt;element name="endereco" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}enderecoVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "titularVO", propOrder = {
    "endereco"
})
public class TitularVO
    extends BeneficiarioVO
{

    protected EnderecoVO endereco;

    /**
     * Gets the value of the endereco property.
     * 
     * @return
     *     possible object is
     *     {@link EnderecoVO }
     *     
     */
    public EnderecoVO getEndereco() {
        return endereco;
    }

    /**
     * Sets the value of the endereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnderecoVO }
     *     
     */
    public void setEndereco(EnderecoVO value) {
        this.endereco = value;
    }

}
