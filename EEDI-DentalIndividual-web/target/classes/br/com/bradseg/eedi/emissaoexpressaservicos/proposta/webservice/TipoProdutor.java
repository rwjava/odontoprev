
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoProdutor.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoProdutor">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ANGARIADOR"/>
 *     &lt;enumeration value="CORRETOR"/>
 *     &lt;enumeration value="CORRETOR_MASTER"/>
 *     &lt;enumeration value="GERENTE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoProdutor")
@XmlEnum
public enum TipoProdutor {

    ANGARIADOR,
    CORRETOR,
    CORRETOR_MASTER,
    GERENTE;

    public String value() {
        return name();
    }

    public static TipoProdutor fromValue(String v) {
        return valueOf(v);
    }

}
