
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listarPlanoDisponivelParaVendaResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listarPlanoDisponivelParaVendaResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="planos" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}planoVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listarPlanoDisponivelParaVendaResponse", propOrder = {
    "planos"
})
public class ListarPlanoDisponivelParaVendaResponse {

    protected List<PlanoVO> planos;

    /**
     * Gets the value of the planos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the planos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlanos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlanoVO }
     * 
     * 
     */
    public List<PlanoVO> getPlanos() {
        if (planos == null) {
            planos = new ArrayList<PlanoVO>();
        }
        return this.planos;
    }

}
