
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listarCanaisVendaResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listarCanaisVendaResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="canaisVenda" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}canalVendaVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listarCanaisVendaResponse", propOrder = {
    "canaisVenda"
})
public class ListarCanaisVendaResponse {

    protected List<CanalVendaVO> canaisVenda;

    /**
     * Gets the value of the canaisVenda property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the canaisVenda property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCanaisVenda().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CanalVendaVO }
     * 
     * 
     */
    public List<CanalVendaVO> getCanaisVenda() {
        if (canaisVenda == null) {
            canaisVenda = new ArrayList<CanalVendaVO>();
        }
        return this.canaisVenda;
    }

}
