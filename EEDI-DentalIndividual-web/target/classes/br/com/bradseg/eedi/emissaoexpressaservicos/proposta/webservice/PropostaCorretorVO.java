
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for propostaCorretorVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="propostaCorretorVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}propostaVO">
 *       &lt;sequence>
 *         &lt;element name="cartaoCreditoVO" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}cartaoCreditoVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "propostaCorretorVO", propOrder = {
    "cartaoCreditoVO"
})
public class PropostaCorretorVO
    extends PropostaVO
{

    protected CartaoCreditoVO cartaoCreditoVO;

    /**
     * Gets the value of the cartaoCreditoVO property.
     * 
     * @return
     *     possible object is
     *     {@link CartaoCreditoVO }
     *     
     */
    public CartaoCreditoVO getCartaoCreditoVO() {
        return cartaoCreditoVO;
    }

    /**
     * Sets the value of the cartaoCreditoVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link CartaoCreditoVO }
     *     
     */
    public void setCartaoCreditoVO(CartaoCreditoVO value) {
        this.cartaoCreditoVO = value;
    }

}
