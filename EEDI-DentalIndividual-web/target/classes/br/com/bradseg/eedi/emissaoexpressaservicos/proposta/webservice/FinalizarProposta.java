
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for finalizarProposta complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="finalizarProposta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="proposta" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}propostaCorretorVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "finalizarProposta", propOrder = {
    "proposta"
})
public class FinalizarProposta {

    protected PropostaCorretorVO proposta;

    /**
     * Gets the value of the proposta property.
     * 
     * @return
     *     possible object is
     *     {@link PropostaCorretorVO }
     *     
     */
    public PropostaCorretorVO getProposta() {
        return proposta;
    }

    /**
     * Sets the value of the proposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropostaCorretorVO }
     *     
     */
    public void setProposta(PropostaCorretorVO value) {
        this.proposta = value;
    }

}
