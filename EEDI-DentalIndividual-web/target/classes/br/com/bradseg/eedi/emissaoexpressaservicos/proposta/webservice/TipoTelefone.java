
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTelefone.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTelefone">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RESIDENCIAL"/>
 *     &lt;enumeration value="COMERCIAL"/>
 *     &lt;enumeration value="CELULAR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTelefone")
@XmlEnum
public enum TipoTelefone {

    RESIDENCIAL,
    COMERCIAL,
    CELULAR;

    public String value() {
        return name();
    }

    public static TipoTelefone fromValue(String v) {
        return valueOf(v);
    }

}
