
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for filtroPropostaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="filtroPropostaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="canalVenda" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}canalVendaVO" minOccurs="0"/>
 *         &lt;element name="codigoProposta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="corretor" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}corretorVO" minOccurs="0"/>
 *         &lt;element name="cpfProponente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataFim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeProponente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="situacaoProposta" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}situacaoProposta" minOccurs="0"/>
 *         &lt;element name="tipoProdutor" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}tipoProdutor" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "filtroPropostaVO", propOrder = {
    "canalVenda",
    "codigoProposta",
    "corretor",
    "cpfProponente",
    "dataFim",
    "dataInicio",
    "nomeProponente",
    "situacaoProposta",
    "tipoProdutor"
})
@XmlSeeAlso({
    FiltroRelatorioAcompanhamentoVO.class
})
public class FiltroPropostaVO {

    protected CanalVendaVO canalVenda;
    protected String codigoProposta;
    protected CorretorVO corretor;
    protected String cpfProponente;
    protected String dataFim;
    protected String dataInicio;
    protected String nomeProponente;
    protected SituacaoProposta situacaoProposta;
    protected TipoProdutor tipoProdutor;

    /**
     * Gets the value of the canalVenda property.
     * 
     * @return
     *     possible object is
     *     {@link CanalVendaVO }
     *     
     */
    public CanalVendaVO getCanalVenda() {
        return canalVenda;
    }

    /**
     * Sets the value of the canalVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link CanalVendaVO }
     *     
     */
    public void setCanalVenda(CanalVendaVO value) {
        this.canalVenda = value;
    }

    /**
     * Gets the value of the codigoProposta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProposta() {
        return codigoProposta;
    }

    /**
     * Sets the value of the codigoProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProposta(String value) {
        this.codigoProposta = value;
    }

    /**
     * Gets the value of the corretor property.
     * 
     * @return
     *     possible object is
     *     {@link CorretorVO }
     *     
     */
    public CorretorVO getCorretor() {
        return corretor;
    }

    /**
     * Sets the value of the corretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorretorVO }
     *     
     */
    public void setCorretor(CorretorVO value) {
        this.corretor = value;
    }

    /**
     * Gets the value of the cpfProponente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfProponente() {
        return cpfProponente;
    }

    /**
     * Sets the value of the cpfProponente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfProponente(String value) {
        this.cpfProponente = value;
    }

    /**
     * Gets the value of the dataFim property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataFim() {
        return dataFim;
    }

    /**
     * Sets the value of the dataFim property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataFim(String value) {
        this.dataFim = value;
    }

    /**
     * Gets the value of the dataInicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataInicio() {
        return dataInicio;
    }

    /**
     * Sets the value of the dataInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInicio(String value) {
        this.dataInicio = value;
    }

    /**
     * Gets the value of the nomeProponente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeProponente() {
        return nomeProponente;
    }

    /**
     * Sets the value of the nomeProponente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeProponente(String value) {
        this.nomeProponente = value;
    }

    /**
     * Gets the value of the situacaoProposta property.
     * 
     * @return
     *     possible object is
     *     {@link SituacaoProposta }
     *     
     */
    public SituacaoProposta getSituacaoProposta() {
        return situacaoProposta;
    }

    /**
     * Sets the value of the situacaoProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link SituacaoProposta }
     *     
     */
    public void setSituacaoProposta(SituacaoProposta value) {
        this.situacaoProposta = value;
    }

    /**
     * Gets the value of the tipoProdutor property.
     * 
     * @return
     *     possible object is
     *     {@link TipoProdutor }
     *     
     */
    public TipoProdutor getTipoProdutor() {
        return tipoProdutor;
    }

    /**
     * Sets the value of the tipoProdutor property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoProdutor }
     *     
     */
    public void setTipoProdutor(TipoProdutor value) {
        this.tipoProdutor = value;
    }

}
