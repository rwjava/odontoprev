
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for acompanhamentoPropostaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="acompanhamentoPropostaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agenciaDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codProposta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoAgenciaDebito" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoAgenciaProdutora" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoAssistenteBS" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoCanalVenda" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoGerenteProdutoBVP" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoPlano" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoStatusAtualProposta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cpdAngariador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpdCorretor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpdCorretorMaster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpfBenificiarioTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpfCnpjAngariador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpfCnpjCorretor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpfCnpjCorretorMaster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpfResponsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpfTitularConta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataCriacaoProposta" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}dateTime" minOccurs="0"/>
 *         &lt;element name="dataCriacaoPropostaMaior" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataCriacaoPropostaMenor" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataInicioCriacaoCodigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataInicioStatusAtualProposta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fimPeriodo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inicioPeriodo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="motivoStatusProposta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeAgenciaDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeAgenciaProdutora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeAngariador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeBenificiarioTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeCorretor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeCorretorMaster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeResponsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeTitularConta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="quantidadeVidasProposta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="situacaoProposta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="statusAtualProposta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tipoCobranca" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}tipoCobranca" minOccurs="0"/>
 *         &lt;element name="tipoProdutor" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}tipoProdutor" minOccurs="0"/>
 *         &lt;element name="valorTotalProposta" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "acompanhamentoPropostaVO", propOrder = {
    "agenciaDebito",
    "codProposta",
    "codigoAgenciaDebito",
    "codigoAgenciaProdutora",
    "codigoAssistenteBS",
    "codigoCanalVenda",
    "codigoGerenteProdutoBVP",
    "codigoPlano",
    "codigoStatusAtualProposta",
    "cpdAngariador",
    "cpdCorretor",
    "cpdCorretorMaster",
    "cpfBenificiarioTitular",
    "cpfCnpjAngariador",
    "cpfCnpjCorretor",
    "cpfCnpjCorretorMaster",
    "cpfResponsavel",
    "cpfTitularConta",
    "dataCriacaoProposta",
    "dataCriacaoPropostaMaior",
    "dataCriacaoPropostaMenor",
    "dataInicioCriacaoCodigo",
    "dataInicioStatusAtualProposta",
    "fimPeriodo",
    "inicioPeriodo",
    "motivoStatusProposta",
    "nomeAgenciaDebito",
    "nomeAgenciaProdutora",
    "nomeAngariador",
    "nomeBenificiarioTitular",
    "nomeCorretor",
    "nomeCorretorMaster",
    "nomeResponsavel",
    "nomeTitularConta",
    "protocolo",
    "quantidadeVidasProposta",
    "situacaoProposta",
    "statusAtualProposta",
    "sucursal",
    "tipoCobranca",
    "tipoProdutor",
    "valorTotalProposta"
})
public class AcompanhamentoPropostaVO {

    protected String agenciaDebito;
    protected String codProposta;
    protected Long codigoAgenciaDebito;
    protected Long codigoAgenciaProdutora;
    protected Long codigoAssistenteBS;
    protected Long codigoCanalVenda;
    protected Long codigoGerenteProdutoBVP;
    protected Long codigoPlano;
    protected Integer codigoStatusAtualProposta;
    protected String cpdAngariador;
    protected String cpdCorretor;
    protected String cpdCorretorMaster;
    protected String cpfBenificiarioTitular;
    protected String cpfCnpjAngariador;
    protected String cpfCnpjCorretor;
    protected String cpfCnpjCorretorMaster;
    protected String cpfResponsavel;
    protected String cpfTitularConta;
    protected DateTime dataCriacaoProposta;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCriacaoPropostaMaior;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCriacaoPropostaMenor;
    protected String dataInicioCriacaoCodigo;
    protected String dataInicioStatusAtualProposta;
    protected String fimPeriodo;
    protected String inicioPeriodo;
    protected String motivoStatusProposta;
    protected String nomeAgenciaDebito;
    protected String nomeAgenciaProdutora;
    protected String nomeAngariador;
    protected String nomeBenificiarioTitular;
    protected String nomeCorretor;
    protected String nomeCorretorMaster;
    protected String nomeResponsavel;
    protected String nomeTitularConta;
    protected String protocolo;
    protected String quantidadeVidasProposta;
    protected Integer situacaoProposta;
    protected String statusAtualProposta;
    protected Long sucursal;
    protected TipoCobranca tipoCobranca;
    protected TipoProdutor tipoProdutor;
    protected BigDecimal valorTotalProposta;

    /**
     * Gets the value of the agenciaDebito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenciaDebito() {
        return agenciaDebito;
    }

    /**
     * Sets the value of the agenciaDebito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenciaDebito(String value) {
        this.agenciaDebito = value;
    }

    /**
     * Gets the value of the codProposta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProposta() {
        return codProposta;
    }

    /**
     * Sets the value of the codProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProposta(String value) {
        this.codProposta = value;
    }

    /**
     * Gets the value of the codigoAgenciaDebito property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoAgenciaDebito() {
        return codigoAgenciaDebito;
    }

    /**
     * Sets the value of the codigoAgenciaDebito property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoAgenciaDebito(Long value) {
        this.codigoAgenciaDebito = value;
    }

    /**
     * Gets the value of the codigoAgenciaProdutora property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoAgenciaProdutora() {
        return codigoAgenciaProdutora;
    }

    /**
     * Sets the value of the codigoAgenciaProdutora property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoAgenciaProdutora(Long value) {
        this.codigoAgenciaProdutora = value;
    }

    /**
     * Gets the value of the codigoAssistenteBS property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoAssistenteBS() {
        return codigoAssistenteBS;
    }

    /**
     * Sets the value of the codigoAssistenteBS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoAssistenteBS(Long value) {
        this.codigoAssistenteBS = value;
    }

    /**
     * Gets the value of the codigoCanalVenda property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoCanalVenda() {
        return codigoCanalVenda;
    }

    /**
     * Sets the value of the codigoCanalVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoCanalVenda(Long value) {
        this.codigoCanalVenda = value;
    }

    /**
     * Gets the value of the codigoGerenteProdutoBVP property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoGerenteProdutoBVP() {
        return codigoGerenteProdutoBVP;
    }

    /**
     * Sets the value of the codigoGerenteProdutoBVP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoGerenteProdutoBVP(Long value) {
        this.codigoGerenteProdutoBVP = value;
    }

    /**
     * Gets the value of the codigoPlano property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoPlano() {
        return codigoPlano;
    }

    /**
     * Sets the value of the codigoPlano property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoPlano(Long value) {
        this.codigoPlano = value;
    }

    /**
     * Gets the value of the codigoStatusAtualProposta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoStatusAtualProposta() {
        return codigoStatusAtualProposta;
    }

    /**
     * Sets the value of the codigoStatusAtualProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoStatusAtualProposta(Integer value) {
        this.codigoStatusAtualProposta = value;
    }

    /**
     * Gets the value of the cpdAngariador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpdAngariador() {
        return cpdAngariador;
    }

    /**
     * Sets the value of the cpdAngariador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpdAngariador(String value) {
        this.cpdAngariador = value;
    }

    /**
     * Gets the value of the cpdCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpdCorretor() {
        return cpdCorretor;
    }

    /**
     * Sets the value of the cpdCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpdCorretor(String value) {
        this.cpdCorretor = value;
    }

    /**
     * Gets the value of the cpdCorretorMaster property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpdCorretorMaster() {
        return cpdCorretorMaster;
    }

    /**
     * Sets the value of the cpdCorretorMaster property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpdCorretorMaster(String value) {
        this.cpdCorretorMaster = value;
    }

    /**
     * Gets the value of the cpfBenificiarioTitular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfBenificiarioTitular() {
        return cpfBenificiarioTitular;
    }

    /**
     * Sets the value of the cpfBenificiarioTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfBenificiarioTitular(String value) {
        this.cpfBenificiarioTitular = value;
    }

    /**
     * Gets the value of the cpfCnpjAngariador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfCnpjAngariador() {
        return cpfCnpjAngariador;
    }

    /**
     * Sets the value of the cpfCnpjAngariador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfCnpjAngariador(String value) {
        this.cpfCnpjAngariador = value;
    }

    /**
     * Gets the value of the cpfCnpjCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfCnpjCorretor() {
        return cpfCnpjCorretor;
    }

    /**
     * Sets the value of the cpfCnpjCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfCnpjCorretor(String value) {
        this.cpfCnpjCorretor = value;
    }

    /**
     * Gets the value of the cpfCnpjCorretorMaster property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfCnpjCorretorMaster() {
        return cpfCnpjCorretorMaster;
    }

    /**
     * Sets the value of the cpfCnpjCorretorMaster property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfCnpjCorretorMaster(String value) {
        this.cpfCnpjCorretorMaster = value;
    }

    /**
     * Gets the value of the cpfResponsavel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfResponsavel() {
        return cpfResponsavel;
    }

    /**
     * Sets the value of the cpfResponsavel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfResponsavel(String value) {
        this.cpfResponsavel = value;
    }

    /**
     * Gets the value of the cpfTitularConta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfTitularConta() {
        return cpfTitularConta;
    }

    /**
     * Sets the value of the cpfTitularConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfTitularConta(String value) {
        this.cpfTitularConta = value;
    }

    /**
     * Gets the value of the dataCriacaoProposta property.
     * 
     * @return
     *     possible object is
     *     {@link DateTime }
     *     
     */
    public DateTime getDataCriacaoProposta() {
        return dataCriacaoProposta;
    }

    /**
     * Sets the value of the dataCriacaoProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTime }
     *     
     */
    public void setDataCriacaoProposta(DateTime value) {
        this.dataCriacaoProposta = value;
    }

    /**
     * Gets the value of the dataCriacaoPropostaMaior property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCriacaoPropostaMaior() {
        return dataCriacaoPropostaMaior;
    }

    /**
     * Sets the value of the dataCriacaoPropostaMaior property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCriacaoPropostaMaior(XMLGregorianCalendar value) {
        this.dataCriacaoPropostaMaior = value;
    }

    /**
     * Gets the value of the dataCriacaoPropostaMenor property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCriacaoPropostaMenor() {
        return dataCriacaoPropostaMenor;
    }

    /**
     * Sets the value of the dataCriacaoPropostaMenor property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCriacaoPropostaMenor(XMLGregorianCalendar value) {
        this.dataCriacaoPropostaMenor = value;
    }

    /**
     * Gets the value of the dataInicioCriacaoCodigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataInicioCriacaoCodigo() {
        return dataInicioCriacaoCodigo;
    }

    /**
     * Sets the value of the dataInicioCriacaoCodigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInicioCriacaoCodigo(String value) {
        this.dataInicioCriacaoCodigo = value;
    }

    /**
     * Gets the value of the dataInicioStatusAtualProposta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataInicioStatusAtualProposta() {
        return dataInicioStatusAtualProposta;
    }

    /**
     * Sets the value of the dataInicioStatusAtualProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInicioStatusAtualProposta(String value) {
        this.dataInicioStatusAtualProposta = value;
    }

    /**
     * Gets the value of the fimPeriodo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFimPeriodo() {
        return fimPeriodo;
    }

    /**
     * Sets the value of the fimPeriodo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFimPeriodo(String value) {
        this.fimPeriodo = value;
    }

    /**
     * Gets the value of the inicioPeriodo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioPeriodo() {
        return inicioPeriodo;
    }

    /**
     * Sets the value of the inicioPeriodo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioPeriodo(String value) {
        this.inicioPeriodo = value;
    }

    /**
     * Gets the value of the motivoStatusProposta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoStatusProposta() {
        return motivoStatusProposta;
    }

    /**
     * Sets the value of the motivoStatusProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoStatusProposta(String value) {
        this.motivoStatusProposta = value;
    }

    /**
     * Gets the value of the nomeAgenciaDebito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeAgenciaDebito() {
        return nomeAgenciaDebito;
    }

    /**
     * Sets the value of the nomeAgenciaDebito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeAgenciaDebito(String value) {
        this.nomeAgenciaDebito = value;
    }

    /**
     * Gets the value of the nomeAgenciaProdutora property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeAgenciaProdutora() {
        return nomeAgenciaProdutora;
    }

    /**
     * Sets the value of the nomeAgenciaProdutora property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeAgenciaProdutora(String value) {
        this.nomeAgenciaProdutora = value;
    }

    /**
     * Gets the value of the nomeAngariador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeAngariador() {
        return nomeAngariador;
    }

    /**
     * Sets the value of the nomeAngariador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeAngariador(String value) {
        this.nomeAngariador = value;
    }

    /**
     * Gets the value of the nomeBenificiarioTitular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeBenificiarioTitular() {
        return nomeBenificiarioTitular;
    }

    /**
     * Sets the value of the nomeBenificiarioTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeBenificiarioTitular(String value) {
        this.nomeBenificiarioTitular = value;
    }

    /**
     * Gets the value of the nomeCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCorretor() {
        return nomeCorretor;
    }

    /**
     * Sets the value of the nomeCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCorretor(String value) {
        this.nomeCorretor = value;
    }

    /**
     * Gets the value of the nomeCorretorMaster property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCorretorMaster() {
        return nomeCorretorMaster;
    }

    /**
     * Sets the value of the nomeCorretorMaster property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCorretorMaster(String value) {
        this.nomeCorretorMaster = value;
    }

    /**
     * Gets the value of the nomeResponsavel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeResponsavel() {
        return nomeResponsavel;
    }

    /**
     * Sets the value of the nomeResponsavel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeResponsavel(String value) {
        this.nomeResponsavel = value;
    }

    /**
     * Gets the value of the nomeTitularConta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeTitularConta() {
        return nomeTitularConta;
    }

    /**
     * Sets the value of the nomeTitularConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeTitularConta(String value) {
        this.nomeTitularConta = value;
    }

    /**
     * Gets the value of the protocolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocolo() {
        return protocolo;
    }

    /**
     * Sets the value of the protocolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocolo(String value) {
        this.protocolo = value;
    }

    /**
     * Gets the value of the quantidadeVidasProposta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantidadeVidasProposta() {
        return quantidadeVidasProposta;
    }

    /**
     * Sets the value of the quantidadeVidasProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantidadeVidasProposta(String value) {
        this.quantidadeVidasProposta = value;
    }

    /**
     * Gets the value of the situacaoProposta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSituacaoProposta() {
        return situacaoProposta;
    }

    /**
     * Sets the value of the situacaoProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSituacaoProposta(Integer value) {
        this.situacaoProposta = value;
    }

    /**
     * Gets the value of the statusAtualProposta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusAtualProposta() {
        return statusAtualProposta;
    }

    /**
     * Sets the value of the statusAtualProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusAtualProposta(String value) {
        this.statusAtualProposta = value;
    }

    /**
     * Gets the value of the sucursal property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSucursal() {
        return sucursal;
    }

    /**
     * Sets the value of the sucursal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSucursal(Long value) {
        this.sucursal = value;
    }

    /**
     * Gets the value of the tipoCobranca property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCobranca }
     *     
     */
    public TipoCobranca getTipoCobranca() {
        return tipoCobranca;
    }

    /**
     * Sets the value of the tipoCobranca property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCobranca }
     *     
     */
    public void setTipoCobranca(TipoCobranca value) {
        this.tipoCobranca = value;
    }

    /**
     * Gets the value of the tipoProdutor property.
     * 
     * @return
     *     possible object is
     *     {@link TipoProdutor }
     *     
     */
    public TipoProdutor getTipoProdutor() {
        return tipoProdutor;
    }

    /**
     * Sets the value of the tipoProdutor property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoProdutor }
     *     
     */
    public void setTipoProdutor(TipoProdutor value) {
        this.tipoProdutor = value;
    }

    /**
     * Gets the value of the valorTotalProposta property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorTotalProposta() {
        return valorTotalProposta;
    }

    /**
     * Sets the value of the valorTotalProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorTotalProposta(BigDecimal value) {
        this.valorTotalProposta = value;
    }

}
