
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listarPropostasAcompanhamento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listarPropostasAcompanhamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filtro" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}filtroAcompanhamentoVO" minOccurs="0"/>
 *         &lt;element name="login" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}loginVO" minOccurs="0"/>
 *         &lt;element name="isIntranet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listarPropostasAcompanhamento", propOrder = {
    "filtro",
    "login",
    "isIntranet"
})
public class ListarPropostasAcompanhamento {

    protected FiltroAcompanhamentoVO filtro;
    protected LoginVO login;
    protected boolean isIntranet;

    /**
     * Gets the value of the filtro property.
     * 
     * @return
     *     possible object is
     *     {@link FiltroAcompanhamentoVO }
     *     
     */
    public FiltroAcompanhamentoVO getFiltro() {
        return filtro;
    }

    /**
     * Sets the value of the filtro property.
     * 
     * @param value
     *     allowed object is
     *     {@link FiltroAcompanhamentoVO }
     *     
     */
    public void setFiltro(FiltroAcompanhamentoVO value) {
        this.filtro = value;
    }

    /**
     * Gets the value of the login property.
     * 
     * @return
     *     possible object is
     *     {@link LoginVO }
     *     
     */
    public LoginVO getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoginVO }
     *     
     */
    public void setLogin(LoginVO value) {
        this.login = value;
    }

    /**
     * Gets the value of the isIntranet property.
     * 
     */
    public boolean isIsIntranet() {
        return isIntranet;
    }

    /**
     * Sets the value of the isIntranet property.
     * 
     */
    public void setIsIntranet(boolean value) {
        this.isIntranet = value;
    }

}
