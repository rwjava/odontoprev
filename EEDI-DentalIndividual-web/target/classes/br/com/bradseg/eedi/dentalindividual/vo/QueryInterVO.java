package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class QueryInterVO implements Serializable {
	
	private static final long serialVersionUID = -2489634137134372872L;
	
	private String query;

	/**
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

}
