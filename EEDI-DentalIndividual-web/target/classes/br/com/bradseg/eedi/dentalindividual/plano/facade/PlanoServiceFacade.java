package br.com.bradseg.eedi.dentalindividual.plano.facade;

import java.util.List;

import br.com.bradseg.eedi.dentalindividual.vo.PagamentoVO;
import br.com.bradseg.eedi.dentalindividual.vo.PlanoVO;

/**
 * Interface responsavel por disponibilizar os metodos referentes a plano.
 */
public interface PlanoServiceFacade {

	/**
	 * Metodo responsavel por listar os planos vigente para o canal CORRETORA CALL CENTER.
	 * 
	 * @return List<PlanoVO> - lista de planos vigente do canal CORRETORA CALL CENTER.
	 */
	public List<PlanoVO> listarPlanosVigente();

	public List<PlanoVO> listarPlanoDisponivelParaVenda(String segmento, PagamentoVO pagamento);
	
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano);
}
