package br.com.bradseg.eedi.dentalindividual.canalvenda.converter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.bradseg.eedi.dentalindividual.vo.CanalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CanalVendaVO;

@Scope("prototype")
@Named("CanalConverter")
public class CanalConverter {

	public CanalVendaVO converterCanalAplicacaoEmCanalServico(CanalVO canalAplicacao){
		CanalVendaVO canalVendaServico = new CanalVendaVO();
		canalVendaServico.setCodigo(canalAplicacao.getCodigo());
		canalVendaServico.setNome(canalAplicacao.getNome());
		
		return canalVendaServico;
	}
	
	public CanalVO converterCanalServicoEmCanalAplicacao(CanalVendaVO canalServico){
		CanalVO canalAplicacao = new CanalVO();
		canalAplicacao.setCodigo(canalServico.getCodigo());
		canalAplicacao.setNome(canalServico.getNome());
		
		return canalAplicacao	;
	}
	
	
	public List<CanalVendaVO> converterListaCanalAplicacaoEmListaCanalServico(List<CanalVO> listaCanalAplicacao){
		
		List<CanalVendaVO> listaCanalVendaServico = new ArrayList<CanalVendaVO>();
		
		for(CanalVO canal : listaCanalAplicacao){
			listaCanalVendaServico.add(converterCanalAplicacaoEmCanalServico(canal));
		}
		
		return listaCanalVendaServico;
	}
	
	public List<CanalVO> converterListaCanalServicoEmListaCanalAplicacao(List<CanalVendaVO> listaCanalServico){
		
		List<CanalVO> listaCanalVendaAplicacao = new ArrayList<CanalVO>();
		
		for(CanalVendaVO canal : listaCanalServico){
			listaCanalVendaAplicacao.add(converterCanalServicoEmCanalAplicacao(canal));
		}
		
		return listaCanalVendaAplicacao;
	}
}
