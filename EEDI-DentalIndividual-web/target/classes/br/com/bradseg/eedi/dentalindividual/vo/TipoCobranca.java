package br.com.bradseg.eedi.dentalindividual.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar os tipos de cobran�a.
 *
 */
public enum TipoCobranca {
	
	MENSAL(1, "Mensal"), ANUAL(2, "Anual");
	
	private int codigo;
	private String nome;
	
	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo - codigo.
	 * @param nome - nome.
	 */
	private TipoCobranca(int codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Metodo responsavel por listar os tipos de cobran�a.
	 * 
	 * @return List<TipoCobranca> - lista de tipo de cobran�a.
	 */
	public static List<TipoCobranca> listar(){
		List<TipoCobranca> lista = new ArrayList<TipoCobranca>();
		for (TipoCobranca tipoCobranca : TipoCobranca.values()) {
			lista.add(tipoCobranca);
		}
		return lista;
	}
	
	public static TipoCobranca obterTipoCobrancaPorCodigo(int codigo){
		for (TipoCobranca tipoCobranca : TipoCobranca.values()) {
			if(codigo == tipoCobranca.getCodigo()){
				return tipoCobranca;
			}
		}
		return TipoCobranca.MENSAL;
	}
	
	public static TipoCobranca obterTipoCobrancaPorDescricao(String descricao){
		for (TipoCobranca tipoCobranca : TipoCobranca.values()) {
			if(tipoCobranca.getNome().equalsIgnoreCase(descricao)){
				return tipoCobranca;
			}
		}
		return TipoCobranca.MENSAL;
	}
}
