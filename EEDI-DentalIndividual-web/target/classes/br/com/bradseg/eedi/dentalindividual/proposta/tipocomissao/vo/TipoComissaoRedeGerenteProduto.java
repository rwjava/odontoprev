package br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.vo;

import br.com.bradseg.eedi.dentalindividual.vo.TipoCobranca;

public class TipoComissaoRedeGerenteProduto implements TipoComissao {

	@Override
	public Integer obterTipoComissao(Integer tipoCobranca, boolean corretorNovaComissao) {
		
		Integer tipoComissao = null;
		if (TipoCobranca.MENSAL.getCodigo() == tipoCobranca) {
			tipoComissao = 3;
		} else if (TipoCobranca.ANUAL.getCodigo() == tipoCobranca) {
			tipoComissao = 6;
		}
		return tipoComissao;
		
	}

}
