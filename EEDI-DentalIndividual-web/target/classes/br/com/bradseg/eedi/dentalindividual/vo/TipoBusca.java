package br.com.bradseg.eedi.dentalindividual.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar os tipos de busca.
 *
 */
public enum TipoBusca {

	PROPOSTA(1, "Proposta"), CPF(2, "CPF"), PERIODO(3, "Per�odo");

	private Integer codigo;
	private String nome;

	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo - codigo.
	 * @param nome - nome.
	 */
	private TipoBusca(Integer codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}
	
	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Metodo responsavel por listar os tipos de busca.
	 * 
	 * @return List<TipoBusca> - lista de tipos de busca.
	 */
	public static List<TipoBusca> listar(){
		List<TipoBusca> lista = new ArrayList<TipoBusca>();
		for (TipoBusca tipoBusca : TipoBusca.values()) {
			lista.add(tipoBusca);
		}
		return lista;
	}

	
	public static TipoBusca obterPorCodigo(Integer codigo) {
		for (TipoBusca tipo : TipoBusca.values()) {
			if (tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}
		return TipoBusca.PROPOSTA;
	}

}
