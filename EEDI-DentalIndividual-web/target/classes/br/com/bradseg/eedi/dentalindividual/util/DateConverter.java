package br.com.bradseg.eedi.dentalindividual.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateConverter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateConverter.class);
	
	public static LocalDate parseDate(String localDate) {
		if (StringUtils.isBlank(localDate)) {
			return null;
		}
		
		try {
			return LocalDate.fromDateFields(new SimpleDateFormat(Constantes.DATA_FORMATO_DD_MM_YYYY).parse(localDate));
		} catch (ParseException e) {
			LOGGER.error("Erro ao converter data", e.getMessage());
		}
		return null;
	}
}
