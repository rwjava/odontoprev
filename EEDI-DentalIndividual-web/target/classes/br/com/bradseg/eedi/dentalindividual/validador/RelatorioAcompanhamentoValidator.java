package br.com.bradseg.eedi.dentalindividual.validador;

import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.eedi.dentalindividual.util.ValidacaoUtil;
import br.com.bradseg.eedi.dentalindividual.vo.FiltroRelatorioAcompanhamentoEEDIVO;

@Scope("prototype")
@Named("RelatorioAcompanhamentoValidator")
public class RelatorioAcompanhamentoValidator {

	public void validate(FiltroRelatorioAcompanhamentoEEDIVO filtro) {
		Validador validador = new Validador();

		if(filtro.getCodigoProposta() != null && StringUtils.isNotBlank(filtro.getCodigoProposta()) && ValidacaoUtil.countDigits(filtro.getCodigoProposta()) < 5){
		    validador.adicionarErro("msg.erro.codigoproposta.menorque5digitos");
		}
		
		if(filtro.getFiltroPeriodoVO().getDataInicio() != null && filtro.getFiltroPeriodoVO().getDataFim() != null){
		    Period periodo = new Period(filtro.getFiltroPeriodoVO().getDataInicio() , filtro.getFiltroPeriodoVO().getDataFim(), PeriodType.days());
		    if(periodo.getDays() > 30){
			    validador.adicionarErro("msg.erro.lista.proposta.periodo.maiorque.trinta.dia");
		    }
		}
		
		if(filtro.getCodigoProposta() == null){
		    
		    if(filtro.isIndicadorIntranet()){
				if(filtro.getSucursal() == null && filtro.getCpdCorretor() == null){
				    validador.adicionarErro("msg.erro.filtro.acompanhamento.sucursal.cpdcorretor");
				}
		    }
		}			
		
	}
}
