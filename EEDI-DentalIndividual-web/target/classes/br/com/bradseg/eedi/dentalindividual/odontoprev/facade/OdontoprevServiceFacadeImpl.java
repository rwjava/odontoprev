package br.com.bradseg.eedi.dentalindividual.odontoprev.facade;

import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONPopulator;
import org.apache.struts2.json.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.dentalindividual.odontoprev.vo.AccessTokenVO;
import br.com.bradseg.eedi.dentalindividual.odontoprev.vo.BandeirasVO;
import br.com.bradseg.eedi.dentalindividual.odontoprev.vo.RetornoVO;
import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.TipoCobranca;

/**
 * Classe responsavel por disponibilizar os metodos de neg�cio referentes a odontoprev.
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class OdontoprevServiceFacadeImpl implements OdontoprevServiceFacade {

	@Autowired
	private URL urlOdontoprev;
	// 100.168.100.137

	/**
	 * 
	 */
	/*private static String METODO_LISTAR_BANDEIRAS = "/mip/api/v1/cartao/bandeiras/EEDI";
	private static String METODO_OBTER_ACCESS_TOKEN = "/mip/api/v1/captura/ticket/acessar/EEDI/BDAXXXXXXXXXXXX";
	private static String METODO_VALIDAR_DADOS_CARTAO_CREDITO = "/mip/api/v1/captura/ticket/guardar";
	private static String METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/mip/api/v1/captura/ticket/cobrar";
	private static String METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/mip/api/v1/cobranca/cancelar/parcela";
	private static String METODO_VERIFICAR_CORRETOR_CARTAO_CREDITO = "/mip/api/v1/config/cartaohabilitado?login=";*/

	private static String METODO_VERIFICAR_CORRETOR_CARTAO_CREDITO = "/mip/api/v1/config/cartaohabilitado";
	
	/**
	 * Novo contexto de mapeamento do MIP
	 * Data de altera��o: 31/07/2019
	 */
	private static String METODO_LISTAR_BANDEIRAS = "/BSMO-OdontoPrev/v2/mip/1.0/cartao/bandeiras/EEDI";
	private static String METODO_OBTER_ACCESS_TOKEN = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/acessar/EEDI/BDAXXXXXXXXXXXX";
	private static String METODO_VALIDAR_DADOS_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/guardar";
	private static String METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/cobrar";
	private static String METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/cancelar/parcela";


	private static final Logger LOGGER = LoggerFactory.getLogger(OdontoprevServiceFacadeImpl.class);

	private static final String POST = "POST";

	private static final String GET = "GET";
	
	/**
	 * Metodo responsavel por listar as bandeiras para pagamento com cart�o de cr�dito.
	 * 
	 * @return List<String> - lista de bandeiras.
	 */
	public List<String> listarBandeiras() {

		try {
			BandeirasVO bandeiras = (BandeirasVO) realizarChamadaOdontoprev(METODO_LISTAR_BANDEIRAS, BandeirasVO.class,
					GET, null);

			
			if (null == bandeiras) {
				return new ArrayList<String>();
			} else {

				if (0 == bandeiras.getSucesso()) {
					LOGGER.error("Problema na comunica��o com a Odontoprev: " + bandeiras.getMensagem());
					throw new BusinessException("Problema na comunica��o com a Odontoprev: " + bandeiras.getMensagem());
				}

				return bandeiras.getBandeiras();
			}
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DO METODO DE LISTAR BANDEIRAS: " + e.getMessage());
			return new ArrayList<String>();
		}

	}

	public boolean verificarCorretorHabilitado(String numeroCorretor) {
		boolean corretorPermiteCartao = false;
		
		try {
			RetornoVO retorno = (RetornoVO) realizarChamadaOdontoprevCartao(METODO_VERIFICAR_CORRETOR_CARTAO_CREDITO,
					RetornoVO.class, GET, numeroCorretor);
			if (retorno != null && retorno.isHabilitado()) {
				corretorPermiteCartao = true;
			}
		} catch (Exception e) {
			LOGGER.error("Erro ao chamar servico para verificar corretor logado");
			corretorPermiteCartao = true;
			
			//throw new IntegrationException("Erro ao chamar servico para verificar corretor logado");
		}
		return corretorPermiteCartao;
	}

	/**
	 * Metodo responsavel por obter o access token para pagamento com cart�o de cr�dito
	 * 
	 * @param numeroProposta - n�mero da proposta gerada.
	 * @return String - access token.
	 */
	public String obterAccessToken() {

		try {
			AccessTokenVO accessToken = (AccessTokenVO) realizarChamadaOdontoprev(METODO_OBTER_ACCESS_TOKEN,
					AccessTokenVO.class, GET, null);

			if (null == accessToken) {
				return "";
			} else {
				if (0 == accessToken.getSucesso()) {
					LOGGER.error("Problema na comunica��o com a Odontoprev: " + accessToken.getMensagem());
					throw new IntegrationException("Problema na comunica��o com a Odontoprev. ");
				}
				return accessToken.getAccessToken();
			}
		} catch (Exception e) {	
			LOGGER.error("ERRO NA CHAMADA DO METODO DE OBTER O ACCESS TOKEN: " + e.getMessage());
			throw new IntegrationException(e.getMessage());
		}
	}

	public RetornoVO debitarPrimeiraParcelaCartaoDeCredito(PropostaVO proposta) {

		RetornoVO retorno = null;
		try {

			LOGGER.error(String.format("INICANDO METODO DE DEBITO 1 PARCELA [%s]", proposta.getCodigo()));

			StringBuilder url = new StringBuilder(METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO);

			retorno = (RetornoVO) realizarChamadaOdontoprev(url.toString(), RetornoVO.class, POST,
					preencherParametros(proposta, true, false));

			LOGGER.error("FINALIZANDO METODO DE DEBITO 1 PARCELA");

			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("Problema na comunica��o com a Odontoprev:" + retorno.getMensagem());
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}
			LOGGER.error(String.format("Retorno metodo debitarPrimeiraParcelaCartaoDeCredito: [%s] [%s]", retorno.getSucesso(), retorno.getMensagem()));
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DEBITAR 1 PARCELA: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}
		return retorno;
	}

	public RetornoVO validarDadosCartaoCredito(PropostaVO proposta) {
		
		RetornoVO retorno = null;
		try {

			LOGGER.error(String.format("INICANDO METODO DE VALIDAR DADOS CARTAO [%s]", proposta.getCodigo()));

			StringBuilder url = new StringBuilder(METODO_VALIDAR_DADOS_CARTAO_CREDITO);

			retorno = (RetornoVO) realizarChamadaOdontoprev(url.toString(), RetornoVO.class, POST,
					preencherParametros(proposta, false, true));

			LOGGER.error("FINALIZANDO METODO DE DEBITO DEMAIS PARCELA");

			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}
			LOGGER.error(String.format("Retorno metodo validarDadosCartaoCredito: [%s] [%s]", retorno.getSucesso(), retorno.getMensagem()));
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA VALIDAR DADOS CARTAO: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}

		return retorno;
	}

	public void cancelarDebitoPrimeiraParcelaCartaoDeCredito(PropostaVO proposta) {

		try {

			LOGGER.error("INICANDO METODO DE CANCELAMENTO DE DEBITO 1 PARCELA");

			StringBuilder url = new StringBuilder(METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO);

			RetornoVO retorno = (RetornoVO) realizarChamadaOdontoprev(url.toString(), RetornoVO.class, POST,
					preencherParametrosCancelamento(proposta, false, false));

			LOGGER.error("FINALIZANDO METODO DE CANCELAMENTO DE DEBITO 1 PARCELA");

			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("Problema na comunica��o com a Odontoprev:" + retorno.getMensagem());
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DE CANCELAMENTO DE DEBITO 1 PARCELA: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}

	private Object realizarChamadaOdontoprev(String urlServico, Class<?> classe, String tipoEnvio, String parametros) {
		URL url;
		HttpURLConnection conexao;
		try {
			LOGGER.error("Endereco url odontoprev: " + urlOdontoprev.toString());
			url = new URL(urlOdontoprev.toString() + urlServico);
			//			url = new URL("http://10.137.11.163:8080" + urlServico);
			conexao = (HttpURLConnection) url.openConnection();
			conexao.setRequestMethod(tipoEnvio);
			conexao.setRequestProperty("Accept", "application/json");
			if (null != parametros) {

				LOGGER.error("Parametros : " + parametros);

				conexao.setDoInput(true);
				conexao.setDoOutput(true);
				OutputStream os = conexao.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				writer.write(parametros);
				writer.flush();
				writer.close();
				os.close();
			}
			conexao.connect();

			if (conexao.getResponseCode() != 200) {
				LOGGER.error("ERRO NA CHAMADA: " + conexao.getResponseCode() + " / " + conexao.getResponseMessage());
				throw new BusinessException(conexao.getResponseCode() + " / " + conexao.getResponseMessage());
			}

			return deserializeJSON(inputStreamToString(conexao.getInputStream()), classe);

		} catch (MalformedURLException e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		}
	}

	private Object realizarChamadaOdontoprevCartao(String urlServico, Class<?> classe, String tipoEnvio,
			String parametros) {
		URL url;
		HttpURLConnection conexao;
		try {
			LOGGER.error("Endereco url odontoprev: " + urlOdontoprev.toString());
			url = new URL(urlOdontoprev.toString() + urlServico);
			if (parametros != null) {
				parametros = parametros.trim();
			}
			url = new URL(urlOdontoprev.toString() + urlServico + parametros);
			conexao = (HttpURLConnection) url.openConnection();
			conexao.setRequestMethod(GET);
			conexao.setRequestProperty("Accept", "application/json");
			conexao.setConnectTimeout(10000);
			conexao.connect();

			if (conexao.getResponseCode() != 200) {
				LOGGER.error("ERRO NA CHAMADA: " + conexao.getResponseCode() + " / " + conexao.getResponseMessage());
				throw new BusinessException(conexao.getResponseCode() + " / " + conexao.getResponseMessage());
			}

			return deserializeJSON(inputStreamToString(conexao.getInputStream()), classe);

		} catch (MalformedURLException e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		}
	}

	private String preencherParametros(PropostaVO proposta, boolean indicativoDebitoPrimeiraParcela,
			boolean indicativoValidacaoDados) {

		StringBuilder parametros = new StringBuilder();
		parametros.append("sistema=EEDI&").append("proposta=" + proposta.getCodigo());

		if (indicativoDebitoPrimeiraParcela) {
			parametros.append("&")
					.append("paymentToken=" + proposta.getPagamento().getCartaoCredito().getPaymentToken() + "&")
					.append("nome=" + proposta.getPagamento().getCartaoCredito().getNomeCartao() + "&")
					.append("cpf=" + proposta.getPagamento().getCartaoCredito().getCpfCartao() + "&")
					.append("bandeira=" + proposta.getPagamento().getCartaoCredito().getBandeira());

			Double valor = 0.0D;

			if (TipoCobranca.MENSAL.getCodigo() == proposta.getPagamento().getCodigoTipoCobranca()) {
				valor = proposta.getPlano().getValorPlanoVO().getValorMensalTitular()
						* (proposta.getBeneficiarios().getDependentes().size()
						+ Constantes.QUANTIDADE_BENEFICIARIO_TITULAR);
			} else if (TipoCobranca.ANUAL.getCodigo() == proposta.getPagamento().getCodigoTipoCobranca()) {
				valor = proposta.getPlano().getValorPlanoVO().getValorAnualTitular()
						* (proposta.getBeneficiarios().getDependentes().size()
						+ Constantes.QUANTIDADE_BENEFICIARIO_TITULAR);
			}

			parametros.append("&valor=" + new DecimalFormat("#0.00").format(valor).replace(",", ""));
		}

		if (indicativoValidacaoDados) {
			parametros.append("&")
					.append("paymentToken=" + proposta.getPagamento().getCartaoCredito().getPaymentToken() + "&")
					.append("nome=" + proposta.getPagamento().getCartaoCredito().getNomeCartao() + "&")
					.append("cpf=" + proposta.getPagamento().getCartaoCredito().getCpfCartao() + "&")
					.append("bandeira=" + proposta.getPagamento().getCartaoCredito().getBandeira());
		}

		return parametros.toString();
	}

	private String preencherParametrosCancelamento(PropostaVO proposta, boolean indicativoDebitoPrimeiraParcela,
			boolean indicativoValidacaoDados) {

		StringBuilder parametros = new StringBuilder();
		parametros.append("sistema=EEDI&").append("proposta=" + proposta.getCodigo() + "&").append("numeroParcela=001");

		return parametros.toString();
	}

	private String inputStreamToString(InputStream is) throws IOException {
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

	private Object deserializeJSON(String json, Class<?> classe) {
		Object objeto = null;
		if (json != null && !"".equals(json.trim())) {
			try {

				LOGGER.error("JSON RETORNADO: " + json);

				objeto = classe.newInstance();
				JSONPopulator jsonPopulator = new JSONPopulator();
				Map<?, ?> map = (Map<?, ?>) JSONUtil.deserialize(json);
				jsonPopulator.populateObject(objeto, map);

			} catch (IllegalAccessException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (InstantiationException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (IllegalArgumentException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (InvocationTargetException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (NoSuchMethodException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (IntrospectionException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (JSONException e) {
				LOGGER.error("Problema na comunica��o com a Odontoprev: " + e.getMessage());
				// throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
				return null;
			}
		}
		return objeto;
	}
}
