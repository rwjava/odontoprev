package br.com.bradseg.eedi.dentalindividual.proposta.facade;

import br.com.bradseg.eedi.dentalindividual.vo.TipoSucursalSeguradora;

/**
 * Interface SucursalCorretor
 * 
 * @author EEDI
 */
public interface SucursalCorretorService {
	/**
	 * Valida sucursal do corretor
	 * 
	 * @param codigo C�digo da sucursal
	 * @return True se for v�lida
	 */
	public boolean validarSucursalCorretor(Integer codigo);

	/**
	 * Valida surcursal mercado
	 * 
	 * @param codigo C�digo da sucursal
	 * @return True se for v�lida
	 */
	public boolean validarSucursalMercado(Integer codigo);

	/**
	 * Valida sucursal rede
	 * 
	 * @param codigo C�digo da sucursal
	 * @return True se for v�lida
	 */
	public boolean validarSucursalRede(Integer codigo);

	/**
	 * Valida sucursal corporate
	 * 
	 * @param codigo C�digo da sucursal
	 * @return True se for v�lida
	 */
	public boolean validarSucursalCorporate(Integer codigo);
	
	public TipoSucursalSeguradora obterTipoSucursalPorCodigo(Integer codigo);

}
