package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;

/**
 * Classe responsavel por transportar os dados da pessoa.
 *
 */
public class PessoaVO implements Serializable {

	private static final long serialVersionUID = -2489634137134372872L;
	
	private String nome;
	private String nomeMae;
	private String cpf;
	private String email;
	private LocalDate dataNascimento;
	private String dataNascimentoDependente;
	private String sexo;
	private EstadoCivilVO estadoCivil;
	private EnderecoVO endereco;
	private List<TelefoneVO> telefones;
	
	/**
	 * Construtor padr�o.
	 */
	public PessoaVO(){
		endereco = new EnderecoVO();
		telefones = new ArrayList<TelefoneVO>();
		estadoCivil = new EstadoCivilVO();
//		dataNascimento = new LocalDate(); 
				
	}
	
	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * Retorna nomeMae.
	 *
	 * @return nomeMae - nomeMae.
	 */
	public String getNomeMae() {
		return nomeMae;
	}
	/**
	 * Especifica nomeMae.
	 *
	 * @param nomeMae - nomeMae.
	 */
	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}
	/**
	 * Retorna cpf.
	 *
	 * @return cpf - cpf.
	 */
	public String getCpf() {
		return cpf;
	}
	/**
	 * Especifica cpf.
	 *
	 * @param cpf - cpf.
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	/**
	 * Retorna dataNascimento.
	 *
	 * @return dataNascimento - dataNascimento.
	 */
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	/**
	 * Especifica dataNascimento.
	 *
	 * @param dataNascimento - dataNascimento.
	 */
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	/**
	 * Retorna email.
	 *
	 * @return email - email.
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Especifica email.
	 *
	 * @param email - email.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Retorna sexo.
	 *
	 * @return sexo - sexo.
	 */
	public String getSexo() {
		return sexo;
	}

	/**
	 * Especifica sexo.
	 *
	 * @param sexo - sexo.
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}



	public EstadoCivilVO getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivilVO estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	/**
	 * Retorna endereco.
	 *
	 * @return endereco - endereco.
	 */
	public EnderecoVO getEndereco() {
		return endereco;
	}
	/**
	 * Especifica endereco.
	 *
	 * @param endereco - endereco.
	 */
	public void setEndereco(EnderecoVO endereco) {
		this.endereco = endereco;
	}

	/**
	 * Retorna telefones.
	 *
	 * @return telefones - telefones.
	 */
	public List<TelefoneVO> getTelefones() {
		return telefones;
	}

	/**
	 * Especifica telefones.
	 *
	 * @param telefones - telefones.
	 */
	public void setTelefones(List<TelefoneVO> telefones) {
		this.telefones = telefones;
	}

	/**
	 * @return the dataNascimentoDependente
	 */
	public String getDataNascimentoDependente() {
		return dataNascimentoDependente;
	}

	/**
	 * @param dataNascimentoDependente the dataNascimentoDependente to set
	 */
	public void setDataNascimentoDependente(String dataNascimentoDependente) {
		this.dataNascimentoDependente = dataNascimentoDependente;
	}
}
