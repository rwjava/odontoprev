package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class EstadoCivilVO implements Serializable{


	private static final long serialVersionUID = 1656031512996545267L;
	
	private Integer codigo;
	private String descricao;
	
	public EstadoCivilVO() {
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
