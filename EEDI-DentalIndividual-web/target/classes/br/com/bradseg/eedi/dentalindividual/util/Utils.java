package br.com.bradseg.eedi.dentalindividual.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import br.com.bradseg.eedi.dentalindividual.vo.TipoSucursalSeguradora;
import br.com.bradseg.inet.consultaexpressa.webservice.CorretorSucursal;

public class Utils {

	
	public static void ordernarRemoverSucursaisRepetidas(List<CorretorSucursal> lista) {
		// Ordena a lista para aplicar o remove
		Collections.sort(lista, new Comparator<CorretorSucursal>() {
			public int compare(CorretorSucursal cs1, CorretorSucursal cs2) {
				if(cs1.getDescricaoConcatenada() != null && cs2.getDescricaoConcatenada() != null){
					return cs1.getDescricaoConcatenada().compareTo(cs2.getDescricaoConcatenada());
				}else{
					return -999;
				}
			}
		});

		CorretorSucursal corretorSucursalVerifica = new CorretorSucursal();

		// Remove caso seja igual
		for (Iterator<CorretorSucursal> iterator = lista.iterator(); iterator.hasNext();) {
			CorretorSucursal corretorSucursal = iterator.next();
			if (corretorSucursalVerifica.getDescricaoConcatenada() != null && corretorSucursalVerifica.getDescricaoConcatenada().equals(corretorSucursal.getDescricaoConcatenada())) {
				iterator.remove();
			}
			corretorSucursalVerifica = corretorSucursal;
		}

		// Ordena lista para exibir
		Collections.sort(lista, new Comparator<CorretorSucursal>() {
			public int compare(CorretorSucursal cs1, CorretorSucursal cs2) {
				return cs1.getCodigoSucursal().compareTo(cs2.getCodigoSucursal());
			}
		});
	}
	
	public static TipoSucursalSeguradora retornaTipoSucursalPeloNome(String nomeSucursal){
		
		if(nomeSucursal == null){
			return null;
		}
		
		if(nomeSucursal.toUpperCase().contains("REDE") || nomeSucursal.toUpperCase().contains("BVP")){
			return TipoSucursalSeguradora.REDE;
		}else if(nomeSucursal.toUpperCase().contains("MERCADO") ){
			return TipoSucursalSeguradora.MERCADO;
		}else if(nomeSucursal.toUpperCase().contains("CORPORATE") ){
			return TipoSucursalSeguradora.CORPORATE;
		}
		return null;
	}
}
