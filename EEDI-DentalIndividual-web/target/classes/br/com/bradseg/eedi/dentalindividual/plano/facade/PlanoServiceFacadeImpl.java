package br.com.bradseg.eedi.dentalindividual.plano.facade;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.util.ListUtil;
import br.com.bradseg.eedi.dentalindividual.vo.Banco;
import br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento;
import br.com.bradseg.eedi.dentalindividual.vo.PagamentoVO;
import br.com.bradseg.eedi.dentalindividual.vo.PlanoVO;
import br.com.bradseg.eedi.dentalindividual.vo.TipoSucursalSeguradora;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.BusinessException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.IntegrationException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Proposta100PorCentoCorretorWebService;

/**
 * Classe de neg�cio responsavel por disponibilizar os metodos referentes a plano.
 *
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class PlanoServiceFacadeImpl implements PlanoServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanoServiceFacadeImpl.class);

	@Autowired
	private Proposta100PorCentoCorretorWebService propostaWebService;

	public List<PlanoVO> listarPlanoDisponivelParaVenda(String segmento, PagamentoVO pagamento) {

		List<PlanoVO> listaPlano = new ArrayList<PlanoVO>();
		List<PlanoVO> listaRetorno = new ArrayList<PlanoVO>();

		if ("REDE".equalsIgnoreCase(segmento)) {

			listaPlano =  listarPlanoDisponivelParaVenda(TipoSucursalSeguradora.REDE.getCodigo());
			listaRetorno.addAll(listaPlano);

		} else if ("MERCADO".equalsIgnoreCase(segmento)) {

			listaPlano = listarPlanoDisponivelParaVenda(TipoSucursalSeguradora.MERCADO.getCodigo());
			listaRetorno.addAll(listaPlano);

		} else if ("CORPORATE".equalsIgnoreCase(segmento)) {

			listaPlano = listarPlanoDisponivelParaVenda(TipoSucursalSeguradora.CORPORATE.getCodigo());

			listaRetorno.addAll(listaPlano);

			if (pagamento.getFormaPagamento() != null && pagamento.getContaCorrente() != null && pagamento.getContaCorrente().getBanco() != null && pagamento.getContaCorrente().getBanco().getCodigo()  != null) {
				Integer formaPagamento = pagamento.getFormaPagamento();
				Banco banco = Banco.buscaBancoPor(pagamento.getContaCorrente().getBanco().getCodigo());
				if (FormaPagamento.BOLETO.getCodigo().equals(formaPagamento) || (FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO.getCodigo().equals(formaPagamento) && (Banco.ITAU.equals(banco) || Banco.SANTANDER.equals(banco)))) {
					PlanoVO plano = ListUtil.recuperarObjetoDeLista(listaPlano, "codigo", "3");
					if (null != plano) {
						listaRetorno.add(plano);
					}
				}
			}
			
		}

		return listaRetorno;
	}

	/**
	 * Metodo responsavel por listar os planos vigente para o canal CORRETORA CALL CENTER.
	 * 
	 * @return List<PlanoVO> - lista de planos vigente do canal CORRETORA CALL CENTER.
	 */
	public List<PlanoVO> listarPlanosVigente() {
		try {
			return converterListaDePlanoServicoParaListaDePlanoAplicacao(propostaWebService.listarPlanosVigentePorCanal(Constantes.CODIGO_CANAL_100_POR_CENTO_CORRETOR));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao obter os planos vigente. " + e.getMessage());
			throw new BusinessException(e.getMessage());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao obter os planos vigente. " + e.getMessage());
			throw new IntegrationException(e.getMessage());
		}
		
	}
	
	public List<PlanoVO> listarPlanoDisponivelParaVenda(Integer segmento){
		try {
			return converterListaDePlanoServicoParaListaDePlanoAplicacao(propostaWebService.listarPlanoDisponivelParaVenda(segmento));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao obter os planos vigente para segmento. " + e.getMessage());
			throw new BusinessException(e.getMessage());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao obter os planos vigente para segmento. " + e.getMessage());
			throw new IntegrationException(e.getMessage());
		}
	}
	
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano){
		try {
			return converterObjetoPlanoServicoParaObjetoPlanoAplicacao(propostaWebService.obterPlanoPorCodigo(codigoPlano));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao obter o codigo do plano.");
			throw new BusinessException(e.getMessage());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao obter o codigo do plano.");
			throw new IntegrationException(e.getMessage());
		}
			
	}
	
	public PlanoVO converterObjetoPlanoServicoParaObjetoPlanoAplicacao(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PlanoVO planoServico){
		
		PlanoVO planoAplicacao = new PlanoVO();
		planoAplicacao = new PlanoVO();
		planoAplicacao.setCodigo(planoServico.getCodigo());
		planoAplicacao.setNome(planoServico.getNome());
		planoAplicacao.setDescricaoCarenciaPeriodoMensal(planoServico.getDescricaoCarenciaPeriodoMensal());
		planoAplicacao.setDescricaoCarenciaPeriodoAnual(planoServico.getDescricaoCarenciaPeriodoAnual());
		br.com.bradseg.eedi.dentalindividual.vo.ValorPlanoVO valor = new br.com.bradseg.eedi.dentalindividual.vo.ValorPlanoVO();
		valor.setValorAnualTitular(planoServico.getValorPlanoVO().getValorAnualTitular());
		valor.setValorAnualDependente(planoServico.getValorPlanoVO().getValorAnualDependente());
		valor.setValorMensalTitular(planoServico.getValorPlanoVO().getValorMensalTitular());
		valor.setValorMensalDependente(planoServico.getValorPlanoVO().getValorMensalDependente());
		planoAplicacao.setValorPlanoVO(valor);
		
		return planoAplicacao;
		
	}
	public List<PlanoVO> converterListaDePlanoServicoParaListaDePlanoAplicacao(List<br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PlanoVO> listaServico) {
		List<PlanoVO> listaAplicacao = new ArrayList<PlanoVO>();

		if (listaServico != null && !listaServico.isEmpty()) {
			PlanoVO planoAplicacao = null;
			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PlanoVO planoServico : listaServico) {
				planoAplicacao = new PlanoVO();
				planoAplicacao.setCodigo(planoServico.getCodigo());
				planoAplicacao.setNome(planoServico.getNome());
				planoAplicacao.setDescricaoCarenciaPeriodoMensal(planoServico.getDescricaoCarenciaPeriodoMensal());
				planoAplicacao.setDescricaoCarenciaPeriodoAnual(planoServico.getDescricaoCarenciaPeriodoAnual());
				br.com.bradseg.eedi.dentalindividual.vo.ValorPlanoVO valor = new br.com.bradseg.eedi.dentalindividual.vo.ValorPlanoVO();
				valor.setValorAnualTitular(planoServico.getValorPlanoVO().getValorAnualTitular());
				valor.setValorAnualDependente(planoServico.getValorPlanoVO().getValorAnualDependente());
				valor.setValorMensalTitular(planoServico.getValorPlanoVO().getValorMensalTitular());
				valor.setValorMensalDependente(planoServico.getValorPlanoVO().getValorMensalDependente());
				planoAplicacao.setValorPlanoVO(valor);
				listaAplicacao.add(planoAplicacao);
			}
		}

		return listaAplicacao;
	}

}
