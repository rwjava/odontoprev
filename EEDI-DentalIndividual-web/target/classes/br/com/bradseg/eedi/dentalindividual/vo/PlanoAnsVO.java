
package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class PlanoAnsVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
    protected Long idPlanoAns;
    protected String modalidade;
    protected String nomePlanoAns;
    protected Long planoAns;

    /**
     * Obt�m o valor da propriedade idPlanoAns.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdPlanoAns() {
        return idPlanoAns;
    }

    /**
     * Define o valor da propriedade idPlanoAns.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdPlanoAns(Long value) {
        this.idPlanoAns = value;
    }

    /**
     * Obt�m o valor da propriedade modalidade.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalidade() {
        return modalidade;
    }

    /**
     * Define o valor da propriedade modalidade.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalidade(String value) {
        this.modalidade = value;
    }

    /**
     * Obt�m o valor da propriedade nomePlanoAns.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomePlanoAns() {
        return nomePlanoAns;
    }

    /**
     * Define o valor da propriedade nomePlanoAns.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomePlanoAns(String value) {
        this.nomePlanoAns = value;
    }

    /**
     * Obt�m o valor da propriedade planoAns.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPlanoAns() {
        return planoAns;
    }

    /**
     * Define o valor da propriedade planoAns.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPlanoAns(Long value) {
        this.planoAns = value;
    }

}
