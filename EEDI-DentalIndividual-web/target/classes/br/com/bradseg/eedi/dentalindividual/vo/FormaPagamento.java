package br.com.bradseg.eedi.dentalindividual.vo;

import java.util.ArrayList;
import java.util.List;

import br.com.bradseg.eedi.dentalindividual.util.Constantes;

/**
 * Enum para transportar as formas de pagamento.
 */
public enum FormaPagamento {

	/*BOLETO_BANCARIO(1, "Boleto Banc�rio"), 
	BOLETO_DEMAIS_DEBITO(2, "1� parcela em boleto e demais via d�bito em conta corrente"), 
	DEBITO_AUTOMATICO(3, "D�bito em conta corrente"),
	DEBITO_DEMAIS_BOLETO(4, "1�. parcela via d�bito em conta corrente e demais em boleto");*/

	//	1	Boleto
	//	2	1� parcela em boleto e demais em d�bito autom�tico
	//	3	D�bito Autom�tico
	//	4	1� parcela em d�bito autom�tico e demais em boleto
	//	5	1� parcela em boleto e demais em cart�o de cr�dito
	//	6	1� parcela em d�bito autom�tico e demais em cart�o de cr�dito
	//	7	Cart�o de Cr�dito

	BOLETO(1, "Boleto"), BOLETO_E_DEBITO_AUTOMATICO(2, "1� parcela em boleto e demais em d�bito autom�tico"), DEBITO_AUTOMATICO(
			3, "D�bito Autom�tico"),
	// DEBITO_AUTOMATICO_E_BOLETO(4, "1� parcela em d�bito autom�tico e demais em boleto"),
	CARTAO_CREDITO(5, "Cart�o de Cr�dito"), BOLETO_E_CARTAO_CREDITO(6,
			"1� parcela em boleto e demais em cart�o de cr�dito"), DEBITO_AUTOMATICO_E_CARTAO_CREDITO(7,
			"1� parcela em d�bito autom�tico e demais em cart�o de cr�dito");

	private Integer codigo;
	private String nome;

	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo - codigo.
	 * @param nome - nome.
	 */
	private FormaPagamento(Integer codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public final Integer getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Busca por codigo.
	 * 
	 * @param codigo - codigo da Forma de Pagamento
	 * @return a forma de pagamento
	 */
	public static FormaPagamento buscaPor(Integer codigo) {
		for (FormaPagamento forma : FormaPagamento.values()) {
			if (forma.getCodigo().equals(codigo)) {
				return forma;
			}
		}
		return null;
	}

	public static List<FormaPagamento> listarFormaPagamento(CorretorVO corretorLogado, String segmento,
			boolean acessoCartao) {
		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();
		//CORRETOR COM PAGAMENTO DE CARTAO DE CREDITO DISPONIVEL

		//02176000854 	
		//22648197826
		//05588939000132

		if (acessoCartao) {
			lista = obterListaPorSeguimentoECorretor(segmento);
		} else {
			lista = obterListaPorSeguimento(segmento);
		}

		return lista;
	}

	public static List<FormaPagamento> obterListaPorSeguimentoECorretor(String segmento) {
		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();

		if (Constantes.REDE.equalsIgnoreCase(segmento)) {
			lista.add(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO);
			lista.add(FormaPagamento.CARTAO_CREDITO);
			lista.add(FormaPagamento.BOLETO_E_CARTAO_CREDITO);
		} else if (Constantes.MERCADO.equalsIgnoreCase(segmento)) {
			//lista.add(FormaPagamento.BOLETO);
			lista.add(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO);
			lista.add(FormaPagamento.CARTAO_CREDITO);
			lista.add(FormaPagamento.BOLETO_E_CARTAO_CREDITO);
			// lista.add(FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO);
		} else if (Constantes.CORPORATE.equalsIgnoreCase(segmento)) {
			lista.add(FormaPagamento.BOLETO);
			lista.add(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO);
			lista.add(FormaPagamento.CARTAO_CREDITO);
			lista.add(FormaPagamento.BOLETO_E_CARTAO_CREDITO);
		}
		return lista;
	}

	public static List<FormaPagamento> obterListaPorSeguimento(String segmento) {

		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();

		if (Constantes.REDE.equalsIgnoreCase(segmento)) {
			lista.add(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO);
			lista.add(FormaPagamento.CARTAO_CREDITO);
			lista.add(FormaPagamento.BOLETO_E_CARTAO_CREDITO);
		} else if (Constantes.MERCADO.equalsIgnoreCase(segmento)) {
			//lista.add(FormaPagamento.BOLETO);
			lista.add(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO);
			lista.add(FormaPagamento.CARTAO_CREDITO);
			lista.add(FormaPagamento.BOLETO_E_CARTAO_CREDITO);
			// lista.add(FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO);
		} else if (Constantes.CORPORATE.equalsIgnoreCase(segmento)) {
			lista.add(FormaPagamento.BOLETO);
			lista.add(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO);
			lista.add(FormaPagamento.CARTAO_CREDITO);
			lista.add(FormaPagamento.BOLETO_E_CARTAO_CREDITO);
		}
		return lista;
	}

	public static List<FormaPagamento> obterListaFormaPagamentoCorporateSemBoleto() {

		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();
		lista.add(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO);
		lista.add(FormaPagamento.CARTAO_CREDITO);
		lista.add(FormaPagamento.BOLETO_E_CARTAO_CREDITO);

		return lista;
	}

	public static List<LabelValueVO> obterLista() {

		List<LabelValueVO> lista = new ArrayList<LabelValueVO>();

		for (FormaPagamento forma : FormaPagamento.values()) {
			lista.add(new LabelValueVO(String.valueOf(forma.getCodigo()), forma.getNome()));
		}
		return lista;
	}

	public static List<LabelValueVO> converterEnumEmLabel(List<FormaPagamento> listaFormaPagamento) {

		List<LabelValueVO> lista = new ArrayList<LabelValueVO>();

		for (FormaPagamento forma : listaFormaPagamento) {
			lista.add(new LabelValueVO(String.valueOf(forma.getCodigo()), forma.getNome()));
		}

		return lista;
	}


	/**
	 * Metodo responsavel por listar as formas de pagamento.
	 * 
	 * @return List<FormaPagamento> - lista de forma de pagamento.
	 */
	/*public static List<FormaPagamento> listar(){
		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();
		lista.add(FormaPagamento.DEBITO_AUTOMATICO);
		return lista;
	}*/

}
