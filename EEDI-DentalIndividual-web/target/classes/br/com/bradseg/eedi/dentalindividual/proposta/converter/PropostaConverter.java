package br.com.bradseg.eedi.dentalindividual.proposta.converter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.util.DateConverter;
import br.com.bradseg.eedi.dentalindividual.vo.AgenciaProdutoraVO;
import br.com.bradseg.eedi.dentalindividual.vo.Banco;
import br.com.bradseg.eedi.dentalindividual.vo.DependenteVO;
import br.com.bradseg.eedi.dentalindividual.vo.EstadoCivil;
import br.com.bradseg.eedi.dentalindividual.vo.EstadoCivilVO;
import br.com.bradseg.eedi.dentalindividual.vo.GrauParentesco;
import br.com.bradseg.eedi.dentalindividual.vo.GrauParentescoVO;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.SituacaoProposta;
import br.com.bradseg.eedi.dentalindividual.vo.TipoCobranca;
import br.com.bradseg.eedi.dentalindividual.vo.TipoTelefone;
import br.com.bradseg.eedi.dentalindividual.vo.ValorPlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.AgenciaBancariaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.AngariadorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.BancoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CartaoCreditoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.ContaCorrenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CorretorMasterVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FormaPagamento;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Sexo;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TelefoneVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoSucursalSeguradora;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.UnidadeFederativaVO;

import com.google.common.base.Strings;

/**
 * Classe responsavel por converter o objeto proposta do servi�o para o objeto proposta da aplica��o.
 */
@Scope("prototype")
@Named("PropostaConverter")
public class PropostaConverter {

	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaConverter.class);

	/**
	 * Metodo responsavel por converter o objeto aplica��o para o objeto do servi�o.
	 * 
	 * @param propostaAplicacao - informa��es da proposta a serem convertidas.
	 * @return PropostaVO - informa��es convertidas para o objeto do servi�o.
	 */
	public br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaCorretorVO converterPropostaAplicacaoParaPropostaServico(PropostaVO propostaAplicacao) {

		br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaCorretorVO propostaServico = new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaCorretorVO();
		
		//sucursal
		propostaServico.setSucursalSeguradora(new SucursalSeguradoraVO());
		propostaServico.getSucursalSeguradora().setCodigo(propostaAplicacao.getSucursalSelecionada().getCodigo());

		if (propostaAplicacao.getSucursalSelecionada().getTipo().getCodigo() == 2) {
			propostaServico.getSucursalSeguradora().setTipo(TipoSucursalSeguradora.BVP);
		} else if (propostaAplicacao.getSucursalSelecionada().getTipo().getCodigo() == 3) {
			propostaServico.getSucursalSeguradora().setTipo(TipoSucursalSeguradora.EMISSORA);
		} else if (propostaAplicacao.getSucursalSelecionada().getTipo().getCodigo() == 4) {
			propostaServico.getSucursalSeguradora().setTipo(TipoSucursalSeguradora.CORPORATE);
		}

		if(propostaAplicacao.getSequencial() != null){
			propostaServico.setNumeroSequencial(propostaAplicacao.getSequencial());
		}
		if(propostaAplicacao.getCodigo() != null && !propostaAplicacao.getCodigo().isEmpty()){
			propostaServico.setCodigo(propostaAplicacao.getCodigo());
		}
		
		//corretor
		propostaServico.setCorretor(new CorretorVO());
		propostaServico.getCorretor().setCpd(propostaAplicacao.getCorretor().getCpd().intValue());
		propostaServico.getCorretor().setCpfCnpj(propostaAplicacao.getCorretor().getCpfCnpj());
		propostaServico.setAgenciaProdutora(new AgenciaBancariaVO());
		if(propostaAplicacao.getCorretor().getAgenciaProdutora() != null){
			propostaServico.getAgenciaProdutora().setCodigo(propostaAplicacao.getCorretor().getAgenciaProdutora().intValue());
		}
		
		if (propostaAplicacao.getAngariador() != null) {
			propostaServico.setAngariador(new AngariadorVO());
			propostaServico.getAngariador().setCpd(propostaAplicacao.getCorretor().getCpd().intValue());
			propostaServico.getAngariador().setCpfCnpj(propostaAplicacao.getCorretor().getCpfCnpj());
		}

		if (propostaAplicacao.getCorretorMaster() != null && propostaAplicacao.getCorretorMaster().getCpd() != null) {
			if(propostaAplicacao.getCorretorMaster().getNome() != null){
				propostaServico.setCorretorMaster(new CorretorMasterVO());
				propostaServico.getCorretorMaster().setCpd(propostaAplicacao.getCorretorMaster().getCpd().intValue());
				propostaServico.getCorretorMaster().setNome(propostaAplicacao.getCorretorMaster().getNome());
				propostaServico.getCorretorMaster().setCpfCnpj(propostaAplicacao.getCorretorMaster().getCpfCnpj());
			}
		}
		
		if(StringUtils.isNotEmpty(propostaAplicacao.getCodigoMatriculaGerente())){
			propostaServico.setCodigoMatriculaGerente(propostaAplicacao.getCodigoMatriculaGerente());
		}

		propostaServico.setCodigoMatriculaAssistente(propostaAplicacao.getCorretor().getAssistenteProducao().toString());
		//plano
		propostaServico.setPlanoVO(new PlanoVO());
		propostaServico.getPlanoVO().setCodigo(propostaAplicacao.getPlano().getCodigo());
		//canal de venda
		propostaServico.setCanalVenda(new CanalVendaVO());
		propostaServico.getCanalVenda().setCodigo(Constantes.CODIGO_CANAL_100_POR_CENTO_CORRETOR);

		//		propostaServico.setCodigoTipoComissao100Corretor(propostaAplicacao.getTipoComissao());
		 propostaServico.setCartaoCreditoVO( new CartaoCreditoVO());
		 if(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.CARTAO_CREDITO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())
				 || br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO_E_CARTAO_CREDITO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())
				 || br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())){
			 propostaServico.getCartaoCreditoVO().setNomeCartao(propostaAplicacao.getPagamento().getCartaoCredito().getNomeCartao());
			 propostaServico.getCartaoCreditoVO().setBandeira(propostaAplicacao.getPagamento().getCartaoCredito().getBandeira());
			 propostaServico.getCartaoCreditoVO().setCpfCartao(propostaAplicacao.getPagamento().getCartaoCredito().getCpfCartao());
			 propostaServico.getCartaoCreditoVO().setAccessToken(propostaAplicacao.getPagamento().getCartaoCredito().getAccessToken());
			 propostaServico.getCartaoCreditoVO().setPaymentToken(propostaAplicacao.getPagamento().getCartaoCredito().getPaymentToken());
		 }

		//beneficiarios
		//#Representante Legal
		if (propostaAplicacao.getBeneficiarios().isTitularMenorIdade()) {
			propostaServico.setResponsavelLegal(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.ResponsavelLegalVO());
			propostaServico.getResponsavelLegal().setNome(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getNome());
			propostaServico.getResponsavelLegal().setCpf(br.com.bradseg.eedi.dentalindividual.util.Strings.desnormalizarCPF(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getCpf()));
			if (propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getDataNascimento() != null) {
				propostaServico.getResponsavelLegal().setDataNascimento(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
			}
			//propostaServico.
			propostaServico.getResponsavelLegal().setEmail(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEmail());
			propostaServico.getResponsavelLegal().setEndereco(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.EnderecoVO());
			propostaServico.getResponsavelLegal().getEndereco().setCep(br.com.bradseg.eedi.dentalindividual.util.Strings.normalizarCep(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getCep()));
			propostaServico.getResponsavelLegal().getEndereco().setLogradouro(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getLogradouro());
			propostaServico.getResponsavelLegal().getEndereco().setNumero(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getNumero());
			propostaServico.getResponsavelLegal().getEndereco().setComplemento(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getComplemento());
			propostaServico.getResponsavelLegal().getEndereco().setBairro(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getBairro());
			propostaServico.getResponsavelLegal().getEndereco().setCidade(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getCidade());
			propostaServico.getResponsavelLegal().getEndereco().setUnidadeFederativa(new UnidadeFederativaVO());
			propostaServico.getResponsavelLegal().getEndereco().getUnidadeFederativa().setSigla(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().getEstado());
			propostaServico.getResponsavelLegal().setTelefone(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TelefoneVO());
			propostaServico.getResponsavelLegal().getTelefone().setTipo(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoTelefone.valueOf(TipoTelefone.obterTipoTelefonePorCodigo(Integer.valueOf(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getTipoTelefone())).name()));
			propostaServico.getResponsavelLegal().getTelefone().setDdd(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getDdd());
			propostaServico.getResponsavelLegal().getTelefone().setNumero(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getNumero());
			if (!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getRamal())) {
				propostaServico.getResponsavelLegal().getTelefone().setRamal(Integer.valueOf(propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().get(0).getRamal()));
			}
		}
		//#Titular
		propostaServico.setTitular(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TitularVO());
		propostaServico.getTitular().setNomeMae(propostaAplicacao.getBeneficiarios().getTitular().getNomeMae());
		propostaServico.getTitular().setNome(propostaAplicacao.getBeneficiarios().getTitular().getNome());
		propostaServico.getTitular().setCpf(br.com.bradseg.eedi.dentalindividual.util.Strings.desnormalizarCPF(propostaAplicacao.getBeneficiarios().getTitular().getCpf()));
		if (propostaAplicacao.getBeneficiarios().getTitular().getDataNascimento() != null) {
			propostaServico.getTitular().setDataNascimento(propostaAplicacao.getBeneficiarios().getTitular().getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		}
		propostaServico.getTitular().setEmail(propostaAplicacao.getBeneficiarios().getTitular().getEmail());
		if (propostaAplicacao.getBeneficiarios().getTitular().getSexo() != null) {
			propostaServico.getTitular().setSexo(Sexo.fromValue(propostaAplicacao.getBeneficiarios().getTitular().getSexo()));
		}
		if (propostaAplicacao.getBeneficiarios().getTitular().getEstadoCivil() != null && propostaAplicacao.getBeneficiarios().getTitular().getEstadoCivil().getCodigo() != null) {
			propostaServico.getTitular().setEstadoCivil(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.EstadoCivil.valueOf(EstadoCivil.obterEstadoCivilPorCodigo(propostaAplicacao.getBeneficiarios().getTitular().getEstadoCivil().getCodigo()).name()));
		}
		if(propostaAplicacao.getBeneficiarios().getTitular().getGrauParentesco() != null && propostaAplicacao.getBeneficiarios().getTitular().getGrauParentesco().getCodigo() != null){
			propostaServico.getTitular().setGrauParentescoTitulaProponente(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.GrauParentesco.valueOf(GrauParentesco.obterGrauParentescoPorCodigo(propostaAplicacao.getBeneficiarios().getTitular().getGrauParentesco().getCodigo()).name()));
		}
		if (!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getCns())) {
			propostaServico.getTitular().setNumeroCartaoNacionalSaude(Long.valueOf(propostaAplicacao.getBeneficiarios().getTitular().getCns()));
		}
		if (!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getDnv())) {
			propostaServico.getTitular().setNumeroDeclaracaoNascidoVivo(Long.valueOf(propostaAplicacao.getBeneficiarios().getTitular().getDnv()));
		}
		propostaServico.getTitular().setEndereco(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.EnderecoVO());
		propostaServico.getTitular().getEndereco().setCep(br.com.bradseg.eedi.dentalindividual.util.Strings.normalizarCep(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getCep()));
		propostaServico.getTitular().getEndereco().setLogradouro(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getLogradouro());
		propostaServico.getTitular().getEndereco().setNumero(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getNumero());
		propostaServico.getTitular().getEndereco().setComplemento(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getComplemento());
		propostaServico.getTitular().getEndereco().setBairro(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getBairro());
		propostaServico.getTitular().getEndereco().setCidade(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getCidade());
		propostaServico.getTitular().getEndereco().setUnidadeFederativa(new UnidadeFederativaVO());
		propostaServico.getTitular().getEndereco().getUnidadeFederativa().setSigla(propostaAplicacao.getBeneficiarios().getTitular().getEndereco().getEstado());
		//#Dependentes
		if (propostaAplicacao.getBeneficiarios().getDependentes().size() > 0) {
			for (DependenteVO dependenteAplicacao : propostaAplicacao.getBeneficiarios().getDependentes()) {
				br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DependenteVO dependenteServico = null;
				dependenteServico = new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DependenteVO();
				dependenteServico.setNomeMae(dependenteAplicacao.getNomeMae());
				dependenteServico.setNome(dependenteAplicacao.getNome());
				dependenteServico.setCpf(br.com.bradseg.eedi.dentalindividual.util.Strings.desnormalizarCPF(dependenteAplicacao.getCpf()));
				if (dependenteAplicacao.getDataNascimento() != null) {
					dependenteServico.setDataNascimento(dependenteAplicacao.getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
				}
				if (dependenteAplicacao.getSexo() != null) {
					dependenteServico.setSexo(Sexo.fromValue(dependenteAplicacao.getSexo()));
				}
				if (dependenteAplicacao.getEstadoCivil() != null) {
					dependenteServico.setEstadoCivil(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.EstadoCivil.valueOf(EstadoCivil.obterEstadoCivilPorCodigo(dependenteAplicacao.getEstadoCivil().getCodigo()).name()));
				}
				if (dependenteAplicacao.getGrauParentesco().getCodigo() != null && dependenteAplicacao.getGrauParentesco().getCodigo() != 0) {
					dependenteServico.setGrauParentesco(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.GrauParentesco.valueOf(GrauParentesco.obterGrauParentescoPorCodigo(dependenteAplicacao.getGrauParentesco().getCodigo()).name()));
				}
				if(dependenteAplicacao.getGrauParentescoBeneficiario().getCodigo() != null && dependenteAplicacao.getGrauParentescoBeneficiario().getCodigo() != 0){
					dependenteServico.setGrauParentescoBeneficiario(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.GrauParentesco.valueOf(GrauParentesco.obterGrauParentescoPorCodigo(dependenteAplicacao.getGrauParentescoBeneficiario().getCodigo()).name()));
				}
				
				if (!Strings.isNullOrEmpty(dependenteAplicacao.getCns())) {
					dependenteServico.setNumeroCartaoNacionalSaude(Long.valueOf(dependenteAplicacao.getCns()));
				}
				if (!Strings.isNullOrEmpty(dependenteAplicacao.getDnv())) {
					dependenteServico.setNumeroDeclaracaoNascidoVivo(Long.valueOf(dependenteAplicacao.getDnv()));
				}
				propostaServico.getDependentes().add(dependenteServico);
			}
		}
		//data de ades�o
		propostaServico.setDataAdesao(new LocalDate().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));

		if(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())){
			propostaServico.setFormaPagamento(FormaPagamento.BOLETO_BANCARIO);
		}else if(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())){
			propostaServico.setFormaPagamento(FormaPagamento.BOLETO_DEMAIS_DEBITO);
		}else if(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.DEBITO_AUTOMATICO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())){
			propostaServico.setFormaPagamento(FormaPagamento.DEBITO_AUTOMATICO);
		}else if(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO_E_CARTAO_CREDITO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())){
			propostaServico.setFormaPagamento(FormaPagamento.BOLETO_E_CARTAO_CREDITO);
		}else if(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())){
			propostaServico.setFormaPagamento(FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO);
		}else if(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.CARTAO_CREDITO.getCodigo().equals(propostaAplicacao.getPagamento().getFormaPagamento())){
			propostaServico.setFormaPagamento(FormaPagamento.CARTAO_CREDITO);
		}
		
		
		if(propostaAplicacao.getDataInicioCobranca() != null){
			propostaServico.setDataInicioCobranca(propostaAplicacao.getDataInicioCobranca().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		}
		
		//tipo de cobran�a
		if (propostaAplicacao.getPagamento().getCodigoTipoCobranca() != null) {
			propostaServico.setTipoCobranca(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoCobranca.valueOf(TipoCobranca.obterTipoCobrancaPorCodigo(propostaAplicacao.getPagamento().getCodigoTipoCobranca()).name()));
		}
		//data de cobran�a
//		if (propostaAplicacao.getPagamento().getDataCobranca() != null) {
//			propostaServico.setDataInicioCobranca(propostaAplicacao.getPagamento().getDataCobranca().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
//		}
		//conta corrente
		propostaServico.setProponente(new ProponenteVO());
		propostaServico.getProponente().setCpf(br.com.bradseg.eedi.dentalindividual.util.Strings.desnormalizarCPF(propostaAplicacao.getPagamento().getContaCorrente().getCpf()));
		propostaServico.getProponente().setNome(propostaAplicacao.getPagamento().getContaCorrente().getNome());
		if (propostaAplicacao.getPagamento().getContaCorrente().getDataNascimento() != null) {
			propostaServico.getProponente().setDataNascimento(propostaAplicacao.getPagamento().getContaCorrente().getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		}
		if(propostaAplicacao.getPagamento().getContaCorrente().getBanco() != null && propostaAplicacao.getPagamento().getContaCorrente().getBanco().getCodigo() != null){
			propostaServico.setBanco(new BancoVO());
			propostaServico.getBanco().setCodigo(propostaAplicacao.getPagamento().getContaCorrente().getBanco().getCodigo());
		}
		
		propostaServico.getProponente().setContaCorrente(new ContaCorrenteVO());
		propostaServico.getProponente().getContaCorrente().setAgenciaBancaria(new AgenciaBancariaVO());
		propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().setBanco(new BancoVO());
		if (propostaAplicacao.getPagamento().getContaCorrente().getBanco().getCodigo() != null && propostaAplicacao.getPagamento().getContaCorrente().getBanco().getCodigo() != 0) {
			propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().getBanco().setCodigo(propostaAplicacao.getPagamento().getContaCorrente().getBanco().getCodigo());
		}
		if (StringUtils.isNotBlank(propostaAplicacao.getPagamento().getContaCorrente().getNumeroAgencia()) || StringUtils.isNotBlank(propostaAplicacao.getPagamento().getContaCorrente().getNumeroConta()) || StringUtils.isNotBlank(propostaAplicacao.getPagamento().getContaCorrente().getDigitoVerificadorConta())) {

			propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().setCodigo(Integer.valueOf(propostaAplicacao.getPagamento().getContaCorrente().getNumeroAgencia()));
			propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().setDigito(propostaAplicacao.getPagamento().getContaCorrente().getDigitoVerificadorAgencia());
			propostaServico.getProponente().getContaCorrente().setNumero(Long.valueOf(propostaAplicacao.getPagamento().getContaCorrente().getNumeroConta()));
			propostaServico.getProponente().getContaCorrente().setDigitoVerificador(propostaAplicacao.getPagamento().getContaCorrente().getDigitoVerificadorConta());

		}
		//contato
		if (!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero())) {
			propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(0).setTipoTelefone(String.valueOf(TipoTelefone.CELULAR.getCodigo()));
			propostaServico.getProponente().getTelefones().add(converterTelefoneAplicacaoParaTelefoneServico(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(0)));
		}
		if (!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero())) {
			propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(1).setTipoTelefone(String.valueOf(TipoTelefone.COMERCIAL.getCodigo()));
			propostaServico.getProponente().getTelefones().add(converterTelefoneAplicacaoParaTelefoneServico(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(1)));
		}
		if (!Strings.isNullOrEmpty(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(2).getDddEnumero())) {
			propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(2).setTipoTelefone(String.valueOf(TipoTelefone.RESIDENCIAL.getCodigo()));
			propostaServico.getProponente().getTelefones().add(converterTelefoneAplicacaoParaTelefoneServico(propostaAplicacao.getBeneficiarios().getTitular().getTelefones().get(2)));
		}

		return propostaServico;
	}

	private TelefoneVO converterTelefoneAplicacaoParaTelefoneServico(br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO telefoneAplicacao) {

		TelefoneVO telefoneServico = new TelefoneVO();
		Integer codigoTelefone = null;
		try{
			codigoTelefone = Integer.parseInt(telefoneAplicacao.getTipoTelefone());
		}catch(NumberFormatException e){
			try{
			telefoneServico.setTipo(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoTelefone.valueOf(telefoneAplicacao.getTipoTelefone().toUpperCase()));
			}catch(Exception e1){
				LOGGER.error( String.format("Erro ao converter telefone [%s] ", e1.getMessage()));
			}
		}
		if(codigoTelefone != null){
			telefoneServico.setTipo(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.TipoTelefone.valueOf(TipoTelefone.obterTipoTelefonePorCodigo(Integer.valueOf(telefoneAplicacao.getTipoTelefone())).name()));
		}
		telefoneServico.setDdd(telefoneAplicacao.getDdd());
		telefoneServico.setNumero(telefoneAplicacao.getNumero());

		

		return telefoneServico;
	}

	/**
	 * Metodo responsavel por converter a lista de proposta oriunda do servi�o para uma lista de proposta da aplica��o.
	 * 
	 * @param listaPropostaServico - lista de proposta oriundas do servi�o.
	 * @return List<PropostaVO> - lista de proposta da aplica��o.
	 */
	public List<PropostaVO> converterListaPropostaServicoParaListaPropostaAplicacao(List<br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO> listaPropostaServico) {
		List<PropostaVO> listaPropostaAplicacao = new ArrayList<PropostaVO>();

		PropostaVO propostaAplicacao = null;

		if (listaPropostaServico != null) {
			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO propostaServico : listaPropostaServico) {
				propostaAplicacao = new PropostaVO();

				propostaAplicacao.setSequencial(propostaServico.getNumeroSequencial());
				propostaAplicacao.setCodigo(propostaServico.getCodigo());
				if (null != propostaServico.getTitular().getNome()) {
					propostaAplicacao.getBeneficiarios().getTitular().setNome(propostaServico.getTitular().getNome().toUpperCase());
				}
				if (Strings.isNullOrEmpty(propostaServico.getMovimento().getDescricao())) {
					propostaAplicacao.setStatus(SituacaoProposta.valueOf(propostaServico.getMovimento().getSituacao().name()).getNome().toUpperCase());
				} else {
					propostaAplicacao.setStatus(SituacaoProposta.valueOf(propostaServico.getMovimento().getSituacao().name()).getNome().toUpperCase() + " / " + propostaServico.getMovimento().getDescricao().toUpperCase());
				}
				propostaAplicacao.getPagamento().getContaCorrente().setCpf(propostaServico.getProponente().getCpf());

				propostaAplicacao.getCanal().setCodigo(propostaServico.getCanalVenda().getCodigo());
				propostaAplicacao.getCanal().setNome(propostaServico.getCanalVenda().getNome());

				listaPropostaAplicacao.add(propostaAplicacao);
			}
		}

		return listaPropostaAplicacao;
	}

	/**
	 * Metodo responsavel por converter o objeto proposta da aplica��o para o objeto proposta do servi�o.
	 * 
	 * @param propostaServico - informa��es da proposta do servi�o a serem convertidas.
	 * @return PropostaVO - informa��es da proposta convertidas para o objeto da aplica��o.
	 */
	public PropostaVO converterPropostaServicoParaPropostaAplicacao(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO propostaServico) {
		PropostaVO propostaAplicacao = new PropostaVO();

		propostaAplicacao.setSequencial(propostaServico.getNumeroSequencial());
		propostaAplicacao.setCodigo(propostaServico.getCodigo());
		if (!Strings.isNullOrEmpty(propostaServico.getDataCriacao())) {
			propostaAplicacao.setDataEmissao(DateTimeFormat.forPattern(Constantes.DATA_FORMATO_DD_MM_YYYY).parseDateTime(propostaServico.getDataCriacao()).toLocalDate());
		}
		

		//plano
		propostaAplicacao.setPlano(new br.com.bradseg.eedi.dentalindividual.vo.PlanoVO());
		propostaAplicacao.getPlano().setCodigo(propostaServico.getPlanoVO().getCodigo());
		propostaAplicacao.getPlano().setNome(propostaServico.getPlanoVO().getNome());
		propostaAplicacao.getPlano().setDescricaoCarenciaPeriodoMensal(propostaServico.getPlanoVO().getDescricaoCarenciaPeriodoMensal());
		propostaAplicacao.getPlano().setDescricaoCarenciaPeriodoAnual(propostaServico.getPlanoVO().getDescricaoCarenciaPeriodoAnual());
		
		if(propostaServico.getPlanoVO() != null && propostaServico.getPlanoVO().getValorPlanoVO() != null){
			propostaAplicacao.getPlano().setValorPlanoVO(new ValorPlanoVO());
			propostaAplicacao.getPlano().getValorPlanoVO().setValorMensalTitular(propostaServico.getPlanoVO().getValorPlanoVO().getValorMensalTitular());
			propostaAplicacao.getPlano().getValorPlanoVO().setValorMensalDependente(propostaServico.getPlanoVO().getValorPlanoVO().getValorMensalDependente());
			propostaAplicacao.getPlano().getValorPlanoVO().setValorAnualTitular(propostaServico.getPlanoVO().getValorPlanoVO().getValorAnualTitular());
			propostaAplicacao.getPlano().getValorPlanoVO().setValorAnualDependente(propostaServico.getPlanoVO().getValorPlanoVO().getValorAnualDependente());

		}
		//TODO Nao estara mais utilizando o objeto de plano do eedi servicos
		//		propostaAplicacao.getPlano().setValorPlanoVO(propostaServico.getPlanoVO().getValorPlanoVO());

		if (propostaServico.getResponsavelLegal() == null) {
			propostaAplicacao.getBeneficiarios().setTitularMenorIdade(false);
		} else {
			propostaAplicacao.getBeneficiarios().setTitularMenorIdade(true);
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setCpf(propostaServico.getResponsavelLegal().getCpf());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setNome(propostaServico.getResponsavelLegal().getNome());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setEmail(propostaServico.getResponsavelLegal().getEmail());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setDataNascimento(DateConverter.parseDate(propostaServico.getResponsavelLegal().getDataNascimento()));
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setCep(propostaServico.getResponsavelLegal().getEndereco().getCep());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setLogradouro(propostaServico.getResponsavelLegal().getEndereco().getLogradouro());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setNumero(propostaServico.getResponsavelLegal().getEndereco().getNumero());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setComplemento(propostaServico.getResponsavelLegal().getEndereco().getComplemento());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setBairro(propostaServico.getResponsavelLegal().getEndereco().getBairro());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setCidade(propostaServico.getResponsavelLegal().getEndereco().getCidade());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getEndereco().setEstado(propostaServico.getResponsavelLegal().getEndereco().getUnidadeFederativa().getSigla());

			br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO telefoneAplicacao = new br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO();
			telefoneAplicacao.setTipoTelefone(br.com.bradseg.eedi.dentalindividual.vo.TipoTelefone.valueOf(propostaServico.getResponsavelLegal().getTelefone().getTipo().name()).getNome().toUpperCase());
			telefoneAplicacao.setDddEnumero("(" + propostaServico.getResponsavelLegal().getTelefone().getDdd() + ") " + propostaServico.getResponsavelLegal().getTelefone().getNumero());
			if (null != propostaServico.getResponsavelLegal().getTelefone().getRamal()) {
				telefoneAplicacao.setRamal(propostaServico.getResponsavelLegal().getTelefone().getRamal().toString());
			}
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().setTelefones(new ArrayList<br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO>());
			propostaAplicacao.getBeneficiarios().getRepresentanteLegal().getTelefones().add(telefoneAplicacao);
		}

		//titular
		if (propostaServico.getTitular() != null) {
			propostaAplicacao.getBeneficiarios().getTitular().setCpf(propostaServico.getTitular().getCpf());
			propostaAplicacao.getBeneficiarios().getTitular().setNomeMae(propostaServico.getTitular().getNomeMae());
			propostaAplicacao.getBeneficiarios().getTitular().setNome(propostaServico.getTitular().getNome());
			propostaAplicacao.getBeneficiarios().getTitular().setDataNascimento(DateConverter.parseDate(propostaServico.getTitular().getDataNascimento()));
			propostaAplicacao.getBeneficiarios().getTitular().setEmail(propostaServico.getTitular().getEmail());
			propostaAplicacao.getBeneficiarios().getTitular().setSexo(br.com.bradseg.eedi.dentalindividual.vo.Sexo.obterPorCodigo(propostaServico.getTitular().getSexo().name()).getCodigo());
			
			EstadoCivil estadoCivilTitular = br.com.bradseg.eedi.dentalindividual.vo.EstadoCivil.obterEstadoCivilPorDescricao(propostaServico.getTitular().getEstadoCivil().name());		
			propostaAplicacao.getBeneficiarios().getTitular().setEstadoCivil(new EstadoCivilVO());
			propostaAplicacao.getBeneficiarios().getTitular().getEstadoCivil().setCodigo(estadoCivilTitular.getCodigo());
			propostaAplicacao.getBeneficiarios().getTitular().getEstadoCivil().setDescricao(estadoCivilTitular.getNome());
			
			
			/*Grau de Parentesco Titular tem que alterar para GrauParentescoTitulaProponente na aplica��o EEDI */
			if (null != propostaServico.getTitular().getGrauParentescoTitulaProponente() && propostaServico.getTitular().getGrauParentescoTitulaProponente().name() != null){
				GrauParentesco grauParentescoTitular = br.com.bradseg.eedi.dentalindividual.vo.GrauParentesco.obterGrauParentescoPorDescricao(propostaServico.getTitular().getGrauParentescoTitulaProponente().name());
				propostaAplicacao.getBeneficiarios().getTitular().setGrauParentesco(new GrauParentescoVO());
				propostaAplicacao.getBeneficiarios().getTitular().getGrauParentesco().setCodigo(grauParentescoTitular.getCodigo());
				propostaAplicacao.getBeneficiarios().getTitular().getGrauParentesco().setDescricao(grauParentescoTitular.getNome());
				
			}
			
			if (null != propostaServico.getTitular().getNumeroCartaoNacionalSaude()) {
				propostaAplicacao.getBeneficiarios().getTitular().setCns(propostaServico.getTitular().getNumeroCartaoNacionalSaude().toString());
			}
			if (null != propostaServico.getTitular().getNumeroDeclaracaoNascidoVivo()) {
				propostaAplicacao.getBeneficiarios().getTitular().setDnv(propostaServico.getTitular().getNumeroDeclaracaoNascidoVivo().toString());
			}
			propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setCep(propostaServico.getTitular().getEndereco().getCep());
			propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setLogradouro(propostaServico.getTitular().getEndereco().getLogradouro());
			propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setNumero(propostaServico.getTitular().getEndereco().getNumero());
			propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setComplemento(propostaServico.getTitular().getEndereco().getComplemento());
			propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setBairro(propostaServico.getTitular().getEndereco().getBairro());
			propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setCidade(propostaServico.getTitular().getEndereco().getCidade());
			propostaAplicacao.getBeneficiarios().getTitular().getEndereco().setEstado(propostaServico.getTitular().getEndereco().getUnidadeFederativa().getSigla());

		}

		if (propostaServico.getProponente() != null) {
			if (propostaServico.getProponente().getTelefones() != null && !propostaServico.getProponente().getTelefones().isEmpty()) {
				propostaAplicacao.getBeneficiarios().getTitular().setTelefones(new ArrayList<br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO>());
				br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO telefoneAplicacao = null;
				for (TelefoneVO telefoneServico : propostaServico.getProponente().getTelefones()) {
					telefoneAplicacao = new br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO();

					telefoneAplicacao.setTipoTelefone(br.com.bradseg.eedi.dentalindividual.vo.TipoTelefone.valueOf(telefoneServico.getTipo().name()).getNome().toUpperCase());
					telefoneAplicacao.setDddEnumero("(" + telefoneServico.getDdd() + ") " + telefoneServico.getNumero());
					if (null != telefoneServico.getRamal()) {
						telefoneAplicacao.setRamal(telefoneServico.getRamal().toString());
					}

					propostaAplicacao.getBeneficiarios().getTitular().getTelefones().add(telefoneAplicacao);
				}
			}
		}
		
		//dependentes
		if (propostaServico.getDependentes() != null) {
			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DependenteVO dependenteServico : propostaServico.getDependentes()) {
				DependenteVO dependenteAplicacao = new DependenteVO();

				dependenteAplicacao.setCpf(dependenteServico.getCpf());
				dependenteAplicacao.setNome(dependenteServico.getNome());
				dependenteAplicacao.setNomeMae(dependenteServico.getNomeMae());
				
				dependenteAplicacao.setDataNascimento(DateConverter.parseDate(dependenteServico.getDataNascimento()));
				dependenteAplicacao.setDataNascimentoDependente(dependenteServico.getDataNascimento().toString());
				dependenteAplicacao.setSexo(br.com.bradseg.eedi.dentalindividual.vo.Sexo.obterPorCodigo(dependenteServico.getSexo().name()).getCodigo());
				
				if (null != dependenteServico.getGrauParentesco() && dependenteServico.getGrauParentesco().name() != null){
					GrauParentesco grauParentesco = GrauParentesco.obterGrauParentescoPorDescricao(dependenteServico.getGrauParentesco().name());
					
					dependenteAplicacao.setGrauParentesco(new GrauParentescoVO());
					dependenteAplicacao.getGrauParentesco().setCodigo(grauParentesco.getCodigo());
					dependenteAplicacao.getGrauParentesco().setDescricao(grauParentesco.getNome());
					
				}
			
				if (null != dependenteServico.getGrauParentescoBeneficiario() && dependenteServico.getGrauParentescoBeneficiario().name() != null){
					GrauParentesco grauParentescoBeneficiario = GrauParentesco.obterGrauParentescoPorDescricao(dependenteServico.getGrauParentescoBeneficiario().name());
					
					dependenteAplicacao.setGrauParentescoBeneficiario(new GrauParentescoVO());
					dependenteAplicacao.getGrauParentescoBeneficiario().setCodigo(grauParentescoBeneficiario.getCodigo());
					dependenteAplicacao.getGrauParentescoBeneficiario().setDescricao(grauParentescoBeneficiario.getNome());
				}
				
				
				EstadoCivil estadoCivilDependente = br.com.bradseg.eedi.dentalindividual.vo.EstadoCivil.obterEstadoCivilPorDescricao(propostaServico.getTitular().getEstadoCivil().name());		
				dependenteAplicacao.setEstadoCivil(new EstadoCivilVO());
				dependenteAplicacao.getEstadoCivil().setCodigo(estadoCivilDependente.getCodigo());
				dependenteAplicacao.getEstadoCivil().setDescricao(estadoCivilDependente.getNome());

				
				if (null != dependenteServico.getNumeroCartaoNacionalSaude()) {
					dependenteAplicacao.setCns(dependenteServico.getNumeroCartaoNacionalSaude().toString());
				}
				if (null != dependenteServico.getNumeroDeclaracaoNascidoVivo()) {
					dependenteAplicacao.setDnv(dependenteServico.getNumeroDeclaracaoNascidoVivo().toString());
				}
				propostaAplicacao.getBeneficiarios().getDependentes().add(dependenteAplicacao);
			}
		}

		//pagamento
		if (propostaServico.getTipoCobranca() != null) {
			propostaAplicacao.getPagamento().setCodigoTipoCobranca(TipoCobranca.obterTipoCobrancaPorDescricao(propostaServico.getTipoCobranca().name()).getCodigo());
		}
		if (propostaServico.getFormaPagamento() != null) {
			
			if(FormaPagamento.BOLETO_BANCARIO.equals(propostaServico.getFormaPagamento())){
				propostaAplicacao.getPagamento().setFormaPagamento
				(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO.getCodigo());
			}else if(FormaPagamento.BOLETO_DEMAIS_DEBITO.equals(propostaServico.getFormaPagamento())){
				propostaAplicacao.getPagamento().setFormaPagamento
				(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO.getCodigo());
			}else if(FormaPagamento.BOLETO_E_CARTAO_CREDITO.equals(propostaServico.getFormaPagamento())){
				propostaAplicacao.getPagamento().setFormaPagamento
				(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO_E_CARTAO_CREDITO.getCodigo());
			}else if(FormaPagamento.CARTAO_CREDITO.equals(propostaServico.getFormaPagamento())){
				propostaAplicacao.getPagamento().setFormaPagamento
				(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.CARTAO_CREDITO.getCodigo());
			}else if(FormaPagamento.DEBITO_AUTOMATICO.equals(propostaServico.getFormaPagamento())){
				propostaAplicacao.getPagamento().setFormaPagamento
				(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.DEBITO_AUTOMATICO.getCodigo());
			}else if(FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.equals(propostaServico.getFormaPagamento())){
				propostaAplicacao.getPagamento().setFormaPagamento
				(br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo());
			}
		}
		
		if (null != propostaServico.getDataInicioCobranca()) {
			propostaAplicacao.getPagamento().setDataCobranca(DateTimeFormat.forPattern(Constantes.DATA_FORMATO_DD_MM_YYYY).parseDateTime(propostaServico.getDataInicioCobranca()).toLocalDate());
		}
		if (propostaServico.getBanco() != null && propostaServico.getBanco().getCodigo() != null && propostaServico.getBanco().getCodigo() != 0) {
			propostaAplicacao.getPagamento().getContaCorrente().getBanco().setCodigo(Banco.buscaBancoPor(propostaServico.getBanco().getCodigo()).getCodigo());
		}

		if (propostaServico.getProponente() != null && propostaServico.getProponente().getCpf() != null) {
			propostaAplicacao.getPagamento().getContaCorrente().setCpf(propostaServico.getProponente().getCpf());
			if(propostaServico.getProponente().getNome() != null){
				propostaAplicacao.getPagamento().getContaCorrente().setNome(propostaServico.getProponente().getNome().toUpperCase());
			}
			if(propostaServico.getProponente().getDataNascimento() != null){
				propostaAplicacao.getPagamento().getContaCorrente().setDataNascimento(DateConverter.parseDate(propostaServico.getProponente().getDataNascimento()));
			}
			if(propostaServico.getProponente().getContaCorrente() != null && propostaServico.getProponente().getContaCorrente().getAgenciaBancaria() != null){
				propostaAplicacao.getPagamento().getContaCorrente().setNumeroAgencia(String.valueOf(propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().getCodigo()));
				propostaAplicacao.getPagamento().getContaCorrente().setDigitoVerificadorAgencia(propostaServico.getProponente().getContaCorrente().getAgenciaBancaria().getDigito());
				propostaAplicacao.getPagamento().getContaCorrente().setNumeroConta(String.valueOf(propostaServico.getProponente().getContaCorrente().getNumero()));
				propostaAplicacao.getPagamento().getContaCorrente().setDigitoVerificadorConta(propostaServico.getProponente().getContaCorrente().getDigitoVerificador());
			}
		}
		//corretor
		if (propostaServico.getCorretor() != null) {
			propostaAplicacao.getCorretor().setCpfCnpj(propostaServico.getCorretor().getCpfCnpj());
			propostaAplicacao.getCorretor().setNome(propostaServico.getCorretor().getNome());
			if (null != propostaServico.getCorretor().getCpd()) {
				propostaAplicacao.getCorretor().setCpd(propostaServico.getCorretor().getCpd().longValue());
			}
		}
		if(propostaServico.getAngariador() != null){
			propostaAplicacao.setAngariador(new br.com.bradseg.eedi.dentalindividual.vo.AngariadorVO());
			propostaAplicacao.getAngariador().setCpd(propostaServico.getAngariador().getCpd());
			propostaAplicacao.getAngariador().setCpfCnpj(propostaServico.getAngariador().getCpfCnpj());
			propostaAplicacao.getAngariador().setNome(propostaServico.getAngariador().getNome());
		}
		if(propostaServico.getCorretorMaster() != null){
			propostaAplicacao.setCorretorMaster(new br.com.bradseg.eedi.dentalindividual.vo.CorretorMasterVO());
			propostaAplicacao.getCorretorMaster().setCpd(Long.valueOf(propostaServico.getCorretorMaster().getCpd()));
			propostaAplicacao.getCorretorMaster().setCpfCnpj(propostaServico.getCorretorMaster().getCpfCnpj());
			propostaAplicacao.getCorretorMaster().setNome(propostaServico.getCorretorMaster().getNome());
		}
		
		if(propostaServico.getAgenciaProdutora() != null && propostaServico.getAgenciaProdutora().getCodigo() != null){
			propostaAplicacao.getCorretor().setAgenciaProdutora(Long.valueOf(propostaServico.getAgenciaProdutora().getCodigo()));
		}
		
		
		if (propostaServico.getSucursalSeguradora() != null) {
			propostaAplicacao.getCorretor().setSucursal(propostaServico.getSucursalSeguradora().getCodigo());
		}
		if (!Strings.isNullOrEmpty(propostaServico.getCodigoMatriculaAssistente())) {
			propostaAplicacao.getCorretor().setAssistenteProducao(Long.valueOf(propostaServico.getCodigoMatriculaAssistente()));
			propostaAplicacao.setCodigoMatriculaAssistente(propostaServico.getCodigoMatriculaAssistente());
		}
		if (!Strings.isNullOrEmpty(propostaServico.getCodigoMatriculaGerente())) {
			propostaAplicacao.setCodigoMatriculaGerente(propostaServico.getCodigoMatriculaGerente());
		}

		//status
		propostaAplicacao.setStatus(SituacaoProposta.valueOf(propostaServico.getMovimento().getSituacao().name()).getNome().toUpperCase());

		//canal
		propostaAplicacao.getCanal().setCodigo(propostaServico.getCanalVenda().getCodigo());
		propostaAplicacao.getCanal().setNome(propostaServico.getCanalVenda().getNome());
		
		//agencia produtora
		if(propostaServico.getAgenciaProdutora() != null){
			propostaAplicacao.getPagamento().getContaCorrente().setAgenciaProdutoraVO(new AgenciaProdutoraVO());
			propostaAplicacao.getPagamento().getContaCorrente().getAgenciaProdutoraVO().setNome(propostaServico.getAgenciaProdutora().getNome());
			propostaAplicacao.getPagamento().getContaCorrente().getAgenciaProdutoraVO().setCodigo(propostaServico.getAgenciaProdutora().getCodigo());
			propostaAplicacao.getPagamento().getContaCorrente().getAgenciaProdutoraVO().setDigito(propostaServico.getAgenciaProdutora().getDigito());
		}
		
		return propostaAplicacao;
	}

}
