package br.com.bradseg.eedi.dentalindividual.validador;

import javax.inject.Named;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.dentalindividual.util.ConstantesMensagemErro;
import br.com.bradseg.eedi.dentalindividual.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.TipoBusca;

/**
 * Classe responsavel por validar se todos os dados necessários para a consulta de propostas foram preenchidos.
 * 
 */
@Scope("prototype")
@Named("ListaPropostaValidator")
public class ListaPropostaValidator {

	/**
	 * Valida se todas as informações necessárias para consultar propostas foram preenchidas.
	 * 
	 * 
	 * @param filtro - informações da consulta.
	 */
	public void validate(FiltroPropostaVO filtro) {
		Validador validador = new Validador();

		if(TipoBusca.PROPOSTA.getCodigo().equals(filtro.getTipoBusca())){
			validador.obrigatorio(filtro.getCodigoProposta(), "Código Proposta");
		}
		else if(TipoBusca.CPF.getCodigo().equals(filtro.getTipoBusca())){
			validador.obrigatorio(filtro.getTipoCPF(), "Tipo de CPF");
			validador.cpf(filtro.getCpf(), "CPF");
		}
		else if(TipoBusca.PERIODO.getCodigo().equals(filtro.getTipoBusca())){
			validarPeriodo(validador, filtro.getFiltroPeriodoVO().getDataInicio(), filtro.getFiltroPeriodoVO().getDataFim());
			validador.obrigatorio(filtro.getFiltroPeriodoVO().getStatus(), "Status");
		}

		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}
	}
	
	/**
	 * Validar se todas as informações necessárias para uma consulta por periodo foram preenchidas.
	 * 
	 * @param validador - mensagens de erro.
	 * @param dataInicio - data de inicio da consulta.
	 * @param dataFim - data fim da consulta.
	 */
	private void validarPeriodo(Validador validador, LocalDate dataInicio, LocalDate dataFim){
		boolean dataInicioPreenchida = validador.obrigatorio(dataInicio, "Data Início");
		boolean dataFimPreenchida = validador.obrigatorio(dataFim, "Data Fim");
		if(dataInicioPreenchida && dataFimPreenchida){
			Days intervaloData = Days.daysBetween(dataInicio, dataFim);
			validador.falso(intervaloData.isGreaterThan(Days.days(30)), ConstantesMensagemErro.MSG_ERRO_LISTA_PROPOSTA_PERIODO_MAIOR_30_DIAS);
			validador.falso(dataFim.isBefore(dataInicio), ConstantesMensagemErro.MSG_ERRO_LISTA_PROPOSTA_PERIODO_DATA_FIM_MENOR_QUE_DATA_INICIO);
		}
	}
}