package br.com.bradseg.eedi.dentalindividual.util;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.bradseg.bsad.framework.core.exception.IntegrationException;

/**
 * Classe com m�todos utilitarios para manipula��o de listas.
 */
public class ListUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ListUtil.class);

	/**
	 * M�todo responsavel por ordenar uma lista baseado em um atributo do objeto.
	 * 
	 * @param lista - lista a ser ordenada.
	 * @param atributo - atributo para ordena��o da lista.
	 * @return List<T> - lista ordenada.
	 */
	@SuppressWarnings({ "unchecked" })
	public static <T> List<T> ordenarListaPorAtributo(List<T> lista, String atributo) {

		Collections.sort(lista, new ComparatorChain(Arrays.asList(new BeanComparator(atributo))));

		return lista;
	}

	/**
	 * Metodo responsavel por verificar se um determinado valor existe numa lista.
	 * 
	 * @param lista - lista a ser verificada.
	 * @param propriedadeDaLista - propriedade do objeto contido na lista a ser verificado.
	 * @param valorProcurado - valor a ser verificado.
	 * @return boolean - se o valor for encontrado, retorna true, caso contrario, retorna false.
	 * @throws Exception - Qualquer erro que aconte�a na verifca��o ser� capturado.
	 */
	public static <T> boolean verificarSeValorExisteNaLista(List<T> lista, String propriedadeDaLista,
			String valorProcurado) {
		boolean retorno = false;

		if (!lista.isEmpty()) {
			for (T objeto : lista) {
				try {
					Field atributo = objeto.getClass().getDeclaredField(propriedadeDaLista);
					atributo.setAccessible(true);
					if (valorProcurado.equals(atributo.get(objeto))) {
						retorno = true;
						break;
					}
				} catch (Exception e) {
					LOGGER.error("Erro ao verificar se um valor existe na lista.", e.getMessage());
					throw new IntegrationException("Erro ao verificar se um valor existe na lista.", e);
				}
			}
		}

		return retorno;
	}

	/**
	 * Metodo responsavel por recuperar um objeto da lista.
	 * 
	 * @param lista - lista a ser verificada.
	 * @param propriedadeDaLista - propriedade do objeto contido na lista a ser verificado.
	 * @param valorProcurado - valor a ser verificado.
	 * @return T - objeto recuperado da lista.
	 * @throws Exception - Qualquer erro que aconte�a na verifca��o ser� capturado.
	 */
	public static <T> T recuperarObjetoDeLista(List<T> lista, String propriedadeDaLista, String valorProcurado) {
		T retorno = null;

		if (!lista.isEmpty()) {
			for (T objeto : lista) {
				try {
					Field atributo = objeto.getClass().getDeclaredField(propriedadeDaLista);
					atributo.setAccessible(true);
					if (valorProcurado.equals(atributo.get(objeto).toString())) {
						retorno = objeto;
						break;
					}
				} catch (Exception e) {
					LOGGER.error("Erro ao recuperar objeto da lista.", e.getMessage());
					throw new IntegrationException("Erro ao recuperar objeto da lista.", e);
				}
			}
		}

		return retorno;
	}

}
