package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

/**
 * Classe responsavel pelo transporte de dados do pagamento.
 *
 */
public class PagamentoVO implements Serializable {

	private static final long serialVersionUID = 232137961041195502L;
	
	private Integer codigoTipoCobranca;
	private ContaCorrenteVO contaCorrente;
	private LocalDate dataCobranca;
	private CartaoCreditoVO cartaoCredito;
	private Integer formaPagamento;
	/**
	 * Construtor padr�o.
	 */
	public PagamentoVO(){
		contaCorrente = new ContaCorrenteVO();
//		formaPagamento = String.valueOf(FormaPagamento.DEBITO_AUTOMATICO.getCodigo());
		setCartaoCredito(new CartaoCreditoVO());
	}






	public Integer getCodigoTipoCobranca() {
		return codigoTipoCobranca;
	}






	public void setCodigoTipoCobranca(Integer codigoTipoCobranca) {
		this.codigoTipoCobranca = codigoTipoCobranca;
	}






	/**
	 * Retorna contaCorrente.
	 *
	 * @return contaCorrente - contaCorrente.
	 */
	public ContaCorrenteVO getContaCorrente() {
		return contaCorrente;
	}

	/**
	 * Especifica contaCorrente.
	 *
	 * @param contaCorrente - contaCorrente.
	 */
	public void setContaCorrente(ContaCorrenteVO contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	/**
	 * Retorna dataCobranca.
	 *
	 * @return dataCobranca - dataCobranca.
	 */
	public LocalDate getDataCobranca() {
		return dataCobranca;
	}

	/**
	 * Especifica dataCobranca.
	 *
	 * @param dataCobranca - dataCobranca.
	 */
	public void setDataCobranca(LocalDate dataCobranca) {
		this.dataCobranca = dataCobranca;
	}

	public CartaoCreditoVO getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCreditoVO cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public Integer getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(Integer formaPagamento) {
		this.formaPagamento = formaPagamento;
	}


}
