package br.com.bradseg.eedi.dentalindividual.odontoprev.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccessTokenVO extends RetornoVO implements Serializable {

	private static final long serialVersionUID = 8366778512310686004L;

	private String accessToken;

	/**
	 * Retorna accessToken.
	 *
	 * @return accessToken - accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * Especifica accessToken.
	 *
	 * @param accessToken - accessToken
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
