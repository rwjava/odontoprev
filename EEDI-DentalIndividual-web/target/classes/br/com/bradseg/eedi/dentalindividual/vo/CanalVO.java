package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

/**
 * Classe responsavel pelo transporte de dados do canal.
 *
 */
public class CanalVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer codigo;
	private String nome;
	
	
	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}
	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	/**
	 * Retorna nome.
	 *
	 * @return nome - nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Especifica nome.
	 *
	 * @param nome - nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

}
