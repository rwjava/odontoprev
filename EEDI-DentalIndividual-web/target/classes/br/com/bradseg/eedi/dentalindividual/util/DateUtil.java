package br.com.bradseg.eedi.dentalindividual.util;

import java.sql.Timestamp;
import java.util.Date;

import org.joda.time.LocalDate;

//import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDate;

public class DateUtil {

	public static Date cloneDate(Date date) {
		if (date != null) {
			return new Date(date.getTime());
		}

		return null;
	}


	public static Timestamp cloneTimestamp(Timestamp ts) {
		if (ts != null) {
			return new Timestamp(ts.getTime());
		}

		return null;
	}
	
	public static LocalDate cloneLocalDate(LocalDate date) {
		if (date != null) {
			return date;
		}
		return null;
	}
}
