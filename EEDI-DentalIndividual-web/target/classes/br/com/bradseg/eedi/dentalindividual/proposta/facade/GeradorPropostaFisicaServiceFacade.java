package br.com.bradseg.eedi.dentalindividual.proposta.facade;

import br.com.bradseg.eedi.dentalindividual.support.ReportFileUtil;

public interface GeradorPropostaFisicaServiceFacade {

	public  byte[] gerarPDFProposta(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto);
	public  byte[] gerarPDFPropostaVazia(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto);
	public ReportFileUtil gerarRelatorioPropostaCompleta(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto);
	public ReportFileUtil gerarTermoDeAdesao(Long codigoProposta);
	public ReportFileUtil gerarBoleto(Long codigoProposta);
}
