package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

/**
 *  Classe responsavel pelo trasnporte de dados do filtro da consulta de proposta.
 *
 */
public class FiltroPropostaVO implements Serializable {

	private static final long serialVersionUID = 6756813998260552217L;

	private Integer tipoBusca;
	private String codigoProposta;
	private String cpf;
	private Integer tipoCPF;
	private FiltroPeriodoVO filtroPeriodoVO;
	private String cpfCpnjCorretor;
	private String protocolo;
	private String nomeTitular;
	private CanalVO canal;
	private Integer cpdCorretor;
	private Integer sucursalCorretor;
	private boolean indicadorIntranet;
	
	/**
	 * Construtor alternativo
	 */
	public FiltroPropostaVO() {
		this.filtroPeriodoVO = new FiltroPeriodoVO();
		this.tipoBusca = TipoBusca.PROPOSTA.getCodigo();
		this.canal = new CanalVO();
	}

	/**
	 * Retorna tipoBusca.
	 *
	 * @return tipoBusca - tipoBusca.
	 */
	public Integer getTipoBusca() {
		return tipoBusca;
	}

	/**
	 * Especifica tipoBusca.
	 *
	 * @param tipoBusca - tipoBusca.
	 */
	public void setTipoBusca(Integer tipoBusca) {
		this.tipoBusca = tipoBusca;
	}

	/**
	 * Especifica tipoCPF.
	 *
	 * @param tipoCPF - tipoCPF.
	 */
	public void setTipoCPF(Integer tipoCPF) {
		this.tipoCPF = tipoCPF;
	}

	/**
	 * Retorna codigoProposta.
	 *
	 * @return codigoProposta - codigoProposta.
	 */
	public String getCodigoProposta() {
		return codigoProposta;
	}

	/**
	 * Especifica codigoProposta.
	 *
	 * @param codigoProposta - codigoProposta.
	 */
	public void setCodigoProposta(String codigoProposta) {
		this.codigoProposta = codigoProposta;
	}

	/**
	 * Retorna cpf.
	 *
	 * @return cpf - cpf.
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Especifica cpf.
	 *
	 * @param cpf - cpf.
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Retorna tipoCPF.
	 *
	 * @return tipoCPF - tipoCPF.
	 */
	public Integer getTipoCPF() {
		return tipoCPF;
	}

	/**
	 * Retorna filtroPeriodoVO.
	 *
	 * @return filtroPeriodoVO - filtroPeriodoVO.
	 */
	public FiltroPeriodoVO getFiltroPeriodoVO() {
		return filtroPeriodoVO;
	}

	/**
	 * Especifica filtroPeriodoVO.
	 *
	 * @param filtroPeriodoVO - filtroPeriodoVO.
	 */
	public void setFiltroPeriodoVO(FiltroPeriodoVO filtroPeriodoVO) {
		this.filtroPeriodoVO = filtroPeriodoVO;
	}

	/**
	 * Retorna cpfCpnjCorretor.
	 *
	 * @return cpfCpnjCorretor - cpfCpnjCorretor
	 */
	public String getCpfCpnjCorretor() {
		return cpfCpnjCorretor;
	}

	/**
	 * Especifica cpfCpnjCorretor.
	 *
	 * @param cpfCpnjCorretor - cpfCpnjCorretor
	 */
	public void setCpfCpnjCorretor(String cpfCpnjCorretor) {
		this.cpfCpnjCorretor = cpfCpnjCorretor;
	}

	public String getProtocolo() {
		return protocolo;
	}

	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public CanalVO getCanal() {
		return canal;
	}

	public void setCanal(CanalVO canal) {
		this.canal = canal;
	}

	public Integer getCpdCorretor() {
		return cpdCorretor;
	}

	public void setCpdCorretor(Integer cpdCorretor) {
		this.cpdCorretor = cpdCorretor;
	}

	public Integer getSucursalCorretor() {
		return sucursalCorretor;
	}

	public void setSucursalCorretor(Integer sucursalCorretor) {
		this.sucursalCorretor = sucursalCorretor;
	}

	public boolean isIndicadorIntranet() {
		return indicadorIntranet;
	}

	public void setIndicadorIntranet(boolean indicadorIntranet) {
		this.indicadorIntranet = indicadorIntranet;
	}

	
	
	
}
