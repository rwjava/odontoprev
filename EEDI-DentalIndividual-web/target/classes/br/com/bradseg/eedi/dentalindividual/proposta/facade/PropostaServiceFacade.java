package br.com.bradseg.eedi.dentalindividual.proposta.facade;

import java.util.List;

import br.com.bradseg.eedi.dentalindividual.proposta.presentation.model.QueryModel;
import br.com.bradseg.eedi.dentalindividual.vo.CorretorVO;
import br.com.bradseg.eedi.dentalindividual.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;

/**
 * Interface de negocio responsavel por disponibilizar os metodos referentes a
 * proposta.
 */
public interface PropostaServiceFacade {

	public Long imprimirPropostaVazia(PropostaVO proposta);

	
	public byte[] gerarPDFPropostaVazia(PropostaVO proposta);

	
	public byte[] gerarPDFProposta(PropostaVO proposta);

	/**
	 * Metodo responsavel por salvar rascunho de uma proposta.
	 * 
	 * @param proposta
	 *            - informações da proposta a serem gravadas.
	 * @return Long - sequencial da proposta gerado.
	 * @throws BusinessException_Exception
	 */
	public Long salvarRascunhoProposta(PropostaVO proposta);
	

	
	//QueryModel query(String sql);

	/**
	 * Metodo responsavel por finalizar uma proposta.
	 * 
	 * @param proposta
	 *            - informações da proposta a serem gravadas.
	 * @return Long - sequencial da proposta gerado.
	 * @throws BusinessException_Exception
	 */
	public Long finalizarProposta(PropostaVO proposta);

	/**
	 * Metodo responsavel por adicionar um dependente na proposta.
	 * 
	 * @param proposta
	 *            - informações da proposta.
	 */
	public void adicionarDependente(PropostaVO proposta);

	/**
	 * Metodo responsavel por consultar as propostas de acordo com o filtro
	 * informado.
	 * 
	 * @param filtro
	 *            - informações a serem levadas em consideração no momento da
	 *            consulta.
	 * @param corretor
	 *            - informações do corretor.
	 * @return List<PropostaVO> - lista com as propostas encontradas.
	 */
	public List<PropostaVO> consultarPropostaPorFiltro(FiltroPropostaVO filtro, CorretorVO corretor);

	/**
	 * Metodo responsavel por obter uma proposta atraves do sequencial da
	 * proposta.
	 * 
	 * @param sequencial
	 *            - sequencial da proposta.
	 * @return PropostaVO - informações da proposta encontrada.
	 */
	public PropostaVO obterPropostaPorSequencial(Long sequencial);
	
	public br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO obterPropostaDoServicoPorSeguencial(Long sequencial);
	

	/**
	 * Metodo responsavel por obter uma proposta atraves do codigo da
	 * proposta.
	 * 
	 * @param codigoProposta
	 *            - codigo da proposta.
	 * @return PropostaVO - informações da proposta encontrada.
	 */
	public PropostaVO obterPropostaPorCodigo(String codigoProposta);

	/**
	 * Metodo responsavel por cancelar a proposta atraves do sequencial da
	 * proposta.
	 * 
	 * @param sequencialProposta
	 *            - sequencial da proposta a ser cancelado.
	 */
	public void cancelarProposta(Long sequencialProposta);


	public List<String> consultarProposta(String textoConsulta);
	
	public boolean validarDadosCartaoCredito(PropostaVO proposta);

}
