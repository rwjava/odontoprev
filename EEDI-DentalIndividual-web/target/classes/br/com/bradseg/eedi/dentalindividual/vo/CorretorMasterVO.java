package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class CorretorMasterVO extends CorretorVO implements Serializable{

	private static final long serialVersionUID = -2035963958965657633L;

	
	public CorretorMasterVO(){
		setProdutor(new ProdutorVO());
		getProdutor().setTipoProdutor(TipoProdutor.CORRETOR_MASTER);
	}
	
}
