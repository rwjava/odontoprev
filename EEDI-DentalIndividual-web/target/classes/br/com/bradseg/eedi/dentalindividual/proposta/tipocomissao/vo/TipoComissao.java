package br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.vo;

public interface TipoComissao {

	public Integer obterTipoComissao(Integer tipoCobranca, boolean corretorTest);
}
