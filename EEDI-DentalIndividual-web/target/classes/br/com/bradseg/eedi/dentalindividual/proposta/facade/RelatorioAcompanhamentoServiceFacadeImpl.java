package br.com.bradseg.eedi.dentalindividual.proposta.facade;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.dentalindividual.support.ReportFileUtil;
import br.com.bradseg.eedi.dentalindividual.support.ReportUtil;
import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.util.FormatacaoUtil;
import br.com.bradseg.eedi.dentalindividual.util.ValidacaoUtil;
import br.com.bradseg.eedi.dentalindividual.validador.RelatorioAcompanhamentoValidator;
import br.com.bradseg.eedi.dentalindividual.vo.AcompanhamentoPropostaEEDIVO;
import br.com.bradseg.eedi.dentalindividual.vo.FiltroRelatorioAcompanhamentoEEDIVO;
import br.com.bradseg.eedi.dentalindividual.vo.ValorRelatorioAcompanhamentoPropostaEEDIVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FiltroRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Proposta100PorCentoCorretorWebService;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class RelatorioAcompanhamentoServiceFacadeImpl implements RelatorioAcompanhamentoServiceFacade {

	@Autowired
	private RelatorioAcompanhamentoValidator relatorioAcompanhamentoValidator;

	@Autowired
	private Proposta100PorCentoCorretorWebService propostaWebService;

	private static final Logger LOGGER = LoggerFactory.getLogger(RelatorioAcompanhamentoServiceFacadeImpl.class);

	private static final String VLR_TOTAL_PROPOSTAS = "vlr_total_propostas";
	private static final String VLR_TOTAL_PROPOSTAS_SUCURSAL = "vlr_total_propostas_sucursal";
	private static final String VLR_TOTAL_PROPOSTAS_CORRETOR = "vlr_total_propostas_corretor";
	private static final String VLR_TOTAL_PROPOSTAS_AG_DEBITO = "vlr_total_propostas_ag_debito";
	private static final String VLR_TOTAL_PROPOSTAS_AG_PROD = "vlr_total_propostas_ag_prod";
	private static final String VLR_TOTAL_PROPOSTAS_ASSIST_BS = "vlr_total_propostas_assistente_bs";
	private static final String VLR_TOTAL_PROPOSTAS_GER_PROD_BVP = "vlr_total_propostas_ger_prod_bvp";

	private static final String PROPOSTA = "proposta";

	private static final int TAM_PADRAO_STRING_BUFFER = 50000;
	private static final String CARACTER_ESPECIAL = ";";

	public List<AcompanhamentoPropostaEEDIVO> listarPropostasRelatorioAcompanhamentoPorFiltro(FiltroRelatorioAcompanhamentoEEDIVO filtro) {

		relatorioAcompanhamentoValidator.validate(filtro);
		List<AcompanhamentoPropostaEEDIVO> listaAcompanhamento = null;

		FiltroAcompanhamentoVO filtroAcompanhamento = converterObjetoAplicacaoParaObjetoServico(filtro);

		try {
			listaAcompanhamento = converterObjetoEEDIServicosParaEEDI(propostaWebService.listarPropostasAcompanhamento(filtroAcompanhamento, new LoginVO(), filtro.isIndicadorIntranet()));
		} catch (Exception e) {
			LOGGER.error("ERRO AO CHAMAR METODO PARA LISTAR PROPOSTAS ACOMPANHAMENTO: " + e.getMessage());
		}
		return listaAcompanhamento;

	}

//	public ReportFile gerarPDFRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoEEDIVO filtro, LoginVO login) {
//
//		relatorioAcompanhamentoValidator.validate(filtro);
//		List<AcompanhamentoPropostaEEDIVO> listaPropostasAcompanhamento = listarPropostasRelatorioAcompanhamentoPorFiltro(filtro);
//		//			relatorioAcompanhamentoPropostaVO.setValorRelatorioAcompanhamentoPropostaVO(obterValoresPropostas(listaPropostasAcompanhamento));
//		ReportFile reportFile = new ReportFile();
//		reportFile.setFileName("relatorioAcompanhamento.pdf");
//		reportFile.adicionarRelatorio(new ReportFile("rela_acompanhamento.jrxml", listaPropostasAcompanhamento, montarParametrosPDF(listaPropostasAcompanhamento)));
//		return reportFile;
//	}
	
	
	public ReportFileUtil gerarPDFRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoEEDIVO filtro, LoginVO login) {

		relatorioAcompanhamentoValidator.validate(filtro);
		List<AcompanhamentoPropostaEEDIVO> listaPropostasAcompanhamento = listarPropostasRelatorioAcompanhamentoPorFiltro(filtro);
		//			relatorioAcompanhamentoPropostaVO.setValorRelatorioAcompanhamentoPropostaVO(obterValoresPropostas(listaPropostasAcompanhamento));
		ReportFileUtil reportFile = new ReportFileUtil();
		reportFile.setFileName("relatorioAcompanhamento.pdf");
		reportFile.adicionarRelatorio(new ReportUtil("rela_acompanhamento.jrxml", listaPropostasAcompanhamento, montarParametrosPDF(listaPropostasAcompanhamento)));
		return reportFile;
	}

	public File gerarArquivoCSVRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoEEDIVO filtro, LoginVO login) {
		File arquivoCSV = null;
		relatorioAcompanhamentoValidator.validate(filtro);

		List<AcompanhamentoPropostaEEDIVO> listaPropostasAcompanhamento = listarPropostasRelatorioAcompanhamentoPorFiltro(filtro);

		try {
			arquivoCSV = gerarArquivo(listaPropostasAcompanhamento);
		} catch (IOException e) {
			LOGGER.error("Erro ao gerar arquivo CSV. : " + e.getMessage());
		}

		return arquivoCSV;

	}

	private File gerarArquivo(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamentoPropostaVO) throws IOException {
		SimpleDateFormat formataDataNomeArquivo = new SimpleDateFormat("ddMMyyyy");
		String nomeArquivo = "AcompanhamentoProposta" + formataDataNomeArquivo.format(new Date()) + ".csv";
		File arquivoCSV = new File(nomeArquivo);
		FileWriter gravarArquivoCSV = new FileWriter(arquivoCSV);
		BufferedWriter escreveArquivoCSV = new BufferedWriter(gravarArquivoCSV);
		int i = 0;
		// atualizarValores(listaAcompanhamentoPropostaVO);
		for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO : listaAcompanhamentoPropostaVO) {
			if (i == 0) {
				// Escreve cabe�alho
				StringBuffer cabecalho = new StringBuffer(TAM_PADRAO_STRING_BUFFER);
				cabecalho.append("C�digo Proposta;");
				cabecalho.append("CPF Titular da Conta;");
				cabecalho.append("Nome do Titular da Conta;");
				cabecalho.append("CPF do Respons�vel;");
				cabecalho.append("Nome do Respons�vel;");
				cabecalho.append("CPF Benefici�rio Titular;");
				cabecalho.append("Nome Benefici�rio Titular;");
				cabecalho.append("Sucursal;");
				cabecalho.append("CPD do Corretor;");
				cabecalho.append("CNPJ/CPF do Corretor Principal;");
				cabecalho.append("Nome do Corretor;");
				cabecalho.append("CPD do Angariador;");
				cabecalho.append("CNPJ/CPF do Angariador;");
				cabecalho.append("Nome do Angariador;");
				cabecalho.append("CPD do Corretor Master;");
				cabecalho.append("CNPJ/CPF do Corretor Master;");
				cabecalho.append("Nome do Corretor Master;");
				cabecalho.append("C�digo Ag�ncia de D�bito;");
				cabecalho.append("Nome Ag�ncia de D�bito;");
				cabecalho.append("C�digo Ag�ncia Produtora;");
				cabecalho.append("Nome Ag�ncia Produtora;");
				cabecalho.append("C�digo Assistente BS;");
				cabecalho.append("C�digo Gerente Produto BVP;");
				cabecalho.append("Valor Total da Proposta;");
				cabecalho.append("Quantidade de Vidas da Proposta;");
				cabecalho.append("Data da Cria��o do C�digo da Proposta;");
				cabecalho.append("Data do In�cio do Status Atual da Proposta;");
				cabecalho.append("Status Atual da Proposta;");
				cabecalho.append("Motivo Status Proposta;");

				escreveArquivoCSV.write(cabecalho.toString());
				escreveArquivoCSV.newLine();
				escreveArquivoCSV.flush();
			}

			StringBuffer dados = new StringBuffer(TAM_PADRAO_STRING_BUFFER);
			dados.append(acompanhamentoPropostaVO.getCodProposta());
			dados.append(CARACTER_ESPECIAL);
			// CPF Titular da conta
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpfTitularConta()) || acompanhamentoPropostaVO.getCpfTitularConta().equals("00000000000")) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCpfTitularConta());
				dados.append(CARACTER_ESPECIAL);
			}
			// Nome do Titular da Conta
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getNomeTitularConta())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getNomeTitularConta());
				dados.append(CARACTER_ESPECIAL);
			}
			// CPF do Respons�vel
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpfResponsavel())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCpfResponsavel());
				dados.append(CARACTER_ESPECIAL);
			}
			// Nome do Respons�vel
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getNomeResponsavel())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getNomeResponsavel());
				dados.append(CARACTER_ESPECIAL);
			}
			// CPF Benefici�rio Titular
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpfBenificiarioTitular())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCpfBenificiarioTitular());
				dados.append(CARACTER_ESPECIAL);
			}
			// Nome Benefici�rio Titular
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getNomeBenificiarioTitular())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getNomeBenificiarioTitular());
				dados.append(CARACTER_ESPECIAL);
			}
			// Sucursal
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getSucursal())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getSucursal());
				dados.append(CARACTER_ESPECIAL);
			}
			// CPD Corretor
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpdCorretor())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCpdCorretor());
				dados.append(CARACTER_ESPECIAL);
			}
			// CPF/CNPJ corretor Principal
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpfCnpjCorretor())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				if (acompanhamentoPropostaVO.getCpfCnpjCorretor().length() == 11) {
					dados.append(FormatacaoUtil.formataCPF(acompanhamentoPropostaVO.getCpfCnpjCorretor()));
					dados.append(CARACTER_ESPECIAL);
				} else {
					dados.append(FormatacaoUtil.formataCNPJ(acompanhamentoPropostaVO.getCpfCnpjCorretor()));
					dados.append(CARACTER_ESPECIAL);
				}
			}
			// Nome do Corretor
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getNomeCorretor())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getNomeCorretor());
				dados.append(CARACTER_ESPECIAL);
			}
			// CPD do Angariador
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpdAngariador())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCpdAngariador());
				dados.append(CARACTER_ESPECIAL);
			}
			// CNPJ/CPF do Angariador
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpfCnpjAngariador())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				if (acompanhamentoPropostaVO.getCpfCnpjCorretor().length() == 11) {
					dados.append(FormatacaoUtil.formataCPF(acompanhamentoPropostaVO.getCpfCnpjAngariador()));
					dados.append(CARACTER_ESPECIAL);
				} else {
					dados.append(FormatacaoUtil.formataCNPJ(acompanhamentoPropostaVO.getCpfCnpjAngariador()));
					dados.append(CARACTER_ESPECIAL);
				}
			}
			// Nome do Angariador
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getNomeAngariador())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getNomeAngariador());
				dados.append(CARACTER_ESPECIAL);
			}
			// CPD do Corretor Master
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpdCorretorMaster())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCpdCorretorMaster());
				dados.append(CARACTER_ESPECIAL);
			}
			// CNPJ/CPF do Corretor Master
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCpfCnpjCorretorMaster())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				if (acompanhamentoPropostaVO.getCpfCnpjCorretorMaster().length() == 11) {
					dados.append(FormatacaoUtil.formataCPF(acompanhamentoPropostaVO.getCpfCnpjCorretorMaster()));
					dados.append(CARACTER_ESPECIAL);
				} else {
					dados.append(FormatacaoUtil.formataCNPJ(acompanhamentoPropostaVO.getCpfCnpjCorretorMaster()));
					dados.append(CARACTER_ESPECIAL);
				}
			}
			// Nome do Corretor Master
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getNomeCorretorMaster())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getNomeCorretorMaster());
				dados.append(CARACTER_ESPECIAL);
			}
			// C�digo Agencia de D�bito
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getAgenciaDebito())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getAgenciaDebito());
				dados.append(CARACTER_ESPECIAL);
			}
			// Nome Agencia de D�bito
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getNomeAgenciaDebito())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getNomeAgenciaDebito());
				dados.append(CARACTER_ESPECIAL);
			}
			// C�digo Agencia Produtora
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCodigoAgenciaProdutora()) || acompanhamentoPropostaVO.getCodigoAgenciaProdutora().longValue() == 0) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCodigoAgenciaProdutora());
				dados.append(CARACTER_ESPECIAL);
			}
			// Nome Agencia Produtora
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getNomeAgenciaProdutora())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getNomeAgenciaProdutora());
				dados.append(CARACTER_ESPECIAL);
			}
			// C�digo Assistente BS
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCodigoAssistenteBS()) || acompanhamentoPropostaVO.getCodigoAssistenteBS().longValue() == 0) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCodigoAssistenteBS());
				dados.append(CARACTER_ESPECIAL);
			}
			// C�digo Gerente Produto BVP
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getCodigoGerenteProdutoBVP())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getCodigoGerenteProdutoBVP());
				dados.append(CARACTER_ESPECIAL);
			}
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getValorTotalProposta())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getValorTotalProposta());
				dados.append(CARACTER_ESPECIAL);
			}
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getQuantidadeVidasProposta())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getQuantidadeVidasProposta());
				dados.append(CARACTER_ESPECIAL);
			}
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getDataInicioCriacaoCodigo())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getDataInicioCriacaoCodigo().substring(0, 10));
				dados.append(CARACTER_ESPECIAL);
			}
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getDataInicioStatusAtualProposta())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getDataInicioStatusAtualProposta().substring(0, 10));
				dados.append(CARACTER_ESPECIAL);
			}
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getStatusAtualProposta())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getStatusAtualProposta());
				dados.append(CARACTER_ESPECIAL);
			}
			if (ValidacaoUtil.isBlankOrNull(acompanhamentoPropostaVO.getMotivoStatusProposta())) {
				dados.append(CARACTER_ESPECIAL);
			} else {
				dados.append(acompanhamentoPropostaVO.getMotivoStatusProposta());
			}

			// regra informada pela L�vea.
			// if (acompanhamentoPropostaVO.getTipoProdutor() != null) {
			//
			// // Angariador
			// if (acompanhamentoPropostaVO.getTipoProdutor().getCodigo() == 1)
			// {
			//
			// // Corretor
			// dados.append(";");
			// dados.append(";");
			// dados.append(";");
			// // Angariador.
			// dados.append(acompanhamentoPropostaVO.getCpdAngariador() + ";");
			// dados.append(acompanhamentoPropostaVO.getCpfCnpjAngariador() +
			// ";");
			// dados.append(acompanhamentoPropostaVO.getNomeAngariador() + ";");
			// // Corretor Master
			// dados.append(";");
			// dados.append(";");
			// dados.append(";");
			// } else if (acompanhamentoPropostaVO.getTipoProdutor().getCodigo()
			// == 2) { // Corretor
			//
			// // Corretor
			// dados.append(acompanhamentoPropostaVO.getCpdCorretor() + ";");
			// dados.append(acompanhamentoPropostaVO.getCpfCnpjCorretor() +
			// ";");
			// dados.append(acompanhamentoPropostaVO.getNomeCorretor() + ";");
			// // Angariador.
			// dados.append(";");
			// dados.append(";");
			// dados.append(";");
			// // Corretor Master
			// dados.append(";");
			// dados.append(";");
			// dados.append(";");
			// } else if (acompanhamentoPropostaVO.getTipoProdutor().getCodigo()
			// == 3) { // Corretor Master
			//
			// // Corretor
			// dados.append(";");
			// dados.append(";");
			// dados.append(";");
			// // Angariador.
			// dados.append(";");
			// dados.append(";");
			// dados.append(";");
			// // Corretor Master
			// dados.append(acompanhamentoPropostaVO.getCpdCorretorMaster() +
			// ";");
			// dados.append(acompanhamentoPropostaVO.getCpfCnpjCorretorMaster()
			// + ";");
			// dados.append(acompanhamentoPropostaVO.getNomeCorretorMaster() +
			// ";");
			// }
			// }
			escreveArquivoCSV.write(dados.toString());
			escreveArquivoCSV.newLine();
			escreveArquivoCSV.flush();
			i++;
		}
		escreveArquivoCSV.close();
		gravarArquivoCSV.close();
		return arquivoCSV;
	}

	private Map<Object, Object> montarParametrosPDF(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamento) {

		final Map<Object, Object> parameters = new HashMap<Object, Object>();

		if (!listaAcompanhamento.isEmpty()) {

			ValorRelatorioAcompanhamentoPropostaEEDIVO valor = obterValoresPropostas(listaAcompanhamento);
			// atualizarValores(listaAcompanhamentoPropostaVO);
			if (!valor.getValorTotalProposta().toString().equals("0")) {
				parameters.put(VLR_TOTAL_PROPOSTAS, valor.getValorTotalProposta());
			}
			if (!valor.getValorTotalProposta().toString().equals("0")) {
				parameters.put(VLR_TOTAL_PROPOSTAS_AG_DEBITO, valor.getValorTotalProposta());
			}
			if (!valor.getValorTotalProposta().toString().equals("0")) {
				parameters.put(VLR_TOTAL_PROPOSTAS_AG_PROD, valor.getValorTotalProposta());
			}
			if (!valor.getValorTotalProposta().toString().equals("0")) {
				parameters.put(VLR_TOTAL_PROPOSTAS_ASSIST_BS, valor.getValorTotalProposta());
			}
			if (!valor.getValorTotalProposta().toString().equals("0")) {
				parameters.put(VLR_TOTAL_PROPOSTAS_CORRETOR, valor.getValorTotalProposta());
			}
			if (!valor.getValorTotalProposta().toString().equals("0")) {
				parameters.put(VLR_TOTAL_PROPOSTAS_GER_PROD_BVP, valor.getValorTotalProposta());
			}
			if (!valor.getValorTotalProposta().toString().equals("0")) {
				parameters.put(VLR_TOTAL_PROPOSTAS_SUCURSAL, valor.getValorTotalProposta());
			}

			for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaEEDIVO : listaAcompanhamento) {

				parameters.put(PROPOSTA, acompanhamentoPropostaEEDIVO.getCodProposta());

				if (acompanhamentoPropostaEEDIVO.getCpfCnpjAngariador() != null) {
					if (acompanhamentoPropostaEEDIVO.getCpfCnpjAngariador().length() == 11) {
						acompanhamentoPropostaEEDIVO.setCpfCnpjAngariador(FormatacaoUtil.formataCPF(acompanhamentoPropostaEEDIVO.getCpfCnpjAngariador()));
					} else {
						acompanhamentoPropostaEEDIVO.setCpfCnpjAngariador(FormatacaoUtil.formataCNPJ(acompanhamentoPropostaEEDIVO.getCpfCnpjAngariador()));
					}
				}

				if (acompanhamentoPropostaEEDIVO.getCpfCnpjCorretor() != null) {
					if (acompanhamentoPropostaEEDIVO.getCpfCnpjCorretor().length() == 11) {
						acompanhamentoPropostaEEDIVO.setCpfCnpjCorretor(FormatacaoUtil.formataCPF(acompanhamentoPropostaEEDIVO.getCpfCnpjCorretor()));
					} else {
						acompanhamentoPropostaEEDIVO.setCpfCnpjCorretor(FormatacaoUtil.formataCNPJ(acompanhamentoPropostaEEDIVO.getCpfCnpjCorretor()));
					}
				}

				if (acompanhamentoPropostaEEDIVO.getDataInicioStatusAtualProposta() != null) {
					acompanhamentoPropostaEEDIVO.setDataInicioStatusAtualProposta(FormatacaoUtil.formataDataInvertida(acompanhamentoPropostaEEDIVO.getDataInicioStatusAtualProposta().substring(0, 10)));
				}
				if (acompanhamentoPropostaEEDIVO.getDataInicioCriacaoCodigo() != null) {
					acompanhamentoPropostaEEDIVO.setDataInicioCriacaoCodigo(FormatacaoUtil.formataDataInvertida(acompanhamentoPropostaEEDIVO.getDataInicioCriacaoCodigo().substring(0, 10)));
				}
			}

		}

		return parameters;
	}

	private FiltroAcompanhamentoVO converterObjetoAplicacaoParaObjetoServico(FiltroRelatorioAcompanhamentoEEDIVO filtro) {
		FiltroAcompanhamentoVO filtroAcompanhamento = new FiltroAcompanhamentoVO();

		filtroAcompanhamento.setCodigoProposta(filtro.getCodigoProposta());
		filtroAcompanhamento.setCodigoAgenciaDebito(filtro.getCodigoAgenciaDebito());
		filtroAcompanhamento.setCodigoAgenciaProdutora(filtro.getCodigoAgenciaProdutora());
		filtroAcompanhamento.setCodigoAssistente(filtro.getCodigoAssistente());
		filtroAcompanhamento.setCodigoGerenteProdutoBVP(filtro.getCodigoGerenteProdutoBVP());
		filtroAcompanhamento.setCodigoStatusProposta(filtro.getCodigoStatusProposta());
		filtroAcompanhamento.setCpdCorretor(filtro.getCpdCorretor());
		filtroAcompanhamento.setCpfBeneficiarioTitular(filtro.getCpfBeneficiarioTitular());
		filtroAcompanhamento.setCpfCnpjCorretor(filtro.getCpfCnpjCorretor());

		filtroAcompanhamento.setFiltroPeriodoVO(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FiltroPeriodoVO());

		if (filtro.getFiltroPeriodoVO().getDataInicio() != null && filtro.getFiltroPeriodoVO().getDataFim() != null) {
			filtroAcompanhamento.getFiltroPeriodoVO().setDataInicio(filtro.getFiltroPeriodoVO().getDataInicio().toString(br.com.bradseg.eedi.dentalindividual.util.Constantes.DATA_FORMATO_DD_MM_YYYY));
			filtroAcompanhamento.getFiltroPeriodoVO().setDataFim(filtro.getFiltroPeriodoVO().getDataFim().toString(br.com.bradseg.eedi.dentalindividual.util.Constantes.DATA_FORMATO_DD_MM_YYYY));
		}

		return filtroAcompanhamento;
	}

	private List<AcompanhamentoPropostaEEDIVO> converterObjetoEEDIServicosParaEEDI(List<br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.AcompanhamentoPropostaVO> listaAcompanhamentoPropostaServico) {

		if (listaAcompanhamentoPropostaServico == null) {
			return null;
		}

		List<AcompanhamentoPropostaEEDIVO> listaAcompanhamentoProposta = new ArrayList<AcompanhamentoPropostaEEDIVO>();

		try {

			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.AcompanhamentoPropostaVO acompanhamentoPropostaServicoVO : listaAcompanhamentoPropostaServico) {

				AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO = converterObjetoAcompanhamentoPropostaServicoParaObjetoAcompanhamentoAplicacao(acompanhamentoPropostaServicoVO);
				listaAcompanhamentoProposta.add(acompanhamentoPropostaVO);
			}
		} catch (Exception e) {
			LOGGER.error("Erro ao converter o objeto AcompanhamentoPropostaVO: " + e.getMessage());
		}

		return listaAcompanhamentoProposta;

	}

	public byte[] gerarPDFRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoEEDIVO filtro) {

		relatorioAcompanhamentoValidator.validate(filtro);
		List<AcompanhamentoPropostaEEDIVO> listaAcompanhamento = null;
		byte[] relatorioPDFAcompanhamento = null;
		FiltroAcompanhamentoVO filtroAcompanhamento = converterObjetoAplicacaoParaObjetoServico(filtro);

		try {
			relatorioPDFAcompanhamento = propostaWebService.gerarRelatorioAcompanhamentoEEDI(filtroAcompanhamento, new LoginVO());
		} catch (Exception e) {
			LOGGER.error("ERRO AO CHAMAR METODO PARA GERAR PDF PROPOSTAS ACOMPANHAMENTO: " + e.getMessage());
		}

		return relatorioPDFAcompanhamento;

	}

	public ValorRelatorioAcompanhamentoPropostaEEDIVO obterValoresPropostas(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamento) {

		ValorRelatorioAcompanhamentoPropostaEEDIVO valorRelatorio = new ValorRelatorioAcompanhamentoPropostaEEDIVO();
		valorRelatorio.setValorTotalProposta(calcularValorTotalProposta(listaAcompanhamento));
		valorRelatorio.setValorTotalPropostaPorAgenciaDebito(calculaValorTotalPropostasAgDebito(listaAcompanhamento));
		valorRelatorio.setValorTotalPropostaPorAgenciaProdutora(calculaValorTotalPropostasAgProdura(listaAcompanhamento));
		valorRelatorio.setValorTotalPropostaPorAssistenteProducao(calculaValorTotalPropostasAssistenteProducao(listaAcompanhamento));
		valorRelatorio.setValorTotalPropostaPorCorretor(calculaValorTotalPropostasCorretor(listaAcompanhamento));
		valorRelatorio.setValorTotalPropostaPorGerenteProdutoBVP(calculaValorTotalPropostasGerenteProdutoBVP(listaAcompanhamento));
		valorRelatorio.setValorTotalPropostaPorSucursal(calculaValorTotalPropostasSucursal(listaAcompanhamento));

		return valorRelatorio;

	}

	private BigDecimal calcularValorTotalProposta(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamento) {

		BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
		if (!listaAcompanhamento.isEmpty()) {
			for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO : listaAcompanhamento) {
				valorTotal = valorTotal.add(acompanhamentoPropostaVO.getValorTotalProposta());
			}
		}
		return valorTotal;
	}

	private BigDecimal calculaValorTotalPropostasAgDebito(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamentoPropostaVO) {
		BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
		if (!listaAcompanhamentoPropostaVO.isEmpty()) {
			for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO : listaAcompanhamentoPropostaVO) {
				if (acompanhamentoPropostaVO.getAgenciaDebito() != null) {
					valorTotal = valorTotal.add(acompanhamentoPropostaVO.getValorTotalProposta());
				}
			}
		}
		return valorTotal;
	}

	private BigDecimal calculaValorTotalPropostasAgProdura(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamentoPropostaVO) {
		BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
		if (!listaAcompanhamentoPropostaVO.isEmpty()) {
			for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO : listaAcompanhamentoPropostaVO) {
				if (acompanhamentoPropostaVO.getCodigoAgenciaProdutora() != null) {
					valorTotal = valorTotal.add(acompanhamentoPropostaVO.getValorTotalProposta());
				}
			}
		}
		return valorTotal;
	}

	public BigDecimal calculaValorTotalPropostasAssistenteProducao(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamentoPropostaVO) {
		BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
		if (!listaAcompanhamentoPropostaVO.isEmpty()) {
			for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO : listaAcompanhamentoPropostaVO) {
				if (acompanhamentoPropostaVO.getCodigoAssistenteBS() != null) {
					valorTotal = valorTotal.add(acompanhamentoPropostaVO.getValorTotalProposta());
				}
			}
		}
		return valorTotal;
	}

	public BigDecimal calculaValorTotalPropostasCorretor(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamentoPropostaVO) {
		BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
		if (!listaAcompanhamentoPropostaVO.isEmpty()) {
			for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO : listaAcompanhamentoPropostaVO) {
				if (acompanhamentoPropostaVO.getCpfCnpjCorretor() != null) {
					valorTotal = valorTotal.add(acompanhamentoPropostaVO.getValorTotalProposta());
				}
			}
		}
		return valorTotal;
	}

	public BigDecimal calculaValorTotalPropostasGerenteProdutoBVP(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamentoPropostaVO) {
		BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
		if (!listaAcompanhamentoPropostaVO.isEmpty()) {
			for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO : listaAcompanhamentoPropostaVO) {
				if (acompanhamentoPropostaVO.getCodigoGerenteProdutoBVP() != null) {
					valorTotal = valorTotal.add(acompanhamentoPropostaVO.getValorTotalProposta());
				}
			}
		}
		return valorTotal;
	}

	public BigDecimal calculaValorTotalPropostasSucursal(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamentoPropostaVO) {
		BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
		if (!listaAcompanhamentoPropostaVO.isEmpty()) {
			for (AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO : listaAcompanhamentoPropostaVO) {
				if (acompanhamentoPropostaVO.getSucursal() != null) {
					valorTotal = valorTotal.add(acompanhamentoPropostaVO.getValorTotalProposta());
				}
			}
		}
		return valorTotal;
	}

	private AcompanhamentoPropostaEEDIVO converterObjetoAcompanhamentoPropostaServicoParaObjetoAcompanhamentoAplicacao(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.AcompanhamentoPropostaVO acompanhamentoPropostaServicoVO) {

		AcompanhamentoPropostaEEDIVO acompanhamentoPropostaVO = new AcompanhamentoPropostaEEDIVO();
		acompanhamentoPropostaVO.setAgenciaDebito(acompanhamentoPropostaServicoVO.getAgenciaDebito());
		acompanhamentoPropostaVO.setCodigoAgenciaDebito(acompanhamentoPropostaServicoVO.getCodigoAgenciaDebito());
		acompanhamentoPropostaVO.setCodigoAgenciaProdutora(acompanhamentoPropostaServicoVO.getCodigoAgenciaProdutora());
		acompanhamentoPropostaVO.setCodigoAssistenteBS(acompanhamentoPropostaServicoVO.getCodigoAssistenteBS());
		acompanhamentoPropostaVO.setCodigoCanalVenda(acompanhamentoPropostaServicoVO.getCodigoCanalVenda());
		acompanhamentoPropostaVO.setCodigoGerenteProdutoBVP(acompanhamentoPropostaServicoVO.getCodigoGerenteProdutoBVP());
		acompanhamentoPropostaVO.setCodigoPlano(acompanhamentoPropostaServicoVO.getCodigoPlano());
		acompanhamentoPropostaVO.setCodigoStatusAtualProposta(acompanhamentoPropostaServicoVO.getCodigoStatusAtualProposta());
		acompanhamentoPropostaVO.setCodProposta(acompanhamentoPropostaServicoVO.getCodProposta());
		acompanhamentoPropostaVO.setCpdAngariador(acompanhamentoPropostaServicoVO.getCpdAngariador());
		acompanhamentoPropostaVO.setCpdCorretor(acompanhamentoPropostaServicoVO.getCpdCorretor());
		acompanhamentoPropostaVO.setCpdCorretorMaster(acompanhamentoPropostaServicoVO.getCpdCorretorMaster());
		acompanhamentoPropostaVO.setCpfBenificiarioTitular(acompanhamentoPropostaServicoVO.getCpfBenificiarioTitular());
		acompanhamentoPropostaVO.setCpfCnpjAngariador(acompanhamentoPropostaServicoVO.getCpfCnpjAngariador());
		acompanhamentoPropostaVO.setCpfCnpjCorretor(acompanhamentoPropostaServicoVO.getCpfCnpjCorretor());
		acompanhamentoPropostaVO.setCpfCnpjCorretorMaster(acompanhamentoPropostaServicoVO.getCpfCnpjCorretorMaster());
		acompanhamentoPropostaVO.setCpfResponsavel(acompanhamentoPropostaServicoVO.getCpfResponsavel());
		acompanhamentoPropostaVO.setCpfTitularConta(acompanhamentoPropostaServicoVO.getCpfTitularConta());
		//	       acompanhamentoPropostaVO.setDataCriacaoProposta(acompanhamentoPropostaServicoVO.getDataCriacaoProposta());
		if(acompanhamentoPropostaServicoVO.getDataCriacaoPropostaMaior() != null){
			acompanhamentoPropostaVO.setDataCriacaoPropostaMaior(acompanhamentoPropostaServicoVO.getDataCriacaoPropostaMaior().toGregorianCalendar().getTime());
			acompanhamentoPropostaVO.setDataCriacaoPropostaMenor(acompanhamentoPropostaServicoVO.getDataCriacaoPropostaMenor().toGregorianCalendar().getTime());
		}
		
		acompanhamentoPropostaVO.setDataInicioCriacaoCodigo(acompanhamentoPropostaServicoVO.getDataInicioCriacaoCodigo());
		acompanhamentoPropostaVO.setDataInicioStatusAtualProposta(acompanhamentoPropostaServicoVO.getDataInicioStatusAtualProposta());
		acompanhamentoPropostaVO.setFimPeriodo(acompanhamentoPropostaServicoVO.getFimPeriodo());
		acompanhamentoPropostaVO.setInicioPeriodo(acompanhamentoPropostaServicoVO.getInicioPeriodo());
		acompanhamentoPropostaVO.setMotivoStatusProposta(acompanhamentoPropostaServicoVO.getMotivoStatusProposta());
		acompanhamentoPropostaVO.setNomeAgenciaDebito(acompanhamentoPropostaServicoVO.getNomeAgenciaDebito());
		acompanhamentoPropostaVO.setNomeAgenciaProdutora(acompanhamentoPropostaServicoVO.getNomeAgenciaProdutora());
		acompanhamentoPropostaVO.setNomeAngariador(acompanhamentoPropostaServicoVO.getNomeAngariador());
		acompanhamentoPropostaVO.setNomeBenificiarioTitular(acompanhamentoPropostaServicoVO.getNomeBenificiarioTitular());
		acompanhamentoPropostaVO.setNomeCorretor(acompanhamentoPropostaServicoVO.getNomeCorretor());
		acompanhamentoPropostaVO.setNomeResponsavel(acompanhamentoPropostaServicoVO.getNomeResponsavel());
		acompanhamentoPropostaVO.setNomeTitularConta(acompanhamentoPropostaServicoVO.getNomeTitularConta());
		acompanhamentoPropostaVO.setProtocolo(acompanhamentoPropostaServicoVO.getProtocolo());
		acompanhamentoPropostaVO.setQuantidadeVidasProposta(acompanhamentoPropostaServicoVO.getQuantidadeVidasProposta());
		acompanhamentoPropostaVO.setSituacaoProposta(acompanhamentoPropostaServicoVO.getSituacaoProposta());
		acompanhamentoPropostaVO.setStatusAtualProposta(acompanhamentoPropostaServicoVO.getStatusAtualProposta());
		acompanhamentoPropostaVO.setSucursal(acompanhamentoPropostaServicoVO.getSucursal());
		//	       acompanhamentoPropostaVO.setTipoCobranca(TipoCobranca.obterTipoCobrancaPorCodigo(acompanhamentoPropostaServicoVO.getTipoCobranca()));
		//	       acompanhamentoPropostaVO.setTipoProdutor(acompanhamentoPropostaServicoVO.getTipoProdutor());
		acompanhamentoPropostaVO.setValorTotalProposta(acompanhamentoPropostaServicoVO.getValorTotalProposta());

		return acompanhamentoPropostaVO;
	}

	private FiltroRelatorioAcompanhamentoVO converterFiltroAplicacaoEmFiltroServico(FiltroRelatorioAcompanhamentoEEDIVO filtroRelatorioAcompanhamentoEEDIVO) {

		FiltroRelatorioAcompanhamentoVO filtroRelatorioAcompanhamentoVOServico = new FiltroRelatorioAcompanhamentoVO();
		filtroRelatorioAcompanhamentoVOServico.setCodigoProposta(filtroRelatorioAcompanhamentoEEDIVO.getCodigoProposta());

		if (filtroRelatorioAcompanhamentoEEDIVO.getCodigoAgenciaDebito() != null && StringUtils.isNotBlank(filtroRelatorioAcompanhamentoEEDIVO.getCodigoAgenciaDebito())) {
			filtroRelatorioAcompanhamentoVOServico.setCodigoAgenciaDebito(Integer.valueOf(filtroRelatorioAcompanhamentoEEDIVO.getCodigoAgenciaDebito()));
		}
		if (filtroRelatorioAcompanhamentoEEDIVO.getCodigoAgenciaProdutora() != null && StringUtils.isNotBlank(filtroRelatorioAcompanhamentoEEDIVO.getCodigoAgenciaProdutora())) {
			filtroRelatorioAcompanhamentoVOServico.setCodigoAgenciaProdutora(Integer.valueOf(filtroRelatorioAcompanhamentoEEDIVO.getCodigoAgenciaProdutora()));
		}
		if (filtroRelatorioAcompanhamentoEEDIVO.getDataInicio() != null && filtroRelatorioAcompanhamentoEEDIVO.getDataFim() != null) {
			filtroRelatorioAcompanhamentoVOServico.setDataInicio(filtroRelatorioAcompanhamentoEEDIVO.getFiltroPeriodoVO().getDataInicio().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
			filtroRelatorioAcompanhamentoVOServico.setDataFim(filtroRelatorioAcompanhamentoEEDIVO.getFiltroPeriodoVO().getDataFim().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		}

		filtroRelatorioAcompanhamentoVOServico.setMatriculaAssistenteBS(String.valueOf(filtroRelatorioAcompanhamentoEEDIVO.getCodigoAssistente()));
		filtroRelatorioAcompanhamentoVOServico.setNomeProponente(filtroRelatorioAcompanhamentoEEDIVO.getNomeProponente());

		return filtroRelatorioAcompanhamentoVOServico;
	}

}
