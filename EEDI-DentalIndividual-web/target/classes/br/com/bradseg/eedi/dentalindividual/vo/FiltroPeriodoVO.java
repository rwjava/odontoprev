package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;



/**
 * Classe responsavel pelo transporte de dados do filtro por periodo.
 *
 */
public class FiltroPeriodoVO implements Serializable {

	private static final long serialVersionUID = 2202332933096591731L;

	private LocalDate dataInicio;
	private LocalDate dataFim;
	private Integer status;

	/**
	 * Retorna dataInicio.
	 *
	 * @return dataInicio - dataInicio.
	 */
	public LocalDate getDataInicio() {
		return dataInicio;
	}

	/**
	 * Especifica dataInicio.
	 *
	 * @param dataInicio - dataInicio.
	 */
	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * Retorna dataFim.
	 *
	 * @return dataFim - dataFim.
	 */
	public LocalDate getDataFim() {
		return dataFim;
	}

	/**
	 * Especifica dataFim.
	 *
	 * @param dataFim - dataFim.
	 */
	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}

	/**
	 * Retorna status.
	 *
	 * @return status - status.
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Especifica status.
	 *
	 * @param status - status.
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

}
