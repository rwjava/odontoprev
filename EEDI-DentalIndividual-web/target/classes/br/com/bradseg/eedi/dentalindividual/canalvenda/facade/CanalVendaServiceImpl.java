package br.com.bradseg.eedi.dentalindividual.canalvenda.facade;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.bradseg.eedi.dentalindividual.canalvenda.converter.CanalConverter;
import br.com.bradseg.eedi.dentalindividual.vo.CanalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.BusinessException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.IntegrationException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Proposta100PorCentoCorretorWebService;

@Service
public class CanalVendaServiceImpl implements CanalVendaService {

	@Autowired
	private Proposta100PorCentoCorretorWebService propostaWebService;
	
	@Autowired
	private CanalConverter canalConverter;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CanalVendaServiceImpl.class);

	public CanalVO consultarPorCodigo(Integer codigo) {
		CanalVO canal = null;
		try {
			canal =  canalConverter.converterCanalServicoEmCanalAplicacao(propostaWebService.obterCanalVendaPorCodigo(codigo));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro ao obter canal venda. " + e.getMessage());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro ao obter canal venda. " + e.getMessage());
		} catch(Exception e){
			LOGGER.error("Erro ao obter canal venda. " + e.getMessage());
		}
		return canal;
	}
	
	
	public List<CanalVO> listaDeCanais(){
		
		List<CanalVendaVO> listaCanaisVenda = null;
		List<CanalVO> listaCanaisVendaAplicacao = new ArrayList<CanalVO>();
		try {
			listaCanaisVenda = propostaWebService.listarCanaisVenda();
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro ao obter canal venda. " + e.getMessage());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro ao obter canal venda. " + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("Erro ao obter canal venda. " + e.getMessage());
		}
		if(listaCanaisVenda != null && !listaCanaisVenda.isEmpty()){
			listaCanaisVendaAplicacao = canalConverter.converterListaCanalServicoEmListaCanalAplicacao(listaCanaisVenda);
		}
		
		return listaCanaisVendaAplicacao;
	}
	

}
