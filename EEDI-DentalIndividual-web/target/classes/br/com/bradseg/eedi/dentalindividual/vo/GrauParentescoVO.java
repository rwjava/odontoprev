package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class GrauParentescoVO implements Serializable{

	private static final long serialVersionUID = -7036589451697183182L;
	
	private Integer codigo;
	private String descricao;
	
	public GrauParentescoVO() {
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
