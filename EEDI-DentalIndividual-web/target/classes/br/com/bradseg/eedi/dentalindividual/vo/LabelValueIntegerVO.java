package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

import com.google.common.base.Preconditions;


public class LabelValueIntegerVO implements Serializable{

	private static final long serialVersionUID = 4018791822855850943L;

	private Integer codigo;
	private String nome;

	public LabelValueIntegerVO(Integer codigo, String nome) {
		Preconditions.checkNotNull(codigo, "O c�digo n�o pode ser nulo");
		this.codigo = codigo;
		this.nome = nome;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	

}