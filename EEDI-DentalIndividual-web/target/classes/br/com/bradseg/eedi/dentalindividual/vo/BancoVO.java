package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class BancoVO implements Serializable{

	private static final long serialVersionUID = -5874833238006379828L;

	private Integer codigo;
	private String descricao;
	
	public BancoVO() {

	}

	public Integer getCodigo() {
		return codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
