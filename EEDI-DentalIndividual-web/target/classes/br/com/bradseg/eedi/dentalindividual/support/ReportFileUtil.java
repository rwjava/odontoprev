package br.com.bradseg.eedi.dentalindividual.support;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * Arquivo de relatório. Possui um nome, um tipo e pode conter vários relatórios (reports)
 * @author WDEV
 */
public class ReportFileUtil implements Serializable {

	private static final long serialVersionUID = -3421878692896349602L;

	private String fileName;
	private String format = "pdf";
	private List<ReportUtil> reports = Lists.newArrayList();

	public List<ReportUtil> getReports() {
		return reports;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFormat() {
		return format;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void adicionarRelatorio(ReportUtil report) {
		reports.add(report);
	}

}
