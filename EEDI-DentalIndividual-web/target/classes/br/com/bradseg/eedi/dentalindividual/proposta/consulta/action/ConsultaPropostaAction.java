package br.com.bradseg.eedi.dentalindividual.proposta.consulta.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.eedi.dentalindividual.proposta.action.BaseAction;
import br.com.bradseg.eedi.dentalindividual.proposta.facade.PropostaServiceFacade;

@Controller
@Scope(value = "request")
public class ConsultaPropostaAction extends BaseAction{


	private static final long serialVersionUID = -3630875795881257835L;
	
	
	private List<String> registros;
	private String textoConsulta;
	private List<String> colunas;
	
	@Autowired
	private PropostaServiceFacade propostaServiceFacade;
	
	public String iniciar(){
		return SUCCESS;
	}
	public String consultar(){
		
		registros = propostaServiceFacade.consultarProposta(textoConsulta);
		
		if(textoConsulta.toUpperCase().contains("SELECT") && registros != null && !registros.isEmpty()){
			String[] colunasSplit = registros.get(0).split(",");
			colunas = new ArrayList<String>();
			for(String coluna : colunasSplit){
				colunas.add(coluna);
			}
		}
		return SUCCESS;
	}
	
	public List<String> getRegistros() {
		return registros;
	}
	public void setRegistros(List<String> registros) {
		this.registros = registros;
	}
	public String getTextoConsulta() {
		return textoConsulta;
	}
	public void setTextoConsulta(String textoConsulta) {
		this.textoConsulta = textoConsulta;
	}
	public List<String> getColunas() {
		return colunas;
	}
	public void setColunas(List<String> colunas) {
		this.colunas = colunas;
	}
	
	
	
	

}
