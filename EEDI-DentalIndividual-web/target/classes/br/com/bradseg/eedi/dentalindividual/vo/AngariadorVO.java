package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class AngariadorVO extends ProdutorVO implements Serializable{


	private static final long serialVersionUID = -5322479176575745775L;
	
	private Integer cpd;
	
	public AngariadorVO(){
		setTipoProdutor(TipoProdutor.ANGARIADOR);
	}

	public Integer getCpd() {
		return cpd;
	}

	public void setCpd(Integer cpd) {
		this.cpd = cpd;
	}
	
	
}
