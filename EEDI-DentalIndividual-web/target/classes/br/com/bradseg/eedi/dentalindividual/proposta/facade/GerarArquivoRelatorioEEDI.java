package br.com.bradseg.eedi.dentalindividual.proposta.facade;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.apache.commons.io.FilenameUtils;
import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.dentalindividual.support.ReportFileUtil;
import br.com.bradseg.eedi.dentalindividual.support.ReportUtil;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Gera o array de bytes com o arquivo de relat�rio
 * @author WDEV
 */
@Service
public class GerarArquivoRelatorioEEDI {

	private static final Logger LOGGER = LoggerFactory.getLogger(GerarArquivoRelatorioEEDI.class);

	public static final String CAMINHO_PADRAO_RELATORIOS = "WEB-INF/reports/";
	public static final String BASE_IMAGE_SERVLET_REPORT = "/images/";

	@Autowired
	protected transient ApplicationContext applicationContext;

	public byte[] gerar(ReportFileUtil reportFile) {
		if (reportFile != null) {
			List<JasperPrint> jasperPrints = processarRelatorios(reportFile);
			return exportarRelatorios(reportFile, jasperPrints);
		}
		return null;
	}

	private List<JasperPrint> processarRelatorios(ReportFileUtil reportFile) {
		List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();
		for (ReportUtil report : reportFile.getReports()) {
			jasperPrints.add(processarRelatorio(report));
		}
		return jasperPrints;
	}

	private JasperPrint processarRelatorio(ReportUtil report) {
		return gerarRelatorio(report.getJasper(CAMINHO_PADRAO_RELATORIOS), report.getDataSource(), report.getParameters());
	}

	private <T> JasperPrint gerarRelatorio(String jasper, Object objeto, Map<Object, Object> parametrosExecucao) {
		try {
			if (parametrosExecucao == null) {
				parametrosExecucao = Maps.newHashMap();
			}
			preencherParametros(parametrosExecucao);
			return JasperFillManager.fillReport(getResourcePath(jasper), parametrosExecucao, getJRDataSource(objeto));
		} catch (JRException e) {
			LOGGER.error("INFO: Erro ao gerar JasperPrint", e);
			throw new IntegrationException(e);
		}
	}

	private Map<Object, Object> preencherParametros(Map<Object, Object> parametrosExecucao) {
		preencherParametroSubreportDirectory(parametrosExecucao);
		preencherParametroServletImagens(parametrosExecucao);
		return parametrosExecucao;
	}

	private void preencherParametroServletImagens(Map<Object, Object> parameters) {
		parameters.put(JRHtmlExporterParameter.IMAGES_URI, ServletActionContext.getRequest().getContextPath() + BASE_IMAGE_SERVLET_REPORT);
	}

	private void preencherParametroSubreportDirectory(Map<Object, Object> parameters) {
		parameters.put("SUBREPORT_DIR", getResourcePath(FilenameUtils.getFullPathNoEndSeparator(CAMINHO_PADRAO_RELATORIOS)));
		parameters.put("BaseDir", getResourcePath(FilenameUtils.getFullPathNoEndSeparator(CAMINHO_PADRAO_RELATORIOS)));
	}

	private String getResourcePath(String resourcePath) {
		try {
			Resource resource = applicationContext.getResource(resourcePath);
			if (!resource.exists()) {
				throw new IntegrationException("O path '" + resourcePath + "' n�o foi encontrado");
			}
			return resource.getFile().getPath();
		} catch (IOException e) {
			LOGGER.error("INFO: Erro ao recuperar caminho informado", e);
			throw new IntegrationException(e);
		}
	}

	private JRDataSource getJRDataSource(Object objeto) {
		if (objeto instanceof Collection<?>) {
			return new JRBeanCollectionDataSource((Collection<?>) objeto);
		} else if (objeto instanceof JRDataSource) {
			return (JRDataSource) objeto;
		}
		return new JRBeanCollectionDataSource(Lists.newArrayList(objeto));
	}

	private byte[] exportarRelatorios(ReportFileUtil reportFile, List<JasperPrint> jasperPrints) {
		return gerarPDF(reportFile.getFileName(), jasperPrints);
	}

	private byte[] gerarPDF(String nome, List<JasperPrint> relatorios) {
		return exportarPDF(relatorios);
	}

	private byte[] exportarPDF(List<JasperPrint> relatorios) {
		try {
			JRExporter exporter = new JRPdfExporter();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, relatorios);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
			exporter.exportReport();
			return out.toByteArray();
		} catch (Exception e) {
			LOGGER.error("INFO: Erro ao exportar relatorios", e);
			throw new IntegrationException("Ocorreu um erro na exporta��o da lista de relat�rios " + relatorios + ". " + e.getClass() + ": " + e.getMessage(), e);
		}
	}

}