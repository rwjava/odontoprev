package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PropostaVaziaReportVO implements Serializable {

	private static final long serialVersionUID = -5224384529787983701L;

	private List<BeneficiariosVO> beneficiarios;
	private String nome;
	private String email;
	private String cpf;
	private Date dataNascimento;
	private Long codigoAgencia;
	private String codigoDigitoAgencia;
	private String codMatriculaGerente;
	private String codMatriculaAssistente;
	private String nomeAbreviado;

	public List<BeneficiariosVO> getBeneficiarios() {
		return beneficiarios;
	}

	public void setBeneficiarios(List<BeneficiariosVO> beneficiarios) {
		this.beneficiarios = beneficiarios;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Long getCodigoAgencia() {
		return codigoAgencia;
	}

	public void setCodigoAgencia(Long codigoAgencia) {
		this.codigoAgencia = codigoAgencia;
	}

	public String getCodigoDigitoAgencia() {
		return codigoDigitoAgencia;
	}

	public void setCodigoDigitoAgencia(String codigoDigitoAgencia) {
		this.codigoDigitoAgencia = codigoDigitoAgencia;
	}

	public String getCodMatriculaGerente() {
		return codMatriculaGerente;
	}

	public void setCodMatriculaGerente(String codMatriculaGerente) {
		this.codMatriculaGerente = codMatriculaGerente;
	}

	public String getCodMatriculaAssistente() {
		return codMatriculaAssistente;
	}

	public void setCodMatriculaAssistente(String codMatriculaAssistente) {
		this.codMatriculaAssistente = codMatriculaAssistente;
	}

	public String getNomeAbreviado() {
		return nomeAbreviado;
	}

	public void setNomeAbreviado(String nomeAbreviado) {
		this.nomeAbreviado = nomeAbreviado;
	}

}
