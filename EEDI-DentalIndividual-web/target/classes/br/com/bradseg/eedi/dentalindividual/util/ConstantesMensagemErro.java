package br.com.bradseg.eedi.dentalindividual.util;

/**
 * Constantes de mensagens de erro para auxiliar a aplica��o.
 *
 */
public class ConstantesMensagemErro {
	
	public static final String MSG_ERRO_GRAUPARENTESCO_IGUAIS = "msg.erro.grau.parentesco.invalido"; 
	public static final String MSG_CPF_PROPRIO_IGUAIS_OBRIGATORIO = "msg.erro.cpf.proprio";
	public static final String MSG_ERRO_CAMPO_OBRIGATORIO = "msg.erro.campo.obrigatorio";
	public static final String MSG_ERRO_CPF_INVALIDO = "msg.erro.cpf.invalido";
	public static final String MSG_ERRO_CPF_IGUAIS_PROPONENTE = "msg.erro.cpf.iguais.proponente";
	public static final String MSG_ERRO_CPF_IGUAIS = "msg.erro.cpf.iguais";
	public static final String MSG_ERRO_NOME_INCOMLETO = "msg.erro.nome.incompleto";
	public static final String MSG_ERRO_NOME_CARACTER_INVALIDO = "msg.erro.nome.caracteres.invalido";
	public static final String MSG_ERRO_EMAIL_INVALIDO = "msg.erro.email.invalido";
	public static final String MSG_ERRO_BENEFICIARIOS_DEPENDENTES_NUMERO_MAXIMO = "msg.erro.beneficiarios.dependentes.numeromaximo";
	public static final String MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA_DENTE_LEITE = "msg.erro.beneficiarios.datanascimento.idademaxima.denteleite";
	public static final String MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA_JUNIOR = "msg.erro.beneficiarios.datanascimento.idademaxima.junior";
	public static final String MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA = "msg.erro.beneficiarios.datanascimento.idademaxima";
	public static final String MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA = "msg.erro.beneficiarios.datanascimento.idademinima";
	public static final String MSG_ERRO_BENEFICIARIOS_CEP_NAO_ENCONTRADO = "msg.erro.beneficiarios.cep.nao.encontrado";
	public static final String MSG_ERRO_BENEFICIARIOS_TITULAR_DATA_NASCIMENTO_MENOR_DE_DEZOITO = "msg.erro.beneficiarios.titular.datanascimento.menordedezoito";
	public static final String MSG_ERRO_BENEFICIARIOS_DEPENDENTE_NOME_MAE_OU_CPF_OBRIGATORIO = "msg.erro.beneficiarios.dependente.nomemae.ou.cpf.obrigatorio";
	public static final String MSG_ERRO_BENEFICIARIOS_TITULAR_TELEFONE_OBRIGATORIO = "msg.erro.beneficiarios.titular.telefone.obrigatorio";
	public static final String MSG_ERRO_LISTA_PROPOSTA_PERIODO_MAIOR_30_DIAS = "msg.erro.lista.proposta.periodo.maiorque.trinta.dias";
	public static final String MSG_ERRO_LISTA_PROPOSTA_PERIODO_DATA_FIM_MENOR_QUE_DATA_INICIO = "msg.erro.lista.proposta.periodo.datafim.menorque.datainicio";
	public static final String MSG_ERRO_PAGAMENTO_DATA_COBRANCA_ENTRE_15_A_45_DIAS = "msg.erro.pagamento.datacobranca.entre.15a45dias";
	public static final String MSG_ERRO_DADOS_SUCURSAL_REDE = "msg.erro.dados.sucursal.rede";
	public static final String MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO = "msg.erro.dados.idade.responsavel.financeiro";
	public static final String MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO_MENOS ="msg.erro.dados.idade.responsavel.financeiro.menos";

}
