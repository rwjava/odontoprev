package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class RelatorioAcompanhamentoPropostaEEDIVO implements Serializable{

    private static final long serialVersionUID = 7627996171921031254L;
    
    private String codigoProposta;
    private Integer cpfProponente;
    private String nomeProponente;
    private Integer cpfResponsavel;
    private String nomeResponsavel;
    private Integer cpfBeneficiarioTitular;
    private String nomeBeneficiarioTitular;
    private ValorRelatorioAcompanhamentoPropostaEEDIVO valorRelatorioAcompanhamentoPropostaVO;
    
    public RelatorioAcompanhamentoPropostaEEDIVO() {
		this.valorRelatorioAcompanhamentoPropostaVO = new ValorRelatorioAcompanhamentoPropostaEEDIVO();
		
		this.valorRelatorioAcompanhamentoPropostaVO.setValorTotalProposta(BigDecimal.ZERO);                           
		this.valorRelatorioAcompanhamentoPropostaVO.setValorTotalPropostaPorSucursal(BigDecimal.ZERO);               
		this.valorRelatorioAcompanhamentoPropostaVO.setValorTotalPropostaPorCorretor(BigDecimal.ZERO);                
		this.valorRelatorioAcompanhamentoPropostaVO.setValorTotalPropostaPorAgenciaDebito(BigDecimal.ZERO);          
		this.valorRelatorioAcompanhamentoPropostaVO.setValorTotalPropostaPorAgenciaProdutora(BigDecimal.ZERO);        
		this.valorRelatorioAcompanhamentoPropostaVO.setValorTotalPropostaPorAssistenteProducao(BigDecimal.ZERO);      
		this.valorRelatorioAcompanhamentoPropostaVO.setValorTotalPropostaPorGerenteProdutoBVP(BigDecimal.ZERO);       
    }
    
    public String getCodigoProposta() {
        return codigoProposta;
    }
    public void setCodigoProposta(String codigoProposta) {
        this.codigoProposta = codigoProposta;
    }
    public Integer getCpfProponente() {
        return cpfProponente;
    }
    public void setCpfProponente(Integer cpfProponente) {
        this.cpfProponente = cpfProponente;
    }
    public String getNomeProponente() {
        return nomeProponente;
    }
    public void setNomeProponente(String nomeProponente) {
        this.nomeProponente = nomeProponente;
    }
    public Integer getCpfResponsavel() {
        return cpfResponsavel;
    }
    public void setCpfResponsavel(Integer cpfResponsavel) {
        this.cpfResponsavel = cpfResponsavel;
    }
    public String getNomeResponsavel() {
        return nomeResponsavel;
    }
    public void setNomeResponsavel(String nomeResponsavel) {
        this.nomeResponsavel = nomeResponsavel;
    }
    public Integer getCpfBeneficiarioTitular() {
        return cpfBeneficiarioTitular;
    }
    public void setCpfBeneficiarioTitular(Integer cpfBeneficiarioTitular) {
        this.cpfBeneficiarioTitular = cpfBeneficiarioTitular;
    }
    public String getNomeBeneficiarioTitular() {
        return nomeBeneficiarioTitular;
    }
    public void setNomeBeneficiarioTitular(String nomeBeneficiarioTitular) {
        this.nomeBeneficiarioTitular = nomeBeneficiarioTitular;
    }
    public ValorRelatorioAcompanhamentoPropostaEEDIVO getValorRelatorioAcompanhamentoPropostaVO() {
        return valorRelatorioAcompanhamentoPropostaVO;
    }
    public void setValorRelatorioAcompanhamentoPropostaVO(
    	ValorRelatorioAcompanhamentoPropostaEEDIVO valorRelatorioAcompanhamentoPropostaVO) {
        this.valorRelatorioAcompanhamentoPropostaVO = valorRelatorioAcompanhamentoPropostaVO;
    }

    
    
    
}
