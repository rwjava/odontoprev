
package br.com.bradseg.inet.consultaexpressa.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for corretorSucursal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="corretorSucursal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoConcatenado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoCorretor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codigoSucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="descricaoConcatenada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeSucursal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "corretorSucursal", propOrder = {
    "codigoConcatenado",
    "codigoCorretor",
    "codigoSucursal",
    "descricaoConcatenada",
    "nomeSucursal"
})
public class CorretorSucursal {

    protected String codigoConcatenado;
    protected Integer codigoCorretor;
    protected Integer codigoSucursal;
    protected String descricaoConcatenada;
    protected String nomeSucursal;

    /**
     * Gets the value of the codigoConcatenado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoConcatenado() {
        return codigoConcatenado;
    }

    /**
     * Sets the value of the codigoConcatenado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoConcatenado(String value) {
        this.codigoConcatenado = value;
    }

    /**
     * Gets the value of the codigoCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoCorretor() {
        return codigoCorretor;
    }

    /**
     * Sets the value of the codigoCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoCorretor(Integer value) {
        this.codigoCorretor = value;
    }

    /**
     * Gets the value of the codigoSucursal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoSucursal() {
        return codigoSucursal;
    }

    /**
     * Sets the value of the codigoSucursal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoSucursal(Integer value) {
        this.codigoSucursal = value;
    }

    /**
     * Gets the value of the descricaoConcatenada property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoConcatenada() {
        return descricaoConcatenada;
    }

    /**
     * Sets the value of the descricaoConcatenada property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoConcatenada(String value) {
        this.descricaoConcatenada = value;
    }

    /**
     * Gets the value of the nomeSucursal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeSucursal() {
        return nomeSucursal;
    }

    /**
     * Sets the value of the nomeSucursal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeSucursal(String value) {
        this.nomeSucursal = value;
    }

}
