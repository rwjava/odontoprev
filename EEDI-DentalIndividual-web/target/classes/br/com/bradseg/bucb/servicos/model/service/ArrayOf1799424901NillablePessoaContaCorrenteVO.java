
package br.com.bradseg.bucb.servicos.model.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import br.com.bradseg.bucb.servicos.model.vo.PessoaContaCorrenteVO;


/**
 * <p>Java class for ArrayOf_1799424901_nillable_PessoaContaCorrenteVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOf_1799424901_nillable_PessoaContaCorrenteVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PessoaContaCorrenteVO" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}PessoaContaCorrenteVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOf_1799424901_nillable_PessoaContaCorrenteVO", propOrder = {
    "pessoaContaCorrenteVO"
})
public class ArrayOf1799424901NillablePessoaContaCorrenteVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(name = "PessoaContaCorrenteVO", nillable = true)
    protected List<PessoaContaCorrenteVO> pessoaContaCorrenteVO;

    /**
     * Gets the value of the pessoaContaCorrenteVO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pessoaContaCorrenteVO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPessoaContaCorrenteVO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PessoaContaCorrenteVO }
     * 
     * 
     */
    public List<PessoaContaCorrenteVO> getPessoaContaCorrenteVO() {
        if (pessoaContaCorrenteVO == null) {
            pessoaContaCorrenteVO = new ArrayList<PessoaContaCorrenteVO>();
        }
        return this.pessoaContaCorrenteVO;
    }

}
