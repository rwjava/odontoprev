
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ListarCorrentistasPorAgCcSaidaVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listarCorrentistasPorAgCcReturn" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ListarCorrentistasPorAgCcSaidaVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listarCorrentistasPorAgCcReturn"
})
@XmlRootElement(name = "listarCorrentistasPorAgCcResponse")
public class ListarCorrentistasPorAgCcResponse {

    @XmlElement(required = true, nillable = true)
    protected ListarCorrentistasPorAgCcSaidaVO listarCorrentistasPorAgCcReturn;

    /**
     * Gets the value of the listarCorrentistasPorAgCcReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ListarCorrentistasPorAgCcSaidaVO }
     *     
     */
    public ListarCorrentistasPorAgCcSaidaVO getListarCorrentistasPorAgCcReturn() {
        return listarCorrentistasPorAgCcReturn;
    }

    /**
     * Sets the value of the listarCorrentistasPorAgCcReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListarCorrentistasPorAgCcSaidaVO }
     *     
     */
    public void setListarCorrentistasPorAgCcReturn(ListarCorrentistasPorAgCcSaidaVO value) {
        this.listarCorrentistasPorAgCcReturn = value;
    }

}
