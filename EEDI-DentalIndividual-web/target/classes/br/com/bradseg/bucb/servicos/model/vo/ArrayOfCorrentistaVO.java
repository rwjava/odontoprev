package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ArrayOfCorrentistaVO complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCorrentistaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorrentistaVO" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}CorrentistaVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCorrentistaVO", propOrder = { "correntistaVO" })
public class ArrayOfCorrentistaVO implements Serializable {

	private static final long serialVersionUID = 3813890431460242515L;
	@XmlElement(name = "CorrentistaVO", nillable = true)
	protected List<CorrentistaVO> correntistaVO;

	/**
	 * Gets the value of the correntistaVO property.
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
	 * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
	 * the correntistaVO property.
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCorrentistaVO().add(newItem);
	 * </pre>
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link CorrentistaVO }
	 */
	public List<CorrentistaVO> getCorrentistaVO() {
		if (correntistaVO == null) {
			correntistaVO = new ArrayList<CorrentistaVO>();
		}
		return this.correntistaVO;
	}

}
