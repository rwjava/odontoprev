
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListarSegmentacaoProdutoClienteSaidaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListarSegmentacaoProdutoClienteSaidaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descSegPobj" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descSegRenc" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="segmentoPobj" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="segmentoRenc" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListarSegmentacaoProdutoClienteSaidaVO", propOrder = {
    "descSegPobj",
    "descSegRenc",
    "segmentoPobj",
    "segmentoRenc"
})
public class ListarSegmentacaoProdutoClienteSaidaVO implements Serializable {

	private static final long serialVersionUID = 1L;

    @XmlElement(required = true, nillable = true)
    protected String descSegPobj;
    @XmlElement(required = true, nillable = true)
    protected String descSegRenc;
    protected int segmentoPobj;
    protected int segmentoRenc;

    /**
     * Gets the value of the descSegPobj property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescSegPobj() {
        return descSegPobj;
    }

    /**
     * Sets the value of the descSegPobj property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescSegPobj(String value) {
        this.descSegPobj = value;
    }

    /**
     * Gets the value of the descSegRenc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescSegRenc() {
        return descSegRenc;
    }

    /**
     * Sets the value of the descSegRenc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescSegRenc(String value) {
        this.descSegRenc = value;
    }

    /**
     * Gets the value of the segmentoPobj property.
     * 
     */
    public int getSegmentoPobj() {
        return segmentoPobj;
    }

    /**
     * Sets the value of the segmentoPobj property.
     * 
     */
    public void setSegmentoPobj(int value) {
        this.segmentoPobj = value;
    }

    /**
     * Gets the value of the segmentoRenc property.
     * 
     */
    public int getSegmentoRenc() {
        return segmentoRenc;
    }

    /**
     * Sets the value of the segmentoRenc property.
     * 
     */
    public void setSegmentoRenc(int value) {
        this.segmentoRenc = value;
    }

}
