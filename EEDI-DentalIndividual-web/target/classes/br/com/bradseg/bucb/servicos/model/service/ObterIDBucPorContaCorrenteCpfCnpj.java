
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.IDBucPorContaCorrenteCpfCnpjVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDBucPorContaCorrenteCpfCnpj" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}IDBucPorContaCorrenteCpfCnpjVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idBucPorContaCorrenteCpfCnpj"
})
@XmlRootElement(name = "obterIDBucPorContaCorrenteCpfCnpj")
public class ObterIDBucPorContaCorrenteCpfCnpj {

    @XmlElement(name = "IDBucPorContaCorrenteCpfCnpj", required = true, nillable = true)
    protected IDBucPorContaCorrenteCpfCnpjVO idBucPorContaCorrenteCpfCnpj;

    /**
     * Gets the value of the idBucPorContaCorrenteCpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link IDBucPorContaCorrenteCpfCnpjVO }
     *     
     */
    public IDBucPorContaCorrenteCpfCnpjVO getIDBucPorContaCorrenteCpfCnpj() {
        return idBucPorContaCorrenteCpfCnpj;
    }

    /**
     * Sets the value of the idBucPorContaCorrenteCpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDBucPorContaCorrenteCpfCnpjVO }
     *     
     */
    public void setIDBucPorContaCorrenteCpfCnpj(IDBucPorContaCorrenteCpfCnpjVO value) {
        this.idBucPorContaCorrenteCpfCnpj = value;
    }

}
