
package br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="obterMeiosContatoCorretorNaSucursalReturn" type="{http://ws.vo.model.cadastrocorretores.ccrr.bradseg.com.br}WSCorretorVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "obterMeiosContatoCorretorNaSucursalReturn"
})
@XmlRootElement(name = "obterMeiosContatoCorretorNaSucursalResponse")
public class ObterMeiosContatoCorretorNaSucursalResponse {

    @XmlElement(required = true, nillable = true)
    protected WSCorretorVO obterMeiosContatoCorretorNaSucursalReturn;

    /**
     * Gets the value of the obterMeiosContatoCorretorNaSucursalReturn property.
     * 
     * @return
     *     possible object is
     *     {@link WSCorretorVO }
     *     
     */
    public WSCorretorVO getObterMeiosContatoCorretorNaSucursalReturn() {
        return obterMeiosContatoCorretorNaSucursalReturn;
    }

    /**
     * Sets the value of the obterMeiosContatoCorretorNaSucursalReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCorretorVO }
     *     
     */
    public void setObterMeiosContatoCorretorNaSucursalReturn(WSCorretorVO value) {
        this.obterMeiosContatoCorretorNaSucursalReturn = value;
    }

}
