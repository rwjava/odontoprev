
package br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.5.2
 * 2016-12-05T14:09:52.342-02:00
 * Generated source version: 2.5.2
 */

@WebFault(name = "WSBusinessException", targetNamespace = "http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br")
public class WSBusinessException_Exception extends Exception {
    
    private br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice.WSBusinessException wsBusinessException;

    public WSBusinessException_Exception() {
        super();
    }
    
    public WSBusinessException_Exception(String message) {
        super(message);
    }
    
    public WSBusinessException_Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public WSBusinessException_Exception(String message, br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice.WSBusinessException wsBusinessException) {
        super(message);
        this.wsBusinessException = wsBusinessException;
    }

    public WSBusinessException_Exception(String message, br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice.WSBusinessException wsBusinessException, Throwable cause) {
        super(message, cause);
        this.wsBusinessException = wsBusinessException;
    }

    public br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice.WSBusinessException getFaultInfo() {
        return this.wsBusinessException;
    }
}
