
package br.com.bradseg.bucb.servicos.model.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WebServiceBusinessException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WebServiceBusinessException">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="message" type="{http" + "://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mensagensValidacao" type="{http" + "://exception.model.servicos.bucb.bradseg.com.br}ArrayOf_143104033_nillable_string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WebServiceBusinessException", propOrder = {
    "message",
    "mensagensValidacao"
})
public class WebServiceBusinessException {

    @XmlElement(required = true, nillable = true)
    protected String message;
    @XmlElement(required = true, nillable = true)
    protected ArrayOf143104033NillableString mensagensValidacao;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the mensagensValidacao property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOf143104033NillableString }
     *     
     */
    public ArrayOf143104033NillableString getMensagensValidacao() {
        return mensagensValidacao;
    }

    /**
     * Sets the value of the mensagensValidacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOf143104033NillableString }
     *     
     */
    public void setMensagensValidacao(ArrayOf143104033NillableString value) {
        this.mensagensValidacao = value;
    }

}
