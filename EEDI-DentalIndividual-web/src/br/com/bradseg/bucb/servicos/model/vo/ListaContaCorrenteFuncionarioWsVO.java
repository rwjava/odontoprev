
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListaContaCorrenteFuncionarioWsVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListaContaCorrenteFuncionarioWsVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="matricula" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="contaCorrenteFuncionario" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ArrayOfContaCorrenteFuncionarioVO"/>
 *         &lt;element name="quantidadeRegistros" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListaContaCorrenteFuncionarioWsVO", propOrder = {
    "matricula",
    "contaCorrenteFuncionario",
    "quantidadeRegistros"
})
public class ListaContaCorrenteFuncionarioWsVO implements Serializable {

	private static final long serialVersionUID = 1L;

    protected long matricula;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfContaCorrenteFuncionarioVO contaCorrenteFuncionario;
    protected long quantidadeRegistros;

    /**
     * Gets the value of the matricula property.
     * 
     */
    public long getMatricula() {
        return matricula;
    }

    /**
     * Sets the value of the matricula property.
     * 
     */
    public void setMatricula(long value) {
        this.matricula = value;
    }

    /**
     * Gets the value of the contaCorrenteFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContaCorrenteFuncionarioVO }
     *     
     */
    public ArrayOfContaCorrenteFuncionarioVO getContaCorrenteFuncionario() {
        return contaCorrenteFuncionario;
    }

    /**
     * Sets the value of the contaCorrenteFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContaCorrenteFuncionarioVO }
     *     
     */
    public void setContaCorrenteFuncionario(ArrayOfContaCorrenteFuncionarioVO value) {
        this.contaCorrenteFuncionario = value;
    }

    /**
     * Gets the value of the quantidadeRegistros property.
     * 
     */
    public long getQuantidadeRegistros() {
        return quantidadeRegistros;
    }

    /**
     * Sets the value of the quantidadeRegistros property.
     * 
     */
    public void setQuantidadeRegistros(long value) {
        this.quantidadeRegistros = value;
    }

}
