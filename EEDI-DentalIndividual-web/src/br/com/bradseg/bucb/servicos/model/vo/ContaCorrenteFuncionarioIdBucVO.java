
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContaCorrenteFuncionarioIdBucVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContaCorrenteFuncionarioIdBucVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contas" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ArrayOfContaCorrenteIdBucVO"/>
 *         &lt;element name="matricula" type="{http" + "://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numeroContas" type="{http" + "://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContaCorrenteFuncionarioIdBucVO", propOrder = {
    "contas",
    "matricula",
    "numeroContas"
})
public class ContaCorrenteFuncionarioIdBucVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
    @XmlElement(required = true, nillable = true)
    protected ArrayOfContaCorrenteIdBucVO contas;
    protected long matricula;
    protected int numeroContas;

    /**
     * Gets the value of the contas property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContaCorrenteIdBucVO }
     *     
     */
    public ArrayOfContaCorrenteIdBucVO getContas() {
        return contas;
    }

    /**
     * Sets the value of the contas property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContaCorrenteIdBucVO }
     *     
     */
    public void setContas(ArrayOfContaCorrenteIdBucVO value) {
        this.contas = value;
    }

    /**
     * Gets the value of the matricula property.
     * 
     */
    public long getMatricula() {
        return matricula;
    }

    /**
     * Sets the value of the matricula property.
     * 
     */
    public void setMatricula(long value) {
        this.matricula = value;
    }

    /**
     * Gets the value of the numeroContas property.
     * 
     */
    public int getNumeroContas() {
        return numeroContas;
    }

    /**
     * Sets the value of the numeroContas property.
     * 
     */
    public void setNumeroContas(int value) {
        this.numeroContas = value;
    }

}
