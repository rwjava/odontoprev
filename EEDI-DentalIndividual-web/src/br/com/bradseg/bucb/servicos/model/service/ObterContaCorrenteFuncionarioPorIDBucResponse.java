
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ListaContaCorrenteFuncionarioWsVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="obterContaCorrenteFuncionarioPorIDBucReturn" type="{http" + "://vo.model.servicos.bucb.bradseg.com.br}ListaContaCorrenteFuncionarioWsVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "obterContaCorrenteFuncionarioPorIDBucReturn"
})
@XmlRootElement(name = "obterContaCorrenteFuncionarioPorIDBucResponse")
public class ObterContaCorrenteFuncionarioPorIDBucResponse {

    @XmlElement(required = true, nillable = true)
    protected ListaContaCorrenteFuncionarioWsVO obterContaCorrenteFuncionarioPorIDBucReturn;

    /**
     * Gets the value of the obterContaCorrenteFuncionarioPorIDBucReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ListaContaCorrenteFuncionarioWsVO }
     *     
     */
    public ListaContaCorrenteFuncionarioWsVO getObterContaCorrenteFuncionarioPorIDBucReturn() {
        return obterContaCorrenteFuncionarioPorIDBucReturn;
    }

    /**
     * Sets the value of the obterContaCorrenteFuncionarioPorIDBucReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaContaCorrenteFuncionarioWsVO }
     *     
     */
    public void setObterContaCorrenteFuncionarioPorIDBucReturn(ListaContaCorrenteFuncionarioWsVO value) {
        this.obterContaCorrenteFuncionarioPorIDBucReturn = value;
    }

}
