
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http" + "://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="consultarContaCorrentePorCPFReturn" type="{http" + "://service.model.servicos.bucb.bradseg.com.br}ArrayOf_1799424901_nillable_ContaCorrenteCpfVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarContaCorrentePorCPFReturn"
})
@XmlRootElement(name = "consultarContaCorrentePorCPFResponse")
public class ConsultarContaCorrentePorCPFResponse {

    @XmlElement(required = true, nillable = true)
    protected ArrayOf1799424901NillableContaCorrenteCpfVO consultarContaCorrentePorCPFReturn;

    /**
     * Gets the value of the consultarContaCorrentePorCPFReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOf1799424901NillableContaCorrenteCpfVO }
     *     
     */
    public ArrayOf1799424901NillableContaCorrenteCpfVO getConsultarContaCorrentePorCPFReturn() {
        return consultarContaCorrentePorCPFReturn;
    }

    /**
     * Sets the value of the consultarContaCorrentePorCPFReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOf1799424901NillableContaCorrenteCpfVO }
     *     
     */
    public void setConsultarContaCorrentePorCPFReturn(ArrayOf1799424901NillableContaCorrenteCpfVO value) {
        this.consultarContaCorrentePorCPFReturn = value;
    }

}
