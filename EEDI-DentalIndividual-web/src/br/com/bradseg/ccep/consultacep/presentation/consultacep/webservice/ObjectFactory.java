
package br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObterListaPorLogradouro_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaPorLogradouro");
    private final static QName _ObterListaPorRaizCep_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaPorRaizCep");
    private final static QName _ObterListaPorCepCompleto_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaPorCepCompleto");
    private final static QName _ObterListaPorCepCompletoResponse_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaPorCepCompletoResponse");
    private final static QName _ObterTipoLogradouro_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterTipoLogradouro");
    private final static QName _ObterListaPorLogradouroResponse_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaPorLogradouroResponse");
    private final static QName _ObterListaLogradouroPorCepResponse_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaLogradouroPorCepResponse");
    private final static QName _ObterTipoLogradouroResponse_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterTipoLogradouroResponse");
    private final static QName _ObterListaLogradouroCompletoPorCep_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaLogradouroCompletoPorCep");
    private final static QName _IntegrationException_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "IntegrationException");
    private final static QName _ObterListaLogradouroCompletoPorCepResponse_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaLogradouroCompletoPorCepResponse");
    private final static QName _ObterListaPorRaizCepResponse_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaPorRaizCepResponse");
    private final static QName _ObterListaLogradouroPorCep_QNAME = new QName("http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", "obterListaLogradouroPorCep");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObterListaPorLogradouroResponse }
     * 
     */
    public ObterListaPorLogradouroResponse createObterListaPorLogradouroResponse() {
        return new ObterListaPorLogradouroResponse();
    }

    /**
     * Create an instance of {@link ObterListaPorLogradouro }
     * 
     */
    public ObterListaPorLogradouro createObterListaPorLogradouro() {
        return new ObterListaPorLogradouro();
    }

    /**
     * Create an instance of {@link ObterListaPorRaizCep }
     * 
     */
    public ObterListaPorRaizCep createObterListaPorRaizCep() {
        return new ObterListaPorRaizCep();
    }

    /**
     * Create an instance of {@link ObterListaPorCepCompleto }
     * 
     */
    public ObterListaPorCepCompleto createObterListaPorCepCompleto() {
        return new ObterListaPorCepCompleto();
    }

    /**
     * Create an instance of {@link ObterListaPorCepCompletoResponse }
     * 
     */
    public ObterListaPorCepCompletoResponse createObterListaPorCepCompletoResponse() {
        return new ObterListaPorCepCompletoResponse();
    }

    /**
     * Create an instance of {@link ObterTipoLogradouro }
     * 
     */
    public ObterTipoLogradouro createObterTipoLogradouro() {
        return new ObterTipoLogradouro();
    }

    /**
     * Create an instance of {@link ObterListaLogradouroCompletoPorCepResponse }
     * 
     */
    public ObterListaLogradouroCompletoPorCepResponse createObterListaLogradouroCompletoPorCepResponse() {
        return new ObterListaLogradouroCompletoPorCepResponse();
    }

    /**
     * Create an instance of {@link ObterListaPorRaizCepResponse }
     * 
     */
    public ObterListaPorRaizCepResponse createObterListaPorRaizCepResponse() {
        return new ObterListaPorRaizCepResponse();
    }

    /**
     * Create an instance of {@link ObterListaLogradouroPorCep }
     * 
     */
    public ObterListaLogradouroPorCep createObterListaLogradouroPorCep() {
        return new ObterListaLogradouroPorCep();
    }

    /**
     * Create an instance of {@link ObterListaLogradouroPorCepResponse }
     * 
     */
    public ObterListaLogradouroPorCepResponse createObterListaLogradouroPorCepResponse() {
        return new ObterListaLogradouroPorCepResponse();
    }

    /**
     * Create an instance of {@link ObterTipoLogradouroResponse }
     * 
     */
    public ObterTipoLogradouroResponse createObterTipoLogradouroResponse() {
        return new ObterTipoLogradouroResponse();
    }

    /**
     * Create an instance of {@link IntegrationException }
     * 
     */
    public IntegrationException createIntegrationException() {
        return new IntegrationException();
    }

    /**
     * Create an instance of {@link ObterListaLogradouroCompletoPorCep }
     * 
     */
    public ObterListaLogradouroCompletoPorCep createObterListaLogradouroCompletoPorCep() {
        return new ObterListaLogradouroCompletoPorCep();
    }

    /**
     * Create an instance of {@link CepVO }
     * 
     */
    public CepVO createCepVO() {
        return new CepVO();
    }

    /**
     * Create an instance of {@link CepCompletoVO }
     * 
     */
    public CepCompletoVO createCepCompletoVO() {
        return new CepCompletoVO();
    }

    /**
     * Create an instance of {@link TipoLogradouroVO }
     * 
     */
    public TipoLogradouroVO createTipoLogradouroVO() {
        return new TipoLogradouroVO();
    }

    /**
     * Create an instance of {@link CepCompletoWebServiceResponse }
     * 
     */
    public CepCompletoWebServiceResponse createCepCompletoWebServiceResponse() {
        return new CepCompletoWebServiceResponse();
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link CepWebServiceResponse }
     * 
     */
    public CepWebServiceResponse createCepWebServiceResponse() {
        return new CepWebServiceResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaPorLogradouro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaPorLogradouro")
    public JAXBElement<ObterListaPorLogradouro> createObterListaPorLogradouro(ObterListaPorLogradouro value) {
        return new JAXBElement<ObterListaPorLogradouro>(_ObterListaPorLogradouro_QNAME, ObterListaPorLogradouro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaPorRaizCep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaPorRaizCep")
    public JAXBElement<ObterListaPorRaizCep> createObterListaPorRaizCep(ObterListaPorRaizCep value) {
        return new JAXBElement<ObterListaPorRaizCep>(_ObterListaPorRaizCep_QNAME, ObterListaPorRaizCep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaPorCepCompleto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaPorCepCompleto")
    public JAXBElement<ObterListaPorCepCompleto> createObterListaPorCepCompleto(ObterListaPorCepCompleto value) {
        return new JAXBElement<ObterListaPorCepCompleto>(_ObterListaPorCepCompleto_QNAME, ObterListaPorCepCompleto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaPorCepCompletoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaPorCepCompletoResponse")
    public JAXBElement<ObterListaPorCepCompletoResponse> createObterListaPorCepCompletoResponse(ObterListaPorCepCompletoResponse value) {
        return new JAXBElement<ObterListaPorCepCompletoResponse>(_ObterListaPorCepCompletoResponse_QNAME, ObterListaPorCepCompletoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTipoLogradouro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterTipoLogradouro")
    public JAXBElement<ObterTipoLogradouro> createObterTipoLogradouro(ObterTipoLogradouro value) {
        return new JAXBElement<ObterTipoLogradouro>(_ObterTipoLogradouro_QNAME, ObterTipoLogradouro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaPorLogradouroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaPorLogradouroResponse")
    public JAXBElement<ObterListaPorLogradouroResponse> createObterListaPorLogradouroResponse(ObterListaPorLogradouroResponse value) {
        return new JAXBElement<ObterListaPorLogradouroResponse>(_ObterListaPorLogradouroResponse_QNAME, ObterListaPorLogradouroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaLogradouroPorCepResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaLogradouroPorCepResponse")
    public JAXBElement<ObterListaLogradouroPorCepResponse> createObterListaLogradouroPorCepResponse(ObterListaLogradouroPorCepResponse value) {
        return new JAXBElement<ObterListaLogradouroPorCepResponse>(_ObterListaLogradouroPorCepResponse_QNAME, ObterListaLogradouroPorCepResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterTipoLogradouroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterTipoLogradouroResponse")
    public JAXBElement<ObterTipoLogradouroResponse> createObterTipoLogradouroResponse(ObterTipoLogradouroResponse value) {
        return new JAXBElement<ObterTipoLogradouroResponse>(_ObterTipoLogradouroResponse_QNAME, ObterTipoLogradouroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaLogradouroCompletoPorCep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaLogradouroCompletoPorCep")
    public JAXBElement<ObterListaLogradouroCompletoPorCep> createObterListaLogradouroCompletoPorCep(ObterListaLogradouroCompletoPorCep value) {
        return new JAXBElement<ObterListaLogradouroCompletoPorCep>(_ObterListaLogradouroCompletoPorCep_QNAME, ObterListaLogradouroCompletoPorCep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntegrationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "IntegrationException")
    public JAXBElement<IntegrationException> createIntegrationException(IntegrationException value) {
        return new JAXBElement<IntegrationException>(_IntegrationException_QNAME, IntegrationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaLogradouroCompletoPorCepResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaLogradouroCompletoPorCepResponse")
    public JAXBElement<ObterListaLogradouroCompletoPorCepResponse> createObterListaLogradouroCompletoPorCepResponse(ObterListaLogradouroCompletoPorCepResponse value) {
        return new JAXBElement<ObterListaLogradouroCompletoPorCepResponse>(_ObterListaLogradouroCompletoPorCepResponse_QNAME, ObterListaLogradouroCompletoPorCepResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaPorRaizCepResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaPorRaizCepResponse")
    public JAXBElement<ObterListaPorRaizCepResponse> createObterListaPorRaizCepResponse(ObterListaPorRaizCepResponse value) {
        return new JAXBElement<ObterListaPorRaizCepResponse>(_ObterListaPorRaizCepResponse_QNAME, ObterListaPorRaizCepResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterListaLogradouroPorCep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.consultacep.presentation.consultacep.ccep.bradseg.com.br/", name = "obterListaLogradouroPorCep")
    public JAXBElement<ObterListaLogradouroPorCep> createObterListaLogradouroPorCep(ObterListaLogradouroPorCep value) {
        return new JAXBElement<ObterListaLogradouroPorCep>(_ObterListaLogradouroPorCep_QNAME, ObterListaLogradouroPorCep.class, null, value);
    }

}
