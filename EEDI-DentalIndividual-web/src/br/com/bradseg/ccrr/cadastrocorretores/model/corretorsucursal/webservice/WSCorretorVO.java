
package br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSCorretorVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSCorretorVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoCPD" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoCpfCnpjCorretor" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoSituacaoSusep" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoSucursalSeguradora" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoSusepCorretor" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoTipoPessoa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoTipoProdutor" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="descricaoSituacaoSusepCompleta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descricaoSituacaoSusepResumida" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="listaContatosEmail" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *         &lt;element name="listaEnderecosComerciais" type="{http://ws.vo.model.cadastrocorretores.ccrr.bradseg.com.br}WSEnderecoVO" maxOccurs="unbounded"/>
 *         &lt;element name="nomeCorretor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="listaTelefones" type="{http://ws.vo.model.cadastrocorretores.ccrr.bradseg.com.br}WSTelefoneVO" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSCorretorVO", namespace = "http://ws.vo.model.cadastrocorretores.ccrr.bradseg.com.br", propOrder = {
    "codigoCPD",
    "codigoCpfCnpjCorretor",
    "codigoSituacaoSusep",
    "codigoSucursalSeguradora",
    "codigoSusepCorretor",
    "codigoTipoPessoa",
    "codigoTipoProdutor",
    "descricaoSituacaoSusepCompleta",
    "descricaoSituacaoSusepResumida",
    "listaContatosEmail",
    "listaEnderecosComerciais",
    "nomeCorretor",
    "listaTelefones"
})
public class WSCorretorVO {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoCPD;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoCpfCnpjCorretor;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoSituacaoSusep;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoSucursalSeguradora;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoSusepCorretor;
    @XmlElement(required = true, nillable = true)
    protected String codigoTipoPessoa;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoTipoProdutor;
    @XmlElement(required = true, nillable = true)
    protected String descricaoSituacaoSusepCompleta;
    @XmlElement(required = true, nillable = true)
    protected String descricaoSituacaoSusepResumida;
    @XmlElement(required = true, nillable = true)
    protected List<String> listaContatosEmail;
    @XmlElement(required = true, nillable = true)
    protected List<WSEnderecoVO> listaEnderecosComerciais;
    @XmlElement(required = true, nillable = true)
    protected String nomeCorretor;
    @XmlElement(required = true, nillable = true)
    protected List<WSTelefoneVO> listaTelefones;

    /**
     * Gets the value of the codigoCPD property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoCPD() {
        return codigoCPD;
    }

    /**
     * Sets the value of the codigoCPD property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoCPD(Long value) {
        this.codigoCPD = value;
    }

    /**
     * Gets the value of the codigoCpfCnpjCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoCpfCnpjCorretor() {
        return codigoCpfCnpjCorretor;
    }

    /**
     * Sets the value of the codigoCpfCnpjCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoCpfCnpjCorretor(Long value) {
        this.codigoCpfCnpjCorretor = value;
    }

    /**
     * Gets the value of the codigoSituacaoSusep property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoSituacaoSusep() {
        return codigoSituacaoSusep;
    }

    /**
     * Sets the value of the codigoSituacaoSusep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoSituacaoSusep(Long value) {
        this.codigoSituacaoSusep = value;
    }

    /**
     * Gets the value of the codigoSucursalSeguradora property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoSucursalSeguradora() {
        return codigoSucursalSeguradora;
    }

    /**
     * Sets the value of the codigoSucursalSeguradora property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoSucursalSeguradora(Integer value) {
        this.codigoSucursalSeguradora = value;
    }

    /**
     * Gets the value of the codigoSusepCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoSusepCorretor() {
        return codigoSusepCorretor;
    }

    /**
     * Sets the value of the codigoSusepCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoSusepCorretor(Long value) {
        this.codigoSusepCorretor = value;
    }

    /**
     * Gets the value of the codigoTipoPessoa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTipoPessoa() {
        return codigoTipoPessoa;
    }

    /**
     * Sets the value of the codigoTipoPessoa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTipoPessoa(String value) {
        this.codigoTipoPessoa = value;
    }

    /**
     * Gets the value of the codigoTipoProdutor property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoTipoProdutor() {
        return codigoTipoProdutor;
    }

    /**
     * Sets the value of the codigoTipoProdutor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoTipoProdutor(Integer value) {
        this.codigoTipoProdutor = value;
    }

    /**
     * Gets the value of the descricaoSituacaoSusepCompleta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoSituacaoSusepCompleta() {
        return descricaoSituacaoSusepCompleta;
    }

    /**
     * Sets the value of the descricaoSituacaoSusepCompleta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoSituacaoSusepCompleta(String value) {
        this.descricaoSituacaoSusepCompleta = value;
    }

    /**
     * Gets the value of the descricaoSituacaoSusepResumida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoSituacaoSusepResumida() {
        return descricaoSituacaoSusepResumida;
    }

    /**
     * Sets the value of the descricaoSituacaoSusepResumida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoSituacaoSusepResumida(String value) {
        this.descricaoSituacaoSusepResumida = value;
    }

    /**
     * Gets the value of the listaContatosEmail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaContatosEmail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaContatosEmail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getListaContatosEmail() {
        if (listaContatosEmail == null) {
            listaContatosEmail = new ArrayList<String>();
        }
        return this.listaContatosEmail;
    }

    /**
     * Gets the value of the listaEnderecosComerciais property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaEnderecosComerciais property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaEnderecosComerciais().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSEnderecoVO }
     * 
     * 
     */
    public List<WSEnderecoVO> getListaEnderecosComerciais() {
        if (listaEnderecosComerciais == null) {
            listaEnderecosComerciais = new ArrayList<WSEnderecoVO>();
        }
        return this.listaEnderecosComerciais;
    }

    /**
     * Gets the value of the nomeCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCorretor() {
        return nomeCorretor;
    }

    /**
     * Sets the value of the nomeCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCorretor(String value) {
        this.nomeCorretor = value;
    }

    /**
     * Gets the value of the listaTelefones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaTelefones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaTelefones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSTelefoneVO }
     * 
     * 
     */
    public List<WSTelefoneVO> getListaTelefones() {
        if (listaTelefones == null) {
            listaTelefones = new ArrayList<WSTelefoneVO>();
        }
        return this.listaTelefones;
    }

}
