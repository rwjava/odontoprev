
package br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WSBusinessException_QNAME = new QName("http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br", "WSBusinessException");
    private final static QName _WSIntegrationException_QNAME = new QName("http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br", "WSIntegrationException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSBusinessException }
     * 
     */
    public WSBusinessException createWSBusinessException() {
        return new WSBusinessException();
    }

    /**
     * Create an instance of {@link WSIntegrationException }
     * 
     */
    public WSIntegrationException createWSIntegrationException() {
        return new WSIntegrationException();
    }

    /**
     * Create an instance of {@link WSCorretorVO }
     * 
     */
    public WSCorretorVO createWSCorretorVO() {
        return new WSCorretorVO();
    }

    /**
     * Create an instance of {@link WSEnderecoVO }
     * 
     */
    public WSEnderecoVO createWSEnderecoVO() {
        return new WSEnderecoVO();
    }

    /**
     * Create an instance of {@link WSTelefoneVO }
     * 
     */
    public WSTelefoneVO createWSTelefoneVO() {
        return new WSTelefoneVO();
    }

    /**
     * Create an instance of {@link ObterDadosCorretorPorCpdResponse }
     * 
     */
    public ObterDadosCorretorPorCpdResponse createObterDadosCorretorPorCpdResponse() {
        return new ObterDadosCorretorPorCpdResponse();
    }

    /**
     * Create an instance of {@link ObterDadosCorretorPorCpd }
     * 
     */
    public ObterDadosCorretorPorCpd createObterDadosCorretorPorCpd() {
        return new ObterDadosCorretorPorCpd();
    }

    /**
     * Create an instance of {@link ObterMeiosContatoCorretorNaSucursalResponse }
     * 
     */
    public ObterMeiosContatoCorretorNaSucursalResponse createObterMeiosContatoCorretorNaSucursalResponse() {
        return new ObterMeiosContatoCorretorNaSucursalResponse();
    }

    /**
     * Create an instance of {@link ObterCorretorResponse }
     * 
     */
    public ObterCorretorResponse createObterCorretorResponse() {
        return new ObterCorretorResponse();
    }

    /**
     * Create an instance of {@link ObterCorretor }
     * 
     */
    public ObterCorretor createObterCorretor() {
        return new ObterCorretor();
    }

    /**
     * Create an instance of {@link ObterMeiosContatoCorretorNaSucursal }
     * 
     */
    public ObterMeiosContatoCorretorNaSucursal createObterMeiosContatoCorretorNaSucursal() {
        return new ObterMeiosContatoCorretorNaSucursal();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSBusinessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br", name = "WSBusinessException")
    public JAXBElement<WSBusinessException> createWSBusinessException(WSBusinessException value) {
        return new JAXBElement<WSBusinessException>(_WSBusinessException_QNAME, WSBusinessException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSIntegrationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.exception.cadastrocorretores.ccrr.bradseg.com.br", name = "WSIntegrationException")
    public JAXBElement<WSIntegrationException> createWSIntegrationException(WSIntegrationException value) {
        return new JAXBElement<WSIntegrationException>(_WSIntegrationException_QNAME, WSIntegrationException.class, null, value);
    }

}
