package br.com.bradseg.eedi.dentalindividual.canalvenda.facade;

import br.com.bradseg.eedi.dentalindividual.vo.CanalVO;

public interface CanalVendaService {

	public CanalVO consultarPorCodigo(Integer codigo);

}
