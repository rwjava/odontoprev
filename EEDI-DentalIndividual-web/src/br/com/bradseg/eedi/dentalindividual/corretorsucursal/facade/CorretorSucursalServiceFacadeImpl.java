package br.com.bradseg.eedi.dentalindividual.corretorsucursal.facade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.dentalindividual.proposta.facade.SucursalCorretorService;
import br.com.bradseg.eedi.dentalindividual.util.Utils;
import br.com.bradseg.eedi.dentalindividual.vo.SucursalVO;
import br.com.bradseg.inet.consultaexpressa.webservice.BusinessException_Exception;
import br.com.bradseg.inet.consultaexpressa.webservice.CorretorSucursal;
import br.com.bradseg.inet.consultaexpressa.webservice.CorretorSucursalWebService;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CorretorSucursalServiceFacadeImpl implements CorretorSucursalServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(CorretorSucursalServiceFacadeImpl.class);

	@Autowired
	private CorretorSucursalWebService corretorSucursalWebService;

	@Autowired
	private SucursalCorretorService sucursalService;

	//	@Override
	public List<SucursalVO> obterSucursaisCorretor(String cpfCnpjCorretor) {
		LOGGER.error("Obter sucursais para corretor: "+cpfCnpjCorretor);
		List<CorretorSucursal> listaSucursaisCorretor = new ArrayList<CorretorSucursal>();

		listaSucursaisCorretor.addAll(obterSucursaisComCia0(listaSucursaisCorretor, cpfCnpjCorretor));
		listaSucursaisCorretor.addAll(obterSucursaisComCia1(listaSucursaisCorretor, cpfCnpjCorretor));
		if (listaSucursaisCorretor != null) {
			try{
				Utils.ordernarRemoverSucursaisRepetidas(listaSucursaisCorretor);
			}catch(Exception e){
				LOGGER.error(String.format("Erro ao remover sucursais repitidas. [%s]", e.getMessage()));
			}
		}

		List<SucursalVO> listaSucursais = new ArrayList<SucursalVO>();

		for (CorretorSucursal sucursal : listaSucursaisCorretor) {
			SucursalVO sucursalTela = new SucursalVO();
			sucursalTela.setCodigo(sucursal.getCodigoSucursal());
			sucursalTela.setNome(sucursal.getDescricaoConcatenada());
			sucursalTela.setTipo(Utils.retornaTipoSucursalPeloNome(sucursalTela.getNome()));
			sucursalTela.setCpd(sucursal.getCodigoCorretor());
			sucursalTela.setCpdSucursal(sucursal.getCodigoSucursal() + ";" + sucursal.getCodigoCorretor());
			listaSucursais.add(sucursalTela);
		}

		List<SucursalVO> listaSucursalCorretorFinal = new ArrayList<SucursalVO>();
		//listaSucursalCorretorFinal.addAll(listaSucursais);

		if (listaSucursaisCorretor != null) {
			trataLista(listaSucursaisCorretor);

			listaSucursalCorretorFinal = new ArrayList<SucursalVO>();

			/*listaSucursaisCorretor = new ArrayList<SucursalCPDVO>(listaSucursaisCorretor.size());*/

			// TODO Extrair para m�todo externo
			for (CorretorSucursal cs : listaSucursaisCorretor) {
				Integer codigoSucursal = cs.getCodigoSucursal();

				SucursalVO vo = new SucursalVO();
				
				vo.setCodigo(cs.getCodigoSucursal());
				vo.setNome(cs.getDescricaoConcatenada());
				vo.setTipo(Utils.retornaTipoSucursalPeloNome(vo.getNome()));
				vo.setCpd(cs.getCodigoCorretor());
				vo.setCpdSucursal(cs.getCodigoSucursal() + ";" + cs.getCodigoCorretor());
				listaSucursais.add(vo);
				
				//vo.setNome(cs.getDescricaoConcatenada());
				//vo.setCpd(cs.getCodigoCorretor()); // CPD
				//vo.setCodigo(codigoSucursal);

				boolean sucursalValida = sucursalService.validarSucursalCorretor(codigoSucursal);
				if (!sucursalValida) {
					continue; // nao adiciona
				}

				listaSucursalCorretorFinal.add(vo);
			}
		}

		return listaSucursalCorretorFinal;

	}

	private List<CorretorSucursal> obterSucursaisComCia0(List<CorretorSucursal> listaSucursaisCorretor, String cpfCnpjCorretor) {
		try {
			listaSucursaisCorretor.addAll(corretorSucursalWebService.consultarListaCorretorSucursal(cpfCnpjCorretor,"0"));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro ao Buscar Sucursais com cia 0" + e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return listaSucursaisCorretor;
	}

	private List<CorretorSucursal> obterSucursaisComCia1(List<CorretorSucursal> listaSucursaisCorretor, String cpfCnpjCorretor) {
		try {
			listaSucursaisCorretor.addAll(corretorSucursalWebService.consultarListaCorretorSucursal(cpfCnpjCorretor, "1"));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro ao Buscar Sucursais com cia 1" + e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return listaSucursaisCorretor;
	}

	/**
	 * Ordena a lista e verifica os repetidos
	 * 
	 * @param lista lista
	 */
	// TODO Porcaria de tratamento, refatorar...
	private void trataLista(List<CorretorSucursal> lista) {
		// Ordena a lista para aplicar o remove
		Collections.sort(lista, new Comparator<CorretorSucursal>() {
			public int compare(CorretorSucursal cs1, CorretorSucursal cs2) {
				if(cs1.getDescricaoConcatenada() != null && cs2.getDescricaoConcatenada() != null){
					return cs1.getDescricaoConcatenada().compareTo(cs2.getDescricaoConcatenada());
				}else{
					return -999;
				}
			}
		});

		CorretorSucursal corretorSucursalVerifica = new CorretorSucursal();

		// Remove caso seja igual
		for (Iterator<CorretorSucursal> iterator = lista.iterator(); iterator.hasNext();) {
			CorretorSucursal corretorSucursal = iterator.next();
			if (corretorSucursalVerifica.getDescricaoConcatenada() != null && corretorSucursalVerifica.getDescricaoConcatenada().equals(corretorSucursal.getDescricaoConcatenada())) {
				iterator.remove();
			}
			corretorSucursalVerifica = corretorSucursal;
		}

		// Ordena lista para exibir
		Collections.sort(lista, new Comparator<CorretorSucursal>() {
			public int compare(CorretorSucursal cs1, CorretorSucursal cs2) {
				return cs1.getCodigoSucursal().compareTo(cs2.getCodigoSucursal());
			}
		});
	}

}
