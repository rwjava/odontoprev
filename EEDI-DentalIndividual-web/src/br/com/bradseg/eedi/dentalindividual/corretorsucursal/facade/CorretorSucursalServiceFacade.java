package br.com.bradseg.eedi.dentalindividual.corretorsucursal.facade;

import java.util.List;

import br.com.bradseg.eedi.dentalindividual.vo.SucursalVO;

public interface CorretorSucursalServiceFacade {
	
	public List<SucursalVO> obterSucursaisCorretor(String cpfCnpjCorretor);

}
