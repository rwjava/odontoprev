package br.com.bradseg.eedi.dentalindividual.odontoprev.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RetornoVO implements Serializable {

	private static final long serialVersionUID = -9136475576650998964L;

	private int sucesso;
	private String mensagem;
	private boolean habilitado;

	/**
	 * Retorna sucesso.
	 *
	 * @return sucesso - sucesso
	 */
	public int getSucesso() {
		return sucesso;
	}

	/**
	 * Especifica sucesso.
	 *
	 * @param sucesso - sucesso
	 */
	public void setSucesso(int sucesso) {
		this.sucesso = sucesso;
	}

	/**
	 * Retorna mensagem.
	 *
	 * @return mensagem - mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Especifica mensagem.
	 *
	 * @param mensagem - mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

}

