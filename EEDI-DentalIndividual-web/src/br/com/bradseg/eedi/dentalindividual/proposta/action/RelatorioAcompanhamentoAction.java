package br.com.bradseg.eedi.dentalindividual.proposta.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.eedi.dentalindividual.proposta.facade.GerarArquivoRelatorioEEDI;
import br.com.bradseg.eedi.dentalindividual.proposta.facade.RelatorioAcompanhamentoServiceFacade;
import br.com.bradseg.eedi.dentalindividual.vo.AcompanhamentoPropostaEEDIVO;
import br.com.bradseg.eedi.dentalindividual.vo.FiltroRelatorioAcompanhamentoEEDIVO;
import br.com.bradseg.eedi.dentalindividual.vo.RelatorioAcompanhamentoPropostaEEDIVO;

@Controller
@Scope(value = "request")
public class RelatorioAcompanhamentoAction  extends BaseAction{

    private static final long serialVersionUID = -1905089433647044580L;

    private static final Logger LOGGER =  LoggerFactory.getLogger(RelatorioAcompanhamentoAction.class);
	private static final int BUFSIZE = 1024;

    private FiltroRelatorioAcompanhamentoEEDIVO  filtroRelatorioAcompanhamentoVO = new FiltroRelatorioAcompanhamentoEEDIVO();
    
    private List<AcompanhamentoPropostaEEDIVO> listaAcompanhamento;
    
    private RelatorioAcompanhamentoPropostaEEDIVO relatorioAcompanhamentoPropostaVO;

    @Autowired
    private RelatorioAcompanhamentoServiceFacade relatorioAcompanhamentoServiceFacade;
    
    @Autowired
    private GerarArquivoRelatorioEEDI gerarArquivoRelatorio;
    
    private InputStream inputStream;
    private String fileName;
    
    private int tamanhoArquivo;
    public String relatorioAcompanhamento() {
	return SUCCESS;
    }

    
    public String buscarAcompanhamento(){
		
    	relatorioAcompanhamentoPropostaVO = new RelatorioAcompanhamentoPropostaEEDIVO();
		filtroRelatorioAcompanhamentoVO.setCpfCnpjCorretor(obterDadosCorretorLogado().getCpfCnpj());
		try{
			setListaAcompanhamento(relatorioAcompanhamentoServiceFacade.listarPropostasRelatorioAcompanhamentoPorFiltro(filtroRelatorioAcompanhamentoVO));		
			
			relatorioAcompanhamentoPropostaVO.setValorRelatorioAcompanhamentoPropostaVO(relatorioAcompanhamentoServiceFacade.obterValoresPropostas(listaAcompanhamento));
		}catch(Exception e){
			LOGGER.error("Erro na chamada do servico: "+ e.getMessage());
		}
		filtroRelatorioAcompanhamentoVO.setCpfCnpjCorretor(null);
		return SUCCESS;
    }
    
    
    public String buscarAcompanhamentoIntranet(){
    	
    	relatorioAcompanhamentoPropostaVO = new RelatorioAcompanhamentoPropostaEEDIVO();
		filtroRelatorioAcompanhamentoVO.setCpfCnpjCorretor(obterDadosCorretorLogado().getCpfCnpj());
		filtroRelatorioAcompanhamentoVO.setIndicadorIntranet(true);
		try{
			setListaAcompanhamento(relatorioAcompanhamentoServiceFacade.listarPropostasRelatorioAcompanhamentoPorFiltro(filtroRelatorioAcompanhamentoVO));
			
			relatorioAcompanhamentoPropostaVO.setValorRelatorioAcompanhamentoPropostaVO(relatorioAcompanhamentoServiceFacade.obterValoresPropostas(listaAcompanhamento));
		}catch(Exception e){
			LOGGER.error("Erro na chamada do servico: "+ e.getMessage());
		}
		filtroRelatorioAcompanhamentoVO.setCpfCnpjCorretor(null);
		return SUCCESS;
    }
    public String gerarPDFAcompanhamento(){
    	
    	LOGGER.error("Inicio do metodo gerarPDFAcompanhamento()");
    	relatorioAcompanhamentoPropostaVO = new RelatorioAcompanhamentoPropostaEEDIVO();
		filtroRelatorioAcompanhamentoVO.setCpfCnpjCorretor(obterDadosCorretorLogado().getCpfCnpj());
		
    	try{    		
	    		byte[] relatorioPDF =  gerarArquivoRelatorio.gerar(relatorioAcompanhamentoServiceFacade.gerarPDFRelatorioAcompanhamento(filtroRelatorioAcompanhamentoVO, null));   		   	        
	    		tamanhoArquivo = relatorioPDF.length;
    	        inputStream = new ByteArrayInputStream(relatorioPDF);
    	}catch(Exception e){
    			LOGGER.error("Erro gerarRelatorioAcompanhamento :"+e.getMessage());
    			return INPUT;
    	}
    	LOGGER.error("fim do metodo gerarPDFAcompanhamento()");
    	return SUCCESS;
    }
    
    public String gerarPDFAcompanhamentoIntranet(){
    	
    	LOGGER.error("Inicio do metodo gerarPDFAcompanhamentoIntranet()");
    	relatorioAcompanhamentoPropostaVO = new RelatorioAcompanhamentoPropostaEEDIVO();
		filtroRelatorioAcompanhamentoVO.setCpfCnpjCorretor(obterDadosCorretorLogado().getCpfCnpj());
		
    	try{    
    			filtroRelatorioAcompanhamentoVO.setIndicadorIntranet(true);
	    		byte[] relatorioPDF =  gerarArquivoRelatorio.gerar(relatorioAcompanhamentoServiceFacade.gerarPDFRelatorioAcompanhamento(filtroRelatorioAcompanhamentoVO, null));   		   	        
	    		tamanhoArquivo = relatorioPDF.length;
    	        inputStream = new ByteArrayInputStream(relatorioPDF);
    	}catch(Exception e){
    			LOGGER.error("Erro gerarRelatorioAcompanhamento :"+e.getMessage());
    			return INPUT;
    	}
    	LOGGER.error("fim do metodo gerarPDFAcompanhamentoIntranet()");
    	return SUCCESS;
    }
    
	public String gerarArquivoCSVRelatorioAcompanhamento(){
	
		try{
			File arquivoCSV = relatorioAcompanhamentoServiceFacade.gerarArquivoCSVRelatorioAcompanhamento(filtroRelatorioAcompanhamentoVO, null);		

    		tamanhoArquivo = Long.valueOf(arquivoCSV.length()).intValue();
    		fileName = arquivoCSV.getAbsoluteFile().getName();
	        inputStream = new FileInputStream(arquivoCSV);
	        
		}catch(Exception e){
			LOGGER.error("Erro ao gerar arquivo csv. : " + e.getMessage());
			return INPUT;
		}     
		return SUCCESS;
	}
	
	public String gerarArquivoCSVRelatorioAcompanhamentoIntranet(){
		
		try{
			filtroRelatorioAcompanhamentoVO.setIndicadorIntranet(true);
			File arquivoCSV = relatorioAcompanhamentoServiceFacade.gerarArquivoCSVRelatorioAcompanhamento(filtroRelatorioAcompanhamentoVO, null);		

    		tamanhoArquivo = Long.valueOf(arquivoCSV.length()).intValue();
    		fileName = arquivoCSV.getAbsoluteFile().getName();
	        inputStream = new FileInputStream(arquivoCSV);
	        
		}catch(Exception e){
			LOGGER.error("Erro ao gerar arquivo csv. : " + e.getMessage());
			return INPUT;
		}     
		return SUCCESS;
	}
	


	public FiltroRelatorioAcompanhamentoEEDIVO getFiltroRelatorioAcompanhamentoVO() {
		return filtroRelatorioAcompanhamentoVO;
	}


	public void setFiltroRelatorioAcompanhamentoVO(FiltroRelatorioAcompanhamentoEEDIVO filtroRelatorioAcompanhamentoVO) {
		this.filtroRelatorioAcompanhamentoVO = filtroRelatorioAcompanhamentoVO;
	}


	public List<AcompanhamentoPropostaEEDIVO> getListaAcompanhamento() {
		return listaAcompanhamento;
	}


	public void setListaAcompanhamento(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamento) {
		this.listaAcompanhamento = listaAcompanhamento;
	}


	public RelatorioAcompanhamentoPropostaEEDIVO getRelatorioAcompanhamentoPropostaVO() {
		return relatorioAcompanhamentoPropostaVO;
	}


	public void setRelatorioAcompanhamentoPropostaVO(RelatorioAcompanhamentoPropostaEEDIVO relatorioAcompanhamentoPropostaVO) {
		this.relatorioAcompanhamentoPropostaVO = relatorioAcompanhamentoPropostaVO;
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}


	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}


	public int getTamanhoArquivo() {
		return tamanhoArquivo;
	}


	public void setTamanhoArquivo(int tamanhoArquivo) {
		this.tamanhoArquivo = tamanhoArquivo;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
    
	
	
    
}

