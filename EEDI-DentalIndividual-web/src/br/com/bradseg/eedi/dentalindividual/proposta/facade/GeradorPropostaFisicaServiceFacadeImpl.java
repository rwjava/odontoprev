package br.com.bradseg.eedi.dentalindividual.proposta.facade;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.dentalindividual.odontoprev.facade.OdontoprevServiceFacade;
import br.com.bradseg.eedi.dentalindividual.plano.facade.PlanoServiceFacade;
import br.com.bradseg.eedi.dentalindividual.support.ReportFileUtil;
import br.com.bradseg.eedi.dentalindividual.support.ReportUtil;
import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.util.FormatacaoUtil;
import br.com.bradseg.eedi.dentalindividual.util.GeradorBoletoBancario;
import br.com.bradseg.eedi.dentalindividual.vo.Banco;
import br.com.bradseg.eedi.dentalindividual.vo.BoletoVO;
import br.com.bradseg.eedi.dentalindividual.vo.DependenteVO;
import br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento;
import br.com.bradseg.eedi.dentalindividual.vo.PlanoVO;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.RepresentanteLegalVO;
import br.com.bradseg.eedi.dentalindividual.vo.TipoCobranca;
import br.com.bradseg.eedi.dentalindividual.vo.TipoSucursalSeguradora;
import br.com.bradseg.eedi.dentalindividual.vo.TitularVO;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;


@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class GeradorPropostaFisicaServiceFacadeImpl implements GeradorPropostaFisicaServiceFacade{

	private static final String TERMO_CONTRATO = "termo_contrato";
	private static final String REGISTRO_ANS = "registro_ans";
	private static final String TIPO_PLANO_LOGO = "tipo_plano_logo";
	private static final String TIPO_PLANO = "tipo_plano";
	private static final String VIA = "via";
	private static final String EXIBIR_PONTILHADO = "exibirPontilhado";
	private static final String VIA_COM_PONTILHADO = "viaComPontilhado";
	private static final String CONTADOR_BOLETO = "contadorBoleto";
	private static final String CONTADOR_VIA = "contadorVia";
	private static final int BENEFICIARIO_TITULAR = 1;
	@Autowired
	private PropostaServiceFacade propostaServiceFacade;
	@Autowired
	private PlanoServiceFacade planoServiceFacade;
	@Autowired
	private GerarArquivoRelatorioEEDI gerarArquivoRelatorioEEDI;
	@Autowired
	private SucursalCorretorService sucursalCorretorService;
	@Autowired
	private OdontoprevServiceFacade odontoprevServiceFacade;
	
	public  byte[] gerarPDFProposta(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto){
		return gerar(gerarRelatorioPropostaCompleta(codigoProposta, gerarViaOperadora, gerarViaCorretor, gerarBoleto));
	}

	/**
	 * Consulta as informa��es detalhadas da proposta e gera o arquivo de relat�rio.
	 * @return Arquivo de relat�rio
	 */
	public ReportFileUtil gerarRelatorioPropostaCompleta(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto) {
		return gerarRelatorio(propostaServiceFacade.obterPropostaPorSequencial(codigoProposta), gerarViaOperadora, gerarViaCorretor, gerarBoleto, false);
	}
	
	
	private byte[] gerar(ReportFileUtil arquivo){
		return gerarArquivoRelatorioEEDI.gerar(arquivo);
	}
	
	public  byte[] gerarPDFPropostaVazia(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto){
		return gerar(gerarRelatorioPropostaVazia(codigoProposta, gerarViaOperadora, gerarViaCorretor, gerarBoleto));
	}

	private ReportFileUtil gerarRelatorioPropostaVazia(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto) {
		PropostaVO propostaVazia = propostaServiceFacade.obterPropostaPorSequencial(codigoProposta);
		propostaVazia.getBeneficiarios().getDependentes().add(new DependenteVO());
		propostaVazia.getBeneficiarios().getDependentes().add(new DependenteVO());
		propostaVazia.getBeneficiarios().getDependentes().add(new DependenteVO());
		return gerarRelatorio(propostaVazia, gerarViaOperadora, gerarViaCorretor, gerarBoleto, true);
	}
	/**
	 * Gera um arquivo de relat�rio para proposta. O arquivo de relat�rio cont�m 2 relat�rios:
	 * 	<ul>
	 * 		<li>3 vias da proposta para ser preenchida manualmente</li>
	 * 		<li>Boleto banc�rio para ser preenchido manualmente</li>
	 * 	</ul>
	 * 
	 * O nome do arquivo de relat�rio gerado ser� composto por:
	 * 	<ul>
	 * 		<li>BS-PROPOSTA- + c�digo da proposta formatado (Ex.: BS-PROPOSTA-BDA00000032932-3)</li>
	 * 	</ul>
	 * 
	 * Existem 2 layouts de relat�rio:
	 * 	<ul>
	 * 		<li>imprime_proposta.jrxml, para propostas que foram criadas com o plano antigo (29,90 + taxa de ades�o de 5,00)</li>
	 * 		<li>imprime_proposta_novo_termo.jrxml, para propostas que foram criadas com o plano novo (35,90 sem taxa de ades�o</li>
	 * 	</ul>
	 * 
	 * @param proposta Dados da proposta para gera��o
	 * @return Arquivo de relat�rio
	 */
	private ReportFileUtil gerarRelatorio(PropostaVO proposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto, boolean indicadorPropostaVazia) {
	
		proposta.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
		
		
		BoletoVO boleto = GeradorBoletoBancario.gerarBoletoProposta(proposta);
		boleto.setIndicadorPropostaVazia(indicadorPropostaVazia);
		
		ReportFileUtil reportFile = new ReportFileUtil();
		reportFile.setFileName("BS-PROPOSTA-" + proposta.getCodigo());
		
		// Cria os par�metros da proposta
		Map<Object, Object> parametros = preencherParametros(proposta, boleto);
		Map<Object, Object> paramsViaOperadora = new HashMap<Object, Object>();
		Map<Object, Object> paramsViaContratante = new HashMap<Object, Object>();
		Map<Object, Object> paramsViaCorretor = new HashMap<Object, Object>();

		
		String nomeArquivoPdf = "imprime_nova_proposta.jrxml";
		if(FormaPagamento.CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento())
				|| FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento())){
			nomeArquivoPdf = "imprime_nova_proposta_cartao_credito.jrxml";
			gerarBoleto = false;
		}
		parametros.put("TOTAL_PAGINAS", calcularTotalDePaginasDoPDF(gerarViaOperadora, gerarViaCorretor, gerarBoleto));
		if(gerarViaOperadora){
			paramsViaOperadora.putAll(parametros);
			paramsViaOperadora.put(VIA, "1�VIA: OPERADORA - CONTRATADA");
			//paramsViaOperadora.put("viaBoleto", "VIA: OPERADORA - CONTRATADA");
			paramsViaOperadora.put(EXIBIR_PONTILHADO, false);
			paramsViaOperadora.put(VIA_COM_PONTILHADO, false);
			if(gerarBoleto){
				paramsViaOperadora.put(CONTADOR_VIA, 0);//3
				paramsViaOperadora.put(CONTADOR_BOLETO, 3);//6
				paramsViaContratante.put(CONTADOR_VIA, 3);
				paramsViaContratante.put(CONTADOR_BOLETO, 6);
			}else{
				paramsViaOperadora.put(CONTADOR_VIA, 0);//2
				paramsViaOperadora.put(CONTADOR_BOLETO, 5);//5
				paramsViaContratante.put(CONTADOR_VIA, 2);
				paramsViaContratante.put(CONTADOR_BOLETO, 6);
				paramsViaCorretor.put(CONTADOR_VIA, 4);
				paramsViaCorretor.put(CONTADOR_BOLETO, 8);
			}			
			reportFile.adicionarRelatorio(new ReportUtil(nomeArquivoPdf, proposta, paramsViaOperadora));
//			if(gerarBoleto){
//				reportFile.adicionarRelatorio(new ReportUtil("boleto_proposta.jrxml", proposta, paramsViaOperadora));
//			}
		}
		
		paramsViaContratante.putAll(parametros);
		paramsViaContratante.put(VIA, "2� VIA: CONTRATANTE");
		paramsViaContratante.put(VIA_COM_PONTILHADO, false);
		//paramsViaContratante.put("viaBoleto", "VIA: CONTRATANTE");
		paramsViaContratante.put(EXIBIR_PONTILHADO, false);

				
		reportFile.adicionarRelatorio(new ReportUtil(nomeArquivoPdf, proposta, paramsViaContratante));
//		if(gerarBoleto){
//			reportFile.adicionarRelatorio(new ReportUtil("boleto_proposta.jrxml", proposta, paramsViaContratante));
//		}
		

		
		if(gerarViaCorretor){
			paramsViaCorretor.putAll(parametros);
			paramsViaCorretor.put(VIA, "3� VIA: CORRETOR");
			//paramsViaCorretor.put("viaBoleto", "VIA: BANCO");
			paramsViaCorretor.put(EXIBIR_PONTILHADO, true);
			paramsViaCorretor.put(VIA_COM_PONTILHADO, true);
			
			if(gerarBoleto){
				paramsViaCorretor.put(CONTADOR_VIA, 6);
				paramsViaCorretor.put(CONTADOR_BOLETO, 9);
			}
			
			reportFile.adicionarRelatorio(new ReportUtil(nomeArquivoPdf, proposta, paramsViaCorretor));
//			if(gerarBoleto){
//				reportFile.adicionarRelatorio(new ReportUtil("boleto_proposta.jrxml", proposta, paramsViaCorretor));
//			}
		}

		return reportFile;
	}


	private Integer calcularTotalDePaginasDoPDF(boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto) {
		
		Integer totalDePaginas = 2;
		
		if(gerarBoleto){
			totalDePaginas = totalDePaginas + 1;
		}
		
		if(gerarViaOperadora){
			totalDePaginas = totalDePaginas + 2;
			if(gerarBoleto){
				totalDePaginas = totalDePaginas + 1;
			}
		}
		if(gerarViaCorretor){
			totalDePaginas = totalDePaginas + 2;
			if(gerarBoleto){
				totalDePaginas = totalDePaginas + 1;
			}
		}
		
		return totalDePaginas;
	}


	/**
	 * Preenche os par�metros que ser�o usados no relat�rio da proposta:
	 * 	- Nome do corretor logado no sistema (Esse nome � usado no corretor da proposta, dever�amos pegar do banco e n�o do usu�rio logado)
	 * 	- Valor da taxa de ades�o, fixo em R$ 5,00 para propostas de antes 01/03/2013
	 * @param proposta Proposta
	 * @return Mapa de par�metros
	 */
	private Map<Object, Object> preencherParametros(PropostaVO proposta, BoletoVO boleto) {
		Map<Object, Object> parametros = Maps.newHashMap();
		
		parametros.put("CODIGO_PROPOSTA", proposta.getCodigoFormatado());
		parametros.put("IMG_BARCODE", proposta.getCodigo());
		parametros.put("IS_BOLETO", FormaPagamento.BOLETO.getCodigo().equals(proposta.getPagamento().getFormaPagamento()));	
		parametros.put("IS_DEBITO_AUTOMATICO", !FormaPagamento.BOLETO.equals(proposta.getPagamento().getFormaPagamento()));
		parametros.put("FORMA_PAGAMENTO", FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO.getNome());
		if(!FormaPagamento.BOLETO.equals(proposta.getPagamento().getFormaPagamento())){
			if(proposta.getPagamento().getFormaPagamento() != null){
				parametros.put("FORMA_PAGAMENTO", FormaPagamento.buscaPor(proposta.getPagamento().getFormaPagamento()).getNome());
			}else{
				parametros.put("FORMA_PAGAMENTO", null);
			}
		}		
		
		parametros.put("BOLETO_IMG_BARCODE", boleto.calcularCodigoBarra());
		if(boleto.getDataDeVencimento() != null && !boleto.isIndicadorPropostaVazia()){
			parametros.put("BOLETO_VENCIMENTO", boleto.getDataDeVencimentoPropostaFinalizada().toString("dd/MM/yyyy"));
		}
		if(proposta.getDataValidadeProposta() != null && !boleto.isIndicadorPropostaVazia()){
			parametros.put("VENCIMENTO", boleto.getDataDeVencimentoPropostaFinalizada().toString("dd/MM/yyyy"));
		}else if(proposta.getDataValidadeProposta() != null){
			parametros.put("VENCIMENTO", proposta.getDataValidadeProposta().toString("dd/MM/yyyy"));
		}
		parametros.put("BOLETO_LINHA_DIGITAVEL", boleto.calcularLinhaDigitavel());
		parametros.put("BOLETO_SACADO", boleto.getSacado());
		parametros.put("BOLETO_NOSSO_NUMERO", boleto.getNossoNumero());
		parametros.put("BOLETO_CODIGO_SUCURSAL", boleto.getCodigoSucursal().toString());
		parametros.put("BOLETO_CODIGO_CORRETOR", boleto.getCodigoCorretor().toString());
		if(BigDecimal.ZERO.compareTo(boleto.getValorBoleto()) == 0){
			parametros.put("BOLETO_VALOR_DOCUMENTO", null);
		}else{
			parametros.put("BOLETO_VALOR_DOCUMENTO", boleto.getValorBoleto());
		}
		//PROPONENTE
		if (proposta.getPagamento().getContaCorrente() != null && proposta.getPagamento().getContaCorrente().getCpf() != null) {
			parametros.put("nome_proponente", proposta.getPagamento().getContaCorrente().getNome());
			parametros.put("cpf_proponente", proposta.getPagamento().getContaCorrente().getCpf());
			parametros.put("email_proponente", Constantes.STRING_VAZIA);
			parametros.put("data_nascimento_proponente", proposta.getPagamento().getContaCorrente().getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
//			parametros.put("data_nascimento_proponente", proposta.getPagamento().getContaCorrente().getDataNascimento());
			if(proposta.getPagamento().getContaCorrente().getBanco() != null 
					&& proposta.getPagamento().getContaCorrente().getBanco().getCodigo() != null
					&& 0 != proposta.getPagamento().getContaCorrente().getBanco().getCodigo()){
				parametros.put("numero_banco", Banco.buscaBancoPor(proposta.getPagamento().getContaCorrente().getBanco().getCodigo()).getDescricao());
				parametros.put("0", proposta.getPagamento().getContaCorrente().getBanco().getCodigo().toString());
				parametros.put("nome_banco", Banco.buscaBancoPor(proposta.getPagamento().getContaCorrente().getBanco().getCodigo()).getNome());
				parametros.put("legenda_banco", Banco.buscaBancoPor(proposta.getPagamento().getContaCorrente().getBanco().getCodigo()).getDescricao());

			}
			if(null != proposta.getPagamento().getContaCorrente().getNumeroConta()){
				parametros.put("numero_conta_corrente_com_digito", proposta.getPagamento().getContaCorrente().getNumeroConta().toString() + "-" + proposta.getPagamento().getContaCorrente().getDigitoVerificadorConta());
			}
			if(null != proposta.getPagamento().getContaCorrente().getNumeroAgencia()){
				parametros.put("nome_agencia", Constantes.STRING_VAZIA);
			}
		}
		
		//TITULAR
		TitularVO titular = proposta.getBeneficiarios().getTitular();
		if(titular != null && titular.getCpf() != null){
			parametros.put("titular_nome", titular.getNome());
			parametros.put("titular_cpf", titular.getCpf());
			parametros.put("titular_estado_civil", titular.getEstadoCivil().getCodigo().toString());
			parametros.put("titular_data_nascimento", titular.getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
			parametros.put("titular_sexo", titular.getSexo());
			parametros.put("titular_nome_mae", titular.getNomeMae());
			parametros.put("titular_cns", titular.getCns());
			parametros.put("titular_dnv", titular.getDnv());
			parametros.put("emailBeneficiario", titular.getEmail());
			//parametros.put("telefoneCelularBeneficiario", );
			//parametros.put("telefoneResidencialBeneficiario", );
			parametros.put("titular_endereco", titular.getEndereco().getLogradouro());
			parametros.put("titular_numero", titular.getEndereco().getNumero().toString());
			parametros.put("titular_compl", titular.getEndereco().getComplemento());
			parametros.put("titular_bairro", titular.getEndereco().getBairro());
			parametros.put("titular_cidade",titular.getEndereco().getCidade());
			parametros.put("titular_uf", titular.getEndereco().getEstado());
			parametros.put("titular_cep", titular.getEndereco().getCep());
			if(titular.getGrauParentesco().getCodigo() != null){
				parametros.put("titular_parentesco_proponente", titular.getGrauParentesco().getCodigo().toString());
			}
			//parametros.put("titular_parentesco", titular.getGrauParentesco().getCodigo().toString());
			//parametros.put("titular_parentesco_proponente", titular.getGrauParentesco().getCodigo().toString());
		}
		parametros.put("dependentes", null);
		
		//RESPONSAVEL LEGAL
		RepresentanteLegalVO representanteLegalVO = proposta.getBeneficiarios().getRepresentanteLegal();
		if(representanteLegalVO != null && representanteLegalVO.getCpf() != null){
			parametros.put("responsavel_nome", representanteLegalVO.getNome());
			parametros.put("responsavel_cpf", representanteLegalVO.getCpf());
			parametros.put("responsavel_email", representanteLegalVO.getEmail());
			if(representanteLegalVO.getTelefones() != null && !representanteLegalVO.getTelefones().isEmpty()){
				parametros.put("responsavel_telefone", "(" + representanteLegalVO.getTelefones().get(0).getDdd() + ") " + representanteLegalVO.getTelefones().get(0).getNumero());
			}
			parametros.put("responsavel_nasc", representanteLegalVO.getDataNascimento().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
			parametros.put("responsavel_logradouro", representanteLegalVO.getEndereco().getLogradouro());
			parametros.put("responsavel_numero", representanteLegalVO.getEndereco().getNumero());
			parametros.put("responsavel_compl", representanteLegalVO.getEndereco().getComplemento());
			parametros.put("responsavel_bairro", representanteLegalVO.getEndereco().getBairro());
			parametros.put("responsavel_municipio", representanteLegalVO.getEndereco().getCidade());
			parametros.put("responsavel_uf", representanteLegalVO.getEndereco().getEstado());
			parametros.put("responsavel_cep", representanteLegalVO.getEndereco().getCep());
		}
		
		//PLANO
		if(proposta.getPlano() != null && proposta.getPlano().getCodigo() != null){
			parametros.put("nome_plano", proposta.getPlano().getNome() + Constantes.IFLE);
			parametros.put("periodo_carencia_mensal", proposta.getPlano().getDescricaoCarenciaPeriodoMensal());
			parametros.put("periodo_carencia_anual", proposta.getPlano().getDescricaoCarenciaPeriodoAnual());
			//valores mensal
			if(0.0 != proposta.getPlano().getValorPlanoVO().getValorMensalTitular()){
				parametros.put("valor_plano_tit_mensal", FormatacaoUtil.formataValor(proposta.getPlano().getValorPlanoVO().getValorMensalTitular()));
				parametros.put("valor_plano_dep_mensal", FormatacaoUtil.formataValor(proposta.getPlano().getValorPlanoVO().getValorMensalDependente()));
			}
			//valores anual
			if(0.0 != proposta.getPlano().getValorPlanoVO().getValorAnualTitular()){
				parametros.put("valor_plano_tit_anual", FormatacaoUtil.formataValor(proposta.getPlano().getValorPlanoVO().getValorAnualTitular()));
				parametros.put("valor_plano_dep_anual", FormatacaoUtil.formataValor(proposta.getPlano().getValorPlanoVO().getValorAnualDependente()));
			}
			//calculando o valor total de acordo com o tipo de cobran�a (mensal/anual)
			Integer quantidadeBeneficiarios = proposta.getBeneficiarios().getDependentes().size() + BENEFICIARIO_TITULAR;
			if(proposta.getPagamento().getCodigoTipoCobranca() != null){
				if( TipoCobranca.MENSAL.getCodigo() == proposta.getPagamento().getCodigoTipoCobranca()   ){
					parametros.put("valor_total_plano_mensal", FormatacaoUtil.formataValor(proposta.getPlano().getValorPlanoVO().getValorMensalTitular() * quantidadeBeneficiarios));
				}else if(TipoCobranca.ANUAL.getCodigo()  == proposta.getPagamento().getCodigoTipoCobranca()){
					parametros.put("valor_plano_total_anual", FormatacaoUtil.formataValor(proposta.getPlano().getValorPlanoVO().getValorAnualTitular() *  quantidadeBeneficiarios));
				}
			}
			//variaveis que mudam de acordo com o plano selecionado
			if (Constantes.CODIGO_PLANO_EXCLUSIVE.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_EXCLUSIVE);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_EXCLUSIVE);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_EXCLUSIVE);
			} else if (Constantes.CODIGO_PLANO_EXCLUSIVE_PLUS.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_EXCLUSIVE_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_EXCLUSIVE_PLUS);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_EXCLUSIVE_PLUS);
			} else if (Constantes.CODIGO_PLANO_PRIME.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_PRIME);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_PRIME);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_PRIME);
			} else if (Constantes.CODIGO_PLANO_PRIME_PLUS.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_PRIME_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_PRIME_PLUS);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_PRIME_PLUS);
			} else if (Constantes.CODIGO_PLANO_MAX.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_MAX);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_MAX);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_MAX);
			} else if (Constantes.CODIGO_PLANO_MAX_PLUS.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_MAX_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_MAX_PLUS);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_MAX_PLUS);
			}else if (Constantes.CODIGO_PLANO_DENTE_LEITE.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTE_LEITE);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTE_LEITE);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE);
			}else if (Constantes.CODIGO_PLANO_DENTE_LEITE_EXCLUSIVE.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTE_LEITE_EXCLUSIVE);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_EXCLUSIVE);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_EXCLUSIVE);
			}else if (Constantes.CODIGO_PLANO_DENTE_LEITE_MAX.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTE_LEITE_MAX);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTE_LEITE_MAX);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX);
			} else if (Constantes.CODIGO_PLANO_DENTE_LEITE_MAX_PLUS.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTE_LEITE_MAX_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTE_LEITE_MAX_PLUS);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX_PLUS);
			}else if (Constantes.CODIGO_PLANO_DENTE_LEITE_PRIME.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTE_LEITE_PRIME);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTE_LEITE_PRIME);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_PRIME);
			} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTAL_JUNIOR);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL_JUNIOR);
			} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTAL_JUNIOR_MAX);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR_MAX);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_JUNIOR_MAX);
			} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_EXCLUSIVE.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTAL_JUNIOR_EXCLUSIVE);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR_EXCLUSIVE);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_JUNIOR_EXCLUSIVE);
			}else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX_PLUS.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_DENTAL_JUNIOR_MAX_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR_MAX_PLUS);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_JUNIOR_MAX_PLUS);
			}else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_PRIME.equals(proposta.getPlano().getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_PRIME_JUNIOR);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR_PRIME);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL_JUNIOR_PRIME);
			
			}else if(Constantes.CODIGO_PLANO_MULTI.equals(proposta.getPlano().getCodigo())){
				parametros.put(TIPO_PLANO, Constantes.PLANO_MULTI);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_MULTI);
				parametros.put(TERMO_CONTRATO,Constantes.TERMO_CONTRATO_MULTI);
			
			}else if(Constantes.CODIGO_PLANO_MULTI_PLUS.equals(proposta.getPlano().getCodigo())){
				parametros.put(TIPO_PLANO, Constantes.PLANO_MULTI_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_DENTAL_MULTI_PLUS);
				parametros.put(TERMO_CONTRATO,Constantes.TERMO_CONTRATO_MULTI_PLUS);			
			
			}else {
				parametros.put(TIPO_PLANO, Constantes.STRING_VAZIA);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(REGISTRO_ANS, Constantes.NUMERO_ANS_PLANO_4);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL);
			}
		}
		
		//CORRETOR
		if(proposta.getCorretor() != null && !Strings.isNullOrEmpty(proposta.getCorretor().getCpfCnpj())){
//			if(null != proposta.getPagamento().getAgenciaProdutora() && null != proposta.getAgenciaProdutora().getCodigo()){
//				parametros.put("codigo_agencia_produtora", proposta.getAgenciaProdutora().getCodigo().longValue());
//			}
			if(null != proposta.getCorretor().getAgenciaProdutora()){
				parametros.put("codigo_agencia_produtora", proposta.getCorretor().getAgenciaProdutora());
			}else{	
				parametros.put("codigo_agencia_produtora", null);
			}
			parametros.put("sucursal_cpd", proposta.getCorretor().getSucursal().toString());
			if(proposta.getAngariador() != null){
				parametros.put("cpd_angariador", proposta.getAngariador().getCpd());
				parametros.put("nome_angariador",  proposta.getAngariador().getNome());
			}
			parametros.put("cpd_corretor", proposta.getCorretor().getCpd().toString());
			parametros.put("nome_corretor", proposta.getCorretor().getNome());
			if(proposta.getCorretorMaster() != null && proposta.getCorretorMaster().getCpd() != null){
				parametros.put("cpd_corretor_master", proposta.getCorretorMaster().getCpd().intValue());
				parametros.put("nome_corretor_master", proposta.getCorretorMaster().getNome());
			}
			parametros.put("codigoMatriculaAssistente", proposta.getCodigoMatriculaAssistente());
			parametros.put("gerente_produto_bvp", proposta.getCodigoMatriculaGerente());
			if(proposta.getCodigoPostoAtendimento() != null){
				parametros.put("codigo_posto_atendimento", proposta.getCodigoPostoAtendimento().toString());
			}
		}
		//FORMA DE PAGAMENTO
		TipoSucursalSeguradora tipoSucursalSeguradora = TipoSucursalSeguradora.MERCADO;
		if(sucursalCorretorService.validarSucursalMercado(proposta.getCorretor().getSucursal())){
			tipoSucursalSeguradora = TipoSucursalSeguradora.MERCADO;
		}else if(sucursalCorretorService.validarSucursalCorporate(proposta.getCorretor().getSucursal())){
			tipoSucursalSeguradora = TipoSucursalSeguradora.CORPORATE;
		}else if(sucursalCorretorService.validarSucursalRede(proposta.getCorretor().getSucursal())){
			tipoSucursalSeguradora = TipoSucursalSeguradora.REDE;
		}
		
		parametros.put("EXIBIR_FORMA_PAGAMENTO_BOLETO", (TipoSucursalSeguradora.CORPORATE.equals(tipoSucursalSeguradora)));

		if(!proposta.getPlano().getCodigo().equals(3L) &&  TipoSucursalSeguradora.CORPORATE.equals(tipoSucursalSeguradora)){
			parametros.put("EXIBIR_FORMA_PAGAMENTO_BOLETO", false);
		}
		if (null == proposta.getPagamento().getFormaPagamento()) {
			parametros.put("FORMA_PAGAMENTO_SELECIONADA", "0");
		} else {
			parametros.put("FORMA_PAGAMENTO_SELECIONADA", proposta.getPagamento().getFormaPagamento().toString());
		}

		boolean corretorCartaoCreditoHabilitado = odontoprevServiceFacade.verificarCorretorHabilitado(proposta.getCorretor().getCpfCnpj());
		parametros.put("EXIBIR_FORMA_PAGAMENTO_CARTAO_CREDITO", corretorCartaoCreditoHabilitado);
		parametros.put("EXIBIR_FORMA_PAGAMENTO_DEBITO_AUTOMATICO_E_DEMAIS_CARTAO", corretorCartaoCreditoHabilitado);
		parametros.put("EXIBIR_FORMA_PAGAMENTO_BOLETO_CARTAO_CREDITO", corretorCartaoCreditoHabilitado);

		if(tipoSucursalSeguradora == TipoSucursalSeguradora.REDE){
			parametros.put("legenda_banco", "237 - Bradesco");
		}else{
			parametros.put("legenda_banco", "033 - Santander 237 - Bradesco 341 - Ita�");
		}
	    parametros.put("data_emissao_proposta", proposta.getDataEmissao().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
	    parametros.put("data_validade_proposta", proposta.getDataValidadeProposta().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		return parametros;
	}


	/**
	 * Gerar o pdf com o termo de ades�o.
	 * 
	 * @param codigoPlano - c�digo de plano.
	 * 
	 * @return ReportFile - Termo de Ades�o em PDF.
	 */
	public ReportFileUtil gerarTermoDeAdesao(Long codigoProposta) {
		
		ReportFileUtil reportFile = new ReportFileUtil();
		
		PropostaVO proposta = propostaServiceFacade.obterPropostaPorSequencial(codigoProposta);
		
		PlanoVO planoVO = planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo());
		
		if(planoVO != null){
			reportFile.setFileName("TERMO DE ADES�O");
			
			Map<Object, Object> parametros = Maps.newHashMap();
			
			//variaveis que mudam de acordo com o plano selecionado
			if (Constantes.CODIGO_PLANO_EXCLUSIVE_PLUS.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_EXCLUSIVE_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_EXCLUSIVE_PLUS);
				
			} else if(Constantes.CODIGO_PLANO_EXCLUSIVE.equals(planoVO.getCodigo())) {
					parametros.put(TIPO_PLANO, Constantes.PLANO_EXCLUSIVE);
					parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
					parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_EXCLUSIVE);
			} else if (Constantes.CODIGO_PLANO_PRIME.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_PRIME);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_PRIME);
			} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE);
			} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_EXCLUSIVE.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR_EXCLUSIVE);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL_JUNIOR_PRIME);
			} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_JUNIOR_MAX);
			}else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX_PLUS.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_JUNIOR_MAX_PLUS);
			}else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_PRIME.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR_PRIME);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL_JUNIOR_PRIME);
			}else if (Constantes.CODIGO_PLANO_DENTE_LEITE.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTE_LEITE);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE);
			}else if (Constantes.CODIGO_PLANO_DENTE_LEITE_MAX.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTE_LEITE_MAX);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX);
			}else if (Constantes.CODIGO_PLANO_DENTE_LEITE_MAX_PLUS.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTE_LEITE_MAX_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX_PLUS);
			}else if (Constantes.CODIGO_PLANO_DENTE_LEITE_PRIME.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTE_LEITE_PRIME);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_PRIME);
			}	
			else if (Constantes.CODIGO_PLANO_PRIME_PLUS.equals(planoVO.getCodigo())) {
				parametros.put(TIPO_PLANO, Constantes.PLANO_PRIME_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_PRIME_PLUS);
			
			}else if(Constantes.CODIGO_PLANO_MULTI.equals(planoVO.getCodigo())){
				parametros.put(TIPO_PLANO, Constantes.PLANO_MULTI);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO,Constantes.TERMO_CONTRATO_MULTI);
			
			}else if(Constantes.CODIGO_PLANO_MULTI_PLUS.equals(planoVO.getCodigo())){
				parametros.put(TIPO_PLANO, Constantes.PLANO_MULTI_PLUS);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO,Constantes.TERMO_CONTRATO_MULTI_PLUS);	
				
			} else {
				parametros.put(TIPO_PLANO, Constantes.STRING_VAZIA);
				parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL);
			}
			
			reportFile.adicionarRelatorio(new ReportUtil("termo_adesao.jrxml", null, parametros));
		}
		
		
		return reportFile;
	}
	
	/**
	 * Metodo responsavel por gerar boleto referente a uma proposta.
	 * 
	 * @param codigoProposta - codigo da proposta.
	 * 
	 * @return ReportFile - PDF com o boleto.
	 */
	public ReportFileUtil gerarBoleto(Long codigoProposta){
		
		PropostaVO proposta = propostaServiceFacade.obterPropostaPorSequencial(codigoProposta);
		
		ReportFileUtil reportFile = new ReportFileUtil();
		reportFile.setFileName("BOLETO-PROPOSTA-" + proposta.getCodigo());
		
		proposta.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
		
		BoletoVO boleto = GeradorBoletoBancario.gerarBoletoProposta(proposta);
		
		Map<Object, Object> parametros = Maps.newHashMap();
		
		parametros.put("CODIGO_PROPOSTA", proposta.getCodigo());
		parametros.put("BOLETO_CODIGO_PROPOSTA", proposta.getCodigo());
		parametros.put("BOLETO_IMG_BARCODE", boleto.calcularCodigoBarra());
		parametros.put("BOLETO_fMENTO", boleto.getDataDeVencimento().toString("dd/MM/yyyy"));
		if(proposta.getDataValidadeProposta() != null){
			parametros.put("VENCIMENTO", proposta.getDataValidadeProposta().toString("dd/MM/yyyy"));
		}
		parametros.put("BOLETO_LINHA_DIGITAVEL", boleto.calcularLinhaDigitavel());
		parametros.put("BOLETO_SACADO", boleto.getSacado());
		parametros.put("BOLETO_NOSSO_NUMERO", boleto.getNossoNumero());
		parametros.put("BOLETO_CODIGO_SUCURSAL", boleto.getCodigoSucursal().toString());
		parametros.put("BOLETO_CODIGO_CORRETOR", boleto.getCodigoCorretor().toString());
		if(BigDecimal.ZERO.compareTo(boleto.getValorBoleto()) == 0){
			parametros.put("BOLETO_VALOR_DOCUMENTO", null);
		}else{
			parametros.put("BOLETO_VALOR_DOCUMENTO", boleto.getValorBoleto());
		}
		if (String.valueOf(Constantes.CODIGO_PLANO_EXCLUSIVE).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.PLANO_EXCLUSIVE);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_EXCLUSIVE);
		} else if (String.valueOf(Constantes.CODIGO_PLANO_EXCLUSIVE_PLUS).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.PLANO_EXCLUSIVE_PLUS);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_EXCLUSIVE_PLUS);
		} else if (String.valueOf(Constantes.CODIGO_PLANO_PRIME).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.PLANO_PRIME);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_PRIME);
		} else if (String.valueOf(Constantes.CODIGO_PLANO_DENTAL_JUNIOR).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE);
		} else if (String.valueOf(Constantes.CODIGO_PLANO_DENTAL_JUNIOR_EXCLUSIVE).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR_EXCLUSIVE);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL_JUNIOR_PRIME);
		} else if (String.valueOf(Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_JUNIOR_MAX);
		}else if (String.valueOf(Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX_PLUS).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX_PLUS);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_JUNIOR_MAX_PLUS);
		}else if (String.valueOf(Constantes.CODIGO_PLANO_DENTAL_JUNIOR_PRIME).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTAL_JUNIOR_PRIME);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL_JUNIOR_PRIME);
		}else if (String.valueOf(Constantes.CODIGO_PLANO_DENTE_LEITE).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTE_LEITE);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE);
		}else if (String.valueOf(Constantes.CODIGO_PLANO_DENTE_LEITE_MAX).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTE_LEITE_MAX);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX);
		}else if (String.valueOf(Constantes.CODIGO_PLANO_DENTE_LEITE_MAX_PLUS).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTE_LEITE_MAX_PLUS);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX_PLUS);
		}else if (String.valueOf(Constantes.CODIGO_PLANO_DENTE_LEITE_PRIME).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.CODIGO_PLANO_DENTE_LEITE_PRIME);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTE_LEITE_PRIME);
		}	
		else if (String.valueOf(Constantes.CODIGO_PLANO_PRIME_PLUS).equals(proposta.getCodigo())) {
			parametros.put(TIPO_PLANO, Constantes.PLANO_PRIME_PLUS);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_PRIME_PLUS);
		}else if(String.valueOf(Constantes.CODIGO_PLANO_MULTI).equals(proposta.getCodigo())){
			parametros.put(TIPO_PLANO, Constantes.PLANO_MULTI);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO,Constantes.TERMO_CONTRATO_MULTI);
		
		}else if(String.valueOf(Constantes.CODIGO_PLANO_MULTI_PLUS).equals(proposta.getCodigo())){
			parametros.put(TIPO_PLANO, Constantes.PLANO_MULTI_PLUS);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO,Constantes.TERMO_CONTRATO_MULTI_PLUS);
			
		} else {
			parametros.put(TIPO_PLANO, Constantes.STRING_VAZIA);
			parametros.put(TIPO_PLANO_LOGO, Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put(TERMO_CONTRATO, Constantes.TERMO_CONTRATO_DENTAL);
		}
				
		reportFile.adicionarRelatorio(new ReportUtil("boleto.jrxml", proposta, parametros));
		
		return reportFile;
	}

}
