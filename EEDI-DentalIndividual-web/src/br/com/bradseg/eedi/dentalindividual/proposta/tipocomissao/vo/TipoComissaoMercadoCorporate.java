package br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.vo;

import br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.util.ConstantesTipoComissao;
import br.com.bradseg.eedi.dentalindividual.vo.TipoCobranca;

public class TipoComissaoMercadoCorporate implements TipoComissao {

	@Override
	public Integer obterTipoComissao(Integer tipoCobranca, boolean corretorNovaComissao) {

		Integer tipoComissao = null;
		if (TipoCobranca.MENSAL.getCodigo() == tipoCobranca) {
			if(corretorNovaComissao){
				tipoComissao = ConstantesTipoComissao.COMISSAO_MENSAL_MERCADO_CORPORATE_AZEVEDODO_LALUPA;
			}else{
				tipoComissao = ConstantesTipoComissao.COMISSAO_MENSAL_MERCADO_CORPORATE;
			}
			
		} else if (TipoCobranca.ANUAL.getCodigo() == tipoCobranca) {
			tipoComissao = ConstantesTipoComissao.COMISSAO_ANUAL_MERCADO_CORPORATE;
		}
		return tipoComissao;
	}

}