package br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.facade;

import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;

public interface TipoComissaoServiceFacade {

	public Integer obterTipoComissao(PropostaVO proposta);
}
