package br.com.bradseg.eedi.dentalindividual.proposta.facade;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.eedi.dentalindividual.exception.EEDIBusinessException;
import br.com.bradseg.eedi.dentalindividual.odontoprev.facade.OdontoprevServiceFacade;
import br.com.bradseg.eedi.dentalindividual.odontoprev.vo.RetornoVO;
import br.com.bradseg.eedi.dentalindividual.plano.facade.PlanoServiceFacade;
import br.com.bradseg.eedi.dentalindividual.proposta.converter.PropostaConverter;
import br.com.bradseg.eedi.dentalindividual.proposta.presentation.model.QueryModel;
//import br.com.bradseg.eedi.dentalindividual.queryIniciar.QueryPage;
import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.util.ConstantesMensagemErro;
import br.com.bradseg.eedi.dentalindividual.validador.ListaPropostaValidator;
import br.com.bradseg.eedi.dentalindividual.validador.NovaPropostaValidator;
import br.com.bradseg.eedi.dentalindividual.vo.CorretorVO;
import br.com.bradseg.eedi.dentalindividual.vo.DependenteVO;
import br.com.bradseg.eedi.dentalindividual.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.BusinessException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.IntegrationException_Exception;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Proposta100PorCentoCorretorWebService;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SucursalSeguradoraVO;

/**
 * Classe de neg�cio responsavel por disponibilizar os metodos referentes a proposta.
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class PropostaServiceFacadeImpl implements PropostaServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaServiceFacadeImpl.class);

	@Autowired
	private NovaPropostaValidator validadorNovaProposta;

	@Autowired
	private ListaPropostaValidator validadorListaProposta;

	@Autowired
	private PropostaConverter converterProposta;

	@Autowired
	private Proposta100PorCentoCorretorWebService propostaWebService;
	@Autowired
	private GeradorPropostaFisicaServiceFacade geradorPropostaFisicaServiceFacade;
	
	

	
	@Autowired
	private transient PlanoServiceFacade planoServiceFacade;

	@Autowired
	private OdontoprevServiceFacade odontoprevServiceFacade;

	private static final int RETONO_MIP_SUCCESSO = 1;

	@Transactional(propagation = Propagation.REQUIRED)
	public Long imprimirPropostaVazia(PropostaVO proposta) {

		try {
			validadorNovaProposta.validatePropostaVazia(proposta);
			return propostaWebService.salvarPropostaVazia(converterProposta.converterPropostaAplicacaoParaPropostaServico(proposta));
		} catch (BusinessException_Exception e) {
			LOGGER.error("Erro de neg�cio ao salvar proposta vazia atrav�s do canal 100%CORRETOR.");
			throw new EEDIBusinessException("", e.getFaultInfo());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao finalizar uma proposta vazia atrav�s do canal 100%CORRETOR.");
			throw new IntegrationException(e.getMessage());
		} catch (BusinessException e) {
			LOGGER.error("Erro ao finalizar uma proposta vazia atrav�s do canal 100%CORRETOR.");
			throw new BusinessException(e.getMessages());
		} catch (Exception e) {
			LOGGER.error("Erro ao finalizar uma proposta vazia atrav�s do canal 100%CORRETOR.");
			throw new IntegrationException(e.getMessage());
		}
	}
	

	
	
	public byte[] gerarPDFPropostaVazia(PropostaVO proposta) {

		byte[] relatorio = null;
		//		proposta.setDataValidadeProposta(null);
		try {
			relatorio = geradorPropostaFisicaServiceFacade.gerarPDFPropostaVazia(proposta.getSequencial(), true, true, true);
		} catch (Exception e) {
			LOGGER.error("Erro ao gerar PDF proposta vazia: " + e.getMessage());
		}
		return relatorio;
	}

	public byte[] gerarPDFProposta(PropostaVO proposta) {

		byte[] relatorio = null;
		//			relatorio = propostaWebService.gerarPDFProposta(proposta.getSequencial(), true, true, true);
		relatorio = geradorPropostaFisicaServiceFacade.gerarPDFProposta(proposta.getSequencial(), true, true, true);

		return relatorio;
	}

	/**
	 * Metodo responsavel por salvar rascunho de uma proposta.
	 * 
	 * @param proposta - informa��es da proposta a serem gravadas.
	 * @return Long - sequencial da proposta gerado.
	 * @throws BusinessException_Exception
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public Long salvarRascunhoProposta(PropostaVO proposta) {

		validadorNovaProposta.validate(proposta);

		try {
			return propostaWebService.salvarRascunho(converterProposta.converterPropostaAplicacaoParaPropostaServico(proposta));
		} catch (BusinessException_Exception e) {
			LOGGER.error(Constantes.ERRO_DE_NEGOCIO_AO_SALVAR_RASCUNHO_DE_UMA_PROPOSTA_ATRAVES_DO_CANAL_100_CORRETOR);

			List<Message> listaErros = new ArrayList<Message>();
			for (br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.Message mensagemErro : e.getFaultInfo().getMessages()) {
				Message erro = new Message(mensagemErro.getMessage());
				listaErros.add(erro);
			}
			throw new EEDIBusinessException(listaErros);
		} catch (IntegrationException_Exception e) {
			LOGGER.error(Constantes.ERRO_DE_INTEGRACAO_AO_SALVAR_RASCUNHO_DE_UMA_PROPOSTA_ATRAVES_DO_CANAL_100_CORRETOR);
			throw new IntegrationException(e.getMessage());
		}
	}

	/**
	 * Metodo responsavel por finalizar uma proposta. - Validar os dados da proposta conforme as regras de neg�cio; -
	 * Converter o objeto (proposta) aplica��o para o objeto (proposta) servi�o; - Chamar o servi�o para finalizar uma
	 * proposta;
	 * 
	 * @param proposta - informa��es da proposta a serem gravadas.
	 * @return Long - sequencial da proposta gerado.
	 */
	public Long finalizarProposta(PropostaVO proposta) {

		validadorNovaProposta.validate(proposta);

		proposta.setDataInicioCobranca(obterDataInicioCobranca(proposta));
		
		try {
			return propostaWebService.finalizar(converterProposta.converterPropostaAplicacaoParaPropostaServico(proposta));
		} catch (BusinessException_Exception e) {
			LOGGER.error(Constantes.ERRO_DE_NEGOCIO_AO_FINALIZAR_UMA_PROPOSTA_ATRAVES_DO_CANAL_100_CORRETOR + e.getMessage());
			throw new EEDIBusinessException(Constantes.ERRO_DE_NEGOCIO_AO_FINALIZAR_UMA_PROPOSTA_ATRAVES_DO_CANAL_100_CORRETOR, e.getFaultInfo());
		} catch (IntegrationException_Exception e) {
			LOGGER.error(Constantes.ERRO_DE_INTEGRACAO_AO_FINALIZAR_UMA_PROPOSTA_ATRAVES_DO_CANAL_100_CORRETOR + e.getMessage());
			throw new IntegrationException(Constantes.ERRO_DE_NEGOCIO_AO_FINALIZAR_UMA_PROPOSTA_ATRAVES_DO_CANAL_100_CORRETOR + e.getMessage());
		}
	}

	protected boolean propostaComCartaoDeCreditoEmRascunho(PropostaVO proposta) {

		if (proposta.getStatus() == null || proposta.getStatus().contains(SituacaoProposta.RASCUNHO.name()) && (FormaPagamento.CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento()) || FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento()))) {
			return true;
		}
		return false;
	}

	private LocalDate obterDataInicioCobranca(PropostaVO proposta) {
		LocalDate dataInicioCobranca = null;
		if (br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento())) {
			dataInicioCobranca = new LocalDate().plusDays(7);
		} else {
			dataInicioCobranca = new LocalDate();
		}
		return dataInicioCobranca;
	}

	/**
	 * Metodo responsavel por adicionar um dependente na proposta.
	 * 
	 * @param proposta - informa��es da proposta.
	 */
	public void adicionarDependente(PropostaVO proposta) {
		if (proposta.getBeneficiarios().getDependentes().size() < Constantes.NUMERO_MAXIMO_DEPENDENTES) {
			proposta.getBeneficiarios().getDependentes().add(new DependenteVO());
		} else {
			throw new BusinessException(new Message(ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DEPENDENTES_NUMERO_MAXIMO));
		}
	}

	/**
	 * Metodo responsavel por consultar as propostas de acordo com o filtro informado.
	 * 
	 * @param filtro - informa��es a serem levadas em considera��o no momento da consulta.
	 * @param corretor - informa��es do corretor.
	 * @return List<PropostaVO> - lista com as propostas encontradas.
	 */
	public List<PropostaVO> consultarPropostaPorFiltro(FiltroPropostaVO filtro, CorretorVO corretor) {

		LoginVO login = new LoginVO();
		login.setId(corretor.getCpfCnpj());

		//		validadorListaProposta.validate(filtro);

		try {
			br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FiltroPropostaVO filtroServico = new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.FiltroPropostaVO();
			if (StringUtils.isNotEmpty(filtro.getCodigoProposta())) {
				filtroServico.setCodigoProposta(filtro.getCodigoProposta().toUpperCase().trim());
			}
			if (StringUtils.isNotEmpty(filtro.getNomeTitular())) {
				filtroServico.setNomeProponente(filtro.getNomeTitular());
			}
			if (filtro.getFiltroPeriodoVO().getStatus() != null && filtro.getFiltroPeriodoVO().getStatus() > 0) {

				if (SituacaoProposta.RASCUNHO.getCodigo().equals(filtro.getFiltroPeriodoVO().getStatus())) {
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.RASCUNHO);
				} else if (SituacaoProposta.PENDENTE.getCodigo().equals(filtro.getFiltroPeriodoVO().getStatus())) {
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.PENDENTE);
				} else if (SituacaoProposta.CRITICADA.getCodigo().equals(filtro.getFiltroPeriodoVO().getStatus())) {
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.CRITICADA);
				}
				if (SituacaoProposta.EM_PROCESSAMENTO.getCodigo().equals(filtro.getFiltroPeriodoVO().getStatus())) {
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.EM_PROCESSAMENTO);
				}
				if (SituacaoProposta.PRE_CANCELADA.getCodigo().equals(filtro.getFiltroPeriodoVO().getStatus())) {
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.PRE_CANCELADA);
				}
				if (SituacaoProposta.CANCELADA.getCodigo().equals(filtro.getFiltroPeriodoVO().getStatus())) {
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.CANCELADA);
				}
				if(SituacaoProposta.IMPLANTADA.getCodigo().equals(filtro.getFiltroPeriodoVO().getStatus())){
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.IMPLANTADA);
				}
				if (SituacaoProposta.TODAS.getCodigo().equals(filtro.getFiltroPeriodoVO().getStatus())) {
					filtroServico.setSituacaoProposta(br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.SituacaoProposta.TODAS);
				}
			}
				
			if ((filtro.getCpdCorretor() != null && filtro.getCpdCorretor() > 0) || (filtro.getSucursalCorretor() != null && filtro.getSucursalCorretor() > 0)) {
				filtroServico.setCorretor(new br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CorretorVO());

				if (filtro.getCpdCorretor() != null && filtro.getCpdCorretor() > 0) {
					filtroServico.getCorretor().setCpd(filtro.getCpdCorretor());
				}
				if (filtro.getSucursalCorretor() != null && filtro.getSucursalCorretor() > 0) {
					filtroServico.getCorretor().setSucursalSeguradora(new SucursalSeguradoraVO());
					filtroServico.getCorretor().getSucursalSeguradora().setCodigo(filtro.getSucursalCorretor());
				}

			}
			if (filtro.getFiltroPeriodoVO().getDataInicio() != null && filtro.getFiltroPeriodoVO().getDataFim() != null) {
				filtroServico.setDataInicio(filtro.getFiltroPeriodoVO().getDataInicio().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
				filtroServico.setDataFim(filtro.getFiltroPeriodoVO().getDataFim().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));

			}
			if (filtro.isIndicadorIntranet() == true) {
				return converterProposta.converterListaPropostaServicoParaListaPropostaAplicacao(propostaWebService.listarPropostasIntranetPorFiltro(filtroServico, login));
			} else {
				return converterProposta.converterListaPropostaServicoParaListaPropostaAplicacao(propostaWebService.listarPorFiltro(filtroServico, login));
			}

		} catch (BusinessException_Exception e) {
			LOGGER.error(Constantes.ERRO_NEGOCIO_CONSULTAR_PROPOSTA_CORRETOR);

			LOGGER.error("Mensagem: " + e.getMessage());
			LOGGER.error("Mensagem FaultInfo: " + e.getFaultInfo().getMessage());
			LOGGER.error("Objeto: " + e.toString());
			//e.getFaultInfo().getMessages()
			throw new EEDIBusinessException(Constantes.ERRO_NEGOCIO_CONSULTAR_PROPOSTA_CORRETOR, e.getFaultInfo().getMessages());
		} catch (IntegrationException_Exception e) {
			LOGGER.error(Constantes.ERRO_INTEGRACAO_CONSULTAR_PROPOSTA_CORRETOR);
			LOGGER.error("Mensagem: " + e.getMessage());
			LOGGER.error("Mensagem FaultInfo: " + e.getFaultInfo().getMessage());
			throw new IntegrationException(Constantes.ERRO_INTEGRACAO_CONSULTAR_PROPOSTA_CORRETOR + e.getMessage());
		}
	}

	/**
	 * Metodo responsavel por obter uma proposta atraves do sequencial da proposta.
	 * 
	 * @param sequencial - sequencial da proposta.
	 * @return PropostaVO - informa��es da proposta encontrada.
	 * @throws BusinessException_Exception
	 */
	public PropostaVO obterPropostaPorSequencial(Long sequencial) {

		try {
			return converterProposta.converterPropostaServicoParaPropostaAplicacao(propostaWebService.obterPropostaPorCodigo(sequencial));
		} catch (BusinessException_Exception e) {
			LOGGER.error(Constantes.ERRO_NEGOCIO_CONSULTAR_PROPOSTA_CORRETOR);
//			throw new EEDIBusinessException("", e.getFaultInfo());
		} catch (IntegrationException_Exception e) {
			LOGGER.error(Constantes.ERRO_INTEGRACAO_CONSULTAR_PROPOSTA_CORRETOR);
//			throw new IntegrationException(e.getMessage());
		}
		return null;
	}

	public br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO obterPropostaDoServicoPorSeguencial(Long sequencial) {
		try {
			return propostaWebService.obterPropostaPorCodigo(sequencial);
		} catch (BusinessException_Exception e) {
			LOGGER.error(Constantes.ERRO_NEGOCIO_CONSULTAR_PROPOSTA_CORRETOR);
			throw new EEDIBusinessException("", e.getFaultInfo());
		} catch (IntegrationException_Exception e) {
			LOGGER.error(Constantes.ERRO_INTEGRACAO_CONSULTAR_PROPOSTA_CORRETOR);
			throw new IntegrationException(e.getMessage());
		}
	}

	/**
	 * Metodo responsavel por obter uma proposta atraves do codigo da
	 * proposta.
	 * 
	 * @param codigoProposta
	 *            - codigo da proposta.
	 * @return PropostaVO - informa��es da proposta encontrada.
	 */
	public PropostaVO obterPropostaPorCodigo(String codigoProposta) {

		try {
			return converterProposta.converterPropostaServicoParaPropostaAplicacao(propostaWebService.obterPorCodigo(codigoProposta));
		} catch (BusinessException_Exception e) {
			LOGGER.error(Constantes.ERRO_NEGOCIO_CONSULTAR_PROPOSTA_CORRETOR);
			throw new EEDIBusinessException("", e.getFaultInfo());
		} catch (IntegrationException_Exception e) {
			LOGGER.error(Constantes.ERRO_INTEGRACAO_CONSULTAR_PROPOSTA_CORRETOR);
			throw new IntegrationException(e.getMessage());
		}
	}

	/**
	 * Metodo responsavel por cancelar a proposta atraves do sequencial da proposta.
	 * 
	 * @param sequencialProposta - sequencial da proposta a ser cancelado.
	 */
	public void cancelarProposta(Long sequencialProposta) {
		try {
			propostaWebService.cancelar(sequencialProposta);
		} catch (BusinessException_Exception e) {
			LOGGER.error(Constantes.ERRO_DE_NEGOCIO_AO_CANCELAR_A_PROPOSTA_ATRAVES_DO_CANAL_100_CORRETOR);
			throw new EEDIBusinessException("", e.getFaultInfo());
		} catch (IntegrationException_Exception e) {
			LOGGER.error(Constantes.ERRO_DE_INTEGRACAO_AO_CANCELAR_A_PROPOSTA_ATRAVES_DO_CANAL_100_CORRETOR);
			throw new IntegrationException(e.getMessage());
		}
	}

	public List<String> consultarProposta(String textoConsulta) {
		List<String> resultadoConsulta = null;
		try {
			resultadoConsulta = propostaWebService.consultarProposta(textoConsulta);
		} catch (BusinessException_Exception e) {
			LOGGER.error("INFO: Erro ao consultar: " + e.getMessage());
		} catch (IntegrationException_Exception e) {
			LOGGER.error("INFO: Erro ao consultar: " + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("INFO: Erro ao consultar: " + e.getMessage());
		}
		return resultadoConsulta;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public boolean validarDadosCartaoCredito(PropostaVO proposta) {
		boolean envioMIPComSucesso = false;
		RetornoVO retornoMIP = null;
		PropostaVO propostaParaTratamentoCartaoCredito = obterPropostaPorSequencial(proposta.getSequencial());
		propostaParaTratamentoCartaoCredito.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
		propostaParaTratamentoCartaoCredito.getPagamento().setCartaoCredito(proposta.getPagamento().getCartaoCredito());

		propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setCpfCartao(proposta.getBeneficiarios().getTitular().getCpf());
		propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setCpfCartao(proposta.getPagamento().getContaCorrente().getCpf());
		if (br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento())) {
			propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setCpfCartao(proposta.getBeneficiarios().getTitular().getCpf());
			retornoMIP = odontoprevServiceFacade.debitarPrimeiraParcelaCartaoDeCredito(propostaParaTratamentoCartaoCredito);
			
		} else if (br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO_E_CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento()) || br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento())) {
			if (br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.BOLETO_E_CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento())) {
				propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setCpfCartao(proposta.getBeneficiarios().getTitular().getCpf());
			} else if (br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento())) {
				propostaParaTratamentoCartaoCredito.getPagamento().getCartaoCredito().setCpfCartao(proposta.getBeneficiarios().getTitular().getCpf());
			}
			retornoMIP = odontoprevServiceFacade.validarDadosCartaoCredito(propostaParaTratamentoCartaoCredito);
		}
		if (retornoMIP != null && retornoMIP.getSucesso() == RETONO_MIP_SUCCESSO) {
			envioMIPComSucesso = true;
		}

		return envioMIPComSucesso;
	}

/*	@Override
	public QueryModel query(String sql) {
		return propostaDao.query(sql);
	}*/

}
