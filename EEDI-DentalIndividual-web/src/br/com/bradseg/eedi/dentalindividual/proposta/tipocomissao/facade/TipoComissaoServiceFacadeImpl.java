package br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.vo.TipoComissaoMercadoCorporate;
import br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.vo.TipoComissaoRedeCorretorMaster;
import br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.vo.TipoComissaoRedeGerenteProduto;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.TipoSucursalSeguradora;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TipoComissaoServiceFacadeImpl implements TipoComissaoServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(TipoComissaoServiceFacadeImpl.class);

	@Override
	public Integer obterTipoComissao(PropostaVO proposta) {
		Integer tipoComissao = null;
		boolean corretorNovaComissao = false;
		
		if(proposta.getCorretor().getCpfCnpj().equals("24878265000155") || proposta.getCorretor().getCpfCnpj().equals("19914513000136")){
			corretorNovaComissao = true;
		}
		try {
			if (TipoSucursalSeguradora.MERCADO.equals(proposta.getSucursalSelecionada().getTipo()) || TipoSucursalSeguradora.CORPORATE.equals(proposta.getSucursalSelecionada().getTipo())) {
				tipoComissao =  new TipoComissaoMercadoCorporate().obterTipoComissao(proposta.getPagamento().getCodigoTipoCobranca(), corretorNovaComissao);
			} else if (TipoSucursalSeguradora.REDE.equals(proposta.getSucursalSelecionada().getTipo())) {
				if (proposta.getCorretorMaster() != null && proposta.getCorretorMaster().getCpd() != null && proposta.getCorretorMaster().getCpd() > 0L) {
					tipoComissao =  new TipoComissaoRedeCorretorMaster().obterTipoComissao(proposta.getPagamento().getCodigoTipoCobranca(), corretorNovaComissao);
				} else {
					tipoComissao =  new TipoComissaoRedeGerenteProduto().obterTipoComissao(proposta.getPagamento().getCodigoTipoCobranca(),corretorNovaComissao);
				}
			}	
		} catch (Exception e) {
			LOGGER.error("Tipo Comiss�o " + e.getMessage());
		}

		return tipoComissao;
	}
}
