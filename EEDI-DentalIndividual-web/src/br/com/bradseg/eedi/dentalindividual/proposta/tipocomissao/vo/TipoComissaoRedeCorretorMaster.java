package br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.vo;

import br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.util.ConstantesTipoComissao;
import br.com.bradseg.eedi.dentalindividual.vo.TipoCobranca;

public class TipoComissaoRedeCorretorMaster implements TipoComissao {

	@Override
	public Integer obterTipoComissao(Integer tipoCobranca, boolean corretorNovaComissao) {
		Integer tipoComissao = null;

		if (TipoCobranca.MENSAL.getCodigo() == tipoCobranca) {
			tipoComissao = ConstantesTipoComissao.COMISSAO_MENSAL_REDE_CORRETOR_MASTER;
		} else if (TipoCobranca.ANUAL.getCodigo() == tipoCobranca) {
			tipoComissao = ConstantesTipoComissao.COMISSAO_ANUAL_REDE_CORRETOR_MASTER;
		}
		return tipoComissao;
	}

}
