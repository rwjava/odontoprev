package br.com.bradseg.eedi.dentalindividual.proposta.presentation.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class QueryModel implements Serializable {
	
	private static final long serialVersionUID = -5323659850547349679L;
	
		private String query;
		private String tipoQuery;
		private List<String> colunas;
		private List<List<String>> dados;

		public QueryModel() {
			this.colunas = new LinkedList<String>();
			this.dados = new LinkedList<List<String>>();
		}

		public String getQuery() {
			return query;
		}

		public void setQuery(String query) {
			this.query = query;
		}

		public String getTipoQuery() {
			return tipoQuery;
		}

		public void setTipoQuery(String tipoQuery) {
			this.tipoQuery = tipoQuery;
		}

		public List<String> getColunas() {
			return colunas;
		}

		public void setColunas(List<String> colunas) {
			this.colunas = colunas;
		}

		public List<List<String>> getDados() {
			return dados;
		}

		public void addDados(List<String> linha) {
			this.dados.add(linha);
		}

	}

