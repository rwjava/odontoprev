package br.com.bradseg.eedi.dentalindividual.proposta.facade;

import java.io.File;
import java.util.List;

import br.com.bradseg.eedi.dentalindividual.support.ReportFileUtil;
import br.com.bradseg.eedi.dentalindividual.vo.AcompanhamentoPropostaEEDIVO;
import br.com.bradseg.eedi.dentalindividual.vo.FiltroRelatorioAcompanhamentoEEDIVO;
import br.com.bradseg.eedi.dentalindividual.vo.ValorRelatorioAcompanhamentoPropostaEEDIVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LoginVO;

public interface RelatorioAcompanhamentoServiceFacade {

    public List<AcompanhamentoPropostaEEDIVO> listarPropostasRelatorioAcompanhamentoPorFiltro(FiltroRelatorioAcompanhamentoEEDIVO filtro);

	public ValorRelatorioAcompanhamentoPropostaEEDIVO obterValoresPropostas(List<AcompanhamentoPropostaEEDIVO> listaAcompanhamento);
	
	public byte[] gerarPDFRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoEEDIVO filtro);

	public ReportFileUtil gerarPDFRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoEEDIVO filtro, LoginVO login);

	 public File gerarArquivoCSVRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoEEDIVO filtro, LoginVO login);
}
