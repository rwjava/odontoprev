package br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.util;

public class ConstantesTipoComissao {

	public static final Integer COMISSAO_MENSAL_MERCADO_CORPORATE = 1;
	public static final Integer COMISSAO_ANUAL_MERCADO_CORPORATE = 5;
	public static final Integer COMISSAO_MENSAL_REDE_CORRETOR_MASTER = 2;
	public static final Integer COMISSAO_ANUAL_REDE_CORRETOR_MASTER = 7;
	public static final Integer COMISSAO_MENSAL_REDE_GERENTE_PRODUTO = 3;
	public static final Integer COMISSAO_ANUAL_REDE_GERENTE_PRODUTO = 6;
	public static final Integer COMISSAO_MENSAL_MERCADO_CORPORATE_AZEVEDODO_LALUPA = 28;
}
