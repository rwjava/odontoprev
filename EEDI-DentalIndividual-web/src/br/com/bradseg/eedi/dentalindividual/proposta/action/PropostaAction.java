package br.com.bradseg.eedi.dentalindividual.proposta.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.dentalindividual.canalvenda.facade.CanalVendaServiceImpl;
import br.com.bradseg.eedi.dentalindividual.ccrr.facade.DadosCorretorFacade;
import br.com.bradseg.eedi.dentalindividual.cep.facade.CepServiceFacade;
import br.com.bradseg.eedi.dentalindividual.contacorrente.facade.ContaCorrenteServiceFacade;
import br.com.bradseg.eedi.dentalindividual.corretorsucursal.facade.CorretorSucursalServiceFacade;
import br.com.bradseg.eedi.dentalindividual.exception.EEDIBusinessException;
import br.com.bradseg.eedi.dentalindividual.odontoprev.facade.OdontoprevServiceFacade;
import br.com.bradseg.eedi.dentalindividual.plano.facade.PlanoServiceFacade;
import br.com.bradseg.eedi.dentalindividual.proposta.facade.GerarArquivoRelatorioEEDI;
import br.com.bradseg.eedi.dentalindividual.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.dentalindividual.proposta.facade.SucursalCorretorServiceImpl;
import br.com.bradseg.eedi.dentalindividual.proposta.presentation.model.QueryModel;
import br.com.bradseg.eedi.dentalindividual.proposta.tipocomissao.facade.TipoComissaoServiceFacade;
import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.vo.Banco;
import br.com.bradseg.eedi.dentalindividual.vo.CanalVO;
import br.com.bradseg.eedi.dentalindividual.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.dentalindividual.vo.CorretorVO;
import br.com.bradseg.eedi.dentalindividual.vo.EnderecoVO;
import br.com.bradseg.eedi.dentalindividual.vo.EstadoCivil;
import br.com.bradseg.eedi.dentalindividual.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento;
import br.com.bradseg.eedi.dentalindividual.vo.GrauParentesco;
import br.com.bradseg.eedi.dentalindividual.vo.LabelValueVO;
import br.com.bradseg.eedi.dentalindividual.vo.PlanoVO;
import br.com.bradseg.eedi.dentalindividual.vo.PlanoVOView;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.Sexo;
import br.com.bradseg.eedi.dentalindividual.vo.SituacaoProposta;
import br.com.bradseg.eedi.dentalindividual.vo.SucursalVO;
import br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO;
import br.com.bradseg.eedi.dentalindividual.vo.TipoBusca;
import br.com.bradseg.eedi.dentalindividual.vo.TipoCPF;
import br.com.bradseg.eedi.dentalindividual.vo.TipoCobranca;
import br.com.bradseg.eedi.dentalindividual.vo.TipoSucursalSeguradora;
import br.com.bradseg.eedi.dentalindividual.vo.TipoTelefone;
import br.com.bradseg.eedi.dentalindividual.vo.ValorPlanoViewVO;

/**
 * Classe responsavel pela apresenta��o das informa��es e realiza��o das a��es
 * referentes a tela.
 */
@Controller
@Scope(value = "request")
public class PropostaAction extends BaseAction {

	private static final long serialVersionUID = 2413271777838545504L;

	private PropostaVO proposta = new PropostaVO();
	
	private String query;
	
	private List<String> colunas;
	
	private QueryModel queryModel;
	
	private int numeroDependente;

	private double valorTotalMensal;

	private double valorTotalAnual;

	private boolean indicadorCepDoTitular;

	private String contaCorrenteFormatada;

	private String cep;

	private List<PlanoVOView> planos;

	private List<Sexo> sexos;

	private List<LabelValueVO> estadosCivis = EstadoCivil.obterLista();

	private List<GrauParentesco> grausParentesco;

	private List<LabelValueVO> bancos;

	private List<TipoCobranca> tiposCobranca;

	// private List<FormaPagamento> formasPagamento;

	private List<LabelValueVO> formasPagamento = FormaPagamento.obterLista();

	private List<TipoTelefone> tiposTelefone;

	private List<ContaCorrenteVO> contasCorrente;

	private List<TipoBusca> tiposBusca;
	private FiltroPropostaVO filtroProposta = new FiltroPropostaVO();
	private List<TipoCPF> tiposCPFs;
	private List<SituacaoProposta> status;
	
	private Integer formaPagamentoSelecionada;

	private List<PropostaVO> propostas;

	private boolean indicativoDeCancelamentoDaProposta;

	@Autowired
	private transient PlanoServiceFacade planoServiceFacade;

	@Autowired
	private transient CepServiceFacade cepServiceFacade;

	@Autowired
	private transient ContaCorrenteServiceFacade contaCorrenteServiceFacade;

	@Autowired
	private transient PropostaServiceFacade propostaServiceFacade;

	@Autowired
	private transient CorretorSucursalServiceFacade corretorSucursalServiceFacade;

	private List<SucursalVO> listaSucursais;

	private String[] cpdEcodigo;

	/* private SucursalVO sucursalSelecionada = new SucursalVO(); */

	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaAction.class);

	@Autowired
	protected SucursalCorretorServiceImpl serviceSucursal;

	@Autowired
	private DadosCorretorFacade dadosCorretorFacade;

	private String tipoSucursal;
	
	private String msg;

	@Autowired
	private OdontoprevServiceFacade odontoprevServiceFacade;

//	private List<String> bandeiras;
	private List<LabelValueVO> bandeiras = new ArrayList<LabelValueVO>();

	private boolean corretorCartaoCreditoHabilitado;

	private Long codigoPlano;

	private String nomePagante;

	@Autowired
	private GerarArquivoRelatorioEEDI gerarArquivoRelatorio;

	private InputStream inputStream;
	private String fileName;

	private int tamanhoArquivo;
	
	private Integer numeroVidas;

	private List<CanalVO> listaCanal;
	@Autowired
	private CanalVendaServiceImpl canalVendaService;
	
	private Date dataEmissao;
	private Date dataValidade;
	
	//private String 


	@Autowired
	private transient TipoComissaoServiceFacade tipoComissaoServiceFacade;

	/**
	 * Metodo responsavel por inicializar todas as informa��es necess�rias para
	 * a cria��o de uma nova proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String novaProposta() {

		// inicializar todas informa��es necess�rias para a tela
		inicializarTelaNovaProposta();

		// setar os dados do corretor
		proposta.setCorretor(obterDadosCorretorLogado());
		proposta.setStatus(SituacaoProposta.RASCUNHO.name());
		return SUCCESS;
	}
	
	public String limparTelaProposta(){
		
 		proposta = new PropostaVO();
		inicializarTelaNovaProposta();
		// setar os dados do corretor
		proposta.setCorretor(obterDadosCorretorLogado());
		ServletActionContext.getRequest().getSession().setAttribute("plano", null);

		
		return SUCCESS;
	}
	
	public String iniciarQuery(){
		return SUCCESS;
	}

/*	public String actionQuery() {
		queryModel = new QueryModel();
		//String sql = getQuery();
		try {
			queryModel = propostaServiceFacade.query(getQuery());
			setColunas(queryModel.getColunas());
			//queryModel.getDados();
			
			setMsg("Query executada com sucesso");
			LOGGER.error("Query executada com sucesso ");
		} catch (Exception ex) {
			queryModel = new QueryModel();
			LOGGER.error("Erro ao executar query: " + ex.getMessage());
		}
		
		return SUCCESS;
		
	}*/
	public String limparTelaPropostaIntranet(){
		
 		proposta = new PropostaVO();
		inicializarTelaNovaPropostaIntranet();
		// setar os dados do corretor
		proposta.setCorretor(obterDadosCorretorLogado());
		ServletActionContext.getRequest().getSession().setAttribute("plano", null);

		return SUCCESS;
	}
	
/*	public String limparTelaPropostaIntranet(){
		proposta = new PropostaVO();
		inicializarTelaNovaPropostaIntranet();
		//setar os dados do corretor
		proposta.setCorretor(obterDadosCorretorLogado());
		ServletActionContext.getRequest().getSession().setAttribute("plano", null);
		
		return SUCCESS;
	}*/

	/**
	 * Metodo responsavel por obter o endere�o a partir do cep informado.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String obterEnderecoPorCep() {

		try {
			// inicializar todas informa��es necess�rias para a tela
//			inicializarTelaNovaProposta();
			codigoPlano = (Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO);

			EnderecoVO endereco = cepServiceFacade.obterEnderecoPorCep(cep);
			if (indicadorCepDoTitular) {
				proposta.getBeneficiarios().getTitular().setEndereco(endereco);
			} else {
				proposta.getBeneficiarios().getRepresentanteLegal().setEndereco(endereco);
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
//		inicializarTelaNovaProposta();
		return SUCCESS;
	}

	public String obterCorretorCCRR() {

		return SUCCESS;
	}

	/**
	 * Metodo responsavel por listar as contas corrente dos clientes bradesco.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String listarContaCorrentePorCPF() {

		try {
			// inicializar todas informa��es necess�rias para a tela
			inicializarTelaNovaProposta();

			contasCorrente = contaCorrenteServiceFacade.listarContaCorrentePorCPF(proposta.getPagamento().getContaCorrente().getCpf());
			ServletActionContext.getRequest().getSession().setAttribute("contasCorrente", contasCorrente);
			if (contasCorrente != null && !contasCorrente.isEmpty()) {
				proposta.getPagamento().getContaCorrente().setNome(contasCorrente.get(0).getNome());
				proposta.getPagamento().getContaCorrente().setDataNascimento(contasCorrente.get(0).getDataNascimento());
			}

		} catch (Exception e) {
			addActionError(e.getMessage());
		}

		return SUCCESS;
	}

	/**
	 * Metodo responsavel por adicionar um dependente na proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String adicionarDependente() {

		try {
			// adicionar dependente
			codigoPlano = (Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO);
			propostaServiceFacade.adicionarDependente(proposta);
			if(proposta.getPlano().getCodigo() != null && proposta.getPlano().getCodigo() != 0){
				proposta.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
				calcularValorTotalDaPropostaNova();
			}
		} catch (BusinessException e) {
			tratarErro(e);
		}

		// inicializar todas informa��es necess�rias para a tela
		inicializarTelaNovaProposta();

		return SUCCESS;
	}

	/**
	 * Metodo responsavel por remover um determinado dependente.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String removerDependente() {

		proposta.getBeneficiarios().getDependentes().remove(numeroDependente);
		if(proposta.getPlano().getCodigo() != null && proposta.getPlano().getCodigo() != 0){
			proposta.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
			calcularValorTotalDaPropostaNova();
		}
		// inicializar todas informa��es necess�rias para a tela
		inicializarTelaNovaProposta();

		return SUCCESS;
	}

	/**
	 * Metodo responsavel por alterar o plano da proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String alterarPlanoDaProposta() {

		// inicializar todas informa��es necess�rias para a tela
		inicializarTelaNovaProposta();

		return SUCCESS;
	}

	public String gerarPropostaVazia(){
		inicializarTelaNovaProposta();
			
		if (proposta.getPlano().getCodigo() == null) {
			obterPlanoSelecionado();
		}

		CorretorVO corretor = obterDadosCorretorLogado();
		proposta.setSucursalSelecionada(formatarSucursalSelecionada(proposta.getSucursalSelecionada()));
		proposta.getSucursalSelecionada().setTipo(serviceSucursal.obterTipoSucursalPorCodigo(proposta.getSucursalSelecionada().getCodigo()));
		proposta.getSucursalSelecionada().setNome(proposta.getSucursalSelecionada().getTipo().getNome());
		proposta.getCorretor().setCpfCnpj(corretor.getCpfCnpj());
		proposta.getCorretor().setCpd(Long.valueOf(proposta.getSucursalSelecionada().getCpd()));
		proposta.getPagamento().setFormaPagamento(null);
		proposta.getPagamento().setCodigoTipoCobranca(null);
		proposta.setStatus(SituacaoProposta.RASCUNHO.name());
		try{		
			proposta.setSequencial(propostaServiceFacade.imprimirPropostaVazia(proposta));
			proposta.setCodigo(propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial()).getCodigo());
		} catch (EEDIBusinessException e) {
			LOGGER.error("Erro ao  gerar proposta vazia: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (BusinessException e) {
			LOGGER.error("Erro ao  gerar proposta vazia: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error("Erro ao  gerar proposta vazia: " + e.getMessage());
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return SUCCESS;
		
	}
	public String imprimirPropostaVazia() {
		inicializarTelaNovaProposta();
		proposta.setDataValidadeProposta(null);
		try{
			byte[] relatorioPDF = propostaServiceFacade.gerarPDFPropostaVazia(proposta); // gerarArquivoRelatorio.gerar(propostaServiceFacade.gerarPDFPropostaVazia(proposta));
			setTamanhoArquivo(relatorioPDF.length);
			setInputStream(new ByteArrayInputStream(relatorioPDF));
			proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());

			setFileName("BS-PROPOSTA-" + proposta.getCodigo());
		
		} catch (EEDIBusinessException e) {
			tratarErro(e);
			LOGGER.error(String.format("Erro ao imprimir proposta vazia [%s]", e.getMessage()));
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			LOGGER.error(String.format("Erro ao imprimir proposta vazia [%s]", e.getMessage()));
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			LOGGER.error(String.format("Erro ao imprimir proposta vazia [%s]", e.getMessage()));
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String imprimirProposta() {

		inicializarTelaNovaProposta();

		
		proposta = propostaServiceFacade.obterPropostaPorCodigo(proposta.getCodigo());
		
		if(proposta.getCorretor().getSucursal() != null && proposta.getCorretor().getCpd() != null){
			proposta.setSucursalSelecionada(new SucursalVO());
			proposta.getSucursalSelecionada().setCodigo(proposta.getCorretor().getSucursal());
			proposta.getSucursalSelecionada().setCpd(proposta.getCorretor().getCpd().intValue());
			proposta.getSucursalSelecionada().setCpdSucursal(String.valueOf(proposta.getCorretor().getSucursal()).concat(";").concat(String.valueOf(proposta.getSucursalSelecionada().getCpd())));
		}

		try {
			byte[] relatorioPDF = propostaServiceFacade.gerarPDFProposta(proposta); // gerarArquivoRelatorio.gerar(propostaServiceFacade.gerarPDFPropostaVazia(proposta));
			setTamanhoArquivo(relatorioPDF.length);
			setInputStream(new ByteArrayInputStream(relatorioPDF));
			setFileName("BS-PROPOSTA-" + proposta.getCodigo());
		} catch (Exception e) {
			LOGGER.error("Erro gerar pdf proposta: " + e.getMessage());
			return INPUT;

		}

		return SUCCESS;
	}
	
	public String gerarPDFPropostaVazia() {

		LOGGER.error("Inicio do metodo gerar pdf proposta vazia ");

		try {
			byte[] relatorioPDF = propostaServiceFacade.gerarPDFPropostaVazia(proposta); // gerarArquivoRelatorio.gerar(propostaServiceFacade.gerarPDFPropostaVazia(proposta));
			setTamanhoArquivo(relatorioPDF.length);
			setInputStream(new ByteArrayInputStream(relatorioPDF));
			setFileName("BS-PROPOSTA-" + proposta.getCodigo());
		} catch (Exception e) {
			LOGGER.error("Erro gerar pdf proposta vazia :" + e.getMessage());
			return INPUT;

		}
		LOGGER.error("fim do metodo gerar pdf");
		return SUCCESS;
	}

	public String salvarRascunhoProposta() {

		try {
			LOGGER.error("Inicio do metodo salvar rascunho de proposta");
			// inicializar todas informa��es necess�rias para a tela
			inicializarTelaNovaProposta();
			codigoPlano = (Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO);
			obterNomePagante();
			obterPlanoSelecionado();

			retirarFormatacaoCPFPagante();

			CorretorVO corretor = obterDadosCorretorLogado();
			proposta.getCorretor().setCpfCnpj(corretor.getCpfCnpj());
//			proposta.getPagamento().setFormaPagamentoEnum(FormaPagamento.buscaPor(formaPagamentoSelecionada));
			proposta.setSucursalSelecionada(formatarSucursalSelecionada(proposta.getSucursalSelecionada()));
			proposta.getSucursalSelecionada().setTipo(serviceSucursal.obterTipoSucursalPorCodigo(proposta.getSucursalSelecionada().getCodigo()));
			proposta.getSucursalSelecionada().setNome(proposta.getSucursalSelecionada().getTipo().getNome());
			
			if(proposta.getSucursalSelecionada().getCpd() != null){
				proposta.getCorretor().setCpd(Long.valueOf(proposta.getSucursalSelecionada().getCpd()));
			}

			proposta.setTipoComissao(tipoComissaoServiceFacade.obterTipoComissao(proposta));
			preencherInformacoesEndereco(proposta);

			PropostaVO propostaTemporaria =  propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			if(propostaTemporaria != null){
				proposta.setCodigo(propostaTemporaria.getCodigo());
			}			
			proposta.setSequencial(propostaServiceFacade.salvarRascunhoProposta(proposta));
			proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			
			if(proposta.getCodigo() != null){
				LOGGER.error(String.format("Proposta salva em sacunho com sucesso [%s]", proposta.getCodigo()));
				addActionMessage("Proposta N� " + codigoPropostaFormatado(proposta.getCodigo()) + " gerada com sucesso.");
			}

		} catch (EEDIBusinessException e) {
			LOGGER.error("Erro ao  salvar rascunho de proposta: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (BusinessException e) {
			LOGGER.error("Erro ao  salvar rascunho de proposta: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error("Erro ao  salvar rascunho de proposta: " + e.getMessage());
			addActionError("Erro ao  salvar rascunho de proposta: " + e.getMessage());
			return SUCCESS;
		}

		return carregarProposta(proposta.getSequencial());
		
	}

	private void preencherInformacoesEndereco(PropostaVO proposta) {
				
		if(StringUtils.isNotBlank(proposta.getBeneficiarios().getTitular().getEndereco().getCep())){
			EnderecoVO endereco = cepServiceFacade.obterEnderecoPorCep(proposta.getBeneficiarios().getTitular().getEndereco().getCep().replaceAll("[.,;\\-\\s]", ""));
			if(endereco != null){
				if(StringUtils.isNotBlank(endereco.getBairro())){
					proposta.getBeneficiarios().getTitular().getEndereco().setBairro(endereco.getBairro());
				}
				if(StringUtils.isNotBlank(endereco.getCidade())){
					proposta.getBeneficiarios().getTitular().getEndereco().setCidade(endereco.getCidade());
				}
				if(StringUtils.isNotBlank(endereco.getEstado())){
					proposta.getBeneficiarios().getTitular().getEndereco().setEstado(endereco.getEstado());
				}
				if(StringUtils.isNotBlank(endereco.getLogradouro())){
					proposta.getBeneficiarios().getTitular().getEndereco().setLogradouro(endereco.getLogradouro());
				}
			}
		}if(StringUtils.isNotBlank(proposta.getBeneficiarios().getRepresentanteLegal().getEndereco().getCep())){
			EnderecoVO endereco = cepServiceFacade.obterEnderecoPorCep(proposta.getBeneficiarios().getRepresentanteLegal().getEndereco().getCep().replaceAll("[.,;\\-\\s]", ""));
			if(endereco != null){
				if(StringUtils.isNotBlank(endereco.getBairro())){
					proposta.getBeneficiarios().getRepresentanteLegal().getEndereco().setBairro(endereco.getBairro());
				}
				if(StringUtils.isNotBlank(endereco.getCidade())){
					proposta.getBeneficiarios().getRepresentanteLegal().getEndereco().setCidade(endereco.getCidade());
				}
				if(StringUtils.isNotBlank(endereco.getEstado())){
					proposta.getBeneficiarios().getRepresentanteLegal().getEndereco().setEstado(endereco.getEstado());
				}
				if(StringUtils.isNotBlank(endereco.getLogradouro())){
					proposta.getBeneficiarios().getRepresentanteLegal().getEndereco().setLogradouro(endereco.getLogradouro());
				}
			}
		}
	}


	private void retirarFormatacaoCPFPagante() {
		proposta.getPagamento().getContaCorrente().setCpf(proposta.getPagamento().getContaCorrente().getCpf().replaceAll("[.,;\\-\\s]", ""));
	}

	private void obterPlanoSelecionado() {
		proposta.getPlano().setCodigo((Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO));
	}

	private void obterNomePagante() {
		try {
			if (StringUtils.isNotBlank(nomePagante) && StringUtils.isBlank(proposta.getPagamento().getContaCorrente().getNome())) {
				proposta.getPagamento().getContaCorrente().setNome(nomePagante);
			}
		} catch (Exception e) {
			LOGGER.error("Erro ao obter nome pagante. " + e.getMessage());
		}
	}

	/**
	 * Metodo responsavel por finalizar uma proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String finalizarProposta() {

		try {

			// inicializar todas informa��es necess�rias para a tela
			inicializarTelaNovaProposta();
			codigoPlano = (Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO);
			obterNomePagante();
			obterPlanoSelecionado();
			obterValoresPlano();
			calcularValorTotalDaPropostaNova();
			retirarFormatacaoCPFPagante();
			// finalizar uma proposta
			CorretorVO corretor = obterDadosCorretorLogado();
			proposta.getCorretor().setCpfCnpj(corretor.getCpfCnpj());
//			proposta.getPagamento().setFormaPagamentoEnum(FormaPagamento.buscaPor(formaPagamentoSelecionada));
			if(proposta.getSucursalSelecionada() != null && proposta.getSucursalSelecionada().getCpdSucursal() != null && !Constantes.ZERO_STRING.equals(proposta.getSucursalSelecionada().getCpdSucursal())) {
				proposta.setSucursalSelecionada(formatarSucursalSelecionada(proposta.getSucursalSelecionada()));
				proposta.getSucursalSelecionada().setTipo(serviceSucursal.obterTipoSucursalPorCodigo(proposta.getSucursalSelecionada().getCodigo()));
				proposta.getSucursalSelecionada().setNome(proposta.getSucursalSelecionada().getTipo().getNome());
				proposta.getCorretor().setCpd(Long.valueOf(proposta.getSucursalSelecionada().getCpd()));
//				preencherDadosCorretor();
			}
			PropostaVO propostaTemporaria =  propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			if(propostaTemporaria != null){
				proposta.setCodigo(propostaTemporaria.getCodigo());
			}
			
			proposta.setTipoComissao(tipoComissaoServiceFacade.obterTipoComissao(proposta));
			preencherInformacoesEndereco(proposta);
			proposta.setSequencial(propostaServiceFacade.finalizarProposta(proposta));
			proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			
			if(proposta.getCodigo() != null){
				addActionMessage("Proposta N� " + codigoPropostaFormatado(proposta.getCodigo()) + " finalizada com sucesso.");
				LOGGER.error(String.format("Proposta finalizada: [%s]", proposta.getSequencial()));
			}

		} catch (EEDIBusinessException e) {
			LOGGER.error("Erro ao  finalizar proposta: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (BusinessException e) {
			LOGGER.error("Erro ao  finalizar proposta: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error("Erro ao  finalizar proposta: " + e.getMessage());
			addActionError(e.getMessage());
			return SUCCESS;
		}


		return carregarProposta(proposta.getSequencial());
	}

	private void obterValoresPlano() {
		if(proposta.getPlano().getCodigo() != null) {
			proposta.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
		}
	}

	/**
	 * Metodo responsavel por inicializar todas as informa��es necess�rias para
	 * a consulta de proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String listarProposta() {

		// inicializar todas informa��es necess�rias para a tela de listar
		// proposta
		inicializarTelaListarProposta();

		return SUCCESS;
	}

	/**
	 * Metodo responsavel por consultar propostas de acordo com o filtro
	 * informado.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String consultarProposta() {

		// inicializar todas informa��es necess�rias para a tela de listar
		// proposta
		inicializarTelaListarProposta();

		try {
			propostas = propostaServiceFacade.consultarPropostaPorFiltro(filtroProposta, obterDadosCorretorLogado());
		} catch (EEDIBusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}
	
	public String consultarPropostaIntranet() {

		// inicializar todas informa��es necess�rias para a tela de listar
		// proposta
		inicializarTelaListarProposta();

		try {
			filtroProposta.setIndicadorIntranet(true);
			propostas = propostaServiceFacade.consultarPropostaPorFiltro(filtroProposta, obterDadosCorretorLogado());
		} catch (EEDIBusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}
	
	

	/**
	 * Metodo responsavel por visualizar uma proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String visualizarProposta() {

		try {
			inicializarTelaListarProposta();
			
			proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			
			try{
				List<TelefoneVO> listaDeTelefones = new ArrayList<TelefoneVO>();
				listaDeTelefones.addAll(proposta.getBeneficiarios().getTitular().getTelefones());
				proposta.getBeneficiarios().getTitular().setTelefones(new ArrayList<TelefoneVO>());
				proposta.getBeneficiarios().getTitular().getTelefones().add(new TelefoneVO(TipoTelefone.CELULAR));
				proposta.getBeneficiarios().getTitular().getTelefones().add(new TelefoneVO(TipoTelefone.RESIDENCIAL));
				proposta.getBeneficiarios().getTitular().getTelefones().add(new TelefoneVO(TipoTelefone.COMERCIAL));
				
				for(TelefoneVO telefone: listaDeTelefones){
					
					if(TipoTelefone.CELULAR.name().equals(telefone.getTipoTelefone().toUpperCase())){
						proposta.getBeneficiarios().getTitular().getTelefones().set(0, telefone);
					}else if(TipoTelefone.RESIDENCIAL.name().equals(telefone.getTipoTelefone().toUpperCase())){
						proposta.getBeneficiarios().getTitular().getTelefones().set(2, telefone);
					}else if(TipoTelefone.COMERCIAL.name().equals(telefone.getTipoTelefone().toUpperCase())){
						proposta.getBeneficiarios().getTitular().getTelefones().set(1, telefone);
					}
				}
			}catch(Exception e){
				LOGGER.error(String.format("Erro ao obter telefones da proposta [%s]", e.getMessage()));
			}
			
			// Arrumando ordem dos inputs de telefone que estavam bugados
//			TelefoneVO[] telefone = new TelefoneVO[3];
//			 
//			telefone[0] = new TelefoneVO();
//			telefone[1] = new TelefoneVO();
//			telefone[2] = new TelefoneVO();
			
//			if(proposta.getBeneficiarios().getTitular().getTelefones().size() == 1) {
//				
//				proposta.getBeneficiarios().getTitular().getTelefones().add(1, proposta.getBeneficiarios().getTitular().getTelefones().get(0));
//				proposta.getBeneficiarios().getTitular().getTelefones().add(2, proposta.getBeneficiarios().getTitular().getTelefones().get(0));
//				
//				if(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getTipoTelefone().equalsIgnoreCase(TipoTelefone.CELULAR.getNome())) {
//					telefone[0].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero());
//					telefone[0].setTipoTelefone(TipoTelefone.CELULAR.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getTipoTelefone().equalsIgnoreCase(TipoTelefone.RESIDENCIAL.getNome())) {
//					telefone[2].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(2).getDddEnumero());
//					telefone[2].setTipoTelefone(TipoTelefone.RESIDENCIAL.getNome());
//				}else if (proposta.getBeneficiarios().getTitular().getTelefones().get(1).getTipoTelefone().equalsIgnoreCase(TipoTelefone.COMERCIAL.getNome())) {
//					telefone[1].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero());
//					telefone[1].setRamal(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getRamal());
//					telefone[1].setTipoTelefone(TipoTelefone.COMERCIAL.getNome());
//				}
//			} else if(proposta.getBeneficiarios().getTitular().getTelefones().size() == 2) {
//				
//				proposta.getBeneficiarios().getTitular().getTelefones().add(2, proposta.getBeneficiarios().getTitular().getTelefones().get(0));
//				
//				if(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getTipoTelefone().equalsIgnoreCase(TipoTelefone.CELULAR.getNome())) {
//					telefone[0].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero());
//					telefone[0].setTipoTelefone(TipoTelefone.CELULAR.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getTipoTelefone().equalsIgnoreCase(TipoTelefone.RESIDENCIAL.getNome())) {
//					telefone[2].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero());
//					telefone[2].setTipoTelefone(TipoTelefone.RESIDENCIAL.getNome());
//				} else if (proposta.getBeneficiarios().getTitular().getTelefones().get(0).getTipoTelefone().equalsIgnoreCase(TipoTelefone.COMERCIAL.getNome())) {
//					telefone[1].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero());
//					telefone[1].setTipoTelefone(TipoTelefone.COMERCIAL.getNome());
//					telefone[1].setRamal(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getRamal());
//				}
//				
//				if(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getTipoTelefone().equalsIgnoreCase(TipoTelefone.CELULAR.getNome())) {
//					telefone[0].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero());
//					telefone[0].setTipoTelefone(TipoTelefone.CELULAR.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getTipoTelefone().equalsIgnoreCase(TipoTelefone.RESIDENCIAL.getNome())) {
//					telefone[2].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero());
//					telefone[2].setTipoTelefone(TipoTelefone.RESIDENCIAL.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getTipoTelefone().equalsIgnoreCase(TipoTelefone.COMERCIAL.getNome())) {
//					telefone[1].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero());
//					telefone[1].setTipoTelefone(TipoTelefone.COMERCIAL.getNome());
//					telefone[1].setRamal(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getRamal());
//				} 
//				
//			} else if(proposta.getBeneficiarios().getTitular().getTelefones().size() == 3) {
//				if(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getTipoTelefone().equalsIgnoreCase(TipoTelefone.CELULAR.getNome())) {
//					telefone[0].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero());
//					telefone[0].setTipoTelefone(TipoTelefone.CELULAR.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getTipoTelefone().equalsIgnoreCase(TipoTelefone.RESIDENCIAL.getNome())) {
//					telefone[2].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero());
//					telefone[2].setTipoTelefone(TipoTelefone.RESIDENCIAL.getNome());
//				} else if (proposta.getBeneficiarios().getTitular().getTelefones().get(0).getTipoTelefone().equalsIgnoreCase(TipoTelefone.COMERCIAL.getNome())) {
//					telefone[1].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(0).getDddEnumero());
//					telefone[1].setTipoTelefone(TipoTelefone.COMERCIAL.getNome());
//					telefone[1].setRamal(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getRamal());
//				}
//				
//				if(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getTipoTelefone().equalsIgnoreCase(TipoTelefone.CELULAR.getNome())) {
//					telefone[0].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero());
//					telefone[0].setTipoTelefone(TipoTelefone.CELULAR.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getTipoTelefone().equalsIgnoreCase(TipoTelefone.RESIDENCIAL.getNome())) {
//					telefone[2].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero());
//					telefone[2].setTipoTelefone(TipoTelefone.RESIDENCIAL.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getTipoTelefone().equalsIgnoreCase(TipoTelefone.COMERCIAL.getNome())) {
//					telefone[1].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getDddEnumero());
//					telefone[1].setTipoTelefone(TipoTelefone.COMERCIAL.getNome());
//					telefone[1].setRamal(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getRamal());
//				} 
//				
//				if(proposta.getBeneficiarios().getTitular().getTelefones().get(2).getTipoTelefone().equalsIgnoreCase(TipoTelefone.CELULAR.getNome())) {
//					telefone[0].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(2).getDddEnumero());
//					telefone[0].setTipoTelefone(TipoTelefone.CELULAR.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(2).getTipoTelefone().equalsIgnoreCase(TipoTelefone.RESIDENCIAL.getNome())) {
//					telefone[2].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(2).getDddEnumero());
//					telefone[2].setTipoTelefone(TipoTelefone.RESIDENCIAL.getNome());
//				} else if(proposta.getBeneficiarios().getTitular().getTelefones().get(2).getTipoTelefone().equalsIgnoreCase(TipoTelefone.COMERCIAL.getNome())) {
//					telefone[1].setDddEnumero(proposta.getBeneficiarios().getTitular().getTelefones().get(2).getDddEnumero());
//					telefone[1].setTipoTelefone(TipoTelefone.COMERCIAL.getNome());
//					telefone[1].setRamal(proposta.getBeneficiarios().getTitular().getTelefones().get(1).getRamal());
//				} 
//			}
			
//			proposta.getBeneficiarios().getTitular().setTelefones(Arrays.asList(telefone));
			//FIM BLOCO TRATAMENTO TELEFONE
			
			if(proposta.getCorretor().getSucursal() != null && proposta.getCorretor().getCpd() != null){
				proposta.setSucursalSelecionada(new SucursalVO());
				proposta.getSucursalSelecionada().setCodigo(proposta.getCorretor().getSucursal());
				proposta.getSucursalSelecionada().setCpd(proposta.getCorretor().getCpd().intValue());
				proposta.getSucursalSelecionada().setCpdSucursal(String.valueOf(proposta.getCorretor().getSucursal()).concat(";").concat(String.valueOf(proposta.getSucursalSelecionada().getCpd())));
			}
			indicativoDeCancelamentoDaProposta = verificarSePropostaPodeSerCancelada();
			calcularValorTotalDaPropostaNova();
			inicializarTelaNovaProposta();
			codigoPlano = proposta.getPlano().getCodigo();
			LOGGER.error("Codigo do plano: " + codigoPlano);
			ServletActionContext.getRequest().getSession().setAttribute("plano", codigoPlano);
			
		} catch (EEDIBusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}
	
	public String visualizarPropostaIntranet(){
		
		try {
			inicializarTelaListarProposta();
			proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			
			if(proposta.getCorretor().getSucursal() != null && proposta.getCorretor().getCpd() != null){
				proposta.setSucursalSelecionada(new SucursalVO());
				proposta.getSucursalSelecionada().setCodigo(proposta.getCorretor().getSucursal());
				proposta.getSucursalSelecionada().setCpd(proposta.getCorretor().getCpd().intValue());
				proposta.getSucursalSelecionada().setCpdSucursal(String.valueOf(proposta.getCorretor().getSucursal()).concat(";").concat(String.valueOf(proposta.getSucursalSelecionada().getCpd())));
			}
			indicativoDeCancelamentoDaProposta = verificarSePropostaPodeSerCancelada();
			calcularValorTotalDaPropostaNova();
			inicializarTelaNovaPropostaIntranet();
			codigoPlano = proposta.getPlano().getCodigo();
			LOGGER.error("Codigo do plano: " + codigoPlano);
			ServletActionContext.getRequest().getSession().setAttribute("plano", codigoPlano);
			
		} catch (EEDIBusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}

	
	
	public String carregarProposta(Long sequencial){
		try {
			inicializarTelaListarProposta();
			proposta = propostaServiceFacade.obterPropostaPorSequencial(sequencial);
			
			if(proposta.getCorretor().getSucursal() != null && proposta.getCorretor().getCpd() != null){
				proposta.setSucursalSelecionada(new SucursalVO());
				proposta.getSucursalSelecionada().setCodigo(proposta.getCorretor().getSucursal());
				proposta.getSucursalSelecionada().setCpd(proposta.getCorretor().getCpd().intValue());
				proposta.getSucursalSelecionada().setCpdSucursal(String.valueOf(proposta.getCorretor().getSucursal()).concat(";").concat(String.valueOf(proposta.getSucursalSelecionada().getCpd())));
			}
			indicativoDeCancelamentoDaProposta = verificarSePropostaPodeSerCancelada();
			calcularValorTotalDaPropostaNova();
			inicializarTelaNovaProposta();
			codigoPlano = proposta.getPlano().getCodigo();
			LOGGER.error("Codigo do plano: " + codigoPlano);
			ServletActionContext.getRequest().getSession().setAttribute("plano", codigoPlano);
			
		} catch (EEDIBusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}
	
	private boolean verificarSePropostaPodeSerCancelada() {
		boolean retorno = false;

		if (proposta.getStatus().contains("CANCELADA")) {
			retorno = false;
		} else if (null != proposta.getDataEmissao() && new LocalDate().isBefore(proposta.getDataEmissao().plusDays(7))) {
			retorno = true;
		}
		return retorno;
	}
	/**
	 * Metodo responsavel por cancelar somente no MIp.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String cancelarPropostaMip() {
		try {
			//chamar o m�todo do MIP(odontoprev) para realizar o estorno da 1� parcela creditada no cart�o
			if((proposta.getPagamento().getCartaoCredito() != null) && SituacaoProposta.CANCELADA.getNome().toUpperCase().equals(proposta.getStatus().toUpperCase())){
				odontoprevServiceFacade.cancelarDebitoPrimeiraParcelaCartaoDeCredito(proposta);
			}

		} catch (EEDIBusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}
		//addActionMessage("Proposta N� " + codigoPropostaFormatado(proposta.getCodigo()) + " cancelada com sucesso.");

		return SUCCESS;
	}
	
	

	/**
	 * Metodo responsavel por cancelar uma proposta.
	 * 
	 * @return String - especifica��o da p�gina a ser retornada (struts.xml).
	 */
	public String cancelarProposta() {
		try {

			propostaServiceFacade.cancelarProposta(proposta.getSequencial());
			proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			
			//chamar o m�todo do MIP(odontoprev) para realizar o estorno da 1� parcela creditada no cart�o
			if((proposta.getPagamento().getCartaoCredito() != null) && SituacaoProposta.CANCELADA.getNome().toUpperCase().equals(proposta.getStatus().toUpperCase())){
				odontoprevServiceFacade.cancelarDebitoPrimeiraParcelaCartaoDeCredito(proposta);
			}

		} catch (EEDIBusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (BusinessException e) {
			tratarErro(e);
			return ERROR;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		addActionMessage("Proposta N� " + codigoPropostaFormatado(proposta.getCodigo()) + " cancelada com sucesso.");

		return SUCCESS;
	}

	
	public String validarDadosCartaoCredito(){
//		.append("paymentToken=" + proposta.getPagamento().getCartaoCredito().getPaymentToken() + "&")
//		.append("nome=" + proposta.getPagamento().getCartaoCredito().getNomeCartao() + "&")
//		.append("cpf=" + proposta.getPagamento().getCartaoCredito().getCpfCartao() + "&")
//		.append("bandeira=" + proposta.getPagamento().getCartaoCredito().getBandeira());
//		
		LOGGER.error( String.format("Validar dados de cartao de credito para proposta : [%s]" , proposta.getSequencial()));
		boolean envioAoMIPComSucesso = false;
		try{
			envioAoMIPComSucesso = propostaServiceFacade.validarDadosCartaoCredito(proposta);
			if(envioAoMIPComSucesso){
				proposta = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			}
			
		}catch (BusinessException e) {
			LOGGER.error(String.format("Erro na valida��o dos dados do cart�o para proposta [%s]. [%s]", proposta.getSequencial(), e.getMessage()));
			proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
			addActionError(e.getMessage());
			return SUCCESS;
		}
		catch(Exception e){
			LOGGER.error(String.format("Erro na valida��o dos dados do cart�o para proposta [%s]. [%s]", proposta.getSequencial(), e.getMessage()));
			proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
			addActionError(e.getMessage());
			return SUCCESS;
		}
		if(envioAoMIPComSucesso == true){
			addActionMessage("Proposta N� " + codigoPropostaFormatado(proposta.getCodigo()) + " gerada com sucesso.");
		}
		return carregarProposta(proposta.getSequencial());
	}
	/**
	 * Metodo responsavel pela inicializa��o das informa��es na tela de listar
	 * proposta.
	 */
	private void inicializarTelaListarProposta() {
		tiposBusca = TipoBusca.listar();
		tiposCPFs = TipoCPF.listar();
		status = SituacaoProposta.listar();
		setListaCanal(obterCanaisVenda());

	}

	private List<CanalVO> obterCanaisVenda() {	
		return canalVendaService.listaDeCanais();
	}

	/**
	 * Metodo responsavel pela inicializa��o das informa��es na tela de nova
	 * proposta. - Listar os planos vigentes para o canal CORRETORA MERCADO; -
	 * Calcular o valor total da proposta de acordo com o plano selecionado;
	 */
	@SuppressWarnings("unchecked")
	private void inicializarTelaNovaProposta() {
		planos = new ArrayList<PlanoVOView>();
		sexos = Sexo.listar();
		estadosCivis = EstadoCivil.obterLista();
		grausParentesco = GrauParentesco.listar();
		bancos = converteObjetoBancoParaLabelTela(Banco.listar());
		tiposCobranca = TipoCobranca.listar();
		formasPagamento = new ArrayList<LabelValueVO>();
		setBandeiras(new ArrayList<LabelValueVO>());
		tiposTelefone = TipoTelefone.listar();
		contasCorrente = (List<ContaCorrenteVO>) ServletActionContext.getRequest().getSession().getAttribute("contasCorrente");
		if (contasCorrente == null) {
			contasCorrente = new ArrayList<ContaCorrenteVO>();
		}

		CorretorVO corretor = obterDadosCorretorLogado();
		
		try{
			listaSucursais = corretorSucursalServiceFacade.obterSucursaisCorretor(corretor.getCpfCnpj());
			if(listaSucursais.isEmpty()){
			 addActionError("O Servi�o que obt�m os dados do Corretor encontra-se indispon�vel. Favor tente novamente mais tarde.");
			}
		}catch(Exception e){
			LOGGER.error(String.format("Erro ao listar sucursais: [%s]", e.getMessage()));
		}
	
		
		if (StringUtils.isNotBlank(proposta.getCodigo())) {
			try {
				tratarTokensCartaoCredito();
			} catch (Exception e) {
				LOGGER.error("Erro na gera��o do token do MIP.");
			}
		}
		corretorCartaoCreditoHabilitado = odontoprevServiceFacade.verificarCorretorHabilitado(proposta.getCorretor().getCpfCnpj());

	}

	@SuppressWarnings("unchecked")
	private void inicializarTelaNovaPropostaIntranet() {
		planos = new ArrayList<PlanoVOView>();
		sexos = Sexo.listar();
		estadosCivis = EstadoCivil.obterLista();
		grausParentesco = GrauParentesco.listar();
		bancos = converteObjetoBancoParaLabelTela(Banco.listar());
		tiposCobranca = TipoCobranca.listar();
		formasPagamento = new ArrayList<LabelValueVO>();
		setBandeiras(new ArrayList<LabelValueVO>());
		tiposTelefone = TipoTelefone.listar();
		contasCorrente = (List<ContaCorrenteVO>) ServletActionContext.getRequest().getSession().getAttribute("contasCorrente");
		if (contasCorrente == null) {
			contasCorrente = new ArrayList<ContaCorrenteVO>();
		}


		try {
			listaSucursais = corretorSucursalServiceFacade.obterSucursaisCorretor(proposta.getCorretor().getCpfCnpj());
		} catch (Exception e) {
			//e.printStackTrace();
			this.setMsg(e.getMessage());
		}

		if (StringUtils.isNotBlank(proposta.getCodigo())) {
			tratarTokensCartaoCredito();
		}
		corretorCartaoCreditoHabilitado = odontoprevServiceFacade.verificarCorretorHabilitado(proposta.getCorretor().getCpfCnpj());

	}

	
	public void tratarTokensCartaoCredito() {
		LOGGER.error("Tratar Token cartao de credito");
		if (FormaPagamento.CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento()) || FormaPagamento.BOLETO_E_CARTAO_CREDITO.getCodigo().equals(proposta.getPagamento().getFormaPagamento())) {
			if(proposta.getPagamento().getCartaoCredito().getAccessToken() == null){
				proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
			}
//			proposta.getPagamento().getCartaoCredito().setAccessToken("token-mock");
			setBandeiras(converterBandeirasParaEnum(odontoprevServiceFacade.listarBandeiras()));
			
			LOGGER.error("AcessToken: "+ proposta.getPagamento().getCartaoCredito().getAccessToken());
		} else {
			proposta.getPagamento().getCartaoCredito().setAccessToken("");
			proposta.getPagamento().getCartaoCredito().setPaymentToken("");
		}
		
//		proposta.getPagamento().getCartaoCredito().setPaymentToken("");

	}

	private List<LabelValueVO> converterBandeirasParaEnum(List<String> listarBandeiras) {
		if(listarBandeiras == null || listarBandeiras.isEmpty()){
			return null;
		}
		List<LabelValueVO> listaBandeirasEnum = new ArrayList<LabelValueVO>();
		Integer codigoBandeira = 0;
		for(String bandeira : listarBandeiras){
			codigoBandeira++;
			listaBandeirasEnum.add(new LabelValueVO(bandeira, bandeira));
		}
		return listaBandeirasEnum;
	}

	public String gerarAccessToken(){
		
		try{
			tratarTokensCartaoCredito();
		} catch (BusinessException e) {
			tratarErro(e);
			return SUCCESS;
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return SUCCESS;
	}
	/**
	 * Metodo responsavel pelo calculo do valor total (mensal e anual) da
	 * proposta. - Obter o plano selecionado em tela; - Calcular o valor total
	 * (mensal e anual) da proposta de acordo com o n�mero de vidas informado;
	 */
	private void calcularValorTotalDaProposta() {
		if (proposta.getSequencial() == null && (proposta.getPlano() == null || proposta.getPlano().getCodigo() == null || proposta.getPlano().getCodigo().longValue() == 0)) {
			valorTotalMensal = 0;
			valorTotalAnual = 0;
		} else if(numeroVidas != null && numeroVidas > 0){
			valorTotalMensal = proposta.getPlano().getValorPlanoVO().getValorMensalTitular() * (numeroVidas + 1);
			valorTotalAnual = proposta.getPlano().getValorPlanoVO().getValorAnualTitular() * (numeroVidas + 1);
		}else {
			valorTotalMensal = proposta.getPlano().getValorPlanoVO().getValorMensalTitular() * (proposta.getBeneficiarios().getDependentes().size() + 1);
			valorTotalAnual = proposta.getPlano().getValorPlanoVO().getValorAnualTitular() * (proposta.getBeneficiarios().getDependentes().size() + 1);
		}

	}
	
	private void calcularValorTotalDaPropostaNova() {
		if (proposta.getPlano().getCodigo() == null || proposta.getPlano().getCodigo().longValue() == 0 ){
			valorTotalMensal = 0;
			valorTotalAnual = 0;
		}else if(numeroVidas != null && numeroVidas > 0){
			valorTotalMensal = proposta.getPlano().getValorPlanoVO().getValorMensalTitular() * (numeroVidas );
			valorTotalAnual = proposta.getPlano().getValorPlanoVO().getValorAnualTitular() * (numeroVidas );
		}else {
			valorTotalMensal = proposta.getPlano().getValorPlanoVO().getValorMensalTitular() * (proposta.getBeneficiarios().getDependentes().size() + 1);
			valorTotalAnual = proposta.getPlano().getValorPlanoVO().getValorAnualTitular() * (proposta.getBeneficiarios().getDependentes().size() + 1);
		}
		/**
		 * Melhoria Incidente 41 e 51
		 */
		if(proposta.getSucursalSelecionada().getTipo()!= null){
			if(TipoSucursalSeguradora.REDE.getCodigo().equals(proposta.getSucursalSelecionada().getTipo().getCodigo())){
				alteraValorTotalMensal(proposta);
			}
		}
	}

	/**
	 * Metodo responsavel por obter o plano selecionado em tela. - Marcar o
	 * primeiro plano como o default; - Procurar o plano selecionado na lista de
	 * planos vigente.
	 */
	private void obterPlanoSeleciondo() {
		// if (planos != null && !planos.isEmpty()
		// && (proposta.getPlano().getCodigo() == null ||
		// proposta.getPlano().getCodigo().longValue() == 0)) {
		// proposta.setPlano(planos.get(0));
		// } else if (planos != null && !planos.isEmpty()) {
		// for (PlanoVO plano : planos) {
		// if (plano.getCodigo().equals(proposta.getPlano().getCodigo())) {
		// proposta.setPlano(plano);
		// break;
		// }
		// }
		// }
	}

	/**
	 * Retorna proposta.
	 *
	 * @return proposta - proposta.
	 */
	public PropostaVO getProposta() {
		return proposta;
	}

	/**
	 * Especifica proposta.
	 *
	 * @param proposta
	 *            - proposta.
	 */
	public void setProposta(PropostaVO proposta) {
		this.proposta = proposta;
	}

	/**
	 * Retorna numeroDependente.
	 *
	 * @return numeroDependente - numeroDependente.
	 */
	public int getNumeroDependente() {
		return numeroDependente;
	}

	/**
	 * Especifica numeroDependente.
	 *
	 * @param numeroDependente
	 *            - numeroDependente.
	 */
	public void setNumeroDependente(int numeroDependente) {
		this.numeroDependente = numeroDependente;
	}

	/**
	 * Retorna planos.
	 *
	 * @return planos - planos.
	 */
	public List<PlanoVOView> getPlanos() {
		return planos;
	}

	/**
	 * Especifica planos.
	 *
	 * @param planos
	 *            - planos.
	 */
	public void setPlanos(List<PlanoVOView> planos) {
		this.planos = planos;
	}

	/**
	 * Retorna valorTotalMensal.
	 *
	 * @return valorTotalMensal - valorTotalMensal.
	 */
	public double getValorTotalMensal() {
		return valorTotalMensal;
	}

	/**
	 * Especifica valorTotalMensal.
	 *
	 * @param valorTotalMensal
	 *            - valorTotalMensal.
	 */
	public void setValorTotalMensal(double valorTotalMensal) {
		this.valorTotalMensal = valorTotalMensal;
	}

	/**
	 * Retorna valorTotalAnual.
	 *
	 * @return valorTotalAnual - valorTotalAnual.
	 */
	public double getValorTotalAnual() {
		return valorTotalAnual;
	}

	/**
	 * Especifica valorTotalAnual.
	 *
	 * @param valorTotalAnual
	 *            - valorTotalAnual.
	 */
	public void setValorTotalAnual(double valorTotalAnual) {
		this.valorTotalAnual = valorTotalAnual;
	}

	/**
	 * Retorna cep.
	 *
	 * @return cep - cep.
	 */
	public String getCep() {
		return cep;
	}

	/**
	 * Especifica cep.
	 *
	 * @param cep
	 *            - cep.
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}

	/**
	 * Retorna indicadorCepDoTitular.
	 *
	 * @return indicadorCepDoTitular - indicadorCepDoTitular.
	 */
	public boolean isIndicadorCepDoTitular() {
		return indicadorCepDoTitular;
	}

	/**
	 * Especifica indicadorCepDoTitular.
	 *
	 * @param indicadorCepDoTitular
	 *            - indicadorCepDoTitular.
	 */
	public void setIndicadorCepDoTitular(boolean indicadorCepDoTitular) {
		this.indicadorCepDoTitular = indicadorCepDoTitular;
	}

	/**
	 * Retorna contasCorrente.
	 *
	 * @return contasCorrente - contasCorrente.
	 */
	public List<ContaCorrenteVO> getContasCorrente() {
		return contasCorrente;
	}

	/**
	 * Especifica contasCorrente.
	 *
	 * @param contasCorrente
	 *            - contasCorrente.
	 */
	public void setContasCorrente(List<ContaCorrenteVO> contasCorrente) {
		this.contasCorrente = contasCorrente;
	}

	/**
	 * Retorna contaCorrenteFormatada.
	 *
	 * @return contaCorrenteFormatada - contaCorrenteFormatada.
	 */
	public String getContaCorrenteFormatada() {
		return contaCorrenteFormatada;
	}

	/**
	 * Especifica contaCorrenteFormatada.
	 *
	 * @param contaCorrenteFormatada
	 *            - contaCorrenteFormatada.
	 */
	public void setContaCorrenteFormatada(String contaCorrenteFormatada) {
		this.contaCorrenteFormatada = contaCorrenteFormatada;
	}

	/**
	 * Retorna sexos.
	 *
	 * @return sexos - sexos.
	 */
	public List<Sexo> getSexos() {
		return sexos;
	}

	/**
	 * Especifica sexos.
	 *
	 * @param sexos
	 *            - sexos.
	 */
	public void setSexos(List<Sexo> sexos) {
		this.sexos = sexos;
	}

	

	public List<LabelValueVO> getEstadosCivis() {
		return estadosCivis;
	}

	public void setEstadosCivis(List<LabelValueVO> estadosCivis) {
		this.estadosCivis = estadosCivis;
	}

	/**
	 * Retorna grausParentesco.
	 *
	 * @return grausParentesco - grausParentesco.
	 */
	public List<GrauParentesco> getGrausParentesco() {
		return grausParentesco;
	}

	/**
	 * Especifica grausParentesco.
	 *
	 * @param grausParentesco
	 *            - grausParentesco.
	 */
	public void setGrausParentesco(List<GrauParentesco> grausParentesco) {
		this.grausParentesco = grausParentesco;
	}

	/**
	 * Retorna bancos.
	 *
	 * @return bancos - bancos.
	 */
	public List<LabelValueVO> getBancos() {
		return bancos;
	}

	/**
	 * Especifica bancos.
	 *
	 * @param bancos
	 *            - bancos.
	 */
	public void setBancos(List<LabelValueVO> bancos) {
		this.bancos = bancos;
	}

	/**
	 * Retorna tiposCobranca.
	 *
	 * @return tiposCobranca - tiposCobranca.
	 */
	public List<TipoCobranca> getTiposCobranca() {
		return tiposCobranca;
	}

	/**
	 * Especifica tiposCobranca.
	 *
	 * @param tiposCobranca
	 *            - tiposCobranca.
	 */
	public void setTiposCobranca(List<TipoCobranca> tiposCobranca) {
		this.tiposCobranca = tiposCobranca;
	}

	/**
	 * Retorna formasPagamento.
	 *
	 * @return formasPagamento - formasPagamento.
	 */
	public List<LabelValueVO> getFormasPagamento() {
		return formasPagamento;
	}

	/**
	 * Especifica formasPagamento.
	 *
	 * @param formasPagamento
	 *            - formasPagamento.
	 */
	public void setFormasPagamento(List<LabelValueVO> formasPagamento) {
		this.formasPagamento = formasPagamento;
	}

	/**
	 * Retorna tiposTelefone.
	 *
	 * @return tiposTelefone - tiposTelefone.
	 */
	public List<TipoTelefone> getTiposTelefone() {
		return tiposTelefone;
	}

	/**
	 * Especifica tiposTelefone.
	 *
	 * @param tiposTelefone
	 *            - tiposTelefone.
	 */
	public void setTiposTelefone(List<TipoTelefone> tiposTelefone) {
		this.tiposTelefone = tiposTelefone;
	}

	/**
	 * Retorna tiposBusca.
	 *
	 * @return tiposBusca - tiposBusca.
	 */
	public List<TipoBusca> getTiposBusca() {
		return tiposBusca;
	}

	/**
	 * Especifica tiposBusca.
	 *
	 * @param tiposBusca
	 *            - tiposBusca.
	 */
	public void setTiposBusca(List<TipoBusca> tiposBusca) {
		this.tiposBusca = tiposBusca;
	}

	/**
	 * Retorna filtroProposta.
	 *
	 * @return filtroProposta - filtroProposta.
	 */
	public FiltroPropostaVO getFiltroProposta() {
		return filtroProposta;
	}

	/**
	 * Especifica filtroProposta.
	 *
	 * @param filtroProposta
	 *            - filtroProposta.
	 */
	public void setFiltroProposta(FiltroPropostaVO filtroProposta) {
		this.filtroProposta = filtroProposta;
	}

	/**
	 * Retorna tiposCPFs.
	 *
	 * @return tiposCPFs - tiposCPFs.
	 */
	public List<TipoCPF> getTiposCPFs() {
		return tiposCPFs;
	}

	/**
	 * Especifica tiposCPFs.
	 *
	 * @param tiposCPFs
	 *            - tiposCPFs.
	 */
	public void setTiposCPFs(List<TipoCPF> tiposCPFs) {
		this.tiposCPFs = tiposCPFs;
	}

	/**
	 * Retorna status.
	 *
	 * @return status - status.
	 */
	public List<SituacaoProposta> getStatus() {
		return status;
	}

	/**
	 * Especifica status.
	 *
	 * @param status
	 *            - status.
	 */
	public void setStatus(List<SituacaoProposta> status) {
		this.status = status;
	}

	/**
	 * Retorna propostas.
	 *
	 * @return propostas - propostas.
	 */
	public List<PropostaVO> getPropostas() {
		return propostas;
	}

	/**
	 * Especifica propostas.
	 *
	 * @param propostas
	 *            - propostas.
	 */
	public void setPropostas(List<PropostaVO> propostas) {
		this.propostas = propostas;
	}

	/**
	 * Retorna indicativoDeCancelamentoDaProposta.
	 *
	 * @return indicativoDeCancelamentoDaProposta -
	 *         indicativoDeCancelamentoDaProposta
	 */
	public boolean isIndicativoDeCancelamentoDaProposta() {
		return indicativoDeCancelamentoDaProposta;
	}

	/**
	 * Especifica indicativoDeCancelamentoDaProposta.
	 *
	 * @param indicativoDeCancelamentoDaProposta
	 *            - indicativoDeCancelamentoDaProposta
	 */
	public void setIndicativoDeCancelamentoDaProposta(boolean indicativoDeCancelamentoDaProposta) {
		this.indicativoDeCancelamentoDaProposta = indicativoDeCancelamentoDaProposta;
	}

	public List<SucursalVO> getListaSucursais() {
		return listaSucursais;
	}

	public void setListaSucursais(List<SucursalVO> listaSucursais) {
		this.listaSucursais = listaSucursais;
	}

	/**
	 * 
	 */
	public String retornaTipoSucursal() {

		if (null != proposta.getSucursalSelecionada().getCpdSucursal()) {
			cpdEcodigo = proposta.getSucursalSelecionada().getCpdSucursal().split(";");
			proposta.getSucursalSelecionada().setCodigo(Integer.parseInt(cpdEcodigo[0]));
			proposta.getSucursalSelecionada().setCpd(Integer.parseInt(cpdEcodigo[1]));
		}
		if (serviceSucursal.validarSucursalCorporate(proposta.getSucursalSelecionada().getCodigo())) {
			proposta.getSucursalSelecionada().setTipo(TipoSucursalSeguradora.CORPORATE);
		} else if (serviceSucursal.validarSucursalMercado(proposta.getSucursalSelecionada().getCodigo())) {
			proposta.getSucursalSelecionada().setTipo(TipoSucursalSeguradora.MERCADO);
		} else if (serviceSucursal.validarSucursalRede(proposta.getSucursalSelecionada().getCodigo())) {
			proposta.getSucursalSelecionada().setTipo(TipoSucursalSeguradora.REDE);
		}
		
		proposta.setCorretor(obterDadosCorretorLogado());


			//corretorCartaoCreditoHabilitado = odontoprevServiceFacade.verificarCorretorHabilitado(proposta.getCorretor().getCpfCnpj());
			formasPagamento = FormaPagamento.converterEnumEmLabel(FormaPagamento.listarFormaPagamento(obterDadosCorretorLogado(), proposta.getSucursalSelecionada().getTipo().getNome(), corretorCartaoCreditoHabilitado));
			corretorCartaoCreditoHabilitado = odontoprevServiceFacade.verificarCorretorHabilitado(proposta.getCorretor().getCpfCnpj());
			/*formasPagamento = FormaPagamento.converterEnumEmLabel(FormaPagamento.listarFormaPagamento(obterDadosCorretorLogado(), proposta.getSucursalSelecionada().getTipo().getNome(), corretorCartaoCreditoHabilitado));*/
	

		List<PlanoVO> listaPlanos = null;
		if(proposta.getSequencial() == null){
			listaPlanos = planoServiceFacade.listarPlanoDisponivelParaVenda(proposta.getSucursalSelecionada().getTipo().name(), proposta.getPagamento());
		
		}else{
			PropostaVO propostaTemporaria = propostaServiceFacade.obterPropostaPorSequencial(proposta.getSequencial());
			proposta.setPlano(propostaTemporaria.getPlano());
			listaPlanos = new ArrayList<PlanoVO>();
			listaPlanos.add(proposta.getPlano());
		}
			
		planos = converteObjetoPlanoParaTela(listaPlanos,proposta.getSucursalSelecionada().getTipo());

		return SUCCESS;
	}

//	private TipoSucursalSeguradora obterTipoSucursalPorCodigo(Integer codigo) {
//
//		if (serviceSucursal.validarSucursalCorporate(codigo)) {
//			proposta.getSucursalSelecionada().setNome("CORPORATE");
//			return TipoSucursalSeguradora.CORPORATE;
//		} else if (serviceSucursal.validarSucursalMercado(codigo)) {
//			proposta.getSucursalSelecionada().setNome("MERCADO");
//			return TipoSucursalSeguradora.MERCADO;
//		} else if (serviceSucursal.validarSucursalRede(codigo)) {
//			proposta.getSucursalSelecionada().setNome("REDE");
//			return TipoSucursalSeguradora.REDE;
//		}
//		return null;
//	}

	public String obterFormasPagamentoEListaBancosDeAcordoComSegmentoEPlano() {
		codigoPlano = proposta.getPlano().getCodigo();
		
		 if (null != proposta.getSucursalSelecionada().getCpdSucursal()) {
			proposta.setSucursalSelecionada(formatarSucursalSelecionada(proposta.getSucursalSelecionada()));
		}
		proposta.getSucursalSelecionada().setTipo(serviceSucursal.obterTipoSucursalPorCodigo(proposta.getSucursalSelecionada().getCodigo()));
		proposta.getSucursalSelecionada().setNome(proposta.getSucursalSelecionada().getTipo().getNome());

		if (proposta.getPlano().getCodigo() == 3 || proposta.getPlano().getCodigo() == 15 || proposta.getPlano().getCodigo() == 20) {
			if (Constantes.REDE.equalsIgnoreCase(proposta.getSucursalSelecionada().getNome()) || Constantes.MERCADO.equalsIgnoreCase(proposta.getSucursalSelecionada().getNome()) || Constantes.CORPORATE.equalsIgnoreCase(proposta.getSucursalSelecionada().getNome())) {
				corretorCartaoCreditoHabilitado = odontoprevServiceFacade.verificarCorretorHabilitado(proposta.getCorretor().getCpfCnpj());
				formasPagamento = FormaPagamento.converterEnumEmLabel(FormaPagamento.listarFormaPagamento(obterDadosCorretorLogado(), proposta.getSucursalSelecionada().getTipo().getNome(), corretorCartaoCreditoHabilitado));
			} else {
				formasPagamento = FormaPagamento.converterEnumEmLabel(FormaPagamento.obterListaFormaPagamentoCorporateSemBoleto());
			}
		} else if(Constantes.CORPORATE.equalsIgnoreCase(proposta.getSucursalSelecionada().getNome()) && proposta.getPlano().getCodigo() != 3L && proposta.getPlano().getCodigo() != 15L && proposta.getPlano().getCodigo() != 20L) {
			formasPagamento = FormaPagamento.converterEnumEmLabel(FormaPagamento.obterListaFormaPagamentoCorporateSemBoleto());
		} else {
			corretorCartaoCreditoHabilitado = odontoprevServiceFacade.verificarCorretorHabilitado(proposta.getCorretor().getCpfCnpj());
			formasPagamento = FormaPagamento.converterEnumEmLabel(FormaPagamento.listarFormaPagamento(obterDadosCorretorLogado(), proposta.getSucursalSelecionada().getTipo().getNome(), corretorCartaoCreditoHabilitado));
		}
		
		bancos = converteObjetoBancoParaLabelTela(Banco.obterListaDeBancoPorSegmento(proposta.getSucursalSelecionada().getTipo().getNome(), proposta.getPlano().getCodigo().intValue()));

		if (proposta.getPlano().getCodigo() != null) {
			LOGGER.error("Codigo do plano: " + codigoPlano);
			ServletActionContext.getRequest().getSession().setAttribute("plano", codigoPlano);
		}else{
			LOGGER.error("Codigo do plano: " + codigoPlano);
			codigoPlano = (Long) ServletActionContext.getRequest().getSession().getAttribute(Constantes.PLANO);
		}
		proposta.setPlano(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlano().getCodigo()));
		calcularValorTotalDaPropostaNova();

		return SUCCESS;
	}

	private SucursalVO formatarSucursalSelecionada(SucursalVO sucursal) {
		SucursalVO sucursalFormatada = new SucursalVO();
		sucursalFormatada.setCpdSucursal(sucursal.getCpdSucursal());
		cpdEcodigo = sucursal.getCpdSucursal().split(";");
		if(cpdEcodigo.length == 2) {
			sucursalFormatada.setCodigo(Integer.parseInt(cpdEcodigo[0]));
			sucursalFormatada.setCpd(Integer.parseInt(cpdEcodigo[1]));
		}
			
		sucursalFormatada.setTipo(sucursal.getTipo());
		
		return sucursalFormatada;
	}

	private List<LabelValueVO> converteObjetoBancoParaLabelTela(List<Banco> listaBancos) {

		List<LabelValueVO> listaBancosTela = new ArrayList<LabelValueVO>();

		if (listaBancos == null) {
			return listaBancosTela;
		}

		for (Banco banco : listaBancos) {

			LabelValueVO bancoTela = new LabelValueVO(String.valueOf(banco.getCodigo()), String.valueOf(banco.getDescricao()));
			listaBancosTela.add(bancoTela);

		}

		return listaBancosTela;

	}

	private List<PlanoVOView> converteObjetoPlanoParaTela(List<PlanoVO> listaPlanoVO,TipoSucursalSeguradora tipoSucursal) {

		List<PlanoVOView> listaPlanoView = new ArrayList<PlanoVOView>();
		if (listaPlanoVO == null) {
			return listaPlanoView;
		}
		for (PlanoVO plano : listaPlanoVO) {

			PlanoVOView planoView = new PlanoVOView();
			planoView.setCodigo(String.valueOf(plano.getCodigo()));
			planoView.setNome(plano.getNome());
			planoView.setDescricaoCarenciaPeriodoAnual(plano.getDescricaoCarenciaPeriodoAnual());
			planoView.setDescricaoCarenciaPeriodoMensal(plano.getDescricaoCarenciaPeriodoMensal());
			planoView.setValorPlanoVO(new ValorPlanoViewVO());
			ValorPlanoViewVO valorView = new ValorPlanoViewVO();
			valorView.setSequencial(String.valueOf(plano.getValorPlanoVO().getSequencial()));
			valorView.setValorAnualDependente(String.valueOf(plano.getValorPlanoVO().getValorAnualDependente()));
			valorView.setValorAnualTitular(String.valueOf(plano.getValorPlanoVO().getValorAnualTitular()));
			valorView.setValorMensalDependente(String.valueOf(plano.getValorPlanoVO().getValorMensalDependente()));
			valorView.setValorMensalTitular(String.valueOf(plano.getValorPlanoVO().getValorMensalTitular()));
			
			if(tipoSucursal.getCodigo() == 2){
				verificarValorMensal(planoView,valorView);
			}
			planoView.setValorPlanoVO(valorView);
			listaPlanoView.add(planoView);

		}

		return listaPlanoView;
	}
	/**
	 * Criada a verifica��o dos plano de codigo:5, 6, 7, 16, 17, 21 e 22 para n�o apresentar o valor
	 * mensal dos planos 
	 *
	 * author: Mateus Ribeiro\Roberto Freire
	 */
	public void verificarValorMensal(PlanoVOView planoView, ValorPlanoViewVO valorView ){
		if(planoView.getCodigo().equals("5") || planoView.getCodigo().equals("6") || planoView.getCodigo().equals("7") ||
		   planoView.getCodigo().equals("16")|| planoView.getCodigo().equals("17") || planoView.getCodigo().equals("21") ||
		   planoView.getCodigo().equals("22")){
			valorView.setValorMensalTitular("0.0");
			valorView.setValorMensalDependente("0.0");
			planoView.setDescricaoCarenciaPeriodoMensal("N�O DISPONIVEL PARA COMERCIALIZA��O MODALIDADE MENSAL");
		}
	}
	
	/**
	 * Criada a verifica��o dos plano de codigo:5, 6, 7, 16, 17, 21 e 22 para n�o apresentar o valor
	 * mensal dos planos 
	 *
	 * author: Roberto Freire
	 */
	public void alteraValorTotalMensal(PropostaVO proposta){
		if(proposta.getPlano().getCodigo() == 5|| proposta.getPlano().getCodigo() == 6 ||
				proposta.getPlano().getCodigo() == 7 || proposta.getPlano().getCodigo() == 16 || proposta.getPlano().getCodigo() == 17 ||
				proposta.getPlano().getCodigo() == 21 || proposta.getPlano().getCodigo() == 22){
			valorTotalMensal = 0.0;
			proposta.getPlano().getValorPlanoVO().setValorMensalTitular(0.0);
			proposta.getPlano().getValorPlanoVO().setValorMensalDependente(0.0);
		}
	}

	public String obterPlanos() {
		// planos =
		// planoServiceFacade.listarPlanoDisponivelParaVenda(tipoSucursal,
		// proposta.getPagamento());
		// inicializarTelaNovaProposta();
		return SUCCESS;
	}

	/**
	 * @return
	 */
	public String retornaDadosCorretor() {

		try{
			if (null != proposta.getSucursalSelecionada().getCpdSucursal()) {
				cpdEcodigo = proposta.getSucursalSelecionada().getCpdSucursal().split(";");
				proposta.getSucursalSelecionada().setCodigo(Integer.parseInt(cpdEcodigo[0]));
				proposta.getSucursalSelecionada().setCpd(Integer.parseInt(cpdEcodigo[1]));
			}
	
			proposta.getCorretor().setSucursal(proposta.getSucursalSelecionada().getCodigo());
			CorretorVO corretorTemporario = dadosCorretorFacade.listarDadosCorretor(proposta.getCorretor());
	
			if (null != corretorTemporario) {
				proposta.getCorretorMaster().setNome(corretorTemporario.getNome());
				proposta.getCorretorMaster().setCpd(corretorTemporario.getCpd());
			}
		} catch (BusinessException e) {
			LOGGER.error("Erro ao  retornar dados de corretor: " + e.getMessage());
			addActionError("CPD do Corretor Master � inv�lido para a sucursal selecionada.");
			//tratarErro(e);
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error("Erro ao  retornar dados de corretor: " + e.getMessage());
			
			return SUCCESS;
		}

		return SUCCESS;
	}

	/**
	 * @return
	 */
	public String retornaDadosAngariador() {

		try{
			if (null != proposta.getSucursalSelecionada().getCpdSucursal()) {
				cpdEcodigo = proposta.getSucursalSelecionada().getCpdSucursal().split(";");
				proposta.getSucursalSelecionada().setCodigo(Integer.parseInt(cpdEcodigo[0]));
				proposta.getSucursalSelecionada().setCpd(Integer.parseInt(cpdEcodigo[1]));
			}
	
			proposta.getCorretor().setSucursal(proposta.getSucursalSelecionada().getCodigo());
			proposta.getCorretor().setCpd(proposta.getSucursalSelecionada().getCpd().longValue());
			CorretorVO angariadorTemporario = dadosCorretorFacade.listarDadosCorretor(proposta.getCorretor());
	
			if (null != angariadorTemporario) {
				proposta.getAngariador().setNome(angariadorTemporario.getNome());
				proposta.getAngariador().setCpd(angariadorTemporario.getCpd().intValue());
			}
		}catch (BusinessException e) {
			LOGGER.error("Erro ao  retornar dados de angariador: " + e.getMessage());
			tratarErro(e);
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error("Erro ao  retornar dados de angariador: " + e.getMessage());
			addActionError(e.getMessage());
			return SUCCESS;
		}

		return SUCCESS;
	}
	
	
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}


	
	
	private String codigoPropostaFormatado(String codigo) {
		StringBuilder codigoProposta = new StringBuilder(codigo);
		codigoProposta.insert(codigo.length() - 1, '-');
		return codigoProposta.toString();
	}

	public String getTipoSucursal() {
		return tipoSucursal;
	}

	public void setTipoSucursal(String tipoSucursal) {
		this.tipoSucursal = tipoSucursal;
	}

//	public List<String> getBandeiras() {
//		return bandeiras;
//	}
//
//	public void setBandeiras(List<String> bandeiras) {
//		this.bandeiras = bandeiras;
//	}
	
	public List<LabelValueVO> getBandeiras() {
		return bandeiras;
	}

	public void setBandeiras(List<LabelValueVO> bandeiras) {
		this.bandeiras = bandeiras;
	}
	
	public Long getCodigoPlano() {
		return codigoPlano;
	}

	public void setCodigoPlano(Long codigoPlano) {
		this.codigoPlano = codigoPlano;
	}

	public String getNomePagante() {
		return nomePagante;
	}

	public void setNomePagante(String nomePagante) {
		this.nomePagante = nomePagante;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getTamanhoArquivo() {
		return tamanhoArquivo;
	}

	public void setTamanhoArquivo(int tamanhoArquivo) {
		this.tamanhoArquivo = tamanhoArquivo;
	}

	public Integer getFormaPagamentoSelecionada() {
		return formaPagamentoSelecionada;
	}

	public void setFormaPagamentoSelecionada(Integer formaPagamentoSelecionada) {
		this.formaPagamentoSelecionada = formaPagamentoSelecionada;
	}

	public Integer getNumeroVidas() {
		return numeroVidas;
	}

	public void setNumeroVidas(Integer numeroVidas) {
		this.numeroVidas = numeroVidas;
	}

	public List<CanalVO> getListaCanal() {
		return listaCanal;
	}

	public void setListaCanal(List<CanalVO> listaCanal) {
		this.listaCanal = listaCanal;
	}

	public Date getDataEmissao() {
		if(proposta.getDataEmissao() != null){
			dataEmissao = proposta.getDataEmissao().toDateTimeAtStartOfDay().toDate();
		}
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Date getDataValidade() {
		if(proposta.getDataValidadeProposta() != null){
			dataValidade = proposta.getDataValidadeProposta().toDateTimeAtStartOfDay().toDate();
		}
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}
	
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	/**
	 * @return the colunas
	 */
	public List<String> getColunas() {
		return colunas;
	}

	/**
	 * @param colunas the colunas to set
	 */
	public void setColunas(List<String> colunas) {
		this.colunas = colunas;
	}




	/*
	 * public SucursalVO getSucursalSelecionada() { return sucursalSelecionada;
	 * }
	 * 
	 * public void setSucursalSelecionada(SucursalVO sucursalSelecionada) {
	 * this.sucursalSelecionada = sucursalSelecionada; }
	 */
	private void setTesteProposta(){
//		proposta.getBeneficiarios().getTitular().setCpf("");
		proposta.getBeneficiarios().getTitular().setDataNascimento(new LocalDate("1990-01-01"));
		proposta.getBeneficiarios().getTitular().setEmail("email@email.com");
		proposta.getBeneficiarios().getTitular().setEndereco(new EnderecoVO());
		proposta.getBeneficiarios().getTitular().getEndereco().setCep("20091007");
		proposta.getBeneficiarios().getTitular().getEndereco().setNumero(123L);
		proposta.getBeneficiarios().getTitular().setNome("nome teste titular");
		proposta.getBeneficiarios().getTitular().setNomeMae("nome teste mae");
		proposta.getBeneficiarios().getTitular().setSexo("F");
		proposta.getBeneficiarios().getTitular().getTelefones().get(0).setDddEnumero("2198128465");
		proposta.getBeneficiarios().getTitular().getTelefones().get(0).setTipoTelefone("3");
		proposta.getCanal().setCodigo(1);
		proposta.getCorretor().setCpfCnpj("01021899000191");
		proposta.getCorretor().setNome("BRADESCOR COR DE SEGUROS LTDA");
		proposta.getCorretor().setAssistenteProducao(1234567L);
		proposta.setDataEmissao(new LocalDate("2017-07-28"));
		proposta.setDataValidadeProposta(new LocalDate("2017-10-26"));
//		proposta.getPagamento().getCartaoCredito().setAccessToken("YWI5OWI2ZjYtOTY3YS00ZGQyLWE3YmMtYzY0ZjY5MGI2MGI3LTE5MTg1NTEwMT");
//		proposta.getPagamento().getCartaoCredito().setBandeira("Master");
//		proposta.getPagamento().getCartaoCredito().setNomeCartao("nome cartao");
//		proposta.getPagamento().getCartaoCredito().setNumeroCartao("5197685977315654");
//		proposta.getPagamento().getCartaoCredito().setPaymentToken("bb821519-901a-4969-b011-33ec0cf177f4");
//		proposta.getPagamento().getCartaoCredito().setValidade("01/2020");
		proposta.getPagamento().setCodigoTipoCobranca(1);
		proposta.getPagamento().setFormaPagamento(5);
		proposta.getPlano().setCodigo(3L);
		proposta.setSucursalSelecionada(new SucursalVO());
		proposta.getSucursalSelecionada().setCpdSucursal("911;401462");
		
	}
}
