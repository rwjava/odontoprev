package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

/**
 * Classe responsavel pelo transporte de dados do produtor.
 *
 */
public class ProdutorVO implements Serializable {

	private static final long serialVersionUID = 1817789260056445482L;
	
	private String nome;
	private String cpfCnpj;
    private TipoProdutor tipoProdutor;

	
	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * Retorna cpfCnpj.
	 *
	 * @return cpfCnpj - cpfCnpj.
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	/**
	 * Especifica cpfCnpj.
	 *
	 * @param cpfCnpj - cpfCnpj.
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public TipoProdutor getTipoProdutor() {
		return tipoProdutor;
	}
	public void setTipoProdutor(TipoProdutor tipoProdutor) {
		this.tipoProdutor = tipoProdutor;
	}


}
