package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class PlanoVOView implements Serializable {

	private static final long serialVersionUID = -927266083674927967L;
	private String codigo;
	private String descricaoCarenciaPeriodoAnual;
	private String descricaoCarenciaPeriodoMensal;
	private String nome;
	private ValorPlanoViewVO valorPlanoVO;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricaoCarenciaPeriodoAnual() {
		return descricaoCarenciaPeriodoAnual;
	}

	public void setDescricaoCarenciaPeriodoAnual(String descricaoCarenciaPeriodoAnual) {
		this.descricaoCarenciaPeriodoAnual = descricaoCarenciaPeriodoAnual;
	}

	public String getDescricaoCarenciaPeriodoMensal() {
		return descricaoCarenciaPeriodoMensal;
	}

	public void setDescricaoCarenciaPeriodoMensal(String descricaoCarenciaPeriodoMensal) {
		this.descricaoCarenciaPeriodoMensal = descricaoCarenciaPeriodoMensal;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ValorPlanoViewVO getValorPlanoVO() {
		return valorPlanoVO;
	}

	public void setValorPlanoVO(ValorPlanoViewVO valorPlanoVO) {
		this.valorPlanoVO = valorPlanoVO;
	}

}
