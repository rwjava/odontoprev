package br.com.bradseg.eedi.dentalindividual.vo;

/**
 * Classe responsavel por transportar os dados do beneficiário dependente.
 *
 */
public class DependenteVO extends PessoaVO {

	private static final long serialVersionUID = -4090451911390720399L;

	private GrauParentescoVO grauParentesco;
	private GrauParentescoVO grauParentescoBeneficiario;
	private String cns;
	private String dnv;
	

	public DependenteVO() {
		grauParentesco = new GrauParentescoVO();
	}
	

	public GrauParentescoVO getGrauParentesco() {
		return grauParentesco;
	}


	public void setGrauParentesco(GrauParentescoVO grauParentesco) {
		this.grauParentesco = grauParentesco;
	}


	/**
	 * Retorna cns.
	 *
	 * @return cns - cns.
	 */
	public String getCns() {
		return cns;
	}
	/**
	 * Especifica cns.
	 *
	 * @param cns - cns.
	 */
	public void setCns(String cns) {
		this.cns = cns;
	}
	/**
	 * Retorna dnv.
	 *
	 * @return dnv - dnv.
	 */
	public String getDnv() {
		return dnv;
	}
	/**
	 * Especifica dnv.
	 *
	 * @param dnv - dnv.
	 */
	public void setDnv(String dnv) {
		this.dnv = dnv;
	}


	public GrauParentescoVO getGrauParentescoBeneficiario() {
		return grauParentescoBeneficiario;
	}


	public void setGrauParentescoBeneficiario(GrauParentescoVO grauParentescoBeneficiario) {
		this.grauParentescoBeneficiario = grauParentescoBeneficiario;
	}


}
