package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

public class CartaoCreditoVO implements Serializable {

	private static final long serialVersionUID = 7561406449971836563L;

	private String nomeCartao;
	private LocalDate dataNascimento; 
	private String cpfCartao;
	private String numeroCartao;
	private String validade;
	private String bandeira;
	private String paymentToken;
	private String accessToken;
	private String mensagemRetorno;

	/**
	 * Retorna nomeCartao.
	 *
	 * @return nomeCartao - nomeCartao
	 */
	public String getNomeCartao() {
		return nomeCartao;
	}

	/**
	 * Especifica nomeCartao.
	 *
	 * @param nomeCartao - nomeCartao
	 */
	public void setNomeCartao(String nomeCartao) {
		this.nomeCartao = nomeCartao;
	}

	/**
	 * Retorna cpfCartao.
	 *
	 * @return cpfCartao - cpfCartao
	 */
	public String getCpfCartao() {
		return cpfCartao;
	}

	/**
	 * Especifica cpfCartao.
	 *
	 * @param cpfCartao - cpfCartao
	 */
	public void setCpfCartao(String cpfCartao) {
		this.cpfCartao = cpfCartao;
	}

	/**
	 * Retorna numeroCartao.
	 *
	 * @return numeroCartao - numeroCartao
	 */
	public String getNumeroCartao() {
		return numeroCartao;
	}

	/**
	 * Especifica numeroCartao.
	 *
	 * @param numeroCartao - numeroCartao
	 */
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	/**
	 * Retorna validade.
	 *
	 * @return validade - validade
	 */
	public String getValidade() {
		return validade;
	}

	/**
	 * Especifica validade.
	 *
	 * @param validade - validade
	 */
	public void setValidade(String validade) {
		this.validade = validade;
	}

	/**
	 * Retorna bandeira.
	 *
	 * @return bandeira - bandeira
	 */
	public String getBandeira() {
		return bandeira;
	}

	/**
	 * Especifica bandeira.
	 *
	 * @param bandeira - bandeira
	 */
	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	/**
	 * Retorna paymentToken.
	 *
	 * @return paymentToken - paymentToken
	 */
	public String getPaymentToken() {
		return paymentToken;
	}

	/**
	 * Especifica paymentToken.
	 *
	 * @param paymentToken - paymentToken
	 */
	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}

	/**
	 * Retorna accessToken.
	 *
	 * @return accessToken - accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * Especifica accessToken.
	 *
	 * @param accessToken - accessToken
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * Retorna mensagemRetorno.
	 *
	 * @return mensagemRetorno - mensagemRetorno
	 */
	public String getMensagemRetorno() {
		return mensagemRetorno;
	}

	/**
	 * Especifica mensagemRetorno.
	 *
	 * @param mensagemRetorno - mensagemRetorno
	 */
	public void setMensagemRetorno(String mensagemRetorno) {
		this.mensagemRetorno = mensagemRetorno;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
