package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

/**
 * Classe responsavel pelo transporte de dados da conta corrente.
 */
public class ContaCorrenteVO implements Serializable {

	private static final long serialVersionUID = 7047327400082288350L;

	private BancoVO banco;
	private String numeroAgencia;
	private String digitoVerificadorAgencia;
	private String numeroConta;
	private String digitoVerificadorConta;
	private String cpf;
	private String nome;
	private LocalDate dataNascimento;
	private String email;
	private AgenciaProdutoraVO agenciaProdutoraVO;

	public ContaCorrenteVO() {
		banco = new BancoVO();
	}

	public BancoVO getBanco() {
		return banco;
	}

	public void setBanco(BancoVO banco) {
		this.banco = banco;
	}

	public String getNumeroAgencia() {
		if (numeroAgencia != null) {
			return numeroAgencia.replaceAll("[,;\\s]", "");
		}
		return numeroAgencia;
	}

	public void setNumeroAgencia(String numeroAgencia) {
		this.numeroAgencia = numeroAgencia;
	}

	public String getNumeroConta() {
		if (numeroConta != null) {
			return numeroConta.replaceAll("[,;\\s]", "");
		}
		return numeroConta;
	}

	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}

	/**
	 * Retorna digitoVerificadorConta.
	 *
	 * @return digitoVerificadorConta - digitoVerificadorConta.
	 */
	public String getDigitoVerificadorConta() {
		return digitoVerificadorConta;
	}

	/**
	 * Especifica digitoVerificadorConta.
	 *
	 * @param digitoVerificadorConta - digitoVerificadorConta.
	 */
	public void setDigitoVerificadorConta(String digitoVerificadorConta) {
		this.digitoVerificadorConta = digitoVerificadorConta;
	}

	/**
	 * Retorna cpf.
	 *
	 * @return cpf - cpf.
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Especifica cpf.
	 *
	 * @param cpf - cpf.
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Retorna dataNascimento.
	 *
	 * @return dataNascimento - dataNascimento.
	 */
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	/**
	 * Especifica dataNascimento.
	 *
	 * @param dataNascimento - dataNascimento.
	 */
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	/**
	 * Retorna a conta corrente formatada.
	 * 
	 * @return String - conta corrente formatada no padr�o: Agencia / Conta - DV.
	 */
	public String getContaCorrenteFormatada() {
		StringBuilder contaCorrenteFormatada = new StringBuilder();

		contaCorrenteFormatada.append(this.getNumeroAgencia()).append("/").append(this.getNumeroConta()).append("-")
				.append(this.digitoVerificadorConta);

		return contaCorrenteFormatada.toString();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public AgenciaProdutoraVO getAgenciaProdutoraVO() {
		return agenciaProdutoraVO;
	}

	public void setAgenciaProdutoraVO(AgenciaProdutoraVO agenciaProdutoraVO) {
		this.agenciaProdutoraVO = agenciaProdutoraVO;
	}

	public String getDigitoVerificadorAgencia() {
		return digitoVerificadorAgencia;
	}

	public void setDigitoVerificadorAgencia(String digitoVerificadorAgencia) {
		this.digitoVerificadorAgencia = digitoVerificadorAgencia;
	}
}
