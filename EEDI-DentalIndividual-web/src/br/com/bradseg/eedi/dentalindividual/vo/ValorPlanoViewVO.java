package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class ValorPlanoViewVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 512585067724295697L;
	private String sequencial;
	private String valorAnualTitular;
	private String valorAnualDependente;
	private String valorMensalTitular;
	private String valorMensalDependente;
	private String valorTaxa;
	private String valorDesconto;

	public String getSequencial() {
		return sequencial;
	}

	public void setSequencial(String sequencial) {
		this.sequencial = sequencial;
	}

	public String getValorAnualTitular() {
		return valorAnualTitular;
	}

	public void setValorAnualTitular(String valorAnualTitular) {
		this.valorAnualTitular = valorAnualTitular;
	}

	public String getValorAnualDependente() {
		return valorAnualDependente;
	}

	public void setValorAnualDependente(String valorAnualDependente) {
		this.valorAnualDependente = valorAnualDependente;
	}

	public String getValorMensalTitular() {
		return valorMensalTitular;
	}

	public void setValorMensalTitular(String valorMensalTitular) {
		this.valorMensalTitular = valorMensalTitular;
	}

	public String getValorMensalDependente() {
		return valorMensalDependente;
	}

	public void setValorMensalDependente(String valorMensalDependente) {
		this.valorMensalDependente = valorMensalDependente;
	}

	public String getValorTaxa() {
		return valorTaxa;
	}

	public void setValorTaxa(String valorTaxa) {
		this.valorTaxa = valorTaxa;
	}

	public String getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(String valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

}
