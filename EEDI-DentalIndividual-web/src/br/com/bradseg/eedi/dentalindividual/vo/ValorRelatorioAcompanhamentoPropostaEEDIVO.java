package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ValorRelatorioAcompanhamentoPropostaEEDIVO implements Serializable{

    private static final long serialVersionUID = -3803625196566661273L;
    
    private BigDecimal valorTotalProposta;
    private BigDecimal valorTotalPropostaPorSucursal;
    private BigDecimal valorTotalPropostaPorCorretor;
    private BigDecimal valorTotalPropostaPorAgenciaDebito;
    private BigDecimal valorTotalPropostaPorAgenciaProdutora;
    private BigDecimal valorTotalPropostaPorAssistenteProducao;
    private BigDecimal valorTotalPropostaPorGerenteProdutoBVP;
    
    
    
    public BigDecimal getValorTotalProposta() {
        return valorTotalProposta;
    }
    public void setValorTotalProposta(BigDecimal valorTotalProposta) {
        this.valorTotalProposta = valorTotalProposta;
    }
    public BigDecimal getValorTotalPropostaPorSucursal() {
        return valorTotalPropostaPorSucursal;
    }
    public void setValorTotalPropostaPorSucursal(
    	BigDecimal valorTotalPropostaPorSucursal) {
        this.valorTotalPropostaPorSucursal = valorTotalPropostaPorSucursal;
    }
    public BigDecimal getValorTotalPropostaPorCorretor() {
        return valorTotalPropostaPorCorretor;
    }
    public void setValorTotalPropostaPorCorretor(
    	BigDecimal valorTotalPropostaPorCorretor) {
        this.valorTotalPropostaPorCorretor = valorTotalPropostaPorCorretor;
    }
    public BigDecimal getValorTotalPropostaPorAgenciaDebito() {
        return valorTotalPropostaPorAgenciaDebito;
    }
    public void setValorTotalPropostaPorAgenciaDebito(
    	BigDecimal valorTotalPropostaPorAgenciaDebito) {
        this.valorTotalPropostaPorAgenciaDebito = valorTotalPropostaPorAgenciaDebito;
    }
    public BigDecimal getValorTotalPropostaPorAgenciaProdutora() {
        return valorTotalPropostaPorAgenciaProdutora;
    }
    public void setValorTotalPropostaPorAgenciaProdutora(
    	BigDecimal valorTotalPropostaPorAgenciaProdutora) {
        this.valorTotalPropostaPorAgenciaProdutora = valorTotalPropostaPorAgenciaProdutora;
    }
    public BigDecimal getValorTotalPropostaPorAssistenteProducao() {
        return valorTotalPropostaPorAssistenteProducao;
    }
    public void setValorTotalPropostaPorAssistenteProducao(
    	BigDecimal valorTotalPropostaPorAssistenteProducao) {
        this.valorTotalPropostaPorAssistenteProducao = valorTotalPropostaPorAssistenteProducao;
    }
    public BigDecimal getValorTotalPropostaPorGerenteProdutoBVP() {
        return valorTotalPropostaPorGerenteProdutoBVP;
    }
    public void setValorTotalPropostaPorGerenteProdutoBVP(
    	BigDecimal valorTotalPropostaPorGerenteProdutoBVP) {
        this.valorTotalPropostaPorGerenteProdutoBVP = valorTotalPropostaPorGerenteProdutoBVP;
    }

    
    
    
}
