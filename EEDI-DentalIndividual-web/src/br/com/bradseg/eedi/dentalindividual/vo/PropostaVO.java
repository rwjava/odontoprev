package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;

import br.com.bradseg.bsad.framework.core.text.formatter.MaskFormatter;
//import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Classe responsavel pelo transporte de dados da proposta.
 */
public class PropostaVO implements Serializable {

	private static final long serialVersionUID = 8739020366500747731L;

	private Long sequencial;
	private String codigo;
	private String codigoFormatado;
	private CorretorVO corretor;
	private PlanoVO plano;
	private BeneficiariosVO beneficiarios;
	private PagamentoVO pagamento;
	private String status;
	private CanalVO canal;
	private LocalDate dataEmissao;
	private LocalDate dataValidadeProposta;
	private String codigoMatriculaGerente;
	private String codigoMatriculaAssistente;
	private AngariadorVO angariador;
	private CorretorMasterVO corretorMaster;
	private SucursalVO sucursalSelecionada;
	private Integer tipoComissao;
	private List<DependenteVO> dependentes;
	private LocalDate dataInicioCobranca;
	private Long codigoPostoAtendimento;

	
	/**
	 * Construtor padr�o.
	 */
	public PropostaVO() {
		beneficiarios = new BeneficiariosVO();
		pagamento = new PagamentoVO();
		plano = new PlanoVO();
		plano.setValorPlanoVO(new ValorPlanoVO());
		canal = new CanalVO();
		corretor = new CorretorVO();
		setAngariador(new AngariadorVO());
		setCorretorMaster(new CorretorMasterVO());
		dataEmissao = new LocalDate();
//		dataValidadeProposta = new LocalDate();
		sucursalSelecionada = new SucursalVO();
		dependentes = new ArrayList<DependenteVO>();
	}

	/**
	 * Retorna sequencial.
	 *
	 * @return sequencial - sequencial.
	 */
	public Long getSequencial() {
		return sequencial;
	}

	/**
	 * Especifica sequencial.
	 *
	 * @param sequencial - sequencial.
	 */
	public void setSequencial(Long sequencial) {
		this.sequencial = sequencial;
	}
	
	private void formatarCodigoProposta(String codigo) {
		MaskFormatter mf = new MaskFormatter("***###########-#");
		mf.setValueContainsLiteralCharacters(false);
		setCodigoFormatado(mf.valueToString(codigo));
	}
	/**
	 * @return the codigoFormatado
	 */
	public String getCodigoFormatado() {
		return codigoFormatado;
	}

	/**
	 * @param codigoFormatado the codigoFormatado to set
	 */
	public void setCodigoFormatado(String codigoFormatado) {
		this.codigoFormatado = codigoFormatado;
		
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(String codigo) {
		formatarCodigoProposta(codigo);
		this.codigo = codigo;
	}

	/**
	 * Retorna plano.
	 *
	 * @return plano - plano.
	 */
	public PlanoVO getPlano() {
		return plano;
	}

	/**
	 * Especifica plano.
	 *
	 * @param plano - plano.
	 */
	public void setPlano(PlanoVO plano) {
		this.plano = plano;
	}

	/**
	 * Retorna beneficiarios.
	 *
	 * @return beneficiarios - beneficiarios.
	 */
	public BeneficiariosVO getBeneficiarios() {
		return beneficiarios;
	}

	/**
	 * Especifica beneficiarios.
	 *
	 * @param beneficiarios - beneficiarios.
	 */
	public void setBeneficiarios(BeneficiariosVO beneficiarios) {
		this.beneficiarios = beneficiarios;
	}

	/**
	 * Retorna corretor.
	 *
	 * @return corretor - corretor
	 */
	public CorretorVO getCorretor() {
		return corretor;
	}

	/**
	 * Especifica corretor.
	 *
	 * @param corretor - corretor
	 */
	public void setCorretor(CorretorVO corretor) {
		this.corretor = corretor;
	}

	/**
	 * Retorna pagamento.
	 *
	 * @return pagamento - pagamento.
	 */
	public PagamentoVO getPagamento() {
		return pagamento;
	}

	/**
	 * Especifica pagamento.
	 *
	 * @param pagamento - pagamento.
	 */
	public void setPagamento(PagamentoVO pagamento) {
		this.pagamento = pagamento;
	}

	/**
	 * Retorna status.
	 *
	 * @return status - status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Especifica status.
	 *
	 * @param status - status.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Retorna canal.
	 *
	 * @return canal - canal
	 */
	public CanalVO getCanal() {
		return canal;
	}

	/**
	 * Especifica canal.
	 *
	 * @param canal - canal
	 */
	public void setCanal(CanalVO canal) {
		this.canal = canal;
	}

	/**
	 * Retorna dataEmissao.
	 *
	 * @return dataEmissao - dataEmissao
	 */
	public LocalDate getDataEmissao() {
		return dataEmissao;
	}

	public LocalDate getDataValidadeProposta() {
		if (this.getDataEmissao() != null) {
			dataValidadeProposta = this.getDataEmissao().plusDays(90);
			if (dataValidadeProposta.getDayOfWeek() == 6) {
				dataValidadeProposta = dataValidadeProposta.plusDays(2);
			} else if (dataValidadeProposta.getDayOfWeek() == 7) {
				dataValidadeProposta = dataValidadeProposta.plusDays(1);
			}
		}

		return dataValidadeProposta;
	}

	public void setDataValidadeProposta(LocalDate dataValidadeProposta) {
		this.dataValidadeProposta = dataValidadeProposta;
	}

	/**
	 * Especifica dataEmissao.
	 *
	 * @param dataEmissao - dataEmissao
	 */
	public void setDataEmissao(LocalDate dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getCodigoMatriculaGerente() {
		return codigoMatriculaGerente;
	}

	public void setCodigoMatriculaGerente(String codigoMatriculaGerente) {
		this.codigoMatriculaGerente = codigoMatriculaGerente;
	}

	public CorretorMasterVO getCorretorMaster() {
		return corretorMaster;
	}

	public void setCorretorMaster(CorretorMasterVO corretorMaster) {
		this.corretorMaster = corretorMaster;
	}

	public AngariadorVO getAngariador() {
		return angariador;
	}

	public void setAngariador(AngariadorVO angariador) {
		this.angariador = angariador;
	}

	public SucursalVO getSucursalSelecionada() {
		return sucursalSelecionada;
	}

	public void setSucursalSelecionada(SucursalVO sucursalSelecionada) {
		this.sucursalSelecionada = sucursalSelecionada;
	}

	public Integer getTipoComissao() {
		return tipoComissao;
	}

	public void setTipoComissao(Integer tipoComissao) {
		this.tipoComissao = tipoComissao;
	}

	public List<DependenteVO> getDependentes() {
		return dependentes;
	}

	public void setDependentes(List<DependenteVO> dependentes) {
		this.dependentes = dependentes;
	}

	public String getCodigoMatriculaAssistente() {
		return codigoMatriculaAssistente;
	}

	public void setCodigoMatriculaAssistente(String codigoMatriculaAssistente) {
		this.codigoMatriculaAssistente = codigoMatriculaAssistente;
	}

	public LocalDate getDataInicioCobranca() {
		return dataInicioCobranca;
	}

	public void setDataInicioCobranca(LocalDate dataInicioCobranca) {
		this.dataInicioCobranca = dataInicioCobranca;
	}

	/**
	 * @return the codigoPostoAtendimento
	 */
	public Long getCodigoPostoAtendimento() {
		return codigoPostoAtendimento;
	}

	/**
	 * @param codigoPostoAtendimento the codigoPostoAtendimento to set
	 */
	public void setCodigoPostoAtendimento(Long codigoPostoAtendimento) {
		this.codigoPostoAtendimento = codigoPostoAtendimento;
	}
	
	

	/**
	 * @return the corretorLista
	 */
/*	public List<CorretorVO> getCorretorLista() {
		return corretorLista;
	}

	*//**
	 * @param corretorLista the corretorLista to set
	 *//*
	public void setCorretorLista(List<CorretorVO> corretorLista) {
		this.corretorLista = corretorLista;
	}*/
	

}
