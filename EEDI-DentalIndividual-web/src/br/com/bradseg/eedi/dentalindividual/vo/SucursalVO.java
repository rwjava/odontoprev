package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

import br.com.bradseg.eedi.dentalindividual.util.Constantes;

public class SucursalVO implements Serializable {

	private static final long serialVersionUID = -4203712494180092028L;
	private Integer codigo;
	private Integer cpd;
	private String cpdSucursal;
	private String nome;
	protected TipoSucursalSeguradora tipo;
	private String[] cpdEcodigo;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoSucursalSeguradora getTipo() {
		return tipo;
	}

	public void setTipo(TipoSucursalSeguradora tipo) {
		this.tipo = tipo;
	}

	public Integer getCpd() {
		return cpd;
	}

	public void setCpd(Integer cpd) {
		this.cpd = cpd;
	}

	public String getCpdSucursal() {
		
		if(cpdSucursal == null){
			return null;
		}
		if(!Constantes.ZERO_STRING.equals(cpdSucursal)){
			cpdEcodigo = cpdSucursal.split(";");
			setCodigo(Integer.parseInt(cpdEcodigo[0]));
			setCpd(Integer.parseInt(cpdEcodigo[1]));
		}
		
		return cpdSucursal;
	}

	public void setCpdSucursal(String cpdSucursal) {
		this.cpdSucursal = cpdSucursal;
	}

}
