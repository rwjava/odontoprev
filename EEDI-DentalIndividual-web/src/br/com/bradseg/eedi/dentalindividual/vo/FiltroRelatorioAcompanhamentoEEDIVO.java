package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

import br.com.bradseg.eedi.dentalindividual.util.DateUtil;
//import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDate;

public class FiltroRelatorioAcompanhamentoEEDIVO implements Serializable {

    private static final long serialVersionUID = 2114986582630923156L;
    
	private FiltroPeriodoVO filtroPeriodoVO;
	private String codigoProposta;
	private String cpfBeneficiarioTitular;
	private Integer sucursal;
	private Integer cpdCorretor;
	private String cpfCnpjCorretor;//cpf  corpo
	private String codigoAgenciaDebito;
	private String codigoAgenciaProdutora;
	private Integer codigoAssistente;
	private Integer codigoGerenteProdutoBVP;
	private Integer codigoStatusProposta;
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private PessoaVO pessoa = new PessoaVO();
	private String nomeProponente;
	private TipoProdutor tipoProdutor;
	private String cpfProponente;
	private boolean indicadorIntranet;
	
	public FiltroRelatorioAcompanhamentoEEDIVO() {
    	this.filtroPeriodoVO = new FiltroPeriodoVO();
    	this.pessoa = new PessoaVO();
	}

	
	public LocalDate getDataInicio() {
	    return DateUtil.cloneLocalDate(dataInicio);
	}

	public void setDataInicio(LocalDate dataInicio) {
	    this.dataInicio = DateUtil.cloneLocalDate(dataInicio);
	}

	public LocalDate getDataFim() {
	    return DateUtil.cloneLocalDate(dataFim);
	}

	public void setDataFim(LocalDate dataFim) {
	    this.dataFim = DateUtil.cloneLocalDate(dataFim);
	}

	public FiltroPeriodoVO getFiltroPeriodoVO() {
	    return filtroPeriodoVO;
	}
	public void setFiltroPeriodoVO(FiltroPeriodoVO filtroPeriodoVO) {
	    this.filtroPeriodoVO = filtroPeriodoVO;
	}
	public String getCodigoProposta() {
	    return codigoProposta;
	}
	public void setCodigoProposta(String codigoProposta) {
	    this.codigoProposta = codigoProposta;
	}
	public String getCpfBeneficiarioTitular() {
	    return cpfBeneficiarioTitular;
	}
	public void setCpfBeneficiarioTitular(String cpfBeneficiarioTitular) {
	    this.cpfBeneficiarioTitular = cpfBeneficiarioTitular;
	}
	public Integer getSucursal() {
	    return sucursal;
	}
	public void setSucursal(Integer sucursal) {
	    this.sucursal = sucursal;
	}
	public Integer getCpdCorretor() {
	    return cpdCorretor;
	}
	public void setCpdCorretor(Integer cpdCorretor) {
	    this.cpdCorretor = cpdCorretor;
	}
	public String getCpfCnpjCorretor() {
	    return cpfCnpjCorretor;
	}
	public void setCpfCnpjCorretor(String cpfCnpjCorretor) {
	    this.cpfCnpjCorretor = cpfCnpjCorretor;
	}
	public String getCodigoAgenciaDebito() {
	    return codigoAgenciaDebito;
	}
	public void setCodigoAgenciaDebito(String codigoAgenciaDebito) {
	    this.codigoAgenciaDebito = codigoAgenciaDebito;
	}
	public String getCodigoAgenciaProdutora() {
	    return codigoAgenciaProdutora;
	}
	public void setCodigoAgenciaProdutora(String codigoAgenciaProdutora) {
	    this.codigoAgenciaProdutora = codigoAgenciaProdutora;
	}
	public Integer getCodigoAssistente() {
	    return codigoAssistente;
	}
	public void setCodigoAssistente(Integer codigoAssistente) {
	    this.codigoAssistente = codigoAssistente;
	}
	public Integer getCodigoGerenteProdutoBVP() {
	    return codigoGerenteProdutoBVP;
	}
	public void setCodigoGerenteProdutoBVP(Integer codigoGerenteProdutoBVP) {
	    this.codigoGerenteProdutoBVP = codigoGerenteProdutoBVP;
	}
	public Integer getCodigoStatusProposta() {
	    return codigoStatusProposta;
	}
	public void setCodigoStatusProposta(Integer codigoStatusProposta) {
	    this.codigoStatusProposta = codigoStatusProposta;
	}

	public PessoaVO getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaVO pessoa) {
		this.pessoa = pessoa;
	}

	public String getNomeProponente() {
		return nomeProponente;
	}

	public void setNomeProponente(String nomeProponente) {
		this.nomeProponente = nomeProponente;
	}

	public TipoProdutor getTipoProdutor() {
		return tipoProdutor;
	}

	public void setTipoProdutor(TipoProdutor tipoProdutor) {
		this.tipoProdutor = tipoProdutor;
	}

	public String getCpfProponente() {
		return cpfProponente;
	}

	public void setCpfProponente(String cpfProponente) {
		this.cpfProponente = cpfProponente;
	}


	public boolean isIndicadorIntranet() {
		return indicadorIntranet;
	}


	public void setIndicadorIntranet(boolean indicadorIntranet) {
		this.indicadorIntranet = indicadorIntranet;
	}


	
   	 
}
