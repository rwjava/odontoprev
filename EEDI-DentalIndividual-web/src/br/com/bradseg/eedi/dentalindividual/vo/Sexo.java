package br.com.bradseg.eedi.dentalindividual.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enum para transportar os sexos.
 *
 */
public enum Sexo {
	
	F("F", "Feminino"), M("M", "Masculino");

	private String codigo;
	private String nome;
	
	Sexo(String codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Metodo responsavel por listar os sexos.
	 * 
	 * @return List<Sexo> - lista de sexo.
	 */
	public static List<Sexo> listar(){
		List<Sexo> lista = new ArrayList<Sexo>();
		for (Sexo sexo : Sexo.values()) {
			lista.add(sexo);
		}
		return lista;
	}
	
	public static Sexo obterPorCodigo(String codigo){
		for (Sexo sexo : Sexo.values()) {
			if(codigo.equals(sexo.getCodigo())){
				return sexo;
			}
		}
		return null;
	}
}
