package br.com.bradseg.eedi.dentalindividual.vo;


import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;

public class Modulo10 {

	/**
	 * Gera o d�gito verificador de um valor n�merico.
	 * 
	 * @param valorSemDigitoVerificador Valor em string contendo apenas n�meros. N�o pode ser nulo.
	 * @return D�gito verificador
	 */
	public static Integer gerarDigitoVerificador(String valorSemDigitoVerificador) {
		Preconditions.checkNotNull(valorSemDigitoVerificador);
		if (StringUtils.isEmpty(valorSemDigitoVerificador)) {
			throw new IllegalArgumentException("O valor n�o pode estar vazio");
		}
		int i = 2;
		int soma = 0;
		for (char c : StringUtils.reverse(valorSemDigitoVerificador).toCharArray()) {
			int digitoMultiplicado = (new Integer(c + "")) * i;
			soma += digitoMultiplicado > 9 ? (digitoMultiplicado - 10) + 1 : digitoMultiplicado;
			i = i == 2 ? 1 : 2;
		}
		int digito = 10 - (soma % 10);
		if (digito == 10) {
			digito = 0;
		}
		return digito;
	}

	/**
	 * Valida o d�gito verificador informado num valor n�merico.
	 * 
	 * @param valorComDigitoVerificador Valor em string contendo apenas n�meros incluindo o d�gito verificador. N�o pode
	 *            ser nulo.
	 * @return Verdadeiro se o d�gito verificador informado for v�lido.
	 */
	public static boolean validarDigitoVerificador(String valorComDigitoVerificador) {
		Preconditions.checkNotNull(valorComDigitoVerificador);
		if (StringUtils.isEmpty(valorComDigitoVerificador)) {
			throw new IllegalArgumentException("O valor para ser verificado n�o pode estar vazio");
		}
		String valorSemDigitoVerificador = valorComDigitoVerificador.substring(0,
				valorComDigitoVerificador.length() - 1);
		String digitoInformado = valorComDigitoVerificador.substring(valorComDigitoVerificador.length() - 1,
				valorComDigitoVerificador.length());
		Integer digitoVerificador = gerarDigitoVerificador(valorSemDigitoVerificador);
		return digitoVerificador != null && digitoVerificador.toString().equals(digitoInformado);
	}
}
