package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

public class AgenciaProdutoraVO implements Serializable{

	private static final long serialVersionUID = 4470996712374853851L;
	
	private Integer codigo;
	private String nome;
	private String digito;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDigito() {
		return digito;
	}
	public void setDigito(String digito) {
		this.digito = digito;
	}
	
	
	
}
