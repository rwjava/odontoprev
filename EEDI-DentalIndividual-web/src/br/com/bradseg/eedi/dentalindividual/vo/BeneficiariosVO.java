package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsavel pelo transporte de dados dos benefici�rios.
 *
 */
public class BeneficiariosVO implements Serializable{

	private static final long serialVersionUID = 8883038277164324248L;
	
	private boolean titularMenorIdade;
	private RepresentanteLegalVO representanteLegal;
	private TitularVO titular;
	private List<DependenteVO> dependentes;
	
	
	/**
	 * Construtor padr�o.
	 */
	public BeneficiariosVO(){
		titular = new TitularVO();
		representanteLegal = new RepresentanteLegalVO();
		dependentes = new ArrayList<DependenteVO>();
		
	}
	

	
	/**
	 * Retorna titularMenorIdade.
	 *
	 * @return titularMenorIdade - titularMenorIdade
	 */
	public boolean isTitularMenorIdade() {
		return titularMenorIdade;
	}

	/**
	 * Especifica titularMenorIdade.
	 *
	 * @param titularMenorIdade - titularMenorIdade
	 */
	public void setTitularMenorIdade(boolean titularMenorIdade) {
		this.titularMenorIdade = titularMenorIdade;
	}

	/**
	 * Retorna representanteLegal.
	 *
	 * @return representanteLegal - representanteLegal.
	 */
	public RepresentanteLegalVO getRepresentanteLegal() {
		return representanteLegal;
	}
	/**
	 * Especifica representanteLegal.
	 *
	 * @param representanteLegal - representanteLegal.
	 */
	public void setRepresentanteLegal(RepresentanteLegalVO representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	/**
	 * Retorna titular.
	 *
	 * @return titular - titular.
	 */
	public TitularVO getTitular() {
		return titular;
	}
	/**
	 * Especifica titular.
	 *
	 * @param titular - titular.
	 */
	public void setTitular(TitularVO titular) {
		this.titular = titular;
	}
	/**
	 * Retorna dependentes.
	 *
	 * @return dependentes - dependentes.
	 */
	public List<DependenteVO> getDependentes() {
		return dependentes;
	}
	/**
	 * Especifica dependentes.
	 *
	 * @param dependentes - dependentes.
	 */
	public void setDependentes(List<DependenteVO> dependentes) {
		this.dependentes = dependentes;
	}



}
