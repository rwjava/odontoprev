package br.com.bradseg.eedi.dentalindividual.vo;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;



public class BoletoVO implements Serializable {

	private static final long serialVersionUID = -6273804794684569107L;

	private Long numeroProposta;
	private Integer codigoSucursal;
	private String codigoProposta;
	private Integer codigoCorretor;
	private String numeroBanco = "000";
	private String campoLivre = "0";
	private String moeda = "9";
	private BigDecimal valorBoleto = BigDecimal.ZERO;
	private String sacado;
	private LocalDate dataDeVencimento;
	
	protected String numeroAgenciaOdonto = "2372";
	protected String codigoCarteira = "22";
	protected String codigoCareiraSegmento = "2";
	protected String ccOdontoPrev = "0008162";
	protected String dataVencimento = "0000";
	private boolean indicadorPropostaVazia;
	private String codigoBarraCompleto;

	/**
	 * @return the codigoBarraCompleto
	 */
	public String getCodigoBarraCompleto() {
		return codigoBarraCompleto;
	}



	/**
	 * @param codigoBarraCompleto the codigoBarraCompleto to set
	 */
	public void setCodigoBarraCompleto(String codigoBarraCompleto) {
		this.codigoBarraCompleto = codigoBarraCompleto;
	}



	public LocalDate getDataDeVencimentoPropostaFinalizada() {
		return new LocalDate().plusDays(1);
	}

	
	
	public String getSacado() {
		return sacado;
	}

	public LocalDate getDataDeVencimento() {
		return dataDeVencimento;
	}

	public void setDataDeVencimento(LocalDate dataDeVencimento) {
		this.dataDeVencimento = dataDeVencimento;
	}

	public void setSacado(String sacado) {
		this.sacado = sacado;
	}

	public void setNumeroProposta(Long numeroProposta) {
		this.numeroProposta = numeroProposta;
	}

	public Long getNumeroProposta() {
		return numeroProposta;
	}

	public BigDecimal getValorBoleto() {
		return valorBoleto;
	}

	public String getNossoNumero() {
		return getNumeroPropostaAsString();
	}

	public Integer getCodigoCorretor() {
		return codigoCorretor;
	}

	public Integer getCodigoSucursal() {
		return codigoSucursal;
	}

	public String getCodigoProposta() {
		return codigoProposta;
	}

	public void setCodigoCorretor(Integer codigoCorretor) {
		this.codigoCorretor = codigoCorretor;
	}

	public void setCodigoProposta(String codigoProposta) {
		this.codigoProposta = codigoProposta;
	}

	public void setCodigoSucursal(Integer codigoSucursal) {
		this.codigoSucursal = codigoSucursal;
	}

	private String getNumeroPropostaAsString() {
		return StringUtils.leftPad(numeroProposta.toString(), 11, "0");
	}

	private String getValorBoletoAsString() {

		return StringUtils.leftPad(String.format("%.2f", valorBoleto).replaceAll("[^\\d]", ""), 10, "0");
	}

	public void setValorBoleto(BigDecimal valorBoleto) {
		this.valorBoleto = valorBoleto;
	}

	public String calcularLinhaDigitavel() {
		return new CalculadorLinhaDigitavel().calcularLinhaDigitalComMascara();
	}

	public String calcularCodigoBarra() {
		return new CalculadorCodigoBarra().calcularCodigoBarra();
	}
	
	public String getCalcularFator() {
		return new CalculadorLinhaDigitavel().calcularFatorLinhaDigital();
	}


	public boolean isIndicadorPropostaVazia() {
		return indicadorPropostaVazia;
	}

	public void setIndicadorPropostaVazia(boolean indicadorPropostaVazia) {
		this.indicadorPropostaVazia = indicadorPropostaVazia;
	}


	protected class CalculadorLinhaDigitavel {
		
		private final Pattern PADRAO = Pattern.compile("(\\d{5})(\\d{5})(\\d{5})(\\d{6})(\\d{5})(\\d{6})(\\d{1})(\\d{14})");

		public String calcularLinhaDigitalComMascara() {
			String linhaDigitavelSemMascara = calcularLinhaDigitavelComFator();
			Matcher matcher = PADRAO.matcher(linhaDigitavelSemMascara);
			if (matcher.find()) {
				return matcher.group(1) + "." + matcher.group(2) + " " + matcher.group(3) + "." + matcher.group(4) + " " + matcher.group(5) + "." + matcher.group(6) + " " + matcher.group(7) + " " + matcher.group(8);
			}
			return null;
		}

		public String calcularFatorLinhaDigital() {
			if (getValorBoleto().compareTo(BigDecimal.ZERO) == 0) {
				return dataVencimento;
			}
			Calendar vencimento = Calendar.getInstance();
			vencimento.set(Calendar.DATE, vencimento.get(Calendar.DATE) + 1);
			Calendar dataBase = Calendar.getInstance();
			dataBase.set(1997, 9, 7);
			long qtdmillis = vencimento.getTimeInMillis() - dataBase.getTimeInMillis();
			long qtddias = qtdmillis / (24 * 60 * 60 * 1000);
			return String.valueOf(Math.abs(qtddias));
		}

		public String calcularLinhaDigitavel() {
			StringBuilder sb = new StringBuilder();
			String primeiraParte = calcularPrimeiraParteLinhaDigitavel();
			String segundaParte = calcularSegundaParteLinhaDigitavel();
			String digitoVerificadorCodigoBarra = new CalculadorCodigoBarra().calcularDigitoVerificadorCodigoBarra();
			sb.append(primeiraParte);
			sb.append(digitoVerificadorCodigoBarra);
			sb.append(segundaParte);
			return sb.toString();
		}

		public String calcularLinhaDigitavelComFator() {
			StringBuilder sb = new StringBuilder();
			String primeiraParte = calcularPrimeiraParteLinhaDigitavel();
			String segundaParte = calcularSegundaParteLinhaDigitavelComFatorDeVencimento();
			String digitoVerificadorCodigoBarra = new CalculadorCodigoBarra().calcularDigitoVerificadorCodigoBarra();
			sb.append(primeiraParte);
			sb.append(digitoVerificadorCodigoBarra);
			sb.append(segundaParte);
			return sb.toString();
		}

		protected String calcularPrimeiraParteLinhaDigitavel() {
			StringBuilder sb = new StringBuilder();
			sb.append(calcularPrimeiroBlocoLinhaDigitavel());
			sb.append(calcularSegundoBlocoLinhaDigitavel());
			sb.append(calcularTerceiroBlocoLinhaDigitavel());
			return sb.toString();
		}

		protected String calcularSegundaParteLinhaDigitavel() {
			return new StringBuilder().append(dataVencimento).append(getValorBoletoAsString()).toString();
		}

		protected String calcularSegundaParteLinhaDigitavelComFatorDeVencimento() {
			return new StringBuilder().append(getCalcularFator()).append(getValorBoletoAsString()).toString();
		}

		protected String calcularPrimeiroBlocoLinhaDigitavel() {
			String bloco = new StringBuilder().append(numeroBanco).append(moeda).append(numeroAgenciaOdonto)
					.append(codigoCareiraSegmento).toString();
			return bloco + Modulo10.gerarDigitoVerificador(bloco);
		}

		protected String calcularSegundoBlocoLinhaDigitavel() {
			String bloco = new StringBuilder().append(codigoCareiraSegmento)
					.append(getNumeroPropostaAsString().substring(0, 9)).toString();
			return bloco + Modulo10.gerarDigitoVerificador(bloco);
		}

		protected String calcularTerceiroBlocoLinhaDigitavel() {
			String bloco = new StringBuilder().append(getNumeroPropostaAsString().substring(9, 11)).append(ccOdontoPrev).append(campoLivre).toString();
			return bloco + Modulo10.gerarDigitoVerificador(bloco);
		}
	}// Fim da Classe CalculadorLinhaDigitavel.

	protected class CalculadorCodigoBarra {// Inicio da classe CalculadorCodigoBarra
		
		public String calcularCodigoBarra() {
			String primeiroBloco = calcularPrimeiroBlocoCodigoBarra();
			String segundoBloco = calcularSegundoBlocoCodigoBarra();
			String terceiroBloco = calcularTerceiroBlocoCodigoBarra();
			String digitoVerificador = calcularDigitoVerificadorCodigoBarra();
			setCodigoBarraCompleto(primeiroBloco + digitoVerificador + segundoBloco + terceiroBloco);
			return primeiroBloco + digitoVerificador + segundoBloco + terceiroBloco;
		}

		public String calcularDigitoVerificadorCodigoBarra() {
			String primeiroBloco = calcularPrimeiroBlocoCodigoBarra();
			String segundoBloco = calcularSegundoBlocoCodigoBarra();
			String terceiroBloco = calcularTerceiroBlocoCodigoBarra();
			Integer digitoVerificador = Modulo11BoletoBradesco.gerarDigitoVerificador(primeiroBloco + segundoBloco
					+ terceiroBloco);
			return digitoVerificador.toString();
		}

		public String calcularPrimeiroBlocoCodigoBarra() {
			StringBuilder sb = new StringBuilder();
			sb.append(numeroBanco).append(moeda);
			return sb.toString();
		}

		public String calcularSegundoBlocoCodigoBarra() {
			StringBuilder sb = new StringBuilder();
			sb.append(getCalcularFator()).append(getValorBoletoAsString());
			return sb.toString();
		}

		public String calcularTerceiroBlocoCodigoBarra() {
			StringBuilder sb = new StringBuilder();
			sb.append(numeroAgenciaOdonto).append(getCodigoCarteiraCom2Posicoes()).append(getNumeroPropostaAsString())
					.append(ccOdontoPrev).append("0");
			return sb.toString();
		}

		private String getCodigoCarteiraCom2Posicoes() {
			return StringUtils.leftPad(codigoCarteira, 2, "0");
		}
	}// Fim da classe CalculadorCodigoBarra.
}