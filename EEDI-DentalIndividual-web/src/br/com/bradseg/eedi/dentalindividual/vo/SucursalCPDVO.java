package br.com.bradseg.eedi.dentalindividual.vo;

import java.io.Serializable;

/**
 * VO para dados de SucursalCPD
 * 
 * @author EEDI
 */
public class SucursalCPDVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer sucursal;

	private Integer cpd;

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getSucursal() {
		return sucursal;
	}

	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}

	public Integer getCpd() {
		return cpd;
	}

	public void setCpd(Integer cpd) {
		this.cpd = cpd;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof SucursalCPDVO)) {
			return false;
		}

		return ((SucursalCPDVO) obj).cpd.equals(cpd) && ((SucursalCPDVO) obj).sucursal.equals(sucursal);
	}

	@Override
	public int hashCode() {
		return cpd.hashCode() * 2 ^ 3 * sucursal.hashCode() * 2 ^ 17;
	}
}
