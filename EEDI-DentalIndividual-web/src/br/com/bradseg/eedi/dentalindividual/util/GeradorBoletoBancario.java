package br.com.bradseg.eedi.dentalindividual.util;


import java.math.BigDecimal;

import org.joda.time.LocalDate;

import br.com.bradseg.eedi.dentalindividual.vo.BoletoVO;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.TipoCobranca;


/**
 * Gerador de boleto banc�rio.
 * @author WDEV
 */
public class GeradorBoletoBancario {

    private static Integer BENEFICIARIO_TITULAR = 1;
	public static BoletoVO gerarBoletoProposta(PropostaVO proposta) {
		BoletoVO boleto = new BoletoVO();
		boleto.setNumeroProposta(proposta.getSequencial());
		boleto.setCodigoProposta(proposta.getCodigo());
		if(proposta.getCorretor().getCpd() != null){
			boleto.setCodigoCorretor(proposta.getCorretor().getCpd().intValue());
		}
		boleto.setCodigoSucursal(proposta.getCorretor().getSucursal());
		
		Integer quantidadeBeneficiarios = proposta.getBeneficiarios().getDependentes().size() + BENEFICIARIO_TITULAR;
		
		//calculando o valor total de acordo com o tipo de cobran�a (mensal/anual)
		if(proposta.getPagamento().getCodigoTipoCobranca()  != null){
			if( TipoCobranca.MENSAL.getCodigo() == proposta.getPagamento().getCodigoTipoCobranca() ){
				boleto.setValorBoleto(new BigDecimal(proposta.getPlano().getValorPlanoVO().getValorMensalTitular() *  quantidadeBeneficiarios));
			}else if(TipoCobranca.ANUAL.getCodigo() == proposta.getPagamento().getCodigoTipoCobranca()){
				boleto.setValorBoleto(new BigDecimal(proposta.getPlano().getValorPlanoVO().getValorAnualTitular() * quantidadeBeneficiarios));
			}
		}
		
		//verificando o nome a ser setado no campo 'sacado'
		if (proposta.getBeneficiarios().getTitular() == null) {
			boleto.setSacado("");
		}else if (proposta.getBeneficiarios().isTitularMenorIdade()) {
			boleto.setSacado(proposta.getBeneficiarios().getRepresentanteLegal().getNome());
		}else{
			boleto.setSacado(proposta.getBeneficiarios().getTitular().getNome());
		}
		boleto.setDataDeVencimento(proposta.getDataValidadeProposta());

		
		
//		//if (!isPropostaVazia) {
//			boleto.setDataDeVencimento(getDataDeVencimento());
//			
//			//TODO Or�amento???
//			//boleto.setValor(proposta.getOrcamento().getValorTotalComTaxaDeAdesao());
//			boleto.setSacado(getNomeSacado(proposta));
//		//}
//
//		String primeiroBlocoCodigoBarras = calcularPrimeiroBlocoCodigoBarras();
//		String segundoBlocoCodigoBarras = calcularSegundoBlocoCodigoBarras(proposta);
//		String terceiroBlocoCodigoBarras = calcularTerceiroBlocoCodigoBarras();
//		String digitoVerificadorCodigoBarras = String.valueOf(Modulo11.gerarDigitoVerificador(primeiroBlocoCodigoBarras + segundoBlocoCodigoBarras + terceiroBlocoCodigoBarras));
//
//		boleto.setCodigoBarra(primeiroBlocoCodigoBarras + digitoVerificadorCodigoBarras + segundoBlocoCodigoBarras + terceiroBlocoCodigoBarras);
//
//		StringBuilder linhaDigitavel = new StringBuilder();
//		linhaDigitavel.append(calcularPrimeiroBlocoLinhaDigitavel());
//		linhaDigitavel.append(calcularSegundoBlocoLinhaDigitavel());
//		linhaDigitavel.append(calcularTerceiroBlocoLinhaDigitavel());
//		linhaDigitavel.append(digitoVerificadorCodigoBarras);
//		linhaDigitavel.append(segundoBlocoCodigoBarras);
//
//		boleto.setLinhaDigitavel(mascararLinhaDigitavel(linhaDigitavel.toString()));

		return boleto;
	}

	private String getNomeSacado(PropostaVO proposta) {
		if (proposta.getBeneficiarios().getTitular() == null) {
			return null;
		}

		if (proposta.getBeneficiarios().isTitularMenorIdade()) {
			return proposta.getBeneficiarios().getRepresentanteLegal().getNome();
		}
		return proposta.getBeneficiarios().getTitular().getNome();
	}

	private LocalDate getDataDeVencimento() {
		return new LocalDate().plusDays(1);
	}

//	private String mascararLinhaDigitavel(String linhaDigitavel) {
//		Matcher matcher = Constantes.PADRAO_LINHA_DIGITAVEL.matcher(linhaDigitavel);
//		if (matcher.find()) {
//			return matcher.group(1) + "." + matcher.group(2) + " " + matcher.group(3) + "." + matcher.group(4) + " " + matcher.group(5) + "." + matcher.group(6) + " " + matcher.group(7) + " " + matcher.group(8);
//		}
//		return null;
//	}
//
//	private String calcularPrimeiroBlocoLinhaDigitavel() {
//		String primeiroBloco = new StringBuilder().append(Constantes.NUMERO_BANCO_BRADESCO).append(Constantes.MOEDA).append(Constantes.NUMERO_AGENCIA_ODONTO).append(Constantes.CAMPO_LIVRE_BOLETO).toString();
//		return primeiroBloco + Modulo10.gerarDigitoVerificador(primeiroBloco);
//	}
//
//	private String calcularSegundoBlocoLinhaDigitavel() {
//		String segundoBloco = new StringBuilder().append(Constantes.CODIGO_CARTEIRA).append(sequencialProposta.substring(0, 9)).toString();
//		return segundoBloco + Modulo10.gerarDigitoVerificador(segundoBloco);
//	}
//
//	private String calcularTerceiroBlocoLinhaDigitavel() {
//		String terceiroBloco = new StringBuilder().append(sequencialProposta.substring(9, 11)).append(Constantes.CONTA_CEDENTE_ODONTO_PREV).append(Constantes.CAMPO_LIVRE_BOLETO).toString();
//		return terceiroBloco + Modulo10.gerarDigitoVerificador(terceiroBloco);
//	}
//
//	private String calcularPrimeiroBlocoCodigoBarras() {
//		return new StringBuilder().append(Constantes.NUMERO_BANCO_BRADESCO).append(Constantes.MOEDA).toString();
//	}
//
//	private String calcularSegundoBlocoCodigoBarras(PropostaVO proposta) {
////		if (isPropostaVazia) {
////			return new StringBuilder().append(Constantes.FATOR_LINHA_DIGITAVEL_PROPOSTA_VAZIA).append(getValorBoletoPropostaVazia()).toString();
////		}
//		return new StringBuilder().append(calcularFatorLinhaDigitavelProposta()).append(getValorBoletoProposta(proposta)).toString();
//
//	}
//
//	private Integer calcularFatorLinhaDigitavelProposta() {
//		return Days.daysBetween(Constantes.DATA_FATOR_LINHA_DIGITAVEL, getDataDeVencimento()).getDays();
//	}
//
//	private String getValorBoletoPropostaVazia() {
//		return StringUtils.leftPad(Constantes.VALOR_BOLETO_PROPOSTA_VAZIA.toString(), 10, "0");
//	}
//
//	private String getValorBoletoProposta(PropostaVO proposta) {
//		//TODO Or�amento
//		//return StringUtils.leftPad(proposta.getOrcamento().getValorTotalComTaxaDeAdesao().toString().replaceAll("[^\\d]", ""), 10, "0");
//		return "";
//	}
//
//	private String calcularTerceiroBlocoCodigoBarras() {
//		return new StringBuilder().append(Constantes.NUMERO_AGENCIA_ODONTO).append(getCodigoCarteiraCom2Posicoes()).append(sequencialProposta).append(Constantes.CONTA_CEDENTE_ODONTO_PREV).append("0").toString();
//	}
//
//	private String getCodigoCarteiraCom2Posicoes() {
//		return StringUtils.leftPad(Constantes.CODIGO_CARTEIRA, 2, "0");
//	}

}