package br.com.bradseg.eedi.dentalindividual.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe utilit�ria para ajudar na valida��o de dados de formul�rio
 * 
 * @author EEDI
 */
public class ValidacaoUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidacaoUtil.class);

	/**
	 * Valida o d�gito verificador da conta. C�digo exatamente igual ao encontrado em: valida_contacorrente.js do
	 * projeto EEDI-EmissaoExpressaDI-web
	 * 
	 * @param contaCorrente Somente os n�meros da conta, sem digito
	 * @param contaDigito O digito verificador
	 * @return true se contaDigito estiver correto
	 */
	
	
	public static boolean validarCNFCNPJ(String cpfCnpj){
		
		if(cpfCnpj == null || "0".equals(cpfCnpj)){
			return false;
		}
		if(isCpfValid(cpfCnpj) || isCNPJ(cpfCnpj)){
			return true;
		}else{
			return false;
		}
	}
	public static boolean isContaCorrenteValida(String contaCorrente, int contaDigito) {
		int lsoma = 0;
		int ipeso = 2;
		int index = contaCorrente.length();
		int digito;
		while (index > 0) {
			digito = Integer.parseInt(String.valueOf(contaCorrente.charAt(--index)));
			if (digito >= 0 && digito <= 9) {
				lsoma += digito * ipeso;
				ipeso++;
				if (ipeso > 7) {
					ipeso = 2;
				}
			}
		}
		int mod11 = 11 - (lsoma % 11);
		if (mod11 == 11 || mod11 == 10) {
			mod11 = 0;
		}
		return contaDigito == mod11;
	}

	/**
	 * Valida CNS.
	 * 
	 * @param cns N�mero CNS.
	 * @return True se for v�lido, false se for inv�lido.
	 */
	public static boolean validaCns(String cns) {
		if (cns.trim().length() != 15) {
			return (false);
		}
		float soma;
		float resto, dv;
		String pis = cns.substring(0, 11);
		String resultado;
		soma = ((Integer.valueOf(pis.substring(0, 1))) * 15) + ((Integer.valueOf(pis.substring(1, 2))) * 14)
				+ ((Integer.valueOf(pis.substring(2, 3))) * 13) + ((Integer.valueOf(pis.substring(3, 4))) * 12)
				+ ((Integer.valueOf(pis.substring(4, 5))) * 11) + ((Integer.valueOf(pis.substring(5, 6))) * 10)
				+ ((Integer.valueOf(pis.substring(6, 7))) * 9) + ((Integer.valueOf(pis.substring(7, 8))) * 8)
				+ ((Integer.valueOf(pis.substring(8, 9))) * 7) + ((Integer.valueOf(pis.substring(9, 10))) * 6)
				+ ((Integer.valueOf(pis.substring(10, 11))) * 5);
		resto = soma % 11;
		dv = 11 - resto;
		if (dv == 11) {
			dv = 0;
		}
		if (dv == 10) {
			soma = soma + 2;
			resto = soma % 11;
			dv = 11 - resto;
			resultado = pis + "001" + dv;
		} else {
			resultado = pis + "000" + dv;
		}
		if (!cns.equals(resultado)) {
			return (false);
		} else {
			return (true);
		}
	}

	/**
	 * Valida n�mero de CNS provis�rio.
	 * 
	 * @param cns N�mero CNS.
	 * @return True se for v�lido, caso contr�rio, false.
	 */
	public static boolean validaCnsProvisorio(String cns) {
		if (cns.trim().length() != 15) {
			return (false);
		}
		float resto, soma;
		soma = ((Integer.valueOf(cns.substring(0, 1))) * 15) + ((Integer.valueOf(cns.substring(1, 2))) * 14)
				+ ((Integer.valueOf(cns.substring(2, 3))) * 13) + ((Integer.valueOf(cns.substring(3, 4))) * 12)
				+ ((Integer.valueOf(cns.substring(4, 5))) * 11) + ((Integer.valueOf(cns.substring(5, 6))) * 10)
				+ ((Integer.valueOf(cns.substring(6, 7))) * 9) + ((Integer.valueOf(cns.substring(7, 8))) * 8)
				+ ((Integer.valueOf(cns.substring(8, 9))) * 7) + ((Integer.valueOf(cns.substring(9, 10))) * 6)
				+ ((Integer.valueOf(cns.substring(10, 11))) * 5) + ((Integer.valueOf(cns.substring(11, 12))) * 4)
				+ ((Integer.valueOf(cns.substring(12, 13))) * 3) + ((Integer.valueOf(cns.substring(13, 14))) * 2)
				+ ((Integer.valueOf(cns.substring(14, 15))) * 1);
		resto = soma % 11;
		if (resto != 0) {
			return (false);
		} else {
			return (true);
		}
	}

	/**
	 * Valida��o CPF. Ap�s a atualiza��o do BSAD, utilizar a valida��o j� existente no framework
	 * 
	 * @param cpf da pessoa fisica
	 * @return true se est� OK
	 */
	public static boolean isCpfValid(String cpf) {
		if (cpf == null || "0".equals(cpf)) {
			return false;
		}
		StringBuffer sb = new StringBuffer(removerCaracteresNaoNumericos(cpf));
		String n = StringUtils.leftPad(sb.toString(), 11, '0');

		boolean isCpf = (11 == n.length()) && !("00000000000").equals(cpf) && !("11111111111").equals(cpf)
				&& !("22222222222").equals(cpf) && !("33333333333").equals(cpf) && !("44444444444").equals(cpf)
				&& !("55555555555").equals(cpf) && !("66666666666").equals(cpf) && !("77777777777").equals(cpf)
				&& !("88888888888").equals(cpf) && !("99999999999").equals(cpf) && !("404040404040404").equals(cpf);
		if (!isCpf) {
			return false;
		}
		int i;
		int j;
		int digit;
		int coeficient;
		int sum;
		int[] foundDv = { 0, 0 };
		int dv1 = Integer.parseInt(String.valueOf(n.charAt(n.length() - 2)));
		int dv2 = Integer.parseInt(String.valueOf(n.charAt(n.length() - 1)));
		for (j = 0; j < 2; j++) {
			sum = 0;
			coeficient = 2;
			for (i = n.length() - 3 + j; i >= 0; i--) {
				digit = Integer.parseInt(String.valueOf(n.charAt(i)));
				sum += digit * coeficient;
				coeficient++;
			}
			foundDv[j] = 11 - sum % 11;
			if (foundDv[j] >= 10) {
				foundDv[j] = 0;
			}
		}
		return dv1 == foundDv[0] && dv2 == foundDv[1];
	}

	public static boolean validaSeMenorIdade(Date data) {
		boolean ok = false;
		Period period = new Period(new LocalDate(data), new LocalDate(Calendar.getInstance().getTime()),
				PeriodType.yearMonthDay());
		int anos = period.getYears();
		if (anos < 18) {
			ok = true;
		} else {
			ok = false;
		}
		return ok;
	}

	public static String obterDV(String fonte) {
		int pesoAlternado = 2;
		int dv = 0;
		String parcela;
		for (int i = fonte.length() - 1; i >= 0; i--) {
			parcela = String.valueOf(Integer.parseInt(fonte.substring(i, i + 1)) * pesoAlternado);
			for (int j = 0; j < parcela.length(); j++) {
				dv += Integer.parseInt(parcela.substring(j, j + 1));
			}
			if (pesoAlternado == 2) {
				pesoAlternado--;
			} else {
				pesoAlternado++;
			}
		}
		dv = dv % 10;
		return String.valueOf(10 - dv);
	}

	public static boolean dataValida(Date date) {
		if (date == null) {
			return false;
		}
		LocalDate data = new LocalDate(date);
		return data.isAfter(new LocalDate().minusYears(150)) && data.isBefore(new LocalDate().plusYears(100));
	}

	/**
	 * M�todo responsavel por verificar se o objeto � vazio ou nulo.
	 * 
	 * @param objeto - objeto a ser verificado.
	 * @return boolean - true/false
	 */
	public static boolean isBlankOrNull(Object objeto) {
		return (objeto == null || objeto.equals(""));
	}

	/**
	 * M�todo responsavel por verificar se o objeto � vazio ou nulo.
	 * 
	 * @param objeto - objeto a ser verificado.
	 * @return boolean - true/false
	 */
	public static boolean isEmptyOrNull(Collection<?> objeto) {
		return (objeto == null || objeto.isEmpty());
	}

	/**
	 * M�todo responsavel por verificar se o objeto � vazio, nulo ou zero.
	 * 
	 * @param objeto - objeto a ser verificado.
	 * @return boolean - true/false
	 */
	public static boolean isBlankOrNullOrZero(Object objeto) {
		return (objeto == null || objeto.equals("") || objeto.equals(0) || objeto.equals("0"));
	}

	/**
	 * Metodo responsavel por formatar uma data no padr�o dd/MM/yyyy HH:mm:ss.
	 * 
	 * @param data - data a ser formatada.
	 * @return string com a data formatada.
	 */
	public synchronized static String formatarDataHora(Date data) {
		return getDateFormatHoraData().format(data);
	}

	/**
	 * M�todo de apoio para converter um texto com uma data formatada no formato DD/MM/YYYY HH:MM:SS para uma inst�ncia
	 * de Date equivalente...
	 * 
	 * @param dataFormatada - Texto com data no formato DD/MM/YYYY HH:MM:SS
	 * @return - Inst�ncia de Date
	 */
	public static Date converterStringParaDate(String dataFormatada) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date data = null;
		try {
			data = new Date(df.parse(dataFormatada).getTime());
		} catch (ParseException e) {
			LOGGER.error("Erro ao converterStringParaDate: " + e.getMessage());
		}
		return data;
	}

	/**
	 * M�todo de apoio para converter um texto com uma data formatada no formato DD/MM/YYYY para uma inst�ncia de Date
	 * equivalente...
	 * 
	 * @param dataFormatada - Texto com data no formato DD/MM/YYYY
	 * @return - Inst�ncia de Date
	 */
	public static Date converterStringParaDateSemHora(String dataFormatada) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date data = null;
		try {
			data = new Date(df.parse(dataFormatada).getTime());
		} catch (ParseException e) {
			LOGGER.error("Erro ao converterStringParaDateSemHora: " + e.getMessage());
		}
		return data;
	}

	/**
	 * Metodo responsavel por formatar uma data no padr�o dd/MM/yyyy.
	 * 
	 * @param data - data a ser formatada.
	 * @return string com a data formatada.
	 */
	public synchronized static String formatarDataSemHora(Date data) {
		if (!isBlankOrNull(data)) {
			return getDateFormatData().format(data);
		} else {
			return null;
		}
	}

	/**
	 * Metodo responsavel por formatar uma data no padr�o dd/MM/yyyy.
	 * 
	 * @param data - data a ser formatada.
	 * @return data com a data formatada.
	 */
	public synchronized static Date formatarDataSemHoraParaDate(Date data) {
		if (!isBlankOrNull(data)) {
			return formatarDataSemHora(getDateFormatData().format(data));
		} else {
			return null;
		}
	}

	/**
	 * Metodo responsavel por formatar uma data no padr�o dd/MM/yyyy.
	 * 
	 * @param data - data a ser formatada.
	 * @return data formatada.
	 */
	public synchronized static Date formatarDataSemHora(String data) {
		try {
			if (!isBlankOrNull(data)) {
				return getDateFormatData().parse(data);
			} else {
				return null;
			}
		} catch (ParseException e) {
			LOGGER.error("Erro ao converterStringParaDateSemHora: " + e.getMessage());
		}
		return null;
	}

	// retorna 0 para data iguais, -1 para 1� maior que a 2� e 1 para 2� maior que a 1�.
	public static int compararDatas(Date dataInicio, Date dataFim) {
		return dataInicio.compareTo(dataFim);
	}

	public static String verificaSeDataInicialMaiorFinal(Calendar data) {
		data = Calendar.getInstance();
		data.set(Calendar.DATE, data.get(Calendar.DATE) + 1);
		Calendar dataBase = Calendar.getInstance();
		dataBase.set(1997, 9, 7);
		long qtdmillis = data.getTimeInMillis() - dataBase.getTimeInMillis();
		long qtddias = qtdmillis / (24 * 60 * 60 * 1000);
		return String.valueOf(Math.abs(qtddias));
	}

	/**
	 * Metodo responsavel por verificar se a data � v�lida (dd/MM/yyyy).
	 * 
	 * @param data - data a ser formatada.
	 */
	public static boolean isDataValida(String data) {
		try {
			getDateFormatData().parse(data);
			return true;
		} catch (ParseException e) {
			LOGGER.error("Erro ao isDataValida: " + e.getMessage());
		}
		return false;
	}

	public static SimpleDateFormat getDateFormatData() {
		return new SimpleDateFormat("dd/MM/yyyy", Constantes.LOCALE_BRASIL);
	}

	public static SimpleDateFormat getDateFormatHoraData() {
		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Constantes.LOCALE_BRASIL);
	}

	public static Integer verificaSeDataAnteriorInicioVigenciaPlano(Date dataAnterior, String dataPlano) {
		Integer retorno = null;
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		// String data = Constantes.DATA_INICIO_VIGENCIA_NOVO_PLANO;
		Calendar dataBase = Calendar.getInstance();
		Calendar dataParametro = Calendar.getInstance();
		try {
			dataBase.setTime(formatador.parse(dataPlano));
			dataParametro.setTime(dataAnterior);
			retorno = dataParametro.getTime().compareTo(dataBase.getTime());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return retorno;
	}

	// public static boolean deveUsarNovoPlano(Date data) {
	// return verificaSeDataAnteriorInicioVigenciaNovoPlano(data) > 0;
	// }

	public static boolean numeroTelefoneValido(String numeroTelefone) {
		if (numeroTelefone == null) {
			return false;
		}
		numeroTelefone = removerCaracteresNaoNumericos(numeroTelefone);

		if (contarOcorrencias(numeroTelefone.charAt(0), numeroTelefone) >= 8) {
			return false;
		}

		// So validamos se o numero n�o vir em branco, pois o wicket ja faz a valida��o de obrigatorio
		if (StringUtils.isNotBlank(numeroTelefone)) {
			return Pattern.compile("[1-9][0-9]{7,8}").matcher(numeroTelefone).matches();
		}
		return true;
	}

	public static int contarOcorrencias(char caracter, String str) {
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == caracter) {
				count++;
			}
		}
		return count;
	}

	public static String removerCaracteresNaoNumericos(String valor) {
		return valor.replaceAll("[^0-9]", "");
	}

	public static boolean isCNPJ(String CNPJ) {
		CNPJ = StringUtils.leftPad(CNPJ, 14, '0');

		if (("00000000000000").equals(CNPJ) || ("11111111111111").equals(CNPJ) || ("22222222222222").equals(CNPJ)
				|| ("33333333333333").equals(CNPJ) || ("44444444444444").equals(CNPJ)
				|| ("55555555555555").equals(CNPJ) || ("66666666666666").equals(CNPJ)
				|| ("77777777777777").equals(CNPJ) || ("88888888888888").equals(CNPJ)
				|| ("99999999999999").equals(CNPJ) || ("404040404040404").equals(CNPJ) || (CNPJ.length() != 14)) {
			return false;
		}
		char dig13, dig14;
		int sm, i, r, num, peso;
		try {
			sm = 0;
			peso = 2;
			for (i = 11; i >= 0; i--) {
				num = (int) (CNPJ.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso + 1;
				if (peso == 10) {
					peso = 2;
				}
			}
			r = sm % 11;
			if ((r == 0) || (r == 1)) {
				dig13 = '0';
			} else {
				dig13 = (char) ((11 - r) + 48);
			}
			sm = 0;
			peso = 2;
			for (i = 12; i >= 0; i--) {
				num = (int) (CNPJ.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso + 1;
				if (peso == 10) {
					peso = 2;
				}
			}
			r = sm % 11;
			if ((r == 0) || (r == 1)) {
				dig14 = '0';
			} else {
				dig14 = (char) ((11 - r) + 48);
			}
			if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13))) {
				return true;
			} else {
				return false;
			}
		} catch (InputMismatchException erro) {
			LOGGER.error("Erro na vericacao isCNPJ : " + erro.getMessage());
		}
		return false;
	}

	public static boolean isOnlyNumeric(String numero) {
		return numero.matches("[0-9]*");
	}

	public static boolean lengthCpfCnpjCorpoValido(String cpfCnpjCorpo) {
		return cpfCnpjCorpo.length() == 8 || cpfCnpjCorpo.length() == 11 || cpfCnpjCorpo.length() == 14;
	}

	public static int countDigits(String str) {
		int count = 0;
		for (int i = 0, len = str.length(); i < len; i++) {
			if (Character.isDigit(str.charAt(i))) {
				count++;
			}
		}
		return count;
	}
}
