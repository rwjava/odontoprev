package br.com.bradseg.eedi.dentalindividual.util;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * The Class LinkBuilder.
 */
public class LinkBuilder {

	/**
	 * Retorna um objeto {@link URL} instanciado com o link informado.
	 * 
	 * @param url URL do Link.
	 * @param complemento Complamento do Link.
	 * @return um objeto {@link URL} instanciado com o link informado.
	 * @throws MalformedURLException
	 */

	public static URL getLink(String url, String complemento) throws MalformedURLException {

		return new URL(url + complemento);

	}

	public static URL getLinkCliente(String url, String complemento) throws MalformedURLException {

		return new URL(url + complemento);

	}
}