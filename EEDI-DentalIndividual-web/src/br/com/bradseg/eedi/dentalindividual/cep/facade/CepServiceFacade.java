package br.com.bradseg.eedi.dentalindividual.cep.facade;

import br.com.bradseg.eedi.dentalindividual.vo.EnderecoVO;

/**
 * Interface responsavel por disponibilizar os metodos referentes a cep.
 *
 */
public interface CepServiceFacade {
	
	/**
	 * Metodo resposanvel por obter um endere�o a partir de um cep.
	 * 
	 * @param cep - cep a ser consultado.
	 * 
	 * @return EnderecoVO - informa��es referentes ao cep consultado.
	 */
	public EnderecoVO obterEnderecoPorCep(String cep);

}
