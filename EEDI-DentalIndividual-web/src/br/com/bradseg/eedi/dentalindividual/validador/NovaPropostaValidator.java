package br.com.bradseg.eedi.dentalindividual.validador;

import java.util.List;

import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.dentalindividual.cep.facade.CepServiceFacade;
import br.com.bradseg.eedi.dentalindividual.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.dentalindividual.proposta.facade.SucursalCorretorServiceImpl;
import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.util.ConstantesMensagemErro;
import br.com.bradseg.eedi.dentalindividual.util.ValidacaoUtil;
import br.com.bradseg.eedi.dentalindividual.vo.BeneficiariosVO;
import br.com.bradseg.eedi.dentalindividual.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.dentalindividual.vo.CorretorVO;
import br.com.bradseg.eedi.dentalindividual.vo.DependenteVO;
import br.com.bradseg.eedi.dentalindividual.vo.EnderecoVO;
import br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento;
import br.com.bradseg.eedi.dentalindividual.vo.PagamentoVO;
import br.com.bradseg.eedi.dentalindividual.vo.PlanoVO;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.RepresentanteLegalVO;
import br.com.bradseg.eedi.dentalindividual.vo.SucursalVO;
import br.com.bradseg.eedi.dentalindividual.vo.TelefoneVO;
import br.com.bradseg.eedi.dentalindividual.vo.TipoSucursalSeguradora;
import br.com.bradseg.eedi.dentalindividual.vo.TitularVO;

import com.google.common.base.Strings;

/**
 * Classe responsavel por validra se todos os dados necess�rios para o cadastro de uma proposta foram preenchidos e
 * est�o v�lidos de acordo com as regras.
 */
@Scope("prototype")
@Named("NovaPropostaValidator")
public class NovaPropostaValidator {

	@Autowired
	protected PropostaServiceFacade propostaServiceFacade;

	@Autowired
	protected CepServiceFacade cepServiceFacade;

	@Autowired
	protected SucursalCorretorServiceImpl serviceSucursal;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NovaPropostaValidator.class);

	public void validatePropostaVazia(PropostaVO proposta) {
		Validador validador = new Validador();

		validarDadosCorretor(validador, proposta.getCorretor());

		validarDadosSucursal(validador, proposta.getSucursalSelecionada());

		/**
		 * Valida sucursal REDE
		 */
		if (serviceSucursal.validarSucursalRede(proposta.getSucursalSelecionada().getCodigo())) {
			validador.obrigatorio(proposta.getCorretor().getAgenciaProdutora(), "Ag�ncia Produtora");
			if (StringUtils.isBlank(proposta.getCodigoMatriculaGerente()) && null == proposta.getCorretor().getCpd()) {
				validador.adicionarErro(ConstantesMensagemErro.MSG_ERRO_DADOS_SUCURSAL_REDE);
			}
			if(StringUtils.isBlank(proposta.getCodigoMatriculaGerente()) &&  null == proposta.getCorretorMaster().getCpd()) {
				validador.adicionarErro(ConstantesMensagemErro.MSG_ERRO_DADOS_SUCURSAL_REDE);
			}
		}

		validarDadosPlano(validador, proposta.getPlano());

		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}
	}

	/**
	 * Valida se todas as informa��es necess�rias para finalizar uma proposta foram preenchidas e est�o de acordo com as
	 * regras do sistema. - Validar os dados dos benefici�rios; - Validar os dados do pagamento;
	 * 
	 * @param proposta - informa��es da proposta.
	 */
	public void validate(PropostaVO proposta) {
		Validador validador = new Validador();

		validarDadosCorretor(validador, proposta.getCorretor());

		validarDadosSucursal(validador, proposta.getSucursalSelecionada());
		/**
		 * Sucursal REDE
		 */
		if (serviceSucursal.validarSucursalRede(proposta.getSucursalSelecionada().getCodigo())) {
			validador.obrigatorio(proposta.getCorretor().getAgenciaProdutora(), "Ag�ncia Produtora");
			validador.obrigatorio(proposta.getCodigoPostoAtendimento(), "Codigo Posto de Atendimento");
			if (StringUtils.isBlank(proposta.getCodigoMatriculaGerente()) && null == proposta.getCorretor().getCpd()) {
				validador.adicionarErro(ConstantesMensagemErro.MSG_ERRO_DADOS_SUCURSAL_REDE);
			}
			if(StringUtils.isBlank(proposta.getCodigoMatriculaGerente()) &&  null == proposta.getCorretorMaster().getCpd()) {
				validador.adicionarErro(ConstantesMensagemErro.MSG_ERRO_DADOS_SUCURSAL_REDE);
			}
		}
		//validarDadosBeneficiarios(validador, beneficiarios, plano, pagamento);
		validarDadosPlano(validador, proposta.getPlano());
		validarDadosBeneficiarios(validador, proposta.getBeneficiarios(), proposta.getPlano(), proposta.getPagamento());
		validarDadosPagamento(validador, proposta.getPagamento(), proposta.getBeneficiarios().getTitular());

		
		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}
	}

	private void validarDadosSucursal(Validador validador, SucursalVO validarSucursal) {

		validador.obrigatorio(validarSucursal.getCpdSucursal(), "CPD/Sucursal");

		if (serviceSucursal.validarSucursalCorporate(validarSucursal.getCodigo())) {
			validarSucursal.setTipo(TipoSucursalSeguradora.CORPORATE);
		} else if (serviceSucursal.validarSucursalMercado(validarSucursal.getCodigo())) {
			validarSucursal.setTipo(TipoSucursalSeguradora.MERCADO);
		} else if (serviceSucursal.validarSucursalRede(validarSucursal.getCodigo())) {
			validarSucursal.setTipo(TipoSucursalSeguradora.REDE);
		}

	}

	protected void validarDadosCorretor(Validador validador, CorretorVO corretor) {
		/*boolean cpdPreenchido = validador.obrigatorio(corretor.getCpd(), "CPD/Sucursal");		
		if(cpdPreenchido){
			validador.obrigatorio(corretor.getSucursal(), "CPD/Sucursal");	
		}*/
//		validador.obrigatorio(corretor.getAssistenteProducao(), "Assistente de Produ��o");
	}

	/**
	 * Metodo responsavel por validar os dados do plano.
	 * 
	 * @param validador - mensagens de erro.
	 * @param plano - informa��es do plano.
	 */
	protected void validarDadosPlano(Validador validador, PlanoVO plano) {
		validador.obrigatorio(plano.getCodigo(), "Plano");
	}
	


	/**
	 * Metodo responsavel por validar os dados dos benefici�rios. - Validar os dados do representante legal (quando o
	 * titular form menor de idade); - Validar os dados do titular; - Validar os dados dos dependentes;
	 * 
	 * @param validador - mensagens de erro.
	 * @param beneficiarios - informa��es dos benecifi�rios.
	 * @param Pagamento para validar a data de nascimento do 
	 * respons�vel financeiro. Para Pai e M�e.
	 */
	private void validarDadosBeneficiarios(Validador validador, BeneficiariosVO beneficiarios, PlanoVO plano, PagamentoVO pagamento) {
		validador.obrigatorio(beneficiarios.isTitularMenorIdade(), "O benefici�rio titular � menor de idade");
		if(plano.getCodigo() == 5 || plano.getCodigo() == 6 || plano.getCodigo() == 7 || plano.getCodigo() == 8 || plano.getCodigo() == 16 || 
				plano.getCodigo() == 17 || plano.getCodigo() == 22 || plano.getCodigo() == 10 || plano.getCodigo() == 11 || plano.getCodigo() == 18 
				|| plano.getCodigo() == 19 || plano.getCodigo() == 23 || plano.getCodigo() == 24 || plano.getCodigo() == 20 || plano.getCodigo() == 21 || plano.getCodigo() == 15){
			
				if(beneficiarios.isTitularMenorIdade()){
					validarDadosTitularDenteLeite(validador, beneficiarios.getTitular(), beneficiarios.getRepresentanteLegal(), beneficiarios.isTitularMenorIdade(), plano);
						}else{
							validarDadosTitular(validador, beneficiarios.getTitular(), beneficiarios.isTitularMenorIdade());
					
								if(beneficiarios.getDependentes() != null){
									validarDadosDependentes(validador, beneficiarios.getDependentes(), pagamento.getContaCorrente(), beneficiarios.getTitular());
								}
								if(beneficiarios.getTitular().getGrauParentesco().getCodigo() == 19){
									String cpfOutPoint = String.valueOf(beneficiarios.getTitular().getCpf()).replaceAll("[.,;\\-\\s]", "");
									if(!pagamento.getContaCorrente().getCpf().equals("")){
										validador.verdadeiro(ValidadorProposta.cpfequals(String.valueOf(pagamento.getContaCorrente().getCpf()).replaceAll("[.,;\\-\\s]", ""), cpfOutPoint), ConstantesMensagemErro.MSG_CPF_PROPRIO_IGUAIS_OBRIGATORIO);
									}else{
										validador.verdadeiro(ValidadorProposta.cpfequals(String.valueOf(pagamento.getCartaoCredito().getCpfCartao()).replaceAll("[.,;\\-\\s]", ""), cpfOutPoint), ConstantesMensagemErro.MSG_CPF_PROPRIO_IGUAIS_OBRIGATORIO);
									}
								}else{
									if(beneficiarios.getTitular().getGrauParentesco().getCodigo() == 1){
										String rotulo = "Pai";
										if(pagamento.getContaCorrente() != null){
											validador.falso(ValidadorProposta.cpfequals(pagamento.getContaCorrente().getCpf(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}else{
											validador.falso(ValidadorProposta.cpfequals(pagamento.getCartaoCredito().getCpfCartao(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}
									}else if(beneficiarios.getTitular().getGrauParentesco().getCodigo() == 2){
										String rotulo = "M�e";
										if(pagamento.getContaCorrente() != null){
											validador.falso(ValidadorProposta.cpfequals(pagamento.getContaCorrente().getCpf(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}else{
											validador.falso(ValidadorProposta.cpfequals(pagamento.getCartaoCredito().getCpfCartao(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}
									}else if(beneficiarios.getTitular().getGrauParentesco().getCodigo() == 3){
										String rotulo = "Filha";
										if(pagamento.getContaCorrente().getCpf() != null && !pagamento.getContaCorrente().getCpf().equals("")){
											validador.falso(ValidadorProposta.cpfequals(pagamento.getContaCorrente().getCpf(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}else{
											validador.falso(ValidadorProposta.cpfequals(pagamento.getCartaoCredito().getCpfCartao(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}
								
									}else if(beneficiarios.getTitular().getGrauParentesco().getCodigo() == 4){
										String rotulo = "Filho";
										if(pagamento.getContaCorrente() != null){
											validador.falso(ValidadorProposta.cpfequals(pagamento.getContaCorrente().getCpf(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}else{
											validador.falso(ValidadorProposta.cpfequals(pagamento.getCartaoCredito().getCpfCartao(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}
									}else if(beneficiarios.getTitular().getGrauParentesco().getCodigo() == 5){
										String rotulo = "C�njuge";
										if(pagamento.getContaCorrente() != null){
											validador.falso(ValidadorProposta.cpfequals(pagamento.getContaCorrente().getCpf(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}else{
											validador.falso(ValidadorProposta.cpfequals(pagamento.getCartaoCredito().getCpfCartao(), beneficiarios.getTitular().getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
										}
									}
								}
					
					if (beneficiarios.isTitularMenorIdade()) {
						validarDadosRepresentanteLegal(validador, beneficiarios.getRepresentanteLegal());
					}
			}
				if(beneficiarios.getTitular().getGrauParentesco().getCodigo().equals(1) || beneficiarios.getTitular().getGrauParentesco().getCodigo().equals(2)){
					if(pagamento.getContaCorrente().getDataNascimento() != null){
						validador.verdadeiro(ValidadorProposta.validaDataNascimentoRespFinanceiro(pagamento.getContaCorrente().getDataNascimento(), 
							beneficiarios.getTitular().getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO, beneficiarios.getTitular().getCpf());
					}else{
						validador.verdadeiro(ValidadorProposta.validaDataNascimentoRespFinanceiro(pagamento.getCartaoCredito().getDataNascimento(), 
							beneficiarios.getTitular().getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO, beneficiarios.getTitular().getCpf());
					
					}
			
				}else if(beneficiarios.getTitular().getGrauParentesco().getCodigo().equals(3) || beneficiarios.getTitular().getGrauParentesco().getCodigo().equals(4)){
					if(pagamento.getContaCorrente().getDataNascimento() != null){
					validador.verdadeiro(ValidadorProposta.validaDataNascimentoRespFinanceiroFilho(pagamento.getContaCorrente().getDataNascimento(), 
							beneficiarios.getTitular().getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO_MENOS, beneficiarios.getTitular().getCpf());
				}else{
					validador.verdadeiro(ValidadorProposta.validaDataNascimentoRespFinanceiroFilho(pagamento.getCartaoCredito().getDataNascimento(), 
						beneficiarios.getTitular().getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO_MENOS, beneficiarios.getTitular().getCpf());
				}
			}
		}
	}

	/**
	 * Metodo responsavel por validar os dados do representante legal.
	 * 
	 * @param validador - mensagens de erro.
	 * @param representanteLegal - informa��es do representante legal.
	 */
	private void validarDadosRepresentanteLegal(Validador validador, RepresentanteLegalVO representanteLegal) {
		validador.nome(representanteLegal.getNome(), "Nome do Representante Legal");
		
		if(StringUtils.isNotEmpty(representanteLegal.getCpf())){
			validador.cpf(representanteLegal.getCpf().replaceAll("[.,;\\-\\s]", ""), "CPF do Representante Legal");
		}
		boolean nascimentoPreenchido = validador.obrigatorio(representanteLegal.getDataNascimento(), "Nascimento do Representante Legal");
		if (nascimentoPreenchido) {
			validador.verdadeiro(ValidadorProposta.validarDataMaxima(representanteLegal.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA, "Representante Legal");
			validador.falso(ValidadorProposta.validarSeMenorDeIdade(representanteLegal.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA, "Representante Legal");
		}
		validador.email(representanteLegal.getEmail(), "E-mail do Representante Legal");
		boolean cepPreenchido = validador.obrigatorio(representanteLegal.getEndereco().getCep(), "Cep " + "Representante Legal");
//		validarEndereco(validador, representanteLegal.getEndereco(), "Representante Legal");
		validador.obrigatorio(representanteLegal.getTelefones().get(0).getTipoTelefone(), "Tipo de Telefone do Representante Legal");
		validador.obrigatorio(representanteLegal.getTelefones().get(0).getDddEnumero(), "N� Telefone/Ramal do Representante Legal");
		validador.verdadeiro(ValidacaoUtil.numeroTelefoneValido(representanteLegal.getTelefones().get(0).getNumero()), "msg.erro.cadastro.proposta.numero.telefone.invalido", "Representante Legal");
	}
	
	private void validarDadosTitularDenteLeite(Validador validador, TitularVO titular, RepresentanteLegalVO representanteLegal, boolean indicativoTitularMenorIdade, PlanoVO plano){
		//Represetante Legal
 		validador.nome(representanteLegal.getNome(), "Nome do Representante Legal");
		if(StringUtils.isNotEmpty(representanteLegal.getCpf())){
			validador.cpf(representanteLegal.getCpf().replaceAll("[.,;\\-\\s]", ""), "CPF do Representante Legal");
		}
		boolean nascimentoPreenchido_represetante = validador.obrigatorio(representanteLegal.getDataNascimento(), "Nascimento do Representante Legal");
		if (nascimentoPreenchido_represetante) {
			validador.verdadeiro(ValidadorProposta.validarDataMaxima(representanteLegal.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA, "Representante Legal");
			validador.falso(ValidadorProposta.validarSeMenorDeIdade(representanteLegal.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA, "Representante Legal");
		}
		validador.email(representanteLegal.getEmail(), "E-mail do Representante Legal");
		boolean cepPreenchido = validador.obrigatorio(representanteLegal.getEndereco().getCep(), "Cep " + "Representante Legal");
		//validarEndereco(validador, representanteLegal.getEndereco(), "Representante Legal");
		validador.obrigatorio(representanteLegal.getTelefones().get(0).getTipoTelefone(), "Tipo de Telefone do Representante Legal");
		validador.obrigatorio(representanteLegal.getTelefones().get(0).getDddEnumero(), "N� Telefone/Ramal do Representante Legal");
		validador.verdadeiro(ValidacaoUtil.numeroTelefoneValido(representanteLegal.getTelefones().get(0).getNumero()), "msg.erro.cadastro.proposta.numero.telefone.invalido", "Representante Legal");
		
		//Titular
		validador.nomeCompleto(titular.getNomeMae(), "Nome da M�e do titular");
		validador.nomeCompleto(titular.getNome(), "Nome do titular");
		if(StringUtils.isNotEmpty(titular.getCpf())){
			validador.cpf(titular.getCpf().replaceAll("[.,;\\-\\s]", ""), "CPF do Titular");
			validador.cpfIguaisDenteDeLeite(titular.getCpf().replaceAll("[.,;\\-\\s]", ""),representanteLegal.getCpf().replaceAll("[.,;\\-\\s]", ""), " 1");
		}
		boolean nascimentoPreenchido = validador.obrigatorio(titular.getDataNascimento(), "Nascimento do Titular");
		if(nascimentoPreenchido){
			if(plano.getCodigo() == 15 || plano.getCodigo()== 16 ||plano.getCodigo() == 17 || plano.getCodigo() == 19 || plano.getCodigo() == 18){
				validador.verdadeiro(ValidadorProposta.validarDataMaximaDenteLeite(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA_DENTE_LEITE, Constantes.STRING_TITULAR);
			}else if(plano.getCodigo() == 20 || plano.getCodigo() == 21 || plano.getCodigo() == 22 || plano.getCodigo() == 24 || plano.getCodigo() == 23){
				
				validador.verdadeiro(ValidadorProposta.validarDataMaximaJunior(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA_JUNIOR, Constantes.STRING_TITULAR);
			}else{
				if (indicativoTitularMenorIdade) {
					validador.verdadeiro(ValidadorProposta.validarSeMenorDeIdade(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_TITULAR_DATA_NASCIMENTO_MENOR_DE_DEZOITO, Constantes.STRING_TITULAR);
				} else {
					validador.falso(ValidadorProposta.validarSeMenorDeIdade(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA, Constantes.STRING_TITULAR);
				}
			}
		}
		
		validador.email(titular.getEmail(), "E-mail do Titular");
		validador.obrigatorio(titular.getCpf(), "CPF do Titular");
		validador.obrigatorio(titular.getSexo(), "Sexo do Titular");
		validador.obrigatorio(titular.getEstadoCivil().getCodigo(), "Estado Civil do Titular");
		//validarEndereco(validador, titular.getEndereco(), Constantes.STRING_TITULAR);
		validador.verdadeiro((!Strings.isNullOrEmpty(titular.getTelefones().get(0).getDddEnumero()) || !Strings.isNullOrEmpty(titular.getTelefones().get(1).getDddEnumero()) || !Strings.isNullOrEmpty(titular.getTelefones().get(2).getDddEnumero())), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_TITULAR_TELEFONE_OBRIGATORIO);		
		for(TelefoneVO telefone : titular.getTelefones()){
			if(StringUtils.isEmpty(telefone.getDddEnumero())){
				continue;
			}
			validador.verdadeiro(ValidacaoUtil.numeroTelefoneValido(telefone.getNumero()), "msg.erro.cadastro.proposta.numero.telefone.invalido", "Titular");
		}
	}

	/**
	 * Metodo responsavel por validar os dados do titular.
	 * 
	 * @param validador - mensagens de erro.
	 * @param titular - informa��es do titular.
	 * @param indicativoTitularMenorIdade - indicativo do titular ser menor de idade.
	 */
	private void validarDadosTitular(Validador validador, TitularVO titular, boolean indicativoTitularMenorIdade) {
		validador.nomeCompleto(titular.getNomeMae(), "Nome da M�e do titular");
		validador.nomeCompleto(titular.getNome(), "Nome do titular");
		if(StringUtils.isNotEmpty(titular.getCpf())){
			validador.cpf(titular.getCpf().replaceAll("[.,;\\-\\s]", ""), "CPF do Titular");
		}
		boolean nascimentoPreenchido = validador.obrigatorio(titular.getDataNascimento(), "Nascimento do Titular");
		if (nascimentoPreenchido) {
			validador.verdadeiro(ValidadorProposta.validarDataMaxima(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA, Constantes.STRING_TITULAR);
			if (indicativoTitularMenorIdade) {
				validador.verdadeiro(ValidadorProposta.validarSeMenorDeIdade(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_TITULAR_DATA_NASCIMENTO_MENOR_DE_DEZOITO, Constantes.STRING_TITULAR);
			} else {
				validador.falso(ValidadorProposta.validarSeMenorDeIdade(titular.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA, Constantes.STRING_TITULAR);
			}
		}
		validador.email(titular.getEmail(), "E-mail do Titular");
		validador.obrigatorio(titular.getSexo(), "Sexo do Titular");
		validador.obrigatorio(titular.getEstadoCivil().getCodigo(), "Estado Civil do Titular");
		validarEndereco(validador, titular.getEndereco(), Constantes.STRING_TITULAR);
		validador.verdadeiro((!Strings.isNullOrEmpty(titular.getTelefones().get(0).getDddEnumero()) || !Strings.isNullOrEmpty(titular.getTelefones().get(1).getDddEnumero()) || !Strings.isNullOrEmpty(titular.getTelefones().get(2).getDddEnumero())), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_TITULAR_TELEFONE_OBRIGATORIO);		
		for(TelefoneVO telefone : titular.getTelefones()){
			if(StringUtils.isEmpty(telefone.getDddEnumero())){
				continue;
			}
			validador.verdadeiro(ValidacaoUtil.numeroTelefoneValido(telefone.getNumero()), "msg.erro.cadastro.proposta.numero.telefone.invalido", "Titular");
		}
	}


	/**
	 * Metodo responsavel por validar os dados dos dependentes. - Validar os dados de todos os dependentes.
	 * 
	 * @param validador - mensagens de erro.
	 * @param dependentes - informa��es dos dependentes.
	 */
	private void validarDadosDependentes(Validador validador, List<DependenteVO> dependentes, ContaCorrenteVO responsavelFinanceiro, TitularVO titularInf) {
		//int anoNascimentoRespfinanceiro = contaCorrenteVO.getYear();
		//String rotuloL = null;
		if (!dependentes.isEmpty()) {
			int contadorDependente = 1;
			for (DependenteVO dependente : dependentes) {
				
				validador.verdadeiro((!Strings.isNullOrEmpty(dependente.getNomeMae()) || !Strings.isNullOrEmpty(dependente.getCpf())), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DEPENDENTE_NOME_MAE_OU_CPF_OBRIGATORIO, contadorDependente);
				if (!Strings.isNullOrEmpty(dependente.getNomeMae())) {
					validador.nomeCompleto(dependente.getNomeMae(), "Nome da M�e do Dependente");
				}
				if(dependente.getGrauParentescoBeneficiario().getCodigo().equals(1)){				
					validador.grauParentescoequals(dependente.getGrauParentescoBeneficiario().getCodigo(), titularInf.getGrauParentesco().getCodigo(), "Pai");
				}else if(dependente.getGrauParentescoBeneficiario().getCodigo().equals(2)){
					validador.grauParentescoequals(dependente.getGrauParentescoBeneficiario().getCodigo(), titularInf.getGrauParentesco().getCodigo(), "M�e");

				}else if(dependente.getGrauParentescoBeneficiario().getCodigo().equals(5)){
					validador.grauParentescoequals(dependente.getGrauParentescoBeneficiario().getCodigo(), titularInf.getGrauParentesco().getCodigo(), "Conjuge");

				}else if(dependente.getGrauParentescoBeneficiario().getCodigo().equals(19)){
					validador.grauParentescoequals(dependente.getGrauParentescoBeneficiario().getCodigo(), titularInf.getGrauParentesco().getCodigo(), "Pr�prio");
				}
				if (!Strings.isNullOrEmpty(dependente.getCpf())) {
					validador.cpf(dependente.getCpf(), "CPF do Dependente");
				}
				validador.nomeCompleto(dependente.getNome(), "Nome do Dependente");
				boolean nascimentoPreenchido = validador.obrigatorio(dependente.getDataNascimento(), "Nascimento do Dependente");
				if (nascimentoPreenchido) {
					validador.verdadeiro(ValidadorProposta.validarDataMaxima(dependente.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA, "Dependente");
					//validador.falso(ValidadorDataNascimento.validarSeMenorDeIdade(dependente.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MINIMA, "Dependente[" + contadorDependente + "]");
				}
				if(dependente.getGrauParentescoBeneficiario().getCodigo().equals(1) || dependente.getGrauParentescoBeneficiario().getCodigo().equals(2)){
					if(validador.verdadeiro(ValidadorProposta.validaDataNascimentoRespFinanceiro(responsavelFinanceiro.getDataNascimento(),dependente.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO, dependente.getCpf())){
						break;
					}
					
				
				}else if(dependente.getGrauParentescoBeneficiario().getCodigo().equals(3) || dependente.getGrauParentesco().getCodigo().equals(4)){
					
					if(ValidadorProposta.validaDataNascimentoRespFinanceiroFilho(responsavelFinanceiro.getDataNascimento(), dependente.getDataNascimento()) == false){
						validador.verdadeiro(false, ConstantesMensagemErro.MSG_ERRO_IDADE_DEPENDENTE_RESPONSAVEL_FINANCEIRO, dependente.getCpf());
						break;
					}
				}
				
				validador.obrigatorio(dependente.getGrauParentesco(), "Parentesco do Dependente");
				validador.obrigatorio(dependente.getSexo(), "Sexo do Dependente");
				validador.obrigatorio(dependente.getEstadoCivil().getCodigo(), "Estado Civil do Dependente");
				contadorDependente++;
			}
		}
	}
	
	/*private boolean validaData(){
		validador.verdadeiro(ValidadorDataNascimento.validaDataNascimentoRespFinanceiro(dependente.getDataNascimento(), conta.getDataNascimento()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_DATA_NASCIMENTO_IDADE_MAXIMA, "Dependente");
		return false;
	}*/

	/**
	 * Metodo responsavel por validar os dados do endere�o.
	 * 
	 * @param validador - mensagens de erro.
	 * @param endereco - informa��es do endere�o.
	 * @param tipoPessoa - tipo de pessoa a qual o endere�o pertence.
	 */
	private void validarEndereco(Validador validador, EnderecoVO endereco, String tipoPessoa) {
		boolean cepPreenchido = validador.obrigatorio(endereco.getCep(), "Cep " + tipoPessoa);
		
		if (cepPreenchido) {
			boolean enderecoPreenchido = validador.falso(Strings.isNullOrEmpty(endereco.getLogradouro()) && Strings.isNullOrEmpty(endereco.getBairro()) && Strings.isNullOrEmpty(endereco.getCidade()) && Strings.isNullOrEmpty(endereco.getEstado()), ConstantesMensagemErro.MSG_ERRO_BENEFICIARIOS_CEP_NAO_ENCONTRADO, "Cep " + tipoPessoa);
			if (cepPreenchido && enderecoPreenchido) {
				validador.obrigatorio(endereco.getLogradouro(), "Logradouro " + tipoPessoa);
				validador.obrigatorio(endereco.getNumero(), "N�mero do Endere�o " + tipoPessoa);
				validador.obrigatorio(endereco.getBairro(), "Bairro " + tipoPessoa);	
				//Complemento comentado para impedir a obrigatoriedade do campo por: Kaio 05/08.
				//validador.obrigatorio(endereco.getComplemento(), "Complemento " + tipoPessoa);
				
			}
		}
	}

	/**
	 * Metodo responsavel por validar os dados do pagamento. - Validar o tipod e cobran�a; - Validar os dados banc�rios
	 * do proponente;
	 * 
	 * @param validador - mensagens de erro.
	 * @param pagamento - informa��es do pagamento.
	 */
	private void validarDadosPagamento(Validador validador, PagamentoVO pagamento, TitularVO titular) {
		validador.obrigatorio(pagamento.getCodigoTipoCobranca(), "Tipo de Cobran�a");
		boolean tipoCobrancaPreenchido = validador.obrigatorio(pagamento.getFormaPagamento(), "Forma de Pagamento");
	/*	if(titular.getGrauParentesco().getCodigo() == 1 || titular.getGrauParentesco().getCodigo() == 2 
				|| titular.getGrauParentesco().getCodigo() == 5 ||titular.getGrauParentesco().getCodigo() == 3 || titular.getGrauParentesco().getCodigo() == 4){
			validador.verdadeiro(ValidadorProposta.cpfequals(pagamento.getContaCorrente().getCpf(), titular.getCpf()), ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS);	
		}*/
		
	/*	boolean dataCobrancaPreenchida = validador.obrigatorio(pagamento.getDataCobranca(), "Data de Cobran�a");
		if (dataCobrancaPreenchida) {
			validador.verdadeiro(pagamento.getDataCobranca().isAfter(new LocalDate().plusDays(14))
					&& pagamento.getDataCobranca().isBefore(new LocalDate().plusDays(46)),
					ConstantesMensagemErro.MSG_ERRO_PAGAMENTO_DATA_COBRANCA_ENTRE_15_A_45_DIAS);
		}*/
		if (tipoCobrancaPreenchido) {
		
			if(FormaPagamento.DEBITO_AUTOMATICO.getCodigo().equals(pagamento.getFormaPagamento())|| FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO.getCodigo().equals(pagamento.getFormaPagamento())){
				boolean bancoPreenchido = validador.obrigatorio(pagamento.getContaCorrente().getBanco().getCodigo(), "Banco");
				if (bancoPreenchido) {
					validarCamposProponente(validador, pagamento);
				}
				LOGGER.error("Banco = " + pagamento.getContaCorrente().getBanco().getCodigo());
			}
			else if(FormaPagamento.BOLETO_E_CARTAO_CREDITO.getCodigo().equals(pagamento.getFormaPagamento()) || FormaPagamento.CARTAO_CREDITO.getCodigo().equals(pagamento.getFormaPagamento())){
				validarCamposcartaoCredito(validador, pagamento);
				//validarCampoValidadeDoCartao(validador, pagamento);
			}
			else if(FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.getCodigo().equals(pagamento.getFormaPagamento())){
				boolean bancoPreenchido = validador.obrigatorio(pagamento.getContaCorrente().getBanco().getCodigo(), "Banco");
				if (bancoPreenchido) {
					validarCamposProponente(validador, pagamento);
					validarCamposcartaoCredito(validador, pagamento);
				}
				LOGGER.error("Banco = " + pagamento.getContaCorrente().getBanco().getCodigo());
			}
			
		}
		
	}
	
	public void validarCampoValidadeDoCartao(Validador validador, PagamentoVO pagamento) {
		if(FormaPagamento.CARTAO_CREDITO.getCodigo().equals(pagamento.getFormaPagamento())) {
			boolean campoValidadePreenchido = validador.obrigatorio(pagamento.getCartaoCredito().getValidade() , "Validade do cart�o");
			
			if(campoValidadePreenchido) {
				LocalDate dataAtual = new LocalDate();
				
				String dataValidadeString = pagamento.getCartaoCredito().getValidade();
				String dataSeparada[] = new String[2];
				dataSeparada = dataValidadeString.split("/");
				
				Integer mes = Integer.parseInt(dataSeparada[0]);
				Integer ano = Integer.parseInt(dataSeparada[1]);
				
	        	if((mes > 12 || mes < 1) || ano < dataAtual.getYear()) {
	        		validador.validaDataValidadeCartao("Validade do Cart�o");
	        	} 
			}
		}
	}
	
	public void validarCamposProponente(Validador validador, PagamentoVO pagamento){
		
		validador.obrigatorio(pagamento.getContaCorrente().getNumeroAgencia(), "Ag�ncia do Proponente");
		validador.obrigatorio(pagamento.getContaCorrente().getNumeroConta(), "N�mero da Conta do Proponente");
		validador.verdadeiro(!Strings.isNullOrEmpty(pagamento.getContaCorrente().getDigitoVerificadorConta()), ConstantesMensagemErro.MSG_ERRO_CAMPO_OBRIGATORIO, "D�gito da Conta do Proponente");
		//validador.obrigatorio(pagamento.getContaCorrente().getDigitoVerificadorConta(), "D�gito da Conta do Proponente");
		//validador.cpf(pagamento.getContaCorrente().getCpf(), "CPF do Proponente");
		validador.nome(pagamento.getContaCorrente().getNome(), "Nome do Proponente");
		validador.obrigatorio(pagamento.getContaCorrente().getDataNascimento(), "Nascimento do Proponente");	
	}
	
	public void validarCamposcartaoCredito(Validador validador, PagamentoVO pagamento){
		validador.nomeCompletoCartaoCredito(pagamento.getCartaoCredito().getNomeCartao());
		validador.obrigatorio(pagamento.getCartaoCredito().getNomeCartao()  , "Nome do cart�o");
		validador.obrigatorio(pagamento.getCartaoCredito().getNumeroCartao(), "N�mero do cart�o");
		validador.obrigatorio(pagamento.getCartaoCredito().getBandeira(), "Bandeira do cart�o");
		validador.obrigatorio(pagamento.getCartaoCredito().getCpfCartao(), "N�mero do CPF");
	}

}