package br.com.bradseg.eedi.dentalindividual.validador;

import org.joda.time.LocalDate;
import org.joda.time.Period;

import com.ibm.websphere.pmi.client.CpdDouble;

import br.com.bradseg.eedi.dentalindividual.util.Constantes;

/**
 * Verifica se a data de nascimento � v�lida de acordo com as regras definidas
 */
public class ValidadorProposta {
	
	/*
	 * M�todo respons�vel por validar data de nascimento para dente de leite 
	 * 
	 * */
	
	public static boolean validaDataNascimentoRespFinanceiro(LocalDate dataNascimentoRespFinanceiro , LocalDate dataNascimentoTitular){
		LocalDate dataAtual = new LocalDate();
		int anoNascimentoTitular = dataNascimentoTitular.getYear();
		int anoNascimentoRespFinanceiro = dataNascimentoRespFinanceiro.getYear(); 
		int anoAtual = dataAtual.getYear();
		int idadeAtualRespFinanceiro = anoAtual - anoNascimentoRespFinanceiro;
		int idadeAtualTitular = anoAtual - anoNascimentoTitular;
		
		
		if(idadeAtualTitular - idadeAtualRespFinanceiro > Constantes.IDADE_PERMITADA_DIFERENCA_RESPONSAVEL_FINANCEIRO){
			return true;
		}
		
		return false;
		
	}
	
	public static boolean validaDataNascimentoRespFinanceiroFilho(LocalDate dataNascimentoRespFinanceiro , LocalDate dataNascimentoTitular){
		LocalDate dataAtual = new LocalDate();
		int anoNascimentoTitular = dataNascimentoTitular.getYear();
		int anoNascimentoRespFinanceiro = dataNascimentoRespFinanceiro.getYear(); 
		int anoAtual = dataAtual.getYear();
		int idadeAtualRespFinanceiro = anoAtual - anoNascimentoRespFinanceiro;
		int idadeAtualTitular = anoAtual - anoNascimentoTitular;
		
		if(idadeAtualRespFinanceiro - idadeAtualTitular > Constantes.IDADE_PERMITADA_DIFERENCA_RESPONSAVEL_FINANCEIRO){
			return true;
		}
		
		return false;
	}
	
/*	public static boolean cpfProprio(String cpfProponente, String cpfTitular){
		if(cpfProponente.equals(cpfTitular)){
			return true;
		}else{
			return false;
		}
	}*/
	
	public static boolean cpfequals(String cpf, String cpfDep){
		if (cpf.equals(cpfDep)) {
			return true;
		} else {
			return false;
		}
}
	
	public static boolean validarDataMaximaDenteLeite(LocalDate dataNascimento){
		LocalDate dataAtual = new LocalDate();
		int mesDataNascimentoUser = dataNascimento.getMonthOfYear();
		int anoDataNascimento = dataNascimento.getYear();
		int mesNascimentoAtual = dataAtual.getMonthOfYear();
		int anoNascimentoAtual = dataAtual.getYear();
		int diaNascimentoUser = dataNascimento.getDayOfMonth();
		int diaNascAtual = dataAtual.getDayOfMonth();
    	
		if((anoNascimentoAtual - anoDataNascimento) < Constantes.ANO_MAXIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_DENTE_LEITE){		
			return true;
    	}else if(anoNascimentoAtual - anoDataNascimento == Constantes.ANO_MAXIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_DENTE_LEITE){	
    			if(mesDataNascimentoUser > mesNascimentoAtual){
	    			return true;
    			}else if(mesDataNascimentoUser == mesNascimentoAtual){
    				if(diaNascAtual < diaNascimentoUser){
    					return true;
    				}else{
    					return false;
    				}
    			}else{
    				return false;
    			}
    		}
    		return false;
	}
	public static boolean validarDataMaximaJunior(LocalDate dataNascimento){
		
		LocalDate dataAtual = new LocalDate();
		int mesDataNascimentoUser = dataNascimento.getMonthOfYear();
		int anoDataNascimentoJunior = dataNascimento.getYear();
		int mesNascimentoAtual = dataAtual.getMonthOfYear();
		int anoNascimentoAtualJunior = dataAtual.getYear();
		int diaNascimentoUserJunior = dataNascimento.getDayOfMonth();
		int diaNascAtual = dataAtual.getDayOfMonth();
    	
		if((anoNascimentoAtualJunior - anoDataNascimentoJunior >= Constantes.ANO_MINIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_JUNIOR)){
			
			if(anoNascimentoAtualJunior - anoDataNascimentoJunior < Constantes.ANO_MAXIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_JUNIOR){
				
				if(anoNascimentoAtualJunior - anoDataNascimentoJunior == Constantes.ANO_MINIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_JUNIOR){
				
					if(mesNascimentoAtual == mesDataNascimentoUser){
					
						if(diaNascimentoUserJunior <= diaNascAtual){
    			
							return true;
						}
					}else if(mesNascimentoAtual > mesDataNascimentoUser){
						return true;
					}
				}else if(anoNascimentoAtualJunior - anoDataNascimentoJunior < Constantes.ANO_MAXIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_JUNIOR){
					return true;
				}else{
					return false;
				}
				return false;
			}else if(anoNascimentoAtualJunior - anoDataNascimentoJunior == Constantes.ANO_MAXIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL_JUNIOR){
					if(mesNascimentoAtual < mesDataNascimentoUser){
						return true;
					}else if(mesNascimentoAtual == mesDataNascimentoUser){
						if(diaNascimentoUserJunior >= diaNascAtual){
							return true;
						}
					}else{
						return false;
					}
			}else{
				return false;
			}
		}
	return false;
	}

/**

	 * Verifica se a data de nascimento � v�lida. Para ser v�lido, a data de nascimento precisa seguir as seguintes regras:
	 * 	- Ser no m�ximo no dia atual;
	 * 	- Possuir no m�nimo 150 anos a partir de hoje;
	 * @param dataNascimento Data de nascimento
	 * @return verdadeiro se a data for v�lida
	 */
	public static boolean validarDataMaxima(LocalDate dataNascimento) {
		LocalDate dataAtual = new LocalDate();
		return dataNascimento.isBefore(dataAtual.plusDays(1)) && dataNascimento.isAfter(dataAtual.minusYears(Constantes.ANO_MINIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL));
	}
	
	public static boolean validarSeMenorDeIdade(LocalDate dataNascimento){
		return (new Period(dataNascimento, new LocalDate()).getYears() < 18);
	}
}