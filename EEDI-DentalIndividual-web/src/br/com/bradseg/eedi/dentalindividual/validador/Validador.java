package br.com.bradseg.eedi.dentalindividual.validador;

import java.text.MessageFormat;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.bsad.framework.core.validator.Validators;
import br.com.bradseg.eedi.dentalindividual.util.ConstantesMensagemErro;

import com.google.common.collect.Lists;

/**
 * Classe utilizada para auxiliar na valida��o de regras. 
 * 
 */
public class Validador {

	private static final Pattern PADRAO_CARACTERES_NAO_PERMITIDOS_NOME = Pattern.compile("[^'\\-\\.\\s\\pL\\pM\\p{Nl}[\\p{InEnclosedAlphanumerics}&&\\p{So}][��������������������������]]");
	private static final Pattern PADRAO_CARACTERES_NAO_PERMITIDOS_COMPLEMENTO = Pattern.compile("[��������������������������!?^:;|/@#$%�&*+-]");
	private static final Pattern PADRAO_EMAIL_VALIDO = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	private static final Pattern PADRAO_DDD_VALIDO = Pattern.compile("(10)|([1-9][1-9])");
	private static final Pattern PADRAO_NUMERO_TELEFONE_VALIDO = Pattern.compile("[2-9][0-9]{7,8}");

	private List<Message> mensagens = Lists.newArrayList();

	/**
	 * Adiciona uma mensagem de erro na lista de mensagens
	 * 
	 * @param messageKey Chave da mensagem
	 * @param params Par�metros da mensagem
	 */
	public void adicionarErro(String messageKey, Object... params) {
		mensagens.add(new Message(messageKey, Message.ERROR_TYPE, params));
	}

	/**
	 * Indica se existe algum erro na valida��o
	 * @return true se existir erro na valida��o
	 */
	public boolean hasError() {
		return !mensagens.isEmpty();
	}

	/**
	 * Retorna as mensagens de erro
	 * 
	 * @return Mensagens de erro
	 */
	public List<Message> getMessages() {
		return mensagens;
	}

	/**
	 * Valida se uma determinada string foi informada.
	 * 
	 * @param valor Valor String
	 * @param rotulo R�tulo do campo
	 * @return true se a string informada passar na regra de obrigatoriedade
	 */
	public boolean obrigatorio(String valor, String rotulo) {
		if (StringUtils.isBlank(valor) || "0".equals(valor)) {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CAMPO_OBRIGATORIO, rotulo);
		} else {
			return true;
		}
		return false;
	}

	/**
	 * Valida se um determinado objeto foi informado.
	 * 
	 * @param objeto Objeto
	 * @param rotulo R�tulo do campo
	 * @return true se o objeto informado passar na regra de obrigatoriedade
	 */
	public boolean obrigatorio(Object objeto, String rotulo) {
		if (objeto == null || objeto.equals(0) || objeto.equals(0L)) {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CAMPO_OBRIGATORIO, rotulo);
		} else {
			return true;
		}
		return false;
	}

	/**
	 * Valida se um cpf foi preenchido e se est� de acordo com as regras de cpf v�lido (11 caracteres, d�gito verificador v�lido).
	 * 
	 * @param cpf Valor do cpf
	 * @param rotulo R�tulo do campo
	 * @return true se o cpf passar nas regras
	 */
	public boolean cpf(String cpf, String rotulo) {
		if (obrigatorio(cpf, rotulo)) {
			if (!Validators.value(cpf).isCpf().validate()) {
				adicionarErro(ConstantesMensagemErro.MSG_ERRO_CPF_INVALIDO, rotulo);
			} else {
				return true;
			}
		}
		return false;
	}

/*	public boolean cnpj(String cnpj, String rotulo) {
		if (obrigatorio(cnpj, rotulo)) {
			if (!Validators.value(cnpj).isCnpj().validate()) {
				adicionarErro("msg.erro.cnpj.invalido", rotulo);
			} else {
				return true;
			}
		}
		return false;
	}
	
	public boolean cpfCnpjCorpo(String cpfCnpj, String rotulo) {
		if (obrigatorio(cpfCnpj, rotulo)) {
			cpfCnpj = Strings.removerCaracteresNaoNumericos(cpfCnpj);
			if(cpfCnpj.length() == 8 || cpfCnpj.length() == 11 || cpfCnpj.length() == 14){
				return true;
			}
			return false;
		}
		return false;
	}

	public boolean cpfCnpj(String cpfCnpj, String rotulo) {
		if (obrigatorio(cpfCnpj, rotulo)) {
			cpfCnpj = Strings.removerCaracteresNaoNumericos(cpfCnpj);
			if (Validators.value(cpfCnpj).isCpf().validate()) {
				return cpf(cpfCnpj, rotulo);
			} else {
				return cnpj(cpfCnpj, rotulo);
			}
		}
		return false;
	}*/

	/**
	 * Valida as regras de um nome completo.
	 * <ul>
	 * 	<li>Deve ser preenchido</li>
	 * 	<li>N�o pode possuir caracteres especiais. Por exemplo: @, #, (, %, etc.</li>
	 * 	<li>Precisa ser nome completo (mais de 2 palavras)</li>
	 * 	<li>O primeiro nome precisa ter mais de 2 letras</li>
	 * 	<li>O �ltimo nome precisa ter mais de 2 letras</li>
	 * </ul>
	 * 
	 * @param nome Nome
	 * @param rotulo R�tulo do campo
	 * @return true se o nome completo passar nas regras
	 */
	public boolean nomeCompleto(String nome, String rotulo) {
		if (nome(nome, rotulo)) {
			// Remove os v�rios espa�os em branco transformando em apenas um (ex.: "Teste     da Silva" -> Teste da Silva)
			nome = nome.replaceAll("\\t", " ").replaceAll("\\s+", " ").trim();
			String[] nomes = nome.split("\\s");
			if (nomes.length < 2) {
				adicionarErro(ConstantesMensagemErro.MSG_ERRO_NOME_INCOMLETO, rotulo);
			} else {
				// Nao pode abreviar nenhuma parte do nome
				for (int i = 0; i < nomes.length; i++) {
					//Se for um nome com somente "e", considera como v�lido
					if (nomes[i].length() < 2 && !nomes[i].equalsIgnoreCase("E")) {
						adicionarErro(ConstantesMensagemErro.MSG_ERRO_NOME_INCOMLETO, rotulo);
						return false;
					} else if(!nomes[i].matches("[a-zA-Z��������������������������]*$")){
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
	
	public boolean nomeCompletoCartaoCredito(String nome) {
		nome = nome.replaceAll("\\t", " ").replaceAll("\\s+", " ").trim();
		String[] nomes = nome.split("\\s");
		
		for (int i = 0; i < nomes.length; i++) {
			if(nomes[i].matches("^.*[�������������������������������].*$")){
		    	adicionarErro(ConstantesMensagemErro.MSG_ERRO_NOME_CARACTER_INVALIDO, "Nome no cart�o");
		    	return false;
			}	
	   }
		    			
	return true;
}

	/**
	 * Valida as regras de um nome (n�o precisa ser completo). Ex.: Jos�
	 * <ul>
	 * 	<li>Deve ser preenchido</li>
	 * 	<li>N�o pode possuir caracteres especiais. Por exemplo: @, #, (, %, etc.</li>
	 * 	<li>O primeiro nome precisa ter mais de 2 letras</li>
	 * 	<li>O �ltimo nome precisa ter mais de 2 letras</li>
	 * </ul>
	 * @param nome Nome
	 * @param rotulo R�tulo do campo
	 * @return true se o nome passar nas regras
	 */
	public boolean nome(String nome, String rotulo) {
		if (possuiCaracteresValidos(nome, rotulo)) {
			return true;
		}
		return false;
	}
	

	
	public boolean grauParentescoequals(Integer grauParentescoTitular, Integer grauParentescoProponente, String rotulo){
		if (grauParentescoTitular.equals(grauParentescoProponente)) {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_GRAUPARENTESCO_IGUAIS, rotulo);
		} else {
			return true;
		}
	return true;
}
	
	public boolean cpfIguaisDenteDeLeite(String cpf, String cpf_dep, String rotulo){
		if (cpf.equals(cpf_dep)) {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
		} else {
			return true;
		}
	return true;
}
	
	public static boolean cpfIguais(String cpf, String cpf_dep){
		if (cpf.equals(cpf_dep)) {
			
			return true;
			//adicionarErro(ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS, rotulo);
		} else {
			return false;
		}
}
	
	
	public boolean cpfIguaisTitularProponente(String cpf, String cpf_dep, String rotulo){
		if (cpf.equals(cpf_dep)) {
			return true;
		} else {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CPF_IGUAIS_PROPONENTE, rotulo);
		}
	return true;
}
	/*
	 * vali nome sem ser obrigat�rio
	 * 
	 */
	
	public boolean nomeNobrigatorio(String nome, String rotulo){
		if(possuiCaracteresValidosNobrigatorio(nome, rotulo)){
			return true;
		}
		return false;
	}
	
	public boolean possuiCaracteresValidosNobrigatorio(String nome, String rotulo){
		if (PADRAO_CARACTERES_NAO_PERMITIDOS_COMPLEMENTO.matcher(nome).find()) {
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_NOME_CARACTER_INVALIDO, rotulo);
		} else {
			return true;
		}
		return false;
	}

	/**
	 * Valida se o nome possui apenas caracters v�lidos.
	 * 
	 * @param nome Nome
	 * @param rotulo R�tulo do campo
	 * @return true se o nome n�o possuir caracteres especiais
	 */
	public boolean possuiCaracteresValidos(String nome, String rotulo) {
		if (obrigatorio(nome, rotulo)) {
			if (PADRAO_CARACTERES_NAO_PERMITIDOS_NOME.matcher(nome).find()) {
				adicionarErro(ConstantesMensagemErro.MSG_ERRO_NOME_CARACTER_INVALIDO, rotulo);
			} else {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verifica se o email informado � v�lido e adiciona mensagem de erro caso contr�rio.
	 * Considera v�lido o seguinte padr�o:
	 * 	xxxxx@xxx.com
	 * 	xxxxx@xxx.com.br
	 * 
	 * @param email Email
	 * @param rotulo R�tulo do campo
	 * @return true se o email for v�lido
	 */
	public boolean email(String email, String rotulo) {
		if(StringUtils.isBlank(email)){
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_CAMPO_OBRIGATORIO, rotulo);
		}
		else if (StringUtils.isNotBlank(email) && PADRAO_EMAIL_VALIDO.matcher(email).matches()) {
			return true;
		}else{
			adicionarErro(ConstantesMensagemErro.MSG_ERRO_EMAIL_INVALIDO, rotulo);
		}
		return false;
	}

	/**
	 * Valida se uma determinada express�o � verdadeira.
	 * 
	 * @param expressao Express�o a ser validada
	 * @param messageKey Chave da mensagem
	 * @param params Par�metros da mensagem
	 * @return true se a express�o for verdadeira
	 */
	public boolean verdadeiro(boolean expressao, String messageKey, Object... params) {
		if (expressao) {
			return true;
		}
		adicionarErro(messageKey, params);
		return false;
	}

	/**
	 * Valida se uma determinada express�o � falsa.
	 * 
	 * @param expressao Express�o a ser validada
	 * @param messageKey Chave da mensagem
	 * @param params Par�metros da mensagem
	 * @return true se a express�o for falsa
	 */
	public boolean falso(boolean expressao, String messageKey, Object... params) {
		return verdadeiro(!expressao, messageKey, params);
	}
	
	public boolean cpfCnpj(String cpfCnpj, String rotulo) {
		if (obrigatorio(cpfCnpj, rotulo)) {

			cpfCnpj = cpfCnpj.replaceAll("[^\\d]", "");

			if (Validators.value(cpfCnpj).isCpf().validate()) {
				return cpf(cpfCnpj, rotulo);
			} else {
				return cnpj(cpfCnpj, rotulo);
			}
		}
		return false;
	}
	

	public boolean cnpj(String cnpj, String rotulo) {
		if (obrigatorio(cnpj, rotulo)) {
			if (!Validators.value(cnpj).isCnpj().validate()) {
				adicionarErro("msg.erro.cnpj.invalido", rotulo);
			} else {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Verifica se o DDD do telefone informado � v�lido e adiciona mensagem de erro caso contr�rio. Considera v�lido o
	 * seguinte padr�o: - DDD = 10 - Algum n�mero iniciando de 1 a 9 e terminando de 1 a 9
	 * 
	 * @param ddd DDD do telefone
	 * @param rotulo R�tulo do campo
	 * @return true se o email for v�lido
	 */
	public boolean ddd(String ddd, String rotulo) {
		if (ddd != null && PADRAO_DDD_VALIDO.matcher(String.valueOf(ddd)).matches()) {
			return true;
		}
		adicionarErro("msg.erro.cadastro.proposta.telefone.ddd.invalido", rotulo);
		return false;
	}
	
	/**
	 * Verifica se o n�mero do telefone informado � v�lido e adiciona mensagem de erro caso contr�rio. Considera v�lido
	 * o seguinte padr�o: - Primeiro d�gito de 2 a 9; - Seguido de 7 ou 8 d�gitos de 0 a 9;
	 * 
	 * @param numeroTelefone N�mero do telefone
	 * @param rotulo R�tulo do campo
	 * @return true se o n�mero do telefone for v�lido
	 */
	public boolean numeroTelefone(String numeroTelefone, String rotulo) {
		if (obrigatorio(numeroTelefone, MessageFormat.format("N�mero do telefone do {0}", rotulo))) {
			return verdadeiro(PADRAO_NUMERO_TELEFONE_VALIDO.matcher(numeroTelefone).matches(),
					"msg.erro.cadastro.proposta.numero.telefone.invalido", rotulo);
		}
		return false;
	}
	
	public void validaDataValidadeCartao(String rotulo) {
		adicionarErro("msg.erro.proposta.beneficiario.cartao.validade.invalido", rotulo);
	}

}