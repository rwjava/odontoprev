package br.com.bradseg.eedi.dentalindividual.ccrr.facade;

import br.com.bradseg.eedi.dentalindividual.vo.CorretorVO;

public interface DadosCorretorFacade {
	
	/**
	 * 
	 * @param corretor
	 * @return
	 */
	public CorretorVO listarDadosCorretor(CorretorVO corretor);
	
}
