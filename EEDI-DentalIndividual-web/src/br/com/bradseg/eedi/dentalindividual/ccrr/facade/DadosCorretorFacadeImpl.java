package br.com.bradseg.eedi.dentalindividual.ccrr.facade;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice.CorretorServiceSessionFacade;
import br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.webservice.WSCorretorVO;
import br.com.bradseg.eedi.dentalindividual.util.ValidacaoUtil;
import br.com.bradseg.eedi.dentalindividual.vo.CorretorVO;

@Service
@Transactional(propagation=Propagation.NOT_SUPPORTED)
public class DadosCorretorFacadeImpl implements DadosCorretorFacade {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DadosCorretorFacadeImpl.class);
	
	@Autowired
	private CorretorServiceSessionFacade corretorServiceSessionFacade;
	
	public CorretorVO listarDadosCorretor(CorretorVO corretor) {
		try {
			Integer cpdCorretor = corretor.getCpd().intValue();
			return converterDadosCorretor(corretorServiceSessionFacade.obterDadosCorretorPorCpd(corretor.getSucursal(), cpdCorretor));
		} catch (Exception e) {
			LOGGER.error("Erro ao obter dados do Corretor " + e.getMessage());
			try{
				Integer cpdCorretor = corretor.getCpd().intValue();
				return converterDadosCorretor(corretorServiceSessionFacade.obterDadosCorretorPorCpd(corretor.getSucursal(), cpdCorretor));
			}
			catch(Exception e1){
				LOGGER.error("Erro ao obter dados do Corretor " + e.getMessage());
				if(e1 instanceof BusinessException){
					throw new BusinessException("Erro ao obter dados do Corretor: " +((BusinessException) e1).getMessages());
				}
				throw new IntegrationException("Erro ao obter dados do Corretor: " + e.getMessage());

			}
			
			
		}		
	}
	


	public CorretorVO converterDadosCorretor(WSCorretorVO corretorVO) {
		CorretorVO dadosCorretor = new CorretorVO();
		
		if(corretorVO != null 
				&&  StringUtils.isNotBlank(corretorVO.getNomeCorretor()) 
				&& ValidacaoUtil.validarCNFCNPJ(String.valueOf(corretorVO.getCodigoCpfCnpjCorretor()))){
			dadosCorretor.setNome(corretorVO.getNomeCorretor());
			dadosCorretor.setCpd(corretorVO.getCodigoCPD());

		}else{
			List<Message> listaDeErros = new ArrayList<Message>();
			Message mensagem = new Message();
			mensagem.setKey("msg.erro.cadastro.proposta.vazia.cpd.corretor.master.invalido.sucursal.selecionada");
			mensagem.setMessage("Cpd Corretor Master inv�lido para sucursal selecionada.");
			listaDeErros.add(mensagem);			
			throw new BusinessException(listaDeErros);
		}
		LOGGER.error(String.format("Corretor: [%s]", dadosCorretor.getNome()));
		return dadosCorretor;
	}

}
