
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listarParaRelatorioAcompanhamento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listarParaRelatorioAcompanhamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filtro" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}filtroRelatorioAcompanhamentoVO" minOccurs="0"/>
 *         &lt;element name="login" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}loginVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listarParaRelatorioAcompanhamento", propOrder = {
    "filtro",
    "login"
})
public class ListarParaRelatorioAcompanhamento {

    protected FiltroRelatorioAcompanhamentoVO filtro;
    protected LoginVO login;

    /**
     * Gets the value of the filtro property.
     * 
     * @return
     *     possible object is
     *     {@link FiltroRelatorioAcompanhamentoVO }
     *     
     */
    public FiltroRelatorioAcompanhamentoVO getFiltro() {
        return filtro;
    }

    /**
     * Sets the value of the filtro property.
     * 
     * @param value
     *     allowed object is
     *     {@link FiltroRelatorioAcompanhamentoVO }
     *     
     */
    public void setFiltro(FiltroRelatorioAcompanhamentoVO value) {
        this.filtro = value;
    }

    /**
     * Gets the value of the login property.
     * 
     * @return
     *     possible object is
     *     {@link LoginVO }
     *     
     */
    public LoginVO getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoginVO }
     *     
     */
    public void setLogin(LoginVO value) {
        this.login = value;
    }

}
