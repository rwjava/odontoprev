
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for filtroAcompanhamentoVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="filtroAcompanhamentoVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoAgenciaDebito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoAgenciaProdutora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoAssistente" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codigoGerenteProdutoBVP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codigoProposta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoStatusProposta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cpdCorretor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cpfBeneficiarioTitular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpfCnpjCorretor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="filtroPeriodoVO" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}filtroPeriodoVO" minOccurs="0"/>
 *         &lt;element name="protocolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "filtroAcompanhamentoVO", propOrder = {
    "codigoAgenciaDebito",
    "codigoAgenciaProdutora",
    "codigoAssistente",
    "codigoGerenteProdutoBVP",
    "codigoProposta",
    "codigoStatusProposta",
    "cpdCorretor",
    "cpfBeneficiarioTitular",
    "cpfCnpjCorretor",
    "filtroPeriodoVO",
    "protocolo",
    "sucursal"
})
public class FiltroAcompanhamentoVO {

    protected String codigoAgenciaDebito;
    protected String codigoAgenciaProdutora;
    protected Integer codigoAssistente;
    protected Integer codigoGerenteProdutoBVP;
    protected String codigoProposta;
    protected Integer codigoStatusProposta;
    protected Integer cpdCorretor;
    protected String cpfBeneficiarioTitular;
    protected String cpfCnpjCorretor;
    protected FiltroPeriodoVO filtroPeriodoVO;
    protected String protocolo;
    protected Integer sucursal;

    /**
     * Gets the value of the codigoAgenciaDebito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAgenciaDebito() {
        return codigoAgenciaDebito;
    }

    /**
     * Sets the value of the codigoAgenciaDebito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAgenciaDebito(String value) {
        this.codigoAgenciaDebito = value;
    }

    /**
     * Gets the value of the codigoAgenciaProdutora property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAgenciaProdutora() {
        return codigoAgenciaProdutora;
    }

    /**
     * Sets the value of the codigoAgenciaProdutora property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAgenciaProdutora(String value) {
        this.codigoAgenciaProdutora = value;
    }

    /**
     * Gets the value of the codigoAssistente property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoAssistente() {
        return codigoAssistente;
    }

    /**
     * Sets the value of the codigoAssistente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoAssistente(Integer value) {
        this.codigoAssistente = value;
    }

    /**
     * Gets the value of the codigoGerenteProdutoBVP property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoGerenteProdutoBVP() {
        return codigoGerenteProdutoBVP;
    }

    /**
     * Sets the value of the codigoGerenteProdutoBVP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoGerenteProdutoBVP(Integer value) {
        this.codigoGerenteProdutoBVP = value;
    }

    /**
     * Gets the value of the codigoProposta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoProposta() {
        return codigoProposta;
    }

    /**
     * Sets the value of the codigoProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoProposta(String value) {
        this.codigoProposta = value;
    }

    /**
     * Gets the value of the codigoStatusProposta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoStatusProposta() {
        return codigoStatusProposta;
    }

    /**
     * Sets the value of the codigoStatusProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoStatusProposta(Integer value) {
        this.codigoStatusProposta = value;
    }

    /**
     * Gets the value of the cpdCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCpdCorretor() {
        return cpdCorretor;
    }

    /**
     * Sets the value of the cpdCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCpdCorretor(Integer value) {
        this.cpdCorretor = value;
    }

    /**
     * Gets the value of the cpfBeneficiarioTitular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfBeneficiarioTitular() {
        return cpfBeneficiarioTitular;
    }

    /**
     * Sets the value of the cpfBeneficiarioTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfBeneficiarioTitular(String value) {
        this.cpfBeneficiarioTitular = value;
    }

    /**
     * Gets the value of the cpfCnpjCorretor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpfCnpjCorretor() {
        return cpfCnpjCorretor;
    }

    /**
     * Sets the value of the cpfCnpjCorretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpfCnpjCorretor(String value) {
        this.cpfCnpjCorretor = value;
    }

    /**
     * Gets the value of the filtroPeriodoVO property.
     * 
     * @return
     *     possible object is
     *     {@link FiltroPeriodoVO }
     *     
     */
    public FiltroPeriodoVO getFiltroPeriodoVO() {
        return filtroPeriodoVO;
    }

    /**
     * Sets the value of the filtroPeriodoVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link FiltroPeriodoVO }
     *     
     */
    public void setFiltroPeriodoVO(FiltroPeriodoVO value) {
        this.filtroPeriodoVO = value;
    }

    /**
     * Gets the value of the protocolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocolo() {
        return protocolo;
    }

    /**
     * Sets the value of the protocolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocolo(String value) {
        this.protocolo = value;
    }

    /**
     * Gets the value of the sucursal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSucursal() {
        return sucursal;
    }

    /**
     * Sets the value of the sucursal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSucursal(Integer value) {
        this.sucursal = value;
    }

}
