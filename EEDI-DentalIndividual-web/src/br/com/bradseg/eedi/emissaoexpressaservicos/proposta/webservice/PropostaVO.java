
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import org.joda.time.LocalDate;


/**
 * <p>Java class for propostaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="propostaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agenciaProdutora" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}agenciaBancariaVO" minOccurs="0"/>
 *         &lt;element name="angariador" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}angariadorVO" minOccurs="0"/>
 *         &lt;element name="banco" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}bancoVO" minOccurs="0"/>
 *         &lt;element name="canalVenda" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}canalVendaVO" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoFormatado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoMatriculaAssistente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoMatriculaGerente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoResponsavelUltimaAtualizacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="corretor" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}corretorVO" minOccurs="0"/>
 *         &lt;element name="corretorMaster" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}corretorMasterVO" minOccurs="0"/>
 *         &lt;element name="dataAdesao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataCriacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataInicioCobranca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataUltimaAtualizacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dependentes" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}dependenteVO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="diaVencimento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="formaPagamento" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}formaPagamento" minOccurs="0"/>
 *         &lt;element name="movimento" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}movimentoPropostaVO" minOccurs="0"/>
 *         &lt;element name="numeroSequencial" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="origem" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}origem" minOccurs="0"/>
 *         &lt;element name="planoVO" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}planoVO" minOccurs="0"/>
 *         &lt;element name="proponente" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}proponenteVO" minOccurs="0"/>
 *         &lt;element name="protocolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="responsavelLegal" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}responsavelLegalVO" minOccurs="0"/>
 *         &lt;element name="subsegmentacao" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}subsegmentacao" minOccurs="0"/>
 *         &lt;element name="sucursalSeguradora" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}sucursalSeguradoraVO" minOccurs="0"/>
 *         &lt;element name="tipoCobranca" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}tipoCobranca" minOccurs="0"/>
 *         &lt;element name="titular" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}titularVO" minOccurs="0"/>
 *         &lt;element name="codigoPostoAtendimento" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "propostaVO", propOrder = {
    "agenciaProdutora",
    "angariador",
    "banco",
    "canalVenda",
    "codigo",
    "codigoFormatado",
    "codigoMatriculaAssistente",
    "codigoMatriculaGerente",
    "codigoResponsavelUltimaAtualizacao",
    "corretor",
    "corretorMaster",
    "dataAdesao",
    "dataCriacao",
    "dataInicioCobranca",
    "dataUltimaAtualizacao",
    "dependentes",
    "diaVencimento",
    "formaPagamento",
    "movimento",
    "numeroSequencial",
    "origem",
    "planoVO",
    "proponente",
    "protocolo",
    "responsavelLegal",
    "subsegmentacao",
    "sucursalSeguradora",
    "tipoCobranca",
    "titular",
    "dataValidadeProposta",
    "dataEmissao",
    "codigoPostoAtendimento"
})
@XmlSeeAlso({
    PropostaCorretorVO.class
})
public class PropostaVO {

    protected AgenciaBancariaVO agenciaProdutora;
    protected AngariadorVO angariador;
    protected BancoVO banco;
    protected CanalVendaVO canalVenda;
    protected String codigo;
    protected String codigoFormatado;
    protected String codigoMatriculaAssistente;
    protected String codigoMatriculaGerente;
    protected String codigoResponsavelUltimaAtualizacao;
    protected CorretorVO corretor;
    //protected List<CorretorVO> corretorLista;
    protected CorretorMasterVO corretorMaster;
    protected String dataAdesao;
    protected String dataCriacao;
    protected String dataInicioCobranca;
    protected String dataUltimaAtualizacao;
    @XmlElement(nillable = true)
    protected List<DependenteVO> dependentes;
    protected Integer diaVencimento;
    protected FormaPagamento formaPagamento;
    protected MovimentoPropostaVO movimento;
    protected Long numeroSequencial;
    protected Origem origem;
    protected PlanoVO planoVO;
    protected ProponenteVO proponente;
    protected String protocolo;
    protected ResponsavelLegalVO responsavelLegal;
    protected Subsegmentacao subsegmentacao;
    protected SucursalSeguradoraVO sucursalSeguradora;
    protected TipoCobranca tipoCobranca;
    protected TitularVO titular;
    protected String dataValidadeProposta;
    protected String dataEmissao;
    protected Long codigoPostoAtendimento;


    /**
     * Gets the value of the agenciaProdutora property.
     * 
     * @return
     *     possible object is
     *     {@link AgenciaBancariaVO }
     *     
     */
    public AgenciaBancariaVO getAgenciaProdutora() {
        return agenciaProdutora;
    }

    /**
     * Sets the value of the agenciaProdutora property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgenciaBancariaVO }
     *     
     */
    public void setAgenciaProdutora(AgenciaBancariaVO value) {
        this.agenciaProdutora = value;
    }

    /**
     * Gets the value of the angariador property.
     * 
     * @return
     *     possible object is
     *     {@link AngariadorVO }
     *     
     */
    public AngariadorVO getAngariador() {
        return angariador;
    }

    /**
     * Sets the value of the angariador property.
     * 
     * @param value
     *     allowed object is
     *     {@link AngariadorVO }
     *     
     */
    public void setAngariador(AngariadorVO value) {
        this.angariador = value;
    }

    /**
     * Gets the value of the banco property.
     * 
     * @return
     *     possible object is
     *     {@link BancoVO }
     *     
     */
    public BancoVO getBanco() {
        return banco;
    }

    /**
     * Sets the value of the banco property.
     * 
     * @param value
     *     allowed object is
     *     {@link BancoVO }
     *     
     */
    public void setBanco(BancoVO value) {
        this.banco = value;
    }

    /**
     * Gets the value of the canalVenda property.
     * 
     * @return
     *     possible object is
     *     {@link CanalVendaVO }
     *     
     */
    public CanalVendaVO getCanalVenda() {
        return canalVenda;
    }

    /**
     * Sets the value of the canalVenda property.
     * 
     * @param value
     *     allowed object is
     *     {@link CanalVendaVO }
     *     
     */
    public void setCanalVenda(CanalVendaVO value) {
        this.canalVenda = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the codigoFormatado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoFormatado() {
        return codigoFormatado;
    }

    /**
     * Sets the value of the codigoFormatado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoFormatado(String value) {
        this.codigoFormatado = value;
    }

    /**
     * Gets the value of the codigoMatriculaAssistente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoMatriculaAssistente() {
        return codigoMatriculaAssistente;
    }

    /**
     * Sets the value of the codigoMatriculaAssistente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoMatriculaAssistente(String value) {
        this.codigoMatriculaAssistente = value;
    }

    /**
     * Gets the value of the codigoMatriculaGerente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoMatriculaGerente() {
        return codigoMatriculaGerente;
    }

    /**
     * Sets the value of the codigoMatriculaGerente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoMatriculaGerente(String value) {
        this.codigoMatriculaGerente = value;
    }

    /**
     * Gets the value of the codigoResponsavelUltimaAtualizacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoResponsavelUltimaAtualizacao() {
        return codigoResponsavelUltimaAtualizacao;
    }

    /**
     * Sets the value of the codigoResponsavelUltimaAtualizacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoResponsavelUltimaAtualizacao(String value) {
        this.codigoResponsavelUltimaAtualizacao = value;
    }

    /**
     * Gets the value of the corretor property.
     * 
     * @return
     *     possible object is
     *     {@link CorretorVO }
     *     
     */
    public CorretorVO getCorretor() {
        return corretor;
    }

    /**
     * Sets the value of the corretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorretorVO }
     *     
     */
    public void setCorretor(CorretorVO value) {
        this.corretor = value;
    }

    /**
     * Gets the value of the corretorMaster property.
     * 
     * @return
     *     possible object is
     *     {@link CorretorMasterVO }
     *     
     */
    public CorretorMasterVO getCorretorMaster() {
        return corretorMaster;
    }

    /**
     * Sets the value of the corretorMaster property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorretorMasterVO }
     *     
     */
    public void setCorretorMaster(CorretorMasterVO value) {
        this.corretorMaster = value;
    }

    /**
     * Gets the value of the dataAdesao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAdesao() {
        return dataAdesao;
    }

    /**
     * Sets the value of the dataAdesao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAdesao(String value) {
        this.dataAdesao = value;
    }

    /**
     * Gets the value of the dataCriacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCriacao() {
        return dataCriacao;
    }

    /**
     * Sets the value of the dataCriacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCriacao(String value) {
        this.dataCriacao = value;
    }

    /**
     * Gets the value of the dataInicioCobranca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataInicioCobranca() {
        return dataInicioCobranca;
    }

    /**
     * Sets the value of the dataInicioCobranca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInicioCobranca(String value) {
        this.dataInicioCobranca = value;
    }

    /**
     * Gets the value of the dataUltimaAtualizacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataUltimaAtualizacao() {
        return dataUltimaAtualizacao;
    }

    /**
     * Sets the value of the dataUltimaAtualizacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataUltimaAtualizacao(String value) {
        this.dataUltimaAtualizacao = value;
    }

    /**
     * Gets the value of the dependentes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dependentes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDependentes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DependenteVO }
     * 
     * 
     */
    public List<DependenteVO> getDependentes() {
        if (dependentes == null) {
            dependentes = new ArrayList<DependenteVO>();
        }
        return this.dependentes;
    }
    /**
     * Gets the value of the dependentes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dependentes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDependentes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DependenteVO }
     * 
     * 
     */
  /*  public List<CorretorVO> getCorretorLista() {
        if (corretorLista == null) {
        	corretorLista = new ArrayList<CorretorVO>();
        }
        return this.corretorLista;
    }*/

    /**
     * Gets the value of the diaVencimento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDiaVencimento() {
        return diaVencimento;
    }

    /**
     * Sets the value of the diaVencimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDiaVencimento(Integer value) {
        this.diaVencimento = value;
    }

    /**
     * Gets the value of the formaPagamento property.
     * 
     * @return
     *     possible object is
     *     {@link FormaPagamento }
     *     
     */
    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    /**
     * Sets the value of the formaPagamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormaPagamento }
     *     
     */
    public void setFormaPagamento(FormaPagamento value) {
        this.formaPagamento = value;
    }

    /**
     * Gets the value of the movimento property.
     * 
     * @return
     *     possible object is
     *     {@link MovimentoPropostaVO }
     *     
     */
    public MovimentoPropostaVO getMovimento() {
        return movimento;
    }

    /**
     * Sets the value of the movimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link MovimentoPropostaVO }
     *     
     */
    public void setMovimento(MovimentoPropostaVO value) {
        this.movimento = value;
    }

    /**
     * Gets the value of the numeroSequencial property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroSequencial() {
        return numeroSequencial;
    }

    /**
     * Sets the value of the numeroSequencial property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroSequencial(Long value) {
        this.numeroSequencial = value;
    }

    /**
     * Gets the value of the origem property.
     * 
     * @return
     *     possible object is
     *     {@link Origem }
     *     
     */
    public Origem getOrigem() {
        return origem;
    }

    /**
     * Sets the value of the origem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Origem }
     *     
     */
    public void setOrigem(Origem value) {
        this.origem = value;
    }

    /**
     * Gets the value of the planoVO property.
     * 
     * @return
     *     possible object is
     *     {@link PlanoVO }
     *     
     */
    public PlanoVO getPlanoVO() {
        return planoVO;
    }

    /**
     * Sets the value of the planoVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanoVO }
     *     
     */
    public void setPlanoVO(PlanoVO value) {
        this.planoVO = value;
    }

    /**
     * Gets the value of the proponente property.
     * 
     * @return
     *     possible object is
     *     {@link ProponenteVO }
     *     
     */
    public ProponenteVO getProponente() {
        return proponente;
    }

    /**
     * Sets the value of the proponente property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProponenteVO }
     *     
     */
    public void setProponente(ProponenteVO value) {
        this.proponente = value;
    }

    /**
     * Gets the value of the protocolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocolo() {
        return protocolo;
    }

    /**
     * Sets the value of the protocolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocolo(String value) {
        this.protocolo = value;
    }

    /**
     * Gets the value of the responsavelLegal property.
     * 
     * @return
     *     possible object is
     *     {@link ResponsavelLegalVO }
     *     
     */
    public ResponsavelLegalVO getResponsavelLegal() {
        return responsavelLegal;
    }

    /**
     * Sets the value of the responsavelLegal property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsavelLegalVO }
     *     
     */
    public void setResponsavelLegal(ResponsavelLegalVO value) {
        this.responsavelLegal = value;
    }

    /**
     * Gets the value of the subsegmentacao property.
     * 
     * @return
     *     possible object is
     *     {@link Subsegmentacao }
     *     
     */
    public Subsegmentacao getSubsegmentacao() {
        return subsegmentacao;
    }

    /**
     * Sets the value of the subsegmentacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Subsegmentacao }
     *     
     */
    public void setSubsegmentacao(Subsegmentacao value) {
        this.subsegmentacao = value;
    }

    /**
     * Gets the value of the sucursalSeguradora property.
     * 
     * @return
     *     possible object is
     *     {@link SucursalSeguradoraVO }
     *     
     */
    public SucursalSeguradoraVO getSucursalSeguradora() {
        return sucursalSeguradora;
    }

    /**
     * Sets the value of the sucursalSeguradora property.
     * 
     * @param value
     *     allowed object is
     *     {@link SucursalSeguradoraVO }
     *     
     */
    public void setSucursalSeguradora(SucursalSeguradoraVO value) {
        this.sucursalSeguradora = value;
    }

    /**
     * Gets the value of the tipoCobranca property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCobranca }
     *     
     */
    public TipoCobranca getTipoCobranca() {
        return tipoCobranca;
    }

    /**
     * Sets the value of the tipoCobranca property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCobranca }
     *     
     */
    public void setTipoCobranca(TipoCobranca value) {
        this.tipoCobranca = value;
    }

    /**
     * Gets the value of the titular property.
     * 
     * @return
     *     possible object is
     *     {@link TitularVO }
     *     
     */
    public TitularVO getTitular() {
        return titular;
    }

    /**
     * Sets the value of the titular property.
     * 
     * @param value
     *     allowed object is
     *     {@link TitularVO }
     *     
     */
    public void setTitular(TitularVO value) {
        this.titular = value;
    }

	/**
	 * @return the dataValidadeProposta
	 */
	public String getDataValidadeProposta() {
		return dataValidadeProposta;
	}

	/**
	 * @param dataValidadeProposta the dataValidadeProposta to set
	 */
	public void setDataValidadeProposta(String dataValidadeProposta) {
		this.dataValidadeProposta = dataValidadeProposta;
	}

	/**
	 * @return the dataEmissao
	 */
	public String getDataEmissao() {
		return dataEmissao;
	}

	/**
	 * @param dataEmissao the dataEmissao to set
	 */
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	/**
	 * @return the codigoPostoAtendimento
	 */
	public Long getCodigoPostoAtendimento() {
		return codigoPostoAtendimento;
	}

	/**
	 * @param codigoPostoAtendimento the codigoPostoAtendimento to set
	 */
	public void setCodigoPostoAtendimento(Long codigoPostoAtendimento) {
		this.codigoPostoAtendimento = codigoPostoAtendimento;
	}
	
	

}
