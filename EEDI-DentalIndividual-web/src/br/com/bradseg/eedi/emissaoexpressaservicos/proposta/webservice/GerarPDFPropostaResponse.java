
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gerarPDFPropostaResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gerarPDFPropostaResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pdfProposta" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gerarPDFPropostaResponse", propOrder = {
    "pdfProposta"
})
public class GerarPDFPropostaResponse {

    protected byte[] pdfProposta;

    /**
     * Gets the value of the pdfProposta property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPdfProposta() {
        return pdfProposta;
    }

    /**
     * Sets the value of the pdfProposta property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPdfProposta(byte[] value) {
        this.pdfProposta = value;
    }

}
