
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for contaCorrenteVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="contaCorrenteVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agenciaBancaria" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}agenciaBancariaVO" minOccurs="0"/>
 *         &lt;element name="digitoVerificador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="siglaPostoAtendimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contaCorrenteVO", propOrder = {
    "agenciaBancaria",
    "digitoVerificador",
    "numero",
    "siglaPostoAtendimento"
})
public class ContaCorrenteVO {

    protected AgenciaBancariaVO agenciaBancaria;
    protected String digitoVerificador;
    protected Long numero;
    protected String siglaPostoAtendimento;

    /**
     * Gets the value of the agenciaBancaria property.
     * 
     * @return
     *     possible object is
     *     {@link AgenciaBancariaVO }
     *     
     */
    public AgenciaBancariaVO getAgenciaBancaria() {
        return agenciaBancaria;
    }

    /**
     * Sets the value of the agenciaBancaria property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgenciaBancariaVO }
     *     
     */
    public void setAgenciaBancaria(AgenciaBancariaVO value) {
        this.agenciaBancaria = value;
    }

    /**
     * Gets the value of the digitoVerificador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitoVerificador() {
        return digitoVerificador;
    }

    /**
     * Sets the value of the digitoVerificador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitoVerificador(String value) {
        this.digitoVerificador = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumero(Long value) {
        this.numero = value;
    }

    /**
     * Gets the value of the siglaPostoAtendimento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiglaPostoAtendimento() {
        return siglaPostoAtendimento;
    }

    /**
     * Sets the value of the siglaPostoAtendimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiglaPostoAtendimento(String value) {
        this.siglaPostoAtendimento = value;
    }

}
