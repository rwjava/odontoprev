
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listarPropostasAcompanhamentoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listarPropostasAcompanhamentoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="relatorioAcompanhamento" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}acompanhamentoPropostaVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listarPropostasAcompanhamentoResponse", propOrder = {
    "relatorioAcompanhamento"
})
public class ListarPropostasAcompanhamentoResponse {

    protected List<AcompanhamentoPropostaVO> relatorioAcompanhamento;

    /**
     * Gets the value of the relatorioAcompanhamento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatorioAcompanhamento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatorioAcompanhamento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AcompanhamentoPropostaVO }
     * 
     * 
     */
    public List<AcompanhamentoPropostaVO> getRelatorioAcompanhamento() {
        if (relatorioAcompanhamento == null) {
            relatorioAcompanhamento = new ArrayList<AcompanhamentoPropostaVO>();
        }
        return this.relatorioAcompanhamento;
    }

}
