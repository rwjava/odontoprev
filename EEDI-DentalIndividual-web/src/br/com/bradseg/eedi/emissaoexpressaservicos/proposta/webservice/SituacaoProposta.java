
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for situacaoProposta.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="situacaoProposta">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PENDENTE"/>
 *     &lt;enumeration value="RASCUNHO"/>
 *     &lt;enumeration value="EM_PROCESSAMENTO"/>
 *     &lt;enumeration value="CRITICADA"/>
 *     &lt;enumeration value="CANCELADA"/>
 *     &lt;enumeration value="IMPLANTADA"/>
 *     &lt;enumeration value="PRE_CANCELADA"/>
 *     &lt;enumeration value="TODAS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "situacaoProposta")
@XmlEnum
public enum SituacaoProposta {

    PENDENTE,
    RASCUNHO,
    EM_PROCESSAMENTO,
    CRITICADA,
    CANCELADA,
    IMPLANTADA,
    PRE_CANCELADA,
    TODAS;

    public String value() {
        return name();
    }

    public static SituacaoProposta fromValue(String v) {
        return valueOf(v);
    }

}
