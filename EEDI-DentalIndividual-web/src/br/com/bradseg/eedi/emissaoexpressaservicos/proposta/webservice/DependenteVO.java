
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dependenteVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dependenteVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}beneficiarioVO">
 *       &lt;sequence>
 *         &lt;element name="grauParentesco" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}grauParentesco" minOccurs="0"/>
 *        &lt;element name="grauParentescoBeneficiario" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}grauParentescoBeneficiario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dependenteVO", propOrder = {
    "grauParentesco",
    "grauParentescoBeneficiario"
})
public class DependenteVO
    extends BeneficiarioVO
{

    protected GrauParentesco grauParentesco;
    protected GrauParentesco grauParentescoBeneficiario;

    /**
     * Gets the value of the grauParentesco property.
     * 
     * @return
     *     possible object is
     *     {@link GrauParentesco }
     *     
     */
    public GrauParentesco getGrauParentesco() {
        return grauParentesco;
    }

    /**
     * Sets the value of the grauParentesco property.
     * 
     * @param value
     *     allowed object is
     *     {@link GrauParentesco }
     *     
     */
    public void setGrauParentesco(GrauParentesco value) {
        this.grauParentesco = value;
    }
    /**
     * Gets the value of the grauParentesco property.
     * 
     * @return
     *     possible object is
     *     {@link GrauParentesco }
     *     
     */
  
    public GrauParentesco getGrauParentescoBeneficiario() {
        return grauParentescoBeneficiario;
    }

    /**
     * Sets the value of the grauParentesco property.
     * 
     * @param value
     *     allowed object is
     *     {@link GrauParentesco }
     *     
     */
    public void setGrauParentescoBeneficiario(GrauParentesco value) {
        this.grauParentescoBeneficiario = value;
    }

}
