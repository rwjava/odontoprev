
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gerarBoletoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gerarBoletoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="boleto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gerarBoletoResponse", propOrder = {
    "boleto"
})
public class GerarBoletoResponse {

    protected byte[] boleto;

    /**
     * Gets the value of the boleto property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBoleto() {
        return boleto;
    }

    /**
     * Sets the value of the boleto property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBoleto(byte[] value) {
        this.boleto = value;
    }

}
