
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de planoAnsVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="planoAnsVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idPlanoAns" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="modalidade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomePlanoAns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planoAns" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "planoAnsVO", propOrder = {
    "idPlanoAns",
    "modalidade",
    "nomePlanoAns",
    "planoAns"
})
public class PlanoAnsVO {

    protected Long idPlanoAns;
    protected String modalidade;
    protected String nomePlanoAns;
    protected Long planoAns;

    /**
     * Obt�m o valor da propriedade idPlanoAns.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdPlanoAns() {
        return idPlanoAns;
    }

    /**
     * Define o valor da propriedade idPlanoAns.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdPlanoAns(Long value) {
        this.idPlanoAns = value;
    }

    /**
     * Obt�m o valor da propriedade modalidade.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalidade() {
        return modalidade;
    }

    /**
     * Define o valor da propriedade modalidade.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalidade(String value) {
        this.modalidade = value;
    }

    /**
     * Obt�m o valor da propriedade nomePlanoAns.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomePlanoAns() {
        return nomePlanoAns;
    }

    /**
     * Define o valor da propriedade nomePlanoAns.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomePlanoAns(String value) {
        this.nomePlanoAns = value;
    }

    /**
     * Obt�m o valor da propriedade planoAns.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPlanoAns() {
        return planoAns;
    }

    /**
     * Define o valor da propriedade planoAns.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPlanoAns(Long value) {
        this.planoAns = value;
    }

}
