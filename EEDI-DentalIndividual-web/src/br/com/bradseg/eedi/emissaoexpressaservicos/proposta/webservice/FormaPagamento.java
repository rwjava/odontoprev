
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formaPagamento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="formaPagamento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BOLETO_BANCARIO"/>
 *     &lt;enumeration value="BOLETO_DEMAIS_DEBITO"/>
 *     &lt;enumeration value="DEBITO_AUTOMATICO"/>
 *     &lt;enumeration value="DEBITO_DEMAIS_BOLETO"/>
 *     &lt;enumeration value="CARTAO_CREDITO"/>
 *     &lt;enumeration value="BOLETO_E_CARTAO_CREDITO"/>
 *     &lt;enumeration value="DEBITO_AUTOMATICO_E_CARTAO_CREDITO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "formaPagamento")
@XmlEnum
public enum FormaPagamento {

    BOLETO_BANCARIO,
    BOLETO_DEMAIS_DEBITO,
    DEBITO_AUTOMATICO,
    DEBITO_DEMAIS_BOLETO,
    CARTAO_CREDITO,
    BOLETO_E_CARTAO_CREDITO,
    DEBITO_AUTOMATICO_E_CARTAO_CREDITO;

    public String value() {
        return name();
    }

    public static FormaPagamento fromValue(String v) {
        return valueOf(v);
    }

}
