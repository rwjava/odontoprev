
package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.CanalVendaVO;


/**
 * <p>Classe Java de planoVO complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="planoVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoRegistro" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoResponsavel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoResponsavelUltimaAtualizacao" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="dataFimVigencia" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}dateTime" minOccurs="0"/>
 *         &lt;element name="dataInicioVigencia" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}dateTime" minOccurs="0"/>
 *         &lt;element name="dataUltimaAtualizacao" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}localDate" minOccurs="0"/>
 *         &lt;element name="descricaoCarenciaPeriodoAnual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descricaoCarenciaPeriodoMensal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaDeCanais" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}canalVendaVO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planoAnsVO" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}planoAnsVO" minOccurs="0"/>
 *         &lt;element name="valorPlanoVO" type="{http://webservice.proposta.emissaoexpressaservicos.eedi.bradseg.com.br/}valorPlanoVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "planoVO", propOrder = {
    "codigo",
    "codigoRegistro",
    "codigoResponsavel",
    "codigoResponsavelUltimaAtualizacao",
    "dataFimVigencia",
    "dataInicioVigencia",
    "dataUltimaAtualizacao",
    "descricaoCarenciaPeriodoAnual",
    "descricaoCarenciaPeriodoMensal",
    "listaDeCanais",
    "nome",
    "planoAnsVO",
    "valorPlanoVO"
})
public class PlanoVO {

    protected Long codigo;
    protected Long codigoRegistro;
    protected String codigoResponsavel;
    protected Long codigoResponsavelUltimaAtualizacao;
    protected DateTime dataFimVigencia;
    protected DateTime dataInicioVigencia;
    protected LocalDate dataUltimaAtualizacao;
    protected String descricaoCarenciaPeriodoAnual;
    protected String descricaoCarenciaPeriodoMensal;
    @XmlElement(nillable = true)
    protected List<CanalVendaVO> listaDeCanais;
    protected String nome;
    protected PlanoAnsVO planoAnsVO;
    protected ValorPlanoVO valorPlanoVO;

    /**
     * Obt�m o valor da propriedade codigo.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigo() {
        return codigo;
    }

    /**
     * Define o valor da propriedade codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigo(Long value) {
        this.codigo = value;
    }

    /**
     * Obt�m o valor da propriedade codigoRegistro.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoRegistro() {
        return codigoRegistro;
    }

    /**
     * Define o valor da propriedade codigoRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoRegistro(Long value) {
        this.codigoRegistro = value;
    }

    /**
     * Obt�m o valor da propriedade codigoResponsavel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoResponsavel() {
        return codigoResponsavel;
    }

    /**
     * Define o valor da propriedade codigoResponsavel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoResponsavel(String value) {
        this.codigoResponsavel = value;
    }

    /**
     * Obt�m o valor da propriedade codigoResponsavelUltimaAtualizacao.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoResponsavelUltimaAtualizacao() {
        return codigoResponsavelUltimaAtualizacao;
    }

    /**
     * Define o valor da propriedade codigoResponsavelUltimaAtualizacao.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoResponsavelUltimaAtualizacao(Long value) {
        this.codigoResponsavelUltimaAtualizacao = value;
    }

    /**
     * Obt�m o valor da propriedade dataFimVigencia.
     * 
     * @return
     *     possible object is
     *     {@link DateTime }
     *     
     */
    public DateTime getDataFimVigencia() {
        return dataFimVigencia;
    }

    /**
     * Define o valor da propriedade dataFimVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTime }
     *     
     */
    public void setDataFimVigencia(DateTime value) {
        this.dataFimVigencia = value;
    }

    /**
     * Obt�m o valor da propriedade dataInicioVigencia.
     * 
     * @return
     *     possible object is
     *     {@link DateTime }
     *     
     */
    public DateTime getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    /**
     * Define o valor da propriedade dataInicioVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTime }
     *     
     */
    public void setDataInicioVigencia(DateTime value) {
        this.dataInicioVigencia = value;
    }

    /**
     * Obt�m o valor da propriedade dataUltimaAtualizacao.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getDataUltimaAtualizacao() {
        return dataUltimaAtualizacao;
    }

    /**
     * Define o valor da propriedade dataUltimaAtualizacao.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setDataUltimaAtualizacao(LocalDate value) {
        this.dataUltimaAtualizacao = value;
    }

    /**
     * Obt�m o valor da propriedade descricaoCarenciaPeriodoAnual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoCarenciaPeriodoAnual() {
        return descricaoCarenciaPeriodoAnual;
    }

    /**
     * Define o valor da propriedade descricaoCarenciaPeriodoAnual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoCarenciaPeriodoAnual(String value) {
        this.descricaoCarenciaPeriodoAnual = value;
    }

    /**
     * Obt�m o valor da propriedade descricaoCarenciaPeriodoMensal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoCarenciaPeriodoMensal() {
        return descricaoCarenciaPeriodoMensal;
    }

    /**
     * Define o valor da propriedade descricaoCarenciaPeriodoMensal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoCarenciaPeriodoMensal(String value) {
        this.descricaoCarenciaPeriodoMensal = value;
    }

    /**
     * Gets the value of the listaDeCanais property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaDeCanais property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaDeCanais().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CanalVendaVO }
     * 
     * 
     */
    public List<CanalVendaVO> getListaDeCanais() {
        if (listaDeCanais == null) {
            listaDeCanais = new ArrayList<CanalVendaVO>();
        }
        return this.listaDeCanais;
    }

    /**
     * Obt�m o valor da propriedade nome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Define o valor da propriedade nome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Obt�m o valor da propriedade planoAnsVO.
     * 
     * @return
     *     possible object is
     *     {@link PlanoAnsVO }
     *     
     */
    public PlanoAnsVO getPlanoAnsVO() {
        return planoAnsVO;
    }

    /**
     * Define o valor da propriedade planoAnsVO.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanoAnsVO }
     *     
     */
    public void setPlanoAnsVO(PlanoAnsVO value) {
        this.planoAnsVO = value;
    }

    /**
     * Obt�m o valor da propriedade valorPlanoVO.
     * 
     * @return
     *     possible object is
     *     {@link ValorPlanoVO }
     *     
     */
    public ValorPlanoVO getValorPlanoVO() {
        return valorPlanoVO;
    }

    /**
     * Define o valor da propriedade valorPlanoVO.
     * 
     * @param value
     *     allowed object is
     *     {@link ValorPlanoVO }
     *     
     */
    public void setValorPlanoVO(ValorPlanoVO value) {
        this.valorPlanoVO = value;
    }

}
