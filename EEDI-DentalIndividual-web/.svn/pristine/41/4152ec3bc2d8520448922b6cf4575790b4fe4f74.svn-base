package br.com.bradseg.eedi.dentalindividual.cep.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.soap.SOAPFaultException;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice.CepWebService;
import br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice.CepWebServiceResponse;
import br.com.bradseg.ccep.consultacep.presentation.consultacep.webservice.IntegrationException_Exception;
import br.com.bradseg.eedi.dentalindividual.util.Strings;
import br.com.bradseg.eedi.dentalindividual.vo.EnderecoVO;

/**
 * Classe de neg�cio responsavel por disponibilizar os metodos referentes a cep.
 *
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CepServiceFacadeImpl implements CepServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(CepServiceFacadeImpl.class);
	
	@Autowired
	private CepWebService cepWebService;
	
	/**
	 * Metodo resposanvel por obter um endere�o a partir de um cep.
	 * 
	 * - Chamar o servi�o para obter o endere�o a partir do cep informado;
	 * - Converter o objeto do servi�o para o objeto da aplica��o;
	 * 
	 * @param cep - cep a ser consultado.
	 * @return EnderecoVO - informa��es referentes ao cep consultado.
	 */
	public EnderecoVO obterEnderecoPorCep(String cep) {		
		try {
			return converterObjetoServicoParaObjetoAplicacao(cepWebService.obterListaPorCepCompleto(cep));
		} catch (IntegrationException_Exception e) {
			LOGGER.error("Erro de integra��o ao obter o endere�o referente ao cep:" + cep);
			throw new BusinessException(e.getMessage());
		}catch (SOAPFaultException e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	/**
	 * Metodo responsavel por converter o objeto cep do servi�o para o objeto cep da aplica��o.
	 * 
	 * @param cepServico - informa��es do objeto obtido pelo servi�o.
	 * @return EnderecoVO - informa��es do objeto obtido pelo servi�o setadas no objeto da aplica��o.
	 */
	private EnderecoVO converterObjetoServicoParaObjetoAplicacao(CepWebServiceResponse cepServico){
		
		EnderecoVO endereco = null;
		
		if(cepServico != null && cepServico.getLista() != null && !cepServico.getLista().isEmpty()){
			endereco = new EnderecoVO();
			endereco.setCep(Strings.normalizarCep(cepServico.getLista().get(0).getCep()));
			endereco.setLogradouro(cepServico.getLista().get(0).getDescricaoLogradouro());
			endereco.setBairro(cepServico.getLista().get(0).getBairro());
			endereco.setCidade(cepServico.getLista().get(0).getCidade());
			endereco.setEstado(cepServico.getLista().get(0).getSiglaUf());
		}
		
		return endereco;
	}

}
