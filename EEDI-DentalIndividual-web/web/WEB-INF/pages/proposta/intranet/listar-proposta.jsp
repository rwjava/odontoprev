<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
			<title>Emiss�o Expressa Dental Individual</title>
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
			
			<!-- Bradesco styles -->
			<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/css.css'/>" />
			<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/tabs.css'/>" />
			<link rel="stylesheet" type="text/css" href="/EEDI-DentalIndividual/includes/css/jquery-ui.css" />
			
			<script type="text/javascript" src="<s:url value='/includes/js/proposta/listar-proposta.js'/>"></script>
			
			<!-- External plugins -->
			<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.js'/>"></script>
			<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-ui.custom.min.js'/>"></script>
			<script type="text/javascript" src="<s:url value="/includes/js/vendor/jquery.ui.datepicker-pt-BR.js" />" ></script>
			<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.inputmask.js'/>"></script>
			<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.blockUI.js'/>"></script>
			<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.scrollto.js'/>"></script>
			<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-maskmoney.js'/>"></script>
			
			<!-- WDEV plugins -->
			<script type="text/javascript" src="<s:url value='/includes/js/wdev-datatypes.js'/>"></script>
			<script type="text/javascript" src="<s:url value='/includes/js/wdev-ajax.js'/>"></script>
			<script type="text/javascript" src="<s:url value='/includes/js/wdev-block.js'/>"></script>
			<script type="text/javascript" src="<s:url value='/includes/js/wdev-required.js'/>"></script>
			<script type="text/javascript" src="<s:url value='/includes/js/wdev-inputcontrol.js'/>"></script>
			
			<s:url var="baseURL" value="/"/>
			<script>window.baseURL = "${baseURL}";</script>
			
		
	
	</head>
	<script type="text/javascript" src="<s:url value='/includes/js/proposta/listar-proposta.js'/>"></script>
	<script>
	
	
		$(function() {
			$(".datepicker").datepicker();
		});
	</script>
	<div id='content'>
				<s:hidden id="hasActionErrors" value="%{hasActionErrors()}" />
				<table width="974px;">
					<tr>
						<td>
							<br/>
								<span class="subtitulo">EMISS�O EXPRESSA INDIVIDUAL</span>
							<br/>
							<font style="color:#0071BB;font-size:14px;font-weight:bold;"><span id="subtitulo">Cadastro de Proposta</span></font>
							<div id="tabs" class="tabpanel"></div>
							<div id="popCalendario" class="pop_calendario_box"></div>
							<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_verm" id="msgErros" style="display:none">
								<tbody>
									<tr>
										<td>
											<div id="serverErrors" class="messages">
												<s:iterator value="actionErrors">
													<li><s:property /></li>
												</s:iterator>
											</div>
											<div id="actionErrors" class="messages"></div>
										</td>
									</tr>
								</tbody>
							</table>
							<s:if test="hasActionMessages()">
								<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_sucesso" id="msgSucesso">
									<tbody>
										<tr>
											<td>
												<div id="serverActionsMessages" class="messages">
													<s:iterator value="actionMessages">
														<li><s:property /></li>
													</s:iterator>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</s:if>
							<br />
							
							<div class="tab-row menuList" >
								<ul>
									<li id="nova_proposta" class="tab0">
										<a href="iniciarNovaPropostaIntranet.do"><span>NOVA PROPOSTA</span></a>
									</li>
									<li id="listar_proposta" class="tab1">
										<a href="listarPropostaIntranet.do"><span>LISTAR PROPOSTA</span></a>
									</li>
									<li id="relatorio_acompanhamento" class="tab2">
										<a href="relatorioAcompanhamentoIntranet.do"><span>RELATORIO DE ACOMPANHAMENTO</span></a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<s:if test="hasActionMessages()">
				<script>
					$("#msgSucesso").fadeOut(50000);
				</script>
			</s:if>
		
	<s:form id="form-intranet" action="consultarPropostaIntranet">
		<table class="tabela_interna">
			<tr>
				<th colspan="9">BUSCA DE PROPOSTA</th> 
			</tr>
			
			<tr>
			
				<td class="td_label"  style="width: 15px" rowSpan="1" colSpan="1">C�digo Proposta</td>
				
				<td>
					<s:textfield name="filtroProposta.codigoProposta" size="18" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta2" />
				</td>
								
				<td class="td_label" style="width: 20px" rowSpan="1"><label id="tdProtocolo">Protocolo</label></td>
				
				<td colspan="2">
					<s:textfield name="filtroProposta.protocolo" size="18" maxlength="30"  cssClass="lista-proposta2" />				
				</td>
			
							
				<td class="td_label" style="width:180px" >Nome Benefici�rio Titular</td>
				
				<td>
					<s:textfield name="filtroProposta.nomeTitular" size="20" maxlength="30" cssClass="lista-proposta2" />	
				</td>
			</tr>
			
			<tr>
				<td class="td_label">Situa��o Proposta</td>
				
				<td colspan="7">
					<s:select name="filtroProposta.filtroPeriodoVO.status" id="status" list="status" headerKey="0" headerValue="Selecione" listKey="codigo" listValue="nome" cssClass="lista-proposta2"/>				
				</td>
			</tr>
			
			<tr>
				<td class="td_label">Canal de Venda</td>
				
				<td colspan="7">
					<s:select name="filtroProposta.canal.codigo" id="status" list="listaCanal" headerKey="0" headerValue="Selecione" listKey="codigo" listValue="nome" cssClass="lista-proposta2" />				
				</td>
			</tr>
			
			<tr>
				<td class="td_label">CPD/Sucursal do Corretor</td>
				
				<td colspan="2">
					<s:textfield name="filtroProposta.cpdCorretor" size="12" maxlength="6" cssClass="lista-proposta2" />	
					&nbsp;/&nbsp;
					<s:textfield name="filtroProposta.sucursalCorretor" size="4" maxlength="4"  cssClass="lista-proposta2"/>	
				</td>
				
				<td class="td_label" colspan="1" style="width: 80px;" >Per�odo
					<span class="obrigatorio">*</span>
				</td>
				
				<td colspan="5">
				<s:textfield name="filtroProposta.filtroPeriodoVO.dataInicio" data-mask="data" placeholder="__/__/___" size="10" maxlength="10" style="width: 30%;" cssClass="datepicker lista-proposta2" /> 
					&nbsp;at�&nbsp; 				
				<s:textfield name="filtroProposta.filtroPeriodoVO.dataFim" data-mask="data" placeholder="__/__/___" size="10" maxlength="10" style="width: 30%;" cssClass="datepicker lista-proposta2" />
				
				
				(M�ximo 30 Dias)
				</td>		
			  </tr>
			</table>
			
			<!-- ------------------------------------------------------------------------------------------------------------------------------ -->
			
			
			
	<!-- 		<tr> -->
	<%-- 			<td class="td_label" colspan="1" width="130px;">Tipo de Busca<span class="obrigatorio">*</span></td> --%>
	<!-- 			<td colspan="3"> -->
	<%-- 				<s:radio name="filtroProposta.tipoBusca" list="tiposBusca" listValue="nome" listKey="codigo"/> --%>
	<!-- 			</td> -->
	<!-- 		</tr> -->
	<!-- 		<tr class="buscarPorCodigo" style="display:none;"> -->
	<%-- 			<td class="td_label" colspan="1">C�digo Proposta<span class="obrigatorio">*</span></td> --%>
	<!-- 			<td colspan="3"> -->
	<%-- 				<s:textfield name="filtroProposta.codigoProposta" size="16" maxlength="15" style="text-transform: uppercase;" /> --%>
	<!-- 			</td> -->
	<!-- 		</tr> -->
	<!-- 		<tr class="buscarPorCPF" style="display:none;"> -->
	<%-- 			<td class="td_label" colspan="1" >Tipo de CPF<span class="obrigatorio">*</span></td> --%>
	<!-- 			<td colspan="1" width="380px;"> -->
	<%-- 				<s:radio name="filtroProposta.tipoCPF" list="tiposCPFs" listValue="nome" listKey="codigo"/> --%>
	<!-- 			</td> -->
	<%-- 			<td class="td_label" colspan="1" >CPF<span class="obrigatorio">*</span></td> --%>
	<!-- 			<td colspan="1" > -->
	<%-- 				<s:textfield name="filtroProposta.cpf" data-mask="cpf" size="12" maxlength="14"  /> --%>
	<!-- 			</td> -->
	<!-- 		</tr> -->
	<!-- 		<tr class="buscarPorPeriodo" style="display:none;"> -->
	<%-- 			<td class="td_label" colspan="1" >Per�odo<span class="obrigatorio">*</span></td> --%>
	<!-- 			<td colspan="3"> -->
	<%-- 				<s:textfield name="filtroProposta.filtroPeriodoVO.dataInicio" data-mask="data" size="11" maxlength="11" /> --%>
	<!-- 				&nbsp;at�&nbsp; -->
	<%-- 				<s:textfield name="filtroProposta.filtroPeriodoVO.dataFim" data-mask="data" size="11" maxlength="11" /> --%>
	<!-- 				(M�ximo 30 Dias) -->
	<!-- 			</td> -->
	<!-- 		</tr> -->
	<!-- 		<tr class="buscarPorPeriodo" style="display:none;"> -->
	<%-- 			<td class="td_label" >Status<span class="obrigatorio">*</span></td> --%>
	<!-- 			<td colspan="3"> -->
	<%-- 				<s:select name="filtroProposta.filtroPeriodoVO.status" id="status" list="status" headerKey="0" headerValue="Selecione" listKey="codigo" listValue="nome"/> --%>
	<!-- 			</td> -->
	<!-- 		</tr> -->
			<!-- tr class="buscarPorPeriodo" style="display:none;">
				<td class="td_label" colspan="1" >CPD</td>
				<td colspan="1" width="380px;"><s:textfield name="filtroProposta.filtroPeriodoVO.cpd" size="4" maxlength="6"  /></td>
				<td class="td_label" colspan="1" width="100px;">Sucursal</td>
				<td colspan="1" >
					<s:textfield name="filtroProposta.filtroPeriodoVO.sucursal" size="3" maxlength="3"  />
				</td>
			</tr-->
	<!-- 	</table> -->
		
		<br/><br/><br/>
		
		<table style="width: 1050px;">
		  <tbody>
			<tr>
				<td width="45%" align="right"><input type="button" class="button margem_botoes" value="Limpar" id="btn_limpar_2" /></td>
				
				<td class="td_table"><input class="button margem_botoes" type="submit" value="Consultar" /></td>
			</tr>
			
		 </tbody>		
		</table>
		
		<br/><br/><br/>
		
		<%-- 	<s:if test="propostas != null && propostas.size > 0"> --%>
	<!-- 		<table class="tabela_interna"> -->
	<!-- 			<tr> -->
	<!-- 				<th colspan="4">PROPOSTAS</th> -->
	<!-- 			</tr> -->
	<!-- 			<tr> -->
	<!-- 				<th width="150px;">C�digo</th> -->
	<!-- 				<th width="450px;">Nome do Benefici�rio</th> -->
	<!-- 				<th width="250px;">Status</th> -->
	<!-- 				<th width="250px;">Canal</th> -->
	<!-- 			</tr> -->
	<%-- 			<s:iterator value="propostas" var="proposta" status="status"> --%>
	<!-- 				<tr> -->
	<!-- 					<td><a -->
	<%-- 						href="visualizarProposta.do?proposta.sequencial=<s:property value='sequencial'/>"> --%>
	<%-- 							<s:property value="codigo" /> --%>
	<!-- 					</a></td> -->
	<%-- 					<td><s:property value="beneficiarios.titular.nome" /></td> --%>
	<%-- 					<td align="center"><s:property value="status" /></td> --%>
	<%-- 					<td align="center"><s:property value="canal.nome" /></td> --%>
	<!-- 				</tr> -->
	<%-- 			</s:iterator> --%>
	<!-- 		</table> -->
	
				<table class="tabela_interna">
					<tr>
						<th colspan="4">PROPOSTAS</th>
					</tr>		
				</table>
		
		
	<!-- 	<table class="tabela_interna">
				<tr style="border-right:#ffffff solid; border-right-width:2px;">
					<th colspan="4">PROPOSTAS</th>
				</tr> -->
				 
		<s:if test="propostas != null && propostas.size > 0">	
		<display:table id="propostasTable" uid="proposta" name="propostas" pagesize="20"
				class="tabela_interna"
				 requestURI="">
				<s:url id="visualizarProposta" var="visualizarProposta" action="visualizarPropostaIntranet">
					<s:param name="proposta.sequencial" value="%{#attr.proposta.sequencial}" />
				</s:url>			
				<display:column title="C�digo">
					<s:a href="%{#visualizarProposta}"><s:property value="%{#attr.proposta.codigo}"/></s:a>
				</display:column>
				
				<display:column title="Nome do Beneficiario">
						<s:property value="%{#attr.proposta.beneficiarios.titular.nome}"/>
				</display:column>
				<display:column title="Status">
						<s:property value="%{#attr.proposta.status}"/>
				</display:column>
				<display:column title="Canal">
						<s:property value="%{#attr.proposta.canal.nome}"/>
				</display:column>
			</display:table> 
				
				
			<%-- 	<s:iterator value="propostas" var="proposta" status="status" >
					<tr>
						<td>
								<a href="visualizarPropostaIntranet.do?proposta.sequencial=<s:property value='sequencial'/>" >
									<s:property value="codigo"/>
								</a>
						</td>
						<td><s:property value="beneficiarios.titular.nome"/></td>
						<td align="center"><s:property value="status"/></td>
						<td align="center"><s:property value="canal.nome"/></td>
					</tr>
				
				</s:iterator>  --%>
				
				
				</s:if>
	
		<br/><br/>
	</s:form>
</html>