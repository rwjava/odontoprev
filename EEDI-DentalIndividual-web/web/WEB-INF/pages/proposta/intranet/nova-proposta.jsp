<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
	<head>
		<title>Emiss�o Expressa Dental Individual</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<!-- Bradesco styles -->
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/css.css'/>" />
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/tabs.css'/>" />
		
		<!-- External plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-ui.custom.min.js'/>"></script>
		<script type="text/javascript" src="<s:url value="/includes/js/vendor/jquery.ui.datepicker-pt-BR.js" />" ></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.inputmask.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.blockUI.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.scrollto.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-maskmoney.js'/>"></script>
		
		<!-- WDEV plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-datatypes.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-ajax.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-block.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-required.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-inputcontrol.js'/>"></script>
		
		<script type="text/javascript" src="<s:url value='/includes/js/proposta/listar-proposta.js'/>"></script>
		<s:url var="baseURL" value="/"/>
		<script>window.baseURL = "${baseURL}";</script>
		
	

</head>
<script>
$(document).ready(function(){
// 	$('#forma_pagamento').val('#forma-pagamento-selecionado');	
	});

</script>
<link rel="stylesheet" type="text/css"
	href="<s:url value='/includes/css/bootstrap.min.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<s:url value='/includes/css/bootstrap-dialog.css'/>" />
<script type="text/javascript"
	src="<s:url value='/includes/js/bootstrap.min.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/includes/js/bootstrap-dialog.js'/>"></script>

<script
	src="https://www.pagador.com.br/post/scripts/silentorderpost-1.0.min.js"></script>
<script type="text/javascript"
	src="<s:url value='/includes/js/proposta/nova-proposta.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/includes/js/proposta/valida-dados-proposta.js'/>"></script>
<%-- 		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/custom.css'/>" /> --%>



<div id='content'>
			<s:hidden id="hasActionErrors" value="%{hasActionErrors()}" />
			<table width="974px;">
				<tr>
					<td>
						<br/>
							<span class="subtitulo">EMISS�O EXPRESSA INDIVIDUAL</span>
						<br/>
						<font style="color:#0071BB;font-size:14px;font-weight:bold;"><span id="subtitulo">Cadastro de Proposta</span></font>
						<div id="tabs" class="tabpanel"></div>
						<div id="popCalendario" class="pop_calendario_box"></div>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_verm" id="msgErros" style="display:none">
							<tbody>
								<tr>
									<td>
										<div id="serverErrors" class="messages">
											<s:iterator value="actionErrors">
												<li><s:property /></li>
											</s:iterator>
										</div>
										<div id="actionErrors" class="messages"></div>
									</td>
								</tr>
							</tbody>
						</table>
						<s:if test="hasActionMessages()">
							<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_sucesso" id="msgSucesso">
								<tbody>
									<tr>
										<td>
											<div id="serverActionsMessages" class="messages">
												<s:iterator value="actionMessages">
													<li><s:property /></li>
												</s:iterator>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</s:if>
						<br />
						
						<div class="tab-row menuList">
							<ul>
								<li id="nova_proposta" class="tab0">
									<a href="iniciarNovaPropostaIntranet.do"><span>NOVA PROPOSTA</span></a>
								</li>
								<li id="listar_proposta" class="tab1">
									<a href="listarPropostaIntranet.do"><span>LISTAR PROPOSTA</span></a>
								</li>
								<li id="relatorio_acompanhamento" class="tab2">
									<a href="relatorioAcompanhamentoIntranet.do"><span>RELATORIO DE ACOMPANHAMENTO</span></a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<s:if test="hasActionMessages()">
			<script>
				$("#msgSucesso").fadeOut(50000);
			</script>
		</s:if>

<s:form id="form" action="finalizarProposta">
	<table class="tabela_interna">
		<s:hidden id="plano_selecionado" name="codigoPlano"></s:hidden>
		<s:hidden name="proposta.status" id="status_proposta" />
		<tr>
			<th colspan="4">1. PROPOSTA</th>
		</tr>
		<tr>
				<td colspan="1" class="td_label" width="150px;">C�digo</td>
			<td colspan="3" width="400px;">
				<%-- <s:label id="sequencial_proposta"> --%>
					<label id="sequencial_proposta"><s:property value="proposta.codigo"></s:property></label> 
					
				<%--	</s:label>--%>

			</td>
			
		</tr>
		<tr>
			<td class="td_label" width="200px;">Canal de Venda</td>
			<td><s:if test="proposta.canal.nome == null">
						<label class="td_label">SITE 100% CORRETOR</label>
					</s:if>
					<s:else>
						<label class="status_label"><s:property value="proposta.canal.nome" /></label>
					</s:else></td>
			<td class="td_label" width="200px;">Status</td>
			<td><s:if test="status == null">
					RASCUNHO
				</s:if> <s:else>					
					<s:property value="proposta.status" />
				</s:else></td>
		</tr>
		<tr>
			<td class="td_label" width="200px;">Data de Emiss�o</td>
			<td><s:hidden name="proposta.dataEmissao" /> <s:property
					value="proposta.dataEmissao" /> <%-- 			<s:property value="proposta.canal.nome"/> --%>
			</td>
			<td class="td_label" width="200px;">Data de Validade do Rascunho</td>
			<td><s:hidden name="proposta.dataValidadeProposta" /> <s:property
					value="proposta.dataValidadeProposta" /></td>
		</tr>
		<s:if test="status == null">
			<tr class="imprimir_proposta_vazia">
				<td colspan="4" align="center"><input type="button"
					id="imprimir-proposta-vazia" value="Imprimir Proposta Vazia"
					style="cursor: pointer; cursor: hand; margin-top: 10px;"
					class="button margem_botoes" /></td>
			</tr>
		</s:if>

		<tr>
			<th colspan="4">2. CORRETOR</th>
		</tr>
		<s:if test="status == null">
			<tr>
				<td class="td_label" width="150px;">Nome</td>
				<td width="400px;"><s:hidden name="proposta.corretor.nome" />
					<span id="codigoProposta"><s:property
							value="proposta.corretor.nome" /></span></td>
				<td class="td_label" width="200px;">CPF/CNPJ</td>
				<td><s:hidden name="proposta.corretor.cpfCnpj" /> <s:property
						value="proposta.corretor.cpfCnpj" /></td>
			</tr>
			<tr>
				<td class="td_label" width="150px;">CPD/Sucursal<span
					class="obrigatorio">*</span></td>
				<td width="400px;" colspan="3"><s:select id="sucursalList"
						list="listaSucursais" listKey="cpdSucursal" listValue="nome"
						headerKey="0" headerValue="Selecione uma sucursal"
						name="proposta.sucursalSelecionada.cpdSucursal"></s:select></td>
			<tr>
				<td class="td_label" width="200px;">CPD Angariador<span
					class="obrigatorio">*</span></td>
				<td colspan="1"><s:label id="cpdAngariador"
						name="proposta.angariador.cpd" /></td>

				<td class="td_label" width="200px;">Angariador<span
					class="obrigatorio">*</span></td>
				<td colspan="2"><s:label id="nomeAngariador"
						name="proposta.angariador.nome" /></td>
			</tr>
			<tr>
				<td class="td_label" width="200px;">Ag�ncia Produtora<span
					class="obrigatorio">*</span></td>
				<td><s:textfield name="proposta.corretor.agenciaProdutora"
						id="agenciaProdutora" data-type="integer" size="5" maxlength="7" />
				</td>
				<td class="td_label" width="200px;">Assistente de Produ��o<span
					class="obrigatorio">*</span></td>
				<td><s:textfield name="proposta.corretor.assistenteProducao"
						id="assistenteProducao" data-type="integer" size="5" maxlength="7" />
				</td>
			</tr>
			
		</s:if>
		<s:else>
			<tr>
				<td class="td_label" width="150px;">Nome</td>
				<td width="400px;"><s:hidden name="proposta.corretor.nome" />
					<span id="codigoProposta"><s:property
							value="proposta.corretor.nome" /></span></td>
				<td class="td_label" width="200px;">CPF/CNPJ</td>
				<td><s:hidden name="proposta.corretor.cpfCnpj" /> <s:property
						value="proposta.corretor.cpfCnpj" /></td>
			</tr>
			<tr>
				<td class="td_label" width="150px;">CPD/Sucursal<span
					class="obrigatorio">*</span></td>
				<td width="400px;" colspan="3"><s:select disabled="true"
						id="sucursalList" list="listaSucursais" listKey="cpdSucursal"
						listValue="nome" headerKey="0"
						headerValue="Selecione uma sucursal"
						name="proposta.sucursalSelecionada.cpdSucursal"></s:select></td>
			<tr>
				<td class="td_label" width="200px;">CPD Angariador<span
					class="obrigatorio">*</span></td>
				<td colspan="1"><s:label id="cpdAngariador"
						name="proposta.angariador.cpd" /></td>

				<td class="td_label" width="200px;">Angariador<span
					class="obrigatorio">*</span></td>
				<td colspan="2"><s:label id="nomeAngariador"
						name="proposta.angariador.nome" /></td>
			</tr>
			<tr>
				<td class="td_label" width="200px;">Ag�ncia Produtora<span
					class="obrigatorio">*</span></td>
				<td><s:property value="proposta.corretor.agenciaProdutora" /></td>
				<td class="td_label" width="200px;">Assistente de Produ��o<span
					class="obrigatorio">*</span></td>
				<td><s:property value="proposta.corretor.assistenteProducao" />
				</td>
			</tr>
		</s:else>
		<tr>
				<td class="td_label" width="200px;">Gerente de Produto<span
					class="obrigatorio">*</span></td>
				<td colspan="3"><s:textfield
						name="proposta.codigoMatriculaGerente" data-type="integer"
						size="5" maxlength="7" /></td>
			</tr>
				<tr>
				<td class="td_label" width="200px;">Cpd Corretor Master<span
					class="obrigatorio">*</span></td>
				<td><s:textfield id="cpdCorretorLogado"
						name="proposta.corretor.cpd" data-type="integer" size="5"
						maxlength="6" /></td>
				<td class="td_label" colspan="1">Corretor Master</td>
				<td><s:label id="nomeCorretorMaster"
						name="proposta.corretorMaster.nome" /></td>
			</tr>
	</table>
	<table class="tabela_interna" id="tabela_planos">
		<tbody>
			<tr>
				<th colspan="7">3. PLANO</th>
			</tr>
			<tr>
				<th width="13%" rowspan="2">Plano</th>
				<th width="10%" colspan="2">Valor Titular</th>
				<th width="11%" colspan="2">Valor Dependente</th>
				<th width="28%" colspan="2">Per&iacute;odo de Car&ecirc;ncia</th>
			</tr>
			<tr id="tr_cabecalho_planos">
				<th>Mensal</th>
				<th>Anual</th>
				<th>Mensal</th>
				<th>Anual</th>
				<th>Mensal</th>
				<th>Anual</th>
			</tr>
		</tbody>
		<%-- 		<s:if test="planos == null || planos == 0"> --%>
		<!-- 			<tr> -->
		<!-- 				<td colspan="7" align="center">Nenhum plano vigente encontrado para o canal CORRETORA CALL CENTER PROPRIO</td> -->
		<!-- 			</tr> -->
		<%-- 		</s:if> --%>
		<%-- 		<s:else> --%>
		<%-- 			<s:iterator  id="planos" value="planos" var="plano" status="status"> --%>
		<!-- 				<tr> -->
		<!-- 			        <td> -->
		<%-- 			        	<s:if test="codigo == proposta.plano.codigo"> --%>
		<%-- 			        		<input type="radio" name="proposta.plano.codigo" value="<s:property value='codigo'/>" checked="checked"><s:property value="nome"/> --%>
		<%-- 			        	</s:if> --%>
		<%-- 			        	<s:else> --%>
		<%-- 			        		<input id="testeRadio" type="radio" name="proposta.plano.codigo" value="<s:property value='codigo'/>"><s:property value="nome"/> --%>
		<%-- 			        	</s:else> --%>
		<!-- 			        </td> -->
		<!-- 			        <td> -->
		<%-- 			        	<fmt:formatNumber value="${plano.valorPlanoVO.valorMensalTitular}" type="currency" currencySymbol="R$"/> --%>
		<!-- 			        </td> -->
		<!-- 			        <td> -->
		<%-- 			        	<fmt:formatNumber value="${plano.valorPlanoVO.valorAnualTitular}" type="currency" currencySymbol="R$"/> --%>
		<!-- 			        </td> -->
		<!-- 			        <td> -->
		<%-- 			        	<fmt:formatNumber value="${plano.valorPlanoVO.valorMensalDependente}" type="currency" currencySymbol="R$"/> --%>
		<!-- 			        </td> -->
		<!-- 			        <td> -->
		<%-- 			        	<fmt:formatNumber value="${plano.valorPlanoVO.valorAnualDependente}" type="currency" currencySymbol="R$"/> --%>
		<!-- 			        </td> -->
		<%-- 			        <td><s:property value="descricaoCarenciaPeriodoMensal"/></td> --%>
		<%-- 			        <td><s:property value="descricaoCarenciaPeriodoAnual"/></td> --%>
		<!-- 			     </tr> -->
		<%-- 			</s:iterator> --%>
		<%-- 		</s:else>		 --%>
		<!-- tr>
			<td colspan="7" align="center">
				<a href="#" class="margem_botoes"><img border="0" src="<s:url value='/includes/img/ic_sbox_calculadora.gif'/>">Simular Contrata��o</a>
			</td>
		</tr-->
	</table>
	<table class="tabela_interna">
		<tr>
			<th colspan="2">4. BENEFICI�RIOS</th>
		</tr>
		<tr>
			<td class="td_label" width="292px;">O benefici�rio titular �
				menor de idade?<span class="obrigatorio">*</span>
			</td>
			<td>
				<!-- input type="radio" name="proposta.beneficiarios.indicadorTitularMenorIdade" value="true" />Sim
	        	<input type="radio" name="proposta.beneficiarios.indicadorTitularMenorIdade" value="false" />N�o-->
				<s:radio name="proposta.beneficiarios.titularMenorIdade"
					list="#{true:'Sim',false:'N�o'}" />
			</td>
		</tr>
	</table>
	<table class="tabela_interna" id="representante_legal"
		style="display: none;">
		<tr>
			<th colspan="4">Representante Legal</th>
		</tr>
		<tr>
			<td class="td_label" width="127px;">Nome<span
				class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.representanteLegal.nome" size="40"
					maxlength="100" style="text-transform: uppercase;" /></td>
			<td class="td_label" width="149px;">CPF<span class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.representanteLegal.cpf"
					data-mask="cpf" size="11" maxlength="14" /></td>
		</tr>
		<tr>
			<td class="td_label">Nascimento<span class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.representanteLegal.dataNascimento"
					data-mask="data" size="7" maxlength="10" /></td>
			<td class="td_label">E-mail<span class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.representanteLegal.email" size="30"
					maxlength="100" style="text-transform: uppercase;" /></td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>ENDERE�O</b></td>
		</tr>
		<tr>
			<td class="td_label">CEP<span class="obrigatorio">*</span></td>
			<td colspan="3"><s:textfield
					name="proposta.beneficiarios.representanteLegal.endereco.cep"
					data-mask="cep" size="8" maxlength="11" /></td>
		</tr>
		<tr class="cep_representante_legal" style="display: none;">
			<td class="td_label">Endere�o</td>
			<td>
				<input type="hidden" name="proposta.beneficiarios.representanteLegal.endereco.logradouro">
				<s:textfield id="logradouro-representante"
					name="proposta.beneficiarios.representanteLegal.endereco.logradouro"
					size="40" maxlength="50" />
			</td>
			<td class="td_label">N�mero<span class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.representanteLegal.endereco.numero"
					data-type="integer" size="5" maxlength="5" /></td>
		</tr>
		<tr class="cep_representante_legal" style="display: none;">
			<td class="td_label">Complemento</td>
			<td><s:textfield
					name="proposta.beneficiarios.representanteLegal.endereco.complemento"
					size="24" style="text-transform: uppercase;" /></td>
			<td class="td_label">Bairro</td>
			<td><s:hidden
					name="proposta.beneficiarios.representanteLegal.endereco.bairro" />
				<label id="bairro-representante">
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.bairro" />
				</label>
				<!-- s:textfield name="proposta.beneficiarios.representanteLegal.endereco.bairro" size="30" /-->
			</td>
		</tr>
		<tr class="cep_representante_legal" style="display: none;">
			<td class="td_label">Cidade</td>
			<td><s:hidden
					name="proposta.beneficiarios.representanteLegal.endereco.cidade" />
				<label id=cidade-representante>
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.cidade" />
				</label>
			</td>
			<td class="td_label">Estado</td>
			<td><s:hidden
					name="proposta.beneficiarios.representanteLegal.endereco.estado" />
				<label id="estado-representante">
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.estado" />
				</label>
			</td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>CONTATO</b></td>
		</tr>
		<tr>
			<td class="td_label">Tipo de Telefone<span class="obrigatorio">*</span></td>
			<td><s:select
					name="proposta.beneficiarios.representanteLegal.telefones[0].tipoTelefone"
					list="tiposTelefone" listValue="nome" listKey="codigo"
					headerKey="0" headerValue="Selecione"
					id="tipo_telefone_representante_legal"
					data-telefone="telefone_representante_legal" /></td>
			<td class="td_label">N� Telefone / Ramal<span
				class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.representanteLegal.telefones[0].dddEnumero"
					id="telefone_representante_legal"
					data-telefone="telefone_representante_legal" data-mask="telefone"
					size="11" /> / <s:textfield
					name="proposta.beneficiarios.representanteLegal.telefones[0].ramal"
					data-type="integer" size="3" maxlength="4" /></td>
		</tr>

	</table>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">Titular</th>
		</tr>
		<tr>					
			<td class="td_label" width="125px;">Nome Completo<span
				class="obrigatorio">*</span></td>
			<td colspan="3"><s:textfield name="proposta.beneficiarios.titular.nome"
					size="40" maxlength="100" style="text-transform: uppercase;" /></td>
		</tr>
		<tr>
			<td class="td_label" width="125px;">Nome da M�e<span
				class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.titular.nomeMae" size="40"
					maxlength="100" style="text-transform: uppercase;" /></td>
			<td class="td_label" width="150px;">CPF<span class="obrigatorio">*</span></td>
			<td><s:textfield name="proposta.beneficiarios.titular.cpf"
					data-mask="cpf" size="11" maxlength="14" /></td>
		</tr>
		<tr>
			<td class="td_label">Nascimento<span class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.titular.dataNascimento"
					data-mask="data" size="11" maxlength="11" /></td>
			<td class="td_label">E-mail<span class="obrigatorio">*</span></td>
			<td><s:textfield name="proposta.beneficiarios.titular.email"
					size="30" maxlength="100" style="text-transform: uppercase;" /></td>
		</tr>
		<tr>
			<td class="td_label">Sexo<span class="obrigatorio">*</span></td>
			<td><s:radio name="proposta.beneficiarios.titular.sexo"
					list="sexos" listKey="codigo" listValue="nome" /></td>
			<td class="td_label">Estado Civil<span class="obrigatorio">*</span></td>
			<td><s:select
					name="proposta.beneficiarios.titular.estadoCivil.codigo"
					list="estadosCivis" listKey="codigo" listValue="descricao"
					headerKey="0" headerValue="Selecione" /></td>
		</tr>
		<tr>
			<td class="td_label">CNS</td>
			<td><s:textfield name="proposta.beneficiarios.titular.cns"
					data-type="integer" size="20" maxlength="20" /></td>
			<td class="td_label">DNV</td>
			<td><s:textfield name="proposta.beneficiarios.titular.dnv"
					data-type="integer" size="20" maxlength="20" /></td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>ENDERE�O</b></td>
		</tr>
		<tr>
			<td class="td_label">CEP<span class="obrigatorio">*</span></td>
			<td colspan="3"><s:textfield
					name="proposta.beneficiarios.titular.endereco.cep" data-mask="cep"
					size="8" maxlength="11" /> <!-- &nbsp;
				<input type="button" value="Buscar">  --></td>
		</tr>
		<tr class="cep_titular" style="display: none;">
			<td class="td_label">Logradouro<span class="obrigatorio">*</span></td>
			<td>
			<input type="hidden" name="proposta.beneficiarios.titular.endereco.logradouro">
			<s:textfield id="logradouro-titular"
					name="proposta.beneficiarios.titular.endereco.logradouro"
					size="40" maxlength="50" />	
			</td>
			<td class="td_label">N�mero<span class="obrigatorio">*</span></td>
			<td><s:textfield
					name="proposta.beneficiarios.titular.endereco.numero"
					data-type="integer" size="5" maxlength="5" /></td>
		</tr>
		<tr class="cep_titular" style="display: none;">
			<td class="td_label">Complemento</td>
			<td><s:textfield
					name="proposta.beneficiarios.titular.endereco.complemento"
					size="24" style="text-transform: uppercase;" /></td>
			<td class="td_label">Bairro</td>
			<td><s:hidden
					name="proposta.beneficiarios.titular.endereco.bairro"  />
					<label id="bairro-titular">
					<s:property 
					value="%{proposta.beneficiarios.titular.endereco.bairro}" /> <!-- s:textfield name="proposta.beneficiarios.titular.endereco.bairro" size="30" /-->
					</label>
					
			</td>
		</tr>
		<tr class="cep_titular" style="display: none;">
			<td class="td_label">Cidade</td>
			<td><s:hidden
					name="proposta.beneficiarios.titular.endereco.cidade"  value="%{proposta.beneficiarios.titular.endereco.cidade}"/>
				<label id="cidade-titular">	
					<s:property
						value="proposta.beneficiarios.titular.endereco.cidade" />
					</label>
			</td>
			<td class="td_label">Estado</td>
			<td><s:hidden
					name="proposta.beneficiarios.titular.endereco.estado" />
				<label id="estado-titular">
				 <s:property
					value="proposta.beneficiarios.titular.endereco.estado" />
				</label>
			</td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>CONTATO</b><span
				class="obrigatorio">*</span></td>
		</tr>
		<tr>
			<td class="td_label">Tipo de Telefone</td>
			<td>Celular <s:hidden
					name="proposta.beneficiarios.titular.telefones[0].tipoTelefone" />
			</td>
			<td class="td_label">N� Telefone</td>
			<td><s:textfield
					name="proposta.beneficiarios.titular.telefones[0].dddEnumero"
					data-mask="celular" size="11" /></td>
		</tr>
		<tr>
			<td class="td_label">Tipo de Telefone</td>
			<td>Residencial <s:hidden
					name="proposta.beneficiarios.titular.telefones[2].tipoTelefone" />
			</td>
			<td class="td_label">N� Telefone</td>
			<td><s:textfield
					name="proposta.beneficiarios.titular.telefones[2].dddEnumero"
					data-mask="telefone" size="11" /></td>
		</tr>
		<tr>
			<td class="td_label">Tipo de Telefone</td>
			<td>Comercial <s:hidden
					name="proposta.beneficiarios.titular.telefones[1].tipoTelefone" />
			</td>
			<td class="td_label">N� Telefone / Ramal</td>
			<td><s:textfield
					name="proposta.beneficiarios.titular.telefones[1].dddEnumero"
					data-mask="telefone" size="11" /> / <s:textfield
					name="proposta.beneficiarios.titular.telefones[0].ramal"
					data-type="integer" size="3" maxlength="4" /></td>
		</tr>
		
	</table>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">Dependente(s)</th>
		</tr>
		<s:if
			test="proposta.beneficiarios.dependentes == null || proposta.beneficiarios.dependentes.size == 0">
			<tr>
				<td colspan="4" align="center"><a href="javacsript:void(0);"
					class="margem_botoes" id="adicionar_dependente"><img border="0"
						src="<s:url value='/includes/img/beneficiario_add.png'/>">
						Adicionar Dependente</a></td>
			</tr>
		</s:if>
	</table>
	<s:iterator value="proposta.beneficiarios.dependentes" var="dependente"
		status="status">
		<table class="tabela_interna" id="dependentes">
			<s:if test="%{#status.index > 0}">
				<tr>
					<td class="td_label" colspan="4" align="center"><b>DEPENDENTE</b></td>
				</tr>
			</s:if>
			<tr>
				<td class="td_label" width="125px;">Nome da M�e</td>
				<td colspan="3"><s:textfield
						name="proposta.beneficiarios.dependentes[%{#status.index}].nomeMae"
						size="35" maxlength="100" style="text-transform: uppercase;" /></td>
			</tr>
			<tr>
				<td class="td_label" width="125px;">Nome Completo<span
					class="obrigatorio">*</span></td>
				<td width="380px;"><s:textfield
						name="proposta.beneficiarios.dependentes[%{#status.index}].nome"
						size="40" maxlength="100" style="text-transform: uppercase;" /></td>
				<td class="td_label" width="125px;">CPF</td>
				<td><s:textfield
						name="proposta.beneficiarios.dependentes[%{#status.index}].cpf"
						data-mask="cpf" size="11" maxlength="14" /></td>
			</tr>
			<tr>
				<td class="td_label">Nascimento<span class="obrigatorio">*</span></td>
				<td><s:textfield
						name="proposta.beneficiarios.dependentes[%{#status.index}].dataNascimento"
						data-mask="data" size="11" maxlength="11" /></td>
				<td class="td_label">Parentesco<span class="obrigatorio">*</span></td>
				<td><s:select
						name="proposta.beneficiarios.dependentes[%{#status.index}].grauParentescoBeneficiario.codigo"
						list="grausParentesco" listKey="codigo" listValue="nome"
						headerKey="0" headerValue="Selecione" /></td>
			</tr>
			<tr>
				<td class="td_label">Sexo<span class="obrigatorio">*</span></td>
				<td><s:radio
						name="proposta.beneficiarios.dependentes[%{#status.index}].sexo"
						list="sexos" listKey="codigo" listValue="nome" /></td>
				<td class="td_label">Estado Civil<span class="obrigatorio">*</span></td>
				<td><s:select
						name="proposta.beneficiarios.dependentes[%{#status.index}].estadoCivil.codigo"
						list="estadosCivis" listKey="codigo" listValue="descricao"
						headerKey="0" headerValue="Selecione" /></td>
			</tr>
			<tr>
				<td class="td_label">CNS</td>
				<td><s:textfield
						name="proposta.beneficiarios.dependentes[%{#status.index}].cns"
						data-type="integer" size="20" maxlength="20" /></td>
				<td class="td_label">DNV</td>
				<td><s:textfield
						name="proposta.beneficiarios.dependentes[%{#status.index}].dnv"
						data-type="integer" size="20" maxlength="20" /></td>
			</tr>
			<s:if
				test="%{proposta.beneficiarios.dependentes.size() == #status.index + 1}">
				<tr>
					<td colspan="2" align="right"><a href="javacsript:void(0);"
						class="margem_botoes" id="adicionar_dependente"><img
							border="0"
							src="<s:url value='/includes/img/beneficiario_add.png'/>">
							Adicionar Dependente</a></td>
					<td colspan="2" align="left"><a href="javacsript:void(0);"
						class="margem_botoes" onclick="javascript: removerDependente(<s:property value='#status.index'/>);"><img
							border="0"
							src="<s:url value='/includes/img/beneficiario_del.png'/>">
							Remover Dependente</a></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td colspan="4" align="center"><a href="javacsript:void(0);"
						class="margem_botoes"
						onclick="javascript: removerDependente(<s:property value='#status.index'/>);"><img
							border="0"
							src="<s:url value='/includes/img/beneficiario_del.png'/>">
							Remover Dependente</a></td>
				</tr>
			</s:else>
		</table>
	</s:iterator>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">5. PAGAMENTO</th>
		</tr>
		<tr>
			<td class="td_label" width="170px;">N�mero de Vidas</td>
			<td colspan="3">
				<s:if test="proposta.beneficiarios.dependentes == null || proposta.beneficiarios.dependentes.size == 0">
						<span id="numero_vidas">1</span>
				</s:if> 
				<s:else>
						<span id="numero_vidas"><s:property value="%{proposta.beneficiarios.dependentes.size + 1}" /></span>
				</s:else>
			</td>
		</tr>
		<tr>

			<td class="td_label" width="170px;">Valor Total Mensal</td>
			<td width="368px;">
				<label id="mensal-label">
					<fmt:formatNumber  value="${valorTotalMensal}" type="currency" currencySymbol="R$" />
				</label>
			</td>
			<td class="td_label" width="140px;">Valor Total Anual</td>
			<td>
				<label id="anual-label">
					<fmt:formatNumber value="${valorTotalAnual}" type="currency" currencySymbol="R$" />
				</label>
			</td>
			<s:hidden value="%{valorTotalMensal}" id="valorTotalMensal"></s:hidden>
			<s:hidden value="%{valorTotalAnual}" id="valorTotalAnual"></s:hidden>
		<tr>
			<td class="td_label">Tipo de Cobran�a<span class="obrigatorio">*</span></td>
			<td colspan="3">
			<s:radio name="proposta.pagamento.codigoTipoCobranca" list="tiposCobranca" listValue="nome" listKey="codigo" /></td>
		</tr>
		<tr>
			<td class="td_label">Forma de Pagamento<span class="obrigatorio">*</span></td>
			<td colspan="4">
				<!-- s:radio id="forma_pagamento" name="proposta.pagamento.formaPagamento" list="formasPagamento" listValue="nome" listKey="codigo"/-->
				<s:hidden value="%{proposta.pagamento.formaPagamento}"
					id="forma-pagamento-selecionado" />
				
				<s:select id="forma_pagamento"
					name="proposta.pagamento.formaPagamento" list="formasPagamento"
					listKey="codigo" listValue="descricao" headerKey="0"
					headerValue="Selecione" />
					
			</td>
		</tr>
		
		<!-- DEBITO AUTOMATICO -->
		<tr class="debito_automatico">
				<td class="td_label" align="center" colspan="4"><b>D�BITO AUTOM�TICO</b></td>
			</tr>
			<tr class="debito_automatico">
				<td class="td_label">CPF</td>
				<td colspan="3"><s:textfield size="12" class="cpf"
					id="debito.cpf" name="proposta.pagamento.contaCorrente.cpf"
					data-mask="cpf" maxlength="14" /></td>
			</tr>
			
			<tr class="debito_automatico">
				<td class="td_label">Nome</td>
				<td>
					<s:textfield size="35" maxlength="80" id="debito.nome"
					name="proposta.pagamento.contaCorrente.nome"
					style="text-transform: uppercase;" />
					
				</td>
				<td class="td_label">Data de Nascimento</td>
				<td>
					<s:textfield class="dataNascimentoProponente"
					id="debito.dataNascimento"
					name="proposta.pagamento.contaCorrente.dataNascimento"
					data-mask="data" size="11" maxlength="11" />
				</td>
			</tr>
			
			<tr class="debito_automatico">
				<td class="td_label">Banco</td> 
				<td>
					<s:select id="banco"
					name="proposta.pagamento.contaCorrente.banco.codigo" list="bancos"
					listKey="codigo" listValue="descricao" headerKey="0"
					headerValue="Selecione" />
				</td>
				<td class="td_label">Ag�ncia/Conta</td>
				<td>
					<s:textfield maxlength="4" size="2"
					id="proposta.pagamento.contaCorrente.numeroAgencia"
					name="proposta.pagamento.contaCorrente.numeroAgencia"
					data-type="integer" /> / 
					<s:textfield size="7"
					id="proposta.pagamento.contaCorrente.numeroConta"
					name="proposta.pagamento.contaCorrente.numeroConta" maxlength="9"
					cssClass="numero_conta" data-type="integer" />
					-
					<s:textfield type="text" size="1" maxlength="1"
					name="proposta.pagamento.contaCorrente.digitoVerificadorConta"
					cssClass="digito_verificador" style="text-transform: uppercase;"
					id="numeroContaCorrente.digitoVerificador" />
				</td>	
			</tr>
			<!-- FIm DEBITO AUTOMATICO -->
			
					<!-- CARTAO CREDITO -->
			
			<tr class="cartao_credito">
				<td class="td_label" align="center" colspan="4"><b>CART�O DE CR�DITO</b></td>
			</tr>
			<tr class="cartao_credito">
				<td class="td_label">Nome</td>
				<td colspan="3"><input type="text" size="35"
				id="cartaoCredito.nomeCartao"
				name="proposta.pagamento.cartaoCredito.nomeCartao"
				class="bp-sop-cardholdername" /></td>
			</tr>
			<tr class="cartao_credito">
				<td class="td_label">N�mero</td>
				<td><input type="text" size="18" maxlength="18"
				id="cartaoCredito.numeroCartao"
				name="proposta.pagamento.cartaoCredito.numeroCartao"
				class="bp-sop-cardnumber" onkeypress='return SomenteNumero(event)' /></td>
				<td class="td_label">Validade</td>
				<td><input type="text" size="6" id="cartaoCredito.validade"
				name="proposta.pagamento.cartaoCredito.validade"
				class="bp-sop-cardexpirationdate" /></td>
			</tr>
			<tr class="cartao_credito">
				<td class="td_label">Bandeira</td>
				<td colspan="3">
					<s:select id="bandeiras"
					name="proposta.pagamento.cartaoCredito.bandeira" list="bandeiras"
					listKey="codigo" listValue="nome" headerKey="0"
					headerValue="Selecione" />
				</td>

			</tr>
	
		
				<!-- FIM CARTAO CREDITO -->
		
		
		
		<!-- tr>
			<td class="td_label">Data de Cobran�a<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:textfield name="proposta.pagamento.dataCobranca" data-mask="data" size="8" maxlength="11" /> (15 a 45 dias da data atual)
			</td>
		</tr-->
<!-- 		<tr class="banco_pagamento"> -->
<%-- 			<td class="td_label">Banco<span class="obrigatorio">*</span></td> --%>
<!-- 			<td colspan="3"> -->
<%-- 				<s:select id="banco" name="proposta.pagamento.contaCorrente.banco" list="bancos" listKey="codigo" listValue="descricao" headerKey="0" headerValue="Selecione"/> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr class="cpf_pagamento" style="display:none;"> -->
<!-- 			<td class="td_label" colspan="4" align="center"><b>PROPONENTE</b></td> -->
<!-- 		</tr> -->
<!-- 		<tr class="cpf_pagamento" style="display:none;"> -->
<%-- 			<td class="td_label">CPF<span class="obrigatorio">*</span></td> --%>
<!-- 			<td colspan="3"> -->
<%-- 				<s:textfield name="proposta.pagamento.contaCorrente.cpf" id="cpf_pagamento" data-mask="cpf" size="12" maxlength="14" /> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr class="dados_debito_automatico" style="display:none;"> -->
<%-- 			<td class="td_label">Nome<span class="obrigatorio">*</span></td> --%>
<%-- 			<td ><s:textfield name="nomePagante" id="nome_pagante" size="40" style="text-transform: uppercase;"/></td> --%>
<%-- 			<td class="td_label">Nascimento<span class="obrigatorio">*</span></td> --%>
<%-- 			<td ><s:textfield name="proposta.pagamento.contaCorrente.dataNascimento" id="nascimento_pagante" data-mask="data" size="9" maxlength="10" /></td> --%>
<!-- 		</tr> -->
<!-- 		<tr class="dados_debito_automatico_outros_bancos" style="display:none;"> -->
<%-- 			<td class="td_label">Ag�ncia / Conta<span class="obrigatorio">*</span></td> --%>
<!-- 			<td colspan="3"> -->
<%-- 				<s:textfield name="proposta.pagamento.contaCorrente.numeroAgencia" id="proposta.pagamento.contaCorrente.numeroAgencia" data-type="integer" size="4" maxlength="4" /> / <s:textfield name="proposta.pagamento.contaCorrente.numeroConta" id="proposta.pagamento.contaCorrente.numeroConta" data-type="integer" size="9" maxlength="9" /> - <s:textfield name="proposta.pagamento.contaCorrente.digitoVerificadorConta" id="digito_conta" size="1" maxlength="1" /> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		<!-- tr class="dados_debito_automatico_bradesco" style="display:none;">
			<td class="td_label">Ag�ncia / Conta<span class="obrigatorio">*</span></td>
			<td colspan="3">
				<s:select name="contaCorrenteFormatada" id="agencia_conta" list="contasCorrente" headerKey="0" headerValue="Selecione" listKey="numeroConta" listValue="contaCorrenteFormatada"/>
			</td>
		</tr-->
	</table>
	
	<br />
	<br />
	<br />
	
	<table style="width: 1050px;">
		<tr>
			<!-- td width="45%" align="right"><input type="button" value="Limpar" id="btn_limpar"/></td>
			<td width="10%"></td-->
			<s:if test="proposta.status == null || proposta.status == 'RASCUNHO' ">
			
				<td align="center">
				  <input type="button" value="Limpar" id="btn_limpar" />
				</td>
				
				<td align="center"><input id="salvar-rascunho" name="salvar-rascunho" type="button" value="Salvar Rascunho" /></td>		
				
				<td align="center"><input id="finalizar" name="finalizar" type="button" value="Finalizar" /></td>
				
				<td align="center" style="display:none;" id="cancelar-proposta_finalizada">
				  <input type="button" value="Cancelar" id="cancelar-proposta_finalizada" class="cancelar-proposta" /> 
				</td>
				
				<td align="right" style="display:none;" id="imprimir_proposta_finalizada">
					<input id="imprimir_proposta_finalizada_botao" name="imprimir" type="button" value="Imprimir" class="margem_botoes imprimir-proposta" />
				</td>
			</s:if>
			
			<s:elseif test="proposta.status == 'EM PROCESSAMENTO' ">
				<td align="center">
					<input type="button" value="Limpar" id="btn_limpar" />
				</td>
				<td align="center" id="imprimir_proposta_finalizada">
					<input style="margin-top: 10px;" id="imprimir_proposta_finalizada_botao" name="imprimir" type="button" value="Imprimir" class="margem_botoes imprimir-proposta" />
				</td>
			</s:elseif>
			
			<s:elseif  test="proposta.status == 'CRITICADA'" >
				<td align="center">
					<input type="button" value="Limpar" id="btn_limpar" />
				</td>	
				<td align="center">
				   <input id="finalizar" name="finalizar"type="button" value="Finalizar" />
				</td>
					
				<td align="center">
					<input type="button" value="Cancelar" id="cancelar-proposta" class="cancelar-proposta" />
				</td>

			</s:elseif>

			<s:elseif test="proposta.status == 'CANCELADA' || proposta.status == 'PR�-CANCELADA' ">
				<td align="center"><input type="button" value="Limpar"
					id="btn_limpar" /></td>
					
			</s:elseif>
			<s:elseif test="proposta.status == 'IMPLANTADA' ">
				<!-- Ocultar botoes quando nestes status -->
				<td align="center">
					<input type="button" value="Limpar" id="btn_limpar" />
				</td>
				<td align="center" id="imprimir_proposta_finalizada">
					<input style="margin-top: 10px;" id="imprimir_proposta_finalizada_botao" name="imprimir" type="button" value="Imprimir" class="margem_botoes imprimir-proposta" />
				</td>
			</s:elseif>
			<s:elseif
				test="proposta.status == 'PENDENTE'">
				<!-- Ocultar botoes quando nestes status -->
				<td align="center"><input type="button" value="Limpar"
					id="btn_limpar" /></td>
					<td align="center"><input type="button" value="Cancelar" class="cancelar-proposta"
					id="cancelar-proposta" /></td>
			</s:elseif>
			<s:if
				test="proposta.status != null && proposta.status != 'RASCUNHO'  ">
					<td align="right" id="imprimir_proposta_finalizada">
<%-- 						<a href="#" class="margem_botoes"><img border="0" src="<s:url value='/includes/img/icon_pdf.png'/>">Imprimir</a> --%>
<!-- 	<a href="#" id="imprimir-proposta" class="margem_botoes">Imprimir</a> -->
<!-- 							<a href="javacsript:void(0);" id="imprimir-proposta" class="margem_botoes" onclick="return false;">Imprimir</a> 	 -->
							<input id="imprimir-proposta" name="imprimir" type="button" value="Imprimir" class="margem_botoes imprimir-proposta" />
					</td>	
			</s:if>
		</tr>
	</table>
	
	<br />
	<br />
	<s:hidden name="proposta.pagamento.cartaoCredito.accessToken"
		cssClass="accessToken"></s:hidden>
	<s:hidden name="proposta.pagamento.cartaoCredito.paymentToken"
		cssClass="paymentToken"></s:hidden>
<%-- 	<s:debug/> --%>
</s:form>
