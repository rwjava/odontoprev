<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
		<title>Emiss�o Expressa Dental Individual</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<!-- Bradesco styles -->
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/css.css'/>" />
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/tabs.css'/>" />
		
		<!-- External plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-ui.custom.min.js'/>"></script>
		<script type="text/javascript" src="<s:url value="/includes/js/vendor/jquery.ui.datepicker-pt-BR.js" />" ></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.inputmask.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.blockUI.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.scrollto.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-maskmoney.js'/>"></script>
		
		<!-- WDEV plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-datatypes.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-ajax.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-block.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-required.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-inputcontrol.js'/>"></script>
		
		<s:url var="baseURL" value="/"/>
		<script>window.baseURL = "${baseURL}";</script>
		
	

</head>
<script type="text/javascript" src="<s:url value='/includes/js/proposta/relatorio-acompanhamento.js'/>"></script>
<div id='content'>
			<s:hidden id="hasActionErrors" value="%{hasActionErrors()}" />
			<table width="974px;">
				<tr>
					<td>
						<br/>
							<span class="subtitulo">EMISS�O EXPRESSA INDIVIDUAL</span>
						<br/>
						<font style="color:#0071BB;font-size:14px;font-weight:bold;"><span id="subtitulo">Cadastro de Proposta</span></font>
						<div id="tabs" class="tabpanel"></div>
						<div id="popCalendario" class="pop_calendario_box"></div>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_verm" id="msgErros" style="display:none">
							<tbody>
								<tr>
									<td>
										<div id="serverErrors" class="messages">
											<s:iterator value="actionErrors">
												<li><s:property /></li>
											</s:iterator>
										</div>
										<div id="actionErrors" class="messages"></div>
									</td>
								</tr>
							</tbody>
						</table>
						<s:if test="hasActionMessages()">
							<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_sucesso" id="msgSucesso">
								<tbody>
									<tr>
										<td>
											<div id="serverActionsMessages" class="messages">
												<s:iterator value="actionMessages">
													<li><s:property /></li>
												</s:iterator>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</s:if>
						<br />
						
						<div class="tab-row menuList">
							<ul>
								<li id="nova_proposta" class="tab0">
									<a href="iniciarNovaPropostaIntranet.do"><span>NOVA PROPOSTA</span></a>
								</li>
								<li id="listar_proposta" class="tab1">
									<a href="listarPropostaIntranet.do"><span>LISTAR PROPOSTA</span></a>
								</li>
								<li id="relatorio_acompanhamento" class="tab2">
									<a href="relatorioAcompanhamentoIntranet.do"><span>RELATORIO DE ACOMPANHAMENTO</span></a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<s:if test="hasActionMessages()">
			<script>
				$("#msgSucesso").fadeOut(50000);
			</script>
		</s:if>

<s:form id="form">
	<table class="tabela_interna">
		<tr>
			<th colspan="4">RELAT�RIO DE ACOMPANHAMENTO</th> 
		</tr>
		<tr>
			<td class="td_label" colspan="1">Per�odo Inicial</td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.filtroPeriodoVO.dataInicio" data-mask="data" size="16" maxlength="11" cssClass="lista-proposta" />
			</td>
			<td class="td_label" colspan="1">Per�odo Final</td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.filtroPeriodoVO.dataFim" data-mask="data" size="16" maxlength="11" cssClass="lista-proposta" />
			</td>
		</tr>
		
		<tr>
			<td class="td_label" colspan="1">C�digo Proposta<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoProposta" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">CPF Benefici�rio Titular<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.cpfBeneficiarioTitular" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
		</tr>
		
		<tr>
			<td class="td_label" colspan="1">Sucursal<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.sucursal" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">CPD do Corretor<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.cpdCorretor" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
		</tr>
		
		<tr>
			<td class="td_label" colspan="1">CPF/CNPJ Corpo<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.cpfCnpjCorretor" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">C�digo Ag�ncia D�bito<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoAgenciaDebito" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
		</tr>
	
		<tr>
			<td class="td_label" colspan="1">C�digo AG Produtora<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoAgenciaProdutora" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">C�digo Assistente BS<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoAssistente" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
		</tr>
		
		<tr>
			<td class="td_label" colspan="1">C�digo Gerente Produto BVP<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoGerenteProdutoBVP" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">Status Atual Proposta<span class="obrigatorio">*</span></td>
			<td>					
						<select name="filtroRelatorioAcompanhamentoVO.codigoStatusProposta" class="lista-proposta">
								<option selected="selected" value="">Escolha</option>
								<option value="0">PENDENTE</option>
								<option value="1">RASCUNHO</option>
								<option value="2">EM PROCESSAMENTO</option>
								<option value="3">CRITICADA</option>
								<option value="4">CANCELADA</option>
								<option value="5">IMPLANTADA</option>
								<option value="6">PR�-CANCELADA</option>
								<option value="7">TODAS</option>
						</select>
			</td>
			
			
		</tr>
		
		
	</table>
	
	<br/><br/><br/>
	
	<table style="width: 974px;">
		<tr>
			<!-- td width="45%" align="right"><input type="button" value="Limpar" id="btn_limpar"/></td>
			<td width="10%"></td-->
			<td align="center"><input id="btn_limpar_intranet" type="button" value="Limpar"/></td>
			<td align="center"><input id="buscarAcompanhamentoIntranet" type="button" value="Buscar"/></td>
			<td align="center"><input type="button" value="Gerar Relat�rio" id="gerarCSVAcompanhamentoIntranet"/></td>
			<td align="center"><input id="gerarPDFAcompanhamentoIntranet" type="button" value="Gerar PDF"/></td>
		</tr>
	</table>
	
	<br/><br/><br/>
	
		<table class="tabela_interna">
			<tr>
				<th colspan="7">PROPOSTAS</th>
			</tr>
			<tr>
				<th width="150px;">C�digo</th>
				<th width="150px;">CPF do Proponente</th>
				<th width="450px;">Nome do Proponente</th>
			    <th width="150px;">CPF do Respons�vel</th>
				<th width="450px;">Nome do Respons�vel</th>
				<th width="150px;">CPF do Benefici�rio Titular</th>
				<th width="450px;">Nome do Benefici�rio Titular</th>
			</tr>
			<s:iterator value="listaAcompanhamento" var="proposta" status="status">
				<tr>
					<td>
						<s:if test="canal.codigo == 8">
<%-- 							<a href="visualizarProposta.do?proposta.sequencial=<s:property value='sequencial'/>" > --%>
<%-- 								<s:property value="codigo"/> --%>
<!-- 							</a> -->
							<a href="#" >
								<s:property value="codProposta"/>
							</a>
						</s:if>
						<s:else>
							<s:property value="codProposta"/>
						</s:else>
					</td>
					<td><s:property value="cpfTitularConta"/></td>
					<td><s:property value="nomeTitularConta"/></td>					
					<td><s:property value="cpfResponsavel"/></td>
					<td><s:property value="nomeResponsavel"/></td>
					<td><s:property value="cpfBenificiarioTitular"/></td>
					<td><s:property value="nomeBenificiarioTitular"/></td>

				</tr>
			</s:iterator>
			<s:if test="listaAcompanhamento !=  null && listaAcompanhamento.size > 0">
				<tr>
					<td colspan="8"></td>
				</tr>
				<tr>
					<td colspan="2">Valor Total das Propostas:</td>
					<td colspan="5" id="valorTotalProposta">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalProposta}" type="currency" currencySymbol="R$"/>						
				
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Sucursal:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorSucursal}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Corretor:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorCorretor}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr> 
				<tr>
					<td colspan="2">Valor Total de Propostas por Ag�ncia de D�bito:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorAgenciaDebito}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Ag�ncia Produtora:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorAgenciaProdutora}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Assistente BS:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorAssistenteProducao}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Gerente Produto BVP:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorGerenteProdutoBVP}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>	
				</s:if>
		</table>
	
	<br/><br/>
</s:form>
