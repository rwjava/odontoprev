<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
		<title>Emiss�o Expressa Dental Individual</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<!-- Bradesco styles -->
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/css.css'/>" />
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/tabs.css'/>" />
		
		<!-- External plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-ui.custom.min.js'/>"></script>
		<script type="text/javascript" src="<s:url value="/includes/js/vendor/jquery.ui.datepicker-pt-BR.js" />" ></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.inputmask.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.blockUI.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.scrollto.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-maskmoney.js'/>"></script>
		
		<!-- WDEV plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-datatypes.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-ajax.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-block.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-required.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-inputcontrol.js'/>"></script>
		
		<s:url var="baseURL" value="/"/>
		<script>window.baseURL = "${baseURL}";</script>
		
	

</head>
<script type="text/javascript" src="<s:url value='/includes/js/proposta/visualizar-proposta.js'/>"></script>
<div id='content'>
			<s:hidden id="hasActionErrors" value="%{hasActionErrors()}" />
			<table width="974px;">
				<tr>
					<td>
						<br/>
							<span class="subtitulo">EMISS�O EXPRESSA INDIVIDUAL</span>
						<br/>
						<font style="color:#0071BB;font-size:14px;font-weight:bold;"><span id="subtitulo">Cadastro de Proposta</span></font>
						<div id="tabs" class="tabpanel"></div>
						<div id="popCalendario" class="pop_calendario_box"></div>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_verm" id="msgErros" style="display:none">
							<tbody>
								<tr>
									<td>
										<div id="serverErrors" class="messages">
											<s:iterator value="actionErrors">
												<li><s:property /></li>
											</s:iterator>
										</div>
										<div id="actionErrors" class="messages"></div>
									</td>
								</tr>
							</tbody>
						</table>
						<s:if test="hasActionMessages()">
							<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_sucesso" id="msgSucesso">
								<tbody>
									<tr>
										<td>
											<div id="serverActionsMessages" class="messages">
												<s:iterator value="actionMessages">
													<li><s:property /></li>
												</s:iterator>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</s:if>
						<br />
						
						<div class="tab-row menuList">
							<ul>
								<li id="nova_proposta" class="tab0">
									<a href="iniciarNovaPropostaIntranet.do"><span>NOVA PROPOSTA</span></a>
								</li>
								<li id="listar_proposta" class="tab1">
									<a href="listarPropostaIntranet.do"><span>LISTAR PROPOSTA</span></a>
								</li>
								<li id="relatorio_acompanhamento" class="tab2">
									<a href="relatorioAcompanhamentoIntranet.do"><span>RELATORIO DE ACOMPANHAMENTO</span></a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<s:if test="hasActionMessages()">
			<script>
				$("#msgSucesso").fadeOut(50000);
			</script>
		</s:if>
<s:form id="form" action="cancelarProposta">
	<s:hidden name="proposta.sequencial"/>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">1. PROPOSTA</th>
		</tr>
		<tr>
			<td class="td_label" width="150px;">C�digo</td>
			<td width="400px;">
				<s:property value="proposta.codigo"/>
			</td>
			<td class="td_label" width="100px;">Canal</td>
			<td>
				<s:property value="proposta.canal.nome"/>
			</td>
		</tr>
		<tr>
			<td class="td_label" width="150px;">Data Emiss�o</td>
			<td width="400px;">
				<s:property value="proposta.dataEmissao"/>
			</td>
			<td class="td_label" width="100px;">Status</td>
			<td>
				<s:property value="proposta.status"/>
			</td>
		</tr>
	</table>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">2. CORRETOR</th>
		</tr>
		<tr>
			<td class="td_label" width="150px;">Nome</td>
			<td width="400px;">
				<s:property value="proposta.corretor.nome"/>
			</td>
			<td class="td_label" width="200px;">CPF/CNPJ</td>
			<td>
				<s:property value="proposta.corretor.cpfCnpj"/>
			</td>
		</tr>
		<tr>
			<td class="td_label" width="150px;">CPD/Sucursal</td>
			<td width="400px;">
				<s:property value="proposta.corretor.cpd"/>/<s:property value="proposta.corretor.sucursal"/>
			</td>
			<td class="td_label" width="200px;">Assistente de Produ��o</td>
			<td><s:property value="proposta.corretor.assistenteProducao"/></td>
		</tr>
	</table>
	<table class="tabela_interna">
		<tr>
			<th colspan="7">2. PLANO</th>
		</tr>
		<tr>
			<th width="13%" rowspan="2">Plano</th>
			<th width="10%" colspan="2">Valor Titular</th>
			<th width="11%" colspan="2">Valor Dependente</th>
			<th width="28%" colspan="2">Per&iacute;odo de Car&ecirc;ncia</th>
		</tr>
		<tr>
			<th>Mensal</th>
			<th>Anual</th>
			<th>Mensal</th>
			<th>Anual</th>
			<th>Mensal</th>
			<th>Anual</th>
		</tr>
		<tr>
	        <td>
	        	<s:property value='proposta.plano.nome'/>
	        </td>
	        <td> 
	        	<fmt:formatNumber value="${proposta.plano.valorPlanoVO.valorMensalTitular}" type="currency" currencySymbol="R$"/>
	        </td>
	        <td>
	        	<fmt:formatNumber value="${proposta.plano.valorPlanoVO.valorAnualTitular}" type="currency" currencySymbol="R$"/>
	        </td>
	        <td>
	        	<fmt:formatNumber value="${proposta.plano.valorPlanoVO.valorMensalDependente}" type="currency" currencySymbol="R$"/>
	        </td>
	        <td>
	        	<fmt:formatNumber value="${proposta.plano.valorPlanoVO.valorAnualDependente}" type="currency" currencySymbol="R$"/>
	        </td>
	        <td><s:property value="proposta.plano.descricaoCarenciaPeriodoMensal"/></td>
	     	<td><s:property value="proposta.plano.descricaoCarenciaPeriodoAnual"/></td>
	     </tr>	
	</table>
	<table class="tabela_interna">
		<tr>
			<th colspan="2">3. BENEFICI�RIOS</th>
		</tr>
		<tr>
	        <td class="td_label" width="292px;">O benefici�rio titular � menor de idade?</td>
	        <td>
	        	<s:if test="proposta.beneficiarios.titularMenorIdade == false">
	        		N�o
	        	</s:if>
	        	<s:else>
	        		Sim
	        	</s:else>
	        </td>
	    </tr>
	</table>
	<s:if test="proposta.beneficiarios.titularMenorIdade == true">
		<table class="tabela_interna" id="representante_legal">
			<tr>
				<th colspan="4">Representante Legal</th>
			</tr>
			<tr>
				<td class="td_label" width="127px;">Nome</td>
				<td><s:property value="proposta.beneficiarios.representanteLegal.nome"/></td>
				<td class="td_label" width="149px;">CPF</td>
				<td ><s:property value="proposta.beneficiarios.representanteLegal.cpf"/></td>
			</tr>
			<tr>
				<td class="td_label">Nascimento</td>
				<td ><s:property value="proposta.beneficiarios.representanteLegal.dataNascimento"/></td>
				<td class="td_label" >E-mail</td>
				<td ><s:property value="proposta.beneficiarios.representanteLegal.email"/></td>
			</tr>
			<tr>
				<td class="td_label" colspan="4" align="center"><b>ENDERE�O</b></td>
			</tr>
			<tr>
				<td class="td_label">CEP</td>
				<td colspan="3">
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.cep"/>
				</td>
			</tr>
			<tr>
				<td class="td_label">Endere�o</td>
				<td>
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.logradouro"/>
				</td>
				<td class="td_label">N�mero</td>
				<td><s:property value="proposta.beneficiarios.representanteLegal.endereco.numero"/></td>
			</tr>
			<tr>
				<td class="td_label">Complemento</td>
				<td><s:property value="proposta.beneficiarios.representanteLegal.endereco.complemento"/></td>
				<td class="td_label">Bairro</td>
				<td>
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.bairro"/>
				</td>
			</tr>
			<tr>
				<td class="td_label">Cidade</td>
				<td>
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.cidade"/>
				</td>
				<td class="td_label">Estado</td>
				<td>
					<s:property value="proposta.beneficiarios.representanteLegal.endereco.estado"/>
				</td>
			</tr>
			<tr>
				<td class="td_label" colspan="4" align="center"><b>CONTATO</b></td>
			</tr>
			<tr>
		        <td class="td_label" >Tipo de Telefone</td>
				<td>
					<s:property value="proposta.beneficiarios.representanteLegal.telefones[0].tipoTelefone"/>	
				</td>
				<td class="td_label" >N� Telefone / Ramal</td>
				<td>
					<s:property value="proposta.beneficiarios.representanteLegal.telefones[0].dddEnumero"/>
					<s:if test="proposta.beneficiarios.representanteLegal.telefones[0].tipoTelefone == 'COMERCIAL'">
					 / <s:property value="proposta.beneficiarios.representanteLegal.telefones[0].ramal"/>
					</s:if>
					
				</td>
		    </tr>
		</table>
	</s:if>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">Titular</th>
		</tr>
		<tr>
			<td class="td_label" width="125px;">Nome da M�e</td>
			<td colspan="3"><s:property value="proposta.beneficiarios.titular.nomeMae"/></td>
		</tr>
		<tr>
			<td class="td_label" width="125px;">Nome Completo</td>
			<td ><s:property value="proposta.beneficiarios.titular.nome"/></td>
			<td class="td_label" width="150px;">CPF</td>
			<td ><s:property value="proposta.beneficiarios.titular.cpf"/></td>
		</tr>
		<tr>
			<td class="td_label">Nascimento</td>
			<td ><s:property value="proposta.beneficiarios.titular.dataNascimento"/></td>
			<td class="td_label" >E-mail</td>
			<td ><s:property value="proposta.beneficiarios.titular.email"/></td>
		</tr>
		<tr>
	        <td class="td_label" >Sexo</td>
	        <td>
	        	<s:property value="proposta.beneficiarios.titular.sexo"/>
	        </td>
	        <td class="td_label" >Estado Civil</td>
			<td>
				<s:property value="proposta.beneficiarios.titular.estadoCivil"/>
			</td>
	    </tr>
	    <tr>
			<td class="td_label">CNS</td>
			<td ><s:property value="proposta.beneficiarios.titular.cns"/></td>
			<td class="td_label" >DNV</td>
			<td ><s:property value="proposta.beneficiarios.titular.dnv"/></td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>ENDERE�O</b></td>
		</tr>
		<tr>
			<td class="td_label">CEP</td>
			<td colspan="3">
				<s:property value="proposta.beneficiarios.titular.endereco.cep"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Logradouro</td>
			<td>
				<s:property value="proposta.beneficiarios.titular.endereco.logradouro"/>
			</td>
			<td class="td_label">N�mero</td>
			<td><s:property value="proposta.beneficiarios.titular.endereco.numero"/></td>
		</tr>
		<tr>
			<td class="td_label">Complemento</td>
			<td><s:property value="proposta.beneficiarios.titular.endereco.complemento"/></td>
			<td class="td_label">Bairro</td>
			<td>
				<s:property value="proposta.beneficiarios.titular.endereco.bairro"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Cidade</td>
			<td>
				<s:property value="proposta.beneficiarios.titular.endereco.cidade"/>
			</td>
			<td class="td_label">Estado</td>
			<td>
				<s:property value="proposta.beneficiarios.titular.endereco.estado"/>
			</td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>CONTATO</b></td>
		</tr>
		<s:iterator value="proposta.beneficiarios.titular.telefones" var="telefone" status="status">
			<tr>
		        <td class="td_label" >Tipo de Telefone</td>
				<td>
					<s:property value="tipoTelefone"/>
				</td>
				<td class="td_label" >N� Telefone</td>
				<td>
					<s:property value="dddEnumero"/>
				</td>
		    </tr>
	    </s:iterator>
	</table>
	<s:iterator value="proposta.beneficiarios.dependentes" var="dependente" status="status">
		<table class="tabela_interna">
			<tr>
				<td class="td_label" colspan="4" align="center"><b>DEPENDENTE</b></td>
			</tr>
			<tr>
				<td class="td_label" width="118px;">Nome da M�e</td>
				<td colspan="3"><s:property value="nomeMae"/></td>
			</tr>
			<tr>
				<td class="td_label" width="118px;">Nome Completo</td>
				<td width="420px;"><s:property value="nome"/></td>
				<td class="td_label" width="125px;">CPF</td>
				<td><s:property value="cpf"/></td>
			</tr>
			<tr>
				<td class="td_label">Nascimento</td>
				<td ><s:property value="dataNascimento"/></td>
				<td class="td_label" >Parentesco</td>
				<td >
					<s:property value="grauParentesco"/>
				</td>
			</tr>
			<tr>
		        <td class="td_label" >Sexo</td>
		        <td>
		        	<s:property value="sexo"/>
		        </td>
		        <td class="td_label" >Estado Civil</td>
				<td>
					<s:property value="estadoCivil"/>
				</td>
		    </tr>
		    <tr>
				<td class="td_label">CNS</td>
				<td ><s:property value="cns"/></td>
				<td class="td_label" >DNV</td>
				<td ><s:property value="dnv"/></td>
			</tr>
		</table>
	</s:iterator>
	<table class="tabela_interna">
		<tr>
			<th colspan="4">4. PAGAMENTO</th>
		</tr>
		<tr>
			<td class="td_label" width="170px;">N�mero de Vidas</td>
			<td colspan="3">
				<s:if test="proposta.beneficiarios.dependentes == null || proposta.beneficiarios.dependentes.size == 0">
					<span id="numero_vidas">1</span>
				</s:if>
				<s:else>
					<span id="numero_vidas"><s:property value="%{proposta.beneficiarios.dependentes.size + 1}"/></span>
				</s:else>
			</td>
		</tr>
		<tr>
			<td class="td_label" width="170px;">Valor Total Mensal</td>
			<td width="368px;"><fmt:formatNumber value="${valorTotalMensal}" type="currency" currencySymbol="R$"/></td>
			<td class="td_label" width="140px;">Valor Total Anual</td>
			<td><fmt:formatNumber value="${valorTotalAnual}" type="currency" currencySymbol="R$"/></td>
		</tr>
		<tr>
			<td class="td_label">Tipo de Cobran�a</td>
			<td colspan="3">
				<s:property value="proposta.pagamento.codigoTipoCobranca"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Forma de Pagamento</td>
			<td colspan="3">
				<s:property value="proposta.pagamento.formaPagamento"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Data de Cobran�a</td>
			<td colspan="3">
				<s:property value="proposta.pagamento.dataCobranca"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Banco</td>
			<td colspan="3">
				<s:property value="proposta.pagamento.contaCorrente.banco"/>
			</td>
		</tr>
		<tr>
			<td class="td_label" colspan="4" align="center"><b>PROPONENTE</b></td>
		</tr>
		<tr>
			<td class="td_label">CPF</td>
			<td colspan="3">
				<s:property value="proposta.pagamento.contaCorrente.cpf"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Ag�ncia / Conta</td>
			<td colspan="3">
				<s:property value="proposta.pagamento.contaCorrente.numeroAgencia"/> / <s:property value="proposta.pagamento.contaCorrente.numeroConta"/> - <s:property value="proposta.pagamento.contaCorrente.digitoVerificadorConta"/>
			</td>
		</tr>
		<tr>
			<td class="td_label">Nome</td>
			<td ><s:property value="proposta.pagamento.contaCorrente.nome"/></td>
			<td class="td_label">Nascimento</td>
			<td ><s:property value="proposta.pagamento.contaCorrente.dataNascimento"/></td>
		</tr>
	</table>
	
	<br/><br/>
	
	<s:if test="indicativoDeCancelamentoDaProposta == false">
		<table style="width: 974px;">
			<tr>
				<!-- td width="45%" align="right"><input type="button" value="Limpar" id="btn_limpar"/></td>
				<td width="10%"></td-->
				<td align="center"><input type="button" value="Voltar" id="btn_voltar"/></td>
			</tr>
		</table>
	</s:if>
	<s:else>
		<table style="width: 974px;">
			<tr>
				<td width="45%" align="right"><input type="button" value="Voltar" id="btn_voltar"/></td>
				<td width="10%"></td>
				<td width="45%" align="left"><input type="submit" value="Cancelar"/></td>
			</tr>
		</table>
	</s:else>	
	<br/><br/>
</s:form>
