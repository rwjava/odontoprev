<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<script type="text/javascript" src="<s:url value='/includes/js/proposta/relatorio-acompanhamento.js'/>"></script>
<s:form id="form">
	<table class="tabela_interna">
		<tr>
			<th colspan="4">RELAT�RIO DE ACOMPANHAMENTO</th> 
		</tr>
		<tr>
			<td class="td_label" colspan="1">Per�odo Inicial</td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.filtroPeriodoVO.dataInicio" data-mask="data" size="16" maxlength="11" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">Per�odo Final</td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.filtroPeriodoVO.dataFim" data-mask="data" size="16" maxlength="11"  cssClass="lista-proposta"/>
			</td>
		</tr>
		
		<tr>
			<td class="td_label" colspan="1">C�digo Proposta<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoProposta" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta" />
			</td>
			<td class="td_label" colspan="1">CPF Benefici�rio Titular<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.cpfBeneficiarioTitular" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta" />
			</td>
		</tr>
		
		<tr>
			<td class="td_label" colspan="1">Sucursal<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.sucursal" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta" />
			</td>
			<td class="td_label" colspan="1">CPD do Corretor<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.cpdCorretor" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
		</tr>
		
		<tr>
			<td class="td_label" colspan="1">CPF/CNPJ Corpo<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.cpfCnpjCorretor" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">C�digo Ag�ncia D�bito<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoAgenciaDebito" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
		</tr>
	
		<tr>
			<td class="td_label" colspan="1">C�digo AG Produtora<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoAgenciaProdutora" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">C�digo Assistente BS<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoAssistente" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
		</tr>
		
		<tr>
			<td class="td_label" colspan="1">C�digo Gerente Produto BVP<span class="obrigatorio">*</span></td>
			<td>
				<s:textfield name="filtroRelatorioAcompanhamentoVO.codigoGerenteProdutoBVP" size="16" maxlength="15" style="text-transform: uppercase;" cssClass="lista-proposta"/>
			</td>
			<td class="td_label" colspan="1">Status Atual Proposta<span class="obrigatorio">*</span></td>
			<td>					
						<select name="filtroRelatorioAcompanhamentoVO.codigoStatusProposta" class="lista-proposta">
								<option selected="selected" value="">Escolha</option>
								<option value="0">PENDENTE</option>
								<option value="1">RASCUNHO</option>
								<option value="2">EM PROCESSAMENTO</option>
								<option value="3">CRITICADA</option>
								<option value="4">CANCELADA</option>
								<option value="5">IMPLANTADA</option>
								<option value="6">PR�-CANCELADA</option>
								<option value="7">TODAS</option>
						</select>
			</td>
			
			
		</tr>
		
		
	</table>
	
	<br/><br/><br/>
	
	<table style="width: 974px;">
		<tr>
			<!-- td width="45%" align="right"><input type="button" value="Limpar" id="btn_limpar"/></td>
			<td width="10%"></td-->
			<td align="center"><input type="button" value="Limpar" id="btn_limpar"/></td>
			<td align="center"><input id="buscarAcompanhamento" type="button" value="Buscar"/></td>
			<td align="center"><input type="button" value="Gerar Relat�rio" id="gerarCSVAcompanhamento"/></td>
			<td align="center"><input id="gerarPDFAcompanhamento" type="button" value="Gerar PDF"/></td>
		</tr>
	</table>
	
	<br/><br/><br/>
	
		<table class="tabela_interna">
			<tr>
				<th colspan="7">PROPOSTAS</th>
			</tr>
			<tr>
				<th width="150px;">C�digo</th>
				<th width="150px;">CPF do Proponente</th>
				<th width="450px;">Nome do Proponente</th>
			    <th width="150px;">CPF do Respons�vel</th>
				<th width="450px;">Nome do Respons�vel</th>
				<th width="150px;">CPF do Benefici�rio Titular</th>
				<th width="450px;">Nome do Benefici�rio Titular</th>
			</tr>
			<s:iterator value="listaAcompanhamento" var="proposta" status="status">
				<tr>
					<td>
						<s:if test="canal.codigo == 8">
<%-- 							<a href="visualizarProposta.do?proposta.sequencial=<s:property value='sequencial'/>" > --%>
<%-- 								<s:property value="codigo"/> --%>
<!-- 							</a> -->
							<a href="#" >
								<s:property value="codProposta"/>
							</a>
						</s:if>
						<s:else>
							<s:property value="codProposta"/>
						</s:else>
					</td>
					<td><s:property value="cpfTitularConta"/></td>
					<td><s:property value="nomeTitularConta"/></td>					
					<td><s:property value="cpfResponsavel"/></td>
					<td><s:property value="nomeResponsavel"/></td>
					<td><s:property value="cpfBenificiarioTitular"/></td>
					<td><s:property value="nomeBenificiarioTitular"/></td>

				</tr>
			</s:iterator>
			<s:if test="listaAcompanhamento !=  null && listaAcompanhamento.size > 0">
				<tr>
					<td colspan="8"></td>
				</tr>
				<tr>
					<td colspan="2">Valor Total das Propostas:</td>
					<td colspan="5" id="valorTotalProposta">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalProposta}" type="currency" currencySymbol="R$"/>						
				
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Sucursal:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorSucursal}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Corretor:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorCorretor}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr> 
				<tr>
					<td colspan="2">Valor Total de Propostas por Ag�ncia de D�bito:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorAgenciaDebito}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Ag�ncia Produtora:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorAgenciaProdutora}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Assistente BS:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorAssistenteProducao}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>
				<tr>
					<td colspan="2">Valor Total de Propostas por Gerente Produto BVP:</td>
					<td colspan="5">
						<fmt:formatNumber value="${relatorioAcompanhamentoPropostaVO.valorRelatorioAcompanhamentoPropostaVO.valorTotalPropostaPorGerenteProdutoBVP}" type="currency" currencySymbol="R$"/>											
					</td>
				</tr>	
				</s:if>
		</table>
	
	<br/><br/>
</s:form>
