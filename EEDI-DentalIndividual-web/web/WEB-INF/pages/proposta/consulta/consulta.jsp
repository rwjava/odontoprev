<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<!-- Bradesco styles -->
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/css.css'/>" />
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/tabs.css'/>" />
		<!-- External plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-ui.custom.min.js'/>"></script>
		<script type="text/javascript" src="<s:url value="/includes/js/vendor/jquery.ui.datepicker-pt-BR.js" />" ></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.inputmask.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.blockUI.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.scrollto.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-maskmoney.js'/>"></script>
		
		<!-- WDEV plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-datatypes.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-ajax.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-block.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-required.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-inputcontrol.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.formatCurrency-1.4.0.min.js'/>"></script>


<link rel="stylesheet" type="text/css"
	href="<s:url value='/includes/css/bootstrap.min.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<s:url value='/includes/css/bootstrap-dialog.css'/>" />
<script type="text/javascript"
	src="<s:url value='/includes/js/bootstrap.min.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/includes/js/bootstrap-dialog.js'/>"></script>

<style>


	.button-consulta{
	-webkit-box-shadow: inset 0px 1px 0px 0px #ffffff;
	box-shadow: inset 0px 1px 0px 0px #ffffff;
	background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background: -moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color: #ededed;
	-webkit-border-top-left-radius: 6px;
	-moz-border-radius-topleft: 6px;
	border-top-left-radius: 6px;
	-webkit-border-top-right-radius: 6px;
	-moz-border-radius-topright: 6px;
	border-top-right-radius: 6px;
	-webkit-border-bottom-right-radius: 6px;
	-moz-border-radius-bottomright: 6px;
	border-bottom-right-radius: 6px;
	-webkit-border-bottom-left-radius: 6px;
	-moz-border-radius-bottomleft: 6px;
	border-bottom-left-radius: 6px;
	text-indent: 0;
	border: 1px solid #dcdcdc;
	display: inline-block;
	color: #777777;
	font-family: arial;
	font-size: 15px;
	font-weight: bold;
	font-style: normal;
	height: 50px;
	line-height: 50px;
	width: 100px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	text-transform: uppercase;
	margin-left: 0;
	cursor: pointer;
	min-width: 156px;
	overflow: visible;
	padding-left: 3px;
	padding-right: 3px;
}
</style>

<s:form id="form" action="consultar">

		 <div>
	  		<s:textarea rows="10" cols="100" name="textoConsulta" id="texto" cssStyle="resize: both;"></s:textarea>
	    </div>
	     <div>
	  		<s:submit value="Enviar"  cssClass="button-consulta"/>
	    </div>
	    
	      <table style="width: 820px;" id="tabela_interna" class="tituloInterno">
		    	<thead>
<%-- 				    	<s:iterator value="colunas" var="coluna-tabela"> --%>
<%-- 				    		<td><c:out value="${coluna-tabela}"/></td> --%>
<%-- 				    	</s:iterator> --%>
<%-- 				    	<c:forEach varStatus="colunas" var="coluna-tabela"> --%>
<%-- 				    	</c:forEach> --%>
<%-- 				    	<c:forEach var="colunas" items="${colunas}" > --%>
<%-- 		 					 <td>${colunas}</td> --%>
<%-- 				     	</c:forEach> --%>
				    	
		    	</thead>
		    	<tbody>

		    	<s:iterator value="registros" var="linha">
			    	
			    	<tr>
			    
			    		<td><c:out value="${linha}"/></td>
			    	
			    	</tr>
			    	<tr>
			    		<s:iterator value="linha" var="coluna">
			    			<td><c:out value="${coluna}"/></td>
			    		</s:iterator>
			    	</tr>
			    </s:iterator>
			    </tbody>
		   </table>
	    
</s:form>