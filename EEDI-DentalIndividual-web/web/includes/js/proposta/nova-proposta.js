$(document).ready(function() {
	
	 FORMA_PAGAMENTO_CARTAO_CREDITO = 5;
	 FORMA_PAGAMENTO_BOLETO_E_CARTAO_CREDITO = 6;
	 FORMA_PAGAMENTO_DEBITO_AUTOMATICO_E_CARTAO_CREDITO = 7;
	
	inicializarTela();
	
function bloqueiaEnter(){
	
}
$('#finalizar').on('click', function (){
		$('#finalizar').prop('disabled',true);
		var accessToken = $('.accessToken').val();
		var paymentToken = $('.paymentToken').val();
		$('#finalizar').hide();
		if(accessToken != "" && paymentToken == ""){
			$('.bp-sop-cardnumber').val($('.bp-sop-cardnumber').val().replace(/^\s+|\s+$/g,''));
			sendCardData();
		}
		BootstrapDialog.show({
	        title: 'CONFIRMA&Ccedil;&Atilde;O DE FINALIZA&Ccedil;&Atilde;O DA PROPOSTA',
	        message: novo_finalizar(),
	        closable: false,
	        buttons: [{
	        	id: 'botao-confirmar-proposta',
	            label: 'Confirmar',
	            cssClass: 'btn-warning',
	            action: function(dialogRef){
	            	$('#botao-confirmar-proposta').prop('disabled',false);
	            	console.log('action finalizarProposta.do');
	            	
	            	var formaDePagamento = $('#forma_pagamento').val();
	            	
	            	if(formaDePagamento == FORMA_PAGAMENTO_CARTAO_CREDITO 
							|| formaDePagamento  == FORMA_PAGAMENTO_BOLETO_E_CARTAO_CREDITO 
							|| formaDePagamento == FORMA_PAGAMENTO_DEBITO_AUTOMATICO_E_CARTAO_CREDITO)
	            	{
	            		salvarPropostaCartao();
	            	}else{
	            		finalizarProposta();
	            	}
	        		
	            	dialogRef.close();
	            	bloquearTela();
	            	$('#finalizar').show();
	                inicializarDadosPagamento();
	        		setTimeout(function(){$('input[name="finalizar-proposta"]').click();}, 2000);
	        		$('#finalizar').prop('disabled',false);
	        		$('#botao-confirmar-proposta').prop('disabled',true);
	            }
	        }, {
	            label: 'Cancelar',
	            action: function(dialogRef){
	            	$('#botao-confirmar-proposta').prop('disabled',false);
	                dialogRef.close();
	                desbloquearTela();
	            	$('#finalizar').show();
	                inicializarDadosPagamento();
	                $('#finalizar').prop('disabled',false);
	                $('#botao-confirmar-proposta').prop('disabled',true);
	            }
	        }]
	    });
});


	function finalizarProposta(){	
	
		var codigo = $('label[id="sequencial_proposta"]').text();
		if(codigo != null && codigo != ""){
			$('#form').attr('action', window.baseURL + 'proposta/finalizarPropostaAjax.do?proposta.codigo='+codigo);	        		
		}else{
			$('#form').attr('action', window.baseURL + 'proposta/finalizarPropostaAjax.do');	        			
		}
		var actionFinalizarPropostaCartao = $('#form').prop('action');
		
			$.ajax({			
				type: "POST",
				url: actionFinalizarPropostaCartao,
				dataType: 'json',
				data: $('#form').serialize(),
				 beforeSend: function(){
				 	bloquearTela();
				 },
				 complete: function(){
				 	desbloquearTela();
				 },
				success: function(data){
					console.log(data);
					if(data.actionErrors.length  > 0){
						montarMengagensErro(data.actionErrors);
					}else{    	 			
							$('input[name="proposta.sequencial"]').val(data.proposta.sequencial);
							$('label[id="sequencial_proposta"]').text(data.proposta.codigo);
							$('input[name="proposta.status"]').val(data.proposta.status);
//							$('#sequencial_proposta').text(data.proposta.codigo);
							$('label[id="sequencial_proposta"]').text(data.proposta.codigo);
							$('#codigo_proposta').val(data.proposta.sequencial);
							$('.status_label').text(data.proposta.status);	
							montarMensagemSucesso(data.actionMessages);
							tratarExibicaoCamposAposFinalizarProposta();
							
					}
			  },
			  error: function( event, xhr, settings ){
				  console.log('event ' + event);
				  console.log('xhr ' + xhr);
				  console.log('settings ' + settings);				  
			  }
				
			});
		}
	
	function cancelarPropostaMip(){
		var codigo = $('label[id="sequencial"]').text();
		if(codigo != null && codigo != ""){
			$('#form').att('action', window.baseURL + 'proposta/cancelarPropostaAjax.do?proposta.codigo='+codigo);
		}else{
			$('#form').att('action', window.baseURL + 'proposta/cancelarPropostaAjax.do');
		}
		
		var actionCancelarCobrancaMip = $('#form').prop('action');
		
		$.ajax({
			type: "POST",
			url: actionCancelarCobrancaMip,
			dataType: 'json',
			data: $('#form').serialize(),
			beforeSend: function(){
				bloqueartela();
			},
			complete: function(){
				desbloquearTela();
			},
			success: function(data){
				console.log(data);
				if(data.actionErros.length > 0){
					montarMengagensErro(data.actionErrors);
				}else{
					$('input[name="proposta.sequencial"]').val(data.proposta.sequencial);
					$('label[id="sequencial_proposta"]').text(data.proposta.codigo);
					$('input[name="proposta.status"]').val(data.proposta.status);
//					$('#sequencial_proposta').text(data.proposta.codigo);
					$('label[id="sequencial_proposta"]').text(data.proposta.codigo);
					$('#codigo_proposta').val(data.proposta.sequencial);
					$('.status_label').text(data.proposta.status);	
					montarMensagemSucesso(data.actionMessages);
					tratarExibicaoCamposAposFinalizarProposta();
				}
				if(data.proposta.sequencial == undefined 
						 && data.proposta.sequencial == null && data.proposta.sequencial == 0){
						$('input[name="proposta.sequencial"]').val(null);
						$('input[name="proposta.status"]').val(null);
						$('label[id="sequencial_proposta"]').text(null);
					}
			},
			  error: function( event, xhr, settings ){
				  console.log('event ' + event);
				  console.log('xhr ' + xhr);
				  console.log('settings ' + settings);
				  if(data.actionErrors.length  > 0){
					  montarMengagensErro(data.actionErrors);
				  }
				  
			  }
		});
	}


	function salvarPropostaCartao(){	
		var codigo = $('label[id="sequencial_proposta"]').text();
    	
		if(codigo != null && codigo != ""){
			$('#form').attr('action', window.baseURL + 'proposta/salvarRascunhoPropostaAjax.do?proposta.codigo='+codigo);
		}else{
			$('#form').attr('action', window.baseURL + 'proposta/salvarRascunhoPropostaAjax.do');
		}
    	
		var actionSalvarRascunhoPropostaCartao = $('#form').prop('action');
    	
    		$.ajax({			
    			type: "POST",
    			url: actionSalvarRascunhoPropostaCartao,
    			dataType: 'json',
    			data: $('#form').serialize(),
    			 beforeSend: function(){
    			 	bloquearTela();
    			 },
    			 complete: function(){
    			 	desbloquearTela();
    			 },
    			success: function(data){
    				console.log(data);
    				if(data.actionErrors.length  > 0){
    					montarMengagensErro(data.actionErrors);
    				}else{  
    					$('input[name="proposta.sequencial"]').val(data.proposta.sequencial);
    					$('label[id="sequencial_proposta"]').text(data.proposta.codigo);
    					$('input[name="proposta.status"]').val(data.proposta.status);
//    					$('#sequencial_proposta').text(data.proposta.codigo);
    					$('label[id="sequencial_proposta"]').text(data.proposta.codigo);
    					$('#codigo_proposta').val(data.proposta.sequencial);
    					$('.status_label').text(data.proposta.status);	
    					tratarCartaoCredito(data);
    				}

    		  },
    		  error: function( event, xhr, settings ){
    			  console.log('event ' + event);
    			  console.log('xhr ' + xhr);
    			  console.log('settings ' + settings);
    		  }
    			
    		});
		}


	function tratarCartaoCredito(data){
		console.log('tratarCartaoCredito');
//		var nomeCartao = $('');
//		var numeroCartao = $('');
//		var validadeCartao = $('');
//		var bandeiraCartao = $('');
		$('#form').attr('action', window.baseURL + 'proposta/validarDadosCartaoCreditoAjax.do');	        			
		var actionValidarDadosCartaoCreditoAjax = $('#form').prop('action');
		
		$.ajax({			
			type: "POST",
			url: actionValidarDadosCartaoCreditoAjax,
			dataType: 'json',
			data: $('#form').serialize(),
			 beforeSend: function(){
			 	bloquearTela();
			 },
			 complete: function(){
			 	desbloquearTela();
			 },
			success: function(data){
				console.log(data);
				data.actionMessages;
				if(data.actionErrors.length  > 0 ){
					montarMengagensErro(data.actionErrors);
					
				}else{				
					$('input[name="proposta.sequencial"]').val(data.proposta.sequencial);
					$('label[id="sequencial_proposta"]').text(data.proposta.codigo);
					$('input[name="proposta.status"]').val(data.proposta.status);
//					$('#sequencial_proposta').text(data.proposta.codigo);
					$('label[id="sequencial_proposta"]').text(data.proposta.codigo);
					$('#codigo_proposta').val(data.proposta.sequencial);
					$('.status_label').text(data.proposta.status);	
					finalizarProposta();
				}
				if(data.proposta.sequencial == undefined 
					 && data.proposta.sequencial == null && data.proposta.sequencial == 0){
					$('input[name="proposta.sequencial"]').val(null);
					$('input[name="proposta.status"]').val(null);
					$('label[id="sequencial_proposta"]').text(null);
				}				
			
			},
		  error: function( event, xhr, settings ){
			  console.log('event ' + event);
			  if(data.actionErrors.length  > 0){
				  montarMengagensErro(data.actionErrors);
			  }
		  }
		});
		
		
	}
	
	
	function tratarExibicaoCamposAposFinalizarProposta(){
//			$('#sequencial_proposta').show();
			$('label[id="sequencial_proposta"]').show();
			$('.imprimir_proposta_vazia').hide();
			$('#btn_limpar').show();
			$('#cancelar-proposta').show();
			$('#imprimir_proposta_finalizada').show();
			$('#cancelar-proposta_finalizada').show();
			$('#salvar-rascunho').hide();
			$('#finalizar').hide();
			$('.bloco-corretor').prop('disabled', 'disabled');
		}

		function novo_finalizar(){
		  	
	  		var plano = "<b>Plano</b>: ";
	  		var planoSelecionado = false;
	  		if(undefined == $("input[name='proposta.plano.codigo']:checked").val()){
	  			plano = plano + "N&atilde;o Selecionado<br/>";
	  		}else{
	  			var tr = $("input[name='proposta.plano.codigo']:checked").closest('tr');
	  	    	plano = plano + tr.find('label').text() + "<br/>";
	  	    	planoSelecionado = true;
	  		}

			var tipoCobranca = "<b>Tipo de Cobran&ccedil;a</b>: ";
			var tipoCobrancaSelecionado = false;
			if(undefined == $("input[name='proposta.pagamento.codigoTipoCobranca']:checked").val()){
				tipoCobranca = tipoCobranca + "N&atilde;o Selecionado<br/>";
			}
			else {
				tipoCobranca = tipoCobranca + $("label[for='" + $("input[name='proposta.pagamento.codigoTipoCobranca']:checked").attr('id') + "']").text() + "<br/>";
				tipoCobrancaSelecionado = true;
			}
			
			var valor = "<b>Valor</b>: ";
			if(planoSelecionado && tipoCobrancaSelecionado){
				var tipo_cobranca = $("label[for='" + $("input[name='proposta.pagamento.codigoTipoCobranca']:checked").attr('id') + "']").text();
				if("Mensal" == tipo_cobranca){
					
					console.log('valor total mensal label: ' + $('#mensal-label').text());
					valor = valor + $('#mensal-label').text() + "<br/><br/><br/>";
				}else if("Anual" == tipo_cobranca){
					console.log('valor total anual label: ' + $('#anual-label').text());
					valor = valor + $('#anual-label').text() + "<br/><br/><br/>";
				}	
//				$('#mensal-label').toNumber();
//				$('#anual-label').toNumber();
				$('.mascara-moeda').formatCurrency({'region': 'pt-BR', 'symbol ' : 'R$'});
			}else{
				valor = valor + "R$ 0,00<br/><br/><br/>";
			}
			
			var resumo = "Resumo da Proposta:<br/><br/>";
			var pergunta = "<i>Deseja confirmar a contrata&ccedil;&atilde;o da proposta com as informa&ccedil;&otilde;es acima?</i>";
			var mensagem_completa = resumo + plano + tipoCobranca + valor + pergunta;
			
			return mensagem_completa;
		} 
		
		
		function sendCardData(){
			var options = {
				accessToken: $('.accessToken').val(),
				onSuccess: function(e){
					var paymentToken = e.PaymentToken;
					$('.paymentToken').val(paymentToken);
					$('input[name="proposta.pagamento.cartaoCredito.paymentToken"]').val(paymentToken);
				},
				onError: function(e){
					var errorCode = e.Code;
					var errorMessage = e.Text;
					//alert(errorCode + " / " + errorMessage);
				},
				onInvalid: function(e){
					for(var i = 0; i < e.length; i++){
						var field = e[i].Field;
						var message = e[i].Message;
						//alert(field + " / " + message);
					}
				},//TODO sempre trocar para production ou sandbox
				environment: $('#ambiente').val(),
				language: "PT"
				,
				cvvrequired: false
			};
			bpSop_silentOrderPost(options);
		}		
		 function SomenteNumero(e){
				var tecla=(window.event)?event.keyCode:e.which;   
					if((tecla>47 && tecla<58)) return true;
						else {
							if (tecla==8 || tecla==0) return true;
						else  return false;
							}
					}
					
			function SomenteLetras(e){
				var tecla=new Number();
				if(window.event) {
					tecla = e.keyCode;
				}
				else if(e.which) {
					tecla = e.which;
				}
				else {
					return true;
				}
				if((tecla >= "48") && (tecla <= "57")){
					return false;
				}
				
			}
//	$( document ).ajaxComplete(function( event, xhr, settings ) {
////		console.log('event: '+event);
////		console.log('xhr: '+xhr);
////		console.log('settings: '+settings);
//		
//		var planoSelecionado = $('[name="proposta.plano.codigo"]').val();
//		marcarPlanoJaSelecionado(planoSelecionado);
////		$("#tabela_planos :radio").each(function() {
////		    console.log('plano:: ' + $(this).val());
////		    
////		    if($(this).val() == $('#plano_selecionado').val()){
////		    	$(this).prop('checked', true);
////		    }
////
////		    
////		});
//		
//	});
	
	
	
	$('#salvar-rascunho').on('click', function(){
//		$('#sequencial_proposta').hide();
		limparMensagensErro();
//		var codigo = $('#sequencial_proposta').text();
		var codigo = $('label[id="sequencial_proposta"]').text();
		if(codigo != null && codigo != ""){
			$('#form').attr('action', window.baseURL + 'proposta/salvarRascunhoProposta.do?proposta.codigo='+codigo);
		}else{
			$('#form').attr('action', window.baseURL + 'proposta/salvarRascunhoProposta.do');
		}
		$('#form').submit();
	});
	
	$('#btn_limpar').on('click', function(){
		$('#form').attr('action', window.baseURL + 'proposta/limparTelaProposta.do');
		$('#form').submit();
	});
	
	$('#intra_btn').on('click', function(){
		$('#form').attr('action', window.baseURL + 'proposta/intranet/limparTelaPropostaIntranet.do');
		$('#form').submit();
	});
	
	
	$('#imprimir-proposta-vazia').on('click', function (){
		console.log('imprimir-proposta-vazia');
		var sucursalSelecionada = $('#sucursalList').val();
		var planoSelecionado = $('.radio_plano:checked').val();
		var agenciaProdutora = $('#agenciaProdutora').val();
		var assistenciaProdutora = $('#assistenteProducao').val();
		var corretorMaster = $('#cpdCorretorMasterLogado').val();
		
		$.ajax({
			
			type: "POST",
			url: window.baseURL + 'proposta/gerarPropostaVazia.do',
			data:$('#form').serializeLatin(),
			dataType: 'json'
			,
			 beforeSend: function(){
			 	bloquearTela();
			 },
			 complete: function(){
			 	desbloquearTela();
			 },
			success: function(data){
				limparMensagensErro();
				console.log(data);					
				if(data.actionErrors.length  > 0){
					montarMengagensErro(data.actionErrors);
				}else{
					var urlBase = window.baseURL + 'proposta/imprimirPropostaVazia.do?proposta.sequencial='+data.proposta.sequencial;
					  $('#form').attr('action', urlBase);
					  $('#form').serialize();
//					  $('#sequencial_proposta').text(data.proposta.codigo);
					  $('label[id="sequencial_proposta"]').text(data.proposta.codigo);
					  $('#codigo_proposta').val(data.proposta.sequencial);
					  $('label[id="sequencial_proposta"]').show();
//					  data.listaSucursais.forEach(function (sucursal){
//					  
//						  $('#sucursalList').append($('<option>', { 
//						        value: sucursal.codigo,
//						        text : sucursal.descricao
//						    }));
//						  
//					  });
					  
					  $.each( data.listaSucursais, function( key, sucursal ) {
						  $('#sucursalList').append($('<option>', { 
						        value: sucursal.codigo,
						        text : sucursal.descricao
						    }));	
						});
					  
					  adicionarIframeParaDownload(urlBase);
					  $('#form :input').prop('disabled', true);
				}
			},
		  error: function(data){
			  console.log('error ' + data);
			  adicionarMensagemErro("Erro ao recuperar as informações do corretor.");
		  }
			
		});
		
		
//		  var urlBase = window.baseURL + 'proposta/imprimirPropostaVazia.do?'+$('#form').serialize();;
//		  $('#form').attr('action', urlBase);
//		  $('#form').serialize();
//		  window.location.href = urlBase;
		  
//		  window.open(urlBase);
//			$('#form').attr('action', urlBase);
//			$('#form').submit();
//		  adicionarIframeParaDownload(urlBase);
	});
	
	function adicionarIframeParaDownload(urlBase){
		
		var iframe = '<iframe width="0px" height="0px" id="ajax-download"></iframe>';
		  if($('#ajax-download').length == 0){
		  	$('#form').append(iframe);
		  }
		  $('#ajax-download').attr('src', urlBase);
		  $('#ajax-download').hide();
		  
	}
	
	
	
	$('.imprimir-proposta').on('click', function (){
		
		console.log('imprimir-proposta');
		var sucursalSelecionada = $('#sucursalList').val();
//		var sequencialProposta = $('#sequencial_proposta').text();
		var sequencialProposta = $('label[id="sequencial_proposta"]').text();
		
		  var urlBase = window.baseURL + 'proposta/imprimirProposta.do?proposta.sucursalSelecionada.cpdSucursal='+sucursalSelecionada
		  +'&proposta.codigo='+sequencialProposta;	  
		  $('#form').attr('action', urlBase);
		  $('#form').serialize();
//		  window.open(urlBase);
//			$('#form').attr('action', urlBase);
//			$('#form').submit();
		  adicionarIframeParaDownload(urlBase);
		  
	});
	$('#forma_pagamento').on('click', function(){
		if(!$("input[type='radio'][name='proposta.plano.codigo']").is(':checked')){
					
			$("#banco").prop( "disabled", true );
		}else{
			$("#banco").prop( "disabled", false );	
		}
		
	});
	$('#tabela_planos').on('change', function(){
		var val_plano = $('.radio_plano:checked').val();
		if(val_plano == 15 || val_plano == 16 || val_plano == 17 || val_plano == 19 || val_plano == 18 || val_plano == 20 || val_plano == 21 || val_plano == 22 ||  val_plano == 23 || val_plano == 24){
			$('input:radio[name="proposta.beneficiarios.titularMenorIdade"][value="true"]').prop('checked', true);
			$('#form_proposta_beneficiarios_titularMenorIdadefalse').attr('disabled', 'disabled');
			$('#representante_legal').show();
			$("#dependente_dente_leite").hide();
		}else{
			$('input:radio[name="proposta.beneficiarios.titularMenorIdade"][value="false"]').prop('checked', true);
			$('#form_proposta_beneficiarios_titularMenorIdadefalse').removeAttr('disabled');
			$('#representante_legal').hide();
			$("#dependente_dente_leite").show();
		}
		if((val_plano == 5 || val_plano == 6) || val_plano == 7 || val_plano == 8 || val_plano == 16 || val_plano == 17 
				|| val_plano == 21 || val_plano == 22 ){
			$('input:radio[name="proposta.pagamento.codigoTipoCobranca"][value="2"]').prop('checked', true);
			$('#form_proposta_pagamento_codigoTipoCobranca1').attr('disabled', 'disabled');
		}else{
			$('input:radio[name="proposta.pagamento.codigoTipoCobranca"][value="2"]').prop('checked', false);
			$('#form_proposta_pagamento_codigoTipoCobranca1').removeAttr('disabled', 'disabled');
		}
		
	});	
	
	
	$('.cancelar-proposta').on('click', function(){
		$('#form').serialize();
		$('#form').attr('action', window.baseURL + 'proposta/cancelarProposta.do');
		$('#form').submit();
	});
	
	$("[name='proposta.beneficiarios.titularMenorIdade']").on('change', function(){
		$('#representante_legal').hide();
		if($(this).val() == 'true'){
			$('#representante_legal').show();
		}
	});
	
//	$("[name='finalizar-proposta']").on('click', function(){
//		$('#form').attr('action', window.baseURL + '/proposta/finalizarProposta.do');
//		$('#form').submit();
//	});
	
	$("[name='listaSucursais']").on('change', function(){		 
		 var sucursalSelecionada = $('#sucursalList').val();
		 console.log('Alterou a sucursal ' + sucursalSelecionada);
		//$.submitAjax('proposta/obterCorretorCCRR.do?sucursal=' + suc);

	});
	
	$("[name='proposta.plano.codigo']").on('change', function(){
		$("#banco").prop( "disabled", false );
		$.submitAjax('proposta/alterarPlanoDaProposta.do');
	});
	
	//[name='proposta.beneficiarios.titular.endereco.cep']
	$("#buscarCep").on('click', function(){
		var cep = $("[name='proposta.beneficiarios.titular.endereco.cep'").val();
		var planoSelecionado = $('[name="proposta.plano.codigo"]').val();

		console.log('cep informado: ' + cep);

		if (cep.length == 8){
//			$("[name='proposta.beneficiarios.titular.endereco.logradouro']").val('');
			obterEnderecoCepAjax(cep, 'true');
			
//			$.executarAjax({
//				url: window.baseURL + '/proposta/obterEnderecoPorCep.do?cep=' + $(this).val() + '&indicadorCepDoTitular=' + true
//				, dataType: 'html'
//				, data: $('#form').serializeLatin()
//				, type: 'POST'
//				, success: function(html){
//					$("input[name='proposta.plano.codigo'][value='" + planoSelecionado + "']").attr('checked', 'checked');
//					$('#content').html(html);
//					$("input[name='proposta.plano.codigo'][value='" + planoSelecionado + "']").attr('checked', 'checked');
//
//				}
//				, complete: function(){
//					exibirCriticas();
//
//				}
//			});
			
		}else{
			$('.cep_titular').show();
		}
	});
	
	$("#buscarCepRepresentanteLegal").on('click', function(){
		var cep = $("[name='proposta.beneficiarios.representanteLegal.endereco.cep'").val();
		if (cep.length == 8){
			obterEnderecoCepAjax(cep, "false");
		}else{
			$('.cep_representante_legal').show();
		}
	});	
	
	$('#adicionar_dependente').click(function(){
		var planoSelecionado = $('[name="proposta.plano.codigo"]').val();
		var quantidadeDependentes = $('#numero_vidas').text();
//		$.submitAjax('proposta/adicionarDependente.do');
		
		$.executarAjax({
				url: window.baseURL + 'proposta/adicionarDependente.do'
				, dataType: 'html'
				, data: $('#form').serializeLatin()
				, type: 'POST'
				, success: function(html){
					$('#content').html(html);
					marcarPlanoJaSelecionado(planoSelecionado);
					console.log('valor mensal :' + $('#valorTotalMensal').val());
					console.log('valor anual :' + $('#valorTotalAnual').val());
				}
				, complete: function(){
					exibirCriticas();
					
				}
			});
		
		
//		$.ajax({			
//			type: "POST",
//			url: window.baseURL + '/proposta/adicionarDependente.do',
//			dataType: 'json',
//			data:{
//				'cep':cepInformado,
//				 indicadorCepDoTitular : indicaCepDoTitular
//			},
//			 beforeSend: function(){
//			 	bloquearTela();
//			 },
//			 complete: function(){
//			 	desbloquearTela();
//			 },
//			success: function(data){
//				console.log(data);
//				
//				var planoSelecionado = $('[name="proposta.plano.codigo"]').val();
//				marcarPlanoJaSelecionado(planoSelecionado);
//				
//				
//			},
//		  error: function( event, xhr, settings ){
//			  console.log('event ' + event);
//			  console.log('xhr ' + xhr);
//			  console.log('settings ' + settings);
//
//		  }
//			
//		});

		
		
	});	
	
	$("[name='proposta.pagamento.contaCorrente.banco']").on('change', function(){
		/*var banco = $(this).val();
		$('#cpf_pagamento').val('');
		$('#nome_pagante').val('');
		$('#nascimento_pagante').val('');
		$('#agencia').val('');
		$('#conta').val('');
		$('#digito_conta').val('');
		$('#agencia_conta').val(0);
		if(banco == 0){
			$('.cpf_pagamento').hide();
		}else{
			$('.cpf_pagamento').show();
		}
		$('.dados_debito_automatico').hide();
		$('.dados_debito_automatico_outros_bancos').hide();*/
	});	
	
	/**
	 * TODO Comentado porque ainda nao colocaremos para listar contas para o cpf informado
	$("[name='proposta.pagamento.contaCorrente.cpf']").on('blur', function(){
		var cpf = $(this).val();
		$('#nome_pagante').val('');
		$('#nascimento_pagante').val('');
		$('#agencia').val('');
		$('#conta').val('');
		$('#digito_conta').val('');
		$('#agencia_conta').val(0);
		if(cpf.length == 11){
			if($("#banco").val() == 237){
				$.submitAjax('proposta/listarContaCorrentePorCPF.do');
			}else{
				$('.dados_debito_automatico').show();
				$('.dados_debito_automatico_outros_bancos').show();
			}
		}else{
			$('.dados_debito_automatico').hide();
			$('.dados_debito_automatico_outros_bancos').hide();
		}
	});
	*/
	
	$('[data-telefone]').change(function(){
		mudarMascaraTelefone($(this).attr('data-telefone'));
	}).trigger('change');
	
	function mudarMascaraTelefone(idTelefone) {		
		var $inputNumeroTelefone = $('#' + idTelefone);
		var mascaraAnterior = $inputNumeroTelefone.inputmask("getemptymask");
		// Regra para nono digito nos telefones moveis
		if($('#tipo_telefone_representante_legal option:selected').text() == 'Celular'){
			$inputNumeroTelefone.inputmask({ mask: "(99) 9999-9999[9]", greedy: false });
		}else{
			$inputNumeroTelefone.inputmask({ mask: "(99) 9999-9999", greedy: false });
		}
	}
	function SomenteNumero(e){
	    var tecla=(window.event)?event.keyCode:e.which;   
	    if((tecla>47 && tecla<58)) return true;
	    else{
	    	if (tecla==8 || tecla==0) return true;
		else  return false;
	    }
	}
	

	
});

//function removerDependente(posicao){
//	$.submitAjax('proposta/removerDependente.do?numeroDependente=' + posicao);
//}

function removerDependente(posicao){

	console.log('remover dependente ' + posicao);
	var planoSelecionado = $('[name="proposta.plano.codigo"]').val();
	var quantidadeDependentes = $('#numero_vidas').text();
//	$.submitAjax('proposta/adicionarDependente.do');
	console.log('plano: ' + planoSelecionado);
	console.log('quantidadeDependentes: ' + quantidadeDependentes);
	$.executarAjax({
			url: window.baseURL + 'proposta/removerDependente.do?numeroDependente=' + posicao
			, dataType: 'html'
			, data: $('#form').serializeLatin()
			, type: 'POST'
			, success: function(html){
				$('#content').html(html);
				marcarPlanoJaSelecionado(planoSelecionado);
				console.log('valor mensal :' + $('#valorTotalMensal').val());
				console.log('valor anual :' + $('#valorTotalAnual').val());
			}
			, complete: function(){
				exibirCriticas();
				
			}
		});
	
}

function inicializarTela(){
	$('#nova_proposta').addClass('selected');
	$('#listar_proposta').removeClass('selected');
	$('#relatorio_acompanhamento').removeClass('selected');
	
	exibirCriticas();
	inicializarDadosRepresentanteLegal();	
	inicializarDadosTitular();
	inicializarDadosPagamento();
	identificarAmbiente();
	var planoSelecionado = $('#plano_selecionado').val();//$('[name="proposta.plano.codigo"]').val();
	if($("[name='proposta.status']").val() != "RASCUNHO"){
		$('#postoAtendimentoLabel').attr('disabled', true);
	}else{
		$('#postoAtendimentoLabel').attr('disabled', false);
	}
	
	$('[name="proposta.plano.codigo"][value= "'+planoSelecionado+'"]').prop('checked', true);
	$('#postoAtendimento').removeProp('disabled')
}

function exibirCriticas(){
	var existeCriticas = $('#hasActionErrors').val();
	if(existeCriticas){
		$('#msgErros').toggle($.parseJSON($('#hasActionErrors').val()));
		$('#msgErros').ScrollTo();
	}else{
		$('#msgErros').hide();
	}
}

//Length modificado dia 05/08/2019
function inicializarDadosRepresentanteLegal(){
	if($("[name='proposta.beneficiarios.titularMenorIdade']:checked").val() == 'true'){
		$('#representante_legal').show();
		
		//cep do representante legal
		// Length do Logradouro modificado por: Gabriel
		if($("[name='proposta.beneficiarios.representanteLegal.endereco.cep']").val().length == 8) {
			if($("[name='proposta.beneficiarios.representanteLegal.endereco.logradouro']").val().length <= 50 || $("[name='proposta.beneficiarios.representanteLegal.endereco.bairro']").val().length > 0 || $("[name='proposta.beneficiarios.representanteLegal.endereco.cidade']").val().length > 0 || $("[name='proposta.beneficiarios.representanteLegal.endereco.estado']").val().length > 0){			
				
				//Verifica se logradouro retorna algo e faz a modifica��o no input do mesmo
				// Length do Logradouro modificado por: Gabriel
				 if($("[name='proposta.beneficiarios.representanteLegal.endereco.logradouro']").val().length <= 50) {
					 $("[name='proposta.beneficiarios.representanteLegal.endereco.logradouro']").css({
						'outline':'0',
						'box-shadow': '0 0 0 0',
						'border': '0 none',
						'background-color': '#F5F5F5',
						'cursor': 'Default',
						'color': '##636161'
					 });
				 }
				 
				//Verifica se bairro retorna algo e faz a modifica��o no input do mesmo
				 if($("[name='proposta.beneficiarios.representanteLegal.endereco.bairro']").val().length > 0) {
					 $("[name='proposta.beneficiarios.representanteLegal.endereco.bairro']").css({
						'outline':'0',
						'box-shadow': '0 0 0 0',
						'border': '0 none',
						'background-color': '#F5F5F5',
						'cursor': 'Default',
						'color': '##636161'
					 });
				 }
				 
				//Exibe o(s) dado(s) retornado(s) pelo CEP
				$('.cep_representante_legal').show();
				
			}
		}
	}
}

function inicializarDadosTitular(){
	//cep do titular
	// Length do Logradouro modificado por: Gabriel
	if($("[name='proposta.beneficiarios.titular.endereco.cep']").val().length == 8){
		 if($("[name='proposta.beneficiarios.titular.endereco.logradouro']").val().length <= 50 || $("[name='proposta.beneficiarios.titular.endereco.bairro']").val().length > 0 || $("[name='proposta.beneficiarios.titular.endereco.cidade']").val().length > 0 || $("[name='proposta.beneficiarios.titular.endereco.estado']").val().length > 0) {	
			 //Verifica se logradouro retorna algo e faz a modifica��o no input do mesmo
			// Length do Logradouro modificado por: Gabriel
			 if($("[name='proposta.beneficiarios.titular.endereco.logradouro']").val().length <= 50) {
				 $("[name='proposta.beneficiarios.titular.endereco.logradouro']").css({
					'outline':'0',
					'box-shadow': '0 0 0 0',
					'border': '0 none',
					'background-color': '#F5F5F5',
					'cursor': 'Default',
					'color': '##636161'
				 });
			 }
			//Verifica se bairro retorna algo e faz a modifica��o no input do mesmo
			 if($("[name='proposta.beneficiarios.titular.endereco.bairro']").val().length > 0) {
				 $("[name='proposta.beneficiarios.titular.endereco.bairro']").css({
					'outline':'0',
					'box-shadow': '0 0 0 0',
					'border': '0 none',
					'background-color': '#F5F5F5',
					'cursor': 'Default',
					'color': '##636161'
				 });
			 }
			 //Exibe o(s) dado(s) retornado(s) do CEP
			$('.cep_titular').show();
		 }
	}
}

function inicializarDadosPagamento(){
	//banco
	inicializarBanco();
	//conta corrente
	inicializarContaCorrente();
}

function identificarAmbiente(){
//	console.log('location.hostname'+ location.hostname);
//	if(location.hostname == 'wwws.bradescoseguros.com.br'){
//		$('#ambiente').val('production');
//	}else{
//		$('#ambiente').val('sandbox');
//	}
	if(location.hostname.indexOf('dsv') != -1 || location.hostname.indexOf('hml') != -1 || location.hostname.indexOf('localhost') != -1){
		$('#ambiente').val('sandbox');
	}else{
		$('#ambiente').val('production');
	}
}
function inicializarBanco(){
	/*var banco = $('#banco').val();
	if(banco == 0){
		$('.cpf_pagamento').hide();
		$('#cpf_pagamento').val('');
		$('.dados_debito_automatico').hide();
		$('#nome_pagante').val('');
		$('#nascimento_pagante').val('');
		$('.dados_debito_automatico_outros_bancos').hide();
		$('#agencia').val('');
		$('#conta').val('');
		$('#digito_conta').val('');
	}else{
		$('.cpf_pagamento').show();
	}*/
}

function inicializarContaCorrente(){
	var cpf = $("[name='proposta.pagamento.contaCorrente.cpf']").val();
	if(cpf.length == 11 && $('#hasActionErrors').val() == 'false'){
		$('.dados_debito_automatico').show();
		var banco = $("#banco").val();
		$('.dados_debito_automatico_outros_bancos').show();
	}else if(cpf.length == 11 && $('#hasActionErrors').val() == 'true'){
		$('.dados_debito_automatico').show();
		$('.dados_debito_automatico_outros_bancos').show();
	}
}

function marcarPlanoJaSelecionado(planoSelecionado){
	
	if($('#plano_selecionado').val() == undefined ){
		return;
	} 
	
	$("#tabela_planos :radio").each(function() {
	    console.log('plano:: ' + $(this).val());
	    
	    if($(this).val() == planoSelecionado){
	    	$(this).prop('checked', true);
	    	$('#plano_selecionado').val(0);
	    	planoSelecionado = 0;
	    }	    
	});
}
//function sendCardData(){
//	var options = {
//		accessToken: $('.accessToken').val(),
//		onSuccess: function(e){
//			var paymentToken = e.PaymentToken;
//			$('.paymentToken').val(paymentToken);
//		},
//		onError: function(e){
//			var errorCode = e.Code;
//			var errorMessage = e.Text;
//			//alert(errorCode + " / " + errorMessage);
//		},
//		onInvalid: function(e){
//			for(var i = 0; i < e.length; i++){
//				var field = e[i].Field;
//				var message = e[i].Message;
//				//alert(field + " / " + message);
//			}
//		},//TODO sempre trocar para production ou sandbox
//		environment: "production",
//		language: "PT"
//		,
//		cvvrequired: false
//	};
//	bpSop_silentOrderPost(options);
//}

function obterEnderecoCepAjax(cepInformado, indicaCepDoTitular){
	
//	'proposta/obterEnderecoPorCep.do?cep=' + $(this).val() + '&indicadorCepDoTitular=' + true
	console.log('cepInformado:' + cepInformado);
	console.log('indicaCepDoTitular:' + indicaCepDoTitular);
	
	$.ajax({			
		type: "POST",
		url: window.baseURL + 'proposta/obterEnderecoPorCep.do',
		dataType: 'json',
		data:{
			'cep':cepInformado,
			 indicadorCepDoTitular : indicaCepDoTitular
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			console.log(data);
			$('.cep_titular').show();
			if(indicaCepDoTitular == 'true'){
				$('#bairro-titular').val(data.proposta.beneficiarios.titular.endereco.bairro);
				$('#cidade-titular').text(data.proposta.beneficiarios.titular.endereco.cidade);
				$('#estado-titular').text(data.proposta.beneficiarios.titular.endereco.estado);
				$('#logradouro-titular').val(data.proposta.beneficiarios.titular.endereco.logradouro);
				$('input[name="proposta.beneficiarios.titular.endereco.bairro"]').val(data.proposta.beneficiarios.titular.endereco.bairro);
				$('input[name="proposta.beneficiarios.titular.endereco.cidade"]').val(data.proposta.beneficiarios.titular.endereco.cidade);
				$('input[name="proposta.beneficiarios.titular.endereco.estado"]').val(data.proposta.beneficiarios.titular.endereco.estado);
				$('input[name="proposta.beneficiarios.titular.endereco.logradouro"]').val(data.proposta.beneficiarios.titular.endereco.logradouro);
				
				if(data.proposta.beneficiarios.titular.endereco.logradouro == null || data.proposta.beneficiarios.titular.endereco.logradouro ==""){
					$('#logradouro-titular').attr('disabled', false);
				}else{
					$('#logradouro-titular').attr('disabled', true);
				}
				
			}else{
				
				$('#bairro-representante').val(data.proposta.beneficiarios.representanteLegal.endereco.bairro);
				$('#cidade-representante').text(data.proposta.beneficiarios.representanteLegal.endereco.cidade);
				$('#estado-representante').text(data.proposta.beneficiarios.representanteLegal.endereco.estado);
				$('#logradouro-representante').val(data.proposta.beneficiarios.representanteLegal.endereco.logradouro);
				$('.cep_representante_legal').show();
				$('input[name="proposta.beneficiarios.representanteLegal.endereco.bairro"]').val(data.proposta.beneficiarios.representanteLegal.endereco.bairro);
				$('input[name="proposta.beneficiarios.representanteLegal.endereco.cidade"]').val(data.proposta.beneficiarios.representanteLegal.endereco.cidade);
				$('input[name="proposta.beneficiarios.representanteLegal.endereco.estado"]').val(data.proposta.beneficiarios.representanteLegal.endereco.estado);
				$('input[name="proposta.beneficiarios.representanteLegal.endereco.logradouro"]').val(data.proposta.beneficiarios.representanteLegal.endereco.logradouro);
				
				if(data.proposta.beneficiarios.representanteLegal.endereco.logradouro == null || data.proposta.beneficiarios.representanteLegal.endereco.logradouro ==""){
					$('#logradouro-representante').attr('disabled', false);
				}else{
					$('#logradouro-representante').attr('disabled', true);
				}
			}
		},
	  error: function( event, xhr, settings ){
		  console.log('event ' + event);
		  console.log('xhr ' + xhr);
		  console.log('settings ' + settings);

	  }
	});
}


function montarMengagensErro(mensagensErro){
	
	$('#serverErrors').text('');
	$.each( mensagensErro, function( key, mensagem ) {
		var descricao = "<li>" + mensagem + "</li>";
		$('#serverErrors').append(descricao);	
	});	
	$('#msgErros').show();
	$('#msgErros').ScrollTo();
}

function montarMensagemSucesso(mensagemSucesso){
	$('#serverErrors').text('');
	$('#msgErros').hide();

	$('#serverActionsMessagesAjax').text('');
	
	mensagemSucesso.forEach(function (mensagem){
		var descricao = "<li>" + mensagem + "</li>";
		$('#serverActionsMessagesAjax').append(descricao);			 
	  });
	$('#msgSucessoAjax').show();
	$('#msgSucessoAjax').ScrollTo();	
}

function limparMensagensErro(){
	$('#serverErrors').text('');
	$('#msgErros').hide();
}



