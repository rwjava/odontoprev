$(document).ready(function() {
	
	inicializarTela();
	
	$("[name='filtroProposta.tipoBusca']").on('change', function(){
		limparCampos();
		if($(this).val() == 1){
			$('.buscarPorCodigo').show();
			$('.buscarPorCPF').hide();
			$('.buscarPorPeriodo').hide();
		}
		else if($(this).val() == 2){
			
			$('.buscarPorCPF').show();
			$('.buscarPorCodigo').hide();
			$('.buscarPorPeriodo').hide();
		}
		else if($(this).val() == 3){
			
			$('.buscarPorPeriodo').show();
			$('.buscarPorCodigo').hide();
			$('.buscarPorCPF').hide();
		}
	});
	
//	$('#btn_limpar').on('click', function(){
//		$('#form').each (function(){
//			this.reset();
//		});
//		inicializarTela();
//	});
	
	$('#btn_limpar_2').on('click', function(){
		$('.lista-proposta2').val('');
		$('#form').attr('action', window.baseURL + 'proposta/intranet/listarProposta.do');
		$('#form').submit();
		
	});
	
	$('#btn_limpar').on('click', function(){
		$('.lista-proposta').val('');
		$('#form').attr('action', window.baseURL + 'proposta/listarProposta.do');
		$('#form').submit();
	});
	
	if ( $( ".calendario" ).size() > 0 ) {
		console.log('abriu calendario');
		$.datepicker.setDefaults( $.datepicker.regional[ "pt-BR" ] );
		$( ".calendario" ).datepicker({dateFormat: 'dd/mm/yy'});
	}
});

function inicializarTela(){
	exibirCriticas();
	
	$('#listar_proposta').addClass('selected');
	$('#nova_proposta').removeClass('selected');
	$('#relatorio_acompanhamento').removeClass('selected');

	
	var tipoBusca = $("[name='filtroProposta.tipoBusca']:checked").val();
	if(tipoBusca == 1){
		$('.buscarPorCodigo').show();
		$('.buscarPorCPF').hide();
		$('.buscarPorPeriodo').hide();
	}
	else if(tipoBusca == 2){
		$('.buscarPorCPF').show();
		$('.buscarPorCodigo').hide();
		$('.buscarPorPeriodo').hide();
	}
	else if(tipoBusca == 3){
		$('.buscarPorPeriodo').show();
		$('.buscarPorCodigo').hide();
		$('.buscarPorCPF').hide();
	}
}

function exibirCriticas(){
	var existeCriticas = $('#hasActionErrors').val();
	if(existeCriticas){
		$('#msgErros').toggle($.parseJSON($('#hasActionErrors').val()));
		$('#msgErros').ScrollTo();
	}else{
		$('#msgErros').hide();
	}
}

function limparCampos(){
	$(':text').each(function () {
        $(this).val('');
    });
	$('#status').val(0);
	$("[name='filtroProposta.tipoCPF']").prop('checked', false);
}



