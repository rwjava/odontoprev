$(document).ready(function() {
	desbloquearTela();
	inicializarTela();
	
	$('#buscarAcompanhamento').on('click', function (){
		
		console.log('buscarAcompanhamento');
//		$.submitAjax('proposta/buscarAcompanhamento.do');
		$('#form').attr('action', window.baseURL + 'proposta/buscarAcompanhamento.do');
		$('#form').submit();
		
	});

	
	$('#gerarPDFAcompanhamento').on('click', function (){
		
		console.log('gerarPDFAcompanhamento');
		 $('#form').serialize();
		  var urlBase = window.baseURL + 'proposta/gerarPDFAcompanhamento.do?'+ $('#form').serialize();
		  adicionarIframeParaDownload(urlBase);
	});
	

	
	$('#gerarCSVAcompanhamento').on('click', function (){
		
		console.log('gerarCSVAcompanhamento');
		  var urlBase = window.baseURL + 'proposta/gerarArquivoCSVRelatorioAcompanhamento.do?'+$('#form').serialize();
		  adicionarIframeParaDownload(urlBase);
	});
	
	$('#buscarAcompanhamentoIntranet').on('click', function (){
		
		console.log('buscarAcompanhamentoIntranet');
//		$.submitAjax('proposta/buscarAcompanhamento.do');
		$('#form').attr('action', window.baseURL + 'proposta/buscarAcompanhamentoIntranet.do');
		$('#form').submit();
		
	});

	$('#gerarPDFAcompanhamentoIntranet').on('click', function (){
		
		console.log('gerarPDFAcompanhamentoIntranet');
		  var urlBase = window.baseURL + 'proposta/intranet/gerarPDFAcompanhamentoIntranet.do';
		  adicionarIframeParaDownload(urlBase);
	});
	

	
	$('#gerarCSVAcompanhamentoIntranet').on('click', function (){
		
		console.log('gerarCSVAcompanhamentoIntranet');
		  var urlBase = window.baseURL + 'proposta/intranet/gerarArquivoCSVRelatorioAcompanhamentoIntranet.do'+$('#form').serialize();
		  adicionarIframeParaDownload(urlBase);
	});
	/*$('#btn_limpar').on('click', function(){
		$('#form').each (function(){
			this.reset();
		});
		inicializarTela();
	});*/
	desbloquearTela();
	
	
	$('#btn_limpar').on('click', function(){
		$('.lista-proposta').val('');
		$('#form').attr('action', window.baseURL + 'proposta/relatorioAcompanhamento.do');
		$('#form').submit();
	});
	
	$('#btn_limpar_intranet').on('click', function(){
		$('.lista-proposta').val('');
	});
	
});

function adicionarIframeParaDownload(urlBase){
	
	var iframe = '<iframe width="0px" height="0px" id="ajax-download"></iframe>';
	  if($('#ajax-download').length == 0){
	  	$('#form').append(iframe);
	  }
	  $('#ajax-download').attr('src', urlBase);
	  $('#ajax-download').hide();
	  
}
function inicializarTela(){
	exibirCriticas();
	
	$('#relatorio_acompanhamento').addClass('selected');
	$('#nova_proposta').removeClass('selected');
	$('#listar_proposta').removeClass('selected');
	

}

function exibirCriticas(){
	var existeCriticas = $('#hasActionErrors').val();
	if(existeCriticas){
		$('#msgErros').toggle($.parseJSON($('#hasActionErrors').val()));
		$('#msgErros').ScrollTo();
	}else{
		$('#msgErros').hide();
	}
}

function limparCampos(){
	$(':text').each(function () {
        $(this).val('');
    });
	$('#status').val(0);
	$("[name='filtroProposta.tipoCPF']").prop('checked', false);
}



