//Função para ser aceito apenas números nos inputs selecionados
function verificaNumero(e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
    }
}

//Função para ser aceito apenas letras
/*function soLetras(obj){
     var tecla = (window.event) ? event.keyCode : obj.which;
     if((tecla >= 65 && tecla <= 90 || tecla >= 97 && tecla <= 122)
         ||(tecla > 97 && tecla < 122))
               return true;
     else{
          if (tecla != 8) return false;
          else return true;
     }
}
*/
function identificaSegmento() {
	var tipoSucursal;
	
	$.ajax({
		
		type: "POST",
		url: window.baseURL + 'proposta/retornaTipoSucursal.do',
		dataType: 'json',
		data:{
			'proposta.sucursalSelecionada.cpdSucursal':$('#sucursalList').val(),
			'proposta.sequencial' : $('#sequencial').val()
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			
			console.log(data.proposta.sucursalSelecionada.tipo);
			tipoSucursal = data.proposta.sucursalSelecionada.tipo;
			console.log(data.formasPagamento);
					  			
				$('#agenciaProdutora').attr('disabled', false);
				$('#gerenciaProduto').attr('disabled', false);
				$('#cpdCorretorMasterLogado').attr('disabled', false);
				$('#postoAtendimento').attr('disabled', false);

			switch(data.proposta.sucursalSelecionada.tipo){
			
			case "MERCADO":
		
				$("#form_proposta_corretor_agenciaProdutora").attr("disabled", true);
				$("#form_proposta_codigoMatriculaGerente").attr("disabled", true); 
				$("#form_proposta_corretor_cpd").attr("disabled", true);
				
				$('#agenciaProdutora').attr('disabled', true);
				$('#gerenciaProduto').attr('disabled', true);
				$('#cpdCorretorMasterLogado').attr('disabled', true);
				$('#postoAtendimento').attr('disabled', true);
				break;
		
			case "CORPORATE":			
				$('#postoAtendimento').attr('disabled', true);
				break;
				
			case "REDE":
				$('#agenciaProdutora').attr('disabled', false);
				$('#gerenciaProduto').attr('disabled', false);
				$('#cpdCorretorMasterLogado').attr('disabled', false);
				$('#postoAtendimento').attr('disabled', false);
				break;
			}
			
			
			if($("#nomeAngariador").text() == "") {
				$("#nomeAngariador").text(data.proposta.corretor.nome);
			}
			
//			$('#forma_pagamento').find('option').remove().end()
//		    .append('<option value="0">Selecione</option>');
		    //.val('whatever');
			
			atualizarComboFormaPagamento(data);
			var formaSelecionada = $('#forma-pagamento-selecionado').val();
			
			/*if($('#sucursalList').find(":selected").text().indexOf('REDE') !== -1){
				var planoSelecionado = 3;
				obterFormasPagamento(planoSelecionado);
			}*/
			//console.log('formaSelecionada: ' + formaSelecionada);
			if(formaSelecionada != undefined && formaSelecionada != null && formaSelecionada != ""){
				$('#forma_pagamento').val(formaSelecionada);
				regraExibicaoDeAcordoComFormaPagamento();
			}

			
			$("#tabela_planos").find('.linha_plano').remove();
			preencherPlanos(data.planos);
			var planoSelecionado = $('#plano_selecionado').val();//$('[name="proposta.plano.codigo"]').val();	
			if(planoSelecionado == 15 || planoSelecionado == 17 || planoSelecionado == 18 || planoSelecionado == 19 || planoSelecionado == 20 || planoSelecionado == 21 || planoSelecionado == 22 || planoSelecionado == 23){
				$('input:radio[name="proposta.beneficiarios.titularMenorIdade"][value="true"]').prop('checked', true);
				$('#form_proposta_beneficiarios_titularMenorIdadefalse').attr('disabled', 'disabled');
				$('#representante_legal').show();
				$("#dependente_dente_leite").hide();
			}
			$('[name="proposta.plano.codigo"][value= "'+planoSelecionado+'"]').prop('checked', true);
			//$('.mascara-moeda').formatCurrency({'region': 'pt-BR', 'symbol ' : 'R$'});
		},
	  error: function(data){
		  console.log('error ' + data);
	  }
		
	});
//	atualizarPlanos(tipoSucursal);
}

function retornaDadosCorretorMaster() {
	
	var tipoSucursal;
	$.ajax({
		
		type: "POST",
		url: window.baseURL + 'proposta/retornaDadosCorretor.do',
		dataType: 'json',
		data:{
			'proposta.corretor.cpd':$('#cpdCorretorMasterLogado').val(),
			'proposta.sucursalSelecionada.cpdSucursal':$('#sucursalList').val()
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			if(data.actionErrors.length  > 0){
				montarMengagensErro(data.actionErrors);
		    }else{
		    	limparMensagensErro();
				console.log(data.proposta.corretorMaster.nome);
				$('#nomeCorretorMaster').text(data.proposta.corretorMaster.nome);
				$('input[name="proposta.corretorMaster.nome"]').val(data.proposta.corretorMaster.nome);
				$('label[name="proposta.corretorMaster.nome"]').text(data.proposta.corretorMaster.nome);
				$('input[name="proposta.corretorMaster.cpd"]').val(data.proposta.corretorMaster.cpd);
				$('label[name="proposta.corretorMaster.cpd"]').text(data.proposta.corretorMaster.cpd);
		    }
		},
	  error: function(data){
		  if(data.actionErrors.length  > 0){
				montarMengagensErro(data.actionErrors);
		  }
	  }
		
	});
}

function retornaDadosAngariador() {
	
	var tipoSucursal;
	$.ajax({
		
		type: "POST",
		url: window.baseURL + 'proposta/retornaDadosAngariador.do',
		dataType: 'json',
		data:{
			'proposta.sucursalSelecionada.cpdSucursal':$('#sucursalList').val()
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
			if(data.actionErrors.length  > 0){
				montarMengagensErro(data.actionErrors);
		    }else{			
		    	console.log(data.proposta.angariador.nome);
						  		  
				$('#nomeAngariador').text(data.proposta.angariador.nome);
				$('#cpdAngariador').text(data.proposta.angariador.cpd);
				$('input[name="proposta.angariador.nome"]').val(data.proposta.angariador.nome);
				$('label[name="proposta.angariador.nome"]').text(data.proposta.angariador.nome);
				$('input[name="proposta.angariador.cpd"]').val(data.proposta.angariador.cpd);
				$('label[name="proposta.angariador.cpd"]').text(data.proposta.angariador.cpd);
		    }
		},
	  error: function(data){
		  console.log('error ' + data);		
		  
		  if(data.actionErrors != undefined && data.actionErrors.length  > 0){
				montarMengagensErro(data.actionErrors);
		  }
	  }
		
	});
}

function preencherPlanos(planos){
	
	
	$.each( planos, function( key, p ) {
		  
		var linha = '<tr class="linha_plano">';
        linha += '<td>';
        linha += '		<input class="radio_plano" type="radio" name="proposta.plano.codigo" value='+p.codigo+'> <label>' +p.nome+'</label>';
        linha += '</td>';
        linha += '<td class="mascara-moeda">';
        linha += '	R$ ' + p.valorPlanoVO.valorMensalTitular;        
//    	linha += '<fmt:formatNumber value="${p.valorPlanoVO.valorMensalTitular}" type="currency" currencySymbol="R$"/>';
    	linha += '</td>';
        
        linha += '<td class="mascara-moeda">';
        linha += '	R$ ' + p.valorPlanoVO.valorAnualTitular;
        linha += '</td>';
        linha += '<td class="mascara-moeda">';
        linha += '	R$ '+ p.valorPlanoVO.valorMensalDependente;
        linha += '</td>';
        linha += '<td class="mascara-moeda">';
        linha += '	R$ '+p.valorPlanoVO.valorAnualDependente;
        linha += '</td>';
        linha += '<td>'+p.descricaoCarenciaPeriodoMensal+'</td>';
        linha += '<td>'+p.descricaoCarenciaPeriodoAnual+'</td>';
        linha += '</tr>';
		
    
        
	    $(linha).appendTo($("#tabela_planos"));
		
		});
	
	
	
}

function atualizarPlanos(tipoSucursal){
	console.log('atualizarPlanos()');
	$.submitAjax('proposta/obterPlanos.do?tipoSucursal='+tipoSucursal);

}

function atualizarComboFormaPagamento(listaFormaPagamento){
	
	$('#forma_pagamento').find('option').remove().end()
    .append('<option value="0">Selecione</option>');
	
	
	$.each( listaFormaPagamento.formasPagamento, function( key, value ) {
		  $('#forma_pagamento').append($('<option>', { 
		        value: value.codigo,
		        text : value.descricao
		    }));
		});
	
	console.log('atualizando combo de forma de pagamento');
}

function limparCamposDebitoAutomatico(){
	
	$('input[name="proposta.pagamento.contaCorrente.numeroAgencia"]').val(null);
	$('input[name="proposta.pagamento.contaCorrente.numeroConta"]').val(null);		
	$('input[name="proposta.pagamento.contaCorrente.cpf"]').val(null);
	$('input[name="proposta.pagamento.contaCorrente.nome"]').val(null);
	$('input[name="proposta.pagamento.contaCorrente.dataNascimento"]').val(null);		
	$('input[name="proposta.pagamento.contaCorrente.digitoVerificadorConta"]').val(null);
}
function limparCampoCartaoCredito(){

	$('.bp-sop-cardholdername').val(null);
	$('.bp-sop-cardnumber').val(null);
	$('.bp-sop-cardexpirationdate').val(null);
}

//n�o esta aki
function atualizarComboBancos(listaPlanos){

	$('#banco').find('option').remove().end()
    .append('<option value="0">Selecione</option>');
	
	
	
	/*listaPlanos.forEach(function(c){
		
		$('#banco').append($('<option>', { 
	        value: c.codigo,
	        text : c.descricao
	}));
	});*/
	
	
	$.each( listaPlanos, function( key, c ) {
				$('#banco').append($('<option>', { 
			        value: c.codigo,
			        text : c.descricao
			    }));
	});
	
}

function obterFormasPagamento(planoSelecionado){
	var codigoPlano;
	//debugger;
	/*if(planoSelecionado == 13 && $('.radio_plano option:selected').val() == undefined){
		codigoPlano = 13;
	} else{
		codigoPlano = $('.radio_plano:checked').val();
	}*/
	codigoPlano = $('.radio_plano:checked').val();
	console.log('C�digo plano: ' + codigoPlano);
	
	var numeroVidas = $('#numero_vidas').text();
	console.log('obterFormasPagamento\t numero de vidas : ' + numeroVidas);
	$.ajax({
		
		type: "POST",
		url: window.baseURL + 'proposta/obterFormasPagamentoEListaBancosDeAcordoComSegmentoEPlano.do',
		dataType: 'json',
		data:{
			'proposta.sucursalSelecionada.cpdSucursal':$('#sucursalList').val(),
			'proposta.plano.codigo' : codigoPlano,
			'numeroVidas' : $('#numero_vidas').text()
		},
		 beforeSend: function(){
		 	bloquearTela();
		 },
		 complete: function(){
		 	desbloquearTela();
		 },
		success: function(data){
				atualizarComboFormaPagamento(data);
				atualizarComboBancos(data.bancos);
				console.log('valor mensal :' + data.valorTotalMensal);
				console.log('valor anual :' + data.valorTotalAnual);
				$('#mensal-label').text(data.valorTotalMensal);
				$('#anual-label').text(data.valorTotalAnual);
				$('.mascara-moeda').formatCurrency({'region': 'pt-BR', 'symbol ' : 'R$'});
				$('.debito_automatico').hide();
				$('.cartao_credito').hide();
				limparCamposDebitoAutomatico();
				limparCampoCartaoCredito();
		},
	  error: function(data){
		  console.log('error ' + data);
	  }
		
	});
}


/*function limpaInformacoes() {
	$("#nomeAngariador").text("");
}*/

$(document).ready(function() {
	
	/*limpaInformacoes();*/
	
//	if($("#nomeAngariador").text() != undefined) {
//		$("#nomeAngariador").hide();
//	}
	
	if($('#sucursalList').val() != '0') {
		identificaSegmento();
//		$('#nomeCorretorMaster').text("");//TODO verificar se deve limpar nome do corretor master mesmo
	}
	
	//Faz a veifica��o nos inputs para ver se � dados numeros digitados
	$('#form_filtroProposta_cpf').keypress(verificaNumero);
		
		//Oculta blocos formas de pagamento at� uma ser selecionada na comboBox
		if($('#forma_pagamento').val() == '0') {
			$('.debito_automatico').hide();
		}
		$('.cartao_credito').hide();
		$('.banco_pagamento').hide();
		
		//Fun��o para exibir blocos de pagamentos
		$('#forma_pagamento').on('change', function() {
			if($('#forma_pagamento').val() == '0') {
				$('.debito_automatico').hide();
				$('.cartao_credito').hide();
			}
			else if($('#forma_pagamento').val() == '1') {
				$('.debito_automatico').hide();
				$('.cartao_credito').hide();
			}
			/*
			 * Para 
			 * 
			 * 
			 * */
			else if($('#forma_pagamento').val() == '2') {
				var sucursalSelecionada = $('#sucursalList').val();
				var codSucursal = sucursalSelecionada.substr(0,3);
					if(codSucursal == '910'|| codSucursal=='915' || codSucursal=='918' || codSucursal=='927' || codSucursal=='931' || codSucursal=='947'
						||codSucursal == '935'|| codSucursal== '939' || codSucursal=='943' || codSucursal == '948' || codSucursal=='949'|| codSucursal=='950'
								||codSucursal=='951'||codSucursal=='952'|| codSucursal=='953'|| codSucursal=='954'||codSucursal=='955'||codSucursal=='956'||
									codSucursal=='956'||codSucursal=='957'||codSucursal=='958'||codSucursal=='959'||codSucursal=='960'||codSucursal=='961'){
						$('.debito_automatico').show();
						$('.cartao_credito').hide();
						$("#banco option[value='237']").attr("selected", true);
						$("#banco option[value='33']").hide();
						$("#banco option[value='0']").hide();
						$("#banco option[value='341']").hide();
					} else {
					$('.debito_automatico').show();
					$('.cartao_credito').hide();
				}
			} else {
				$('.debito_automatico').hide();
			}
			
			if($('#forma_pagamento').val() == 5 || $('#forma_pagamento').val() == 6 || $('#forma_pagamento').val() == 7){
				$('.debito_automatico').hide();
				$('.cartao_credito').show();
				gerarAccessToken();
			}
			
		});

/*$(document).ready(function() {
	
	limpaInformacoes();
	
//	if($("#nomeAngariador").text() != undefined) {
//		$("#nomeAngariador").hide();
//	}
	
	if($('#sucursalList').val() != '0') {
		identificaSegmento();
//		$('#nomeCorretorMaster').text("");//TODO verificar se deve limpar nome do corretor master mesmo
	}
	
	//Faz a veificação nos inputs para ver se é dados numeros digitados
	$('#form_filtroProposta_cpf').keypress(verificaNumero);
		
		//Oculta blocos formas de pagamento até uma ser selecionada na comboBox
		if($('#forma_pagamento').val() == '0') {
			$('.debito_automatico').hide();
		}
		$('.cartao_credito').hide();
		$('.banco_pagamento').hide();
		
		//Função para exibir blocos de pagamentos
		$('#forma_pagamento').on('change', function() {
			if($('#forma_pagamento').val() == '0') {
				$('.debito_automatico').hide();
				$('.cartao_credito').hide();
			}
			else if($('#forma_pagamento').val() == '1') {
				$('.debito_automatico').hide();
				$('.cartao_credito').hide();
			}
			else if($('#forma_pagamento').val() == '2') {
				var sucursalSelecionada = $('#sucursalList').val();
				var codSucursal = sucursalSelecionada.substr(0,3);
				//910, 915, 918, 927, 931, 935, 939, 943, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961
				if(codSucursal == '910' || codSucursal == '915' || codSucursal == '918' || codSucursal == '927' || codSucursal == '931'|| codSucursal == '947' || codSucursal == '935' || codSucursal == '939'
					|| codSucursal == '943' || codSucursal == '948' || codSucursal == '949' || codSucursal == '950' || codSucursal == '951' || codSucursal == '952' || codSucursal == '953' || codSucursal == '954'
						|| codSucursal == '955' || codSucursal == '956' || codSucursal == '957' || codSucursal == '958' || codSucursal == '959' || codSucursal == '960' || codSucursal == '961'){				
					$("#banco option[value='237']").attr("selected", true);
					$("#banco option[value='33']").hide();
					$("#banco option[value='0']").hide();
					$("#banco option[value='341']").hide();
				} else {
				$('.debito_automatico').hide();
				$('.cartao_credito').hide();
			}
			
			if($('#forma_pagamento').val() == 5 || $('#forma_pagamento').val() == 6 || $('#forma_pagamento').val() == 7){
				$('.debito_automatico').hide();
				$('.cartao_credito').show();
				gerarAccessToken();
			}
			
		});*/
		
	
		//Função para exibir botão 'IMPRIMIR PROPOSTA VAZIA'
		if($('#sucursalList').val() == '0') {
			$('.imprimir_proposta_vazia').hide();
		}
		
		$('#sucursalList').on('change', function() {
			$('#form input[type ="text"]').val("");
			$("#forma_pagamento").val("0");
			$('#form_proposta_beneficiarios_titular_estadoCivil_codigo').val("0");
			$('#form_proposta_beneficiarios_dependentes_0__grauParentesco_codigo').val("0");
			$('#form_proposta_beneficiarios_dependentes_0__estadoCivil_codigo').val("0");
			if(this.value != '0') {
				$("#nomeAngariador").show();
				retornaDadosAngariador();
			} else {
				$("#nomeAngariador").hide();
				$('#cpdCorretorMasterLogado').prop('disabled', 'disabled');
			}
			
			$('#cpdCorretorMasterLogado').val(null);
			$('#nomeCorretorMaster').text('');
			$('#gerenciaProduto').val(null);
			
			identificaSegmento();
		});
		
		$('#cpdCorretorMasterLogado').on('blur', function() {
			if($('#cpdCorretorMasterLogado').val() != null && $('#cpdCorretorMasterLogado').val() != ''){
				retornaDadosCorretorMaster();
			}
		});
		
		/*$("#sucursalList").on('change', function(){
			obterFormasPagamento(3);
		});*/
		
		$("#tabela_planos").on('change','.radio_plano', function(){
			console.log('planoselecionado: ' + $('.radio_plano:checked').val());
			var planoSelecionado = $('.radio_plano:checked').val();
//			$('#plano_selecionado').val(planoSelecionado);
			obterFormasPagamento(planoSelecionado);
			
			if(this.value != '0' && $('#codigoProposta').val() == "" && $('.radio_plano:checked').val() != undefined) {
				$('.imprimir_proposta_vazia').show();
		  	}else{
		  	 	$('.imprimir_proposta_vazia').hide();
		  	}

		});
		
	});
/*$(".radio_plano option:selected").val();
	   var radio_plano = $(this).val();
	   
	alert(radio_plano);*/
/*	var val = $('.linha_plano').val();
		alert(val);*/
	
	
	$(window).load(function(){
		 $('#subtitulo').html('Cadastro de Proposta');
	 });
	
	function teste() {
		alert("Text = " + $("#sucursalList option:selected").text());
		alert("Val = " + $("#sucursalList").val());
		alert("Tipo CPD = " + $("#form_proposta_sucursalSelecionada_tipo").val());
		alert("Tipo CPD = " + $("#form_proposta_sucursalSelecionada_tipo").text());
		alert("Plano = " + $("input[name='proposta.plano.codigo']:checked").val());
		alert($("#forma_pagamento option:selected").text());
		alert($('#forma_pagamento').val());
		alert("Banco valor text = " + $("#banco option:selected").text());
		alert("banco valor val = " + $('#banco').val());		
		
		/*alert("Tipo CPD = " + $("#form_proposta_sucursalSelecionada_tipo").val());
		alert("Tipo CPD = " + $("#form_proposta_sucursalSelecionada_tipo").text());
		if($("#form_proposta_sucursalSelecionada_tipo").val() == 'MERCADO') {
			alert("TRUE");
		} else if($("#form_proposta_sucursalSelecionada_tipo").val() == '' || $("#form_proposta_sucursalSelecionada_tipo").val() == null) {
			alert("FALSE");
		}*/  
	}
	
	function regraExibicaoDeAcordoComFormaPagamento(){
		if($('#forma_pagamento').val() == '0') {
			$('.debito_automatico').hide();
		}
		$('.cartao_credito').hide();
		$('.banco_pagamento').hide();
		
		//Função para exibir blocos de pagamentos
			
			if($('#forma_pagamento').val() == '0') {
				$('.debito_automatico').hide();
				$('.cartao_credito').hide();
			}
			else if($('#forma_pagamento').val() == '2') {
				$('.debito_automatico').show();
				$('.cartao_credito').hide();
			} else {
				$('.debito_automatico').hide();
			}
			
			if($('#forma_pagamento').val() == 5 || $('#forma_pagamento').val() == 6){
				$('.debito_automatico').hide();
				$('.cartao_credito').show();
			}
			
	}
	
	function gerarAccessToken(){
		console.log('gerarAccessToken');
		var formaPagamento = $('#forma_pagamento').val();
		$.ajax({			
			type: "POST",
			url: window.baseURL + 'proposta/gerarAccessToken.do',
			dataType: 'json',
			data:{
				'proposta.pagamento.formaPagamento':formaPagamento
			},
			 beforeSend: function(){
			 	bloquearTela();
			 },
			 complete: function(){
			 	desbloquearTela();
			 },
			success: function(data){
				console.log(data);
				if(data.actionErrors.length  > 0){
					montarMengagensErro(data.actionErrors);
					$('.cartao_credito').hide();
			    }
				$('.accessToken').val(data.proposta.pagamento.cartaoCredito.accessToken);
				$('.paymentToken').val(data.proposta.pagamento.cartaoCredito.paymentToken);
				
				atualizarComboBandeiras(data.bandeiras);
				
			},
		  error: function( event, xhr, settings ){
			  console.log('event ' + event);
			  console.log('xhr ' + xhr);
			  console.log('settings ' + settings);

		  }
			
		});
	}
	
	
	function atualizarComboBandeiras(listaBandeiras){
		
		$('#bandeiras').find('option').remove().end()
	    .append('<option value="0">Selecione</option>');
		
		listaBandeiras.forEach(function(c){
			
			$('#bandeiras').append($('<option>', { 
		        value: c.codigo,
		        text : c.descricao
		    }));

		});
	}
	
	function adicionarMensagemErro(mensagem){
		$('#msgErros').show();
		$('#serverErrors').text('');
		descricao = "<li>" + mensagem + "</li>";
		$('#serverErrors').append(descricao);
		$('#msgErros').ScrollTo();
		//$("#msgErros").fadeOut(60000);
	}