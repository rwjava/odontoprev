package br.com.bradseg.eedi.dentalindividual.validador;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.dentalindividual.proposta.support.BaseValidatorTest;
import br.com.bradseg.eedi.dentalindividual.spring.IntegrationTest;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.SucursalVO;

public class NovaPropostaValidadorTest extends IntegrationTest {

	@Autowired
	private NovaPropostaValidator novaPropostaValidator;
	private PropostaVO proposta;

	@Before
	public void setUp() {
		proposta = new PropostaVO();
		proposta.setSucursalSelecionada(new SucursalVO());
//		proposta.getSucursalSelecionada().setCpdSucursal("911;401462");
	}

	@Test(expected=BusinessException.class)
	public void deveOcorrerErroAoValidarPropostaVaziaSemCampos(){
		
		try{
			novaPropostaValidator.validatePropostaVazia(proposta);
		}catch(BusinessException e){
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Assistente de Produ��o");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "CPD/Sucursal");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Plano");
			throw e;
		}
		
	}
	
	@Test(expected=BusinessException.class)
	public void deveRetornarErroAoNaoPreencherAssistenteProducao(){
		try{
			Validador validador = new Validador();
			novaPropostaValidator.validarDadosCorretor(validador, proposta.getCorretor());
			if(validador.hasError()){
				throw new BusinessException(validador.getMessages());
			}
		}catch(BusinessException e){
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Assistente de Produ��o");
			throw e;
		}
	}
}
