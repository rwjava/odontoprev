package br.com.bradseg.eedi.dentalindividual.spring;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;


public abstract class IntegrationTest extends BaseValidatorTest{

	private PlatformTransactionManager transactionManager;
	private TransactionStatus transaction;

	private static SpringLoader springLoader = SpringLoader.INSTANCE;

	@Before
	public void before() {
		try {
			springLoader.autowire(this);
			beginTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void after() {
		rollbackTransaction();
	}

	public void beginTransaction() {
		System.out.println("Iniciando transa��o");
		transactionManager = springLoader.getBean(DataSourceTransactionManager.class);
		DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRED);
		transactionDefinition.setIsolationLevel(TransactionDefinition.ISOLATION_READ_UNCOMMITTED);
		transaction = transactionManager.getTransaction(transactionDefinition);
	}

	public void rollbackTransaction() {
		transactionManager.rollback(transaction);
		System.out.println("Finalizando transa��o");
	}

	protected Sql sql(String sql) {
		return new Sql(springLoader.getContext().getBean(DataSource.class), sql);
	}

}