package br.com.bradseg.eedi.dentalindividual.spring;

import static org.junit.Assert.fail;

import java.util.List;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.message.Message;

import com.google.common.collect.Lists;

public abstract class BaseValidatorTest {
	
	protected void assertHasMessage(BusinessException e, String errorKey, Object... params) {
		for (Message message : e.getMessages()) {
			if (message.getKey().equals(errorKey)) {
				List<Object> messageParams = Lists.newArrayList(message.getParameters());
				if (messageParams.size() == 0 && params.length == 0) {
					return;
				} else {
					for (Object param : params) {
						if (messageParams.contains(param)) {
							return;
						}
					}
				}
			}
		}
		fail("Mensagem: [" + errorKey + "] n�o encontrada com os par�metros: [" + descricaoParametros(params) + "]\nMensagens encontradas: " + e.getMessages());
	}

	private String descricaoParametros(Object[] params) {
		StringBuilder descricao = new StringBuilder();
		for (int i = 0; i < params.length; i++) {
			if (i > 0) {
				descricao.append(",");
			}
			descricao.append(params[i]);
		}
		return descricao.toString();
	}

}