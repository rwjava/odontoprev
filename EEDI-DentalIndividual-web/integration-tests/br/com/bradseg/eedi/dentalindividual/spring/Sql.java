package br.com.bradseg.eedi.dentalindividual.spring;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import com.google.common.collect.Maps;

public class Sql {

	private String sql;
	private MapSqlParameterSource params = new MapSqlParameterSource();
	private DataSource dataSource;

	public Sql(DataSource dataSource, String sql) {
		this.dataSource = dataSource;
		this.sql = sql;
	}

	public Sql param(String name, Object value) {
		params.addValue(name, value);
		return this;
	}

	public Sql param(String name, Object value, int sqlType) {
		params.addValue(name, value, sqlType);
		return this;
	}

	public Sql param(String name, Object value, int sqlType, String typeName) {
		params.addValue(name, value, sqlType, typeName);
		return this;
	}

	public Sql params(Map<String, Object> params) {
		this.params.addValues(params);
		return this;
	}

	public int update() {
		long start = System.currentTimeMillis();
		try {
			System.out.print("EXECU��O DE Sql INSERT/UPDATE: " + sql + " com os par�metros: " + params.getValues());
			return getJdbcTemplate().update(sql, params);
		} finally {
			System.out.println(" OK. Dura��o: " + (System.currentTimeMillis() - start) + " ms");
		}
	}

	public Number updateAndReturnKey() {
		long start = System.currentTimeMillis();
		try {
			System.out.print("EXECU��O DE Sql INSERT/UPDATE: " + sql + " com os par�metros: " + params.getValues());
			GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
			getJdbcTemplate().update(sql, params, keyHolder);
			return keyHolder.getKey();
		} finally {
			System.out.println(" OK. Dura��o: " + (System.currentTimeMillis() - start) + " ms");
		}
	}

	private NamedParameterJdbcTemplate getJdbcTemplate() {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	public Map<String, Object> queryFirst() {
		long start = System.currentTimeMillis();
		try {
			System.out.print("EXECU��O DE Sql SELECT: " + sql + " com os par�metros: " + params.getValues());
			return getJdbcTemplate().queryForObject(sql, params, new RowMapper<Map<String, Object>>() {
				public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
					HashMap<String, Object> map = Maps.newHashMap();
					final int columnCount = rs.getMetaData().getColumnCount();
					for (int i = 1; i <= columnCount; i++) {
						String key = rs.getMetaData().getColumnName(i);
						Object value = rs.getObject(i);
						map.put(key, value);
					}
					return map;
				}
			});
		} finally {
			System.out.println(" OK. Dura��o: " + (System.currentTimeMillis() - start) + " ms");
		}

	}

	public List<Map<String, Object>> list() {
		long start = System.currentTimeMillis();
		try {
			System.out.print("EXECU��O DE Sql SELECT: " + sql + " com os par�metros: " + params.getValues());
			return getJdbcTemplate().queryForList(sql, params);
		} finally {
			System.out.println(" OK. Dura��o: " + (System.currentTimeMillis() - start) + " ms");
		}
	}

}