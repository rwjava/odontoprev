package br.com.bradseg.eedi.dentalindividual.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestScope;
import org.springframework.web.context.request.ServletRequestAttributes;

public enum SpringLoader {

	INSTANCE("classpath*:META-INF/AppTestContext.xml");

	private String configLocations;
	private ClassPathXmlApplicationContext context;

	private SpringLoader(String configLocations) {
		this.configLocations = configLocations;
	}

	protected ApplicationContext getContext() {
		if (context == null) {
			context = createApplicationContext();
			context.registerShutdownHook();
		}
		return context;
	}

	private ClassPathXmlApplicationContext createApplicationContext() {
		ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(configLocations);

		configureMockRequestScope(classPathXmlApplicationContext);

		return classPathXmlApplicationContext;
	}

	private void configureMockRequestScope(ClassPathXmlApplicationContext classPathXmlApplicationContext) {
		// Registra o escopo Request para que possa se testar a partir da action
		classPathXmlApplicationContext.getBeanFactory().registerScope("request", new RequestScope());

		// Cria um mock do escopo de request para simular o container
		MockHttpServletRequest request = new MockHttpServletRequest();
		ServletRequestAttributes attributes = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attributes);
	}

	public void autowire(Object bean) {
		getContext().getAutowireCapableBeanFactory().autowireBean(bean);
	}

	public <T> T getBean(Class<T> beanClass) {
		return getContext().getBean(beanClass);
	}

}