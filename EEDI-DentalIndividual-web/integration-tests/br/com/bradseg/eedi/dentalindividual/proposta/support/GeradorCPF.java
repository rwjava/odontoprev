package br.com.bradseg.eedi.dentalindividual.proposta.support;

public class GeradorCPF {

	  public static String geraCPF() {  
	        String iniciais = "";  
	        Integer numero;  
	        for (int i = 0; i < 9; i++) {  
	            numero = new Integer((int) (Math.random() * 10));  
	            iniciais += numero.toString();  
	        }  
	        return iniciais + calcularDigitoVerificador(iniciais);  
	    }
	  
	  private static String calcularDigitoVerificador(String cpfParaCalculo) {  
	        Integer primeiroDigito, segundoDigito;  
	        int soma = 0, peso = 10;  
	        for (int i = 0; i < cpfParaCalculo.length(); i++)  
	                soma += Integer.parseInt(cpfParaCalculo.substring(i, i + 1)) * peso--;  
	        if (soma % 11 == 0 | soma % 11 == 1)  
	            primeiroDigito = new Integer(0);  
	        else  
	            primeiroDigito = new Integer(11 - (soma % 11));  
	        soma = 0;  
	        peso = 11;  
	        for (int i = 0; i < cpfParaCalculo.length(); i++)  
	                soma += Integer.parseInt(cpfParaCalculo.substring(i, i + 1)) * peso--;  
	        soma += primeiroDigito.intValue() * 2;  
	        if (soma % 11 == 0 | soma % 11 == 1)  
	            segundoDigito = new Integer(0);  
	        else  
	            segundoDigito = new Integer(11 - (soma % 11));  
	        return primeiroDigito.toString() + segundoDigito.toString();  
	    } 
}
