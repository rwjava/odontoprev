package br.com.bradseg.eedi.dentalindividual.proposta.facade;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.matchers.JUnitMatchers.containsString;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.dentalindividual.odontoprev.facade.OdontoprevServiceFacade;
import br.com.bradseg.eedi.dentalindividual.spring.IntegrationTest;
import br.com.bradseg.eedi.dentalindividual.util.Constantes;
import br.com.bradseg.eedi.dentalindividual.vo.BancoVO;
import br.com.bradseg.eedi.dentalindividual.vo.EnderecoVO;
import br.com.bradseg.eedi.dentalindividual.vo.EstadoCivilVO;
import br.com.bradseg.eedi.dentalindividual.vo.FormaPagamento;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.SituacaoProposta;
import br.com.bradseg.eedi.dentalindividual.vo.SucursalVO;
import static br.com.bradseg.eedi.dentalindividual.proposta.support.GeradorCPF.geraCPF;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class PropostaServiceFacadeIntegrationTest extends IntegrationTest {

	private PropostaVO proposta;
	@Autowired
	private PropostaServiceFacade propostaServiceFacade;
	@Autowired
	private OdontoprevServiceFacade odontoprevServiceFacade;

	@Before
	public void setUp() { 

		proposta = new PropostaVO();
		proposta.getBeneficiarios().getTitular().setCpf("26484383359");
		proposta.getBeneficiarios().getTitular().setDataNascimento(new LocalDate("1990-01-01"));
		proposta.getBeneficiarios().getTitular().setEmail("email@email.com");
		proposta.getBeneficiarios().getTitular().setEndereco(new EnderecoVO());
		proposta.getBeneficiarios().getTitular().getEndereco().setCep("20091007");
		proposta.getBeneficiarios().getTitular().getEndereco().setLogradouro("Rua alguma coisa");
		proposta.getBeneficiarios().getTitular().getEndereco().setBairro("Bairro");
		proposta.getBeneficiarios().getTitular().getEndereco().setCidade("RJ");
		proposta.getBeneficiarios().getTitular().getEndereco().setEstado("RJ");
		proposta.getBeneficiarios().getTitular().getEndereco().setNumero(123L);
		proposta.getBeneficiarios().getTitular().setNome("nome teste titular");
		proposta.getBeneficiarios().getTitular().setNomeMae("nome teste mae");
		proposta.getBeneficiarios().getTitular().setSexo("F");
		proposta.getBeneficiarios().getTitular().getTelefones().get(0).setDddEnumero("2198128465");
		proposta.getBeneficiarios().getTitular().getTelefones().get(0).setTipoTelefone("3");
		proposta.getBeneficiarios().getTitular().setEstadoCivil(new EstadoCivilVO());
		proposta.getBeneficiarios().getTitular().getEstadoCivil().setCodigo(1);

		proposta.getCanal().setCodigo(1);
		proposta.getCorretor().setCpfCnpj("01021899000191");
		proposta.getCorretor().setNome("BRADESCOR COR DE SEGUROS LTDA");
		proposta.getCorretor().setCpd(401462L);

		proposta.getCorretor().setAssistenteProducao(1234567L);
		proposta.setDataEmissao(new LocalDate("2017-07-28"));
		proposta.setDataValidadeProposta(new LocalDate("2017-10-26"));
		//		proposta.getPagamento().getCartaoCredito().setAccessToken("YWI5OWI2ZjYtOTY3YS00ZGQyLWE3YmMtYzY0ZjY5MGI2MGI3LTE5MTg1NTEwMT");
		//		proposta.getPagamento().getCartaoCredito().setBandeira("Master");
		//		proposta.getPagamento().getCartaoCredito().setNomeCartao("nome cartao");
		//		proposta.getPagamento().getCartaoCredito().setNumeroCartao("5197685977315654");
		//		proposta.getPagamento().getCartaoCredito().setPaymentToken("bb821519-901a-4969-b011-33ec0cf177f4");
		//		proposta.getPagamento().getCartaoCredito().setValidade("01/2020");
		proposta.getPagamento().setCodigoTipoCobranca(1);
		proposta.getPagamento().setFormaPagamento(1);
		proposta.getPlano().setCodigo(3L);
		proposta.setSucursalSelecionada(new SucursalVO());
		proposta.getSucursalSelecionada().setCpdSucursal("911;401462");

	}

	@Test
	public void deveSalvarRascunhoDePropostaComFormaDePagamentoBoleto() {

		assertTrue(propostaServiceFacade.salvarRascunhoProposta(proposta) > 0L);
	}

	@Test
	public void deveSalvarRascunhoDePropostaComFormaDePagamentoBoletoEDebito() {

		proposta.getPagamento().setCodigoTipoCobranca(2);
		proposta.getPagamento().setFormaPagamento(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO.getCodigo());
		proposta.getPagamento().getContaCorrente().setBanco(new BancoVO());
		proposta.getPagamento().getContaCorrente().getBanco().setCodigo(237);
		;
		proposta.getPagamento().getContaCorrente().setNumeroAgencia("2094");
		proposta.getPagamento().getContaCorrente().setNumeroConta("4158");
		proposta.getPagamento().getContaCorrente().setDigitoVerificadorConta("0");
		proposta.getPagamento().getContaCorrente().setCpf("47383283104");
		proposta.getPagamento().getContaCorrente().setNome("Proponente Teste");
		proposta.getPagamento().getContaCorrente().setDataNascimento(new LocalDate(1990, 10, 1));

		assertTrue(propostaServiceFacade.salvarRascunhoProposta(proposta) > 0L);
	}
	
	@Test(expected = br.com.bradseg.bsad.framework.core.exception.BusinessException.class)
	public void deveOcorrerErroAoImprimirPropostaVaziaSemAssistenteDeProducaoESemPlano()  throws Exception{

		try{
			PropostaVO propostaVazia = new PropostaVO();
			propostaVazia.setCorretor(proposta.getCorretor());
			propostaVazia.setSucursalSelecionada(proposta.getSucursalSelecionada());

			propostaServiceFacade.imprimirPropostaVazia(propostaVazia);
		
		} catch (br.com.bradseg.bsad.framework.core.exception.BusinessException e) {
			assertThat(e.getMessage(), containsString("msg.erro.campo.obrigatorio"));
			throw e;
		}

		
	}

	@Test
	public void deveSalvarRascunhoDePropostaComFormaDePagamentoDebito() {

		proposta.getPagamento().setCodigoTipoCobranca(1);
		proposta.getPagamento().setFormaPagamento(FormaPagamento.BOLETO_E_DEBITO_AUTOMATICO.getCodigo());
		proposta.getPagamento().getContaCorrente().setBanco(new BancoVO());
		proposta.getPagamento().getContaCorrente().getBanco().setCodigo(237);
		proposta.getPagamento().getContaCorrente().setNumeroAgencia("2094");
		proposta.getPagamento().getContaCorrente().setNumeroConta("4158");
		proposta.getPagamento().getContaCorrente().setDigitoVerificadorConta("0");
		proposta.getPagamento().getContaCorrente().setCpf("47383283104");
		proposta.getPagamento().getContaCorrente().setNome("Proponente Teste");
		proposta.getPagamento().getContaCorrente().setDataNascimento(new LocalDate(1990, 10, 1));

		assertTrue(propostaServiceFacade.salvarRascunhoProposta(proposta) > 0L);
	}

	@Test
	public void deveSalvarRascunhoDePropostaComFormaDePagamentoCartaoCredito() {

		
		proposta.getPagamento().setCodigoTipoCobranca(1);
		proposta.getPagamento().setFormaPagamento(FormaPagamento.CARTAO_CREDITO.getCodigo());
		proposta.getPagamento().getCartaoCredito().setNomeCartao("Nome Cartao");
		proposta.getPagamento().getCartaoCredito().setNumeroCartao("5420696111154924");
		proposta.getPagamento().getCartaoCredito().setValidade("09/2099");
		proposta.getPagamento().getCartaoCredito().setBandeira("Master");

		
		assertTrue(propostaServiceFacade.salvarRascunhoProposta(proposta) > 0L);
	}
	
	@Test(expected = br.com.bradseg.bsad.framework.core.exception.BusinessException.class)
	public void deveOcorrerErroAoSalvarRascunhoDePropostaComFormaDePagamentoCartaoCreditoSemNumeroCartao()  throws Exception{

		try{
			
			proposta.getPagamento().setCodigoTipoCobranca(1);
			proposta.getPagamento().setFormaPagamento(FormaPagamento.CARTAO_CREDITO.getCodigo());
			proposta.getPagamento().getCartaoCredito().setNomeCartao("Nome Cartao");
			proposta.getPagamento().getCartaoCredito().setNumeroCartao(null);
			proposta.getPagamento().getCartaoCredito().setValidade("09/2099");
			proposta.getPagamento().getCartaoCredito().setBandeira("Master");

			propostaServiceFacade.salvarRascunhoProposta(proposta);
		
		} catch (br.com.bradseg.bsad.framework.core.exception.BusinessException e) {
			assertThat(e.getMessage(), containsString("msg.erro.campo.obrigatorio"));
			throw e;
		}
		
	}

	@Test
	public void devePossuirDataDeEmissaoEmPropostaVazia() {
		PropostaVO propostaVazia = new PropostaVO();
		propostaVazia.setCorretor(proposta.getCorretor());
		propostaVazia.setSucursalSelecionada(proposta.getSucursalSelecionada());
		propostaVazia.setPlano(proposta.getPlano());
		Long sequencial = propostaServiceFacade.imprimirPropostaVazia(propostaVazia);

		assertTrue(sequencial != null);

		propostaVazia = propostaServiceFacade.obterPropostaPorSequencial(sequencial);
		assertTrue(propostaVazia != null);
		assertTrue(propostaVazia.getDataEmissao() != null);
	}

	@Test
	public void devePossuirDataDeInicioDeCobrancaAoFinalizarProposta() {

		Long sequencialProposta = propostaServiceFacade.finalizarProposta(proposta);
		assertTrue(sequencialProposta != null);
		br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaVO propostaServicoVO = propostaServiceFacade.obterPropostaDoServicoPorSeguencial(sequencialProposta);
		propostaServiceFacade.cancelarProposta(sequencialProposta);
		assertTrue(propostaServicoVO.getDataInicioCobranca() != null);
	}
	
	
	//@Test
	public void deveGerarPDFProposta(){
		proposta.setSequencial(52506L);
		byte[] relatorio = propostaServiceFacade.gerarPDFProposta(proposta);
		
		assertTrue(relatorio != null);
	}
	
	@Test
	public void deveAdicionarDependente(){
		Integer quantidadeDependentesEsperado = proposta.getBeneficiarios().getDependentes().size()+1;
		propostaServiceFacade.adicionarDependente(proposta);
		Integer quantidadeDependentesAposAdicao = proposta.getBeneficiarios().getDependentes().size();

		assertEquals(quantidadeDependentesEsperado, quantidadeDependentesAposAdicao);
	}

	
	
	
	@Test
	public void deveSalvarPropostaComSucursalMercado(){
		
		proposta.getSucursalSelecionada().setCpdSucursal("914;401462");
		proposta.getCorretor().setCpd(401462L);

		proposta.getBeneficiarios().getTitular().setCpf(geraCPF());
		
		Long cpdCorretor = proposta.getCorretor().getCpd();
		Long sequencialProposta = propostaServiceFacade.finalizarProposta(proposta);
		assertTrue(sequencialProposta != null);
		proposta = propostaServiceFacade.obterPropostaPorSequencial(sequencialProposta);
		assertEquals(cpdCorretor, proposta.getCorretor().getCpd());
		assertTrue(proposta.getCorretorMaster().getCpd() == null);
		
	}
	
	
	@Test
	public void deveSalvarPropostaComSucursalCorporate(){
		
		proposta.getSucursalSelecionada().setCpdSucursal("912;401462");
		proposta.getCorretor().setCpd(401462L);

		proposta.getBeneficiarios().getTitular().setCpf(geraCPF());
		
		Long cpdCorretor = proposta.getCorretor().getCpd();
		Long sequencialProposta = propostaServiceFacade.finalizarProposta(proposta);
		assertTrue(sequencialProposta != null);
		proposta = propostaServiceFacade.obterPropostaPorSequencial(sequencialProposta);
		assertEquals(cpdCorretor, proposta.getCorretor().getCpd());
		assertTrue(proposta.getCorretorMaster().getCpd() == null);
		
	}
	
	
	@Test
	public void deveSalvarPropostaComSucursalRede(){
		
		proposta.getSucursalSelecionada().setCpdSucursal("910;401458");
		proposta.getCorretor().setCpd(401458L);
		proposta.getCorretorMaster().setCpd(401458L);
		proposta.getCorretorMaster().setNome("ZQWF WHCF UQKQBFEF BQK UDJU MUCF");
		proposta.getCorretorMaster().setCpfCnpj("01021899000191");
		proposta.getCorretor().setAgenciaProdutora(20L);
		proposta.getBeneficiarios().getTitular().setCpf(geraCPF());
		
		Long cpdCorretorMaster = proposta.getCorretorMaster().getCpd();
		Long sequencialProposta = propostaServiceFacade.finalizarProposta(proposta);
		assertTrue(sequencialProposta != null);
		proposta = propostaServiceFacade.obterPropostaPorSequencial(sequencialProposta);
		assertEquals(cpdCorretorMaster, proposta.getCorretorMaster().getCpd());
		
	}
	
	@Test
	public void deveSalvarPropostaComSucursalRedeEMatriculaGerente(){
		
		proposta.getSucursalSelecionada().setCpdSucursal("910;401458");
		proposta.getCorretor().setCpd(401458L);
		proposta.getCorretor().setAgenciaProdutora(20L);
		proposta.setCodigoMatriculaGerente("5440");
		proposta.getBeneficiarios().getTitular().setCpf(geraCPF());
		
		String matriculaGerente = proposta.getCodigoMatriculaGerente();
		Long sequencialProposta = propostaServiceFacade.finalizarProposta(proposta);
		assertTrue(sequencialProposta != null);
		proposta = propostaServiceFacade.obterPropostaPorSequencial(sequencialProposta);
		assertEquals(matriculaGerente, proposta.getCodigoMatriculaGerente());
		
	}
	
	@Test(expected = BusinessException.class)
	public void deveRetornarMensagensDeErroAoFinalizarPropostaPreenchendoApenasSucursal(){
		
		proposta = new PropostaVO();
		proposta.getCanal().setCodigo(1);
		proposta.getCorretor().setCpfCnpj("01021899000191");
		proposta.getCorretor().setNome("BRADESCOR COR DE SEGUROS LTDA");
		proposta.getCorretor().setCpd(401462L);
		proposta.getSucursalSelecionada().setCpdSucursal("910;401458");
		propostaServiceFacade.finalizarProposta(proposta);

	}
	
	@Test
	public void deveSalvarPropostaVaziaComSucursalCorporate(){
		
		proposta = new PropostaVO();
		proposta.getCanal().setCodigo(1);
		proposta.getCorretor().setCpfCnpj("01021899000191");
		proposta.getCorretor().setNome("BRADESCOR COR DE SEGUROS LTDA");
		proposta.getCorretor().setCpd(401462L);
		proposta.getSucursalSelecionada().setCpdSucursal("912;401462");
		proposta.getCorretor().setAssistenteProducao(1234567L);
		proposta.getPlano().setCodigo(3L);
		
		Long sequencial = propostaServiceFacade.imprimirPropostaVazia(proposta);
		
		proposta = propostaServiceFacade.obterPropostaPorSequencial(sequencial);
		
		assertEquals(912, proposta.getCorretor().getSucursal().intValue());

	}
	
	/**
	 * <p>Cenario:</p> 
	 * <ol>
	 * 	<li>Salvar rascunho de proposta com forma de pagamento de cartao de credito.</li>
	 * 	<li>Chamar MIP para validar dados e debitar primeira parcela de cartao.</li>
	 *  <li>Status de proposta deve permanecer em rascunho.</li>
	 * </ol>
	 */
	@Test
	@Ignore
	public void deveManterEmRascunhoPropostaComRetornoInvalidoDoMIP(){
		proposta.getPagamento().setFormaPagamento(5);
		proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
		proposta.getPagamento().getCartaoCredito().setBandeira("Master");
		proposta.getPagamento().getCartaoCredito().setNomeCartao("nome cartao");
		proposta.getPagamento().getCartaoCredito().setNumeroCartao("5197685977315654");
		proposta.getPagamento().getCartaoCredito().setPaymentToken("bb821519-901a-4969-b011-33ec0cf177f4");
		proposta.getPagamento().getCartaoCredito().setValidade("01/2020");
		
		proposta.setSequencial(propostaServiceFacade.salvarRascunhoProposta(proposta));
		assertTrue(proposta.getSequencial() != null);
				
		PropostaServiceFacade propostaServiceFacadeMock = mock(PropostaServiceFacade.class);
		when(propostaServiceFacadeMock.validarDadosCartaoCredito(proposta)).thenReturn(false);
		
		assertEquals(SituacaoProposta.RASCUNHO.name(), proposta.getStatus());
	}
	
	@Test
	@Ignore
	public void deveFinalizarPropostaComRetornoPositivoDoMIP(){
		
		proposta.getPagamento().setFormaPagamento(5);
		
		String accessToken = odontoprevServiceFacade.obterAccessToken();
		
		proposta.getPagamento().getCartaoCredito().setAccessToken(accessToken);
		proposta.getPagamento().getCartaoCredito().setBandeira("Master");
		proposta.getPagamento().getCartaoCredito().setNomeCartao("nome cartao");
		proposta.getPagamento().getCartaoCredito().setNumeroCartao("5197685977315654");
		proposta.getPagamento().getCartaoCredito().setPaymentToken("bb821519-901a-4969-b011-33ec0cf177f4");
		proposta.getPagamento().getCartaoCredito().setValidade("01/2020");
		
		proposta.setSequencial(propostaServiceFacade.salvarRascunhoProposta(proposta));
		assertTrue(proposta.getSequencial() != null);
		Long sequencialProposta = proposta.getSequencial();
		
		boolean chamadaMIPSucesso = propostaServiceFacade.validarDadosCartaoCredito(proposta);		
		assertTrue(chamadaMIPSucesso);
		
		proposta.setSequencial(propostaServiceFacade.finalizarProposta(proposta));
		assertEquals(sequencialProposta, proposta.getSequencial());
	}

	@Test
	@Ignore
	public void deveConseguirFinalizarPropostaAposAcertoDadosCartao(){
	proposta.getPagamento().setFormaPagamento(5);
		
		String accessToken = odontoprevServiceFacade.obterAccessToken();
		proposta.getPagamento().setFormaPagamento(5);
		proposta.getPagamento().getCartaoCredito().setAccessToken(accessToken);
		proposta.getPagamento().getCartaoCredito().setBandeira("Master");
		proposta.getPagamento().getCartaoCredito().setNomeCartao("nome cartao");
		proposta.getPagamento().getCartaoCredito().setNumeroCartao("5197685977315654");
		proposta.getPagamento().getCartaoCredito().setPaymentToken("bb821519-901a-4969-b011-33ec0cf177f4");
		proposta.getPagamento().getCartaoCredito().setValidade("01/2010");
		
		proposta.setSequencial(propostaServiceFacade.salvarRascunhoProposta(proposta));
		assertTrue(proposta.getSequencial() != null);
		Long sequencialProposta = proposta.getSequencial();
		
		boolean chamadaMIPSucesso = propostaServiceFacade.validarDadosCartaoCredito(proposta);		
		assertTrue(chamadaMIPSucesso == false);
		proposta.getPagamento().getCartaoCredito().setValidade("01/2020");
		
		chamadaMIPSucesso = propostaServiceFacade.validarDadosCartaoCredito(proposta);		
		assertTrue(chamadaMIPSucesso);
	
		proposta.setSequencial(propostaServiceFacade.finalizarProposta(proposta));
		assertEquals(sequencialProposta, proposta.getSequencial());
		
	}
}
