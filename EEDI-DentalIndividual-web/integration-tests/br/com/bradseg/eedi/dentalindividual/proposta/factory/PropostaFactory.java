package br.com.bradseg.eedi.dentalindividual.proposta.factory;

import org.joda.time.LocalDate;

import br.com.bradseg.eedi.dentalindividual.vo.EnderecoVO;
import br.com.bradseg.eedi.dentalindividual.vo.EstadoCivilVO;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.SucursalVO;

public class PropostaFactory {

	private PropostaVO proposta;
	
	public PropostaFactory() {
		proposta = new PropostaVO();
	}
	
	public PropostaVO criarPropostaComFormaPagamentoCartaoCredito(){
		
		proposta = new PropostaVO();
		proposta.getBeneficiarios().getTitular().setCpf("26484383359");
		proposta.getBeneficiarios().getTitular().setDataNascimento(new LocalDate("1990-01-01"));
		proposta.getBeneficiarios().getTitular().setEmail("email@email.com");
		proposta.getBeneficiarios().getTitular().setEndereco(new EnderecoVO());
		proposta.getBeneficiarios().getTitular().getEndereco().setCep("20091007");
		proposta.getBeneficiarios().getTitular().getEndereco().setLogradouro("Rua alguma coisa");
		proposta.getBeneficiarios().getTitular().getEndereco().setBairro("Bairro");
		proposta.getBeneficiarios().getTitular().getEndereco().setCidade("RJ");
		proposta.getBeneficiarios().getTitular().getEndereco().setEstado("RJ");
		proposta.getBeneficiarios().getTitular().getEndereco().setNumero(123L);
		proposta.getBeneficiarios().getTitular().setNome("nome teste titular");
		proposta.getBeneficiarios().getTitular().setNomeMae("nome teste mae");
		proposta.getBeneficiarios().getTitular().setSexo("F");
		proposta.getBeneficiarios().getTitular().getTelefones().get(0).setDddEnumero("2198128465");
		proposta.getBeneficiarios().getTitular().getTelefones().get(0).setTipoTelefone("3");
		proposta.getBeneficiarios().getTitular().setEstadoCivil(new EstadoCivilVO());
		proposta.getBeneficiarios().getTitular().getEstadoCivil().setCodigo(1);

		proposta.getCanal().setCodigo(1);
		proposta.getCorretor().setCpfCnpj("01021899000191");
		proposta.getCorretor().setNome("BRADESCOR COR DE SEGUROS LTDA");
		proposta.getCorretor().setCpd(401462L);

		proposta.getCorretor().setAssistenteProducao(1234567L);
		proposta.setDataEmissao(new LocalDate("2017-07-28"));
		proposta.setDataValidadeProposta(new LocalDate("2017-10-26"));

		proposta.getPagamento().setFormaPagamento(5);
		proposta.getPagamento().getCartaoCredito().setAccessToken(null);
		proposta.getPagamento().getCartaoCredito().setBandeira("Master");
		proposta.getPagamento().getCartaoCredito().setNomeCartao("nome cartao");
		proposta.getPagamento().getCartaoCredito().setNumeroCartao("5197685977315654");
		proposta.getPagamento().getCartaoCredito().setPaymentToken("bb821519-901a-4969-b011-33ec0cf177f4");
		proposta.getPagamento().getCartaoCredito().setValidade("01/2010");

		proposta.getPagamento().setCodigoTipoCobranca(1);
		proposta.getPagamento().setFormaPagamento(1);
		proposta.getPlano().setCodigo(3L);
		proposta.setSucursalSelecionada(new SucursalVO());
		proposta.getSucursalSelecionada().setCpdSucursal("911;401462");

		return proposta;
	}
}
