package br.com.bradseg.eedi.dentalindividual.odontoprev.facade;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.bradseg.eedi.dentalindividual.odontoprev.vo.RetornoVO;
import br.com.bradseg.eedi.dentalindividual.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.dentalindividual.proposta.factory.PropostaFactory;
import br.com.bradseg.eedi.dentalindividual.spring.IntegrationTest;
import br.com.bradseg.eedi.dentalindividual.util.Strings;
import br.com.bradseg.eedi.dentalindividual.vo.EnderecoVO;
import br.com.bradseg.eedi.dentalindividual.vo.EstadoCivilVO;
import br.com.bradseg.eedi.dentalindividual.vo.PropostaVO;
import br.com.bradseg.eedi.dentalindividual.vo.SucursalVO;
import br.com.bradseg.eedi.dentalindividual.vo.ValorPlanoVO;
import static  org.junit.Assert.*;

public class OdontoprevServiceFacadeTest extends IntegrationTest {

	private PropostaVO proposta;
	@Autowired
	private OdontoprevServiceFacade odontoprevServiceFacade;
	
	private PropostaServiceFacade propostaServiceFacade;
	@Before
	public void setUp() { 

		proposta = new PropostaVO();
		proposta.getBeneficiarios().getTitular().setCpf("26484383359");
		proposta.getBeneficiarios().getTitular().setDataNascimento(new LocalDate("1990-01-01"));
		proposta.getBeneficiarios().getTitular().setEmail("email@email.com");
		proposta.getBeneficiarios().getTitular().setEndereco(new EnderecoVO());
		proposta.getBeneficiarios().getTitular().getEndereco().setCep("20091007");
		proposta.getBeneficiarios().getTitular().getEndereco().setLogradouro("Rua alguma coisa");
		proposta.getBeneficiarios().getTitular().getEndereco().setBairro("Bairro");
		proposta.getBeneficiarios().getTitular().getEndereco().setCidade("RJ");
		proposta.getBeneficiarios().getTitular().getEndereco().setEstado("RJ");
		proposta.getBeneficiarios().getTitular().getEndereco().setNumero(123L);
		proposta.getBeneficiarios().getTitular().setNome("nome teste titular");
		proposta.getBeneficiarios().getTitular().setNomeMae("nome teste mae");
		proposta.getBeneficiarios().getTitular().setSexo("F");
		proposta.getBeneficiarios().getTitular().getTelefones().get(0).setDddEnumero("2198128465");
		proposta.getBeneficiarios().getTitular().getTelefones().get(0).setTipoTelefone("3");
		proposta.getBeneficiarios().getTitular().setEstadoCivil(new EstadoCivilVO());
		proposta.getBeneficiarios().getTitular().getEstadoCivil().setCodigo(1);

		proposta.getCanal().setCodigo(1);
		proposta.getCorretor().setCpfCnpj("01021899000191");
		proposta.getCorretor().setNome("BRADESCOR COR DE SEGUROS LTDA");
		proposta.getCorretor().setCpd(401462L);

		proposta.getCorretor().setAssistenteProducao(1234567L);
		proposta.setDataEmissao(new LocalDate("2017-07-28"));
		proposta.setDataValidadeProposta(new LocalDate("2017-10-26"));
		proposta.getPagamento().getCartaoCredito().setAccessToken("YWI5OWI2ZjYtOTY3YS00ZGQyLWE3YmMtYzY0ZjY5MGI2MGI3LTE5MTg1NTEwMT");
		proposta.getPagamento().getCartaoCredito().setBandeira("Master");
		proposta.getPagamento().getCartaoCredito().setNomeCartao("nome cartao");
		proposta.getPagamento().getCartaoCredito().setNumeroCartao("5197685977315654");
		proposta.getPagamento().getCartaoCredito().setCpfCartao("47383283104");
		proposta.getPagamento().getCartaoCredito().setPaymentToken("bb821519-901a-4969-b011-33ec0cf177f4");
		proposta.getPagamento().getCartaoCredito().setValidade("01/2020");
		proposta.getPagamento().setCodigoTipoCobranca(1);
		proposta.getPagamento().setFormaPagamento(5);
		
		proposta.getPlano().setCodigo(3L);
		proposta.getPlano().getValorPlanoVO().setValorMensalTitular(45.90D);
		proposta.getPlano().getValorPlanoVO().setValorMensalDependente(45.90D);
		proposta.getPlano().getValorPlanoVO().setValorAnualTitular(459.90D);
		proposta.getPlano().getValorPlanoVO().setValorAnualDependente(459.90D);
		proposta.setSucursalSelecionada(new SucursalVO());
		proposta.getSucursalSelecionada().setCpdSucursal("911;401462");
		
		proposta.setSequencial(52506L);
		proposta.setCodigo("BDA000000525060");

	}
	

	
	//@Test
	public void deveRetornarListaDeBandeiras(){
		List<String> listarBandeiras = odontoprevServiceFacade.listarBandeiras();
		assertTrue(!listarBandeiras.isEmpty());
		
		for(String bandeiras : listarBandeiras){
			assertTrue(StringUtils.isNotEmpty(bandeiras));
		}
	}
	
	//@Test
	public void deveVerificarCorretorHabilitado(){
		assertTrue(odontoprevServiceFacade.verificarCorretorHabilitado("01021899000191"));
	}
	
	//@Test
	public void deveGerarAcessToken(){
		String tokenDeAcesso = odontoprevServiceFacade.obterAccessToken();
		assertTrue(StringUtils.isNotEmpty(tokenDeAcesso));
	}
	
//	@Test
	public void deveDebitarPrimeiraParcelaCartaoDeCredito(){
		RetornoVO retorno = odontoprevServiceFacade.debitarPrimeiraParcelaCartaoDeCredito(proposta);
		assertTrue(retorno != null);
		assertEquals(1, retorno.getSucesso());
	}	
	
//	@Test
	public void deveValidarDadosCartaoDeCredito(){
		RetornoVO retorno = odontoprevServiceFacade.validarDadosCartaoCredito(proposta);
		assertTrue(retorno != null);
		assertEquals(1, retorno.getSucesso());
	}

	//@Test
	public void deveCancelarDebitoPrimeiraParcelaCartaoDeCredito(){
		PropostaVO proposta = new PropostaFactory().criarPropostaComFormaPagamentoCartaoCredito();
		proposta.getPagamento().getCartaoCredito().setAccessToken(odontoprevServiceFacade.obterAccessToken());
		odontoprevServiceFacade.cancelarDebitoPrimeiraParcelaCartaoDeCredito(proposta);
	
		assertTrue(proposta != null);
	}



	private PropostaVO preencherPropostaComFormaDePagamentoCartaoCredito() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
