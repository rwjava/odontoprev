package br.com.bradseg.eedi.administrativo.plano.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.administrativo.rowmapper.HistoricoCanalVendasRowMapper;
import br.com.bradseg.eedi.administrativo.rowmapper.HistoricoPlanoRowMapper;
import br.com.bradseg.eedi.administrativo.rowmapper.HistoricoTodosPlanosRowMapper;
import br.com.bradseg.eedi.administrativo.rowmapper.ValorPlanoRowMapper;
import br.com.bradseg.eedi.administrativo.support.Constantes;
import br.com.bradseg.eedi.administrativo.support.DateUtil;
import br.com.bradseg.eedi.administrativo.vo.FiltroHistoricoVO;
import br.com.bradseg.eedi.administrativo.vo.HistoricoCanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.HistoricoPlanoVO;
import br.com.bradseg.eedi.administrativo.vo.ValorPlanoVO;

@Repository
public class HistoricoDaoImpl extends JdbcDao  implements HistoricoDao{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HistoricoDaoImpl.class);

	@Autowired
	private DataSource dataSource;
	
	
	
	private static final String LISTAR_HISTORICO_CANAL_VENDA = new StringBuilder()
	
		.append(" SELECT                                      \n ")
		.append(" CCANAL_VDA_DNTAL_INDVD,                     \n ")
		.append(" DINCL_REG_TBELA,                           \n ")
		.append(" CSGL_CANAL_VDA_DNTAL_INDVD,                \n ")
		.append(" RCANAL_VDA_DNTAL_INDVD,                    \n ")
		.append(" DDSATV_CANAL_VDA_DNTAL_INDVD,                \n ")
		.append(" CRESP_ULT_ATULZ                             \n ")
		.append(" FROM                                        \n ")
		.append(" DBPROD.HIST_CANAL_VDA_DNTAL_INDVD   \n ")
		.append("  where CCANAL_VDA_DNTAL_INDVD = :codigo     \n ") 
		.append(" ORDER BY DINCL_REG_TBELA   DESC            \n ")
		.toString();
	
	
	private static final String LISTAR_TODOS_PLANOS = new StringBuilder()
	
		.append(" SELECT                           \n")
		.append(" CPLANO_DNTAL_INDVD,              \n")
		.append(" RPLANO_DNTAL_INDVD,              \n")
		.append(" CREG_PLANO_DNTAL,                \n")
		.append(" CRESP_ULT_ATULZ,                 \n")
		.append(" DINIC_PLANO_INDVD,               \n")
		.append(" DFIM_VGCIA,                      \n")
		.append(" RPER_CAREN_PLANO_ANO,            \n")
		.append(" RPER_CAREN_PLANO_MES,            \n")
		.append(" DULT_ATULZ_REG                  \n")
		.append(" FROM                             \n")
		.append(" DBPROD.PLANO_DNTAL_INDVD        \n")
		.toString();
	
	
	private static final String LISTAR_HISTORICO_PLANO = new StringBuilder()                       
	
		.append(" SELECT                                \n")
		.append(" CPLANO_DNTAL_INDVD,                   \n")
		.append(" DINCL_REG_TBELA,                     \n")
		.append(" RPLANO_DNTAL_INDVD,                   \n")
		.append(" CREG_PLANO_DNTAL,                     \n")
		.append(" CRESP_ULT_ATULZ,                      \n")
		.append(" DINIC_VGCIA,                          \n")
		.append(" DFIM_VGCIA,                           \n")
		.append(" RPER_CAREN_PLANO_ANO,                 \n")
		.append(" RPER_CAREN_PLANO_MES                  \n")
		.append(" FROM                                  \n")
		.append(" DBPROD.HIST_PLANO_DNTAL_INDVD \n")
		.append("  where CPLANO_DNTAL_INDVD = :codigo     \n ")
		.append(" ORDER BY DINCL_REG_TBELA                      \n")
		.toString();
	
	
	private static final String LISTAR_TODOS_HISTORICO_PLANO = new StringBuilder()                       
	
		.append(" SELECT                                \n")
		.append(" CPLANO_DNTAL_INDVD,                   \n")
		.append(" DINCL_REG_TBELA,                     \n")
		.append(" RPLANO_DNTAL_INDVD,                   \n")
		.append(" CREG_PLANO_DNTAL,                     \n")
		.append(" CRESP_ULT_ATULZ,                      \n")
		.append(" DINIC_VGCIA,                          \n")
		.append(" DFIM_VGCIA,                           \n")
		.append(" RPER_CAREN_PLANO_ANO,                 \n")
		.append(" RPER_CAREN_PLANO_MES                  \n")
		.append(" FROM                                  \n")
		.append(" DBPROD.HIST_PLANO_DNTAL_INDVD \n")		
		.toString();
	
	private static final String LISTAR_HISTORICO_CANAL_DATA_ANTERIOR = new StringBuilder()
	
		.append(" SELECT                                           \n")
		.append(" canalPlano.CCANAL_VDA_DNTAL_INDVD,               \n")
		.append(" canalPlano.CPLANO_DNTAL_INDVD,                   \n")
		.append(" canalPlano.CRESP_ULT_ATULZ,                      \n")
		.append(" canalPlano.DULT_ATULZ_REG,                       \n")
		.append(" canal.RCANAL_VDA_DNTAL_INDVD,                    \n")
		.append(" canalPlano.DINIC_VGCIA,                          \n")
		.append(" canalPlano.DFIM_VGCIA                            \n")
		.append(" FROM                                             \n")
		.append(" DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD canalPlano,   \n")
		.append(" DBPROD.CANAL_VDA_DNTAL_INDVD canal               \n")
		.append(" where canalPlano.CCANAL_VDA_DNTAL_INDVD = canal.CCANAL_VDA_DNTAL_INDVD and canalPlano.CPLANO_DNTAL_INDVD =:codigoPlano and canalPlano.DULT_ATULZ_REG < :dataAlteracao     \n")
		.append(" FETCH FIRST 1 ROWS ONLY                          \n")
	    .toString();
	
	private static final String LISTAR_HISTORICO_CANAL_PLANO_DATA = new StringBuilder()
	
		.append(" SELECT                                                 \n")
		.append("     canalPlano.CCANAL_VDA_DNTAL_INDVD,                 \n") 
		.append("     canalPlano.CPLANO_DNTAL_INDVD,                     \n")
		.append("     canalPlano.CRESP_ULT_ATULZ,                        \n")
		.append("     canalPlano.DULT_ATULZ_REG,                         \n")
		.append("     canal.RCANAL_VDA_DNTAL_INDVD,                      \n") 
		.append("      canalPlano.DINIC_VGCIA,                           \n")
		.append("     canalPlano.DFIM_VGCIA                              \n")
		.append(" FROM                                                   \n")
		.append("     DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD canalPlano,     \n")
		.append("     DBPROD.CANAL_VDA_DNTAL_INDVD canal                 \n")
		.append(" where canalPlano.CCANAL_VDA_DNTAL_INDVD = canal.CCANAL_VDA_DNTAL_INDVD  \n")  
		.append(" and canalPlano.CPLANO_DNTAL_INDVD =:codigoPlano and    \n")
		.append(" day(canalPlano.DULT_ATULZ_REG) = :dia and    \n")
		.append(" month(canalPlano.DULT_ATULZ_REG) = :mes and    \n")
		.append(" year(canalPlano.DULT_ATULZ_REG) = :ano and    \n")
		.append(" hour(canalPlano.DULT_ATULZ_REG) = :hora and    \n")
		.append(" minute(canalPlano.DULT_ATULZ_REG) = :minuto     \n")		
		.toString();
	
	private static final String LISTAR_HISTORICO_USUARIO = new StringBuilder()                       
	
		.append(" SELECT                                \n")	
		.append(" distinct(CRESP_ULT_ATULZ)             \n")	
		.append(" FROM                                  \n")
		.append(" DBPROD.HIST_PLANO_DNTAL_INDVD \n")
		.toString();
	
	private static final String LISTAR_VALORES_POR_DATA = new StringBuilder()

		.append(" SELECT                            \n")
		.append(" NSEQ_VLR_PLANO_DNTAL,             \n")
		.append(" CPLANO_DNTAL_INDVD,               \n")
		.append(" VANUDD_PLANO_DNTAL_TTLAR,         \n")
		.append(" VANUDD_PLANO_DNTAL_DEPDT,         \n")
		.append(" VMESD_PLANO_TTLAR,                \n")
		.append(" VMESD_PLANO_DEPDT,                \n")
		.append(" VDESC_PLANO_DNTAL,                \n")
		.append(" VTX_PLANO_INDVD,                  \n")
		.append(" DINIC_VGCIA,                      \n")
		.append(" DFIM_VGCIA,                       \n")
		.append(" DINCL_REG_TBELA,					\n")
		.append(" CRESP_ULT_ATULZ					\n")
		.append(" FROM                              \n")
		.append(" DBPROD.VLR_PLANO_DNTAL_INDVD      \n")
		.append(" WHERE                             \n")
		.append(" CPLANO_DNTAL_INDVD =  :codigoPlano and  \n")
		.append(" day(DINCL_REG_TBELA) = :dia and    \n")
		.append(" month(DINCL_REG_TBELA) = :mes and    \n")
		.append(" year(DINCL_REG_TBELA) = :ano and    \n")
		.append(" hour(DINCL_REG_TBELA) = :hora and    \n")
		.append(" minute(DINCL_REG_TBELA) = :minuto     \n")		
		.toString();

	private static final String LISTAR_VALORES_POR_DATA_MENOR = new StringBuilder()
		.append(" SELECT                            \n")
		.append(" NSEQ_VLR_PLANO_DNTAL,             \n")
		.append(" CPLANO_DNTAL_INDVD,               \n")
		.append(" VANUDD_PLANO_DNTAL_TTLAR,         \n")
		.append(" VANUDD_PLANO_DNTAL_DEPDT,         \n")
		.append(" VMESD_PLANO_TTLAR,                \n")
		.append(" VMESD_PLANO_DEPDT,                \n")
		.append(" VDESC_PLANO_DNTAL,                \n")
		.append(" VTX_PLANO_INDVD,                  \n")
		.append(" DINIC_VGCIA,                      \n")
		.append(" DFIM_VGCIA,                       \n")
		.append(" DINCL_REG_TBELA,					\n")
		.append(" CRESP_ULT_ATULZ					\n")
		.append(" FROM                              \n")
		.append(" DBPROD.VLR_PLANO_DNTAL_INDVD      \n")
		.append(" WHERE                             \n")
		.append(" DINCL_REG_TBELA > :dataAlteracao   \n")
		.append(" and CPLANO_DNTAL_INDVD =  :codigoPlano   \n")
		.toString();
		
	
	@Override
	public ValorPlanoVO listarValoresPorData(HistoricoPlanoVO historicoPlano) {
		
		ValorPlanoVO valorPlano = null; 
		
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(historicoPlano.getDataAlteracao());
		int dia = calendar.get(Calendar.DAY_OF_MONTH);
		int mes =  calendar.get(Calendar.MONTH) + 1;
		int ano =  calendar.get(Calendar.YEAR);
		int hora =  calendar.get(Calendar.HOUR_OF_DAY);
		int minuto = calendar.get(Calendar.MINUTE);
		int segundo =  calendar.get(Calendar.SECOND);
		
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPlano", historicoPlano.getCodigoPlano());	 
		param.addValue("dataAlteracao", historicoPlano.getDataAlteracao());
		param.addValue("dia", dia);
		param.addValue("mes", mes);
		param.addValue("ano", ano);
		param.addValue("hora", hora);
		param.addValue("minuto", minuto);
		param.addValue("segundo", segundo);
		
		try{
			valorPlano = getJdbcTemplate().queryForObject(LISTAR_VALORES_POR_DATA,param ,new ValorPlanoRowMapper());
		}catch(EmptyResultDataAccessException e){
			LOGGER.error(e.getMessage());
		}
  
		return valorPlano;
	}
	
	@Override
	public ValorPlanoVO listaValorPorMenorData(HistoricoPlanoVO historicoPlano) {
		
		ValorPlanoVO valorPlano = null; 
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPlano", historicoPlano.getCodigoPlano());	 
		param.addValue("dataAlteracao", historicoPlano.getDataAlteracao());
		
		try{
			valorPlano = getJdbcTemplate().queryForObject(LISTAR_VALORES_POR_DATA_MENOR,param ,new ValorPlanoRowMapper());
		}catch(EmptyResultDataAccessException e){
			LOGGER.error(e.getMessage());
		}
  
		return valorPlano;		
  
		
	}
	
	@Override
	public List<HistoricoCanalVendaVO> listarHistoricoCanalVendaPorCodigo(Integer codigo) {
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigo", codigo);
		
		List<HistoricoCanalVendaVO> historicoVenda =   getJdbcTemplate().query(LISTAR_HISTORICO_CANAL_VENDA,param ,new HistoricoCanalVendasRowMapper());

		return historicoVenda;
	}
	
	@Override
	public List<HistoricoCanalVendaVO> listarHistoricoPorData(Date dataAlteracao, Integer codigoPlano) {
		
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(dataAlteracao);
		int dia = calendar.get(Calendar.DAY_OF_MONTH);
		int mes =  calendar.get(Calendar.MONTH) + 1;
		int ano =  calendar.get(Calendar.YEAR);
		int hora =  calendar.get(Calendar.HOUR_OF_DAY);
		int minuto = calendar.get(Calendar.MINUTE);
		int segundo =  calendar.get(Calendar.SECOND);
		
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPlano", codigoPlano);	 
		param.addValue("dataAlteracao", dataAlteracao);
		param.addValue("dia", dia);
		param.addValue("mes", mes);
		param.addValue("ano", ano);
		param.addValue("hora", hora);
		param.addValue("minuto", minuto);
		param.addValue("segundo", segundo);
		
		//adicionarDatasBetween(dataAlteracao, dataAlteracao, param);		
  
		return getJdbcTemplate().query(LISTAR_HISTORICO_CANAL_PLANO_DATA,param ,new HistoricoCanalVendasRowMapper());
	}
	
	@Override
	public HistoricoCanalVendaVO listarHistoricoMenorData(Date dataAlteracao, Integer codigoPlano) {
		
				
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("dataAlteracao", dataAlteracao);	
		param.addValue("codigoPlano", codigoPlano);	

		
		HistoricoCanalVendaVO historicoCanalVendaVO = null;
		
		try{
			historicoCanalVendaVO = getJdbcTemplate().queryForObject(LISTAR_HISTORICO_CANAL_DATA_ANTERIOR,param ,new HistoricoCanalVendasRowMapper());
		}catch(EmptyResultDataAccessException e ){
			LOGGER.error(e.getMessage());
		}

		return historicoCanalVendaVO;
	}
	
	
	@Override
	public List<HistoricoPlanoVO> listarHistoricoPlanoPorCodigo(Long codigo) {
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigo", codigo);
		
		return  getJdbcTemplate().query(LISTAR_HISTORICO_PLANO,param ,new HistoricoPlanoRowMapper());

		
	}	
	
	
	@Override
	public List<HistoricoPlanoVO> consultarHistorico(FiltroHistoricoVO filtro) {
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("responsavel", filtro.getResponsavel());
		StringBuilder sb = new StringBuilder();
		sb.append(LISTAR_TODOS_HISTORICO_PLANO);
		
		
		
		 if(StringUtils.isBlank(filtro.getResponsavel()) || Constantes.ZERO.equals(filtro.getResponsavel()) && (filtro.getDataInicioPeriodo() != null || filtro.getDataFimPeriodo() != null)){
			
				sb.append("  where DINCL_REG_TBELA  BETWEEN  :DATA_INICIO  and  :DATA_FIM     \n ");
				sb.append(" ORDER BY DINCL_REG_TBELA              \n");
				adicionarDatasBetween(filtro.getDataInicioPeriodo(), filtro.getDataFimPeriodo(), param);
		
		 }else if(StringUtils.isNotBlank(filtro.getResponsavel()) && !Constantes.ZERO.equals(filtro.getResponsavel()) && (filtro.getDataInicioPeriodo() == null || filtro.getDataFimPeriodo() == null)){
			 	sb.append("  where CRESP_ULT_ATULZ = :responsavel     \n ");
			 	sb.append(" ORDER BY DINCL_REG_TBELA              \n");
			 
		 }else if(StringUtils.isNotBlank(filtro.getResponsavel()) &&  !Constantes.ZERO.equals(filtro.getResponsavel()) && (filtro.getDataInicioPeriodo() != null || filtro.getDataFimPeriodo() != null)){
			 	sb.append("  where CRESP_ULT_ATULZ = :responsavel  and DINCL_REG_TBELA  BETWEEN  :DATA_INICIO  and  :DATA_FIM \n ");
			 	sb.append(" ORDER BY DINCL_REG_TBELA              \n");
			 	adicionarDatasBetween(filtro.getDataInicioPeriodo(), filtro.getDataFimPeriodo(), param);
			 
		 }
		 
				
		return  getJdbcTemplate().query(sb.toString(),param ,new HistoricoPlanoRowMapper());

		
	}	
	
	@Override
	public List<HistoricoPlanoVO> listarHistoricoPlano() {
		
		StringBuilder sb = new StringBuilder();
		sb.append(LISTAR_TODOS_HISTORICO_PLANO);
		sb.append(" ORDER BY DINCL_REG_TBELA                      \n");
		
		return  getJdbcTemplate().query(sb.toString(),new MapSqlParameterSource() ,new HistoricoPlanoRowMapper());	
		
	}	
	
	@Override
	public HistoricoPlanoVO listarHistoricoPlanoMenorData(HistoricoPlanoVO historicoPlano) {
		
		
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("dataAlteracao", historicoPlano.getDataAlteracao());
		param.addValue("codigoPlano", historicoPlano.getCodigoPlano());
		
		StringBuilder sb = new StringBuilder();
		sb.append(LISTAR_TODOS_HISTORICO_PLANO);
		sb.append(" where CPLANO_DNTAL_INDVD = :codigoPlano and DINCL_REG_TBELA < :dataAlteracao  \n");
		sb.append(" ORDER BY DINCL_REG_TBELA DESC                \n");
		sb.append(" FETCH FIRST 1 ROWS ONLY                      \n");
		
		HistoricoPlanoVO hisorico = null; 
		try{
			hisorico = getJdbcTemplate().queryForObject(sb.toString(), param ,new HistoricoPlanoRowMapper());
		}catch(EmptyResultDataAccessException e){
			LOGGER.error(e.getMessage());
		}
		
		return	hisorico;
		
	}	
	
	
	public List<String> listarUsuarios (){ //LISTAR_HISTORICO_USUARIO
		
		
		List<String> lista = new ArrayList<String>();
		
		try{			
			
		    lista = getJdbcTemplate().query(LISTAR_HISTORICO_USUARIO, new MapSqlParameterSource(), new RowMapper<String>(){
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					return StringUtils.trim(StringUtils.trim(rs.getString("CRESP_ULT_ATULZ")));
				}
		    });		
			
		}catch(EmptyResultDataAccessException e){
			LOGGER.error( String.format("INFO: A lista de Usu�rios. ") );
		}catch (Exception e){
			LOGGER.error( String.format("INFO: Ocorreu um erro de conex�o.") );
		}
		
		return lista;		
		
	}
	
	@Override
	public List<HistoricoPlanoVO>listarTodosPlanos() {
		
		List<HistoricoPlanoVO> historicoPlano =   getJdbcTemplate().query(LISTAR_TODOS_PLANOS,new MapSqlParameterSource() ,new HistoricoTodosPlanosRowMapper());

		return historicoPlano;
	}
	
	
	@Override
	public DataSource getDataSource() {
		// TODO Auto-generated method stub
		return dataSource;
	} 
	
	private DateUtil tratarDataInicial(Date dataReferencia ) {
		DateUtil ini = new DateUtil(dataReferencia);
		ini.clearTimeInfo();
		return ini;
	}
	private DateUtil tratarDataFinal(Date dataReferencia ) {
		DateUtil fim = new DateUtil(dataReferencia);
		fim.lastTimeInfo();
		return fim;
	}
	
//	private void adicionarDatasBetween(Date dataReferencia,  MapSqlParameterSource params) {
//		adicionarDatasBetween(dataReferencia, dataReferencia, params);
//	}

	private void adicionarDatasBetween(Date dataInicio, Date dataFinal,  MapSqlParameterSource params) {
		// Preencher parametros...
		params.addValue("DATA_INICIO", tratarDataInicial(dataInicio));
		params.addValue("DATA_FIM",  tratarDataFinal(dataFinal));
	}
	
	

}
