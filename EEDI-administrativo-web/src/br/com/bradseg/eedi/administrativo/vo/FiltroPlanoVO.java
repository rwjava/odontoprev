package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

import br.com.bradseg.eedi.administrativo.support.Strings;

/**
 * Filtro para consulta dos planos.
 * 
 * @author WDEV
 *
 */
public class FiltroPlanoVO implements Serializable {

	private static final long serialVersionUID = -1861546701806459408L;
	
	private LocalDate dataInicioVigencia;
	private LocalDate dataFimVigencia;
	private Integer status;
	private String nomePlano;
	private Integer codigoCanal;
	
	
	
	public boolean isFiltroPreenchido(){
		if(!Strings.isBlankOrNull(nomePlano) || (codigoCanal != null && codigoCanal != 0) || (status != null &&  status != 0)){
			return true;
		}
		return false;
	}
	
	/**
	 * Retorna dataInicioVigencia.
	 *
	 * @return dataInicioVigencia.
	 */
	public LocalDate getDataInicioVigencia() {
		return dataInicioVigencia;
	}


	/**
	 * Especifica dataInicioVigencia.
	 *
	 * @param dataInicioVigencia - dataInicioVigencia.
	 */
	public void setDataInicioVigencia(LocalDate dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}


	/**
	 * Retorna dataFimVigencia.
	 *
	 * @return dataFimVigencia.
	 */
	public LocalDate getDataFimVigencia() {
		return dataFimVigencia;
	}


	/**
	 * Especifica dataFimVigencia.
	 *
	 * @param dataFimVigencia - dataFimVigencia.
	 */
	public void setDataFimVigencia(LocalDate dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}


//	/**
//	 * Retorna status.
//	 *
//	 * @return status.
//	 */
//	public String getStatus() {
//		return status;
//	}
//	/**
//	 * Especifica status.
//	 *
//	 * @param status - status.
//	 */
//	public void setStatus(String status) {
//		this.status = status;
//	}

	
	public String getNomePlano() {
		return nomePlano;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setNomePlano(String nomePlano) {
		this.nomePlano = nomePlano;
	}

	public Integer getCodigoCanal() {
		return codigoCanal;
	}

	public void setCodigoCanal(Integer codigoCanal) {
		this.codigoCanal = codigoCanal;
	}
}

