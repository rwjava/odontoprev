package br.com.bradseg.eedi.administrativo.vo;

import java.util.ArrayList;
import java.util.List;



/**
 * Classe enumeration TipoCobranca
 */
public enum TipoCobranca {

	MENSAL(1, "Mensal"), ANUAL(2, "Anual"),MENSAL_E_ANUAL(3,"Mensal e Anual");

	private Integer codigo;
	private String descricao;

	private TipoCobranca(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public static List<TipoCobranca>retornarMensalEAnual(){
		List<TipoCobranca>listaDeTipoCobranca = new ArrayList<TipoCobranca>();
		listaDeTipoCobranca.add(TipoCobranca.MENSAL);
		listaDeTipoCobranca.add(TipoCobranca.ANUAL);
		return listaDeTipoCobranca;
	}
	

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String toString() {
		return descricao;
	}

	/**
	 * Busca por codigo.
	 * 
	 * @param codigo
	 *            - codigo do tipo de cobran�a
	 * @return o tipo cobran�a
	 */
	public static TipoCobranca buscaPor(Integer codigo) {
		for (TipoCobranca tipo : TipoCobranca.values()) {
			if (tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}
		return null;
	}

	
	/**
	 * Retorna uma lista com todos os sexos.
	 * 
	 * @return List<LabelValueVO> - lista de sexo.
	 */
	public static List<LabelValueVO> obterLista(){
		List<LabelValueVO> lista = new ArrayList<LabelValueVO>();
		for (TipoCobranca tipoCobranca : TipoCobranca.values()) {
			lista.add(new LabelValueVO(String.valueOf(tipoCobranca.getCodigo()), tipoCobranca.getDescricao()));
		}
		return lista;
	}

}
