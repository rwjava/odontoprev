package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HistoricoPlanoViewVO extends HistoricoPlanoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5252072737758864720L;
	
	
	private List<HistoricoPlanoVO> listaHistoricoPlanoVO = new ArrayList<HistoricoPlanoVO>();
	private List<String> listaUsuarios = new ArrayList<String>();


	public List<HistoricoPlanoVO> getListaHistoricoPlanoVO() {
		return listaHistoricoPlanoVO;
	}


	public void setListaHistoricoPlanoVO(
			List<HistoricoPlanoVO> listaHistoricoPlanoVO) {
		this.listaHistoricoPlanoVO = listaHistoricoPlanoVO;
	}


	public List<String> getListaUsuarios() {
		return listaUsuarios;
	}


	public void setListaUsuarios(List<String> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	} 
	
	
	
	
	

	

}
