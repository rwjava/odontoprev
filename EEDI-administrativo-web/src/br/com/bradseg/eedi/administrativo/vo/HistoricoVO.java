package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;

/**
 * Classe responsável por transportar os dados do histórico.
 *
 */
public class HistoricoVO implements Serializable {

	private static final long serialVersionUID = -8829625269448665203L;
	
	private long codigoPlano;
	private String dataOperacao;
	private String nomePlano;
	private String codigoResponsavel;
	private String operacao;
	
	/**
	 * Retorna codigoPlano.
	 *
	 * @return codigoPlano - codigoPlano.
	 */
	public long getCodigoPlano() {
		return codigoPlano;
	}
	/**
	 * Especifica codigoPlano.
	 *
	 * @param codigoPlano - codigoPlano.
	 */
	public void setCodigoPlano(long codigoPlano) {
		this.codigoPlano = codigoPlano;
	}
	/**
	 * Retorna dataOperacao.
	 *
	 * @return dataOperacao - dataOperacao.
	 */
	public String getDataOperacao() {
		return dataOperacao;
	}
	/**
	 * Especifica dataOperacao.
	 *
	 * @param dataOperacao - dataOperacao.
	 */
	public void setDataOperacao(String dataOperacao) {
		this.dataOperacao = dataOperacao;
	}
	/**
	 * Retorna nomePlano.
	 *
	 * @return nomePlano - nomePlano.
	 */
	public String getNomePlano() {
		return nomePlano;
	}
	/**
	 * Especifica nomePlano.
	 *
	 * @param nomePlano - nomePlano.
	 */
	public void setNomePlano(String nomePlano) {
		this.nomePlano = nomePlano;
	}
	/**
	 * Retorna codigoResponsavel.
	 *
	 * @return codigoResponsavel - codigoResponsavel.
	 */
	public String getCodigoResponsavel() {
		return codigoResponsavel;
	}
	/**
	 * Especifica codigoResponsavel.
	 *
	 * @param codigoResponsavel - codigoResponsavel.
	 */
	public void setCodigoResponsavel(String codigoResponsavel) {
		this.codigoResponsavel = codigoResponsavel;
	}
	/**
	 * Retorna operacao.
	 *
	 * @return operacao - operacao.
	 */
	public String getOperacao() {
		return operacao;
	}
	/**
	 * Especifica operacao.
	 *
	 * @param operacao - operacao.
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
}
