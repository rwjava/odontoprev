package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PlanoViewVO extends PlanoVO implements Serializable{

	
	private static final long serialVersionUID = -7663547702326232814L;
	
	private List<CanalVendaVO> listaDeCanaisSelecionados;
	private List<CanalVendaVO> listaDeCanaisDoPlano;
	private TipoCobranca tipoCobranca;
	private TipoCobranca novoTipoCobranca;
	
	private Date dataInicioVigenciaTela;
	private Date dataFimVigenciaTela;
	
	private Date dataInicioNovoValor;
	
	private String valorMensalTitular;
	private String valorMensalDependente;
	private String novoValorMensalTitular;
	private String novoValorMensalDependente;
	
	private String valorAnualTitular;
	private String valorAnualDependente;
	private String novoValorAnualTitular;
	private String novoValorAnualDependente;
	
	private String valorDesconto;
	private String valorTaxa;
	
	private Integer tipoCobrancaPlano;
	private Integer novoTipoCobrancaPlano;
	
	private Boolean podeAlterar; 
	
	private String descricaoTipoCobranca;
	private String descricaoNovoTipoCobranca;

	
	public PlanoViewVO(){
	}
	
	/**
	 * @return the tipoCobrança
	 */
	public TipoCobranca getTipoCobranca() {
		return tipoCobranca;
	}

	/**
	 * @param tipoCobrança the tipoCobrança to set
	 */
	public void setTipoCobrança(TipoCobranca tipoCobranca) {
		this.tipoCobranca = tipoCobranca;
	}

	/**
	 * @return the listaDeCanaisSelecionados
	 */
	public List<CanalVendaVO> getListaDeCanaisSelecionados() {
		return listaDeCanaisSelecionados;
	}

	/**
	 * @param listaDeCanaisSelecionados the listaDeCanaisSelecionados to set
	 */
	public void setListaDeCanaisSelecionados(
			List<CanalVendaVO> listaDeCanaisSelecionados) {
		this.listaDeCanaisSelecionados = listaDeCanaisSelecionados;
	}

	/**
	 * @return the dataInicioVigenciaTela
	 */
	public Date getDataInicioVigenciaTela() {
		return dataInicioVigenciaTela;
	}

	/**
	 * @param dataInicioVigenciaTela the dataInicioVigenciaTela to set
	 */
	public void setDataInicioVigenciaTela(Date dataInicioVigenciaTela) {
		this.dataInicioVigenciaTela = dataInicioVigenciaTela;
	}

	/**
	 * @return the dataFimVigenciaTela
	 */
	public Date getDataFimVigenciaTela() {
		return dataFimVigenciaTela;
	}

	/**
	 * @param dataFimVigenciaTela the dataFimVigenciaTela to set
	 */
	public void setDataFimVigenciaTela(Date dataFimVigenciaTela) {
		this.dataFimVigenciaTela = dataFimVigenciaTela;
	}

	public String getValorMensalTitular() {
		return valorMensalTitular;
	}

	public void setValorMensalTitular(String valorMensalTitular) {
		this.valorMensalTitular = valorMensalTitular;
	}

	public String getValorMensalDependente() {
		return valorMensalDependente;
	}

	public void setValorMensalDependente(String valorMensalDependente) {
		this.valorMensalDependente = valorMensalDependente;
	}

	public String getValorAnualTitular() {
		return valorAnualTitular;
	}

	public void setValorAnualTitular(String valorAnualTitular) {
		this.valorAnualTitular = valorAnualTitular;
	}

	public String getValorAnualDependente() {
		return valorAnualDependente;
	}

	public void setValorAnualDependente(String valorAnualDependente) {
		this.valorAnualDependente = valorAnualDependente;
	}

	public void setTipoCobranca(TipoCobranca tipoCobranca) {
		this.tipoCobranca = tipoCobranca;
	}

	/**
	 * @return the listaDeCanaisDoPlano
	 */
	public List<CanalVendaVO> getListaDeCanaisDoPlano() {
		return listaDeCanaisDoPlano;
	}

	/**
	 * @param listaDeCanaisDoPlano the listaDeCanaisDoPlano to set
	 */
	public void setListaDeCanaisDoPlano(List<CanalVendaVO> listaDeCanaisDoPlano) {
		this.listaDeCanaisDoPlano = listaDeCanaisDoPlano;
	}

	public String getValorDesconto() {
		// TODO Auto-generated method stub
		return valorDesconto;
	}
	
	public void  setValorDesconto(String valorDesconto) {
		// TODO Auto-generated method stub
		 this.valorDesconto = valorDesconto;
	}

	/**
	 * @return the tipoCobrancaPlano
	 */
	public Integer getTipoCobrancaPlano() {
		return tipoCobrancaPlano;
	}

	/**
	 * @param tipoCobrancaPlano the tipoCobrancaPlano to set
	 */
	public void setTipoCobrancaPlano(Integer tipoCobrancaPlano) {
		this.tipoCobrancaPlano = tipoCobrancaPlano;
	}

	public String getValorTaxa() {
		return valorTaxa;
	}

	public void setValorTaxa(String valorTaxa) {
		this.valorTaxa = valorTaxa;
	}

	public Boolean getPodeAlterar() {
		return podeAlterar;
	}

	public void setPodeAlterar(Boolean podeAlterar) {
		this.podeAlterar = podeAlterar;
	}

	public Integer getNovoTipoCobrancaPlano() {
		return novoTipoCobrancaPlano;
	}

	public void setNovoTipoCobrancaPlano(Integer novoTipoCobrancaPlano) {
		this.novoTipoCobrancaPlano = novoTipoCobrancaPlano;
	}

	public TipoCobranca getNovoTipoCobranca() {
		return novoTipoCobranca;
	}

	public void setNovoTipoCobranca(TipoCobranca novoTipoCobranca) {
		this.novoTipoCobranca = novoTipoCobranca;
	}

	public String getNovoValorMensalTitular() {
		return novoValorMensalTitular;
	}

	public void setNovoValorMensalTitular(String novoValorMensalTitular) {
		this.novoValorMensalTitular = novoValorMensalTitular;
	}

	public String getNovoValorMensalDependente() {
		return novoValorMensalDependente;
	}

	public void setNovoValorMensalDependente(String novoValorMensalDependente) {
		this.novoValorMensalDependente = novoValorMensalDependente;
	}

	public String getNovoValorAnualTitular() {
		return novoValorAnualTitular;
	}

	public void setNovoValorAnualTitular(String novoValorAnualTitular) {
		this.novoValorAnualTitular = novoValorAnualTitular;
	}

	public String getNovoValorAnualDependente() {
		return novoValorAnualDependente;
	}

	public void setNovoValorAnualDependente(String novoValorAnualDependente) {
		this.novoValorAnualDependente = novoValorAnualDependente;
	}

	public Date getDataInicioNovoValor() {
		return dataInicioNovoValor;
	}

	public void setDataInicioNovoValor(Date dataInicioNovoValor) {
		this.dataInicioNovoValor = dataInicioNovoValor;
	}

	public String getDescricaoTipoCobranca() {
		return descricaoTipoCobranca;
	}

	public void setDescricaoTipoCobranca(String descricaoTipoCobranca) {
		this.descricaoTipoCobranca = descricaoTipoCobranca;
	}

	public String getDescricaoNovoTipoCobranca() {
		return descricaoNovoTipoCobranca;
	}

	public void setDescricaoNovoTipoCobranca(String descricaoNovoTipoCobranca) {
		this.descricaoNovoTipoCobranca = descricaoNovoTipoCobranca;
	}

}
