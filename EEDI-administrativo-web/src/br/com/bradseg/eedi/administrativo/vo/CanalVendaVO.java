package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;

import org.joda.time.DateTime;

/**
 * Informações do canal de venda da do plano
 * @author WDEV
 */
public class CanalVendaVO implements Serializable {

	private static final long serialVersionUID = -1604712149064807210L;
	private Integer codigo;
	private String sigla;
	private String nome;
	private DateTime dataUltimaAtualizacao;
	private String codigoResponsavel;
	


	public CanalVendaVO() {
	}
	
	
	public CanalVendaVO(Integer codigo) {
		super();
		this.codigo = codigo;
	}


	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		CanalVendaVO other = (CanalVendaVO) obj;
		if (codigo == null) {
			if (other.codigo != null){
				return false;
			}
		} else if (!codigo.equals(other.codigo)){
			return false;
		}
		return true;
	}


	/**
	 * @return the dataUltimaAtualizacao
	 */
	public DateTime getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}


	/**
	 * @param dataUltimaAtualizacao the dataUltimaAtualizacao to set
	 */
	public void setDataUltimaAtualizacao(DateTime dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}


	public String getCodigoResponsavel() {
		return codigoResponsavel;
	}


	public void setCodigoResponsavel(String codigoResponsavel) {
		this.codigoResponsavel = codigoResponsavel;
	}
	
}