package br.com.bradseg.eedi.administrativo.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enumera��o dos status do plano: Vigente, Programado e Expirado.
 *
 */
public enum Status {

	EXPIRADO(1, "Expirado"),
	PROGRAMADO(2, "Programado"),
	VIGENTE(3, "Vigente");
	
	
	private Integer codigo;
	private String descricao;

	/**
	 * Construtor alterantivo.
	 * 
	 * @param codigo - c�digo.
	 * @param descricao - descri��o.
	 */
	private Status(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo.
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * Retorna descricao.
	 *
	 * @return descricao.
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Especifica descricao.
	 *
	 * @param descricao - descricao.
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
	/**
	 * Retorna uma lista com todos os sexos.
	 * 
	 * @return List<LabelValueVO> - lista de sexo.
	 */
	public static List<LabelValueVO> obterLista(){
		List<LabelValueVO> lista = new ArrayList<LabelValueVO>();
		for (Status status : Status.values()) {
			lista.add(new LabelValueVO(String.valueOf(status.getCodigo()), status.getDescricao()));
		}
		return lista;
	}
}
