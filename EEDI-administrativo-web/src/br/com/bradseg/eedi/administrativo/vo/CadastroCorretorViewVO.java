package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import br.com.bradseg.eedi.administrativo.plano.action.CorretorAction;

public class CadastroCorretorViewVO extends CadastroCorretorVO implements Serializable {

	private static final long serialVersionUID = -4371581988279351376L;
	
	private List<CadastroCorretorVO> listaCadastroCorretor;
	
	private Date dataInicioContratoTela;
	private Date dataFimContratoTela;
	
	private String data = "18/01/2014";
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CorretorAction.class);
	
	
	/*private String dataInicioContratoStr;
	private String dataFimContratoStr;*/

	
	/**
	 * @return Retorna listaCadastroCorretor
	 */
	public List<CadastroCorretorVO> getListaCadastroCorretor() {
		return listaCadastroCorretor;
	}
	/**
	 * @param Insere valor listaCadastroCorretor
	 */
	public void setListaCadastroCorretor(List<CadastroCorretorVO> listaCadastroCorretor) {
		this.listaCadastroCorretor = listaCadastroCorretor;
	}
	
	
	public Date getDataInicioContratoTela() {
		try {
			return dataInicioContratoTela = dateFormat.parse(data);
		} catch (ParseException e) {
			LOGGER.error("Error de parse" + e.getMessage());
		}
		return dataInicioContratoTela;
	}
	public void setDataInicioContratoTela(Date dataInicioContratoTela) {
		this.dataInicioContratoTela = dataInicioContratoTela;
	}
	
	
	public Date getDataFimContratoTela() {
		return dataFimContratoTela;
	}
	public void setDataFimContratoTela(Date dataFimContratoTela) {
		this.dataFimContratoTela = dataFimContratoTela;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
	/**
	 * 
	 * @return
	 */
/*	public String getDataInicioContratoStr() {
		return dataInicioContratoStr;
	}
	public void setDataInicioContratoStr(String dataInicioContratoStr) {
		this.dataInicioContratoStr = dataInicioContratoStr;
	}
	
	*//**
	 * 
	 * @return
	 *//*
	public String getDataFimContratoStr() {
		return dataFimContratoStr;
	}
	public void setDataFimContratoStr(String dataFimContratoStr) {
		this.dataFimContratoStr = dataFimContratoStr;
	}*/
	
}