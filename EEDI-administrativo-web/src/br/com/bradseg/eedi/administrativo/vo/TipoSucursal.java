package br.com.bradseg.eedi.administrativo.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Enumera��o dos tipos de sucursal: Mercado, Rede e Corporate.
 *
 */
public enum TipoSucursal {
	
	CORPORATE(4, "CORPORATE"),
	MERCADO(3, "MERCADO"),
	REDE(2, "REDE");
	
	private Integer codigo;
	private String nome;
	
	/**
	 * Construtor padr�o.
	 * 
	 * @param codigo - codigo do tipo de sucursal.
	 * @param nome - nome do tipo da sucursal.
	 */
	private TipoSucursal(Integer codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}
	
	/**
	 * Retorna codigo.
	 *
	 * @return codigo - codigo.
	 */
	public Integer getCodigo() {
		return codigo;
	}
	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	/**
	 * Retorna nome.
	 *
	 * @return nome - nome.
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Especifica nome.
	 *
	 * @param nome - nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Metodo responsavel por listar os tipos de sucursal.
	 * 
	 * @return List<TipoSucursal> - lista de tipo de sucursal.
	 */
	public static List<TipoSucursal> listar(){
		List<TipoSucursal> lista = new ArrayList<TipoSucursal>();
		for (TipoSucursal tipoSucursal : TipoSucursal.values()) {
			lista.add(tipoSucursal);
		}
		return lista;
	}

	public static TipoSucursal buscaPor(Integer codigo) {
		for (TipoSucursal segmento : TipoSucursal.values()) {
			if (segmento.getCodigo().equals(codigo)) {
				return segmento;
			}
		}
		return null;
	}
}
