package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import br.com.bradseg.eedi.administrativo.support.Strings;

/**
 * Classe responsavel pelo transporte de dados do plano.
 */
public class PlanoVO implements Serializable {

	private static final long serialVersionUID = 8831909890562447616L;
	private Long codigo;
	private String nome;
	private Long codigoRegistro;
	private String codigoResponsavel;
	private DateTime dataInicioVigencia;
	private DateTime dataFimVigencia;
	private String descricaoCarenciaPeriodoMensal;
	private String descricaoCarenciaPeriodoAnual;
	private Long codigoResponsavelUltimaAtualizacao;
	private DateTime dataUltimaAtualizacao;
	private int podeAlterarPlano;

	private List<CanalVendaVO> listaDeCanais;

	private ValorPlanoVO valorPlanoVO;
	
	private String status;
	
	private String novaDescricaoCarenciaPeriodoMensal;
	private String novaDescricaoCarenciaPeriodoAnual;
	private DateTime dataProgramadaNovoValorPlano;
	private ValorPlanoVO novoValorPlanoVO;
	
	private boolean existeValorProgramado;
	
	private boolean valorProgramadoExcluido;
	
	private List<Integer> listaTipoSucursal;
	
	/**
	 * Construtor alternativo.
	 */
	public PlanoVO(){
		
		
	}
	
	/**
	 * Retorna codigo.
	 *
	 * @return codigo.
	 */
	public Long getCodigo() {
		return codigo;
	}
	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	
	/**
	 * Retorna codigoRegistro.
	 *
	 * @return codigoRegistro.
	 */
	public Long getCodigoRegistro() {
		return codigoRegistro;
	}
	/**
	 * Especifica codigoRegistro.
	 *
	 * @param codigoRegistro - codigoRegistro.
	 */
	public void setCodigoRegistro(Long codigoRegistro) {
		this.codigoRegistro = codigoRegistro;
	}
	
	public String getCodigoResponsavel() {
		return codigoResponsavel;
	}

	public void setCodigoResponsavel(String codigoResponsavel) {
		this.codigoResponsavel = codigoResponsavel;
	}



	public String getDescricaoCarenciaPeriodoMensal() {
		return descricaoCarenciaPeriodoMensal;
	}

	public void setDescricaoCarenciaPeriodoMensal(
			String descricaoCarenciaPeriodoMensal) {
		this.descricaoCarenciaPeriodoMensal = descricaoCarenciaPeriodoMensal;
	}

	public String getDescricaoCarenciaPeriodoAnual() {
		return descricaoCarenciaPeriodoAnual;
	}

	public void setDescricaoCarenciaPeriodoAnual(
			String descricaoCarenciaPeriodoAnual) {
		this.descricaoCarenciaPeriodoAnual = descricaoCarenciaPeriodoAnual;
	}

	/**
	 * Retorna codigoResponsavelUltimaAtualizacao.
	 *
	 * @return codigoResponsavelUltimaAtualizacao.
	 */
	public Long getCodigoResponsavelUltimaAtualizacao() {
		return codigoResponsavelUltimaAtualizacao;
	}
	/**
	 * Especifica codigoResponsavelUltimaAtualizacao.
	 *
	 * @param codigoResponsavelUltimaAtualizacao - codigoResponsavelUltimaAtualizacao.
	 */
	public void setCodigoResponsavelUltimaAtualizacao(Long codigoResponsavelUltimaAtualizacao) {
		this.codigoResponsavelUltimaAtualizacao = codigoResponsavelUltimaAtualizacao;
	}
	
	/**
	 * Retorna dataUltimaAtualizacao.
	 *
	 * @return dataUltimaAtualizacao.
	 */
	public DateTime getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	/**
	 * Especifica dataUltimaAtualizacao.
	 *
	 * @param dataUltimaAtualizacao - dataUltimaAtualizacao.
	 */
	public void setDataUltimaAtualizacao(DateTime dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	/**
	 * @return the valorPlanoVO
	 */
	public ValorPlanoVO getValorPlanoVO() {
		return valorPlanoVO;
	}

	/**
	 * @param valorPlanoVO the valorPlanoVO to set
	 */
	public void setValorPlanoVO(ValorPlanoVO valorPlanoVO) {
		this.valorPlanoVO = valorPlanoVO;
	}
	
	/**
	 * @return the listaDeCanais
	 */
	public List<CanalVendaVO> getListaDeCanais() {
		if(listaDeCanais == null){
			return new ArrayList<CanalVendaVO>();
		}
		return listaDeCanais;
	}

	/**
	 * @param listaDeCanais the listaDeCanais to set
	 */
	public void setListaDeCanais(List<CanalVendaVO> listaDeCanais) {
		this.listaDeCanais = listaDeCanais;
	}

	@XmlJavaTypeAdapter(type = org.joda.time.DateTime.class, value = br.com.bradseg.eedi.administrativo.plano.webservice.DateTimeAdapter.class)
	public DateTime getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(DateTime dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	@XmlJavaTypeAdapter(type = org.joda.time.DateTime.class, value = br.com.bradseg.eedi.administrativo.plano.webservice.DateTimeAdapter.class)
	public DateTime getDataFimVigencia() {
		return dataFimVigencia;
	}
	public void setDataFimVigencia(DateTime dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	public int getPodeAlterarPlano() {
		return podeAlterarPlano;
	}

	public void setPodeAlterarPlano(int podeAlterarPlano) {
		this.podeAlterarPlano = podeAlterarPlano;
	}

	

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		if(Strings.isBlankOrNull(this.status)){
			if(this.dataInicioVigencia != null && this.dataFimVigencia != null){
				LocalDate dataAtual = new LocalDate();
				
				// Se existir DATA de inicio de vig�ncia, avaliar se o registro est� programado ou n�o...
				if (dataAtual.isBefore(this.dataInicioVigencia.toLocalDate())) {
					status = Status.PROGRAMADO.getDescricao();
					
				// Se existir DATA de fim de vig�ncia, avaliar se o registro est� expirado...
				}else if (dataAtual.isAfter(this.dataFimVigencia.toLocalDate())) {
					status = Status.EXPIRADO.getDescricao();
					
				//Se existir DATA de in�cio e fim de vig�ncia, avaliar se o registro est� vigente...
				}else if((dataAtual.isAfter(this.dataInicioVigencia.toLocalDate())  || dataAtual.isEqual(this.dataInicioVigencia.toLocalDate())) 
						&& (dataAtual.isBefore(this.dataFimVigencia.toLocalDate()) 
						|| dataAtual.isEqual(this.dataFimVigencia.toLocalDate()))){
					status = Status.VIGENTE.getDescricao();
				}
			}
		}
		return status;
	}

	public String getNovaDescricaoCarenciaPeriodoMensal() {
		return novaDescricaoCarenciaPeriodoMensal;
	}

	public void setNovaDescricaoCarenciaPeriodoMensal(String novaDescricaoCarenciaPeriodoMensal) {
		this.novaDescricaoCarenciaPeriodoMensal = novaDescricaoCarenciaPeriodoMensal;
	}

	public String getNovaDescricaoCarenciaPeriodoAnual() {
		return novaDescricaoCarenciaPeriodoAnual;
	}

	public void setNovaDescricaoCarenciaPeriodoAnual(
			String novaDescricaoCarenciaPeriodoAnual) {
		this.novaDescricaoCarenciaPeriodoAnual = novaDescricaoCarenciaPeriodoAnual;
	}

	public DateTime getDataProgramadaNovoValorPlano() {
		return dataProgramadaNovoValorPlano;
	}

	public void setDataProgramadaNovoValorPlano(DateTime dataProgramadaNovoValorPlano) {
		this.dataProgramadaNovoValorPlano = dataProgramadaNovoValorPlano;
	}

	public ValorPlanoVO getNovoValorPlanoVO() {
		return novoValorPlanoVO;
	}

	public void setNovoValorPlanoVO(ValorPlanoVO novoValorPlanoVO) {
		this.novoValorPlanoVO = novoValorPlanoVO;
	}

	public boolean isExisteValorProgramado() {
		return this.existeValorProgramado;
	}

	public void setExisteValorProgramado(boolean existeValorProgramado) {
		this.existeValorProgramado = existeValorProgramado;
	}

	public boolean isValorProgramadoExcluido() {
		return valorProgramadoExcluido;
	}

	public void setValorProgramadoExcluido(boolean valorProgramadoExcluido) {
		this.valorProgramadoExcluido = valorProgramadoExcluido;
	}

	/**
	 * Retorna listaTipoSucursal.
	 *
	 * @return listaTipoSucursal - listaTipoSucursal.
	 */
	public List<Integer> getListaTipoSucursal() {
		return listaTipoSucursal;
	}

	/**
	 * Especifica listaTipoSucursal.
	 *
	 * @param listaTipoSucursal - listaTipoSucursal.
	 */
	public void setListaTipoSucursal(List<Integer> listaTipoSucursal) {
		this.listaTipoSucursal = listaTipoSucursal;
	}
}
