package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

public class HistoricoPlanoVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6089761617257501516L;
	private Integer codigoPlano;
	private String descricao;
	private Date dataAlteracao;
	private String tipoAlteracao;
	private String codigoResponsavelAtualizacao;
	private String campoAlterado;
	private String valorCampoAntigo;
	private String valorCampoNovo;
	private DateTime inicioVigencia; 
	private DateTime fimVigencia;
	/*private Double valorTaxa;
	private Double valorDesconto;
	private Double valorMensal;
	private Double valorAnual;
	private Double valorDependenteAnual;
	private Double valorDependenteMensal;*/
	private String periodoCarenciaMensal;
	private String periodoCarenciaAnual;
	private String dataAlteracaoFormatada;
	private List<String> listaCamposAlterados; 
	private List<String> listaValoresAntigos;
	private List<String> listaValoresNovos;
	
	
	
	
	
	
	
	public HistoricoPlanoVO() {
		
		
	}





	public String getDescricao() {
		return descricao;
	}





	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}





	public Date getDataAlteracao() {
		return dataAlteracao;
	}





	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}





	public String getTipoAlteracao() {
		return tipoAlteracao;
	}





	public void setTipoAlteracao(String tipoAlteracao) {
		this.tipoAlteracao = tipoAlteracao;
	}





	public String getCodigoResponsavelAtualizacao() {
		return codigoResponsavelAtualizacao;
	}





	public void setCodigoResponsavelAtualizacao(String codigoResponsavelAtualizacao) {
		this.codigoResponsavelAtualizacao = codigoResponsavelAtualizacao;
	}





	public String getCampoAlterado() {
		return campoAlterado;
	}





	public void setCampoAlterado(String campoAlterado) {
		this.campoAlterado = campoAlterado;
	}





	public String getValorCampoAntigo() {
		return valorCampoAntigo;
	}





	public void setValorCampoAntigo(String valorCampoAntigo) {
		this.valorCampoAntigo = valorCampoAntigo;
	}





	public String getValorCampoNovo() {
		return valorCampoNovo;
	}





	public void setValorCampoNovo(String valorCampoNovo) {
		this.valorCampoNovo = valorCampoNovo;
	}





	public DateTime getInicioVigencia() {
		return inicioVigencia;
	}





	public void setInicioVigencia(DateTime inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}





	public DateTime getFimVigencia() {
		return fimVigencia;
	}





	public void setFimVigencia(DateTime fimVigencia) {
		this.fimVigencia = fimVigencia;
	}





	/*public Double getValorTaxa() {
		return valorTaxa;
	}





	public void setValorTaxa(Double valorTaxa) {
		this.valorTaxa = valorTaxa;
	}





	public Double getValorDesconto() {
		return valorDesconto;
	}





	public void setValorDesconto(Double valorDesconto) {
		this.valorDesconto = valorDesconto;
	}





	public Double getValorMensal() {
		return valorMensal;
	}





	public void setValorMensal(Double valorMensal) {
		this.valorMensal = valorMensal;
	}





	public Double getValorAnual() {
		return valorAnual;
	}





	public void setValorAnual(Double valorAnual) {
		this.valorAnual = valorAnual;
	}





	public Double getValorDependenteAnual() {
		return valorDependenteAnual;
	}





	public void setValorDependenteAnual(Double valorDependenteAnual) {
		this.valorDependenteAnual = valorDependenteAnual;
	}





	public Double getValorDependenteMensal() {
		return valorDependenteMensal;
	}





	public void setValorDependenteMensal(Double valorDependenteMensal) {
		this.valorDependenteMensal = valorDependenteMensal;
	}
*/




	public String getPeriodoCarenciaMensal() {
		return periodoCarenciaMensal;
	}





	public void setPeriodoCarenciaMensal(String periodoCarenciaMensal) {
		this.periodoCarenciaMensal = periodoCarenciaMensal;
	}





	public String getPeriodoCarenciaAnual() {
		return periodoCarenciaAnual;
	}





	public void setPeriodoCarenciaAnual(String periodoCarenciaAnual) {
		this.periodoCarenciaAnual = periodoCarenciaAnual;
	}





	public Integer getCodigoPlano() {
		return codigoPlano;
	}





	public void setCodigoPlano(Integer codigoPlano) {
		this.codigoPlano = codigoPlano;
	}





	public String getDataAlteracaoFormatada() {
		return dataAlteracaoFormatada;
	}





	public void setDataAlteracaoFormatada(String dataAlteracaoFormatada) {
		this.dataAlteracaoFormatada = dataAlteracaoFormatada;
	}





	public List<String> getListaCamposAlterados() {
		return listaCamposAlterados;
	}





	public void setListaCamposAlterados(List<String> listaCamposAlterados) {
		this.listaCamposAlterados = listaCamposAlterados;
	}





	public List<String> getListaValoresAntigos() {
		return listaValoresAntigos;
	}





	public void setListaValoresAntigos(List<String> listaValoresAntigos) {
		this.listaValoresAntigos = listaValoresAntigos;
	}





	public List<String> getListaValoresNovos() {
		return listaValoresNovos;
	}





	public void setListaValoresNovos(List<String> listaValoresNovos) {
		this.listaValoresNovos = listaValoresNovos;
	}

	

	
	

}
