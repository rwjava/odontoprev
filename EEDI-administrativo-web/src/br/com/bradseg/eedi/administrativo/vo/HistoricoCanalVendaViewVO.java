package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.util.List;

public class HistoricoCanalVendaViewVO extends HistoricoCanalVendaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5252072737758864720L;
	
	private List<HistoricoCanalVendaVO> listaHistoricoCanal;

	public List<HistoricoCanalVendaVO> getListaHistoricoCanal() {
		return listaHistoricoCanal;
	}

	public void setListaHistoricoCanal(
			List<HistoricoCanalVendaVO> listaHistoricoCanal) {
		this.listaHistoricoCanal = listaHistoricoCanal;
	}

	

	

}
