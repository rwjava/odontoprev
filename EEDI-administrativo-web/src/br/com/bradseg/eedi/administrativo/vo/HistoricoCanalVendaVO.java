package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.DateTime;

public class HistoricoCanalVendaVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3714817288060592124L;
	
	private Integer codigoCanal;
	private String descricao;
	private Date dataUltimaAtualizacao;
	private String responsavelAlteracao ; 
	private Integer codigoPlano ;
	private DateTime dataInicioVigencia;
	private DateTime dataFimVigencia;
	
	
	public HistoricoCanalVendaVO() {
		
		
	}	
	
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public String getResponsavelAlteracao() {
		return responsavelAlteracao;
	}

	public void setResponsavelAlteracao(String responsavelAlteracao) {
		this.responsavelAlteracao = responsavelAlteracao;
	}

	public Integer getCodigoCanal() {
		return codigoCanal;
	}

	public void setCodigoCanal(Integer codigoCanal) {
		this.codigoCanal = codigoCanal;
	}

	public Integer getCodigoPlano() {
		return codigoPlano;
	}

	public void setCodigoPlano(Integer codigoPlano) {
		this.codigoPlano = codigoPlano;
	}

	public DateTime getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(DateTime dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public DateTime getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(DateTime dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	
	

}
