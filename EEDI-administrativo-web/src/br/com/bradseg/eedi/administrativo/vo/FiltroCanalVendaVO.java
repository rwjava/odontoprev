package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;

import br.com.bradseg.eedi.administrativo.support.Strings;

/**
 * Filtro para consulta dos canal de venda
 * 
 * @author WDEV
 *
 */
public class FiltroCanalVendaVO implements Serializable {

	
	private static final long serialVersionUID = 1777403511064257698L;
	private String sigla;
	private String nome;
	
	
	public boolean isFiltroPreenchido(){
		if(!Strings.isBlankOrNull(sigla) || !Strings.isBlankOrNull(nome)){
			return true;
		}
		return false;
	}


	public String getSigla() {
		return sigla;
	}


	public void setSigla(String sigla) {
		this.sigla = sigla;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
