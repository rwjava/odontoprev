package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;

import org.joda.time.LocalDate;

import br.com.bradseg.eedi.administrativo.support.Strings;

public class VigenciaCanalVendaVO implements Serializable{
	
	
	private static final long serialVersionUID = 7729772284146856174L;
	private CanalVendaVO canalVendaVO;
	//private PlanoVO planoVO;
	private LocalDate dataInicioVigenciaAssociacaoPlano;
	private LocalDate dataFimVigenciaAssociacaoPlano;
	private LocalDate dataInicioAntesDeAlteracao;
	private LocalDate dataFimAntesDeAlteracao;
	private String status;
	private Integer indicaRemocao;
	private String responsavelUltimaAtualizacao;
	
	
	public LocalDate getDataFimAntesDeAlteracao() {
		return dataFimAntesDeAlteracao;
	}

	public void setDataFimAntesDeAlteracao(LocalDate dataFimAntesDeAlteracao) {
		this.dataFimAntesDeAlteracao = dataFimAntesDeAlteracao;
	}



	public VigenciaCanalVendaVO() {
		canalVendaVO = new CanalVendaVO();
		//planoVO = new PlanoVO();
	}
	
	

	public VigenciaCanalVendaVO(CanalVendaVO canalVendaVO,
			LocalDate dataInicioVigenciaAssociacaoPlano,
			LocalDate dataFimVigenciaAssociacaoPlano) {
		super();
		this.canalVendaVO = canalVendaVO;
		this.dataInicioVigenciaAssociacaoPlano = dataInicioVigenciaAssociacaoPlano;
		this.dataFimVigenciaAssociacaoPlano = dataFimVigenciaAssociacaoPlano;
	}



	public CanalVendaVO getCanalVendaVO() {
	
		return canalVendaVO;
	}

	public void setCanalVendaVO(CanalVendaVO canalVendaVO) {
		this.canalVendaVO = canalVendaVO;
	}

	public LocalDate getDataInicioVigenciaAssociacaoPlano() {
		return dataInicioVigenciaAssociacaoPlano;
	}

	public void setDataInicioVigenciaAssociacaoPlano(
			LocalDate dataInicioVigenciaAssociacaoPlano) {
		this.dataInicioVigenciaAssociacaoPlano = dataInicioVigenciaAssociacaoPlano;
	}

	public LocalDate getDataFimVigenciaAssociacaoPlano() {
		return dataFimVigenciaAssociacaoPlano;
	}

	public void setDataFimVigenciaAssociacaoPlano(
			LocalDate dataFimVigenciaAssociacaoPlano) {
		this.dataFimVigenciaAssociacaoPlano = dataFimVigenciaAssociacaoPlano;
	}



	
	public String getStatus(){
	if(Strings.isBlankOrNull(this.status)){
		if(dataInicioVigenciaAssociacaoPlano != null && dataFimVigenciaAssociacaoPlano != null){
			LocalDate dataAtual = new LocalDate();
			
			// Se existir DATA de inicio de vig�ncia, avaliar se o registro est� programado ou n�o...
			if (dataAtual.isBefore(this.dataInicioVigenciaAssociacaoPlano)) {
				status = Status.PROGRAMADO.getDescricao();
				
			// Se existir DATA de fim de vig�ncia, avaliar se o registro est� expirado...
			}else if (dataAtual.isAfter(dataFimVigenciaAssociacaoPlano)) {
				status = Status.EXPIRADO.getDescricao();
				
			//Se existir DATA de in�cio e fim de vig�ncia, avaliar se o registro est� vigente...
			}else if((dataAtual.isAfter(this.dataInicioVigenciaAssociacaoPlano)  || dataAtual.isEqual(this.dataInicioVigenciaAssociacaoPlano))
					&& (dataAtual.isBefore(this.dataFimVigenciaAssociacaoPlano) || dataAtual.isEqual(this.dataFimVigenciaAssociacaoPlano))){
				status = Status.VIGENTE.getDescricao();
			}
		}
	}
	return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}



	public LocalDate getDataInicioAntesDeAlteracao() {
		return dataInicioAntesDeAlteracao;
	}



	public void setDataInicioAntesDeAlteracao(LocalDate dataInicioAntesDeAlteracao) {
		this.dataInicioAntesDeAlteracao = dataInicioAntesDeAlteracao;
	}



	public LocalDate getDataFimDepoisDeAlteracao() {
		return dataFimAntesDeAlteracao;
	}



	public void setDataFimDepoisDeAlteracao(LocalDate dataFimDepoisDeAlteracao) {
		this.dataFimAntesDeAlteracao = dataFimDepoisDeAlteracao;
	}


	
	
	@Override
	public String toString() {
		return "VigenciaCanalVendaVO [canalVendaVO=" + canalVendaVO.getNome()
				+ ", dataInicioVigenciaAssociacaoPlano="
				+ dataInicioVigenciaAssociacaoPlano
				+ ", dataFimVigenciaAssociacaoPlano="
				+ dataFimVigenciaAssociacaoPlano
				+ ", dataInicioAntesDeAlteracao=" + dataInicioAntesDeAlteracao
				+ ", dataFimAntesDeAlteracao=" + dataFimAntesDeAlteracao
				+ ", status=" + status + ", indicaRemocao=" + indicaRemocao
				+ "]";
	}

	public Integer getIndicaRemocao() {
		return indicaRemocao;
	}

	public void setIndicaRemocao(Integer indicaRemocao) {
		this.indicaRemocao = indicaRemocao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((canalVendaVO == null) ? 0 : canalVendaVO.hashCode());
		result = prime
				* result
				+ ((dataInicioVigenciaAssociacaoPlano == null) ? 0
						: dataInicioVigenciaAssociacaoPlano.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		VigenciaCanalVendaVO other = (VigenciaCanalVendaVO) obj;
		if (canalVendaVO == null) {
			if (other.canalVendaVO != null){
				return false;
			}
		} else if (!canalVendaVO.equals(other.canalVendaVO)){
			return false;
		}
		if (dataInicioVigenciaAssociacaoPlano == null) {
			if (other.dataInicioVigenciaAssociacaoPlano != null){
				return false;
			}
		} else if (!dataInicioVigenciaAssociacaoPlano
				.equals(other.dataInicioVigenciaAssociacaoPlano)){
			return false;
		}
		return true;
	}

	/**
	 * @return the planoVO
	 */
//	public PlanoVO getPlanoVO() {
//		return planoVO;
//	}

	/**
	 * @param planoVO the planoVO to set
	 */
//	public void setPlanoVO(PlanoVO planoVO) {
//		this.planoVO = planoVO;
//	}

	/**
	 * @return the responsavelUltimaAtualizacao
	 */
	public String getResponsavelUltimaAtualizacao() {
		return responsavelUltimaAtualizacao;
	}

	/**
	 * @param responsavelUltimaAtualizacao the responsavelUltimaAtualizacao to set
	 */
	public void setResponsavelUltimaAtualizacao(
			String responsavelUltimaAtualizacao) {
		this.responsavelUltimaAtualizacao = responsavelUltimaAtualizacao;
	}

	

	
	
	
   	
}
