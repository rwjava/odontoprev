package br.com.bradseg.eedi.administrativo.vo;


/**
 * Enumera��o dos tipos de benefici�rios: Titular e Dependente.
 *
 */
public enum TipoBeneficiario {

	TITULAR(1, "Titular", "T"),
	DEPENDENTE(2, "Dependente", "D");
	
	
	private Integer codigo;
	private String descricao;
	private String sigla;

	/**
	 * Construtor alterantivo.
	 * 
	 * @param codigo - c�digo.
	 * @param descricao - descri��o.
	 * @param sigla - sigla.
	 */
	private TipoBeneficiario(Integer codigo, String descricao, String sigla) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.sigla = sigla;
	}

	/**
	 * Retorna codigo.
	 *
	 * @return codigo.
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * Retorna descricao.
	 *
	 * @return descricao.
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Especifica descricao.
	 *
	 * @param descricao - descricao.
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/**
	 * Retorna sigla.
	 *
	 * @return sigla.
	 */
	public String getSigla() {
		return sigla;
	}

	/**
	 * Especifica sigla.
	 *
	 * @param sigla - sigla.
	 */
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
