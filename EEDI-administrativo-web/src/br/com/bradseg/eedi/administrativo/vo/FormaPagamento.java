package br.com.bradseg.eedi.administrativo.vo;

/**
 * Enum contendo as formas de pagamento
 * 
 * @author EEDI
 */
public enum FormaPagamento {

	D(2, "D�bito Autom�tico"), B(1, "Boleto Banc�rio"), DEBITO_AUTOMATICO(3, "");
	private Integer codigo;
	private String descricao;

	private FormaPagamento(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public final Integer getCodigo() {
		return codigo;
	}

	public final String getDescricao() {
		return descricao;
	}

	public String toString() {
		return getDescricao();
	}

	/**
	 * Busca por codigo.
	 * 
	 * @param codigo - codigo da Forma de Pagamento
	 * @return a forma de pagamento
	 */
	public static FormaPagamento buscaPor(Integer codigo) {
		for (FormaPagamento forma : FormaPagamento.values()) {
			if (forma.getCodigo().equals(codigo)) {
				return forma;
			}
		}
		return null;
	}

}
