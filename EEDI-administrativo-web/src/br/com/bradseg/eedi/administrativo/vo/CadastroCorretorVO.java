package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import br.com.bradseg.eedi.administrativo.support.Strings;

public class CadastroCorretorVO implements Serializable {

	private static final long serialVersionUID = 8254778459189925161L;
	
	private Long sucursal;	
	private Integer status;
	private String descricaoStatus;
	private Long cnpj;
	private String nome;
	private DateTime dataInicioContrato = new DateTime();
	private DateTime dataFimContrato;
	
	private int podeAlterarCorretor;
	
	
	/**
	 * Construtor padr�o
	 */
	public CadastroCorretorVO() {
	}
	
	/**
	 * @return Retorna sucursal
	 */
	public Long getSucursal() {
		return sucursal;
	}
	/**
	 * @param Insere valor sucursal
	 */
	public void setSucursal(Long sucursal) {
		this.sucursal = sucursal;
	}
	
	/**
	 * @return Retorna status
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * @param Insere valor status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getDescricaoStatus() {
		
		if(Strings.isBlankOrNull(this.descricaoStatus)){
			if(this.dataInicioContrato != null && this.dataFimContrato != null){
				LocalDate dataAtual = new LocalDate();

				if (dataAtual.isBefore(this.dataInicioContrato.toLocalDate())) {
					if(getStatus().equals(Status.PROGRAMADO.getCodigo())) {
						descricaoStatus = Status.PROGRAMADO.getDescricao();
					}
				}else if (dataAtual.isAfter(this.dataFimContrato.toLocalDate())) {
					if(getStatus().equals(Status.EXPIRADO.getCodigo())) {
						descricaoStatus = Status.EXPIRADO.getDescricao();
					}
				}else if((dataAtual.isAfter(this.dataInicioContrato.toLocalDate()) || dataAtual.isEqual(this.dataInicioContrato.toLocalDate())) && (dataAtual.isBefore(this.dataFimContrato.toLocalDate()) || dataAtual.isEqual(this.dataFimContrato.toLocalDate()))){
					if(getStatus().equals(Status.VIGENTE.getCodigo())) {
						descricaoStatus = Status.VIGENTE.getDescricao();
					}
				}
			}
		}
		
		return descricaoStatus;
	}

	public void setDescricaoStatus(String descricaoStatus) {
		this.descricaoStatus = descricaoStatus;
	}

	/**
	 * @return Retorna cnpj
	 */
	public Long getCnpj() {
		return cnpj;
	}
	/**
	 * @param Insere valor cnpj
	 */
	public void setCnpj(Long cnpj) {
		this.cnpj = cnpj;
	}

	/**
	 * @return Retorna nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param Insere valor nome
	 */
	public void setNome(String nome) {
		this.nome = nome.toUpperCase();
	}

	/**
	 * @return Retorna dataInicioContrato
	 */
	public DateTime getDataInicioContrato() {
		return dataInicioContrato;
	}
	/**
	 * @param Insere valor dataInicioContrato
	 */
	public void setDataInicioContrato(DateTime dataInicioContrato) {
		this.dataInicioContrato = dataInicioContrato;
	}

	/**
	 * @return Retorna dataFimContrato
	 */
	public DateTime getDataFimContrato() {
		return dataFimContrato;
	}
	/**
	 * @param Insere valor dataFimContrato
	 */
	public void setDataFimContrato(DateTime dataFimContrato) {
		this.dataFimContrato = dataFimContrato;
	}

	public int getPodeAlterarCorretor() {
		return podeAlterarCorretor;
	}

	public void setPodeAlterarCorretor(int podeAlterarCorretor) {
		this.podeAlterarCorretor = podeAlterarCorretor;
	}

}
