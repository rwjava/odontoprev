package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.util.List;

public class CanalVendaViewVO extends CanalVendaVO implements Serializable{


	private static final long serialVersionUID = -6020493100588167024L;
	
	
	private List<CanalVendaVO> listaCanalVenda;


	/**
	 * @return the listaCanalVenda
	 */
	public List<CanalVendaVO> getListaCanalVenda() {
		return listaCanalVenda;
	}


	/**
	 * @param listaCanalVenda the listaCanalVenda to set
	 */
	public void setListaCanalVenda(List<CanalVendaVO> listaCanalVenda) {
		this.listaCanalVenda = listaCanalVenda;
	}

}
