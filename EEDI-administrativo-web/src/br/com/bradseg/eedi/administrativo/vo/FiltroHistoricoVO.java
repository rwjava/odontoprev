package br.com.bradseg.eedi.administrativo.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import br.com.bradseg.eedi.administrativo.support.Constantes;

/**
 * Filtro para consulta dos planos.
 * 
 * @author WDEV
 *
 */
public class FiltroHistoricoVO implements Serializable {

	private static final long serialVersionUID = -1861546701806459408L;
	
	private Date dataInicioPeriodo;
	private Date dataFimPeriodo;	
	private String responsavel;
	
	
	
	
	public boolean isFiltroPreenchido(){
		if((StringUtils.isNotBlank(responsavel) && !Constantes.ZERO.equals(responsavel)) || dataInicioPeriodo != null  || dataFimPeriodo != null ){
			return true;
		}
		return false;
	}
	
	/**
	 * Retorna dataInicioVigencia.
	 *
	 * @return dataInicioVigencia.
	 */
	public Date getDataInicioPeriodo() {
		return dataInicioPeriodo;
	}


	/**
	 * Especifica dataInicioVigencia.
	 *
	 * @param dataInicioVigencia - dataInicioVigencia.
	 */
	public void setDataInicioPeriodo(Date dataInicioPeriodo) {
		this.dataInicioPeriodo = dataInicioPeriodo;
	}


	/**
	 * Retorna dataFimVigencia.
	 *
	 * @return dataFimVigencia.
	 */
	public Date getDataFimPeriodo() {
		return dataFimPeriodo;
	}


	/**
	 * Especifica dataFimVigencia.
	 *
	 * @param dataFimVigencia - dataFimVigencia.
	 */
	public void setDataFimPeriodo(Date dataFimPeriodo) {
		this.dataFimPeriodo = dataFimPeriodo;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}



	
}

