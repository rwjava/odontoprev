package br.com.bradseg.eedi.administrativo.rowmapper;


import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.administrativo.support.ResultSetAdapter;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;

public class CanalVendaRowMapper implements RowMapper<CanalVendaVO> {


	@Override
	public CanalVendaVO mapRow(java.sql.ResultSet rs, int rowNum)
			throws SQLException {
		
		ResultSetAdapter resultSet = new ResultSetAdapter(rs);
		
		CanalVendaVO canalVendaVO = new CanalVendaVO();
			
	 	canalVendaVO.setCodigo(resultSet.getInteger("CCANAL_VDA_DNTAL_INDVD"));
	 	canalVendaVO.setNome(resultSet.getString("RCANAL_VDA_DNTAL_INDVD"));
	 	canalVendaVO.setSigla(resultSet.getString("CSGL_CANAL_VDA_DNTAL_INDVD"));
		

		return canalVendaVO;
		
	}

}