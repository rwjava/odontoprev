package br.com.bradseg.eedi.administrativo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * RowMapper para obter as informações da base de dados do segmento.
 */
public class SegmentoRowMapper implements RowMapper<Integer>{

	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {

		return rs.getInt("CSGMTO_SUCUR");
	}

}
