package br.com.bradseg.eedi.administrativo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.administrativo.support.ResultSetAdapter;
import br.com.bradseg.eedi.administrativo.vo.HistoricoVO;

public class HistoricoRowMapper implements RowMapper<HistoricoVO>{

	@Override
	public HistoricoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ResultSetAdapter resultSet = new ResultSetAdapter(rs);
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		HistoricoVO historicoVO = new HistoricoVO();
		
		historicoVO.setCodigoPlano(resultSet.getLong("CPLANO_DNTAL_INDVD"));
		historicoVO.setNomePlano(rs.getString("RPLANO_DNTAL_INDVD"));
		historicoVO.setCodigoResponsavel(rs.getString("CRESP_ULT_ATULZ"));
		historicoVO.setDataOperacao(df.format(rs.getTimestamp("DINCL_REG_TBELA")));
		
		return historicoVO;
	}

}
