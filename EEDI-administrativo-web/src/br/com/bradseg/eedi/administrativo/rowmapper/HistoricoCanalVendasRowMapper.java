package br.com.bradseg.eedi.administrativo.rowmapper;


import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.administrativo.support.ResultSetAdapter;
import br.com.bradseg.eedi.administrativo.vo.HistoricoCanalVendaVO;



public class HistoricoCanalVendasRowMapper implements RowMapper<HistoricoCanalVendaVO>{

	@Override
	public HistoricoCanalVendaVO mapRow(java.sql.ResultSet rs, int rowNum)
			throws SQLException {
		
		ResultSetAdapter resultSet = new ResultSetAdapter(rs);
		
		HistoricoCanalVendaVO historicoCanalVendaVO = new HistoricoCanalVendaVO();
			
		historicoCanalVendaVO.setCodigoCanal(resultSet.getInteger("CCANAL_VDA_DNTAL_INDVD"));
		historicoCanalVendaVO.setCodigoPlano(resultSet.getInteger("CPLANO_DNTAL_INDVD"));
		historicoCanalVendaVO.setDescricao(resultSet.getString("RCANAL_VDA_DNTAL_INDVD"));
		historicoCanalVendaVO.setResponsavelAlteracao(resultSet.getString("CRESP_ULT_ATULZ"));
		historicoCanalVendaVO.setDataUltimaAtualizacao(rs.getDate("DULT_ATULZ_REG"));
		
		historicoCanalVendaVO.setDataInicioVigencia(resultSet.getDateTime("DINIC_VGCIA"));
		historicoCanalVendaVO.setDataFimVigencia(resultSet.getDateTime("DFIM_VGCIA"));
		
		
		return historicoCanalVendaVO;	
		
		
	}

	

}
