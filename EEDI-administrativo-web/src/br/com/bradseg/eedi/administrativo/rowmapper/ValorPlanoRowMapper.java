package br.com.bradseg.eedi.administrativo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.administrativo.support.ResultSetAdapter;
import br.com.bradseg.eedi.administrativo.vo.ValorPlanoVO;

public class ValorPlanoRowMapper implements RowMapper<ValorPlanoVO>{

public ValorPlanoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ResultSetAdapter resultSet = new ResultSetAdapter(rs);
		
		ValorPlanoVO valorPlanoVO = new ValorPlanoVO();

		valorPlanoVO.setSequencial(resultSet.getLong("NSEQ_VLR_PLANO_DNTAL"));
		valorPlanoVO.setDataInicio(resultSet.getDateTime("DINIC_VGCIA"));
		//valorPlanoVO.setDataFim(resultSet.getDateTime("DFIM_VGCIA"));
		valorPlanoVO.setValorAnualTitular(resultSet.getDouble("VANUDD_PLANO_DNTAL_TTLAR"));
		valorPlanoVO.setValorAnualDependente(resultSet.getDouble("VANUDD_PLANO_DNTAL_DEPDT"));
		valorPlanoVO.setValorMensalTitular(resultSet.getDouble("VMESD_PLANO_TTLAR"));
		valorPlanoVO.setValorMensalDependente(resultSet.getDouble("VMESD_PLANO_DEPDT"));
		valorPlanoVO.setValorTaxa(resultSet.getDouble("VTX_PLANO_INDVD"));
		valorPlanoVO.setValorDesconto(resultSet.getDouble("VDESC_PLANO_DNTAL"));

		return valorPlanoVO;
	}

}
