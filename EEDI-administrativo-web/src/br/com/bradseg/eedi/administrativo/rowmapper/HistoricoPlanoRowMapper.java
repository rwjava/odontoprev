package br.com.bradseg.eedi.administrativo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.administrativo.support.ResultSetAdapter;
import br.com.bradseg.eedi.administrativo.vo.HistoricoPlanoVO;

public class HistoricoPlanoRowMapper implements RowMapper<HistoricoPlanoVO>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HistoricoPlanoRowMapper.class);

	@Override
	public HistoricoPlanoVO mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 	
		
		//String pattern = "dd/MM/yyyy HH:mm:ss";
		
		ResultSetAdapter resultSet = new ResultSetAdapter(rs);
		
		HistoricoPlanoVO historicoPlanoVO = new HistoricoPlanoVO();
			
		historicoPlanoVO.setCodigoPlano(rs.getInt("CPLANO_DNTAL_INDVD"));//CPLANO_DNTAL_INDVD
		historicoPlanoVO.setDescricao(rs.getString("RPLANO_DNTAL_INDVD")); //OK
		historicoPlanoVO.setDataAlteracao(rs.getDate("DINCL_REG_TBELA")); //OK
		historicoPlanoVO.setCodigoResponsavelAtualizacao(rs.getString("CRESP_ULT_ATULZ"));//OK		
		historicoPlanoVO.setInicioVigencia(resultSet.getDateTime("DINIC_VGCIA"));
		historicoPlanoVO.setFimVigencia(resultSet.getDateTime("DFIM_VGCIA"));
		historicoPlanoVO.setPeriodoCarenciaMensal(rs.getString("RPER_CAREN_PLANO_MES"));
		historicoPlanoVO.setPeriodoCarenciaAnual(rs.getString("RPER_CAREN_PLANO_ANO"));		
		historicoPlanoVO.setDataAlteracaoFormatada(format.format(historicoPlanoVO.getDataAlteracao()));
		
		try {
			Date date = format.parse(historicoPlanoVO.getDataAlteracaoFormatada());
			historicoPlanoVO.setDataAlteracao(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error(e.getMessage());
		}
		
		
		return historicoPlanoVO;

		
	}

}




