package br.com.bradseg.eedi.administrativo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.administrativo.support.ResultSetAdapter;
import br.com.bradseg.eedi.administrativo.vo.PlanoVO;

/**
 * RowMapper para obter as informações da base de dados do plano.
 */
public class PlanoRowMapper implements RowMapper<PlanoVO>{

	public PlanoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ResultSetAdapter resultSet = new ResultSetAdapter(rs);
		
		PlanoVO planoVO = new PlanoVO();
		//List<TipoCobranca> listaDeTiposDeCobranca = Arrays.asList(TipoCobranca.values());
			
		planoVO.setCodigo(resultSet.getLong("CPLANO_DNTAL_INDVD"));
		planoVO.setNome(rs.getString("RPLANO_DNTAL_INDVD"));
		planoVO.setCodigoRegistro(rs.getLong("CREG_PLANO_DNTAL"));
		planoVO.setCodigoResponsavel(rs.getString("CRESP_ULT_ATULZ"));
		planoVO.setDataInicioVigencia(new DateTime(rs.getDate("DINIC_PLANO_INDVD").getTime()));
		planoVO.setDataFimVigencia(new DateTime(rs.getDate("DFIM_VGCIA").getTime()));
		planoVO.setDescricaoCarenciaPeriodoAnual(rs.getString("RPER_CAREN_PLANO_ANO"));
		planoVO.setDescricaoCarenciaPeriodoMensal(rs.getString("RPER_CAREN_PLANO_MES"));
		planoVO.setDataUltimaAtualizacao(new DateTime(rs.getDate("DULT_ATULZ_REG")));

		return planoVO;
	}

}
