package br.com.bradseg.eedi.administrativo.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.administrativo.support.ResultSetAdapter;
import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorVO;

public class CadastroCorretorRowMapper implements RowMapper<CadastroCorretorVO> {

	@Override
	public CadastroCorretorVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ResultSetAdapter resultSet = new ResultSetAdapter(rs);
		
		CadastroCorretorVO cadastroCorretorVO = new CadastroCorretorVO();
		// TODO Auto-generated method stub
		
		cadastroCorretorVO.setCnpj(resultSet.getLong("NM_TABELA"));
		cadastroCorretorVO.setSucursal(rs.getLong("NM_PROGRAMA"));
		cadastroCorretorVO.setNome(rs.getString("LINHA_TABELA"));
		cadastroCorretorVO.setStatus(rs.getInt("ACAO"));
		//cadastroCorretorVO.setDataInicioContrato(new DateTime(rs.getDate("DATULZ_REG").getTime()));
		cadastroCorretorVO.setDataFimContrato(new DateTime(rs.getDate("DATULZ_REG").getTime()));
		
		return cadastroCorretorVO;
	}

}
