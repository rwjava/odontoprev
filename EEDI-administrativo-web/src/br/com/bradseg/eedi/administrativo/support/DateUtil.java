package br.com.bradseg.eedi.administrativo.support;



import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;

/**
 * Classe que implementa Gregorian Calendar
 */
public class DateUtil extends GregorianCalendar {

	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);
	private static final long serialVersionUID = 1L;

	/**
	 * Zona a ser implementada
	 * 
	 * @param zone Zona a ser implementada
	 */	
	public DateUtil(final SimpleTimeZone zone) {
		super();
		this.setTimeZone(zone);
	}
	
	/**
	 * Classe que implementa fun��es de Data
	 */
	public DateUtil() {
		super();
	}
	
	/**
	 * Se vai limpar o Timeinfo
	 * 
	 * @param timeInfo Se vai limpar o Timeinfo
	 */
	public DateUtil(final boolean timeInfo) {
		super();
		if (!timeInfo) {
			this.clearTimeInfo();
		}
	}
	
	/**
	 * Data em mili segundos
	 * 
	 * @param milliseconds Data em mili segundos
	 */
	public DateUtil(final long milliseconds) {
		setTimeInMillis(milliseconds);
	}
	
	/**
	 * Data no formato Date
	 * 
	 * @param date Data no formato Date
	 */
	public DateUtil(final java.sql.Date date) {
		this.setSQLDate(date);
	}
	
	/**
	 * Data no formato Date
	 * 
	 * @param date Data no formato Date
	 */
	public DateUtil(final java.util.Date date) {
		this.setDate(date);
	}
	
	/**
	 * O timestamp
	 * 
	 * @param timestamp O timestamp
	 */
	public DateUtil(final Timestamp timestamp) {
		this.setTime(timestamp);
	}
	
	/**
	 * Hora em time
	 * 
	 * @param time Hora em time
	 */
	public DateUtil(final Time time) {
		this.setTime(time);
	}
	
	/**
	 * Valor a ser transformado
	 * 
	 * @param valor Valor a ser transformado
	 */
    public DateUtil(String valor)    {
        final SimpleDateFormat formatter = new SimpleDateFormat ("dd/MM/yyyy");
        formatter.setLenient(false);
        java.util.Date data = null;
        try {
			data = new java.util.Date();
        	data = formatter.parse(valor);
		} catch (ParseException e) {
			LOGGER.error(e.toString());
			data = new java.util.Date();
		}
        this.setDate(data);
    }
    
	/**
	 * Valor a ser transformado
	 * 
	 * @param valor Valor a ser transformado
	 * @param formato Formato a ser transformado
	 */
	public DateUtil(String valor,String formato)    {
		final SimpleDateFormat formatter = new SimpleDateFormat (formato);
		formatter.setLenient(false);
		java.util.Date data = null;
		try {
			data = new java.util.Date();
			data = formatter.parse(valor);
		} catch (ParseException e) {
			LOGGER.error(e.toString());
			data = new java.util.Date();
		}
		this.setDate(data);
	}
	
	/**
	 * Data em formato para SQL
	 * 
	 * @return Data em formato para SQL
	 */
	public Date getSQLDate() {
		return new Date(this.getTimeInMillis());
	}
	
	/**
	 * Data no formato java.sql.Date
	 * 
	 * @param date Data no formato java.sql.Date 
	 */
	public final void setSQLDate(java.sql.Date date) {
		this.setTime(date);
		clearTimeInfo();
	}
	
	/**
	 * Data no formato java.util.Date
	 * 
	 * @return Data no formato java.util.Date 
	 */
	public java.util.Date getDate() {
		return new Date(this.getTimeInMillis());
	}
	
	/**
	 * Data no formato java.util.Date
	 * 
	 * @param date Data no formato java.util.Date
	 */
	public final void setDate(java.util.Date date) {
		this.setTime(date);
	}
	
	/**
	 * Hora/Dia no formato timestamp
	 * 
	 * @return Hora/Dia no formato timestamp
	 */
	public Timestamp getSQLTimestamp() {
		return new Timestamp(this.getTimeInMillis());
	}
	
	/**
	 * Hora/Dia no formato timestamp
	 * 
	 * @param timestamp Hora/Dia no formato timestamp
	 */
	public void setSQLTimestamp(Timestamp timestamp) {
		this.setTime(timestamp);
	}
	
	public static Date formataData(String data) throws Exception {   
        if (data == null || data.equals("")) {
            return null;  
        }
          
        Date date = null;  
        try {  
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
            date = (Date)formatter.parse(data);  
        } catch (ParseException e) {              
            throw new BusinessException(e);  
        }  
        return date;  
    }  
	
	/**
	 * Hora/Dia no formato java.sql.Time
	 * 
	 * @return Hora/Dia no formato java.sql.Time
	 */
	public java.sql.Time getSQLTime() {
		return new Time(this.getTimeInMillis());
	}
	
	/**
	 * Hora/Dia no formato java.sql.Time
	 * 
	 * @param time Hora/Dia no formato java.sql.Time 
	 */
	public void setSQLTime(Time time) {
		this.setTime(time);
	}
	
	/**
	 * Hora em formato long
	 * 
	 * @param time Hora em formato long
	 */
	public void setSAFTimeInMillis(long time) {
		this.setTimeInMillis(time);
	}
	
	/**
	 * Hora em formato long
	 * 
	 * @return long Hora em formato long
	 */
	public long getSAFTimeInMillis() {
		return this.getTimeInMillis();
	}

	/**
	 * Formato a ser utilizado
	 * 
	 * @param formato Formato a ser utilizado
	 * @return String Data no formato especificado
	 */
	public String getSAFDateFormatado(String formato) {
		return new SimpleDateFormat(formato).format(this.getTime());
	}
	
	/**
	 * Data no formato String
	 * 
	 * @return String Data no formato String
	 */
	public String toString() {
		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.getTime());
	}
	
	/**
	 * Data no formato String
	 * 
	 * @return String Data no formato String
	 */
	public String getDia() {
		return new SimpleDateFormat("dd/MM/yyyy").format(this.getDate());
	}
		
	/**
	 * Data no formato String
	 * 
	 * @return String Data no formato String
	 */
	public String getHora() {
		return new SimpleDateFormat("HH:mm:ss").format(this.getDate());
	}
		
	/**
	 * Ano no formato especificado YYYY
	 * 
	 * @return  Ano no formato especificado YYYY
	 */
	public String getYear() {
		return new SimpleDateFormat("yyyy").format(this.getTime());
	}
	
	/**
	 * Mes no formato especificado MM
	 * 
	 * @return Mes no formato especificado MM
	 */
	public String getMonth() {
		return new SimpleDateFormat("MM").format(this.getTime());
	}
	
	/**
	 * Dia no formato especificado DD
	 * 
	 * @return Dia no formato especificado DD
	 */
	public String getDay() {
		return new SimpleDateFormat("dd").format(this.getTime());
	}
	
	/**
	 * Hora no formato especificado HH
	 * 
	 * @return Hora no formato especificado HH
	 */
	public String getHour() {
		return new SimpleDateFormat("HH").format(this.getTime());
	}
	
	/**
	 * Hora no formato especificado mm
	 * 
	 * @return Hora no formato especificado mm
	 */
	public String getMinutes() {
		return new SimpleDateFormat("mm").format(this.getTime());
	}
	
	/**
	 * Hora no formato especificado ss
	 * 
	 * @return Hora no formato especificado ss
	 */
	public String getSeconds() {
		return new SimpleDateFormat("ss").format(this.getTime());
	}
	
	/**
	 * Limpa data  
	*/
	public final void clearTimeInfo() {
		this.set(Calendar.HOUR_OF_DAY, 0);
		this.set(Calendar.MINUTE, 0);
		this.set(Calendar.SECOND, 0);
		this.set(Calendar.MILLISECOND, 0);
	}

	/**
	 * Limpa data  
	*/	
	public void lastTimeInfo() {
		this.set(Calendar.HOUR_OF_DAY, 23);
		this.set(Calendar.MINUTE, 59);
		this.set(Calendar.SECOND, 59);
		this.set(Calendar.MILLISECOND, 999);
	}

	/**
	 * Insere hora
	 * @param hora Hora
	 * @param minuto Minuto 
	 * @param segundo Segundo
	 * @param mili Milisegundos
	 */
	public void insereHora(int hora, int minuto, int segundo, int mili) {
		this.set(Calendar.HOUR_OF_DAY, hora);
		this.set(Calendar.MINUTE, minuto);
		this.set(Calendar.SECOND, segundo);
		this.set(Calendar.MILLISECOND, mili);
	}

	/**
	 * Insere hora
	 * @param hora Hora
	 */
	public void insereHora(java.util.Date hora) {
		DateUtil dia = new DateUtil(hora);
		this.set(Calendar.HOUR_OF_DAY, new Integer(dia.getHour()));
		this.set(Calendar.MINUTE, new Integer(dia.getMinutes()));
		this.set(Calendar.SECOND, new Integer(dia.getSeconds()));
		this.set(Calendar.MILLISECOND, 0);
	}

	/**
	 * Data no formato yyyy-MM-dd-HH.mm.ss
	 * 
	 * @return Data no formato yyyy-MM-dd-HH.mm.ss
	 */
	public String getTimestamp() {
		return new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss").format(this.getTime());
	}
	
	/**
	 * Soma dias (campo = campos do Calendar)
	 * @param campo Campo do calendar (YEAR,MONTH,DAY,HOUR,...) DAY = 5;
	 * @param valor Quantidade a somar
	 */
	public void somaData(int campo, int valor) {
		this.add(campo,valor);
	}

	/**
	 * Calcula diferen�a de dadtas
	 * @param initialDate Data inicial
	 * @param finalDate Data final
	 * @return Numero de dias
	 */
	public static int deductDates( Date initialDate, Date finalDate ) {  
	    if( initialDate == null || finalDate == null ) {  
	        return 0;  
	    }  
	    int days = ( int ) ( ( finalDate.getTime() - initialDate.getTime() )/( 24*60*60*1000 ) );  
	    if(days > 0) {
	    	return days;
	    } else {
	    	return 0;
	    }
	}  
	  
	/**
	 * Transforma date me String
	 * @param date Data
	 * @return Texto
	 */
	public static String dateToString( Date date ) {      
	       SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy", new Locale( "pt_BR" ) );  
	       return sdf.format( date );  
	}  
	
	
	  
	
	
	/**
	 * Numero de dias uteis
	 * @param initialDate Data inicial
	 * @param finalDate Data final
	 * @return numero de dias uteis
	 */
	public static int getWorkingDays( Date initialDate, Date finalDate ){  
	    int workingDays = 0;  
	    int totalDays = deductDates( initialDate, finalDate );  
	    Calendar calendar = new GregorianCalendar();
        calendar.setTime(initialDate);  
	    for( int i = 0; i <= totalDays; i++ ) {  
	        if( !( calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ) && !( calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ) ) {  
	            workingDays++;  
	        }  
	        calendar.add( Calendar.DATE, 1 );  
	    }  
	    return workingDays;  
	}  
	
	
	
}