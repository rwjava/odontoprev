package br.com.bradseg.eedi.administrativo.support.action;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.util.StrutsTypeConverter;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;

/**
 * Conversor de {@link LocalDate} para String e de String para {@link LocalDate} no padrao dd/MM/yyyy
 * 
 * @author WDEV
 */
public class LocalDateConverter extends StrutsTypeConverter {

	private static final Logger LOGGER = LoggerFactory.getLogger(LocalDateConverter.class);

	@SuppressWarnings("rawtypes")
	public Object convertFromString(Map map, String[] values, Class definedClass) {
		try {
			if (values != null && values.length > 0) {
				return parseDate(values[0]);
			}
		} catch (IllegalArgumentException e) {
			LOGGER.error("Erro ao converter string para LocalDate. Formato invalido", e);
			throw new BusinessException("msg.erro.formato.data.invalido");
		}
		return null;
	}

	private LocalDate parseDate(String localDate) {
		if (StringUtils.isBlank(localDate)) {
			return null;
		}
		return DateTimeFormat.forPattern("dd/MM/yyyy").parseDateTime(localDate).toLocalDate();
	}

	@SuppressWarnings("rawtypes")
	public String convertToString(Map map, Object localDate) {
		return ((LocalDate) localDate).toString("dd/MM/yyyy");
	}

}