package br.com.bradseg.eedi.administrativo.support;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Adaptador para converter uma String representando uma data no formato dd/MM/yyyy para um DateTime
 * 
 * @author WDEV
 */
public class DateTimeAdapter extends XmlAdapter<String, DateTime> {

	private static final String FORMATO_LOCAL_DATE_WEB_SERVICE = "dd/MM/yyyy";

	@Override
	public DateTime unmarshal(String data) {
		return DateTimeFormat.forPattern(FORMATO_LOCAL_DATE_WEB_SERVICE).parseDateTime(data).toDateTime();
	}
    
	@Override
	public String marshal(DateTime data) {
		return data.toString(FORMATO_LOCAL_DATE_WEB_SERVICE);
	}

	

}