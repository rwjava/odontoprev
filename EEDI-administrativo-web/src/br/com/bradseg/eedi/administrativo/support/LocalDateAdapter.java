package br.com.bradseg.eedi.administrativo.support;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

/**
 * Adaptador para converter uma String representando uma data no formato dd/MM/yyyy para um LocalDate
 * 
 * @author WDEV
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

	private static final String FORMATO_LOCAL_DATE_WEB_SERVICE = "dd/MM/yyyy";

	@Override
	public LocalDate unmarshal(String data) {
		return DateTimeFormat.forPattern(FORMATO_LOCAL_DATE_WEB_SERVICE).parseDateTime(data).toLocalDate();
	}

	@Override
	public String marshal(LocalDate data) {
		return data.toString(FORMATO_LOCAL_DATE_WEB_SERVICE);
	}

}