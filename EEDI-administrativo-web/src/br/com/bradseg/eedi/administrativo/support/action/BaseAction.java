package br.com.bradseg.eedi.administrativo.support.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.bradseg.bsad.filtrologin.filters.ConstantesLogin;
import br.com.bradseg.bsad.filtrologin.vo.LoginVo;
import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.FrameworkException;
import br.com.bradseg.bsad.framework.core.message.Message;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Classe base para as actions
 * 
 * @author WDEV
 */
public class BaseAction extends ActionSupport {

	private static final long serialVersionUID = -8201118430792396214L;

	@Autowired
	protected transient Sessions sessions;

	public String getIdentificacaoUsuarioLogado() {
		
		LoginVo loginVo = sessions.get(ConstantesLogin.LOGIN_SESSION_KEY, LoginVo.class);
		
		if(loginVo != null){
			return loginVo.getId();
		}
		
		return "";
	}

	protected LoginVo getLogin() {
		return sessions.get(ConstantesLogin.LOGIN_SESSION_KEY, LoginVo.class);
	}
	
	protected void sucesso(String key, String... params) {
		addActionMessage(getText(key, params));
	}

	protected void erro(Exception exception) {
		if (exception instanceof BusinessException) {
			adicionarErro((BusinessException) exception);
		} else {
			erro(exception.getMessage());
		}
	}
	
	private void adicionarErro(FrameworkException exception) {
		if (exception.getMessages() == null || exception.getMessages().isEmpty()) {
			addActionError(exception.getMessage());
		} else {
			for (Message message : exception.getMessages()) {
				addActionError(message.getMessage());
			}
		}
	}

	protected void erro(String key) {
		addActionError(getText(key));
	}

	protected HttpServletResponse getResponse() {
		return ServletActionContext.getResponse();
	}
	
	public static HttpServletRequest getRequest() {
		return ServletActionContext.getRequest();
	}

}