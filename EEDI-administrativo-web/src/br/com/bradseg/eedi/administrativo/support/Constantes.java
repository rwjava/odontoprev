package br.com.bradseg.eedi.administrativo.support;


/**
 * Constantes da aplica��o.
 */
public class Constantes {
	
	public static final String FORMATO_DATA_PT_BR = "dd/MM/yyyy";
	
	//public static final BigDecimal ZERO_DOUBLE = BigDecimal.ZERO;
	
	public static final Integer INDICA_REMOVIDO = 1;
	
	public static final String TABELA_PLANO = "DBPROD.PLANO_DNTAL_INDVD";
	
	public static final String ZERO = "0";
	public static final Double ZERO_DOUBLE =  0.0;
	public static final int NAO_ALTERA =  0;
	public static final int ALTERA =  1;
	public static final String STRING_VAZIA = "";
	
	public static final String WEB_SERVICE_PARAM_DATA = "dataMovimento";

}
