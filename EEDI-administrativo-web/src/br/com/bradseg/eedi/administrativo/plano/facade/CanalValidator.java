package br.com.bradseg.eedi.administrativo.plano.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bradseg.eedi.administrativo.support.Validador;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;

@Component
public class CanalValidator {
	
	@Autowired
	private CanalVendaServiceFacade canalVendaServiceFacade;
	
	public void validarInclusaoCanalVenda(CanalVendaVO canalVendaVO){
		
		Validador validador = new Validador();
		
		validador.obrigatorio(canalVendaVO.getSigla(), "Sigla");
		validador.obrigatorio(canalVendaVO.getNome(), "Nome");
		//validador.obrigatorio(canalVendaVO.getCodigo(),"C�digo");
	
		if(validador.getMensagens().isEmpty()){
			verificarExistenciaSiglaCanalVendaInclusao(canalVendaVO, validador);
			verificarExistenciaNomeCanalVenda(canalVendaVO, validador);
			//verificarExistenciaCodigoCanalVendaInclusao(canalVendaVO, validador);
		}

		validador.validar();
	}
	
	
	public void adicionarErroEValidar(String mensagem){
		Validador validador = new Validador();
		
		validador.adicionarErro(mensagem);
		validador.validar();
	}
	
	private void verificarExistenciaSiglaCanalVendaInclusao(CanalVendaVO canalVendaVO,Validador validador) {

		if(canalVendaServiceFacade.verificarExistenciaCanalVendaPorSigla(canalVendaVO.getSigla().toUpperCase())){
			validador.adicionarErro("msg.erro.canalVenda.sigla.existente");
		}
	}
	
	private void verificarExistenciaNomeCanalVenda(CanalVendaVO canalVendaVO, Validador validador) {
	
		if(canalVendaServiceFacade.verificarExistenciaCanalVendaPorNome(canalVendaVO.getNome().toUpperCase())){
			validador.adicionarErro("msg.erro.canalVenda.nome.existente");
		}
	}

//	private void verificarExistenciaCodigoCanalVendaInclusao(CanalVendaVO canalVendaVO, Validador validador) {
//	
//		if(canalVendaServiceFacade.verificarExistenciaCanalVendaPorCodigo(canalVendaVO.getCodigo())){
//			validador.adicionarErro("msg.erro.canalVenda.codigo.existente");
//		}
//	}
	
	
}