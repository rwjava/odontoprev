package br.com.bradseg.eedi.administrativo.plano.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.administrativo.plano.dao.CanalVendaDao;
import br.com.bradseg.eedi.administrativo.support.Validador;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaViewVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroCanalVendaVO;


@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CanalVendaServiceFacadeImpl implements CanalVendaServiceFacade{

	//private static final Logger LOGGER = LoggerFactory.getLogger(CanalVendaServiceFacadeImpl.class);
	
	@Autowired
	private CanalVendaDao canalVendaDao;
	
	@Autowired
	private CanalValidator canalValidator;
	
	@Override
	public List<CanalVendaVO> listarTodosCanaisVenda() {

		return canalVendaDao.listarTodosCanaisVenda();
	}

	@Override
	public List<CanalVendaVO> consultarCanalVenda(FiltroCanalVendaVO filtro) {

		return canalVendaDao.consultarCanalVenda(filtro);
	}
	
	public void incluirCanalVenda(CanalVendaVO canalVendaVO){
		
		canalValidator.validarInclusaoCanalVenda(canalVendaVO);
		
		try{
			canalVendaDao.incluirCanalVenda(canalVendaVO);
		}catch(Exception e){
			canalValidator.adicionarErroEValidar("msg.erro.canal.plano.erro.incluir");
		//	throw new BusinessException("Erro ao incluir canal");
		}
		
	}

	@Override
	public boolean verificarExistenciaCanalVendaPorSigla(String  sigla) {

		return canalVendaDao.verificarExistenciaSiglaCanalVenda(sigla);
	}

	@Override
	public boolean verificarExistenciaCanalVendaPorNome(String nomeCanal) {

		return canalVendaDao.verificarExistenciaNomeCanalVenda(nomeCanal);
	}

	@Override
	public boolean verificarExistenciaCanalVendaPorCodigo(
			Integer codigoCanal ) {

		return canalVendaDao.verificarExistenciaCodigoCanalVendaInclusao(codigoCanal);
	}
	

	@Override
	public CanalVendaVO obterCanalDeVendaPorCodigo(Integer codigo){
		
		CanalVendaVO canalVendaVO;
		try{
			canalVendaVO =  canalVendaDao.obterCanalDeVendaPorCodigo(codigo);
		}catch(Exception e){
			throw new BusinessException("msg.erro.canal.plano.nao.encontrado");
		}
		return canalVendaVO;
	}

	@Override
	public void validarPreenchimentoDosCampos(CanalVendaViewVO canalVendaVO) {

		Validador validador = new Validador();
		
		validador.obrigatorio(canalVendaVO.getSigla(), "Sigla");
		validador.obrigatorio(canalVendaVO.getNome(), "Nome");
		//validador.obrigatorio(canalVendaVO.getCodigo(),"C�digo");
		
		validador.validar();
	}

	@Override
	public void validarExistenciaCampos(CanalVendaVO canalVendaVO,
			CanalVendaViewVO canalVendaViewVO) {
		
		Validador validador = new Validador();
//		if(canalVendaViewVO.getCodigo() != canalVendaVO.getCodigo()){
//			if(verificarExistenciaCanalVendaPorCodigo(canalVendaViewVO.getCodigo())){
//				validador.adicionarErro("msg.erro.canalVenda.sigla.existente");
//			}
//		}
		if(!canalVendaViewVO.getNome().equalsIgnoreCase(canalVendaVO.getNome())){
			if(verificarExistenciaCanalVendaPorNome(canalVendaViewVO.getNome())){
				validador.adicionarErro("msg.erro.canalVenda.nome.existente");
			}
		}
		if(!canalVendaViewVO.getSigla().equalsIgnoreCase(canalVendaVO.getSigla())){
			if(verificarExistenciaCanalVendaPorSigla(canalVendaViewVO.getSigla())){
				validador.adicionarErro("msg.erro.canalVenda.codigo.existente");
			}
		}
		
		validador.validar();
	}

	@Override
	public void alterarCanal(CanalVendaVO canalVendaVO) {
		
		try{
			canalVendaDao.alterarCanal(canalVendaVO);
		}catch(Exception e){
			//sthrow new BusinessException("msg.erro.canal.plano.erro.alterar");
			canalValidator.adicionarErroEValidar("msg.erro.canal.plano.erro.alterar");
		}
	}


	@Override
	public List<CanalVendaVO> obterCanaisPorCodigoPlano(Long codigoPlano) {
		// TODO Auto-generated method stub
		return canalVendaDao.obterCanaisPorCodigoPlano(codigoPlano);
	}

}
