package br.com.bradseg.eedi.administrativo.plano.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.eedi.administrativo.plano.facade.CanalVendaServiceFacade;
import br.com.bradseg.eedi.administrativo.support.Strings;
import br.com.bradseg.eedi.administrativo.support.action.BaseAction;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaViewVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroCanalVendaVO;

/**
 * Action respons�vel por disponibilizar os metodos referentes as a��es de tela da funcionalidade plano.
 * 
 * @author WDEV
 */
@Controller
@Scope("request")
public class CanalAction extends BaseAction {

	private static final long serialVersionUID = -1161409943678292285L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CanalAction.class);
	
	private CanalVendaVO canalVendaVO = new CanalVendaVO();
	
	private CanalVendaViewVO canalVendaViewVO = new CanalVendaViewVO();
	private FiltroCanalVendaVO filtroCanalVendaVO = new FiltroCanalVendaVO();

	@Autowired
	private CanalVendaServiceFacade canalVendaServiceFacade;
	
	public String listarPorFiltroCanal(){
		
	try{	
		if(!Strings.isBlankOrNull(canalVendaViewVO.getNome())){
			filtroCanalVendaVO.setNome(canalVendaViewVO.getNome());
		}
		if(!Strings.isBlankOrNull(canalVendaViewVO.getSigla())){
			filtroCanalVendaVO.setSigla(canalVendaViewVO.getSigla());
		}
		
		if (filtroCanalVendaVO.isFiltroPreenchido()) {
			canalVendaViewVO.setListaCanalVenda(canalVendaServiceFacade.consultarCanalVenda(filtroCanalVendaVO));
		}else{
			canalVendaViewVO.setListaCanalVenda(canalVendaServiceFacade.listarTodosCanaisVenda());
		}
	} catch (Exception e) {
		LOGGER.error("Erro ao listar os planos.", e);
		erro(e);
	}
	
		return SUCCESS;
	}
	
	public String iniciarIncluirCanal(){
		
		return SUCCESS;
	}
	
	public String incluirCanal(){
		
		preencherCanalVendaVO();
		
		canalVendaServiceFacade.incluirCanalVenda(canalVendaVO);
		sucesso("msg.sucesso.cadastro.canal", "Canal de Venda");
		listarCanais();
		
		return SUCCESS;
	}
	
	public String iniciarAlterarCanal(){
		
		canalVendaVO = canalVendaServiceFacade.obterCanalDeVendaPorCodigo(canalVendaVO.getCodigo());
		
		canalVendaViewVO.setCodigo(canalVendaVO.getCodigo());
		canalVendaViewVO.setNome(canalVendaVO.getNome());
		canalVendaViewVO.setSigla(canalVendaVO.getSigla());
		
		return SUCCESS;
	}
	
	
	public String iniciarVisualizarCanal(){
		
		canalVendaVO = canalVendaServiceFacade.obterCanalDeVendaPorCodigo(canalVendaVO.getCodigo());
		
		
		return SUCCESS;
	}
	
	public String alterarCanal(){
		
		canalVendaServiceFacade.validarPreenchimentoDosCampos(canalVendaViewVO);
		
		//TODO esse campo de c�digo ser� substitu�do por uma sequence
		canalVendaVO = canalVendaServiceFacade.obterCanalDeVendaPorCodigo(canalVendaVO.getCodigo());
		canalVendaServiceFacade.validarExistenciaCampos(canalVendaVO, canalVendaViewVO);
		
		canalVendaVO.setNome(canalVendaViewVO.getNome());
		canalVendaVO.setSigla(canalVendaViewVO.getSigla());
		
		canalVendaVO.setCodigoResponsavel(getIdentificacaoUsuarioLogado());
		
		canalVendaServiceFacade.alterarCanal(canalVendaVO);
		sucesso("msg.alteracao.canal.sucesso", "Canal de Venda");
		listarCanais();
		
		return SUCCESS;
	}
	
	
     public String listarCanais(){
		
    	 try{
    		 canalVendaViewVO.setListaCanalVenda(canalVendaServiceFacade.listarTodosCanaisVenda());
    		 canalVendaViewVO.setCodigo(null);
    		 canalVendaViewVO.setNome(null);
    		 canalVendaViewVO.setSigla(null);
    	 }catch(Exception e){
    		 LOGGER.error("INFO: ERRO AO LISTAR CANAIS");
    	 }
		return SUCCESS;
	}
     
     

	public CanalVendaVO getCanalVendaVO() {
		return canalVendaVO;
	}

	public void setCanalVendaVO(CanalVendaVO canalVendaVO) {
		this.canalVendaVO = canalVendaVO;
	}

	public CanalVendaViewVO getCanalVendaViewVO() {
		return canalVendaViewVO;
	}

	public void setCanalVendaViewVO(CanalVendaViewVO canalVendaViewVO) {
		this.canalVendaViewVO = canalVendaViewVO;
	}

	public FiltroCanalVendaVO getFiltroCanalVendaVO() {
		return filtroCanalVendaVO;
	}

	public void setFiltroCanalVendaVO(FiltroCanalVendaVO filtroCanalVendaVO) {
		this.filtroCanalVendaVO = filtroCanalVendaVO;
	}
	
	private void preencherCanalVendaVO(){
		
		canalVendaVO.setCodigo(canalVendaViewVO.getCodigo());
		canalVendaVO.setNome(canalVendaViewVO.getNome());
		canalVendaVO.setSigla(canalVendaViewVO.getSigla());
		
		canalVendaVO.setCodigoResponsavel(getIdentificacaoUsuarioLogado());
	}
	
	
}