package br.com.bradseg.eedi.administrativo.plano.dao;

import java.util.List;

import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorVO;

public interface CadastroCorretorDao {
	
	public List<CadastroCorretorVO> listarCorretores();
	
	public CadastroCorretorVO obterDadosCorretorPorCnpj(Long cnpj);

	public List<CadastroCorretorVO> listarCorretorPorSucursal(Long sucursal);

	public List<CadastroCorretorVO> listarCorretorPorSucursalEStatus(Long sucursal, Integer status);

	public void atualizarVigencia(CadastroCorretorVO cadastroCorretorVO);

	public void incluirAcessoCorretor(CadastroCorretorVO cadastroCorretorVO);

	public boolean verificarExistenciaCorretorPorCnpj(Long cnpj);
	
}
