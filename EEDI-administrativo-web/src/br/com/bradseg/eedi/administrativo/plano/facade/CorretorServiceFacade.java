package br.com.bradseg.eedi.administrativo.plano.facade;

import java.util.List;

import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorVO;
import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorViewVO;

public interface CorretorServiceFacade {

	/**
	 * 
	 * @param cadastroCorretorVO
	 */
	public void validarPreenchimentoCampoDeConsulta(CadastroCorretorViewVO cadastroCorretorVO);

	
	/**
	 * 
	 * @param cadastroCorretorViewVO
	 */
	public void validarPreenchimentoCamposCadastroCorretor(CadastroCorretorViewVO cadastroCorretorViewVO);
	
	
	/**
	 * 
	 * @param cadastroCorretorViewVO
	 */
	public void validarCampoDataFimVigenciaAlteracaoCorretor(CadastroCorretorViewVO cadastroCorretorViewVO);
	
	
	/**
	 * 
	 * @return
	 */
	public List<CadastroCorretorVO> listarCorretores();

	
	public CadastroCorretorVO obterDadosCorretorPorCnpj(Long cnpj);


	public List<CadastroCorretorVO> listarCorretoresPorSucursal(Long sucursal);


	public List<CadastroCorretorVO> listarCorretorPorSucursalEStatus(Long sucursal, Integer status);
	
	
	public void atualizarVigencia(CadastroCorretorVO cadastroCorretorVO);


	public void incluirAcessoCorretor(CadastroCorretorVO cadastroCorretorVO);


	public void validarCnpjEDataVigenciaContrato(CadastroCorretorVO cadastroCorretorVO);


	public boolean verificarExistenciaCorretorPorCnpj(Long cnpj);
	
	
	public void verificarSeHouveAlteracaoDoCorretor(CadastroCorretorVO cadastroCorretorAnterior, CadastroCorretorVO cadastroCorretorVO);


	
}
