package br.com.bradseg.eedi.administrativo.plano.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.administrativo.plano.dao.PlanoDao;
import br.com.bradseg.eedi.administrativo.support.DateTimeAdapter;
import br.com.bradseg.eedi.administrativo.support.Validador;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroPlanoVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoViewVO;
import br.com.bradseg.eedi.administrativo.vo.Status;
import br.com.bradseg.eedi.administrativo.vo.ValorPlanoVO;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class PlanoServiceFacadeImpl implements PlanoServiceFacade{

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanoServiceFacadeImpl.class);
	
	@Autowired
	private transient PlanoDao planoDao;
	
	@Autowired
	private PlanoValidator planoValidator;
	
	@Autowired
	private CanalVendaServiceFacade CanalVendaServiceFacade;
	
	@Override
	public List<PlanoVO> listarPlanos() {

		return planoDao.listarPlanos();
	}
	
	@Override
	public List<PlanoVO>consultarPlanos(FiltroPlanoVO filtro){
		
		return planoDao.consultarPlanos(filtro);
	}

	@Override
	public void validarPreenchimentoCampos(PlanoViewVO planoVO) {
		// TODO Auto-generated method stub
		
		planoValidator.validarPreenchimentoCampos(planoVO);
		
	}

	
	@Override
	public void validarNomeEDataVigenciaPlano(PlanoVO planoVO) {
		Validador validador = new Validador();
		validarNomePlano(planoVO.getNome(), validador);
		//validarDataVigenciaPlano(planoVO, validador);
		validador.validar();
	}
	
	public void validarNomePlano(String nomePlano, Validador validador){
		
		if(verificarExistenciaPlanoPorCodigo(nomePlano)){
			validador.adicionarErro("msg.erro.plano.nome.informado");
		}
	}
	
	public void validarDataVigenciaPlano(PlanoVO planoVO, Validador validador){
		 if(planoVO.getDataInicioVigencia().isAfter(planoVO.getDataFimVigencia())){
			 validador.adicionarErro("msg.erro.dataFim.dataInicio");
			 //validador.validar();
		 }
		 
		 
		 if(planoVO.getDataFimVigencia().toLocalDate().isBefore(new LocalDate())){
			 validador.adicionarErro("msg.erro.datafim.menor.que.data.atual");
		 }
	}
	
	public void validarNomePlano(String nomePlano){
		
		Validador validador = new Validador();
		if(verificarExistenciaPlanoPorCodigo(nomePlano)){
			validador.adicionarErro("msg.erro.plano.nome.informado");
		}

		validador.validar();
}

       public void validarDataVigenciaPlano(PlanoVO planoVO){
	
    	 Validador validador = new Validador();
	     //validar se data fim � maior que data in�cio
		 if(planoVO.getDataInicioVigencia().isAfter(planoVO.getDataFimVigencia())){
			 validador.adicionarErro("msg.erro.dataFim.dataInicio");
		 }
		 validador.validar();
}
	
	
	@Override
	public boolean verificarExistenciaPlanoPorCodigo(String nomePlano) {

		return planoDao.verificarExistenciaPlanoPorCodigo(nomePlano); 
	
	}

	@Override
	public void validarCanaisVenda(PlanoVO planoVO) {
		
		Validador validador = new Validador();
		if(planoVO.getListaDeCanais() != null && !planoVO.getListaDeCanais().isEmpty()){
			//validar se c�digo de canal existe na base
			//validar se canais de venda j� n�o est�o cadastrados em outro plano em data que interfere no plano novo			
			for(CanalVendaVO canalVendaVO : planoVO.getListaDeCanais()){					
					//verificar se canal de venda n�o existe na base
					if(!validarExistenciaCanalPorCodigo(canalVendaVO.getCodigo(), validador)){
						validador.adicionarErro("msg.erro.canalVenda.codigo.nao.existente", canalVendaVO.getCodigo());
					}else{
						//caso exista na base, verificar se ele j� est� associado a algum plano
						//caso esteja, verificar se a diferen�a de datas � maior que 1 dia
						//caso seja, deve adicionar mensagem de erro
						PlanoVO plano = obterPlanoMaisRecentePorCodigoCanal(canalVendaVO.getCodigo());
						//caso o plano mais recente com o canal esteja expirado
						//a valida��o de intervalo entre planos n�o deve ser feita
						if(plano != null &&  !Status.EXPIRADO.getDescricao().equals(plano.getStatus())){

							if(Status.PROGRAMADO.getDescricao().equals(plano.getStatus()) && !plano.getCodigo().equals(planoVO.getCodigo())){
								
								if(planoVO.getDataInicioVigencia().isBefore(plano.getDataInicioVigencia())){
									Days intervaloData = Days.daysBetween(planoVO.getDataFimVigencia(), plano.getDataInicioVigencia());
									if(intervaloData.getDays() != 1){
										validador.adicionarErro("msg.erro.data.planoProgramado.comIntervaloDeUmDia", canalVendaVO.getNome(), plano.getNome(), new DateTimeAdapter().marshal(plano.getDataInicioVigencia()), "data fim de vig�ncia", new DateTimeAdapter().marshal(plano.getDataInicioVigencia().minusDays(1)));
									}
								}else{
									Days intervaloData = Days.daysBetween(planoVO.getDataFimVigencia(), plano.getDataInicioVigencia());
									if(intervaloData.getDays() != 1){
										validador.adicionarErro("msg.erro.data.planoProgramado.comIntervaloDeUmDia", canalVendaVO.getNome(), plano.getNome(), new DateTimeAdapter().marshal(plano.getDataInicioVigencia()), "data in�cio de vig�ncia", new DateTimeAdapter().marshal(plano.getDataFimVigencia().plusDays(1)));
									}
								}
							}
							else{
								if(planoVO.getCodigo() == null && plano != null){
									Days intervaloData = Days.daysBetween(plano.getDataFimVigencia(), planoVO.getDataInicioVigencia());
									validador.verdadeiro((intervaloData.getDays() == 1), "msg.erro.plano.dataVigenciaPlano.comIntervaloDeUmDia", canalVendaVO.getNome(),plano.getNome(), new DateTimeAdapter().marshal(plano.getDataFimVigencia()),  new DateTimeAdapter().marshal(plano.getDataFimVigencia().plusDays(1)));							
								}else{
									if(plano != null){							
										if(!planoVO.getDataInicioVigencia().equals(plano.getDataInicioVigencia()) || !planoVO.getDataFimVigencia().equals(plano.getDataFimVigencia())){	
											
											if(plano.getCodigo().longValue() != planoVO.getCodigo().longValue()){
												Days intervaloData = Days.daysBetween(plano.getDataFimVigencia(), planoVO.getDataInicioVigencia());
												validador.falso((intervaloData.getDays() < 1), "msg.erro.plano.dataVigenciaPlano.comIntervaloDeUmDia",
														canalVendaVO.getNome(),"Data In�cio Vig�ncia", "ap�s a", new DateTimeAdapter().marshal(plano.getDataFimVigencia()));	
											}
												
										}
										
										else if((planoVO.getDataInicioVigencia().equals(plano.getDataInicioVigencia()) && planoVO.getDataFimVigencia().equals(plano.getDataFimVigencia())) && (plano.getCodigo().longValue() != planoVO.getCodigo().longValue())){
											
												Days intervaloData = Days.daysBetween(plano.getDataInicioVigencia(), planoVO.getDataFimVigencia());
												validador.verdadeiro((intervaloData.getDays() < 1), "msg.erro.plano.dataVigenciaPlano.comIntervaloDeUmDia",
													canalVendaVO.getNome(),"Data In�cio Vig�ncia", "ap�s a", new DateTimeAdapter().marshal(plano.getDataFimVigencia()));									
										}
									}
									
								}
							}
						}
							
					}					
			}
			validador.validar();			
		}
		
	}
	
	
	public void validarCanaisVendaNovos(PlanoVO planoVO, PlanoVO planoAnterior){
		
		List<CanalVendaVO> listaDeNovosCanais = new ArrayList<CanalVendaVO>();
		
		listaDeNovosCanais.addAll(planoVO.getListaDeCanais());
		listaDeNovosCanais.removeAll(planoAnterior.getListaDeCanais());
//		for (CanalVendaVO canalTela : planoVO.getListaDeCanais()) {
//			for (CanalVendaVO canalAnterior : planoAnterior.getListaDeCanais()){
//				if(canalTela.getCodigo() != canalAnterior.getCodigo()){
//					listaDeNovosCanais.add(canalTela);
//				}					
//			}				
//		}
		
		Validador validador = new Validador();
		if(!listaDeNovosCanais.isEmpty()){
			//validar se c�digo de canal existe na base
			//validar se canais de venda j� n�o est�o cadastrados em outro plano em data que interfere no plano novo			
			for(CanalVendaVO canalVendaVO : listaDeNovosCanais){					
					//verificar se canal de venda n�o existe na base
					if(!validarExistenciaCanalPorCodigo(canalVendaVO.getCodigo(), validador)){
						validador.adicionarErro("msg.erro.canalVenda.codigo.nao.existente", canalVendaVO.getCodigo());
					}else{
						//caso exista na base, verificar se ele j� est� associado a algum plano
						//caso esteja, verificar se a diferen�a de datas � maior que 1 dia
						//caso seja, deve adicionar mensagem de erro
						PlanoVO plano = obterPlanoMaisRecentePorCodigoCanal(canalVendaVO.getCodigo());
						//caso o plano mais recente com o canal esteja expirado
						//a valida��o de intervalo entre planos n�o deve ser feita
						if(plano != null &&  !Status.EXPIRADO.getDescricao().equals(plano.getStatus())){
							
							if(planoVO.getCodigo() == null && plano != null){
								Days intervaloData = Days.daysBetween(plano.getDataFimVigencia(), planoVO.getDataInicioVigencia());
								validador.verdadeiro((intervaloData.getDays() == 1), "msg.erro.plano.dataVigenciaPlano.comIntervaloDeUmDia",
										canalVendaVO.getNome(),"Data In�cio Vig�ncia", "ap�s a", new DateTimeAdapter().marshal(plano.getDataFimVigencia()));							
							}else{
								if(plano != null){							
									if(!planoVO.getDataInicioVigencia().equals(plano.getDataInicioVigencia()) || !planoVO.getDataFimVigencia().equals(plano.getDataFimVigencia())){	
										
											if(plano.getCodigo().longValue() != planoVO.getCodigo().longValue()){
												Days intervaloData = Days.daysBetween(plano.getDataFimVigencia(), planoVO.getDataInicioVigencia());
												validador.falso((intervaloData.getDays() < 1), "msg.erro.plano.dataVigenciaPlano.comIntervaloDeUmDia",
														canalVendaVO.getNome(),"Data In�cio Vig�ncia", "ap�s a", new DateTimeAdapter().marshal(plano.getDataFimVigencia()));	
											}
											
									}
									
									else if((planoVO.getDataInicioVigencia().equals(plano.getDataInicioVigencia()) && planoVO.getDataFimVigencia().equals(plano.getDataFimVigencia()))&& 
										   (plano.getCodigo().longValue() != planoVO.getCodigo().longValue())){
										
											Days intervaloData = Days.daysBetween(plano.getDataInicioVigencia(), planoVO.getDataFimVigencia());
											validador.verdadeiro((intervaloData.getDays() < 1), "msg.erro.plano.dataVigenciaPlano.comIntervaloDeUmDia",
												canalVendaVO.getNome(),"Data In�cio Vig�ncia", "ap�s a", new DateTimeAdapter().marshal(plano.getDataFimVigencia()));									
									}
								}
								
							}
						}
							
					}					
			}
			validador.validar();			
		}
	}
	
	@Override
	public PlanoVO obterPlanoMaisRecentePorCodigoCanal(Integer codigoCanal){
		return planoDao.obterPlanoMaisRecentePorCodigoCanal(codigoCanal);
		
	}
	
	@Override
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano){
		PlanoVO planoVO = null;
		try{
		   planoVO =  planoDao.obterPlanoPorCodigo(codigoPlano);
		   planoVO.setListaTipoSucursal(planoDao.obterSegmentoAssociadoAoPlano(codigoPlano));
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO BUSCAR PLANO");
			throw new BusinessException("ERRO AO BUSCAR PLANO");
		}
		return planoVO;
		
	}
	
	private boolean validarExistenciaCanalPorCodigo(Integer codigoCanal, Validador validador){
		
		return CanalVendaServiceFacade.verificarExistenciaCanalVendaPorCodigo(codigoCanal);
	}

	
	@Transactional(propagation = Propagation.REQUIRED)
	public void incluir(PlanoVO planoVO){
		try{
			incluirPlano(planoVO);
			incluirValoresPlano(planoVO);
			incluirVigenciaCanais(planoVO);
			incluirSegmentos(planoVO);
		}catch(Exception e){
			planoValidator.adicionarErroEValidar("msg.erro.plano.erro.incluir");
			LOGGER.error(e.getMessage());
		}
	}
	
	

	@Override
	public boolean verificarSeHouveAlteracaoDeValorTaxaOuDesconto(PlanoVO planoVO, PlanoVO planoAnterior){
		
		boolean houveAlteracao = false;
		
		if(!planoVO.getValorPlanoVO().getValorTaxa().equals(planoAnterior.getValorPlanoVO().getValorTaxa())){
			houveAlteracao = true;
		}else if(!planoVO.getValorPlanoVO().getValorDesconto().equals(planoAnterior.getValorPlanoVO().getValorDesconto())){
			houveAlteracao = true;
		}
		
		return houveAlteracao;
	}
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void atualizarValoresTaxaEDescontoVigenteEProgramdado(PlanoVO planoVO){
		
		ValorPlanoVO valorPlanoVigente = planoDao.obterValorAtualPlanoPorCodigo(planoVO.getCodigo());
		ValorPlanoVO valorPlanoProgramado = planoDao.obterNovoValorPlanoPorCodigo(planoVO.getCodigo());
		
		if(valorPlanoVigente != null){
			
			valorPlanoVigente.setValorTaxa(planoVO.getValorPlanoVO().getValorTaxa());
			valorPlanoVigente.setValorDesconto(planoVO.getValorPlanoVO().getValorDesconto());
			planoDao.atualizarValoresTaxaEDesconto(valorPlanoVigente);
		}
		if(valorPlanoProgramado != null){
			
			valorPlanoProgramado.setValorTaxa(planoVO.getValorPlanoVO().getValorTaxa());
			valorPlanoProgramado.setValorDesconto(planoVO.getValorPlanoVO().getValorDesconto());
			planoDao.atualizarValoresTaxaEDesconto(valorPlanoProgramado);
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void incluirPlano(PlanoVO planoVO) {
		planoDao.incluirPlano(planoVO);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void incluirVigenciaCanais(PlanoVO planoVO) {
		if(planoVO.getListaDeCanais() != null){
			for(CanalVendaVO canalVendaVO : planoVO.getListaDeCanais()){
				if(verificarSeCanalEstaAssociadoAoPlano(planoVO.getCodigo(), canalVendaVO.getCodigo(), planoVO.getDataInicioVigencia().toDate())){
					planoDao.atualizarVigencia(planoVO.getCodigo(), canalVendaVO.getCodigo(), planoVO.getDataInicioVigencia().toDate());
				}else{
					planoDao.incluirVigenciaCanal(canalVendaVO, planoVO);
				}				
			}
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void incluirTodasVigencias(PlanoVO planoVO, List<CanalVendaVO> listaDeCanais) {
		if(listaDeCanais != null){
			for(CanalVendaVO canalVendaVO : listaDeCanais){
				planoDao.incluirVigenciaCanal(canalVendaVO, planoVO);
			}				
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	private void incluirSegmentos(PlanoVO planoVO) {
		if(null != planoVO.getListaTipoSucursal() && !planoVO.getListaTipoSucursal().isEmpty()){
			for (Integer segmento: planoVO.getListaTipoSucursal()) {
					planoDao.incluirSegmento(segmento, planoVO);
			}
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	private boolean consultaSegmento(PlanoVO planoVO){
		boolean dataSeg = false;
		if(planoDao.consultarData() == true){
			dataSeg = true;
			return dataSeg; 
		}
		return false;
	}

	/*
	 * n�o funciona o update 
	 * 
	 * if(planoDao.consultarData() == true){
				for(Integer segmento : planoVO.getListaTipoSucursal()){
					planoDao.incluirAlterarSeguimento(segmento, planoVO);
				}
			}else{
				for(Integer segmento: planoVO.getListaTipoSucursal()){
					planoDao.incluirSegmento(segmento, planoVO);
				}
			}*\
	 */
	
	private boolean verificarSeCanalEstaAssociadoAoPlano(Long codigoPlano, Integer codigoCanal, Date dataInicioVigencia) {
		return planoDao.verificarSeCanalEstaAssociadoAoPlano(codigoPlano, codigoCanal, dataInicioVigencia);
	}

	public Long obterValorSequence(){
		return planoDao.obterNovoValorSequence();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void incluirValoresPlano(PlanoVO planoVO) {
		
		if(planoVO.isExisteValorProgramado()){
			planoVO.getValorPlanoVO().setDataFim(new DateTime(planoVO.getDataProgramadaNovoValorPlano()).minusDays(1));
			planoDao.desativarValoresPlano(planoVO);
			planoVO.getNovoValorPlanoVO().setDataInicio(new DateTime(planoVO.getDataProgramadaNovoValorPlano()));
			planoVO.getNovoValorPlanoVO().setDataFim(new DateTime(planoVO.getDataFimVigencia()));
			planoVO.getNovoValorPlanoVO().setValorTaxa(planoVO.getValorPlanoVO().getValorTaxa());
			planoVO.getNovoValorPlanoVO().setValorDesconto(planoVO.getValorPlanoVO().getValorDesconto());
		}
		
		
		planoDao.incluirValoresPlano(planoVO);	
	}


	@Override
	public ValorPlanoVO obterValorPlanoPorCodigo(Long codigo){
		
		return planoDao.obterValorAtualPlanoPorCodigo(codigo);
	}
	
	@Override
	public ValorPlanoVO obterNovoValorPlanoPorCodigo(Long codigo){
		
		return planoDao.obterNovoValorPlanoPorCodigo(codigo);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void desativarVigencia(Long codigoPlano, Integer codigoCanal, DateTime dataInicioVigencia, DateTime dataFimVigencia, DateTime dataAtualizacao){
		
		planoDao.desativarVigencia(codigoPlano,  codigoCanal, dataInicioVigencia, dataFimVigencia, dataAtualizacao);
	}
	
	
	/**
	 * Alterar um plano
	 * @param planoVO - objeto de {@link PlanoVO}
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void alterarPlano(PlanoVO planoVO, PlanoVO planoAnterior){
		
		try{			
			
			//primeiro desativa vig�ncias que foram desmarcadas
			desativarVigencias(planoAnterior, planoVO);
			
			//primeiro desativa vig�ncias dos segmentos que foram desmarcadas
			desativarSegmentos(planoAnterior, planoVO);
			
			planoVO.setDataUltimaAtualizacao(new DateTime());
			//se a data de in�cio foi alterada
			//desativa todas as associa��es do plano
			//e insere novamente com a nova data de in�cio
			if(!planoVO.getDataInicioVigencia().equals(planoAnterior.getDataInicioVigencia())){
				desativarTodasVigencias(planoAnterior, new DateTime());
				incluirTodasVigencias(planoVO, planoAnterior.getListaDeCanais());
			}
			planoDao.alterarPlano(planoVO);
			
			if(verificarSeHouveAlteracaoDeValorTaxaOuDesconto(planoVO, planoAnterior)){
				atualizarValoresTaxaEDescontoVigenteEProgramdado(planoVO);
			}
			
			if(verificarSeValoresAlterados(planoVO, planoAnterior)){
				//desativa um valor de plano
				//desativarValoresPlano(planoVO);
				//inclui novos valores plano
				if(planoAnterior.isExisteValorProgramado()){
					excluirValorProgramado(planoVO);
				}
				incluirValoresPlano(planoVO);
			}else if(planoVO.isValorProgramadoExcluido()){
				excluirValorProgramado(planoVO);
			}else if(!planoVO.getDataFimVigencia().equals(planoAnterior.getDataFimVigencia())){
				planoDao.incluirValoresPlano(planoVO);
			}

			//troca a lista de canais por uma com apenas os novos canais 
			planoVO.setListaDeCanais(listaComNovosCanais(planoVO, planoAnterior));
					
			
		}catch(Exception e){
			planoValidator.adicionarErroEValidar("msg.erro.plano.erro.alterar");
			LOGGER.error(e.getMessage());
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	private void desativarSegmentos(PlanoVO planoAnterior, PlanoVO planoVO) {
		if(planoAnterior.getListaTipoSucursal() != null && !planoAnterior.getListaTipoSucursal().isEmpty()){
			
    		for(Integer segmento : planoAnterior.getListaTipoSucursal()){
				planoDao.desativarSegmentos(planoAnterior.getCodigo(), segmento, planoVO.getDataInicioVigencia());
			}
		}	
		
	}

	private void excluirValorProgramado(PlanoVO planoVO) {
		if(planoVO != null){
			planoDao.desativarValorProgramadoPlano(planoVO);
		}
	}    
 
 	private void desativarVigencias(PlanoVO planoAnterior, PlanoVO planoVO){

    	if(planoAnterior.getListaDeCanais() != null && !planoAnterior.getListaDeCanais().isEmpty()){
			
    		for(CanalVendaVO canalAnterior : planoAnterior.getListaDeCanais()){
				
				//se canal anterior n�o est� na lista atual, h� vig�ncia a desativar
				if(!planoVO.getListaDeCanais().contains(canalAnterior)){
					desativarVigencia(planoAnterior.getCodigo(), canalAnterior.getCodigo(), planoAnterior.getDataInicioVigencia(), planoAnterior.getDataFimVigencia(), new DateTime());
				}
			}
		}
    }
    
    private void desativarTodasVigencias(PlanoVO planoAnterior, DateTime dataAtualizacao){

    	if(planoAnterior.getListaDeCanais() != null && !planoAnterior.getListaDeCanais().isEmpty()){
			
    		for(CanalVendaVO canalAnterior : planoAnterior.getListaDeCanais()){
				
				//se canal anterior n�o est� na lista atual, h� vig�ncia a desativar
				desativarVigencia(planoAnterior.getCodigo(), canalAnterior.getCodigo(), planoAnterior.getDataInicioVigencia(), planoAnterior.getDataFimVigencia(), dataAtualizacao);
			}
		}
	}
    

    
	/**
	 * retorna uma lista com canais que n�o tinham anteriormente
	 * @param planoAtual
	 * @param planoAnterior
	 * @return List<CanalVendaVO> listaCanais
	 */
	private List<CanalVendaVO> listaComNovosCanais(PlanoVO planoAtual, PlanoVO planoAnterior){
		
		List<CanalVendaVO> novaListaCanais = new ArrayList<CanalVendaVO>();
		
		if(planoAnterior.getListaDeCanais() != null && !planoAnterior.getListaDeCanais().isEmpty()){
			
			for(CanalVendaVO canal : planoAtual.getListaDeCanais()){
				
				if(!planoAnterior.getListaDeCanais().contains(canal)){
					novaListaCanais.add(canal);
				}
			}
		}
		else{
			return planoAtual.getListaDeCanais();
		}
		return novaListaCanais;
	}
	
	/**
	 * Desativa valores de plano
	 * @param planoVO 
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void desativarValoresPlano(PlanoVO planoVO){
		
		planoDao.desativarValoresPlano(planoVO);
	}


	public PlanoVO obterPlanoPorCodigoCanalDeVenda(Integer codigoCanalVenda){
		
		return planoDao.obterPlanoPorCodigoCanalDeVenda(codigoCanalVenda);
	}
	
	public PlanoVO obterPlanoAtualPorCodigoCanal(Integer codigoCanalVenda){
		
		return planoDao.obterPlanoAtualPorCodigoCanal(codigoCanalVenda);
	}
	
	public PlanoVO buscarPlanoPorCodigoCanalEDataMovimento(Long canal, Date dataMovimento){
		
		return planoDao.buscarPlanoPorCodigoCanalEDataMovimento( canal, dataMovimento);
	}
	
	
	//TODO alterar, n�o funciona assim
	public void verificarSeHouveAlteracaoDoPlano(PlanoVO planoAnterior, PlanoVO planoVO){
		
		boolean houveAlteracao = false;
		if(!planoAnterior.getCodigo().equals(planoVO.getCodigo()) || !planoAnterior.getCodigoRegistro().equals(planoVO.getCodigoRegistro())
				|| !planoAnterior.getDataInicioVigencia().equals(planoVO.getDataInicioVigencia())
				|| !planoAnterior.getDataFimVigencia().equals(planoVO.getDataFimVigencia())
				|| !planoAnterior.getNome().equals(planoVO.getNome())
				|| !planoAnterior.getDescricaoCarenciaPeriodoAnual().equals(planoVO.getDescricaoCarenciaPeriodoAnual())
				|| !planoAnterior.getDescricaoCarenciaPeriodoMensal().equals(planoVO.getDescricaoCarenciaPeriodoMensal())){
			houveAlteracao = true;
		}
		else if(!planoAnterior.getValorPlanoVO().getValorAnualTitular().equals(planoVO.getValorPlanoVO().getValorAnualTitular())
				|| !planoAnterior.getValorPlanoVO().getValorAnualDependente().equals(planoVO.getValorPlanoVO().getValorAnualDependente())
				|| !planoAnterior.getValorPlanoVO().getValorMensalTitular().equals(planoVO.getValorPlanoVO().getValorMensalTitular())
				|| !planoAnterior.getValorPlanoVO().getValorMensalDependente().equals(planoVO.getValorPlanoVO().getValorMensalDependente())
				|| !planoAnterior.getValorPlanoVO().getValorTaxa().equals(planoVO.getValorPlanoVO().getValorTaxa())
				|| !planoAnterior.getValorPlanoVO().getValorDesconto().equals(planoVO.getValorPlanoVO().getValorDesconto())){
			houveAlteracao = true;
		}
		else if(planoAnterior.getNovoValorPlanoVO() != null && planoVO.getNovoValorPlanoVO() != null){
			
			if(!planoAnterior.getNovoValorPlanoVO().getValorAnualTitular().equals(planoVO.getNovoValorPlanoVO().getValorAnualTitular())
					|| !planoAnterior.getNovoValorPlanoVO().getValorAnualDependente().equals(planoVO.getNovoValorPlanoVO().getValorAnualDependente())
					|| !planoAnterior.getNovoValorPlanoVO().getValorMensalTitular().equals(planoVO.getNovoValorPlanoVO().getValorMensalTitular())
					|| !planoAnterior.getNovoValorPlanoVO().getValorMensalDependente().equals(planoVO.getNovoValorPlanoVO().getValorMensalDependente())
					|| !planoAnterior.getNovoValorPlanoVO().getValorTaxa().equals(planoVO.getNovoValorPlanoVO().getValorTaxa())
					|| !planoAnterior.getNovoValorPlanoVO().getValorDesconto().equals(planoVO.getNovoValorPlanoVO().getValorDesconto())
					|| !planoAnterior.getDataProgramadaNovoValorPlano().toDate().equals(planoVO.getDataProgramadaNovoValorPlano().toDate())){
				houveAlteracao = true;
			}
			
		}
		
		if(!houveAlteracao && planoAnterior.getNovoValorPlanoVO() == null && planoVO.getNovoValorPlanoVO() != null){
			houveAlteracao = true;
		}
		if(!houveAlteracao && planoAnterior.getNovoValorPlanoVO() != null && planoVO.getNovoValorPlanoVO() == null){
			houveAlteracao = true;
		}
		
		
		if(!houveAlteracao && planoVO.getListaDeCanais().size() != planoAnterior.getListaDeCanais().size()){
			houveAlteracao = true;
		}
		
		
		if(!houveAlteracao && planoVO.getListaDeCanais().size() != planoAnterior.getListaDeCanais().size()){
			//caso as duas listas de canais tenham o mesmo tamanho
			//� preciso verificar se possuem os mesmos canais
			//para isso adiciona toda a lista do planoVO em um List tempor�rio
			//depois se remove todos os que s�o iguais da outra lista
			// caso o List tempor�rio fique vazio
			// n�o houve altera��o
			List<CanalVendaVO> listaDeNovosCanais = new ArrayList<CanalVendaVO>();
			listaDeNovosCanais.addAll(planoVO.getListaDeCanais());
			listaDeNovosCanais.removeAll(planoAnterior.getListaDeCanais());
			
			if(!listaDeNovosCanais.isEmpty()){
				houveAlteracao = true;
			}
		}
		
		if(!houveAlteracao && planoVO.getListaTipoSucursal().size() != planoAnterior.getListaTipoSucursal().size()){
			houveAlteracao = true;
		}
		
		if(!houveAlteracao && planoVO.getListaTipoSucursal().size() != planoAnterior.getListaTipoSucursal().size()){
			//caso as duas listas de canais tenham o mesmo tamanho
			//� preciso verificar se possuem os mesmos canais
			//para isso adiciona toda a lista do planoVO em um List tempor�rio
			//depois se remove todos os que s�o iguais da outra lista
			// caso o List tempor�rio fique vazio
			// n�o houve altera��o
			List<Integer> novosSegmentos = new ArrayList<Integer>();
			novosSegmentos.addAll(planoVO.getListaTipoSucursal());
			novosSegmentos.removeAll(planoAnterior.getListaTipoSucursal());
			
			if(!novosSegmentos.isEmpty()){
				houveAlteracao = true;
			}
		}
		
		
					
		if(houveAlteracao == false){
			planoValidator.adicionarErroEValidar("msg.erro.nao.houve.alteracao");
		}
		
	}
	
	
	public boolean verificarSeValoresAlterados(PlanoVO planoVO, PlanoVO planoAnterior){
		
		boolean houveAlteracao = false;
		
		if(!planoAnterior.getValorPlanoVO().getValorAnualTitular().equals(planoVO.getValorPlanoVO().getValorAnualTitular())
				|| !planoAnterior.getValorPlanoVO().getValorAnualDependente().equals(planoVO.getValorPlanoVO().getValorAnualDependente())
				|| !planoAnterior.getValorPlanoVO().getValorMensalTitular().equals(planoVO.getValorPlanoVO().getValorMensalTitular())
				|| !planoAnterior.getValorPlanoVO().getValorMensalDependente().equals(planoVO.getValorPlanoVO().getValorMensalDependente())){
			
			houveAlteracao = true;
		}
		
		
		if(!houveAlteracao){
			if(planoAnterior.getNovoValorPlanoVO() != null && planoVO.getNovoValorPlanoVO() != null){
				
				if(!planoAnterior.getNovoValorPlanoVO().getValorAnualTitular().equals(planoVO.getNovoValorPlanoVO().getValorAnualTitular())
						|| !planoAnterior.getNovoValorPlanoVO().getValorAnualDependente().equals(planoVO.getNovoValorPlanoVO().getValorAnualDependente())
						|| !planoAnterior.getNovoValorPlanoVO().getValorMensalTitular().equals(planoVO.getNovoValorPlanoVO().getValorMensalTitular())
						|| !planoAnterior.getNovoValorPlanoVO().getValorMensalDependente().equals(planoVO.getNovoValorPlanoVO().getValorMensalDependente())
					){
					
					houveAlteracao = true;
				}
		
			}else if(planoVO.getNovoValorPlanoVO() != null){
				houveAlteracao = true;
			}
		}
		
		return houveAlteracao; 
	}
}
