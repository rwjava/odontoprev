package br.com.bradseg.eedi.administrativo.plano.facade;

import java.util.List;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.administrativo.plano.dao.CadastroCorretorDao;
import br.com.bradseg.eedi.administrativo.support.Validador;
import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorVO;
import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorViewVO;


@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CorretorServiceFacadeImpl implements CorretorServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(CorretorServiceFacadeImpl.class);
	
	@Autowired
	private CorretorValidator corretorValidator;
	
	@Autowired
	private transient CadastroCorretorDao cadastroCorretorDao;
	
	//@Autowired
	//private CorretorServiceFacade CorretorServiceFacade;
	
	/**
	 * 
	 */
	@Override
	public void validarPreenchimentoCampoDeConsulta(CadastroCorretorViewVO cadastroCorretorVO) {
		corretorValidator.validarPreenchimentoCampoDeConsulta(cadastroCorretorVO);
	}
	
	/**
	 * 
	 */
	@Override
	public void validarPreenchimentoCamposCadastroCorretor(CadastroCorretorViewVO cadastroCorretorViewVO) {
		corretorValidator.validarPreenchimentoCamposCadastroCorretor(cadastroCorretorViewVO);
	}
	
	/**
	 * 
	 */
	@Override
	public void validarCampoDataFimVigenciaAlteracaoCorretor(CadastroCorretorViewVO cadastroCorretorViewVO) {
		corretorValidator.validarCampoDataFimVigenciaAlteracaoCorretor(cadastroCorretorViewVO);
	}
	
	@Override
	public List<CadastroCorretorVO> listarCorretores() {
		return cadastroCorretorDao.listarCorretores();
	}
	
	@Override
	public List<CadastroCorretorVO> listarCorretoresPorSucursal(Long sucursal) {
		return cadastroCorretorDao.listarCorretorPorSucursal(sucursal);
	}
	
	
	@Override
	public CadastroCorretorVO obterDadosCorretorPorCnpj(Long cnpj) {
		CadastroCorretorVO cadastroCorretorVO = null;
		try {
			cadastroCorretorVO = cadastroCorretorDao.obterDadosCorretorPorCnpj(cnpj);
		} catch (Exception e) {
			LOGGER.error("INFO: ERRO AO BUSCAR CORRETOR");
			throw new BusinessException("ERRO AO BUSCAR CORRETOR");
		}
		return cadastroCorretorVO;
	}
	
	@Override
	public List<CadastroCorretorVO> listarCorretorPorSucursalEStatus(Long sucursal, Integer status) {
		return cadastroCorretorDao.listarCorretorPorSucursalEStatus(sucursal, status);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void atualizarVigencia(CadastroCorretorVO cadastroCorretorVO) {
		cadastroCorretorDao.atualizarVigencia(cadastroCorretorVO);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void incluirAcessoCorretor(CadastroCorretorVO cadastroCorretorVO) {
		cadastroCorretorDao.incluirAcessoCorretor(cadastroCorretorVO);
	}
	
	
	@Override
	public void validarCnpjEDataVigenciaContrato(CadastroCorretorVO cadastroCorretorVO) {
		Validador validador = new Validador();
		validarCnpjCorretor(cadastroCorretorVO.getCnpj(), validador);
		validarDataVigenciaCorretor(cadastroCorretorVO, validador);
		validador.validar();
	}
	
	
	/**
	 * M�todo respons�vel por verificar se CNPJ/CPF j� est� cadastrado na base
	 * @param cnpj
	 * @param validador
	 */
	public void validarCnpjCorretor(Long cnpj, Validador validador){
		if(verificarExistenciaCorretorPorCnpj(cnpj)){
			validador.adicionarErro("msg.erro.corretor.cnpj.informado");
		}
	}
	
	public void validarDataVigenciaCorretor(CadastroCorretorVO cadastroCorretorVO, Validador validador){
		 if(cadastroCorretorVO.getDataInicioContrato().isAfter(cadastroCorretorVO.getDataFimContrato()) || cadastroCorretorVO.getDataFimContrato().toLocalDate().isBefore(new LocalDate())){
			 validador.adicionarErro("msg.erro.corretor.datafim.menor.que.data.atual");
		 }
	}
	
	
	
	@Override
	public boolean verificarExistenciaCorretorPorCnpj(Long cnpj) {
		return cadastroCorretorDao.verificarExistenciaCorretorPorCnpj(cnpj); 
	}
	
	
	/**
	 * M�todo respons�vel por verificar se houve altera��o dos dados de vig�ncia.
	 */
	public void verificarSeHouveAlteracaoDoCorretor(CadastroCorretorVO cadastroCorretorAnterior, CadastroCorretorVO cadastroCorretorVO) {
		boolean houveAlteracao = false;
		
		if(!cadastroCorretorAnterior.getDataFimContrato().equals(cadastroCorretorVO.getDataFimContrato()) || !cadastroCorretorAnterior.getStatus().equals(cadastroCorretorVO.getStatus())) {
			houveAlteracao = true;
		}
		
		if(houveAlteracao == false){
			corretorValidator.adicionarErroEValidar("msg.erro.corretor.nao.houve.alteracao");
		}
	}
	
}
