package br.com.bradseg.eedi.administrativo.plano.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.administrativo.rowmapper.PlanoRowMapper;
import br.com.bradseg.eedi.administrativo.rowmapper.SegmentoRowMapper;
import br.com.bradseg.eedi.administrativo.rowmapper.ValorPlanoRowMapper;
import br.com.bradseg.eedi.administrativo.support.Constantes;
import br.com.bradseg.eedi.administrativo.support.Strings;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroPlanoVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoVO;
import br.com.bradseg.eedi.administrativo.vo.Status;
import br.com.bradseg.eedi.administrativo.vo.ValorPlanoVO;


@Repository
public class PlanoDaoImpl extends JdbcDao  implements PlanoDao{

	@Autowired
	private DataSource dataSource;
	
	private static final String LISTA_PLANOS = new StringBuilder()
	
	.append(" SELECT CPLANO_DNTAL_INDVD, RPLANO_DNTAL_INDVD, CREG_PLANO_DNTAL,   \n")
	.append(" CRESP_ULT_ATULZ, DINIC_PLANO_INDVD, DFIM_VGCIA,                    \n")
	.append(" RPER_CAREN_PLANO_ANO, RPER_CAREN_PLANO_MES ,                       \n")
	.append(" DULT_ATULZ_REG FROM                                                \n")
	.append(Constantes.TABELA_PLANO)	
	.append(" ORDER BY RPLANO_DNTAL_INDVD                                        \n")
	.toString();
	
//	private static final String CONSULTAR_PLANOS = new StringBuilder()                       
//	
//	.append(" SELECT CPLANO_DNTAL_INDVD, RPLANO_DNTAL_INDVD, CREG_PLANO_DNTAL, \n")
//	.append(" CRESP_ULT_ATULZ, DINIC_PLANO_INDVD, DFIM_VGCIA,                        \n")
//	.append(" RPER_CAREN_PLANO_ANO, RPER_CAREN_PLANO_MES ,                   \n")
//	.append(" DULT_ATULZ_REG FROM                                              \n")
//	.append(Constantes.TABELA_PLANO)						
//	.append(" WHERE DINIC_PLANO_INDVD IS NOT NULL                                     \n")
//	.toString();
	

	private static final String BUSCAR_PLANOS = new StringBuilder() 
	
   .append(" SELECT	                                   \n")
   .append("     PLANO.CPLANO_DNTAL_INDVD ,	           \n")
   .append("     PLANO.RPLANO_DNTAL_INDVD,	           \n")
   .append("     PLANO.CREG_PLANO_DNTAL,	           \n")
   .append("     PLANO.CRESP_ULT_ATULZ,	               \n")
   .append("     PLANO.DINIC_PLANO_INDVD,	           \n")
   .append("     PLANO.DFIM_VGCIA,	                   \n")
   .append("     PLANO.RPER_CAREN_PLANO_ANO,	       \n")
   .append("     PLANO.RPER_CAREN_PLANO_MES ,	       \n")
   .append("     PLANO.DULT_ATULZ_REG	               \n")
   .append(" FROM	                                   \n")
   .append("  DBPROD.PLANO_DNTAL_INDVD PLANO   \n").toString();
	
	private static final String VERIFICAR_EXISTENCIA_NOME = new StringBuilder()
	.append(" SELECT COUNT(*) FROM 											   \n")
	.append(Constantes.TABELA_PLANO)
	.append(" WHERE RPLANO_DNTAL_INDVD = :nomePlano                            \n").toString();
	
	private static final String OBTER_PLANO_MAIS_RECENTE_POR_CODIGO_CANAL = new StringBuilder()
	.append(" SELECT PLANO.CPLANO_DNTAL_INDVD, PLANO.RPLANO_DNTAL_INDVD, PLANO.CREG_PLANO_DNTAL, \n")
	.append(" PLANO.CRESP_ULT_ATULZ, PLANO.DINIC_PLANO_INDVD, PLANO.DFIM_VGCIA,                  \n")
	.append(" PLANO.RPER_CAREN_PLANO_ANO, PLANO.RPER_CAREN_PLANO_MES ,                           \n")
	.append(" PLANO.DULT_ATULZ_REG FROM                                                          \n")
	.append(  Constantes.TABELA_PLANO)
	.append(" PLANO, 																			 \n")
	.append(" DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL                     				 \n")
	.append(" WHERE CANAL.CCANAL_VDA_DNTAL_INDVD = :canal                                        \n")
	.append(" AND PLANO.CPLANO_DNTAL_INDVD = CANAL.CPLANO_DNTAL_INDVD                            \n")
	.append(" AND CANAL.DFIM_VGCIA IS NULL AND CANAL.DINIC_VGCIA =                               \n")
	.append(" (SELECT MAX(DINIC_VGCIA) FROM DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD           \n")
	.append(" WHERE CCANAL_VDA_DNTAL_INDVD = CANAL.CCANAL_VDA_DNTAL_INDVD                        \n")
	.append(" AND DFIM_VGCIA IS NULL)		 							      					 \n")
	.toString();
	
	private static final String OBTER_PLANO_POR_CODIGO = new StringBuilder()
	.append(" SELECT PLANO.CPLANO_DNTAL_INDVD, PLANO.RPLANO_DNTAL_INDVD, PLANO.CREG_PLANO_DNTAL, \n")
	.append(" PLANO.CRESP_ULT_ATULZ, PLANO.DINIC_PLANO_INDVD, PLANO.DFIM_VGCIA,                        \n")
	.append(" PLANO.RPER_CAREN_PLANO_ANO, PLANO.RPER_CAREN_PLANO_MES ,                         \n")
	.append(" PLANO.DULT_ATULZ_REG FROM                                                          \n")
	.append(Constantes.TABELA_PLANO)
	.append(" PLANO  																			 \n")
	.append(" WHERE  PLANO.CPLANO_DNTAL_INDVD = :codigoPlano                                     \n")
	.toString();
	
	private static final String INCLUIR_PLANO = new StringBuilder()
	.append(" INSERT INTO 																		 \n")
	.append(  Constantes.TABELA_PLANO)
	.append(" (CPLANO_DNTAL_INDVD, RPLANO_DNTAL_INDVD, CREG_PLANO_DNTAL,                         \n")
	.append(" CRESP_ULT_ATULZ, DINIC_PLANO_INDVD, DFIM_VGCIA,                                          \n")
	.append(" RPER_CAREN_PLANO_ANO, RPER_CAREN_PLANO_MES,                                     \n")
	.append(" DULT_ATULZ_REG )																	 \n")
	.append(" VALUES(:codigoPlano, :nomePlano,:codigoRegistroPlano,	                             \n")
	.append(" :codigoResponsavel, :dataInicioVigencia, :dataFimVigencia,                         \n")
	.append(" :descricaoPeriodoCarenciaAnual, :descricaoPeriodoCarenciaMensal,                   \n")
	.append(" :dataInclusao )                                                                \n").toString();
	

	
	private static final String INCLUIR_VIGENCIA_CANAL_VENDA = new StringBuilder()
	.append(" INSERT INTO DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD                             \n")
	.append(" ( CCANAL_VDA_DNTAL_INDVD, CPLANO_DNTAL_INDVD, DINIC_VGCIA, CRESP_ULT_ATULZ,        \n")
	.append(" DULT_ATULZ_REG ) 																	 \n")
	.append(" VALUES( :codigoCanal, :codigoPlano, :dataInicioVigencia,                           \n")
	.append(" :codigoResponsavel, :dataAtualizacao )                                            \n")
	.toString();
	
	private static final String INCLUIR_VALORES_PLANO = new StringBuilder()
	
	.append(" INSERT INTO DBPROD.VLR_PLANO_DNTAL_INDVD                                         		     \n")
	.append(" (NSEQ_VLR_PLANO_DNTAL, CPLANO_DNTAL_INDVD,                                        	     \n")
	.append(" VANUDD_PLANO_DNTAL_TTLAR, VANUDD_PLANO_DNTAL_DEPDT, VMESD_PLANO_TTLAR,               \n")
	.append(" VMESD_PLANO_DEPDT,  VDESC_PLANO_DNTAL, VTX_PLANO_INDVD, DINIC_VGCIA, DFIM_VGCIA, DINCL_REG_TBELA, CRESP_ULT_ATULZ)     \n")
	.append(" VALUES((SELECT COALESCE(MAX(NSEQ_VLR_PLANO_DNTAL),0)+1 FROM DBPROD.VLR_PLANO_DNTAL_INDVD ),             \n")
	.append(" :codigoPlano, :valorAnualTitular, :valorAnualDependente, :valorMensalTitular,              \n")
	.append(" :valorMensalDependente, :valorDesconto, :valorTaxa, :dataInicioVigencia, :dataFimVigencia, :dataInclusao, :codigoResponsavel) \n")
	.toString();
	
	private static final String OBTER_VALORES_ATUAL_PLANO = new StringBuilder()
	
	.append(" SELECT                                          		                   \n")
	.append(" NSEQ_VLR_PLANO_DNTAL, CPLANO_DNTAL_INDVD,                                \n")
	.append(" VANUDD_PLANO_DNTAL_TTLAR, VANUDD_PLANO_DNTAL_DEPDT, VMESD_PLANO_TTLAR,   \n")
	.append(" VMESD_PLANO_DEPDT,  VDESC_PLANO_DNTAL, VTX_PLANO_INDVD, DINIC_VGCIA      \n")
	.append(" FROM DBPROD.VLR_PLANO_DNTAL_INDVD                                \n")
	.append(" WHERE CPLANO_DNTAL_INDVD = :codigoPlano                                  \n")
	.append(" AND((DINIC_VGCIA <= CURRENT_DATE                                     \n")
	.append(" AND DFIM_VGCIA >= CURRENT_DATE) OR (DINIC_VGCIA < CURRENT_DATE AND DFIM_VGCIA < CURRENT_DATE))       \n")
	.append(" ORDER BY DINCL_REG_TBELA DESC FETCH FIRST 1 ROWS ONLY                    \n")
	.toString();
	
	private static final String OBTER_ASSOCIACAO_PLANO_CANAL = new StringBuilder()
	.append(" SELECT CPLANO_DNTAL_INDVD  FROM  DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD                \n")
	.append(" WHERE  CCANAL_VDA_DNTAL_INDVD = :codigoCanal                                               \n")
	.append(" AND CPLANO_DNTAL_INDVD = :codigoPlano AND DINIC_VGCIA = :dataInicioVigencia                \n")
	.toString();
	
	private static final String OBTER_NOVO_VALOR_PLANO = new StringBuilder()
	
	.append(" SELECT                                          		                   \n")
	.append(" NSEQ_VLR_PLANO_DNTAL, CPLANO_DNTAL_INDVD,                                \n")
	.append(" VANUDD_PLANO_DNTAL_TTLAR, VANUDD_PLANO_DNTAL_DEPDT, VMESD_PLANO_TTLAR,   \n")
	.append(" VMESD_PLANO_DEPDT,  VDESC_PLANO_DNTAL, VTX_PLANO_INDVD, DINIC_VGCIA      \n")
	.append(" FROM DBPROD.VLR_PLANO_DNTAL_INDVD                                \n")
	.append(" WHERE CPLANO_DNTAL_INDVD = :codigoPlano                                  \n")
	.append(" AND DINIC_VGCIA <= CURRENT_DATE                                           \n")
	.append(" AND DFIM_VGCIA > CURRENT_DATE                                           \n")
	.append(" ORDER BY DINCL_REG_TBELA DESC FETCH FIRST 1 ROWS ONLY                    \n")
	.toString();
	
private static final String OBTER_VALORES_PLANO = new StringBuilder()
	
.append(" SELECT																															  \n ")
.append(" valor_plano.NSEQ_VLR_PLANO_DNTAL, valor_plano.CPLANO_DNTAL_INDVD,                                                                   \n ")
.append(" valor_plano.VANUDD_PLANO_DNTAL_TTLAR, valor_plano.VANUDD_PLANO_DNTAL_DEPDT,                                                         \n ")
.append(" valor_plano.VMESD_PLANO_TTLAR, valor_plano.VMESD_PLANO_DEPDT,                                                                       \n ")
.append(" valor_plano.VDESC_PLANO_DNTAL, valor_plano.VTX_PLANO_INDVD, valor_plano.DINIC_VGCIA                                                 \n ")
.append("                                                                                                                                     \n ")
.append("  FROM  DBPROD.PLANO_DNTAL_INDVD PLANO,  DBPROD.PPSTA_DNTAL_INDVD PROPOSTA,                                              \n ")
.append(" DBPROD.MOVTO_PPSTA_DNTAL MOVIMENTO, DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_VENDA   ,                              \n ")
.append(" DBPROD.VLR_PLANO_DNTAL_INDVD valor_plano                                                                                    \n ")
.append("                                                                                                                                     \n ")
.append(" WHERE                                                                                                                               \n ")
.append(" valor_plano.CPLANO_DNTAL_INDVD = :codigoPlano                                                                                       \n ")
.append(" and canal_venda.cplano_dntal_indvd = valor_plano.cplano_dntal_indvd                                                                 \n ")
.append(" and canal_venda.dfim_vgcia is null                                                                                                  \n ")
.append(" and PROPOSTA.CCANAL_VDA_DNTAL_INDVD = canal_venda.ccanal_vda_dntal_indvd                                                            \n ")
.append(" AND MOVIMENTO.NSEQ_PPSTA_DNTAL = PROPOSTA.NSEQ_PPSTA_DNTAL                                                                          \n ")
.append(" AND MOVIMENTO.DINIC_MOVTO_PPSTA = (SELECT  DINIC_MOVTO_PPSTA FROM DBPROD.MOVTO_PPSTA_DNTAL                                  \n ")
.append(" WHERE NSEQ_PPSTA_DNTAL = PROPOSTA.NSEQ_PPSTA_DNTAL ORDER BY DINIC_MOVTO_PPSTA ASC FETCH FIRST 1 ROWS ONLY )                         \n ")
.append(" AND DATE(MOVIMENTO.DINIC_MOVTO_PPSTA) >= DATE(PLANO.DINIC_PLANO_INDVD) AND DATE(MOVIMENTO.DINIC_MOVTO_PPSTA) <= DATE(PLANO.DFIM_VGCIA)	) \n ")
.toString();
	
	
	
	private static final String DESATIVAR_VALOR_PLANO = new StringBuilder()
	.append(" UPDATE DBPROD.VLR_PLANO_DNTAL_INDVD                              \n")
	.append(" SET DFIM_VGCIA = :dataFimVigencia                                        \n")
	.append(" WHERE CPLANO_DNTAL_INDVD = :codigoPlano                                          \n")
	.append(" AND DINIC_VGCIA < CURRENT_DATE                                           \n")
	.append(" AND DFIM_VGCIA > CURRENT_DATE                                            \n")
	.toString();
	
	private static final String DESATIVAR_VALOR_PROGRAMADO_PLANO = new StringBuilder()
	.append(" UPDATE DBPROD.VLR_PLANO_DNTAL_INDVD                              \n")
	.append(" SET DFIM_VGCIA = :dataFimVigencia, cresp_ult_atulz = :codigoResponsavel  \n")
	.append(" WHERE CPLANO_DNTAL_INDVD = :codigoPlano                                  \n")
	.append(" AND DINIC_VGCIA > CURRENT_DATE                                           \n")
	.append(" AND dincl_reg_tbela = (SELECT MAX(dincl_reg_tbela) FROM DBPROD.VLR_PLANO_DNTAL_INDVD WHERE CPLANO_DNTAL_INDVD = :codigoPlano AND DINIC_VGCIA > CURRENT_DATE)\n")
	.toString();
	
	private static final String DESATIVAR_VIGENCIA = new StringBuilder()
	
	.append(" UPDATE  DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD                                     	\n")
	.append(" SET DFIM_VGCIA = :dataFimVigencia, DULT_ATULZ_REG = :dataAtualizacao       	            \n")
	.append(" WHERE  CCANAL_VDA_DNTAL_INDVD = :codigoCanal                                              \n")
	.append(" AND CPLANO_DNTAL_INDVD = :codigoPlano                                                     \n")
	.append(" AND DINIC_VGCIA = :dataInicioVigencia                                                      \n")
	.toString();
	
	private static final String ATUALIZAR_VIGENCIA = new StringBuilder()
	
	.append(" UPDATE  DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD                                     	 \n")
	.append(" SET DFIM_VGCIA = cast (Null as date), DULT_ATULZ_REG = CURRENT_TIMESTAMP       	         \n")
	.append(" WHERE  CCANAL_VDA_DNTAL_INDVD = :codigoCanal                                               \n")
	.append(" AND CPLANO_DNTAL_INDVD = :codigoPlano AND DINIC_VGCIA = :dataInicioVigencia                \n")
	.toString();
	
	private static final String ALTERAR_PLANO = new StringBuilder()
	.append(" UPDATE      																		 \n")
	.append(  Constantes.TABELA_PLANO)
	.append(" SET RPLANO_DNTAL_INDVD = :nomePlano , CREG_PLANO_DNTAL = :codigoRegistroPlano,   \n")
	.append(" CRESP_ULT_ATULZ = :codigoResponsavel, DINIC_PLANO_INDVD = :dataInicioVigencia, DFIM_VGCIA = :dataFimVigencia,                     \n")
	.append(" RPER_CAREN_PLANO_ANO = :descricaoPeriodoCarenciaAnual, RPER_CAREN_PLANO_MES = :descricaoPeriodoCarenciaMensal ,         \n")
	.append(" DULT_ATULZ_REG = :dataAtualizacao WHERE CPLANO_DNTAL_INDVD = :codigoPlano     											\n")
	.toString();
	
	
	private static final String OBTER_PLANO_POR_CANAL_E_DATA_MOVIMENTO = new StringBuilder()                                                                                                                                                
	.append( " SELECT PLANO.CPLANO_DNTAL_INDVD, PLANO.RPLANO_DNTAL_INDVD, PLANO.CREG_PLANO_DNTAL,                                                   \n")
	.append( " PLANO.CRESP_ULT_ATULZ, PLANO.DINIC_PLANO_INDVD, PLANO.DFIM_VGCIA,                                                                    \n")
	.append( " PLANO.RPER_CAREN_PLANO_ANO, PLANO.RPER_CAREN_PLANO_MES ,                                                                             \n")
	.append( " PLANO.DULT_ATULZ_REG FROM  DBPROD.PLANO_DNTAL_INDVD PLANO,  DBPROD.PPSTA_DNTAL_INDVD PROPOSTA,                       \n")
	.append( " DBPROD.MOVTO_PPSTA_DNTAL MOVIMENTO, DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_VENDA                                   \n")
	.append( "                                                                                                                                      \n")
	.append( " WHERE PROPOSTA.CCANAL_VDA_DNTAL_INDVD = :codigoCanalVenda                                                                            \n")
	.append( "                                                                                                                                      \n")
	.append( " AND CANAL_VENDA.CCANAL_VDA_DNTAL_INDVD = PROPOSTA.CCANAL_VDA_DNTAL_INDVD                                                             \n")
	.append( " AND MOVIMENTO.NSEQ_PPSTA_DNTAL = PROPOSTA.NSEQ_PPSTA_DNTAL                                                                           \n")
	.append( " AND MOVIMENTO.DINIC_MOVTO_PPSTA = (SELECT  DINIC_MOVTO_PPSTA FROM DBPROD.MOVTO_PPSTA_DNTAL                                   \n")
	.append( " WHERE NSEQ_PPSTA_DNTAL = PROPOSTA.NSEQ_PPSTA_DNTAL ORDER BY DINIC_MOVTO_PPSTA ASC FETCH FIRST 1 ROWS ONLY )                          \n")
	.append( " AND DATE(MOVIMENTO.DINIC_MOVTO_PPSTA) >= DATE(PLANO.DINIC_PLANO_INDVD) AND DATE(MOVIMENTO.DINIC_MOVTO_PPSTA) < DATE(PLANO.DFIM_VGCIA)\n")
	.toString();
	
	private static final String OBTER_PLANO_ATUAL_POR_CANAL = new StringBuilder()                                                                                                                                                
	.append( " SELECT PLANO.CPLANO_DNTAL_INDVD, PLANO.RPLANO_DNTAL_INDVD, PLANO.CREG_PLANO_DNTAL,                                                   \n")
	.append( " PLANO.CRESP_ULT_ATULZ, PLANO.DINIC_PLANO_INDVD, PLANO.DFIM_VGCIA,                                                                    \n")
	.append( " PLANO.RPER_CAREN_PLANO_ANO, PLANO.RPER_CAREN_PLANO_MES ,                                                                             \n")
	.append( " PLANO.DULT_ATULZ_REG FROM  DBPROD.PLANO_DNTAL_INDVD PLANO,  DBPROD.PPSTA_DNTAL_INDVD PROPOSTA,                                       \n")
	.append( " DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_VENDA                                                                                       \n")
	.append( "                                                                                                                                      \n")
	.append( " WHERE PROPOSTA.CCANAL_VDA_DNTAL_INDVD = :codigoCanalVenda                                                                            \n")
	.append( "                                                                                                                                      \n")
	.append( " AND CANAL_VENDA.CCANAL_VDA_DNTAL_INDVD = PROPOSTA.CCANAL_VDA_DNTAL_INDVD                                                             \n")
	.append( " AND  DATE(PLANO.DINIC_PLANO_INDVD) <= CURRENT_DATE  AND   (DATE(PLANO.DFIM_VGCIA) >= CURRENT_DATE    OR PLANO.DFIM_VGCIA IS NULL)    \n")
	.append("  fetch first 1 rows only                                                                                                              \n")
	.toString();
    		
	private static final String  OBTER_VALOR_PLANO_POR_CODIGO_PLANO_E_DATA_MOVIMENTO = new StringBuilder()
	
	 .append(" SELECT VALOR_PLANO.NSEQ_VLR_PLANO_DNTAL, VALOR_PLANO.CPLANO_DNTAL_INDVD,                                                       \n")
	 .append(" VALOR_PLANO.DINIC_VGCIA, VALOR_PLANO.DFIM_VGCIA , VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR, VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT , \n")
	 .append(" VALOR_PLANO.VMESD_PLANO_TTLAR, VALOR_PLANO.VMESD_PLANO_DEPDT, VALOR_PLANO.VTX_PLANO_INDVD, VALOR_PLANO.VDESC_PLANO_DNTAL       \n")
	 .append(" from DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO,                                                                                 \n")
	 .append(" DBPROD.PLANO_DNTAL_INDVD PLANO,  DBPROD.PPSTA_DNTAL_INDVD PROPOSTA,                                                            \n")
	 .append(" DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_VENDA                                                                                 \n")
	 .append(" WHERE                                                                                                                          \n")
	 .append(" valor_plano.CPLANO_DNTAL_INDVD = :codigoPlano                                                                                  \n")
	 .append(" and canal_venda.cplano_dntal_indvd = valor_plano.cplano_dntal_indvd                                                            \n")
	 .append(" and canal_venda.dfim_vgcia is null                                                                                             \n")
	 .append(" and PROPOSTA.CCANAL_VDA_DNTAL_INDVD = canal_venda.ccanal_vda_dntal_indvd                                                       \n")
	 .append(" AND DATE(VALOR_PLANO.DINIC_VGCIA) <= :dataMovimento AND                                                                        \n")
	 .append(" ( DATE(VALOR_PLANO.DFIM_VGCIA) >= :dataMovimento OR VALOR_PLANO.DFIM_VGCIA IS NULL)                                            \n")
	 .append(" FETCH FIRST 1 ROWS ONLY                                                                                                        \n")
	 .toString();
	
	
    public static final String SEQUENCE_PLANO = "SELECT COALESCE(MAX(CPLANO_DNTAL_INDVD),0)+1 FROM DBPROD.PLANO_DNTAL_INDVD";                      
	                                                                                                                                                
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanoDaoImpl.class);
	
	
	@Override
	public List<PlanoVO> listarPlanos() {
		
		List<PlanoVO> planos =   getJdbcTemplate().query(LISTA_PLANOS,new MapSqlParameterSource() ,new PlanoRowMapper());

		return planos;
	}
	
	@Override
	public List<PlanoVO> consultarPlanos(FiltroPlanoVO filtro) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(BUSCAR_PLANOS);
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		 if(Strings.isBlankOrNull(filtro.getNomePlano()) && (filtro.getCodigoCanal()!= 0 && filtro.getCodigoCanal() != null)){
				
				sb.append(" 	INNER JOIN                                           \n")
				.append("     DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD canal      \n")
				.append(" ON                                                        \n")
				.append("     canal.CPLANO_DNTAL_INDVD = plano.CPLANO_DNTAL_INDVD   \n")
				.append(" AND canal.ccanal_vda_dntal_indvd = :canal                 \n")
				.append(" AND canal.dfim_vgcia is null                              \n");
				
				params.addValue("canal", filtro.getCodigoCanal());
		}
		 
		 if(!Strings.isBlankOrNull(filtro.getNomePlano()) && (filtro.getStatus() != null && filtro.getStatus() != 0)){
			 
			 sb.append(" WHERE plano.rplano_dntal_indvd LIKE '%"+filtro.getNomePlano().toUpperCase()+"%' ");
			 if(Status.VIGENTE.getCodigo().equals(filtro.getStatus())){
					sb.append(" AND DINIC_PLANO_INDVD <= CURRENT_DATE AND DFIM_VGCIA >= CURRENT_DATE        \n");
				}else if(Status.PROGRAMADO.getCodigo().equals(filtro.getStatus())){
					sb.append(" AND DINIC_PLANO_INDVD > CURRENT_DATE 									     \n");
				}else if(Status.EXPIRADO.getCodigo().equals(filtro.getStatus())){
					sb.append(" AND DFIM_VGCIA <  CURRENT_DATE 									 \n");
				}
			// params.addValue("nomePlano", filtro.getNomePlano());
			 
		 }
		 else if(Strings.isBlankOrNull(filtro.getNomePlano()) && (filtro.getStatus() != null && filtro.getStatus() != 0)){
			 
			 if(Status.VIGENTE.getCodigo().equals(filtro.getStatus())){
					sb.append(" WHERE plano.DINIC_PLANO_INDVD <= CURRENT_DATE AND plano.DFIM_VGCIA >= CURRENT_DATE        \n");
				}else if(Status.PROGRAMADO.getCodigo().equals(filtro.getStatus())){
					sb.append(" WHERE plano.DINIC_PLANO_INDVD > CURRENT_DATE 									     \n");
				}else if(Status.EXPIRADO.getCodigo().equals(filtro.getStatus())){
					sb.append(" WHERE plano.DFIM_VGCIA <  CURRENT_DATE 									 \n");
				}
			 
		 }
		 else if(!Strings.isBlankOrNull(filtro.getNomePlano()) && (filtro.getStatus() == null || filtro.getStatus() == 0)){
			 
			 sb.append(" WHERE plano.rplano_dntal_indvd LIKE '%"+filtro.getNomePlano().toUpperCase()+"%' ");
			// params.addValue("nomePlano", filtro.getNomePlano());
			 
		 }

		
		 sb.append(" ORDER BY RPLANO_DNTAL_INDVD ASC ");
		
		List<PlanoVO> planos =   null; 
		
		try{
			if(params.getValues().isEmpty()){
				
				planos = getJdbcTemplate().query(sb.toString(),new MapSqlParameterSource() ,new PlanoRowMapper());
			}else{

				planos = getJdbcTemplate().query(sb.toString(), params ,new PlanoRowMapper());
			}
		}catch(Exception e){
			LOGGER.error("INFO: ERRO VERIFICAR_EXISTENCIA_CODIGO.");
		}
		return planos;
	}
	
	
	public boolean verificarExistenciaPlanoPorCodigo(String nomePlano){
		

		boolean existeNomePlano = false;
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("nomePlano", nomePlano.toUpperCase().trim());
		
		try{
			if(getJdbcTemplate().queryForInt(VERIFICAR_EXISTENCIA_NOME, param)> 0){
				existeNomePlano = true;
			}
		}catch(Exception e){
			LOGGER.error("INFO: ERRO VERIFICAR_EXISTENCIA_CODIGO.");
		}
		return existeNomePlano;
		
	}

	@Override
	public DataSource getDataSource() {
		// TODO Auto-generated method stub
		return dataSource;
	}

	@Override
	public PlanoVO obterPlanoMaisRecentePorCodigoCanal(Integer codigoCanal) {
		
		PlanoVO plano = null;
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("canal", codigoCanal);
		try{
			
			plano =   getJdbcTemplate().queryForObject(OBTER_PLANO_MAIS_RECENTE_POR_CODIGO_CANAL, param ,new PlanoRowMapper());
		}catch(Exception e){
			
			LOGGER.error("INFO: ERRO AO OBTER PLANO MAIS RECENTE");
		}
		return plano;
	}
	
	@Override
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano) {
		
		PlanoVO plano = null;
		
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("codigoPlano", codigoPlano);
			plano =   getJdbcTemplate().queryForObject(OBTER_PLANO_POR_CODIGO,  param, new PlanoRowMapper());
		
		return plano;
	}

	@Override
	public void incluirPlano(PlanoVO planoVO) {

		planoVO.setCodigo(obterNovoValorSequence());
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoPlano", planoVO.getCodigo());
		params.addValue("nomePlano", planoVO.getNome().toUpperCase());
		params.addValue("codigoRegistroPlano", planoVO.getCodigoRegistro());
		params.addValue("codigoResponsavel", planoVO.getCodigoResponsavel());
		params.addValue("dataInicioVigencia", new java.sql.Date(planoVO.getDataInicioVigencia().getMillis()));
		params.addValue("dataFimVigencia", new java.sql.Date(planoVO.getDataFimVigencia().getMillis()));
		params.addValue("descricaoPeriodoCarenciaAnual", planoVO.getDescricaoCarenciaPeriodoAnual());
		params.addValue("descricaoPeriodoCarenciaMensal", planoVO.getDescricaoCarenciaPeriodoMensal());
		
		params.addValue("dataInclusao", new java.sql.Timestamp(planoVO.getDataUltimaAtualizacao().getMillis()));
			
		getJdbcTemplate().update(INCLUIR_PLANO, params);
	}

	@Override
	public void incluirVigenciaCanal(CanalVendaVO canalVendaVO, PlanoVO planoVO) {	
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoCanal", canalVendaVO.getCodigo());
		params.addValue("codigoPlano", planoVO.getCodigo());
		params.addValue("dataInicioVigencia", new java.sql.Date(planoVO.getDataInicioVigencia().getMillis()));
		params.addValue("codigoResponsavel", planoVO.getCodigoResponsavel());
		params.addValue("dataAtualizacao", new java.sql.Timestamp(planoVO.getDataUltimaAtualizacao().getMillis()));
				
		getJdbcTemplate().update(INCLUIR_VIGENCIA_CANAL_VENDA, params);
	}
	
	/** {@inheritDoc} */
	public Long obterNovoValorSequence() {
		try{
			
			return this.getJdbcTemplate().queryForLong(SEQUENCE_PLANO, new MapSqlParameterSource());
		}catch(Exception e){
			
			LOGGER.error("INFO: ERRO AO OBTER CODIGO PLANO");
			return null;
		}
	}

	@Override
	public void incluirValoresPlano(PlanoVO planoVO) {
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		params.addValue("codigoPlano", planoVO.getCodigo());
		
		params.addValue("valorDesconto", planoVO.getValorPlanoVO().getValorDesconto());
		params.addValue("valorTaxa", planoVO.getValorPlanoVO().getValorTaxa());
		
		if(planoVO.isExisteValorProgramado()){
			
			params.addValue("valorAnualTitular",planoVO.getNovoValorPlanoVO().getValorAnualTitular());
			params.addValue("valorAnualDependente", planoVO.getNovoValorPlanoVO().getValorAnualDependente());
			params.addValue("valorMensalTitular", planoVO.getNovoValorPlanoVO().getValorMensalTitular());
			params.addValue("valorMensalDependente", planoVO.getNovoValorPlanoVO().getValorMensalDependente());
			
			params.addValue("dataInicioVigencia", new java.sql.Date(planoVO.getNovoValorPlanoVO().getDataInicio().getMillis()));
			params.addValue("dataFimVigencia", new java.sql.Date(planoVO.getNovoValorPlanoVO().getDataFim().getMillis()));
			
		}else{
			
			params.addValue("valorAnualTitular",planoVO.getValorPlanoVO().getValorAnualTitular());
			params.addValue("valorAnualDependente", planoVO.getValorPlanoVO().getValorAnualDependente());
			params.addValue("valorMensalTitular", planoVO.getValorPlanoVO().getValorMensalTitular());
			params.addValue("valorMensalDependente", planoVO.getValorPlanoVO().getValorMensalDependente());
			
			params.addValue("dataInicioVigencia", new java.sql.Date(planoVO.getDataInicioVigencia().getMillis()));
			params.addValue("dataFimVigencia", new java.sql.Date(planoVO.getDataFimVigencia().getMillis()));
		}
		
		params.addValue("dataInclusao", new java.sql.Timestamp(planoVO.getDataUltimaAtualizacao().getMillis()));
		params.addValue("codigoResponsavel", planoVO.getCodigoResponsavel());
					
		getJdbcTemplate().update(INCLUIR_VALORES_PLANO, params);
		
	}
	
	@Override
	public ValorPlanoVO obterValorAtualPlanoPorCodigo(Long codigo){
		
		ValorPlanoVO valorPlanoVO= null;
		try{

			MapSqlParameterSource params = new MapSqlParameterSource();		
			params.addValue("codigoPlano", codigo);
			valorPlanoVO = getJdbcTemplate().queryForObject(OBTER_VALORES_ATUAL_PLANO, params, new ValorPlanoRowMapper());
			
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO OBTER VALORES PLANO");
		}
		
		return valorPlanoVO;
	}
	
	@Override
	public ValorPlanoVO obterNovoValorPlanoPorCodigo(Long codigo){
		
		ValorPlanoVO valorPlanoVO= null;
		try{

			MapSqlParameterSource params = new MapSqlParameterSource();		
			params.addValue("codigoPlano", codigo);
			valorPlanoVO = getJdbcTemplate().queryForObject(OBTER_NOVO_VALOR_PLANO, params, new ValorPlanoRowMapper());
			
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO OBTER VALORES PLANO");
		}
		
		return valorPlanoVO;
	}
	
	@Override
	public ValorPlanoVO obterValorPlanoPorCodigo(Long codigo){
		
		ValorPlanoVO valorPlanoVO= null;
		try{

			MapSqlParameterSource params = new MapSqlParameterSource();		
			params.addValue("codigoPlano", codigo);
			valorPlanoVO = getJdbcTemplate().queryForObject(OBTER_VALORES_PLANO, params, new ValorPlanoRowMapper());
			
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO INCLUIR VALORES PLANO");
		}
		
		return valorPlanoVO;
	}	
	
	
	@Override
	public void desativarVigencia(Long codigoPlano, Integer codigoCanal, DateTime dataInicioVigencia, DateTime dataFimVigencia, DateTime dataAtualizacao){
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoPlano", codigoPlano);
		params.addValue("codigoCanal", codigoCanal);
		params.addValue("dataInicioVigencia", new java.sql.Date(dataInicioVigencia.getMillis()));
		params.addValue("dataFimVigencia", new java.sql.Date(dataFimVigencia.getMillis()));
		params.addValue("dataAtualizacao", new java.sql.Timestamp(dataAtualizacao.getMillis()));
		
		getJdbcTemplate().update(DESATIVAR_VIGENCIA, params);
	}
	
	@Override
	public void atualizarVigencia(Long codigoPlano, Integer codigoCanal, Date dataInicioVigencia){
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoPlano", codigoPlano);
		params.addValue("codigoCanal", codigoCanal);
		params.addValue("dataInicioVigencia", dataInicioVigencia);
		
		getJdbcTemplate().update(ATUALIZAR_VIGENCIA, params);

	}
	
	public boolean verificarSeCanalEstaAssociadoAoPlano(Long codigoPlano, Integer codigoCanal, Date dataInicioVigencia){
		MapSqlParameterSource params = new MapSqlParameterSource();		
		params.addValue("codigoPlano", codigoPlano);
		params.addValue("codigoCanal", codigoCanal);
		params.addValue("dataInicioVigencia", dataInicioVigencia);
		try{
			return (getJdbcTemplate().queryForInt(OBTER_ASSOCIACAO_PLANO_CANAL, params) != 0);
		}catch (EmptyResultDataAccessException eda){
			return false;
		}		
	}
	
	@Override
	public void alterarPlano(PlanoVO planoVO){
			
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoPlano", planoVO.getCodigo());
		params.addValue("nomePlano", planoVO.getNome().toUpperCase());
		params.addValue("codigoRegistroPlano", planoVO.getCodigoRegistro());
		params.addValue("codigoResponsavel", planoVO.getCodigoResponsavel());
		params.addValue("dataInicioVigencia", new java.sql.Date(planoVO.getDataInicioVigencia().getMillis()));
		params.addValue("dataFimVigencia", new java.sql.Date(planoVO.getDataFimVigencia().getMillis()));
		params.addValue("descricaoPeriodoCarenciaAnual", planoVO.getDescricaoCarenciaPeriodoAnual());
		params.addValue("descricaoPeriodoCarenciaMensal", planoVO.getDescricaoCarenciaPeriodoMensal());
		
		params.addValue("dataAtualizacao", new Timestamp(planoVO.getDataUltimaAtualizacao().getMillis()));

		getJdbcTemplate().update(ALTERAR_PLANO, params);
		
	}
	
	@Override
	public void desativarValoresPlano(PlanoVO planoVO){
			
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPlano", planoVO.getCodigo());
		param.addValue("dataFimVigencia", new Timestamp(planoVO.getValorPlanoVO().getDataFim().getMillis()));
		
		getJdbcTemplate().update(DESATIVAR_VALOR_PLANO, param);
	}
	
	@Override
	public void desativarValorProgramadoPlano(PlanoVO planoVO){
			
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPlano", planoVO.getCodigo());
		param.addValue("dataFimVigencia", new Timestamp(new DateTime().getMillis()));
		param.addValue("codigoResponsavel", planoVO.getCodigoResponsavel());
		
		getJdbcTemplate().update(DESATIVAR_VALOR_PROGRAMADO_PLANO, param);
	}
	
	public PlanoVO obterPlanoPorCodigoCanalDeVenda(Integer codigoCanalVenda){
		
		PlanoVO plano = null;
		
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("codigoCanalVenda", codigoCanalVenda);
			plano =   getJdbcTemplate().queryForObject(OBTER_PLANO_POR_CANAL_E_DATA_MOVIMENTO,  param, new PlanoRowMapper());
		    plano.setValorPlanoVO(obterValorPlanoPorCodigo(plano.getCodigo()));
		return plano;
		
	}
	
	public PlanoVO obterPlanoAtualPorCodigoCanal(Integer codigoCanalVenda){
		
		PlanoVO plano = null;
		
			MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("codigoCanalVenda", codigoCanalVenda);
			plano =   getJdbcTemplate().queryForObject(OBTER_PLANO_ATUAL_POR_CANAL,  param, new PlanoRowMapper());
		    plano.setValorPlanoVO(obterValorAtualPlanoPorCodigo(plano.getCodigo()));
		return plano;
		
	}
	
	public PlanoVO buscarPlanoPorCodigoCanalEDataMovimento(Long canal, Date dataMovimento){
		
		PlanoVO planoVO = new PlanoVO();	
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT PLANO.CPLANO_DNTAL_INDVD, PLANO.RPLANO_DNTAL_INDVD, PLANO.CREG_PLANO_DNTAL,    \n");
		sql.append(" PLANO.CRESP_ULT_ATULZ, PLANO.DINIC_PLANO_INDVD, PLANO.DFIM_VGCIA,                     \n");                                 
		sql.append(" PLANO.RPER_CAREN_PLANO_ANO, PLANO.RPER_CAREN_PLANO_MES ,                              \n");                                 
		sql.append(" PLANO.DULT_ATULZ_REG 	                                                               \n");                                 
		sql.append(" FROM DBPROD.PLANO_DNTAL_INDVD PLANO, 	                                               \n");                             
		sql.append(" DBPROD.PPSTA_DNTAL_INDVD PROPOSTA, DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_VENDA     \n");                         
		sql.append(" WHERE PROPOSTA.CCANAL_VDA_DNTAL_INDVD = :codigoCanalVenda    	                       \n");                                 
		sql.append(" AND CANAL_VENDA.CCANAL_VDA_DNTAL_INDVD = PROPOSTA.CCANAL_VDA_DNTAL_INDVD	           \n");                                 
		sql.append(" AND PLANO.CPLANO_DNTAL_INDVD = CANAL_VENDA.CPLANO_DNTAL_INDVD	                       \n");                                 
		sql.append(" AND CANAL_VENDA.DFIM_VGCIA is null                                                    \n");   							   
		sql.append(" AND PLANO.DINIC_PLANO_INDVD <= :dataMovimento AND PLANO.DFIM_VGCIA >= :dataMovimento  \n");                                     
		sql.append(" ORDER BY PLANO.DINIC_PLANO_INDVD DESC FETCH FIRST 1 ROWS ONLY                         \n");   
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoCanalVenda", canal);
		params.addValue("dataMovimento", new java.sql.Date(dataMovimento.getTime()));
	
	    try{
	    	 planoVO = getJdbcTemplate().queryForObject(sql.toString(), params, new PlanoRowMapper());
	    	  planoVO.setValorPlanoVO(obterValorPlanoPorCodigoPlanoEDataMovimento(planoVO.getCodigo(), dataMovimento));
	    	 
	    }catch(Exception e){
	    	LOGGER.error("INFO: ERRO AO BUSCAR PLANO POR C�DIGO CANAL E DATA MOVIMENTO");
	    }
	    
	    return planoVO;
		
	}
	
	
	public void atualizarValoresTaxaEDesconto(ValorPlanoVO valorPlano){
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		StringBuilder sql = new StringBuilder();
		

		sql.append(" UPDATE DBPROD.VLR_PLANO_DNTAL_INDVD  SET VTX_PLANO_INDVD = :valorTaxa, VDESC_PLANO_DNTAL = :valorDesconto \n");
		sql.append(" WHERE NSEQ_VLR_PLANO_DNTAL = :sequencial																		   \n");
		
		params.addValue("valorTaxa", valorPlano.getValorTaxa());
		params.addValue("valorDesconto", valorPlano.getValorDesconto());
		params.addValue("sequencial", valorPlano.getSequencial());
		
		getJdbcTemplate().update(sql.toString(), params);
	}

	private ValorPlanoVO obterValorPlanoPorCodigoPlanoEDataMovimento(Long codigoPlano,
			Date dataMovimento) {
		ValorPlanoVO valorPlanoVO = null;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoPlano", codigoPlano);
		params.addValue("dataMovimento", new java.sql.Date(dataMovimento.getTime()));
		
		try{
			valorPlanoVO =  getJdbcTemplate().queryForObject(OBTER_VALOR_PLANO_POR_CODIGO_PLANO_E_DATA_MOVIMENTO, params, new ValorPlanoRowMapper());
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO OBTER VALOR DE PLANO.");
		}
		
		return valorPlanoVO;
	}
	
	public boolean consultarData(){
		MapSqlParameterSource param = new MapSqlParameterSource();
		String sql = "SELECT COUNT(*) FROM DBPROD.PARMZ_COMCZ_PLANO_DNTAL WHERE DINIC_VGCIA_PARMZ_PLANO = CURRENT_DATE";
		boolean result = false;
		int count = getJdbcTemplate().queryForInt(sql, param);
		
		if(count > 0){
			result = true;
		}
		return result;
	}
	
	/*n�o funciona o update de seguimento*\
	/*public void incluirAlterarSeguimento(Integer segmento, PlanoVO planoVO){
		StringBuilder sql = new StringBuilder();
		sql.append("DBPROD.PARMZ_COMCZ_PLANO_DNTAL SET CSGMTO_SUCUR =: segmento  WHERE NOT  CSGMTO_SUCUR = 2  AND NOT CSGMTO_SUCUR = 3 AND DFIM_VGCIA_PARMZ_PLANO IS NULL ");
		sql.append("DBPROD.PARMZ_COMCZ_PLANO_DNTAL              \n");
		sql.append("SET CSGMTO_SUCUR = :segmento \n");
		sql.append(" WHERE NOT CSGMTO_SUCUR = 2 AND NOT CSGMTO_SUCUR = 3");
		 
        
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("usuario", planoVO.getCodigoResponsavel());
		params.addValue("segmento", segmento);
		getJdbcTemplate().update(sql.toString(), params);
	}*/
	
	public void incluirSegmento(Integer segmento, PlanoVO planoVO){
		
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT                                     ")
			.append("INTO                                       ")
			.append("    DBPROD.PARMZ_COMCZ_PLANO_DNTAL         ")
			.append("    (                                      ")
			.append("        CPLANO_DNTAL_INDVD,                ")
			.append("        CSGMTO_SUCUR,                      ")
			.append("        CIND_FORMA_PGTO,                   ")
			.append("        CBCO_PGTO,                         ")
			.append("        DINIC_VGCIA_PARMZ_PLANO,           ")
			.append("        DFIM_VGCIA_PARMZ_PLANO,            ")
			.append("        CRESP_ULT_ATULZ,                   ")
			.append("        DULT_ATULZ_REG                     ")
			.append("    )                                      ")
			.append("    VALUES                                 ")
			.append("    (                                      ")
			.append("        :plano,                            ")
			.append("        :segmento,                         ")
			.append("        1,                                 ")
			.append("        237,                               ")
			.append("        CURRENT_DATE,                      ")
			.append("        null,                              ")
			.append("        :usuario,                          ")
			.append("        :data_atualizacao                  ")
			.append("    )                                      ");
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		params.addValue("plano", planoVO.getCodigo());
		params.addValue("segmento", segmento);
		params.addValue("usuario", planoVO.getCodigoResponsavel());
		params.addValue("data_atualizacao", new java.sql.Timestamp(planoVO.getDataUltimaAtualizacao().getMillis()));
				
		getJdbcTemplate().update(sql.toString(), params);
	}
	
	public List<Integer> obterSegmentoAssociadoAoPlano(Long codigoPlano){
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT                                  ")
		   .append("    CSGMTO_SUCUR                        ")
		   .append("FROM                                    ")
		   .append("    DBPROD.PARMZ_COMCZ_PLANO_DNTAL      ")
		   .append("WHERE                                   ")
		   .append("    CPLANO_DNTAL_INDVD = :plano         ")
		   .append("AND DFIM_VGCIA_PARMZ_PLANO IS NULL      ");
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("plano", codigoPlano);
		
		return getJdbcTemplate().query(sql.toString(), params , new SegmentoRowMapper());
	}
	
	public void desativarSegmentos(Long codigo, Integer segmento, DateTime dataInicioVigencia){
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE                                          ")
		.append("    DBPROD.PARMZ_COMCZ_PLANO_DNTAL                 ")
		.append("SET                                                ")
		.append("    DFIM_VGCIA_PARMZ_PLANO = CURRENT_DATE          ")
		.append("WHERE                                              ")
		.append("    CPLANO_DNTAL_INDVD = :plano                    ")
		.append("AND DFIM_VGCIA_PARMZ_PLANO IS NULL                 ");
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		params.addValue("plano", codigo);
				
		getJdbcTemplate().update(sql.toString(), params);
	}

}
