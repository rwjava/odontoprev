package br.com.bradseg.eedi.administrativo.plano.facade;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import br.com.bradseg.eedi.administrativo.support.Validador;
import br.com.bradseg.eedi.administrativo.vo.FiltroPlanoVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoViewVO;
import br.com.bradseg.eedi.administrativo.vo.ValorPlanoVO;

public interface PlanoServiceFacade {

	/**
	 * Obt�m uma lista de planos cadastrados
	 * @return List PlanoVO - lista de {@link PlanoVO}
	 */
	public List<PlanoVO>listarPlanos();
	
	/**
	 * Consulta planos por filtro
	 * @param filtro - {@link FiltroPlanoVO}
	 * @return List PlanoVO - lista de {@link PlanoVO}
	 */
	public List<PlanoVO>consultarPlanos(FiltroPlanoVO filtro);

	/**
	 * Valida se os campos obrigat�rios foram preenchidos
	 * @param planoVO - Objeto de {@link PlanoViewVO}
	 */
	public void validarPreenchimentoCampos(PlanoViewVO planoVO);

	/**
	 * Verifica se existe plano cadastrado com c�digo informado
	 * @param nomePlano - nome do plano
	 * @return true se existe, false se n�oe existe
	 */
	public boolean verificarExistenciaPlanoPorCodigo(String nomePlano);

	/**
	 * Valida se os canais de venda selecionados existem<br>
	 * Valida se a diferen�a de datas entre a nova vig�ncia e a antiga � de 1(um) dia.
	 * @param planoVO - objeto de {@link PlanoVO}
	 */
	public void validarCanaisVenda(PlanoVO planoVO);

	/**
	 * Valida se o nome do plano existe na base
	 * Valida se a data de vig�ncia inicial � maior que a final
	 * @param planoVO - objeto de {@link PlanoVO}
	 */
	public void validarNomeEDataVigenciaPlano(PlanoVO planoVO);
	
	/**
	 * Valida se o nome do plano existe na base
	 * @param nomePlano
	 * @param validador
	 */
	public void validarNomePlano(String nomePlano, Validador validador);

	
	/**
	 * Valida se o nome do plano existe na base
	 * @param nomePlano
	 * 
	 */
	public void validarNomePlano(String nomePlano);
	
	/**
	 * Valida se a data de vig�ncia inicial � maior que a final
	 * @param planoVO
	 * @param validador
	 */
	public void validarDataVigenciaPlano(PlanoVO planoVO, Validador validador);
	
	/**
	 * Valida se a data de vig�ncia inicial � maior que a final
	 * @param planoVO
	 * 
	 */
	public void validarDataVigenciaPlano(PlanoVO planoVO);
	
	/**
	 * Busca o plano mais recente por c�digo de canal
	 * @param codigoCanal - c�digo do canal de venda
	 * @return PlanoVO - objeto de {@link PlanoVO}
	 */
	public PlanoVO obterPlanoMaisRecentePorCodigoCanal(Integer codigoCanal);
	
	/**
	 * Busca o plano mais recente por c�digo de canal
	 * @param codigoPlano - c�digo do plano
	 * @return PlanoVO - objeto de {@link PlanoVO}
	 */
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano);


	/**
	 * Inclui um novo plano
	 * @param planoVO - objeto de {@link PlanoVO}
	 */
	public void incluirPlano(PlanoVO planoVO);
	


	/**
	 * Inclui vig�ncias do plano
	 * @param planoVO - Objeto de planoVO
	 */
	public void incluirVigenciaCanais(PlanoVO planoVO);

    /**
     * Inclui valores do plano
     * @param planoVO - Objeto de planoVO
     */
	public void incluirValoresPlano(PlanoVO planoVO);

	/**
	 * Procura valor do plano por c�digo
	 * @param codigo
	 * @return {@link ValorPlanoVO}
	 */
	public ValorPlanoVO obterValorPlanoPorCodigo(Long codigo);
	
	/**
	 * Procura valor do plano por c�digo
	 * @param codigo
	 * @return {@link ValorPlanoVO}
	 */
	public ValorPlanoVO obterNovoValorPlanoPorCodigo(Long codigo);


	/**
	 * Desativa o canal de venda
	 * @param codigo do plano
	 * @param codigo do canal de venda
	 */
	public void desativarVigencia(Long codigoPlano, Integer codigoCanal, DateTime dataInicioVigencia, DateTime dataFimVigencia, DateTime dataAtualizacao);
	
	
	/**
	 * Alterar um  plano
	 * @param planoVO - objeto de {@link PlanoVO}
	 */
	public void alterarPlano(PlanoVO planoVO, PlanoVO planoAnteriorVO);

	/**
	 * Desativa valor de plano
	 * @param planoVO
	 */
	public void desativarValoresPlano(PlanoVO planoVO);
	
	/**
	 * Obt�m 
	 * @param codigoCanalVenda
	 * @return PlanoVO
	 */
	public PlanoVO obterPlanoPorCodigoCanalDeVenda(Integer codigoCanalVenda);

	/**
	 * Obt�m 
	 * @param codigoCanalVenda
	 * @return PlanoVO
	 */
	public PlanoVO obterPlanoAtualPorCodigoCanal(Integer codigoCanalVenda);
	
	/**
	 * Buscar plano por c�digo de canal de venda e data de movimento
	 * @param canal
	 * @param dataMovimento
	 * @return PlanoVO 
	 */
	public PlanoVO buscarPlanoPorCodigoCanalEDataMovimento(Long canal, Date dataMovimento);

	/**
	 * Verifica se houve altera��o do plano 
	 * @param planoAnterior - plano pego da base
	 * @param planoVO - plano pego da tela
	 */
	public void verificarSeHouveAlteracaoDoPlano(PlanoVO planoAnterior,
			PlanoVO planoVO);
	
	/**
	 * Verifica os novos canais de venda
	 * @param planoVO - objeto com poss�veis canais novos
	 * @param planoAnterior -objeto com os canais 
	 */
	public void validarCanaisVendaNovos(PlanoVO planoVO, PlanoVO planoAnterior);

	/**
	 * Verifica se houve altera��o de valor
	 * N�o verifica valor de taxa e desconto
	 * @param planoVO - objeto com os novos valores
	 * @param planoAnterior - objeto com valores da base
	 * @return
	 */
	public boolean verificarSeValoresAlterados(PlanoVO planoVO,
			PlanoVO planoAnterior);

	/**
	 * M�todo respons�vel pela inclus�o de plano
	 * <ul>
	 * <li>incluirPlano</li>
	 * <li>incluirValoresPlano</li>
	 * <li>incluirVigenciaCanais</li>
	 * </ul>
	 * @param planoVO - objeto de planoVO
	 */
	public void incluir(PlanoVO planoVO);

	/**
	 * Verifica se valor de taxa ou de desconto foram alterados
	 * @param planoVO
	 * @param planoAnterior
	 * @return true se houve altera��o, false se n�o houve
	 */
	public boolean verificarSeHouveAlteracaoDeValorTaxaOuDesconto(
			PlanoVO planoVO, PlanoVO planoAnterior);

	/**
	 * Atualiza valores de taxa e desconto
	 * @param planoVO
	 */
	public void atualizarValoresTaxaEDescontoVigenteEProgramdado(PlanoVO planoVO);

	
}
