package br.com.bradseg.eedi.administrativo.plano.dao;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroPlanoVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoVO;
import br.com.bradseg.eedi.administrativo.vo.ValorPlanoVO;

public interface PlanoDao {

	/**
	 * Obt�m uma lista de planos cadastrados
	 * @return List PlanoVO - lista de {@link PlanoVO}
	 */
	public List<PlanoVO>listarPlanos();
	
	/**
	 * Consulta planos por filtro
	 * @param filtro - {@link FiltroPlanoVO}
	 * @return List PlanoVO - lista de {@link PlanoVO}
	 */
	public List<PlanoVO>consultarPlanos(FiltroPlanoVO filtro);

	/**
	 * Verifica se existe plano cadastrado com c�digo informado
	 * @param nomePlano - nome do plano
	 * @return true se existe, false se n�oe existe
	 */
	public boolean verificarExistenciaPlanoPorCodigo(String nomePlano);
	
	/**
	 * verificar se existe altera��o nesta data
	 * 
	 * */
	
	public boolean consultarData();
	
	/**
	 * Busca o plano mais recente por c�digo de canal
	 * @param codigoCanal - c�digo do canal de venda
	 * @return PlanoVO - objeto de {@link PlanoVO}
	 */
	public PlanoVO obterPlanoMaisRecentePorCodigoCanal(Integer codigoCanal);
	
	/**
	 * Busca o plano  por c�digo 
	 * @param codigoPlano - c�digo do plano
	 * @return PlanoVO - objeto de {@link PlanoVO}
	 */
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano);

	/**
	 * Inclui um novo plano
	 * @param planoVO - objeto de {@link PlanoVO}
	 */
	public void incluirPlano(PlanoVO planoVO);

	/**
	 * Inclui vig�ncias do plano
	 * @param planoVO - Objeto de planoVO
	 */
	public void incluirVigenciaCanal(CanalVendaVO canalVendaVO, PlanoVO planoVO);
	
	/**
	 * Obt�m novo valor da sequence
	 * @return Long - sequence
	 */
	public Long obterNovoValorSequence();

	/**
     * Inclui valores do plano
     * @param planoVO - Objeto de planoVO
     */
	public void incluirValoresPlano(PlanoVO planoVO);
	
	/**
	 * Procura valor do plano por c�digo
	 * @param codigo
	 * @return {@link ValorPlanoVO}
	 */
	public ValorPlanoVO obterValorAtualPlanoPorCodigo(Long codigo);

	/**
	 * Desativa a vig�ncia
	 * @param codigoPlano
	 * @param codigoCanal
	 */
	public void desativarVigencia(Long codigoPlano, Integer codigoCanal, DateTime dataInicioVigencia, DateTime dataFimVigencia, DateTime dataAtualizacao);
	
	/**
	 * Desativa a vig�ncia
	 * @param codigoPlano
	 * @param codigoCanal
	 */
	public void atualizarVigencia(Long codigoPlano, Integer codigoCanal, Date dataInicioVigencia);
	
	public boolean verificarSeCanalEstaAssociadoAoPlano(Long codigoPlano, Integer codigoCanal, Date dataInicioVigencia);

	/**
	 * Alterar um plano
	 * @param planoVO
	 */
	public void alterarPlano(PlanoVO planoVO);
	
	/**
	 * Desativa valor de plano
	 * @param planoVO
	 */
	public void desativarValoresPlano(PlanoVO planoVO);
	
	/**
	 * Desativa valor de plano
	 * @param planoVO
	 */
	public void desativarValorProgramadoPlano(PlanoVO planoVO);
	
	public ValorPlanoVO obterValorPlanoPorCodigo(Long codigo);
	
	public ValorPlanoVO obterNovoValorPlanoPorCodigo(Long codigo);
	
	/**
	 * Obt�m plano por canal
	 * @param codigoCanalVenda
	 * @return
	 */
	public PlanoVO obterPlanoPorCodigoCanalDeVenda(Integer codigoCanalVenda);
	
	/**
	 * Obt�m plano por canal
	 * @param codigoCanalVenda
	 * @return
	 */
	public PlanoVO obterPlanoAtualPorCodigoCanal(Integer codigoCanalVenda);
	
	/**
	 * Buscar plano por c�digo de canal de venda e data de movimento
	 * @param canal
	 * @param dataMovimento
	 * @return PlanoVO 
	 */
	public PlanoVO buscarPlanoPorCodigoCanalEDataMovimento(Long canal, Date dataMovimento);

	/**
	 * Atualiza valor da taxa e de desconto de plano
	 * @param valorPlanoVigente
	 */
	public void atualizarValoresTaxaEDesconto(ValorPlanoVO valorPlanoVigente);

	public void incluirSegmento(Integer segmento, PlanoVO planoVO);
	
	//public void incluirAlterarSeguimento(Integer segmento, PlanoVO planoVO);

	public List<Integer> obterSegmentoAssociadoAoPlano(Long codigoPlano);

	public void desativarSegmentos(Long codigo, Integer segmento, DateTime dataInicioVigencia);
	
	
	
}
