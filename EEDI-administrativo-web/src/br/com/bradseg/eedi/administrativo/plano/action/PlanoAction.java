

package br.com.bradseg.eedi.administrativo.plano.action;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.eedi.administrativo.plano.facade.CanalVendaServiceFacade;
import br.com.bradseg.eedi.administrativo.plano.facade.PlanoServiceFacade;
import br.com.bradseg.eedi.administrativo.support.Constantes;
import br.com.bradseg.eedi.administrativo.support.action.BaseAction;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroPlanoVO;
import br.com.bradseg.eedi.administrativo.vo.LabelValueVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoViewVO;
import br.com.bradseg.eedi.administrativo.vo.Status;
import br.com.bradseg.eedi.administrativo.vo.TipoCobranca;
import br.com.bradseg.eedi.administrativo.vo.TipoSucursal;
import br.com.bradseg.eedi.administrativo.vo.ValorPlanoVO;



/**
 * Action respons�vel por disponibilizar os metodos referentes as a��es de tela da funcionalidade plano.
 * 
 * @author WDEV
 */
@Controller
@Scope("request")
public class PlanoAction extends BaseAction {

	private static final long serialVersionUID = -1161409943678292285L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanoAction.class);

	private PlanoViewVO planoViewVO = new PlanoViewVO();
	
	private PlanoVO planoVO = new PlanoVO();
	
	private FiltroPlanoVO filtro = new FiltroPlanoVO();

	@Autowired
	private PlanoServiceFacade planoServiceFacade;
	
	@Autowired
	private CanalVendaServiceFacade canalServiceFacade;
	
	
	private List<PlanoVO>listaDePlanos;
	
	private List<Integer> listaDeCanaisSelecionados = new ArrayList<Integer>();

	private List<Integer> listaDeCanaisSelecionadosParaAlteracao = new ArrayList<Integer>();

	private List<CanalVendaVO> listaDeCanais = new ArrayList<CanalVendaVO>();
	
	//private List<CanalVendaVO> listaDeCanaisSelecionadosTela = new ArrayList<CanalVendaVO>();
	
	private List<LabelValueVO> listaDeStatus;
	
	private List<LabelValueVO>listaDeTipoCobranca = TipoCobranca.obterLista();
	
	private List<TipoSucursal> listaTipoSucursal;
	
	private List<Integer> listaTipoSucursalSelecionado = new ArrayList<Integer>();
	
	private List<TipoSucursal> segmentos = new ArrayList<TipoSucursal>();
	
	private String valorMes;
	
	private String valorAnual;
	
	
	
	/**
	 * Lista os planos cadastrados
	 * @return
	 */
	public String listarPorFiltro() {		
		
		setListaDeTipoCobranca(TipoCobranca.obterLista());
		planoViewVO.setListaDeCanaisSelecionados(new ArrayList<CanalVendaVO>());
		listaDeCanaisSelecionados = new ArrayList<Integer>();
		
		try {
			//obt�m todos os status
			listaDeStatus = Status.obterLista();
			
			setListaDeCanais(canalServiceFacade.listarTodosCanaisVenda());			
			
			if (filtro.isFiltroPreenchido()) {
				listaDePlanos = planoServiceFacade.consultarPlanos(filtro);
				
			}else{
				 listaDePlanos =  planoServiceFacade.listarPlanos();
				 //Mock
				 /*listaDePlanos = new ArrayList<PlanoVO>();
				 PlanoVO plano = new PlanoVO();
				 plano.setCodigo(1L);
				 plano.setNome("Bradesco Dental");
				 plano.setCodigoRegistro(11L);
				 plano.setCodigoResponsavel("BRA");
				 plano.setDataInicioVigencia(new DateTime(new Date().getTime()));
				 plano.setDataFimVigencia(new DateTime(new Date().getTime()));
				 plano.setDescricaoCarenciaPeriodoAnual("Um ano");
				 plano.setDescricaoCarenciaPeriodoMensal("UM m�s");
				 plano.setDataUltimaAtualizacao(new LocalDate(new Date()));
				 
				 listaDePlanos.add(plano);
				*/
			}
			consultarCanaisDosPlanos(listaDePlanos);
			verificarPodeAlterarPlano(listaDePlanos);
			
		} catch (Exception e) {
			LOGGER.error("Erro ao listar o"
					+ "s planos.", e);
			erro(e);
		}
//		filtro.setNomePlano(null);
//		filtro.setCodigoCanal(null);
		return SUCCESS;
	}
   /**
    * Preenche os canais de venda dos planos listados
    * @param listaDePlanos
    */
	private void consultarCanaisDosPlanos(List<PlanoVO> listaDePlanos){
		
		for(PlanoVO plano : listaDePlanos){
			
			plano.setListaDeCanais(canalServiceFacade.obterCanaisPorCodigoPlano(plano.getCodigo()));
		}
	}
	
	private void verificarPodeAlterarPlano(List<PlanoVO> listaDePlanos){
		
		for (PlanoVO planoVO : listaDePlanos) {
			if(planoVO.getDataFimVigencia().toLocalDate().isBefore(new LocalDate())){
				planoVO.setPodeAlterarPlano(Constantes.NAO_ALTERA);				
			}else{
				planoVO.setPodeAlterarPlano(Constantes.ALTERA);
			}
		}		
	}
    
	/**
	 * Inicia a tela de incluir plano
	 * @return
	 */
	public String iniciarIncluir(){
		
		setListaDeTipoCobranca(TipoCobranca.obterLista());
		planoViewVO.setListaDeCanais(canalServiceFacade.listarTodosCanaisVenda());
		setListaTipoSucursal(TipoSucursal.listar());
		
		return SUCCESS;
	}
	
	/**
	 * Inclui um novo plano
	 * @return
	 */
	public String incluir(){
		
		setListaTipoSucursal(TipoSucursal.listar());
		
		planoViewVO.setTipoCobranca(TipoCobranca.buscaPor(planoViewVO.getTipoCobrancaPlano()));		
		
		preencherCanais();
		//seta com os canais que foram marcados
		
		//preenche os valores do plano
		planoViewVO.setValorPlanoVO(preencherValorPlano());
		
		try{
			//seta as datas de vig�ncia
			planoViewVO.setDataInicioVigencia(new DateTime(planoViewVO.getDataInicioVigenciaTela().getTime()));
			planoViewVO.setDataFimVigencia(new DateTime(planoViewVO.getDataFimVigenciaTela().getTime()));
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO PEGAR DATAS");
		}
		setPlanoVO(popularObjetoPlanoVO(planoViewVO));
		//planoVO.setCodigoRegistro(123L);
		planoViewVO.setListaDeCanais(canalServiceFacade.listarTodosCanaisVenda());
		
		planoServiceFacade.validarPreenchimentoCampos(planoViewVO);
		
		

		//validar se nome j� existe na base 
		//validar se data fim menor que data in�cio
		planoServiceFacade.validarNomeEDataVigenciaPlano(planoVO);
		
		//validar canais de venda
		//planoServiceFacade.validarCanaisVenda(planoVO);
		
		planoServiceFacade.incluir(planoVO);
		
		
		listaDePlanos =  planoServiceFacade.listarPlanos();
		planoViewVO.setListaDeCanais(canalServiceFacade.listarTodosCanaisVenda());
		
		sucesso("msg.sucesso.cadastro", "Plano");
		
		return SUCCESS;
	}
	
	
	public String visualizar(){
		
		planoVO = planoServiceFacade.obterPlanoPorCodigo(planoVO.getCodigo());
		planoVO.setListaDeCanais(canalServiceFacade.obterCanaisPorCodigoPlano(planoVO.getCodigo()));
		planoVO.setValorPlanoVO(planoServiceFacade.obterValorPlanoPorCodigo(planoVO.getCodigo()));
		planoVO.setNovoValorPlanoVO(planoServiceFacade.obterNovoValorPlanoPorCodigo(planoVO.getCodigo()));
		
		planoViewVO = popularObjetoVisualizarPlanoView(planoVO);
		planoViewVO.setTipoCobrancaPlano(planoViewVO.getTipoCobranca().getCodigo());
		planoViewVO.setDescricaoTipoCobranca(planoViewVO.getTipoCobranca().getDescricao());
		if(planoVO.isExisteValorProgramado()){
			planoViewVO.setNovoTipoCobrancaPlano(planoViewVO.getNovoTipoCobranca().getCodigo());
			planoViewVO.setDescricaoNovoTipoCobranca(planoViewVO.getNovoTipoCobranca().getDescricao());
		}
		
		if(!planoVO.getListaTipoSucursal().isEmpty()){
			
			for (Integer segmento : planoVO.getListaTipoSucursal()) {
				TipoSucursal tipoSucursal = TipoSucursal.buscaPor(segmento);
				if(tipoSucursal != null){
					segmentos.add(tipoSucursal);
				}
			}
		}
		
		return SUCCESS;
	}
	
	public String iniciarAlterar(){
		
		setListaTipoSucursal(TipoSucursal.listar());
		
		planoViewVO.setTipoCobranca(TipoCobranca.buscaPor(planoViewVO.getTipoCobrancaPlano()));
		setListaDeTipoCobranca(TipoCobranca.obterLista());
		
		
		planoVO = planoServiceFacade.obterPlanoPorCodigo(planoVO.getCodigo());
		
		planoVO.setListaDeCanais(canalServiceFacade.listarTodosCanaisVenda());
		
		
		planoVO.setValorPlanoVO(planoServiceFacade.obterValorPlanoPorCodigo(planoVO.getCodigo()));
		planoVO.setNovoValorPlanoVO(planoServiceFacade.obterNovoValorPlanoPorCodigo(planoVO.getCodigo()));
	
		
		planoViewVO = popularObjetoPlanoView( planoVO);		
		planoViewVO.setListaDeCanaisSelecionados(canalServiceFacade.obterCanaisPorCodigoPlano(planoVO.getCodigo()));
		
		//planoViewVO.setListaDeCanaisSelecionados(canais);
		listaDeCanaisSelecionados = new ArrayList<Integer>();
		
		preencherValoresTipoDeCobrancaTelaAlterar(planoVO);
		
		planoViewVO.setTipoCobrancaPlano(planoViewVO.getTipoCobranca().getCodigo());
		if(planoViewVO.isExisteValorProgramado()){
			planoViewVO.setNovoTipoCobrancaPlano(planoViewVO.getNovoTipoCobranca().getCodigo());
		}
		//planoViewVO.getCodigo();
		
		return SUCCESS;
	}
	
	

	public String alterar(){
		
		setListaTipoSucursal(TipoSucursal.listar());
		
		//int canaisIguais = 0;
		preencherCanais();
		//planoViewVO.setListaDeCanaisSelecionados(canalServiceFacade.obterCanaisPorCodigoPlano(planoVO.getCodigo()));
		planoViewVO.setTipoCobranca(TipoCobranca.buscaPor(planoViewVO.getTipoCobrancaPlano()));
		planoViewVO.setNovoTipoCobranca(TipoCobranca.buscaPor(planoViewVO.getNovoTipoCobrancaPlano()));
		
		
		setListaDeTipoCobranca(TipoCobranca.obterLista());
		listaDeCanaisSelecionados = new ArrayList<Integer>();
		
		PlanoVO planoAnterior = new PlanoVO();
		planoAnterior = planoServiceFacade.obterPlanoPorCodigo(planoVO.getCodigo());
		
		planoAnterior.setListaDeCanais(canalServiceFacade.obterCanaisPorCodigoPlano(planoVO.getCodigo()));	
		
		planoAnterior.setValorPlanoVO(planoServiceFacade.obterValorPlanoPorCodigo(planoAnterior.getCodigo()));
		planoAnterior.setNovoValorPlanoVO(planoServiceFacade.obterNovoValorPlanoPorCodigo(planoVO.getCodigo()));
		if(planoAnterior.getValorPlanoVO() == null){
			planoAnterior.setValorPlanoVO(planoAnterior.getNovoValorPlanoVO());
			planoAnterior.setNovoValorPlanoVO(null);
		}
		planoAnterior.setExisteValorProgramado(planoAnterior.getNovoValorPlanoVO() != null);
		if(planoAnterior.isExisteValorProgramado()){
			planoAnterior.setDataProgramadaNovoValorPlano(planoAnterior.getNovoValorPlanoVO().getDataInicio());
		}
		
		//preenche os valores do plano
		planoViewVO.setValorPlanoVO(preencherValorPlano());
		if(planoViewVO.isExisteValorProgramado()){
			planoViewVO.setNovoValorPlanoVO(preencherNovoValorPlano());
		}
		
		try{
			//seta as datas de vig�ncia
			planoViewVO.setDataInicioVigencia(new DateTime(planoViewVO.getDataInicioVigenciaTela().getTime()));
			planoViewVO.setDataFimVigencia(new DateTime(planoViewVO.getDataFimVigenciaTela().getTime()));
			if(planoViewVO.isExisteValorProgramado()){
				planoViewVO.setDataProgramadaNovoValorPlano(new DateTime(planoViewVO.getDataInicioNovoValor().getTime()));
			}
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO PEGAR DATAS");
		}
		alterarObjetoPlanoVO(planoViewVO, planoVO);
		
		listaDePlanos =  planoServiceFacade.listarPlanos();
		planoViewVO.setListaDeCanais(canalServiceFacade.listarTodosCanaisVenda());
		
		
		planoServiceFacade.verificarSeHouveAlteracaoDoPlano(planoAnterior,planoVO);
		
		//seta com os canais que foram marcados
		//planoVO.setListaDeCanais(planoViewVO.getListaDeCanais());
		
		//apenas para a lista n�o ficar vazia 
		//planoViewVO.setListaDeCanais(canalServiceFacade.listarTodosCanaisVenda());		
		
		planoServiceFacade.validarPreenchimentoCampos(planoViewVO);		

		//validar se nome j� existe na base 
		//validar se data fim menor que data in�cio
		if(!planoVO.getNome().equalsIgnoreCase(planoAnterior.getNome())){
			planoServiceFacade.validarNomePlano(planoVO.getNome());
		}
		if(!planoVO.getDataInicioVigencia().equals(planoAnterior.getDataInicioVigencia()) || !planoVO.getDataFimVigencia().equals(planoAnterior.getDataFimVigencia())){
			planoServiceFacade.validarDataVigenciaPlano(planoVO);
		}

		//TODO validar canais de venda
		// data digitada == data do anterior && canal selecionado == canal anterior
		
//		for (CanalVendaVO canalTela : planoVO.getListaDeCanais()) {
//			for (CanalVendaVO canalAnterior : planoAnterior.getListaDeCanais()){
//				if(canalTela.getCodigo().intValue() == canalAnterior.getCodigo().intValue()){
//					canaisIguais++;
//				}					
//			}				
//		}
		
//		if(!planoVO.getDataInicioVigencia().equals(planoAnterior.getDataInicioVigencia()) || !planoVO.getDataFimVigencia().equals(planoAnterior.getDataFimVigencia())){			
//			planoServiceFacade.validarCanaisVenda(planoVO);			
//		}else{
//			if (canaisIguais != planoVO.getListaDeCanais().size()){
//				planoServiceFacade.validarCanaisVendaNovos(planoVO, planoAnterior);
//			}
//		}
		//planoVO.setDataUltimaAtualizacao(new DateTime());
		
		
		planoServiceFacade.alterarPlano(planoVO, planoAnterior);


//		//primeiro desativa vig�ncias que foram desmarcadas
//		desativarVigencias(planoAnterior);
//		
		
//		planoServiceFacade.alterarPlano(planoVO);
//		
//		//desativa um valor de plano
//		//planoServiceFacade.desativarValoresPlano(planoVO);
//		//inclui novos valores plano
//		//planoServiceFacade.incluirValoresPlano(planoVO);
//
//		
//		if(planoServiceFacade.verificarSeHouveAlteracaoDeValorTaxaOuDesconto(planoVO, planoAnterior)){
//			
//			planoServiceFacade.atualizarValoresTaxaEDescontoVigenteEProgramdado(planoVO);
//		}
//		
//		if(planoServiceFacade.verificarSeValoresAlterados(planoVO, planoAnterior)){
//			//desativa um valor de plano
//			planoServiceFacade.desativarValoresPlano(planoVO);
//			//inclui novos valores plano
//			planoServiceFacade.incluirValoresPlano(planoVO);
//		}
//		
//
//		//troca a lista de canais por uma com apenas os novos canais 
//		planoVO.setListaDeCanais(listaComNovosCanais(planoVO, planoAnterior));
//		
//		//inclui as novas vig�ncias
//		planoServiceFacade.incluirVigenciaCanais(planoVO);
		
		
		
		sucesso("msg.sucesso.alteracao", "Plano");
		
		

		
		return SUCCESS;
	}
	


	public String ocultarExibirTipoCobranca(){
		setListaDeTipoCobranca(TipoCobranca.obterLista());
		return SUCCESS;
	}
	
	/**
	 * Preenche o tipo de cobran�a e os valores para a tela de alterar plano 
	 * @param planoVO Objeto de {@link PlanoVO}
	 */
	private void preencherValoresTipoDeCobrancaTelaAlterar(PlanoVO planoVO){
		
		//Se o valor anual for zero eo valor mensal for diferente de zero o tipo de cobran�a � mensal
				if(Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorAnualTitular()) && !Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorMensalTitular())){
						
					planoViewVO.setTipoCobranca(TipoCobranca.MENSAL);
					
					planoViewVO.setValorMensalTitular(String.valueOf(planoVO.getValorPlanoVO().getValorMensalTitular()));
					planoViewVO.setValorMensalDependente(String.valueOf(planoVO.getValorPlanoVO().getValorMensalDependente()));
					planoViewVO.setValorAnualTitular(Constantes.ZERO);
					planoViewVO.setValorAnualDependente(Constantes.ZERO);
					
				}else if(!Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorAnualTitular()) && Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorMensalTitular())){
					
					planoViewVO.setTipoCobranca(TipoCobranca.ANUAL);
					planoViewVO.setValorAnualTitular(String.valueOf(planoVO.getValorPlanoVO().getValorAnualTitular()));
					planoViewVO.setValorAnualDependente(String.valueOf(planoVO.getValorPlanoVO().getValorAnualDependente()));
					planoViewVO.setValorMensalTitular(Constantes.ZERO);
					planoViewVO.setValorMensalDependente(Constantes.ZERO);
					
				}else  if(!Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorAnualTitular()) && !Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorMensalTitular())){
					planoViewVO.setTipoCobranca(TipoCobranca.MENSAL_E_ANUAL);
					
					planoViewVO.setValorMensalTitular(String.valueOf(planoVO.getValorPlanoVO().getValorMensalTitular()));
					planoViewVO.setValorMensalDependente(String.valueOf(planoVO.getValorPlanoVO().getValorMensalDependente()));
					planoViewVO.setValorAnualTitular(String.valueOf(planoVO.getValorPlanoVO().getValorAnualTitular()));
					planoViewVO.setValorAnualDependente(String.valueOf(planoVO.getValorPlanoVO().getValorAnualDependente()));
				}
				
				if(planoVO.isExisteValorProgramado()){					
					if(Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorAnualTitular()) && !Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorMensalTitular())){
						planoViewVO.setNovoTipoCobranca(TipoCobranca.MENSAL);
					}
					else if(!Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorAnualTitular()) && Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorMensalTitular())){
						planoViewVO.setNovoTipoCobranca(TipoCobranca.ANUAL);
					}
					else  if(!Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorAnualTitular()) && !Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorMensalTitular())){
						planoViewVO.setNovoTipoCobranca(TipoCobranca.MENSAL_E_ANUAL);
					}
				}
				
	}
	
	/**
	 * popula o objeto de PlanoView com os valores da tela
	 * @param planoViewVO
	 * @return
	 */
	private PlanoViewVO popularObjetoPlanoView(PlanoVO planoVO){
		
		
		PlanoViewVO planoViewVO = new PlanoViewVO();
		planoViewVO.setDataInicioVigenciaTela(new Date(planoVO.getDataInicioVigencia().getMillis()));
		planoViewVO.setDataFimVigenciaTela(new Date(planoVO.getDataFimVigencia().getMillis()));
		planoViewVO.setDescricaoCarenciaPeriodoAnual(planoVO.getDescricaoCarenciaPeriodoAnual());
		planoViewVO.setDescricaoCarenciaPeriodoMensal(planoVO.getDescricaoCarenciaPeriodoMensal());
		planoViewVO.setListaDeCanais(planoVO.getListaDeCanais());
		planoViewVO.setListaTipoSucursal(planoVO.getListaTipoSucursal());
		
	    planoViewVO.setNome(planoVO.getNome());
	    planoViewVO.setCodigoRegistro(planoVO.getCodigoRegistro());
	    planoViewVO.setCodigo(planoVO.getCodigo());
	    
	    planoViewVO.setNovaDescricaoCarenciaPeriodoAnual(planoVO.getDescricaoCarenciaPeriodoAnual());
		planoViewVO.setNovaDescricaoCarenciaPeriodoMensal(planoVO.getDescricaoCarenciaPeriodoMensal());
	    
		if(planoVO.getValorPlanoVO() == null && planoVO.getNovoValorPlanoVO() != null){
	    	planoVO.setValorPlanoVO(planoVO.getNovoValorPlanoVO());
	    	planoVO.setNovoValorPlanoVO(null);
	    }
		
		planoViewVO.setValorPlanoVO(planoVO.getValorPlanoVO());
		planoViewVO.setNovoValorPlanoVO(planoVO.getNovoValorPlanoVO());
		planoViewVO.setExisteValorProgramado(planoVO.getNovoValorPlanoVO() != null);
		planoVO.setExisteValorProgramado(planoVO.getNovoValorPlanoVO() != null);
		if(planoViewVO.isExisteValorProgramado()){
			planoViewVO.setDataInicioNovoValor(planoVO.getNovoValorPlanoVO().getDataInicio().toDate());
		}
		
		return planoViewVO;
	}
	
	
	private PlanoViewVO popularObjetoVisualizarPlanoView(PlanoVO planoVO){
		
		DecimalFormat formataMoeda = new DecimalFormat("#,##0.00");		
		
		PlanoViewVO planoViewVO = new PlanoViewVO();
		planoViewVO.setDataInicioVigenciaTela(new Date(planoVO.getDataInicioVigencia().getMillis()));
		planoViewVO.setDataFimVigenciaTela(new Date(planoVO.getDataFimVigencia().getMillis()));
		planoViewVO.setDescricaoCarenciaPeriodoAnual(planoVO.getDescricaoCarenciaPeriodoAnual());
		planoViewVO.setDescricaoCarenciaPeriodoMensal(planoVO.getDescricaoCarenciaPeriodoMensal());
		planoViewVO.setListaDeCanais(planoVO.getListaDeCanais());
		planoViewVO.setValorPlanoVO(planoVO.getValorPlanoVO());
		planoViewVO.setNovoValorPlanoVO(planoVO.getNovoValorPlanoVO());
	    planoViewVO.setNome(planoVO.getNome());
	    planoViewVO.setCodigoRegistro(planoVO.getCodigoRegistro());
	    planoViewVO.setCodigo(planoVO.getCodigo());
	    
	    if(planoVO.getValorPlanoVO() == null && planoVO.getNovoValorPlanoVO() != null){
	    	planoVO.setValorPlanoVO(planoVO.getNovoValorPlanoVO());
	    	planoVO.setNovoValorPlanoVO(null);
	    }
	    
	    planoVO.setExisteValorProgramado(planoVO.getNovoValorPlanoVO() != null);
	    
	    if(planoVO.getValorPlanoVO() != null){
	    	if(Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorAnualTitular()) && !Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorMensalTitular())){
				
				planoViewVO.setTipoCobranca(TipoCobranca.MENSAL);
				
				planoViewVO.setValorMensalTitular(formataMoeda.format(planoVO.getValorPlanoVO().getValorMensalTitular()));
				planoViewVO.setValorMensalDependente(formataMoeda.format(planoVO.getValorPlanoVO().getValorMensalDependente()));
				planoViewVO.setValorAnualTitular(formataMoeda.format(Constantes.ZERO_DOUBLE));
				planoViewVO.setValorAnualDependente(formataMoeda.format(Constantes.ZERO_DOUBLE));
				
							
			}else if(!Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorAnualTitular()) && Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorMensalTitular())){
				
				planoViewVO.setTipoCobranca(TipoCobranca.ANUAL);
				planoViewVO.setValorAnualTitular(formataMoeda.format(planoVO.getValorPlanoVO().getValorAnualTitular()));
				planoViewVO.setValorAnualDependente(formataMoeda.format(planoVO.getValorPlanoVO().getValorAnualDependente()));
				planoViewVO.setValorMensalTitular(formataMoeda.format(Constantes.ZERO_DOUBLE));
				planoViewVO.setValorMensalDependente(formataMoeda.format(Constantes.ZERO_DOUBLE));			
				
			}else  if(!Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorAnualTitular()) && !Constantes.ZERO_DOUBLE.equals( planoVO.getValorPlanoVO().getValorMensalTitular())){
				planoViewVO.setTipoCobranca(TipoCobranca.MENSAL_E_ANUAL);
				
				planoViewVO.setValorMensalTitular(formataMoeda.format(planoVO.getValorPlanoVO().getValorMensalTitular()));
				planoViewVO.setValorMensalDependente(formataMoeda.format(planoVO.getValorPlanoVO().getValorMensalDependente()));
				planoViewVO.setValorAnualTitular(formataMoeda.format(planoVO.getValorPlanoVO().getValorAnualTitular()));
				planoViewVO.setValorAnualDependente(formataMoeda.format(planoVO.getValorPlanoVO().getValorAnualDependente()));			
				
			}
	    }
	    
	    if(planoVO.isExisteValorProgramado()){					
			planoViewVO.setDataInicioNovoValor(planoVO.getNovoValorPlanoVO().getDataInicio().toDate());
	    	
	    	if(Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorAnualTitular()) && !Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorMensalTitular())){
				planoViewVO.setNovoTipoCobranca(TipoCobranca.MENSAL);
				
				planoViewVO.setNovoValorMensalTitular(formataMoeda.format(planoVO.getNovoValorPlanoVO().getValorMensalTitular()));
				planoViewVO.setNovoValorMensalDependente(formataMoeda.format(planoVO.getNovoValorPlanoVO().getValorMensalDependente()));
				planoViewVO.setNovoValorAnualTitular(formataMoeda.format(Constantes.ZERO_DOUBLE));
				planoViewVO.setNovoValorAnualDependente(formataMoeda.format(Constantes.ZERO_DOUBLE));
			}
			else if(!Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorAnualTitular()) && Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorMensalTitular())){
				planoViewVO.setNovoTipoCobranca(TipoCobranca.ANUAL);
				
				planoViewVO.setNovoValorAnualTitular(formataMoeda.format(planoVO.getNovoValorPlanoVO().getValorAnualTitular()));
				planoViewVO.setNovoValorAnualDependente(formataMoeda.format(planoVO.getNovoValorPlanoVO().getValorAnualDependente()));
				planoViewVO.setNovoValorMensalTitular(formataMoeda.format(Constantes.ZERO_DOUBLE));
				planoViewVO.setNovoValorMensalDependente(formataMoeda.format(Constantes.ZERO_DOUBLE));
			}
			else  if(!Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorAnualTitular()) && !Constantes.ZERO_DOUBLE.equals( planoVO.getNovoValorPlanoVO().getValorMensalTitular())){
				planoViewVO.setNovoTipoCobranca(TipoCobranca.MENSAL_E_ANUAL);
				
				planoViewVO.setNovoValorMensalTitular(formataMoeda.format(planoVO.getNovoValorPlanoVO().getValorMensalTitular()));
				planoViewVO.setNovoValorMensalDependente(formataMoeda.format(planoVO.getNovoValorPlanoVO().getValorMensalDependente()));
				planoViewVO.setNovoValorAnualTitular(formataMoeda.format(planoVO.getNovoValorPlanoVO().getValorAnualTitular()));
				planoViewVO.setNovoValorAnualDependente(formataMoeda.format(planoVO.getNovoValorPlanoVO().getValorAnualDependente()));
			}
		}
	    
	    planoViewVO.setValorDesconto(formataMoeda.format(planoVO.getValorPlanoVO().getValorDesconto()));
	    planoViewVO.setValorTaxa(formataMoeda.format(planoVO.getValorPlanoVO().getValorDesconto()));
	    
		
		return planoViewVO;
	}
	/**
	 * popula o objeto de PlanoVO com os valores da tela
	 * @param planoViewVO
	 * @return
	 */
	private PlanoVO popularObjetoPlanoVO(PlanoViewVO planoViewVO){
		
		PlanoVO planoVO = new PlanoVO();
		planoVO.setDataInicioVigencia(planoViewVO.getDataInicioVigencia());
		planoVO.setDataFimVigencia(planoViewVO.getDataFimVigencia());
		
		planoVO.setDataUltimaAtualizacao(new DateTime());
		
		planoVO.setListaDeCanais(new ArrayList<CanalVendaVO>());
		for(CanalVendaVO canal : planoViewVO.getListaDeCanais()){
			planoVO.getListaDeCanais().add(canal);
		}
		
		planoVO.setListaTipoSucursal(new ArrayList<Integer>());
		for(Integer segmento : planoViewVO.getListaTipoSucursal()){
			planoVO.getListaTipoSucursal().add(segmento);
		}
		
		planoVO.setValorPlanoVO(planoViewVO.getValorPlanoVO());
		planoVO.setNovoValorPlanoVO(planoViewVO.getNovoValorPlanoVO());
	    planoVO.setNome(planoViewVO.getNome());
	    planoVO.setCodigoRegistro(planoViewVO.getCodigoRegistro());
	    
	    planoVO.setCodigoResponsavel(getIdentificacaoUsuarioLogado());
	    
	    if(TipoCobranca.MENSAL.equals(planoViewVO.getTipoCobranca())){
	    	planoVO.setDescricaoCarenciaPeriodoAnual("");
			planoVO.setDescricaoCarenciaPeriodoMensal(planoViewVO.getDescricaoCarenciaPeriodoMensal());			
		} else if(TipoCobranca.ANUAL.equals(planoViewVO.getTipoCobranca())){
			planoVO.setDescricaoCarenciaPeriodoAnual(planoViewVO.getDescricaoCarenciaPeriodoAnual());
			planoVO.setDescricaoCarenciaPeriodoMensal("");			
		}else{
			planoVO.setDescricaoCarenciaPeriodoAnual(planoViewVO.getDescricaoCarenciaPeriodoAnual());
			planoVO.setDescricaoCarenciaPeriodoMensal(planoViewVO.getDescricaoCarenciaPeriodoMensal());	
			
		}
	    
		
		return planoVO;
		
	}
	
	
	/**
	 * popula o objeto de PlanoVO com os valores da tela, sem alterar o c�digo do plano
	 * @param planoViewVO
	 * @return
	 */
	private void alterarObjetoPlanoVO(PlanoViewVO planoViewVO, PlanoVO planoVO){
		
		
		planoVO.setDataInicioVigencia(planoViewVO.getDataInicioVigencia());
		planoVO.setDataFimVigencia(planoViewVO.getDataFimVigencia());
		
		planoVO.setExisteValorProgramado(planoViewVO.isExisteValorProgramado());
		planoVO.setValorProgramadoExcluido(planoViewVO.isValorProgramadoExcluido());
		
		planoVO.setListaDeCanais(new ArrayList<CanalVendaVO>());
		for(CanalVendaVO canal : planoViewVO.getListaDeCanais()){
			planoVO.getListaDeCanais().add(canal);
		}
		
		planoVO.setListaTipoSucursal(new ArrayList<Integer>());
		for(Integer segmento : planoViewVO.getListaTipoSucursal()){
			planoVO.getListaTipoSucursal().add(segmento);
		}
		
		planoVO.setValorPlanoVO(planoViewVO.getValorPlanoVO());
	    planoVO.setNome(planoViewVO.getNome());
	    planoVO.setCodigoRegistro(planoViewVO.getCodigoRegistro());
	    
	    planoVO.setCodigoResponsavel(getIdentificacaoUsuarioLogado());
	    
	    if(TipoCobranca.MENSAL.equals(planoViewVO.getTipoCobranca())){
	    	planoVO.setDescricaoCarenciaPeriodoAnual("");
			planoVO.setDescricaoCarenciaPeriodoMensal(planoViewVO.getDescricaoCarenciaPeriodoMensal());			
		} else if(TipoCobranca.ANUAL.equals(planoViewVO.getTipoCobranca())){
			planoVO.setDescricaoCarenciaPeriodoAnual(planoViewVO.getDescricaoCarenciaPeriodoAnual());
			planoVO.setDescricaoCarenciaPeriodoMensal("");			
		}else{
			planoVO.setDescricaoCarenciaPeriodoAnual(planoViewVO.getDescricaoCarenciaPeriodoAnual());
			planoVO.setDescricaoCarenciaPeriodoMensal(planoViewVO.getDescricaoCarenciaPeriodoMensal());			
			
		}
	    
	    if(planoViewVO.isExisteValorProgramado()){	    	
	    	planoVO.setDataProgramadaNovoValorPlano(planoViewVO.getDataProgramadaNovoValorPlano());
	    	
	    	planoVO.setNovoValorPlanoVO(planoViewVO.getNovoValorPlanoVO());
	    	
	    	if(TipoCobranca.MENSAL.equals(planoViewVO.getNovoTipoCobranca())){
		    	planoVO.setNovaDescricaoCarenciaPeriodoAnual("");
				planoVO.setNovaDescricaoCarenciaPeriodoMensal(planoViewVO.getNovaDescricaoCarenciaPeriodoMensal());	
				
				if(TipoCobranca.ANUAL.equals(planoViewVO.getTipoCobranca())){
					planoVO.setDescricaoCarenciaPeriodoMensal(planoViewVO.getNovaDescricaoCarenciaPeriodoMensal());
				}
			} 
	    	else if(TipoCobranca.ANUAL.equals(planoViewVO.getNovoTipoCobranca())){
				planoVO.setNovaDescricaoCarenciaPeriodoAnual(planoViewVO.getNovaDescricaoCarenciaPeriodoAnual());
				planoVO.setNovaDescricaoCarenciaPeriodoMensal("");
				
				if(TipoCobranca.MENSAL.equals(planoViewVO.getTipoCobranca())){
					planoVO.setDescricaoCarenciaPeriodoAnual(planoViewVO.getNovaDescricaoCarenciaPeriodoAnual());
				}
			}
	    	else{
				planoVO.setNovaDescricaoCarenciaPeriodoAnual(planoViewVO.getNovaDescricaoCarenciaPeriodoAnual());
				planoVO.setNovaDescricaoCarenciaPeriodoMensal(planoViewVO.getNovaDescricaoCarenciaPeriodoMensal());	
				
				if(!TipoCobranca.MENSAL_E_ANUAL.equals(planoViewVO.getTipoCobranca())){
					planoVO.setDescricaoCarenciaPeriodoAnual(planoViewVO.getNovaDescricaoCarenciaPeriodoAnual());
					planoVO.setDescricaoCarenciaPeriodoMensal(planoViewVO.getNovaDescricaoCarenciaPeriodoMensal());	
				}
			}
	    }
	}
	
	/**
	 * Preenche os canais marcados
	 */
	private void preencherCanais(){
		
		planoViewVO.setListaDeCanais(new ArrayList<CanalVendaVO>());
		planoVO.setListaDeCanais(new ArrayList<CanalVendaVO>());
		for(Integer codigoCanalVenda : listaDeCanaisSelecionados){
			CanalVendaVO canalVendaVO = canalServiceFacade.obterCanalDeVendaPorCodigo(codigoCanalVenda);
			planoViewVO.getListaDeCanais().add(canalVendaVO);
			planoVO.getListaDeCanais().add(canalVendaVO);
			
		}
		planoViewVO.setListaDeCanaisSelecionados(new ArrayList<CanalVendaVO>());
		planoViewVO.getListaDeCanaisSelecionados().addAll(planoViewVO.getListaDeCanais());
	}
	
	/**
	 * Preenche o objeto de valorPlanoVO com os valores da tela
	 * @return
	 */
	private ValorPlanoVO preencherValorPlano(){
		
		ValorPlanoVO valorPlanoVO = new ValorPlanoVO();
		try{
			
			if(TipoCobranca.MENSAL.equals(planoViewVO.getTipoCobranca())){
				
				valorPlanoVO.setValorAnualTitular(0.0);
				valorPlanoVO.setValorAnualDependente(0.0);
				valorPlanoVO.setValorMensalTitular(planoViewVO.getValorPlanoVO().getValorMensalTitular());
				valorPlanoVO.setValorMensalDependente(planoViewVO.getValorPlanoVO().getValorMensalDependente());
			} else if(TipoCobranca.ANUAL.equals(planoViewVO.getTipoCobranca())){
				
				valorPlanoVO.setValorAnualTitular(planoViewVO.getValorPlanoVO().getValorAnualTitular());
				valorPlanoVO.setValorAnualDependente(planoViewVO.getValorPlanoVO().getValorAnualDependente());
				valorPlanoVO.setValorMensalTitular(0.0);
				valorPlanoVO.setValorMensalDependente(0.0);
			}else{
				
				valorPlanoVO.setValorMensalTitular(planoViewVO.getValorPlanoVO().getValorMensalTitular());
				valorPlanoVO.setValorMensalDependente(planoViewVO.getValorPlanoVO().getValorMensalDependente());
				valorPlanoVO.setValorAnualTitular(planoViewVO.getValorPlanoVO().getValorAnualTitular());
				valorPlanoVO.setValorAnualDependente(planoViewVO.getValorPlanoVO().getValorAnualDependente());
			}
			
			if(planoViewVO.getValorPlanoVO().getValorTaxa() == null){
				valorPlanoVO.setValorTaxa(0.0);
			}else{
				valorPlanoVO.setValorTaxa(planoViewVO.getValorPlanoVO().getValorTaxa());
			}
			
			if(planoViewVO.getValorPlanoVO().getValorDesconto() == null){
				valorPlanoVO.setValorDesconto(0.0);
			}else{
				valorPlanoVO.setValorDesconto(planoViewVO.getValorPlanoVO().getValorDesconto());
			}
			
			
			
			
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO CONVERTER VALORES.");
		
		}
		
		return valorPlanoVO;
	}
	
	/**
	 * Preenche o objeto de valorPlanoVO com os valores da tela
	 * @return
	 */
	private ValorPlanoVO preencherNovoValorPlano(){
		
		ValorPlanoVO novoValorPlanoVO = new ValorPlanoVO();
		try{
			
			if(TipoCobranca.MENSAL.equals(planoViewVO.getNovoTipoCobranca())){
				
				novoValorPlanoVO.setValorAnualTitular(0.0);
				novoValorPlanoVO.setValorAnualDependente(0.0);
				novoValorPlanoVO.setValorMensalTitular(planoViewVO.getNovoValorPlanoVO().getValorMensalTitular());
				novoValorPlanoVO.setValorMensalDependente(planoViewVO.getNovoValorPlanoVO().getValorMensalDependente());
				
			} else if(TipoCobranca.ANUAL.equals(planoViewVO.getNovoTipoCobranca())){
				
				novoValorPlanoVO.setValorAnualTitular(planoViewVO.getNovoValorPlanoVO().getValorAnualTitular());
				novoValorPlanoVO.setValorAnualDependente(planoViewVO.getNovoValorPlanoVO().getValorAnualDependente());
				novoValorPlanoVO.setValorMensalTitular(0.0);
				novoValorPlanoVO.setValorMensalDependente(0.0);
			}else{
				
				novoValorPlanoVO.setValorMensalTitular(planoViewVO.getNovoValorPlanoVO().getValorMensalTitular());
				novoValorPlanoVO.setValorMensalDependente(planoViewVO.getNovoValorPlanoVO().getValorMensalDependente());
				novoValorPlanoVO.setValorAnualTitular(planoViewVO.getNovoValorPlanoVO().getValorAnualTitular());
				novoValorPlanoVO.setValorAnualDependente(planoViewVO.getNovoValorPlanoVO().getValorAnualDependente());
			}
			
			if(planoViewVO.getValorPlanoVO().getValorTaxa() == null){
				novoValorPlanoVO.setValorTaxa(0.0);
			}else{
				novoValorPlanoVO.setValorTaxa(planoViewVO.getValorPlanoVO().getValorTaxa());
			}
			
			if(planoViewVO.getValorPlanoVO().getValorDesconto() == null){
				novoValorPlanoVO.setValorDesconto(0.0);
			}else{
				novoValorPlanoVO.setValorDesconto(planoViewVO.getValorPlanoVO().getValorDesconto());
			}
			
			
			
			
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO CONVERTER VALORES.");
		
		}
		
		return novoValorPlanoVO;
	}
	
	
	
	
	public String verificarSePodeIncluir(){
		
		planoViewVO.getCodigo();
		
		return SUCCESS;
	}
	
	
	public PlanoViewVO getPlanoViewVO() {
		return planoViewVO;
	}


	public void setPlanoViewVO(PlanoViewVO planoViewVO) {
		this.planoViewVO = planoViewVO;
	}


	public FiltroPlanoVO getFiltro() {
		return filtro;
	}


	public void setFiltro(FiltroPlanoVO filtro) {
		this.filtro = filtro;
	}


	/**
	 * @return the listaDePlanos
	 */
	public List<PlanoVO> getListaDePlanos() {
		return listaDePlanos;
	}


	/**
	 * @param listaDePlanos the listaDePlanos to set
	 */
	public void setListaDePlanos(List<PlanoVO> listaDePlanos) {
		this.listaDePlanos = listaDePlanos;
	}


	/**
	 * @return the listaDeCanaisSelecionados
	 */
	public List<Integer> getListaDeCanaisSelecionados() {
		return listaDeCanaisSelecionados;
	}


	/**
	 * @param listaDeCanaisSelecionados the listaDeCanaisSelecionados to set
	 */
	public void setListaDeCanaisSelecionados(
			List<Integer> listaDeCanaisSelecionados) {
		this.listaDeCanaisSelecionados = listaDeCanaisSelecionados;
	}


	/**
	 * @return the listaDeCanais
	 */
	public List<CanalVendaVO> getListaDeCanais() {
		return listaDeCanais;
	}


	/**
	 * @param listaDeCanais the listaDeCanais to set
	 */
	public void setListaDeCanais(List<CanalVendaVO> listaDeCanais) {
		this.listaDeCanais = listaDeCanais;
	}

	/**
	 * @return the planoVO
	 */
	public PlanoVO getPlanoVO() {
		return planoVO;
	}

	/**
	 * @param planoVO the planoVO to set
	 */
	public void setPlanoVO(PlanoVO planoVO) {
		this.planoVO = planoVO;
	}
	/**
	 * @return the listaDeStatus
	 */
	public List<LabelValueVO> getListaDeStatus() {
		return listaDeStatus;
	}
	/**
	 * @param listaDeStatus the listaDeStatus to set
	 */
	public void setListaDeStatus(List<LabelValueVO> listaDeStatus) {
		this.listaDeStatus = listaDeStatus;
	}
	/**
	 * @return the listaDeTipoCobranca
	 */
	public List<LabelValueVO> getListaDeTipoCobranca() {
		return listaDeTipoCobranca;
	}
	/**
	 * @param listaDeTipoCobranca the listaDeTipoCobranca to set
	 */
	public void setListaDeTipoCobranca(List<LabelValueVO> listaDeTipoCobranca) {
		this.listaDeTipoCobranca = listaDeTipoCobranca;
	}
	/**
	 * @return the listaDeCanaisSelecionadosParaAlteracao
	 */
	public List<Integer> getListaDeCanaisSelecionadosParaAlteracao() {
		return listaDeCanaisSelecionadosParaAlteracao;
	}
	/**
	 * @param listaDeCanaisSelecionadosParaAlteracao the listaDeCanaisSelecionadosParaAlteracao to set
	 */
	public void setListaDeCanaisSelecionadosParaAlteracao(
			List<Integer> listaDeCanaisSelecionadosParaAlteracao) {
		this.listaDeCanaisSelecionadosParaAlteracao = listaDeCanaisSelecionadosParaAlteracao;
	}
	public String getValorMes() {
		return valorMes;
	}
	public void setValorMes(String valorMes) {
		this.valorMes = valorMes;
	}
	public String getValorAnual() {
		return valorAnual;
	}
	public void setValorAnual(String valorAnual) {
		this.valorAnual = valorAnual;
	}
	/**
	 * Retorna listaTipoSucursal.
	 *
	 * @return listaTipoSucursal - listaTipoSucursal.
	 */
	public List<TipoSucursal> getListaTipoSucursal() {
		return listaTipoSucursal;
	}
	/**
	 * Especifica listaTipoSucursal.
	 *
	 * @param listaTipoSucursal - listaTipoSucursal.
	 */
	public void setListaTipoSucursal(List<TipoSucursal> listaTipoSucursal) {
		this.listaTipoSucursal = listaTipoSucursal;
	}
	/**
	 * Retorna listaTipoSucursalSelecionado.
	 *
	 * @return listaTipoSucursalSelecionado - listaTipoSucursalSelecionado.
	 */
	public List<Integer> getListaTipoSucursalSelecionado() {
		return listaTipoSucursalSelecionado;
	}
	/**
	 * Especifica listaTipoSucursalSelecionado.
	 *
	 * @param listaTipoSucursalSelecionado - listaTipoSucursalSelecionado.
	 */
	public void setListaTipoSucursalSelecionado(List<Integer> listaTipoSucursalSelecionado) {
		this.listaTipoSucursalSelecionado = listaTipoSucursalSelecionado;
	}
	/**
	 * Retorna segmentos.
	 *
	 * @return segmentos - segmentos.
	 */
	public List<TipoSucursal> getSegmentos() {
		return segmentos;
	}
	/**
	 * Especifica segmentos.
	 *
	 * @param segmentos - segmentos.
	 */
	public void setSegmentos(List<TipoSucursal> segmentos) {
		this.segmentos = segmentos;
	}
	
}
