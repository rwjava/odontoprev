package br.com.bradseg.eedi.administrativo.plano.action;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.eedi.administrativo.plano.facade.HistoricoServiceFacade;
import br.com.bradseg.eedi.administrativo.support.action.BaseAction;
import br.com.bradseg.eedi.administrativo.vo.FiltroHistoricoVO;
import br.com.bradseg.eedi.administrativo.vo.HistoricoCanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.HistoricoCanalVendaViewVO;
import br.com.bradseg.eedi.administrativo.vo.HistoricoPlanoViewVO;
import br.com.bradseg.eedi.administrativo.vo.PlanoVO;



@Controller
@Scope("request")
public class HistoricoAction extends BaseAction {

	
	private static final long serialVersionUID = 50635548150475923L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HistoricoAction.class);  
	 
	private HistoricoCanalVendaVO historicoCanalVenda = new HistoricoCanalVendaVO();
	
	private HistoricoCanalVendaViewVO historicoCanalVendaView = new HistoricoCanalVendaViewVO();
	
	private HistoricoPlanoViewVO historicoPlanoViewVO = new HistoricoPlanoViewVO();
	
	private List<String>responsaveisAlteracao = new ArrayList<String>();
	
	private FiltroHistoricoVO filtro = new FiltroHistoricoVO();
	
	
	
	private PlanoVO planoVO = new PlanoVO();
	
	
	@Autowired
	private HistoricoServiceFacade historicoServiceFacade;
	
	
	
	public String iniciarHistorico (){		
		return  SUCCESS;
		
	}
	
	public String iniciarHistoricoPlano(){
		
		try{
			historicoPlanoViewVO.setListaHistoricoPlanoVO(historicoServiceFacade.listarHistoricoPlano(planoVO.getCodigo()));		
		}catch(Exception e){
			LOGGER.error("Erro ao listar os planos.", e);
			erro(e);	
		}
		
		return SUCCESS;		
	}
	
	
	public String iniciarHistoricoPorCodigo(){
		
		try{			
			historicoPlanoViewVO.setListaHistoricoPlanoVO(historicoServiceFacade.listarHistoricoPlano(planoVO.getCodigo()));			
		}catch(Exception e){
			LOGGER.error("Erro ao listar os planos.", e);
			erro(e);	
		}
		
		return SUCCESS;		
	}
	
	
	public String listarHistorico(){
		
		try{			
			historicoPlanoViewVO.setListaUsuarios(historicoServiceFacade.listarUsuarios());				
		} catch (Exception e) {
			LOGGER.error("Erro ao listar os planos.", e);
			erro(e);
		}		
		return SUCCESS;
	}
	
	public String listarHistoricoPlano(){
		
		try{				
			//historicoPlanoViewVO.setListaHistoricoPlano(historicoServiceFacade.listarHistoricoPlano());				
		} catch (Exception e) {
			LOGGER.error("Erro ao listar os planos.", e);
			erro(e);
		}		
			return SUCCESS;
	}
	
	public String listarPorFiltro() {		
		
		try {			
			if (filtro.isFiltroPreenchido()) {
				historicoPlanoViewVO.setListaHistoricoPlanoVO(historicoServiceFacade.consultarHistorico(filtro));				
			}else{
				historicoPlanoViewVO.setListaHistoricoPlanoVO(historicoServiceFacade.listarHistoricoPlano());				
			}			
		} catch (Exception e) {
			LOGGER.error("Erro ao listar os planos.", e);
			erro(e);
		}		

		filtro.setDataInicioPeriodo(null);
		filtro.setDataFimPeriodo(null);		
		historicoPlanoViewVO.setListaUsuarios(historicoServiceFacade.listarUsuarios());			
		
		return SUCCESS;
	}

	public HistoricoCanalVendaVO getHistoricoCanalVenda() {
		return historicoCanalVenda;
	}

	public void setHistoricoCanalVenda(HistoricoCanalVendaVO historicoCanalVenda) {
		this.historicoCanalVenda = historicoCanalVenda;
	}

	public HistoricoCanalVendaViewVO getHistoricoCanalVendaView() {
		return historicoCanalVendaView;
	}
	
	public void setHistoricoCanalVendaView(
			HistoricoCanalVendaViewVO historicoCanalVendaView) {
		this.historicoCanalVendaView = historicoCanalVendaView;
	}
	
	public HistoricoPlanoViewVO getHistoricoPlanoViewVO() {
		return historicoPlanoViewVO;
	}
	
	public void setHistoricoPlanoViewVO(HistoricoPlanoViewVO historicoPlanoViewVO) {
		this.historicoPlanoViewVO = historicoPlanoViewVO;
	}

	public List<String> getResponsaveisAlteracao() {
		return responsaveisAlteracao;
	}

	public void setResponsaveisAlteracao(List<String> responsaveisAlteracao) {
		this.responsaveisAlteracao = responsaveisAlteracao;
	}

	
	public PlanoVO getPlanoVO() {
		return planoVO;
	}

	public void setPlanoVO(PlanoVO planoVO) {
		this.planoVO = planoVO;
	}

	public FiltroHistoricoVO getFiltro() {
		return filtro;
	}

	public void setFiltro(FiltroHistoricoVO filtro) {
		this.filtro = filtro;
	}	

}
