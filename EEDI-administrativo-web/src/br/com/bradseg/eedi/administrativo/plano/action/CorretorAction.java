package br.com.bradseg.eedi.administrativo.plano.action;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.eedi.administrativo.plano.facade.CorretorServiceFacade;
import br.com.bradseg.eedi.administrativo.support.Constantes;
import br.com.bradseg.eedi.administrativo.support.action.BaseAction;
import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorVO;
import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorViewVO;
import br.com.bradseg.eedi.administrativo.vo.LabelValueVO;
import br.com.bradseg.eedi.administrativo.vo.Status;

/**
 * Action respons�vel por disponibilizar os metodos referentes as a��es de tela da funcionalidade corretor.
 * 
 * @author EBIX
 */
@Controller
@Scope("request")
public class CorretorAction extends BaseAction {

	private static final long serialVersionUID = -1602655047427034963L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CorretorAction.class);
	
	private CadastroCorretorVO cadastroCorretorVO = new CadastroCorretorVO();
	
	private CadastroCorretorViewVO cadastroCorretorViewVO = new CadastroCorretorViewVO();
	
	@Autowired
	private CorretorServiceFacade corretorServiceFacade;
	
	private List<CadastroCorretorVO> listarCorretores;
	
	private List<LabelValueVO> listaDeStatus;
	

	/**
	 * 
	 * @return SUCCESS
	 */
	public String listarPorFiltroCorretor(){
		try {
			//obt�m todos os status
			listaDeStatus = Status.obterLista();
			listarCorretores = corretorServiceFacade.listarCorretores();
			
			if(null != cadastroCorretorViewVO.getSucursal()){
				if(cadastroCorretorViewVO.getStatus() != null &&  cadastroCorretorViewVO.getStatus() != 0) {
					listarCorretores = corretorServiceFacade.listarCorretorPorSucursalEStatus(cadastroCorretorViewVO.getSucursal(), cadastroCorretorViewVO.getStatus());
				} else {
					listarCorretores = corretorServiceFacade.listarCorretoresPorSucursal(cadastroCorretorViewVO.getSucursal());
				}
			}
			verificarPodeAlterarCorretor(listarCorretores);
		} catch (Exception e) {
			LOGGER.error("Erro ao listar o(s) corretor(es).", e);
			erro(e);
		}
		return SUCCESS;
	}
	
	
	/**
	 * Alterar nome do m�todo depois
	 * @param listarCorretores
	 */
	private void verificarPodeAlterarCorretor(List<CadastroCorretorVO> listarCorretores){
		for (CadastroCorretorVO cadastroCorretorVO : listarCorretores) {
			if(cadastroCorretorVO.getDataFimContrato().toLocalDate().isBefore(new LocalDate())){
				cadastroCorretorVO.setPodeAlterarCorretor(Constantes.NAO_ALTERA);				
			} else {
				cadastroCorretorVO.setPodeAlterarCorretor(Constantes.ALTERA);
			}
		}		
	}
	
	
	/**
	 * M�todo respons�vel por fazer a chamada da tela de visualiza��o e de valida��o do campo de sucursal
	 * @return SUCCESS
	 */
	public String iniciarConsultarSucursal() {
		corretorServiceFacade.validarPreenchimentoCampoDeConsulta(cadastroCorretorViewVO);
		return SUCCESS;
	}
	
	/**
	 * M�todo respons�vel por iniciar a tela para inclus�o de cadastro de um corretor na DB 
	 * @return SUCCESS
	 */
	public String iniciarIncluirCorretor(){
		return SUCCESS;
	}
	
	/**
	 * M�todo respons�vel por incluir os dados de um corretor na DB 
	 * @return SUCCESS
	 */
	public String incluirCorretor() {
		//obt�m todos os status
		listaDeStatus = Status.obterLista();
		
		corretorServiceFacade.validarPreenchimentoCamposCadastroCorretor(cadastroCorretorViewVO);
		
		cadastroCorretorVO.setCnpj(cadastroCorretorViewVO.getCnpj());
		cadastroCorretorVO.setSucursal(cadastroCorretorViewVO.getSucursal());
		cadastroCorretorVO.setNome(cadastroCorretorViewVO.getNome());
		cadastroCorretorVO.setDataInicioContrato(new DateTime(cadastroCorretorViewVO.getDataInicioContratoTela().getTime()));
		cadastroCorretorVO.setDataFimContrato(new DateTime(cadastroCorretorViewVO.getDataFimContratoTela().getTime()));
		
		if(cadastroCorretorVO.getDataInicioContrato() != null && cadastroCorretorVO.getDataFimContrato() != null){
			LocalDate dataAtual = new LocalDate();
			
			if (dataAtual.isBefore(cadastroCorretorVO.getDataInicioContrato().toLocalDate())) {
				cadastroCorretorVO.setStatus(Status.PROGRAMADO.getCodigo());
			}else if (dataAtual.isAfter(cadastroCorretorVO.getDataFimContrato().toLocalDate())) {
				cadastroCorretorVO.setStatus(Status.EXPIRADO.getCodigo());
			}else if((dataAtual.isAfter(cadastroCorretorVO.getDataInicioContrato().toLocalDate())  || dataAtual.isEqual(cadastroCorretorVO.getDataInicioContrato().toLocalDate())) && (dataAtual.isBefore(cadastroCorretorVO.getDataFimContrato().toLocalDate()) || dataAtual.isEqual(cadastroCorretorVO.getDataFimContrato().toLocalDate()))){
				cadastroCorretorVO.setStatus(Status.VIGENTE.getCodigo());
			}
		}
		
		corretorServiceFacade.validarCnpjEDataVigenciaContrato(cadastroCorretorVO);
		corretorServiceFacade.incluirAcessoCorretor(cadastroCorretorVO);
		sucesso("msg.sucesso.cadastro", "Corretor");
		
		return SUCCESS;
	}
	
	/**
	 * M�todo respons�vel por chamar a tela (visualizar) de visualiza��o dos corretores cadastrados
	 * @return SUCCESS
	 */
	public String visualizar() {		
		cadastroCorretorVO = corretorServiceFacade.obterDadosCorretorPorCnpj(cadastroCorretorVO.getCnpj());
		cadastroCorretorViewVO = popularObjetoVisualizarCorretorView(cadastroCorretorVO);
		
		return SUCCESS;
	}
	
	 
	/**
	 * M�todo respons�vel por iniciar a tela para altera��o dos dados do Corretor cadastrado
	 * @return SUCCESS
	 */
	public String iniciarAlterar() {
		//obt�m todos os status
		listaDeStatus = Status.obterLista();
		
		cadastroCorretorVO = corretorServiceFacade.obterDadosCorretorPorCnpj(cadastroCorretorVO.getCnpj()); 
		cadastroCorretorViewVO = popularObjetoCadastroCorretorView(cadastroCorretorVO);	
		
		return SUCCESS;
	}
	
	/**
	 * M�todo respons�vel pelas a��es na tela de alterar dados Corretor cadastrado
	 * @return SUCCESS
	 */
	public String alterarCorretor() {
		//obt�m todos os status
		listaDeStatus = Status.obterLista();
		
		cadastroCorretorVO.setCnpj(cadastroCorretorViewVO.getCnpj());
		
		CadastroCorretorVO cadastroCorretorAnterior = new CadastroCorretorVO();
		cadastroCorretorAnterior = corretorServiceFacade.obterDadosCorretorPorCnpj(cadastroCorretorVO.getCnpj());
		
		corretorServiceFacade.validarPreenchimentoCamposCadastroCorretor(cadastroCorretorViewVO);
				
		cadastroCorretorVO.setDataFimContrato(new DateTime(cadastroCorretorViewVO.getDataFimContratoTela().getTime()));
		cadastroCorretorVO.setStatus(cadastroCorretorViewVO.getStatus());
		
		if(cadastroCorretorVO.getStatus().equals(Status.EXPIRADO.getCodigo())) {
			cadastroCorretorVO.setDataFimContrato(new DateTime(cadastroCorretorViewVO.getDataInicioContratoTela().getTime()));
		}
		
		corretorServiceFacade.verificarSeHouveAlteracaoDoCorretor(cadastroCorretorAnterior, cadastroCorretorVO);
				
		corretorServiceFacade.atualizarVigencia(cadastroCorretorVO);
		
		sucesso("msg.sucesso.alteracao", "Corretor");
		
		return SUCCESS;
	}
	
	
	/**
	 * 
	 * @param cadastroCorretorVO
	 * @return
	 */
	private CadastroCorretorViewVO popularObjetoVisualizarCorretorView(CadastroCorretorVO cadastroCorretorVO) {
		CadastroCorretorViewVO cadastroCorretorViewVO = new CadastroCorretorViewVO();
		
		cadastroCorretorViewVO.setCnpj(cadastroCorretorVO.getCnpj());
		cadastroCorretorViewVO.setSucursal(cadastroCorretorVO.getSucursal());
		cadastroCorretorViewVO.setNome(cadastroCorretorVO.getNome());
		//cadastroCorretorViewVO.setDataFimContrato(cadastroCorretorVO.getDataFimContrato());
		cadastroCorretorViewVO.setDataInicioContratoTela(new Date(cadastroCorretorVO.getDataInicioContrato().getMillis()));
		cadastroCorretorViewVO.setDataFimContratoTela(new Date(cadastroCorretorVO.getDataFimContrato().getMillis()));
		cadastroCorretorViewVO.setStatus(cadastroCorretorVO.getStatus());
		cadastroCorretorViewVO.setDescricaoStatus(cadastroCorretorVO.getDescricaoStatus());
		
		return cadastroCorretorViewVO;
	}
	
	
	/**
	 * 
	 * @param cadastroCorretorVO
	 * @return
	 */
	private CadastroCorretorViewVO popularObjetoCadastroCorretorView(CadastroCorretorVO cadastroCorretorVO) {
		CadastroCorretorViewVO cadastroCorretorViewVO = new CadastroCorretorViewVO();
		
		cadastroCorretorViewVO.setCnpj(cadastroCorretorVO.getCnpj());
		cadastroCorretorViewVO.setSucursal(cadastroCorretorVO.getSucursal());
		cadastroCorretorViewVO.setNome(cadastroCorretorVO.getNome());
		//cadastroCorretorViewVO.setDataInicioContratoTela(new Date(cadastroCorretorVO.getDataInicioContrato().getMillis()));
		cadastroCorretorViewVO.setDataInicioContratoTela(new Date(cadastroCorretorVO.getDataInicioContrato().getMillis()));
		cadastroCorretorViewVO.setDataFimContratoTela(new Date(cadastroCorretorVO.getDataFimContrato().getMillis()));
		cadastroCorretorViewVO.setStatus(cadastroCorretorVO.getStatus());
		
		return cadastroCorretorViewVO;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public CadastroCorretorVO getCadastroCorretorVO() {
		return cadastroCorretorVO;
	}
	/**
	 * 
	 * @param cadastroCorretorVO
	 */
	public void setCadastroCorretorVO(CadastroCorretorVO cadastroCorretorVO) {
		this.cadastroCorretorVO = cadastroCorretorVO;
	}

	
	/**
	 * 
	 * @return
	 */
	public CadastroCorretorViewVO getCadastroCorretorViewVO() {
		return cadastroCorretorViewVO;
	}
	/**
	 * 
	 * @param cadastroCorretorViewVO
	 */
	public void setCadastroCorretorViewVO(
			CadastroCorretorViewVO cadastroCorretorViewVO) {
		this.cadastroCorretorViewVO = cadastroCorretorViewVO;
	}
	
	/**
	 * @return
	 */
	public List<CadastroCorretorVO> getListarCorretores() {
		return listarCorretores;
	}
	/**
	 * @param listarCorretores
	 */
	public void setListarCorretores(List<CadastroCorretorVO> listarCorretores) {
		this.listarCorretores = listarCorretores;
	}
	
	
	/**
	 * @return the listaDeStatus
	 */
	public List<LabelValueVO> getListaDeStatus() {
		return listaDeStatus;
	}
	/**
	 * @param listaDeStatus the listaDeStatus to set
	 */
	public void setListaDeStatus(List<LabelValueVO> listaDeStatus) {
		this.listaDeStatus = listaDeStatus;
	}
	
}
