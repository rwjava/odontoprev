package br.com.bradseg.eedi.administrativo.plano.facade;

import java.util.List;

import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaViewVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroCanalVendaVO;

public interface CanalVendaServiceFacade {

	/**
	 * Lista Todos os canais de venda
	 * @return List - lista de {@link CanalVendaVO}
	 */
	public List<CanalVendaVO> listarTodosCanaisVenda();
	
	public List<CanalVendaVO> obterCanaisPorCodigoPlano(Long codigoPlano);
	
	/**
	 * Consulta canal de venda por filtro informado
	 * @param filtro - inst�ncia de {@link FiltroCanalVendaVO}
	 * @return List - Lista de {@link CanalVendaVO}
	 */
	public List<CanalVendaVO>consultarCanalVenda(FiltroCanalVendaVO filtro);
	
	/**
	 * Inclui canal de venda
	 * @param canalVendaVO - Inst�ncia de {@link CanalVendaVO}
	 */
	public void incluirCanalVenda(CanalVendaVO canalVendaVO);
	
	/**
	 * Verifica na base exist�ncia de canal de venda com sigla informada
	 * @param sigla - {@link String}
	 * @return {@link Boolean} - true se existe na base, false se n�o existe na base
	 */
    public boolean verificarExistenciaCanalVendaPorSigla(String  sigla);

    /**
	 * Verifica na base exist�ncia de canal de venda com nome informado
	 * @param nomeCanal - {@link String}
	 * @return {@link Boolean} - true se existe na base, false se n�o existe na base
	 */
    public boolean verificarExistenciaCanalVendaPorNome(String nomeCanal);

    /**
	 * Verifica na base exist�ncia de canal de venda com codigoCanal informado
	 * @param codigoCanal - {@link String}
	 * @return {@link Boolean} - true se existe na base, false se n�o existe na base
	 */
	public boolean verificarExistenciaCanalVendaPorCodigo(Integer codigoCanal);
	
	
	/**
	 * Obt�m canal de venda por c�digo informado.
	 * @param codigo - {@link Integer} - C�digo do canal de venda procurado
	 * @return {@link CanalVendaVO} - Inst�ncia de CanalVendaVO
	 */
	public CanalVendaVO obterCanalDeVendaPorCodigo(Integer codigo);

	/**
	 * Valida o preenchimento da sigla, do nome e do c�digo do canal de venda
	 * @param canalVendaVO - Inst�ncia de {@link CanalVendaVO}
	 */
	public void validarPreenchimentoDosCampos(CanalVendaViewVO canalVendaVO);

	/**
	 * Verifica na base se os campos informados na altera��o
	 * j� existem na base, levando em considera��o os valores anteriores e os valores alterados
	 * @param canalVendaVO - Inst�ncia de {@link CanalVendaVO} com os valores antes da altera��o
	 * @param canalVendaViewVO - Inst�ncia de {@link CanalVendaVO} com os valores ap�s altera��o
	 *
	 */
	public void validarExistenciaCampos(CanalVendaVO canalVendaVO,
			CanalVendaViewVO canalVendaViewVO);

	/**
	 * Altera um canal de venda
	 * @param CanalVendaVO - Inst�ncia de {@link CanalVendaVO}
	 */
	public void alterarCanal(CanalVendaVO canalVendaVO);

}
