package br.com.bradseg.eedi.administrativo.plano.dao;

import java.util.List;

import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroCanalVendaVO;

public interface CanalVendaDao {

	/**
	 * Lista Todos os canais de venda
	 * @return List - lista de {@link CanalVendaVO}
	 */
	public List<CanalVendaVO> listarTodosCanaisVenda();
	
	/**
	 * Consulta canal de venda por filtro informado
	 * @param filtro - inst�ncia de {@link FiltroCanalVendaVO}
	 * @return List - Lista de {@link CanalVendaVO}
	 */
	public List<CanalVendaVO> consultarCanalVenda(FiltroCanalVendaVO filtro);

	/**
	 * Verifica na base exist�ncia de canal de venda com sigla informada
	 * @param sigla - {@link String}
	 * @return {@link Boolean} - true se existe na base, false se n�o existe na base
	 */
	public boolean verificarExistenciaSiglaCanalVenda(String  sigla);

	/**
	 * Verifica na base exist�ncia de canal de venda com nome informado
	 * @param nomeCanal - {@link String}
	 * @return {@link Boolean} - true se existe na base, false se n�o existe na base
	 */
	public boolean verificarExistenciaNomeCanalVenda(String nomeCanal);

	/**
	 * Verifica na base exist�ncia de canal de venda com codigoCanal informado
	 * @param codigoCanal - {@link String}
	 * @return {@link Boolean} - true se existe na base, false se n�o existe na base
	 */
	public boolean verificarExistenciaCodigoCanalVendaInclusao(Integer codigoCanal);

	/**
	 * Inclui canal de venda
	 * @param canalVendaVO - Inst�ncia de {@link CanalVendaVO}
	 */
	public void incluirCanalVenda(CanalVendaVO canalVendaVO);
	
	/**
	 * Obt�m canal de venda por c�digo informado.
	 * @param codigo - {@link Integer} - C�digo do canal de venda procurado
	 * @return {@link CanalVendaVO} - Inst�ncia de CanalVendaVO
	 */
	public CanalVendaVO obterCanalDeVendaPorCodigo(Integer codigo);

	/**
	 * Altera um canal de venda
	 * @param CanalVendaVO - Inst�ncia de {@link canalVendaVO}
	 */
	public void alterarCanal(CanalVendaVO canalVendaVO);

	public List<CanalVendaVO> obterCanaisPorCodigoPlano(Long codigoPlano);

}
