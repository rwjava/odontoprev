package br.com.bradseg.eedi.administrativo.plano.dao;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.administrativo.rowmapper.CanalVendaRowMapper;
import br.com.bradseg.eedi.administrativo.support.Strings;
import br.com.bradseg.eedi.administrativo.vo.CanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.FiltroCanalVendaVO;


@Repository
public class CanalVendaDaoImpl  extends JdbcDao implements CanalVendaDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(CanalVendaDaoImpl.class);
	
	@Autowired
	private DataSource dataSource;
	
	private static final String OBTER_CANAIS_VENDA = new StringBuilder()
	.append(" SELECT CCANAL_VDA_DNTAL_INDVD, RCANAL_VDA_DNTAL_INDVD, CSGL_CANAL_VDA_DNTAL_INDVD         \n")
	.append(" from DBPROD.CANAL_VDA_DNTAL_INDVD ORDER BY RCANAL_VDA_DNTAL_INDVD                         \n").toString();

	
	private static final String VERIFICAR_EXISTENCIA_SIGLA = new StringBuilder()
	.append(" SELECT COALESCE(COUNT(*),0)															\n")
	.append(" FROM  DBPROD.CANAL_VDA_DNTAL_INDVD  								        \n")
	.append(" WHERE CSGL_CANAL_VDA_DNTAL_INDVD = :sigla 										\n").toString();
	
	private static final String VERIFICAR_EXISTENCIA_NOME = new StringBuilder()
	.append(" SELECT COALESCE(COUNT(*),0)														    \n")
	.append(" FROM  DBPROD.CANAL_VDA_DNTAL_INDVD 								        \n")
	.append(" WHERE RCANAL_VDA_DNTAL_INDVD = :nome 												\n").toString();
	
	private static final String VERIFICAR_EXISTENCIA_CODIGO = new StringBuilder()
	.append(" SELECT COALESCE(COUNT(*),0)														    \n")
	.append(" FROM  DBPROD.CANAL_VDA_DNTAL_INDVD								        \n")
	.append(" WHERE CCANAL_VDA_DNTAL_INDVD = :codigo 											\n").toString();
	
	private static final String INSERT_CANAL_VENDA = new StringBuilder()
	.append(" INSERT INTO DBPROD.CANAL_VDA_DNTAL_INDVD (CCANAL_VDA_DNTAL_INDVD, CSGL_CANAL_VDA_DNTAL_INDVD , RCANAL_VDA_DNTAL_INDVD) \n")
	.append(" VALUES(:codigoCanal, :siglaCanal , :nomeCanal)                                                                         \n").toString();
	
	private static final String OBTER_CANAL_VENDA = new StringBuilder()
	.append(" SELECT CCANAL_VDA_DNTAL_INDVD, RCANAL_VDA_DNTAL_INDVD, CSGL_CANAL_VDA_DNTAL_INDVD \n")
	.append(" FROM DBPROD.CANAL_VDA_DNTAL_INDVD 								        \n")
	.append(" WHERE CCANAL_VDA_DNTAL_INDVD = :codigo							                \n").toString();
	
	private static final String ATUALIZAR_CANAL_VENDA = new StringBuilder()
	.append(" UPDATE DBPROD.CANAL_VDA_DNTAL_INDVD SET RCANAL_VDA_DNTAL_INDVD = :nomeCanal ,        \n")
	.append(" CSGL_CANAL_VDA_DNTAL_INDVD = :siglaCanal, DULT_ATULZ_REG = CURRENT TIMESTAMP WHERE CCANAL_VDA_DNTAL_INDVD = :codigoCanal         \n").toString();
	

	private static final String OBTER_CANAIS_POR_CODIGO_PLANO = new StringBuilder()
		.append(" SELECT CANAL.CCANAL_VDA_DNTAL_INDVD, CANAL.RCANAL_VDA_DNTAL_INDVD, CANAL.CSGL_CANAL_VDA_DNTAL_INDVD                   \n")
		.append(" FROM DBPROD.CANAL_VDA_DNTAL_INDVD CANAL, DBPROD.PLANO_DNTAL_INDVD PLANO, DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CVDA  \n")
		.append(" WHERE PLANO.CPLANO_DNTAL_INDVD = :codigoPlano                                                                         \n")
//		.append(" AND DATE(PLANO.DFIM_VGCIA) >=  DATE(CURRENT_TIMESTAMP)                                                                \n")
		.append(" AND CVDA.DFIM_VGCIA IS NULL                                                                                           \n")
		.append(" AND CVDA.CPLANO_DNTAL_INDVD = PLANO.CPLANO_DNTAL_INDVD                                                                \n")
		.append(" AND CANAL.CCANAL_VDA_DNTAL_INDVD = CVDA.CCANAL_VDA_DNTAL_INDVD                                                        \n") 
		.append(" ORDER BY      CANAL.RCANAL_VDA_DNTAL_INDVD                                                                            \n") 
		.toString();
	/*.append(" SELECT CANAL.CCANAL_VDA_DNTAL_INDVD, CANAL.RCANAL_VDA_DNTAL_INDVD, CANAL.CSGL_CANAL_VDA_DNTAL_INDVD   ")              
	.append(" FROM DBPROD.CANAL_VDA_DNTAL_INDVD_NEW CANAL, DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD_NEW CVDA              ")
	.append(" WHERE CVDA.CPLANO_DNTAL_INDVD = 1                                                                     ")
	.append(" AND CANAL.CCANAL_VDA_DNTAL_INDVD = CVDA.CCANAL_VDA_DNTAL_INDVD                                        ")
	.toString();*/
	

    public static final String SEQUENCE_CANAL = "SELECT COALESCE(MAX(CCANAL_VDA_DNTAL_INDVD),0)+1 FROM DBPROD.CANAL_VDA_DNTAL_INDVD ";
	
	@Override
	public List<CanalVendaVO> listarTodosCanaisVenda() {
	 
		return 	 getJdbcTemplate().query(OBTER_CANAIS_VENDA, new MapSqlParameterSource(), new CanalVendaRowMapper());
	}
	
	@Override
	public List<CanalVendaVO> consultarCanalVenda(FiltroCanalVendaVO filtro) {
	 
		
		StringBuilder sb = new StringBuilder();
		sb.append(OBTER_CANAIS_VENDA);
		
		if(!Strings.isBlankOrNull(filtro.getNome()) && !Strings.isBlankOrNull(filtro.getSigla())){
			sb.append(" WHERE RCANAL_VDA_DNTAL_INDVD LIKE  '%"+filtro.getNome().toUpperCase()+ "%' AND CSGL_CANAL_VDA_DNTAL_INDVD LIKE '%"+filtro.getSigla().toUpperCase()+"%' ");
		}
		if(!Strings.isBlankOrNull(filtro.getNome()) && Strings.isBlankOrNull(filtro.getSigla())){
			sb.append(" WHERE RCANAL_VDA_DNTAL_INDVD LIKE  '%"+filtro.getNome().toUpperCase()+ "%' ");
		}
		if(Strings.isBlankOrNull(filtro.getNome()) && !Strings.isBlankOrNull(filtro.getSigla())){
			sb.append(" WHERE CSGL_CANAL_VDA_DNTAL_INDVD LIKE '%"+filtro.getSigla().toUpperCase()+"%' ");
		}
		
		sb.append(" ORDER BY CCANAL_VDA_DNTAL_INDVD ASC ");
		
		return getJdbcTemplate().query(sb.toString(), new MapSqlParameterSource(), new CanalVendaRowMapper());
	}
	

	@Override
	public DataSource getDataSource() {
		
		return dataSource;
	}

	@Override
	public boolean verificarExistenciaSiglaCanalVenda(String  sigla) {
		
		boolean existeSigla = false;
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("sigla", sigla);
		try{	
			if(getJdbcTemplate().queryForInt(VERIFICAR_EXISTENCIA_SIGLA, param)> 0){
				existeSigla = true;
			}
		}catch(Exception e){
			LOGGER.error("INFO: ERRO VERIFICAR_EXISTENCIA_SIGLA.");
		}
		

		return existeSigla;
	}

	@Override
	public boolean verificarExistenciaNomeCanalVenda(String nomeCanal) {
		
		boolean existeNome = false;
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("nome", nomeCanal.toUpperCase());
		try{
			
			if(getJdbcTemplate().queryForInt(VERIFICAR_EXISTENCIA_NOME, param)> 0){
				existeNome = true;
			}
			
		}catch(Exception e){
			
			LOGGER.error("INFO: ERRO VERIFICAR_EXISTENCIA_NOME.");

		}
		return existeNome;
	}

	@Override
	public boolean verificarExistenciaCodigoCanalVendaInclusao(
			Integer codigoCanal) {

		boolean existeCodigo = false;
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigo", codigoCanal);
		
		try{
			if(getJdbcTemplate().queryForInt(VERIFICAR_EXISTENCIA_CODIGO, param)> 0){
				existeCodigo = true;
			}
		}catch(Exception e){
			
			LOGGER.error("INFO: ERRO VERIFICAR_EXISTENCIA_CODIGO.");

		}
		return existeCodigo;
		
	}

	@Override
	public void incluirCanalVenda(CanalVendaVO canalVendaVO) {
		
		MapSqlParameterSource paramsInsert = new MapSqlParameterSource();
		
		canalVendaVO.setCodigo(obterNovoValorSequence());
		
		paramsInsert.addValue("codigoCanal", canalVendaVO.getCodigo());
		paramsInsert.addValue("siglaCanal", canalVendaVO.getSigla().toUpperCase());
		paramsInsert.addValue("nomeCanal", canalVendaVO.getNome().toUpperCase());
		
//		try{
			getJdbcTemplate().update(INSERT_CANAL_VENDA, paramsInsert);
//		}catch(Exception e){
//			LOGGER.error("INFO: ERRO INSERT_CANAL_VENDA.");
//		}
		
	}
	
	/** {@inheritDoc} */
	public Integer obterNovoValorSequence() {
		try{
			
			return this.getJdbcTemplate().queryForInt(SEQUENCE_CANAL, new MapSqlParameterSource());
		}catch(Exception e){
			
			LOGGER.error("INFO: ERRO AO OBTER CODIGO CANAL");
			return null;
		}
	}

	

	public CanalVendaVO obterCanalDeVendaPorCodigo(Integer codigo){
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		
		param.addValue("codigo", codigo);
		return getJdbcTemplate().queryForObject(OBTER_CANAL_VENDA, param, new CanalVendaRowMapper());
	}

	@Override
	public void alterarCanal(CanalVendaVO canalVendaVO) {
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoCanal", canalVendaVO.getCodigo());
		params.addValue("siglaCanal", canalVendaVO.getSigla().toUpperCase());
		params.addValue("nomeCanal", canalVendaVO.getNome().toUpperCase());
		
		getJdbcTemplate().update(ATUALIZAR_CANAL_VENDA, params);
	}

	@Override
	public List<CanalVendaVO> obterCanaisPorCodigoPlano(Long codigoPlano) {
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPlano", codigoPlano);
		
		List<CanalVendaVO> canal = null; 
			
		canal =  getJdbcTemplate().query(OBTER_CANAIS_POR_CODIGO_PLANO, param, new CanalVendaRowMapper());
		
		
		return canal;
	}



}
