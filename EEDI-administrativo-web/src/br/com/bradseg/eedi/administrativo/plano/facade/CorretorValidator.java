package br.com.bradseg.eedi.administrativo.plano.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;




import br.com.bradseg.eedi.administrativo.support.Validador;
import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorViewVO;


@Component
public class CorretorValidator {
	
	@Autowired
	private transient CorretorServiceFacade corretorServiceFacade;
	
	/**
	 * 
	 * @param cadastroCorretorViewVO
	 */
	public void validarPreenchimentoCampoDeConsulta(CadastroCorretorViewVO cadastroCorretorViewVO) {
		Validador validador = new Validador(); 
		validador.obrigatorio(cadastroCorretorViewVO.getSucursal(), "SUCURSAL");
		validador.validar();
	}
	
	public void validarPreenchimentoCamposCadastroCorretor(CadastroCorretorViewVO cadastroCorretorViewVO) {
		Validador validador = new Validador();
		validador.obrigatorio(cadastroCorretorViewVO.getCnpj(), "CPNJ/CPF");
		validador.obrigatorio(cadastroCorretorViewVO.getSucursal(), "SUCURSAL");
		validador.obrigatorio(cadastroCorretorViewVO.getNome(), "NOME");
		//validador.obrigatorio(cadastroCorretorViewVO.getDataInicioContrato(), "DATA INICIO");
		validador.obrigatorio(cadastroCorretorViewVO.getDataInicioContratoTela(), "DATA INICIO");
		validador.obrigatorio(cadastroCorretorViewVO.getDataFimContratoTela(), "DATA FIM");
		validador.validar();
	}
	
	public void validarCampoDataFimVigenciaAlteracaoCorretor(CadastroCorretorViewVO cadastroCorretorViewVO) {
		Validador validador = new Validador();
		validador.obrigatorio(cadastroCorretorViewVO.getDataFimContratoTela(), "DATA FIM");
		validador.validar();
	}
	
	/**
	 * 
	 * @param mensagem
	 */
	public void adicionarErroEValidar(String mensagem){
		Validador validador = new Validador();
		
		validador.adicionarErro(mensagem);
		validador.validar();
	}
	
}
