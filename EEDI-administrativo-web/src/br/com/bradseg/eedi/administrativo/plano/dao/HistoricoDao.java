package br.com.bradseg.eedi.administrativo.plano.dao;

import java.util.Date;
import java.util.List;

import br.com.bradseg.eedi.administrativo.vo.FiltroHistoricoVO;
import br.com.bradseg.eedi.administrativo.vo.HistoricoCanalVendaVO;
import br.com.bradseg.eedi.administrativo.vo.HistoricoPlanoVO;
import br.com.bradseg.eedi.administrativo.vo.ValorPlanoVO;

public interface HistoricoDao {
	
	/**
	 * Consulta o histórico do canal de venda
	 *
	 * @return List - Lista de {@link HistoricoCanalVendaVO}
	 */
	public List<HistoricoCanalVendaVO> listarHistoricoCanalVendaPorCodigo(Integer codigo);
	/**
	 * Consulta o histórico do Plano.
	 * 
	 * @return List - Lista de {@link HistoricoPlanoVO}
	 */
	// List<HistoricoCanalVendaVO> listarTodosCanais();
	
	public List<HistoricoPlanoVO>listarTodosPlanos();
	
	public List<HistoricoPlanoVO> listarHistoricoPlanoPorCodigo(Long codigo);
	
	public List<HistoricoPlanoVO> listarHistoricoPlano();
	
	public List<String> listarUsuarios (); 
	
	public List<HistoricoPlanoVO> consultarHistorico (FiltroHistoricoVO filtro);
	
	public HistoricoPlanoVO listarHistoricoPlanoMenorData(HistoricoPlanoVO historicoPlano);	
	
	public List<HistoricoCanalVendaVO> listarHistoricoPorData(Date dataAlteracao, Integer codigoPlano);		
	
	public HistoricoCanalVendaVO listarHistoricoMenorData(Date dataAlteracao, Integer codigoPlano);
	
	public ValorPlanoVO listarValoresPorData(HistoricoPlanoVO historicoPlano);
	
	public ValorPlanoVO listaValorPorMenorData(HistoricoPlanoVO historicoPlano); 
	
	
	
	
	

}
