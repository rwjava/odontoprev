package br.com.bradseg.eedi.administrativo.plano.webservice;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Adaptador de String para DateTime e DateTime para String para ser usado pelos webservices expostos
 * 
 * @author WDEV
 */
public class DateTimeAdapter extends XmlAdapter<String, DateTime> {

	public static final String FORMATO_DATE_TIME_WEB_SERVICE = "dd/MM/yyyy";
	
	@Override
	public DateTime unmarshal(String dataHora) {
		return DateTimeFormat.forPattern(FORMATO_DATE_TIME_WEB_SERVICE).parseDateTime(dataHora);
	}

	@Override
	public String marshal(DateTime dataHora) {
		return dataHora.toString(FORMATO_DATE_TIME_WEB_SERVICE);
	}

}