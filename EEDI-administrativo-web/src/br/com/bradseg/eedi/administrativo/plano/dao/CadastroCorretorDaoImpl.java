package br.com.bradseg.eedi.administrativo.plano.dao;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.administrativo.rowmapper.CadastroCorretorRowMapper;
import br.com.bradseg.eedi.administrativo.vo.CadastroCorretorVO;

@Repository
public class CadastroCorretorDaoImpl extends JdbcDao implements CadastroCorretorDao {

	@Autowired
	private DataSource dataSource;
	
	/********************************************************************
	 *							SELECT - LISTAR							*
	 ********************************************************************/
	
	private static final String LISTA_CORRETORES = new StringBuilder()
	.append(" SELECT 												\n")
	.append(" 		*												\n")
	.append(" FROM 													\n")
	.append(" 		DBPROD.TESTE_LOG_COBER							\n")
	.toString();
	
	private static final String OBTER_CORRETOR_POR_CNPJ = new StringBuilder()
	.append(" SELECT 												\n")
	.append(" 		*										 		\n")
	.append(" FROM 													\n")
	.append(" 		DBPROD.TESTE_LOG_COBER 							\n")
	.append(" WHERE 												\n")
	.append(" 		NM_TABELA = :cnpj 								\n")
	.toString();
	
	private static final String LISTA_CORRETORES_POR_SUCURSAL = new StringBuilder()
	.append(" SELECT 												\n")
	.append(" 		* 												\n")
	.append(" FROM													\n")
	.append("		DBPROD.TESTE_LOG_COBER							\n")
	.append(" WHERE 												\n")
	.append("		NM_PROGRAMA = :sucursal 						\n")
	.toString();
	
	private static final String LISTA_CORRETOR_POR_SUCURSAL_E_STATUS = new StringBuilder()
	.append(" SELECT 												\n")
	.append(" 		*												\n")
	.append(" FROM 													\n")
	.append(" 		DBPROD.TESTE_LOG_COBER 							\n")
	.append(" WHERE 												\n")
	.append(" 		NM_PROGRAMA = :sucursal 						\n")
	.append(" AND 													\n")
	.append(" 		ACAO = :status 									\n")
	.toString();
	
	private static final String VERIFICAR_EXISTENCIA_CNPJ = new StringBuilder()
	.append(" SELECT  												\n")
	.append(" 		COUNT(*)  										\n")
	.append(" FROM  												\n")
	.append(" 		DBPROD.TESTE_LOG_COBER  						\n")
	.append(" WHERE													\n")
	.append(" 		NM_TABELA = :cnpj 								\n")
	.toString();
	
	/********************************************************************
	 *							UPDATE - ALTERAR						*
	 ********************************************************************/
	
	private static final String ATUALIZA_DADOS_CORRETOR = new StringBuilder()
	.append(" UPDATE 												\n")
	.append("		DBPROD.TESTE_LOG_COBER 							\n")
	.append(" SET 													\n")
	.append(" 		DATULZ_REG = :dataFimContrato, 					\n")
	.append("		ACAO = :status 									\n")
	.append(" WHERE 												\n")
	.append(" 		NM_TABELA = :cnpj 								\n")
	.toString();
	
	/********************************************************************
	 *							INSERT - INCLUIR						*
	 ********************************************************************/
	private static final String CADASTRA_ACESSO_CORRETOR = new StringBuilder() 
	.append(" INSERT INTO 											\n")
	.append(" 			DBPROD.TESTE_LOG_COBER 						\n")
	.append(" 	(NM_TABELA, NM_PROGRAMA, LINHA_TABELA,				\n")
	.append(" 	CRESP_ATULZ, DATULZ_REG, ACAO, NM_CREATOR)			\n")
	.append(" VALUES 												\n")
	.append(" 	(:cnpj, :sucursal, :nome, '18012014', 				\n")
	.append("  	:dataFimContrato, :status, 	'DBPROD' )			    \n")
	.toString();
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanoDaoImpl.class);
	
	public boolean verificarExistenciaCorretorPorCnpj(Long cnpj) {
		boolean existeCnpj = false;
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("cnpj", cnpj);
		
		try{
			if(getJdbcTemplate().queryForInt(VERIFICAR_EXISTENCIA_CNPJ, param)> 0){
				existeCnpj = true;
			}
		}catch(Exception e){
			LOGGER.error("INFO: ERRO VERIFICAR_EXISTENCIA_CODIGO.");
		}
		return existeCnpj;
		
	}
	
	
	/**
	 * 
	 */
	@Override
	public DataSource getDataSource() {
		return dataSource;
	}
	
	/**
	 * 
	 */
	@Override
	public List<CadastroCorretorVO> listarCorretores() {
		List<CadastroCorretorVO> corretores =   getJdbcTemplate().query(LISTA_CORRETORES,new MapSqlParameterSource() ,new CadastroCorretorRowMapper());
		
		return corretores;
	}
	
	@Override
	public CadastroCorretorVO obterDadosCorretorPorCnpj(Long cnpj) {
		CadastroCorretorVO cadastroCorretor = null;
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("cnpj", cnpj);
		
		cadastroCorretor = getJdbcTemplate().queryForObject(OBTER_CORRETOR_POR_CNPJ, param, new CadastroCorretorRowMapper());
		
		return cadastroCorretor;
	}
	
	@Override
	public List<CadastroCorretorVO> listarCorretorPorSucursal(Long sucursal) {
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("sucursal", sucursal);
		List<CadastroCorretorVO> corretores = getJdbcTemplate().query(LISTA_CORRETORES_POR_SUCURSAL, param, new CadastroCorretorRowMapper());
		
		return corretores;
	}
	
	@Override
	public List<CadastroCorretorVO> listarCorretorPorSucursalEStatus(Long sucursal, Integer status) {
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("sucursal", sucursal);
		param.addValue("status", status);
		List<CadastroCorretorVO> corretoresPorSucursalEStatus = getJdbcTemplate().query(LISTA_CORRETOR_POR_SUCURSAL_E_STATUS, param, new CadastroCorretorRowMapper());
		
		return corretoresPorSucursalEStatus;
	}
	
	@Override
	public void atualizarVigencia(CadastroCorretorVO cadastroCorretorVO) {
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("cnpj", cadastroCorretorVO.getCnpj());
		param.addValue("dataFimContrato", new java.sql.Date(cadastroCorretorVO.getDataFimContrato().getMillis()));
		param.addValue("status", cadastroCorretorVO.getStatus());
		getJdbcTemplate().update(ATUALIZA_DADOS_CORRETOR, param);
	}
	
	@Override
	public void incluirAcessoCorretor(CadastroCorretorVO cadastroCorretorVO) {
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("cnpj", cadastroCorretorVO.getCnpj());
		param.addValue("sucursal", cadastroCorretorVO.getSucursal());
		param.addValue("nome", cadastroCorretorVO.getNome());
		//param.addValue("dataInicioContrato", new java.sql.Date(cadastroCorretorVO.getDataInicioContrato().getMillis()));
		param.addValue("dataFimContrato", new java.sql.Date(cadastroCorretorVO.getDataFimContrato().getMillis()));
		param.addValue("status", cadastroCorretorVO.getStatus());
		getJdbcTemplate().update(CADASTRA_ACESSO_CORRETOR, param);
	}

}
