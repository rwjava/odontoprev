<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Plano - Visualizar" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/visualiza.js'/>"></script>
			<script type="text/javascript">
		$(document).ready(function(){
			$('#abaPlano').addClass('tabact');
			$('#abaCanal').removeClass('tabact');
			
			$('#tab_plano').addClass('open');
			$('#tab_canal').removeClass('open');
			$('#tab_historico').removeClass('open');	
		});

		/* alert($('#valorAno').val()); 
		alert($('#valorMes').val());
		
		if($('#valorAno').val() == ''){
		//div_tipo_cobranca_anual
			alert($('#valorAno').val());
			$('#div_tipo_cobranca_anual').hide();		
		}

		if($('#valorMes').val() == ''){
		//div_tipo_cobranca_mensal
			alert($('#valorMes').val());
			$('#div_tipo_cobranca_mensal').hide();
		} */
		
		</script>
	</head>
<body>
	<s:form action="alterar" id="form">
		<div class="container painel" >
			<div class="row">
		
				<div hidden="true">
					 <s:select name="planoViewVO.tipoCobrancaPlano" id="tipoCobranca"  cssClass="form-control" data-required="Tipo de Cobran�a" list="listaDeTipoCobranca" headerKey="0"  listKey="codigo" listValue="descricao"></s:select>
				</div>
				
				<s:hidden name="planoViewVO.novoTipoCobrancaPlano" id="novoTipoCobranca"></s:hidden>
				
				<s:hidden name="planoVO.existeValorProgramado" id="existeValorProgramado"></s:hidden>
				
				<s:hidden name="planoVO.status"   id="status" />
			</div>	
			
			<div class="col-md-12 box">
				<h4 id="plano">Dados do Plano</h4>
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>C�digo</label>
						<br>
						<p>
						<strong>
							<s:property value="planoViewVO.codigo" />
						</strong>
						</p>
					</div>	
				</div>
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Nome</label>
						<br>
						<p>
							<strong>
								<s:property value="planoViewVO.nome" />
							</strong>
						</p>
					</div>	
				</div>
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>C�digo de Registro</label>
						<br>
						<p>
							<strong>
								<s:property value="planoViewVO.codigoRegistro" />
							</strong>
						</p>
					</div>	
				</div>
				
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Vig�ncia</label>
						<br>	
			        	<p>
			                <strong>
			                	<s:property  value="planoViewVO.dataInicioVigenciaTela"  />
			                </strong>		            
		
						   		&nbsp;<span>At�</span>&nbsp;
						
							<strong>  		
								<s:property  value="planoViewVO.dataFimVigenciaTela" />
							</strong>
						</p>
					</div>	
				</div>
				
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Taxa (R$)</label>
						<br>
						<p>
							<strong>
								<s:property value="planoViewVO.valorTaxa"/>
							</strong>
						</p> 
					</div>	
				</div>
		
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Valor Desconto (R$)</label>
						<br>
						<p>
							<strong>
								<s:property value="planoViewVO.valorDesconto"/>
							</strong>
						</p>
					</div>	
				</div>
			</div>	
			
			
			
			<div class="col-md-12 box">
				<h4 >Valor do Plano</h4>
				
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Data In�cio do Valor</label>
						<p>
							<strong>
		        				<s:property value="planoViewVO.dataInicioVigenciaTela"/>
							</strong>
						</p>					
					</div>
				</div>
				
				
				
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Tipo de Cobran�a </label>
						<p>
							<strong>
								   <s:property value="planoViewVO.descricaoTipoCobranca"/>
							</strong>
						</p>														
													
					</div>
				</div>
				<div class="row " id="div_tipo_cobranca_mensal" >
					<div class="form-group col-md-12 " style="margin-left: 0px;">
						<h4 style="margin-left: -14px; padding-bottom: 0px;">Valor Mensal</h4>
						<br/>
						<div class="form-group col-md-3 " style="font-weight: 600;">
				        	<label>Valor Titular(R$)</label><br/>
								<p>
									<strong>
				        				<s:property value="planoViewVO.valorMensalTitular"/>
									</strong>
								</p>
			
							<label>Valor Dependente(R$)</label><br/>
				        		<p>
									<strong>
				        				<s:property value="planoViewVO.valorMensalDependente"/>
									</strong>
								</p>
								
						</div>
						<div class="col-md-9" style="font-weight: 600;">
							<label >Per�odo de Car�ncia:</label>
						   		 <p>
									<strong>
						   					 <s:property value="planoViewVO.descricaoCarenciaPeriodoMensal"/>
									</strong>
								</p>
						</div>
					</div>
				</div>
				<div class="row " id="div_tipo_cobranca_anual" >
					<div class="form-group col-md-12 " style="margin-left: 0px;">
						<h4 style="margin-left: -14px; padding-bottom: 0px;">Valor Anual</h4>
						<br/>
						<div class="form-group col-md-3 " style="font-weight: 600;">
				        	<label>Valor Titular(R$)</label><br/>
								<p>
									<strong>
				        				<s:property value="planoViewVO.valorAnualTitular"/>
									</strong>
								</p>
			
							<label>Valor Dependente(R$)</label><br/>
				        		<p>
									<strong>
				        				<s:property value="planoViewVO.valorAnualDependente"/>
									</strong>
								</p>
								
						</div>
						<div class="col-md-9" style="font-weight: 600;">
							<label >Per�odo de Car�ncia:</label>
						   		 <p>
									<strong>
						   					 <s:property value="planoViewVO.descricaoCarenciaPeriodoAnual"/>
									</strong>
								</p>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-12 box" id="div_novo_valor">
				<h4 >Novo Valor do Plano</h4>
				
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Data In�cio do Novo Valor</label>
						<p>
							<strong>
		        				<s:property value="planoViewVO.dataInicioNovoValor"/>
							</strong>
						</p>					
					</div>
				</div>
				
				
				
				<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Tipo de Cobran�a </label>
						<p>
							<strong>
		        				<s:property value="planoViewVO.descricaoNovoTipoCobranca"/>
							</strong>
						</p>														
													
					</div>
				</div>
				<div class="row " id="nova_div_tipo_cobranca_mensal" >
					<div class="form-group col-md-12 " style="margin-left: 0px;">
						<h4 style="margin-left: -14px; padding-bottom: 0px;">Novo Valor Mensal</h4>
						<br/>
						<div class="form-group col-md-3 " style="font-weight: 600;">
				        	<label>Valor Titular(R$)</label><br/>
								<p>
									<strong>
				        				<s:property value="planoViewVO.novoValorMensalTitular"/>
									</strong>
								</p>
			
							<label>Valor Dependente(R$)</label><br/>
				        		<p>
									<strong>
				        				<s:property value="planoViewVO.novoValorMensalDependente"/>
									</strong>
								</p>
								
						</div>
						<div class="col-md-9" style="font-weight: 600;">
							<label >Per�odo de Car�ncia:</label>
						   		 <p>
									<strong>
						   					 <s:property value="planoViewVO.descricaoCarenciaPeriodoMensal"/>
									</strong>
								</p>
						</div>
					</div>
				</div>
				<div class="row " id="nova_div_tipo_cobranca_anual" >
					<div class="form-group col-md-12 " style="margin-left: 0px;">
						<h4 style="margin-left: -14px; padding-bottom: 0px;">Novo Valor Anual</h4>
						<br/>
						<div class="form-group col-md-3 " style="font-weight: 600;">
				        	<label>Valor Titular(R$)</label><br/>
								<p>
									<strong>
				        				<s:property value="planoViewVO.novoValorAnualTitular"/>
									</strong>
								</p>
			
							<label>Valor Dependente(R$)</label><br/>
				        		<p>
									<strong>
				        				<s:property value="planoViewVO.novoValorAnualDependente"/>
									</strong>
								</p>
								
						</div>
						<div class="col-md-9" style="font-weight: 600;">
							<label >Per�odo de Car�ncia:</label>
						   		 <p>
									<strong>
						   					 <s:property value="planoViewVO.descricaoCarenciaPeriodoAnual"/>
									</strong>
								</p>
						</div>
					</div>
				</div>
			</div>
		
			<div class="col-md-12 box">
				<h4 id="plano">Lista de Canais</h4>
				<div class="row">
					<display:table name="planoViewVO.listaDeCanais" id="canal" style="width: 100%" class="table table-striped table-hover table-pendencias " requestURI="listarPorFiltro.do" >
						<display:setProperty name="basic.msg.empty_list"  value="Nenhum canal para este plano."/> 
						<display:column title="C�digo" style="width: 20%;">
							${canal.codigo} 
							
						</display:column>
						<display:column title="Nome" style="width: 20%;">
							 ${canal.nome}	
						</display:column>
					</display:table>
			 	</div>  
			</div>  
			
			<div class="col-md-12 box">
				<h4 id="plano">Segmentos</h4>
				<div class="row">
					<display:table name="segmentos" id="segmento" style="width: 100%" class="table table-striped table-hover table-pendencias " >
						<display:setProperty name="basic.msg.empty_list"  value="Nenhum segmento para este plano."/> 
						<display:column title="Nome">
							 ${segmento.nome}	
						</display:column>
					</display:table>
				</div>
			</div>	
		</div>
		<br/><br/>
		<div align="center">
			<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="voltarConsultarPlano" onclick="location.href='../plano/listarPorFiltro.do';">Voltar</button>
		</div>
		<br/><br/>

	</s:form>
	</body>
</html>