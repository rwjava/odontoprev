<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Plano - Consultar" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
		<script type="text/javascript">
		$(document).ready(function(){
		
			$('#abaCanal').removeClass('tabact');	
			$('#tab_canal').removeClass('open');
			$('#tab_historico').removeClass('open');
			$('#abaPlano').addClass('tabact');
			$('#tab_plano').addClass('open');
		});
		</script>
	</head>
<body>
	<s:form action="listarPorFiltro" id="form">
		<div class="container painel"  >
			<div class="col-md-12 box">
			   <h4 id="plano">Dados para Consulta de Plano</h4>
			   <div class="row">
					<div class="row" style="font-weight: 600;">
						<div class="form-group col-md-4 ">
							<label>Nome</label>
							<s:textfield name="filtro.nomePlano" id="nome" cssClass="form-control"  maxlength="50" size="50"/>
						</div>
					</div>
					<div class="row" style="font-weight: 600;">
						<div class="form-group col-md-4 ">
							<label>Canal de Venda</label>
								<s:select name="filtro.codigoCanal"  cssClass="form-control" id="canal"class="form-control"  list="listaDeCanais" headerKey="0" headerValue="Selecione um Canal de Venda" listKey="codigo" listValue="nome" />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4 " style="font-weight: 600;">
							<label>Status</label>
							<s:select name="filtro.status" cssClass="form-control" id="status" value="filtro.status" list="listaDeStatus" headerKey="0" headerValue="Selecione o status" listKey="codigo" listValue="descricao" >
							</s:select>
						</div>
					</div>
					<br/>
					<div class="row">
						<div align="center">
							<button type="button" class="btn btn-primary btn-sm ui-margin-bottom"  id="consultar">Consultar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="limpar">Limpar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="novo">Novo</button>
						</div>
					</div>
					<br/><br/>
				</div>
			</div> <!-- fim col-md-12 box -->
			<br/><br/><br/>
	
			<div class="col-md-12 box">
				<h4 id="plano">Planos</h4>
				<div class="row">
					<display:table name="listaDePlanos" id="plano" style="width: 100%" class="table table-striped table-hover table-pendencias " requestURI="listarPorFiltro.do" pagesize="100" >
						<display:setProperty name="basic.msg.empty_list"  value="Nenhum plano encontrado."/> 
						<display:column title="C�digo" style="width: 10%;">
							${plano.codigo} 
						</display:column>
						<display:column title="Nome" style="width: 35%;">
							 ${plano.nome}	
						</display:column>
						<display:column title="Canal de Venda" style="width: 35%;">
						<c:forEach var="canal" items="${plano.listaDeCanais}" >
			 				 ${canal.nome}<br/>
						</c:forEach>
						</display:column>
						<display:column title="Status" style="width: 15%;">
							 ${plano.status}	
						</display:column>
						<display:column title="A��es" style="width:5%;">
							<div class="dropdown">
								<button class="btn btn-default btn-xs btn-block btn-default" data-toggle="dropdown">
									<span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>
								</button>							
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">											
								    <li class="breaker">
									   <s:a action="visualizar.do" namespace="/plano">
											<s:param name="planoVO.codigo">
												${plano.codigo}
											</s:param>
											<span>Visualizar</span>
										</s:a>  
									</li>	
									<s:if test="#attr.plano.podeAlterarPlano > 0">
									    <li>
										    <s:a action="iniciarAlterar.do" namespace="/plano">
												<s:param name="planoVO.codigo">
													${plano.codigo}
												</s:param>
												<span>Alterar</span>
											</s:a>
									    </li>
								    </s:if>	
								    <li>
									    <s:a action="iniciarHistoricoPlano.do" namespace="/historico">
											<s:param name="planoVO.codigo">
												${plano.codigo}
											</s:param>
											<span>Hist�rico</span>
										</s:a>	
									</li>								
								</ul>
							</div>			
						</display:column>
					</display:table>
				</div>
			</div> <!-- fim col-md-12 box -->
		</div>
	</s:form>
</body>
</html>