<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Plano - Cadastro" />
		    	<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
		    	<script type="text/javascript" src="<s:url value='/includes/js/altera.js'/>"></script>
		    
		
		<script type="text/javascript">
			$(document).ready(function(){
	
				$('#abaPlano').addClass('tabact');
				$('#abaCanal').removeClass('tabact');
				
				$('#tab_plano').addClass('open');
				$('#tab_canal').removeClass('open');
				$('#tab_historico').removeClass('open');	
				
				 $('#multiselect').multiselect();

				    $(document).on('change','#multiselect', function(){
				    });

			});
		</script>
			
	</head>
<body>


	<s:form action="incluir" id="form">
		<div class="container painel" >
		<div class="row">
		
			<div class="col-md-12 box">
			<h4 id="plano">Dados para Inclus�o de Plano</h4>
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Nome</label>
						<span class="ui-required"><em> (Obrigat�rio)</em></span>
						<s:textfield name="planoViewVO.nome" id="nome" data-required="Nome" cssClass="form-control"  maxlength="50" size="50"/>
					</div>	
			</div>	
		
			<div class="row">
				<div class="form-group col-md-4 " style="font-weight: 600;">	
					<label>Vig�ncia</label><span class="ui-required"><em> (Obrigat�rio)</em></span>	
	                <s:textfield name="planoViewVO.dataInicioVigenciaTela"  data-date-type="default"  id ="dataInicioVigencia" cssClass="form-control"  style="width: 30%;"/>&nbsp;<span>At�</span>&nbsp;
					<s:textfield name="planoViewVO.dataFimVigenciaTela"  data-date-type="default"  size="12"  data-required="Data Fim de Vig�ncia"  id="dataFimVigencia" cssClass="form-control"  style="width: 30%;"/>	
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-4 " style="font-weight: 600;">
					<label>C�digo de Registro</label><span class="ui-required"><em> (Obrigat�rio)</em></span>
					<s:textfield name="planoViewVO.codigoRegistro" id="codigoRegistro" cssClass="form-control"  data-type="integer"  maxlength="9" size="10" style="width:40%"/>
				</div>	
			</div>
			
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Taxa (R$)</label>
						<s:textfield name="planoViewVO.valorPlanoVO.valorTaxa" id="valorTaxa" data-type="currency" cssClass="form-control" maxlength="6" size="6" style="width:20%"/>
					</div>	
			</div>
			
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Valor Desconto (R$)</label>
						<s:textfield name="planoViewVO.valorPlanoVO.valorDesconto" id="valorDesconto" data-type="currency"   cssClass="form-control" maxlength="6" size="6" style="width:20%"/>
					</div>	
			</div>
			
			
		</div><!-- fim box -->
		<div class="col-md-12 box">
		<h4 id="tipoDeCobranca">Sele��o do tipo de cobran�a</h4>
		<div class="row">
				<div class="form-group col-md-4 " style="font-weight: 600;">
					<label>Tipo de Cobran�a </label>
					<span class="ui-required"><em> (Obrigat�rio)</em></span>
						
						
						<s:select name="planoViewVO.tipoCobrancaPlano" id="tipoCobranca"  cssClass="form-control" 
						 data-required="Tipo de Cobran�a" list="listaDeTipoCobranca" headerValue="Selecione um Tipo de Cobran�a"
						 headerKey=""  listKey="codigo" listValue="descricao"></s:select>								
				</div>
		</div>
		
	
		<div class="row " id="div_tipo_cobranca_mensal" style="display: none;" >
			<div class="form-group col-md-12 " style="margin-left: 0px;">
				<h4 style="margin-left: -14px; padding-bottom: 0px;">Valor Mensal</h4>
				<br/>
				<div class="row col-md-3" style="font-weight: 600; margin-left: -15px;">	
		        	<label>Valor Titular(R$)</label><span class="ui-required mensal"><em> (Obrigat�rio)</em></span>
					<br/>
					<s:textfield maxlength="6" cssClass="form-control" size="12"  name="planoViewVO.valorPlanoVO.valorMensalTitular" data-required="Valor Mensal Titular" data-type="currency" style="width: 30%;"/>
					<br/><br/>
					<label>Valor Dependente(R$)</label><span class="ui-required mensal"><em> (Obrigat�rio)</em></span>
					<br/>
					<s:textfield maxlength="6" cssClass="form-control" size="12"  name="planoViewVO.valorPlanoVO.valorMensalDependente" data-required="Valor Mensal Dependente" data-type="currency" style="width: 30%;"/>				
					
				</div>
				<div class="col-md-8" style="font-weight: 600;">
					<label >Per�odo de Car�ncia</label><span class="ui-required mensal"><em> (Obrigat�rio)</em></span>
					<s:textarea cols="70" rows="4" maxlength="500" cssClass="form-control" name="planoViewVO.descricaoCarenciaPeriodoMensal" data-required="Per�odo de Car�ncia Anual" style="width: 90%;"></s:textarea>
				</div>
			</div>
		</div>	<!--  fim div tipo cobran�a mensal -->
	
		<div class="row " id="div_tipo_cobranca_anual" style="display: none;">
			<div class="form-group col-md-12 " style="margin-left: 0px;">
				<h4 style="margin-left: -14px; padding-bottom: 0px;">Valor Anual</h4>
				<br/>
				<div class="row col-md-3" style="font-weight: 600; margin-left: -15px;">	
		        	<label>Valor Titular(R$)</label><span class="ui-required anual"><em> (Obrigat�rio)</em></span>		
		        	<br/>		
					<s:textfield maxlength="7" cssClass="form-control" size="12"  name="planoViewVO.valorPlanoVO.valorAnualTitular"  data-required="Valor Anual Titular" data-type="currency" style="width: 30%;"/>
					<br/><br/>
					<label>Valor Dependente(R$)</label><span class="ui-required anual"><em> (Obrigat�rio)</em></span>
					<br/>
					<s:textfield type="text" maxlength="7" cssClass="form-control" size="12"  name="planoViewVO.valorPlanoVO.valorAnualDependente" data-required="Valor Anual Dependente"  data-type="currency" style="width: 30%;"/>
					
				</div>	
					<div class="col-md-8" style="font-weight: 600;">
					<label >Per�odo de Car�ncia</label><span class="ui-required anual"><em> (Obrigat�rio)</em></span>					
					<s:textarea cols="70" rows="4" maxlength="500" cssClass="form-control" name="planoViewVO.descricaoCarenciaPeriodoAnual" data-required="Per�odo de Car�ncia Anual" style="width: 90%;"></s:textarea>
					
					</div>
				</div>
			</div>	<!--  fim div tipo cobran�a anual -->
		</div><!--  fim do box -->
	

	 <div class="col-md-12 box">
			<h4 id="canalDeVenda">canais de venda</h4>
		
			<div class="row">
	
			
			 <s:select multiple="true" id="multiselect" list="planoViewVO.listaDeCanais" listKey="codigo" listValue="nome"  name="listaDeCanaisSelecionados"  size="%{planoViewVO.listaDeCanais.size()}"></s:select>  
			
			</div>
		</div><!-- fim box -->


		<div class="col-md-12 box">
			<h4>Segmentos</h4>
			<div class="row">
				  <s:checkboxlist name="planoViewVO.listaTipoSucursal" list="listaTipoSucursal" listKey="codigo" listValue="nome"/>
			</div>
		</div>

  			
	   </div> <!-- fim box -->
	   </div><!-- fim container principal -->
		
		<br/><br/><br/>
	
		<div class="row">
			<div align="center">
				<button type="button" class="btn btn-primary btn-sm ui-margin-bottom"  id="incluir">Incluir</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="voltarConsultarPlano">Voltar</button>
			</div>
		</div>
		<br/><br/>
	</s:form>
	</body>
</html>