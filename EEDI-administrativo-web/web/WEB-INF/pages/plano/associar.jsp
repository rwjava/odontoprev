<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Plano - Canal de Venda - Associa��o" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/plano.js'/>"></script>
			<script type="text/javascript">
		$(document).ready(function(){
			$('#abaPlano').addClass('tabact');
			$('#abaCanal').removeClass('tabact');
			
			$('#tab_plano').addClass('open');
			$('#tab_canal').removeClass('open');
			$('#tab_historico').removeClass('open');	
		});
		</script>
	</head>
<body>
	
	<s:form action="associarPlanoCanal" id="form">
	<jsp:include page="../menu.jsp"></jsp:include>
	<table style="width: 100%" class="tabela_interna">
			<tr>
				<th colspan="7">Associar Plano</th>
			</tr>
			<tr>
				<td width="10%" class="td_label">Data In�cio Vig�ncia: <span class="obrigatorio">*</span> </td>
				<td width="40%">
					<s:textfield name="planoVO.dataInicioVigencia" id="dataInicioVigencia" data-mask="data" data-date-type="default" size="12"/>
				</td>
				<td width="10%" class="td_label">Data Fim Vig�ncia: <span class="obrigatorio">*</span> </td>
				<td width="40%">
					<s:textfield name="planoVO.dataFimVigencia" id="dataFimVigencia" data-mask="data" data-date-type="default" size="12"/>
				</td>
			</tr>
			<tr>
				<td width="10%" class="td_label" align="center">Plano: <span class="obrigatorio">*</span> </td>
				<td colspan="3" width="90%">
					<s:select name="planoVO.codigo" id="plano" value="planoVO.nome" list="listaDePlanos" headerKey="0"  listKey="codigo" listValue="nome"/>
				</td>
			</tr>
			<tr>
				<td width="10%" class="td_label" align="center" >Canais de Venda: <span class="obrigatorio">*</span></td>
                
                <td width="90%" colspan="3">
               		 <s:checkboxlist name="listaCanaisSelecionados" list="listaDeCanais" listValue="nome" listKey="codigo"/>   
				</td>
				
				
			</tr>
		</table>
		<br/><br/><br/>
		<table id="tabela_botoes" style="width: 100%">
			<tr>
				<td align="center" width="100%">
					<img src="<c:url value="/includes/images/bt_associar.jpg" />" id="associar" style="cursor:pointer;cursor:hand;"/>
					&nbsp;&nbsp;&nbsp;
					<img src="<c:url value="/includes/images/bt_voltar.jpg" />" id="voltar" style="cursor:pointer;cursor:hand;"/>
				</td>
			</tr>
		</table>
	</s:form>
	</body>
</html>