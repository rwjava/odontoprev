<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Plano - Cadastro" />
    	<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
    	<script type="text/javascript" src="<s:url value='/includes/js/altera.js'/>"></script> 
    
	
	
		<script type="text/javascript">

		
			$(document).ready(function(){
	
				$('#abaPlano').addClass('tabact');
				$('#abaCanal').removeClass('tabact');
				
				$('#tab_plano').addClass('open');
				$('#tab_canal').removeClass('open');
				$('#tab_historico').removeClass('open');	
				
				 $('#multiselect').multiselect();
				 $('#multiselectOculto').multiselect();		
				    
			     $("#multiselect > option").each(function() {
				       var codigoCanal = this.value;
				    	//alert('multiselect '+ this.text + ' ' + this.value);
				        
				        $("#multiselectOculto > option").each(function() {
					        //alert('multiselectOculto '+this.text + ' ' + this.value);					        
					        if(codigoCanal == this.value){
					        
					        	$('li input').each(function(){
							    	
					        		if(this.value == codigoCanal){
							    		//alert('ola'+ this.value);
							    		this.click();
					        		}
						    	});							
							}
				        });					        
				       
				   });		
			});
		

					
		</script>
			
	</head>
<body>


	<s:form action="incluir" id="form">
		<div class="container painel" >
		<div class="row">
		
		
		<s:hidden name="planoVO.codigo"   id="codigoPlano" />
		<s:hidden name="planoViewVO.existeValorProgramado"  id="existeValorProgramado" />
		<s:hidden name="planoVO.status"   id="status" />
		<s:hidden name="planoViewVO.valorProgramadoExcluido"   id="valorProgramadoExcluido" />
		<div hidden="true">
			<s:select multiple="true" id="multiselectOculto" list="planoViewVO.listaDeCanaisSelecionados" listKey="codigo" listValue="nome" 
				name="listaDeCanaisSelecionadosParaAlteracao"  size="%{planoViewVO.listaDeCanaisSelecionados.size()}" disabled="true"  ></s:select> 
			   
	 
	    </div>
		
			<div class="col-md-12 box">
			<h4 id="plano">Dados do Plano</h4>
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Nome</label>
						<span class="ui-required"><em>(Obrigat�rio)</em></span>
						<s:textfield name="planoViewVO.nome" id="nome" data-required="Nome" cssClass="form-control"  maxlength="50" size="50"/>
					</div>	
			</div>	
		
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
					
						<label>Vig�ncia</label><span class="ui-required"><em> (Obrigat�rio)</em></span>
						<div>	
				        
			                <s:textfield name="planoViewVO.dataInicioVigenciaTela"   data-date-type="default"  size="12" 
			                data-required="Data In�cio de Vig�ncia" id ="dataInicioVigencia" cssClass="form-control" data-mask="data" style="width: 30%;">
				                <s:param name="value">
								    <s:date name="planoViewVO.dataInicioVigenciaTela" format="dd/MM/yyyy"/>
								  </s:param>
			                </s:textfield>
			
							   		&nbsp;<span>At�</span>&nbsp;
							<s:textfield name="planoViewVO.dataFimVigenciaTela"  data-date-type="default"  size="12"
							  data-required="Data Fim de Vig�ncia"  id="dataFimVigencia" cssClass="form-control" data-mask="data" style="width: 30%;">
							      <s:param name="value">
								    <s:date name="planoViewVO.dataFimVigenciaTela" format="dd/MM/yyyy"/>
								  </s:param>
							  </s:textfield>
							
						</div>
					</div>	
			</div>
			
			<div class="row">
				<div class="form-group col-md-4 " style="font-weight: 600;">
					<label>C�digo de Registro</label>
					<s:textfield name="planoViewVO.codigoRegistro" id="codigoRegistro" data-type="integer" cssClass="form-control"  maxlength="9" size="10"/>
				</div>	
			</div>
			
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Taxa (R$)</label>
						<s:textfield name="planoViewVO.valorPlanoVO.valorTaxa" id="valorTaxa" data-type="currency" cssClass="form-control" maxlength="6" size="6" style="width:20%"/>
					</div>	
			</div>
			
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Valor Desconto (R$)</label>
						<s:textfield name="planoViewVO.valorPlanoVO.valorDesconto" id="valorDesconto" data-type="currency"   cssClass="form-control" maxlength="6" size="6" style="width:20%"/>
					</div>	
			</div>
			
			
		</div><!-- fim box -->
		<div class="col-md-12 box">
			<h4 id="tipoDeCobranca">Valor do Plano</h4>
			
			<div class="row">
				<div class="form-group col-md-4 " style="font-weight: 600;">
					
					<label>Data In�cio do Valor</label>
					<div class="row"></div>
						<s:textfield name="planoViewVO.valorPlanoVO.dataInicio"   data-date-type="default"  size="12" 
	                data-required="Data In�cio do Valor" id ="dataInicioVigencia" disabled="true" cssClass="form-control" data-mask="data" style="width: 30%;">
		                <s:param name="value">
						    <s:date name="planoViewVO.valorPlanoVO.dataInicio" format="dd/MM/yyyy"/>
						  </s:param>
	                </s:textfield>							
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-4 " style="font-weight: 600;">
					<label>Tipo de Cobran�a </label>
					<span class="ui-required"><em>(Obrigat�rio)</em></span>
						<s:select name="planoViewVO.tipoCobrancaPlano" id="tipoCobranca"  cssClass="form-control" data-required="Tipo de Cobran�a" list="listaDeTipoCobranca" headerKey="0"  listKey="codigo" listValue="descricao"></s:select>							
				</div>
			</div>
		
			

			<div class="row " id="div_tipo_cobranca_mensal" >
				<div class="form-group col-md-12 " style="margin-left: 0px;">
					<h4 style="margin-left: -14px; padding-bottom: 0px;">Valor Mensal</h4>
					<br/>
					<div class="row col-md-3" style="margin-left: -15px;font-weight: 600;">	
			        	<label>Valor Titular(R$)</label><span class="ui-required mensal"><em> (Obrigat�rio)</em></span>
			        	<br/>
						<s:textfield maxlength="6" cssClass="form-control" size="12" name="planoViewVO.valorPlanoVO.valorMensalTitular" id="valorMensalTitular" data-required="Valor Mensal Titular" data-type="currency" style="width: 30%;"/>
						<br/><br/>
						<label>Valor Dependente(R$)</label><span class="ui-required mensal"><em> (Obrigat�rio)</em></span>
						<br/>
						<s:textfield maxlength="6" cssClass="form-control" size="12"  name="planoViewVO.valorPlanoVO.valorMensalDependente" id="valorMensalDependente" data-required="Valor Mensal Dependente" data-type="currency" style="width: 30%;"/>
					</div>
					<div class="col-md-8" style="font-weight: 600;">
						<label >Per�odo de Car�ncia</label><span class="ui-required mensal"><em> (Obrigat�rio)</em></span>
						<s:textarea cols="70" rows="4" maxlength="500" cssClass="form-control"  id="descricaoCarenciaPeriodoMensal" name="planoViewVO.descricaoCarenciaPeriodoMensal" data-required="Per�odo de Car�ncia Mensal" style="width: 90%;"></s:textarea>	
					</div>
				</div>
			</div>	<!--  fim div tipo cobran�a mensal -->
	
			<div class="row " id="div_tipo_cobranca_anual">
				<div class="form-group col-md-12 " style="margin-left: 0px;">
					<h4 style="margin-left: -14px; padding-bottom: 0px;">Valor Anual</h4>
					<br/>
					<div class="row col-md-3" style="margin-left: -15px; font-weight: 600;">	
			        	<label>Valor Titular(R$)</label><span class="ui-required anual"><em> (Obrigat�rio)</em></span>
						<br/>
						<s:textfield maxlength="7" cssClass="form-control" size="8" name="planoViewVO.valorPlanoVO.valorAnualTitular" id="valorAnualTitular" data-required="Valor Anual Titular" data-type="currency" style="width: 50%;"/>
						
						<br/><br/>
						<label>Valor Dependente(R$)</label><span class="ui-required anual"><em> (Obrigat�rio)</em></span>
						<br/>
						<s:textfield type="text" maxlength="7" cssClass="form-control" size="12"name="planoViewVO.valorPlanoVO.valorAnualDependente" id="valorAnualDependente" data-required="Valor Anual Dependente"  data-type="currency" style="width: 50%;"/>
					</div>	
					<div class="col-md-8" style="font-weight: 600;">
						<label >Per�odo de Car�ncia</label><span class="ui-required anual"><em> (Obrigat�rio)</em></span>
						<s:textarea cols="70" rows="4" maxlength="500" cssClass="form-control"  id="descricaoCarenciaPeriodoAnual" name="planoViewVO.descricaoCarenciaPeriodoAnual" data-required="Per�odo de Car�ncia Anual" style="width: 90%;"></s:textarea>	
					</div>
				</div>
			</div>	<!--  fim div tipo cobran�a anual -->		
			
			<div class="row ui-margin-bottom">
            	<div class="col-md-12" id="adicionar_novo_valor"> <a href="javascript:void(0);"><span class="glyphicon glyphicon-plus"></span> Adicionar Novo Valor do Plano</a> </div>
         	</div>
			
		</div><!--  fim do box -->
		
		
		<div class="col-md-12 box" id="div_novo_valor">
			<h4 id="novoValorPlano">Novo Valor do Plano</h4>
			
			
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Data In�cio do Novo Valor</label>
						<span class="ui-required"><em> (Obrigat�rio)</em></span>
						<br/>
							<s:textfield name="planoViewVO.dataInicioNovoValor"   data-date-type="default"  size="15" 
	                data-required="Data In�cio do Novo Valor" id ="dataProgramadaNovoValorPlano" cssClass="form-control" data-mask="data" style="width: 30%;">
		                <s:param name="value">
						    <s:date name="planoViewVO.dataInicioNovoValor" format="dd/MM/yyyy"/>
						  </s:param>
	                </s:textfield>
																				
													
					</div>
			</div>
			
			<div class="row">
					<div class="form-group col-md-4 " style="font-weight: 600;">
						<label>Tipo de Cobran�a </label>
						<span class="ui-required"><em> (Obrigat�rio)</em></span>
							<s:select name="planoViewVO.novoTipoCobrancaPlano" id="novoTipoCobranca"  cssClass="form-control" 
							 data-required="Tipo de Cobran�a" list="listaDeTipoCobranca" headerValue="Selecione um Tipo de Cobran�a"
						 headerKey=""  listKey="codigo" listValue="descricao" 
							 >
							
							</s:select>
																				
													
					</div>
			</div>
			
			<div class="row " id="nova_div_tipo_cobranca_mensal" >
				<div class="form-group col-md-12 " style="margin-left: 0px;">
				<h4 style="margin-left: -14px; padding-bottom: 0px;">Novo Valor Mensal</h4>
				<br/>
				<div class="row col-md-3" style="margin-left: -15px; font-weight: 600;">	
		        	<label>Valor Titular(R$)</label><span class="ui-required mensal"><em> (Obrigat�rio)</em></span>
		        	<br/>
					<s:textfield maxlength="6" cssClass="form-control" size="12"  name="planoViewVO.novoValorPlanoVO.valorMensalTitular" data-required="Valor Mensal Titular" data-type="currency" style="width: 50%;"/>
					<br/><br/>
					<label>Valor Dependente(R$)</label><span class="ui-required mensal"><em> (Obrigat�rio)</em></span>
					<s:textfield maxlength="6" cssClass="form-control" size="12"  name="planoViewVO.novoValorPlanoVO.valorMensalDependente" data-required="Valor Mensal Dependente" data-type="currency" style="width: 50%;"/>
				</div>
				<div class="col-md-8" style="font-weight: 600;">
					<label >Per�odo de Car�ncia</label><span id="labelNovaDescricaoCarenciaPeriodoMensal" class="ui-required anual"><em> (Obrigat�rio)</em></span>
					<s:textarea cols="70" rows="4" maxlength="500" id="novaDescricaoCarenciaPeriodoMensal" cssClass="form-control" name="planoViewVO.novaDescricaoCarenciaPeriodoMensal" data-required="Per�odo de Car�ncia Anual" style="width: 90%;"></s:textarea>	
				</div>
			</div>
		</div>	<!--  fim div tipo cobran�a mensal -->
	
		<div class="row " id="nova_div_tipo_cobranca_anual">
			<div class="form-group col-md-12 " style="margin-left: 0px;">
				<h4 style="margin-left: -14px; padding-bottom: 0px;">Novo Valor Anual</h4>
				<br/>
				<div class="row col-md-3" style="margin-left: -15px; font-weight: 600;">	
		        	<label>Valor Titular(R$)</label><span class="ui-required anual"><em> (Obrigat�rio)</em></span>
					<br/>
					<s:textfield maxlength="7" cssClass="form-control" size="12"  name="planoViewVO.novoValorPlanoVO.valorAnualTitular"  data-required="Valor Anual Titular" data-type="currency" style="width: 50%;"/>
					<br/><br/>
					<label>Valor Dependente(R$)</label><span class="ui-required anual"><em> (Obrigat�rio)</em></span>
					<br/>
					<s:textfield type="text" maxlength="7" cssClass="form-control" size="12"  name="planoViewVO.novoValorPlanoVO.valorAnualDependente" data-required="Valor Anual Dependente"  data-type="currency" style="width: 50%;"/>
				</div>	
				<div class="col-md-8" style="font-weight: 600;">
					<label >Per�odo de Car�ncia</label><span id="labelNovaDescricaoCarenciaPeriodoAnual" class="ui-required anual"><em> (Obrigat�rio)</em></span>
					<s:textarea cols="70" rows="4" maxlength="500" id="novaDescricaoCarenciaPeriodoAnual" cssClass="form-control" name="planoViewVO.novaDescricaoCarenciaPeriodoAnual"style="width: 90%;"></s:textarea>	
				</div>
				</div>
			</div>	<!--  fim div tipo cobran�a anual -->
			
			<div class="row ui-margin-bottom">
            	<div class="col-md-12" id="remover_novo_valor"> <a href="javascript:void(0);"><span class="glyphicon glyphicon-minus"></span> Remover Novo Valor do Plano</a> </div>
         	</div>
			
		</div><!--  fim do box -->
	

	 <div class="col-md-12 box">
			<h4 id="canalDeVenda">canais de venda</h4>
		
			<div class="row">			
				  <s:select multiple="true" id="multiselect" list="planoViewVO.listaDeCanais" listKey="codigo" listValue="nome" 			
				  name="listaDeCanaisSelecionados"  size="%{planoViewVO.listaDeCanais.size()}" ></s:select>   
				   
				<%-- <s:optiontransferselect 
				    
					list="planoViewVO.listaDeCanais"
					listKey="codigo" 
					listValue="nome"
					
					name="leftList"
					
					doubleList="planoViewVO.listaDeCanaisSelecionados"  
					doubleListValue="nome"
					doubleName="rightList" 
					
					headerKey="rightSelectNon" 
					headerValue="<-- Selecione Canal -->"
					doubleHeaderKey="leftSelectNon"
					doubleHeaderValue="<-- Selecione Canal -->" 
					
					allowUpDownOnLeft="false"
					allowUpDownOnRight="false"
					
					>
				</s:optiontransferselect> --%>		  			
			
	 		</div>
			
		</div><!-- fim box -->

	  	
  			
	   </div> <!-- fim box -->
	   
	   <div class="col-md-12 box">
			<h4>Segmentos</h4>
			<div class="row">
				  <s:checkboxlist name="planoViewVO.listaTipoSucursal" list="listaTipoSucursal" listKey="codigo" listValue="nome"/>
			</div>
		</div>
	   

	   </div><!-- fim container principal -->
			
		<br/><br/>
		<div class="row">
			<div align="center">
					<button type="button" class="btn btn-primary btn-sm ui-margin-bottom"  id="alterar">Alterar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="voltarConsultarPlano" onclick="location.href='../plano/listarPorFiltro.do';">Voltar</button>
			</div>
		</div>
		<br/><br/><br/><br/>
	
	</s:form>
	</body>
</html>