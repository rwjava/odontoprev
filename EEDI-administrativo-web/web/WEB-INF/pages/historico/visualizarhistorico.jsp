<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Hist�rico - Consultar" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/historico.js'/>"></script>		
		<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
		
		<script type="text/javascript">
		$(document).ready(function(){
			$('#abaHistorico').addClass('tabact');
			$('#abaPlano').removeClass('tabact');
			$('#abaCanal').removeClass('tabact');
			$('#tab_plano').removeClass('open');
			$('#tab_canal').removeClass('open');
			$('#tab_historico').addClass('open');
	
			$('#abaPlanoSub').hide();
		});
		</script>	
		
	</head>
<body>
	
	<s:form action="iniciarHistorico" id="form">
		<div class="container painel">
				<%--<jsp:include page="../menu.jsp"></jsp:include>--%>
				<%--<table style="width: 100%" class="tabela_interna">
					<tbody>
					<tr>
						<th colspan="7">Hist�rico</th>
					</tr>
					<tr>
						<td width="10%" class="td_dados" colspan="6"><label for="sigla">Usu�rio:</label></td>
						<td width="90%">	
							<s:textfield type="text" id="id" value="" maxlength="6" size="10" name="historicoVO.loginVO.id"/>
		
						</td>	
					</tr>
					<tr>
						<td width="10%" class="td_dados" colspan="6"><label for="nome">Per�odo:</label>
						</td>
							<td align="left" width="40%">
						         <s:textfield name="historicoVO.dataInicioPesquisa" id="dataInicioPesquisa" data-mask="data" data-date-type="default" size="12"/>
						   		 &nbsp;&nbsp;&nbsp;&nbsp;At�&nbsp;&nbsp;&nbsp;&nbsp;
							     <s:textfield name="historicoVO.dataFimPesquisa" id="dataFimPesquisa" data-mask="data" data-date-type="default" size="12"/>
						   </td>
					</tr>
			        	<%--<tr>
							<td  width="10%" class="td_dados" colspan="6">Canal de Venda:</td>
							<td  width="40%" align="left">	
								<s:select name="historicoVO.canalVendaVO.codigo" id="canal" value="" list="listaDeCanais" headerKey="0" headerValue="Selecione um Canal de Venda" listKey="codigo" listValue="nome" />
							</td>
						</tr>--%>
					
			<%-- </tbody>
			</table>--%>		
				<%--<table id="tabela_botoes" style="width: 100%">
					<tr>
						<td align="center" width="99%">
							<img src="<c:url value="/includes/images/bt_consultar.gif" />" id="consultarHistorico" style="cursor:pointer;cursor:hand;margin-left: 80px;"/>
							&nbsp;&nbsp;&nbsp;
							<img src="<c:url value="/includes/images/bt_limpar.gif" />" id="limpar" style="cursor:pointer;cursor:hand;"/>
						</td>
					</tr>
				</table>	--%>	
				
				<%--<div class="row">
						<div class="col-md-9">
							<div class="row ui-margin-bottom">
								<div class="col-md-12 box">
									<ul class="nav nav-tabs nav-tabs-primary">
										<li class="active" onclick="activeTab(this); $('#abaCanalSub').show(); $('#abaPlanoSub').hide();">
											<a href="javascript:void(0);">Canais de Venda</a>
										</li>
										<li onclick="activeTab(this); $('#abaCanalSub').hide(); $('#abaPlanoSub').show();">
											<a href="javascript:void(0);">Planos</a>
										</li>
										
									</ul>
								</div>
							 </div>
						</div>
						
				</div>--%>
				
					<div class="col-md-12 box" style="width:1700px; margin-left:-415px">
						<h4 id="plano">Hist�rico de Planos</h4>
						<div class="row">
								<div class="form-group col-md-4 " style="font-weight: 600;">
									<label>Respons�vel</label>									
									<s:select name="filtro.responsavel"  cssClass="form-control" id="canal"class="form-control"   list="historicoPlanoViewVO.listaUsuarios" headerKey="0" headerValue="Selecione um Respons�vel"  />
								   
								
								</div>	
						</div>	
					
						<div class="row">
								
								<div class="form-group col-md-4 " style="padding-left: 23px;font-weight: 600;">
									<label>Per�odo</label><span class="ui-required"></span>
									<div class="row">	
							        
						                <s:textfield name="filtro.dataInicioPeriodo"   data-date-type="default"  size="12" 
						                 id ="dataInicioPeriodo" cssClass="form-control" data-mask="data" style="width: 30%;">
							                <s:param name="value">
											    <s:date name="dataInicioPeriodo" format="dd/MM/yyyy"/>
											  </s:param>
						                </s:textfield>
						
										   		&nbsp;<span>At�</span>&nbsp;
										<s:textfield name="filtro.dataFimPeriodo"  data-date-type="default"  size="12"
										    id="dataFimPeriodo" cssClass="form-control" data-mask="data" style="width: 30%;">
										      <s:param name="value">
											    <s:date name="dataFimPeriodo" format="dd/MM/yyyy"/>
											  </s:param>
										  </s:textfield>
										
									</div>
								</div>
									
						</div>
						
					<div class="row">
							<div align="center">
									<button type="button" class="btn btn-primary btn-sm ui-margin-bottom"  id="consultarHistorico">Consultar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="limpar">Limpar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!-- 									<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="novo">Novo</button> -->
							</div>
					</div>	
					
					</div> <!-- fim col-md-12 box -->
			</div>
		
		<br/><br/><br/>
		
		<div class="col-md-12 box" style="width:1700px; margin-left:-275px">
			<h4 id="plano">Planos</h4>
			<div class="row">
			<display:table name="historicoPlanoViewVO.listaHistoricoPlanoVO" id="plano" style="width: 100%" class="table table-striped table-hover table-pendencias " requestURI="listarPorFiltro.do" pagesize="1000" >
				<display:setProperty name="basic.msg.empty_list"  value="Nenhum plano encontrado."/> 
				
				<display:column title="Data" style="width: 10%;"> 
				 	${plano.dataAlteracaoFormatada} 	
				</display:column>
				
				<display:column title="Plano" style="width: 10%;">
					${plano.descricao} 				
				</display:column>
				
				<display:column title="Tipo" style="width: 10%;">
					${plano.tipoAlteracao}				
				</display:column>
				
				<display:column title="Respons�vel" style="width: 10%;">			
	 				 ${plano.codigoResponsavelAtualizacao}			
				</display:column>
				
				<display:column title="Campo" style="width: 10%;">
					<c:forEach var="camposAlterados" items="${plano.listaCamposAlterados}" >
		 				 ${camposAlterados}<br/>
					</c:forEach>
				</display:column>
				
				<display:column title="Valor Antigo" style="width: 25%;">
					<c:forEach var="valorAntigo" items="${plano.listaValoresAntigos}" >
		 				 ${valorAntigo}<br/>
					</c:forEach>
				</display:column>
				
				<display:column title="Valor Novo" style="width: 25%;">
					<c:forEach var="valornovo" items="${plano.listaValoresNovos}" >
		 				 ${valornovo}<br/>
					</c:forEach>
				</display:column>	
				
										
			</display:table>
			
			
	
	
			</div>
			
			<div class="row">
						<div align="center">
								<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="voltarConsultarPlano" onclick="location.href='../plano/listarPorFiltro.do';">Voltar</button>
						</div>
			</div>
		</div> <!-- fim col-md-12 box -->
		
	</s:form>
	</body>
</html>