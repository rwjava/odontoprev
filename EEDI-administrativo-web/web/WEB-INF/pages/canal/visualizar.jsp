<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Canal de Venda - Visualizar" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#abaCanal').addClass('tabact');
			$('#abaPlano').removeClass('tabact');
			$('#abaCanal').addClass('tabact');
			$('#abaPlano').removeClass('tabact');
			$('#tab_plano').removeClass('open');
			$('#tab_canal').addClass('open');
			$('#tab_historico').removeClass('open');	
		});
		</script>
	</head>
<body>

<s:form action="incluir" id="form">
	<div class="container painel" /*style="width:1560px;*/">
		<div class="row">
			<!--  <h3 id="canal">Canal de Venda</h3> -->
			<div class="col-md-12 box">
				<h4>Dados do Canal de Venda</h4>
				<div class="row">
					<div class="form-group col-md-3 ">	
						<label>C�digo</label>
						<br>
						<p >
							<strong>
								<s:property value="canalVendaVO.codigo" />
							</strong>
						</p>
					</div>
				</div>
      				
      			<div class="row">
      				<div class="form-group col-md-3 ">
						<label>Nome</label>
						<br>
						<p>
							<strong>
								<s:property value="canalVendaVO.nome" />
							</strong>
						</p>
					</div>
      			</div>
      				
      			<div class="row">
      				<div class="form-group col-md-3 ">
						<label>Sigla</label>
						<br>
						<p>
							<strong>
								<s:property value="canalVendaVO.sigla" />
							</strong>
						</p>
					</div>
      			</div>
      					
      			<div class="row">
					<div align="center">
        				<button class="btn btn-primary ui-margin-bottom-sm"  id="voltarConsultarCanal">Voltar</button>
        			</div>
      			</div>
      		</div><!-- box -->
      	</div>
	</div>
		
	<!--  
	<div class="container painel" >
		<div class="row">
			<div class="col-md-12 box">
		    	<h4>Hist�rico</h4>
		        <div class="row">
		        	<div class="form-group col-md-12 ">
						<p >
							<strong>
								Usu�rio: x000000 Cadastrou: Canal 'SITE 100% CORRETOR' C�digo 169 no dia 00/00/2015 �s 00:00.
							</strong>
						</p>
					</div>
		        </div>
			</div>
		</div>
	</div>
    -->
</s:form>

	</body>
</html>