<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Canal de Venda - Cadastrar" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
			<script type="text/javascript">
		$(document).ready(function(){
			$('#abaCanal').addClass('tabact');
			$('#abaPlano').removeClass('tabact');
			$('#abaCanal').addClass('tabact');
			$('#abaPlano').removeClass('tabact');
			$('#tab_plano').removeClass('open');
			$('#tab_canal').addClass('open');
			$('#tab_historico').removeClass('open');	
			$('#sigla').val('');
			$('#nome').val('');
		});
		</script>	
	</head>
<body>


        
        <s:form action="incluirCanal" id="form">
        		<div class="container painel" /*style="width:1560px;"*/>
						<div class="row">
						
								<div class="col-md-12 box">
								<h4 id="canal">Dados para Inclus�o de Canal de Venda</h4>
								   <div class="row">
<!-- 									<div class="row"> -->
<!-- 											<div class="form-group col-md-4 "> -->
<!-- 												<label>C�digo do Canal</label> -->
<%-- 												<span class="ui-required"><em>(Obrigat�rio)</em></span> --%>
<%-- 												<s:textfield name="canalVendaViewVO.codigo" id="codigoDoCanal"  data-type="integer" cssClass="form-control" data-required="C�digo do Canal"   maxlength="2" size="50"/>			 --%>
<!-- 											</div> -->
<!-- 									 </div>row codigo -->
									 <div class="row">
											<div class="form-group col-md-4 ">
												<label>Nome</label>
												<span class="ui-required"><em>(Obrigat�rio)</em></span>
												<s:textfield name="canalVendaViewVO.nome" id="nome"cssClass="form-control" maxlength="50" size="50"/>
											</div>
									 </div><!-- row nome -->
									<div class="row">
										<div class="form-group col-md-4 ">
											<label>Sigla</label>
											<span class="ui-required"><em>(Obrigat�rio)</em></span>
											<s:textfield name="canalVendaViewVO.sigla" id="sigla" cssClass="form-control" data-required="Sigla"  maxlength="4" size="4"  style="width:20%"/>
										</div>
									 </div><!-- row sigla -->
									 
									 	<br/><br/><br/>
	
										<div class="row">
											<div align="center">
												<button type="button" class="btn btn-primary btn-sm ui-margin-bottom"  id="incluirCanal">Incluir</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="voltarConsultarCanal" >Voltar</button>
											</div>
										</div>
										<br/><br/><br/>
										
								</div>	<!--  fim box principal -->	
						</div><!-- fim row inclus�o de canal -->
						
				</div>
        
        </s:form>
        
   
	</body>
</html>