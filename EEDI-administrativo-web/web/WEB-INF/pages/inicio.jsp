<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta name="titulo" content="EEDI-Administrativo" />
<meta name="subtitulo" content="In�cio" />
</head>
<body>
<p class="destaque">Importante, esta funcionalidade � apenas uma
refer�ncia para o desenvolvimento, e deve ser removida da aplica��o.</p>
<p class="destaque">O pacote br.com.bradseg.eedi.administrativo.funcao deve ser removido antes de uma entrega de EAR.</p>
<br/>
<p class="destaque">Para testar, preencha com os nomes: 'bsp', ou 'bsad', ou 'fulano' e veja os diferentes comportamentos da aplica��o. Os nomes 'bsp' e 'bsad' geram respectivamente BusinessException e IntegrationException.</p>
<br/>
<table>
	<thead>
		<tr>
			<th>Nome</th><th>Efeito</th>
		</tr>
	</thead>
	<tbody>
		<tr><td>bsp</td><td>Ocorre de forma proposital, um erro de neg�cio na camada de servi�o, mas que ser� tratada pela action e exibida como mensagem de erro. (BusinessException)</td></tr>
		<tr><td>bsad</td><td>Ocorre de forma proposital, um erro de integra��o na camada de servi�o, que n�o ser� tratado por essa action, e ser� redirecionado para uma p�gina de erro. (IntegrationException)</td></tr>
		<tr><td>fulano</td><td>Exibe uma mensagem espec�fica para este nome.</td></tr>
		<tr><td>(qualquer outro nome)</td><td>Exibe uma mensagem padr�o para qualquer outro nome n�o cadastrado.</td></tr>
	</tbody>
</table>
<br/>

<s:form action="consultarSaudacao">
	<table width="30%" class="tabela_interna">
		<tr>
			<th colspan="2">Sauda��o</th>
		</tr>
		<tr>
			<td width="120">Nome: <span class="obrigatorio">*</span></td>
			<td><s:textfield name="nome" maxlength="200" size="30" /></td>
		</tr>
	</table>
	<table id="tabela_botoes" cellspacing="0" cellpadding="0" border="0"
		width="30%">
		<tr align="center">
		<td>
			<s:submit value="Consultar" cssClass="margem_botoes button" />
			</td>
		</tr>
	</table>
	
	<h1>WDEV - JS Plugins - Demo</h1>
	
	<table style="width: 60%" class="tabela_interna">
		<tr>
			<th colspan="2">M�scaras</th>
		</tr>
		<tr>
			<td>
				<p>Somente n�meros: <input type="text" data-type="integer" /></p>
			</td>
			<td>
				<p>Valores monet�rios: <input type="text" data-type="currency" /></p>
			</td>
		</tr>
		<tr>
			<td>
				<p>Sem caracteres especiais: <input type="text" data-type="text" /></p>
			</td>
			<td>
				<p>Data simples: <input type="text" data-mask="data" /></p>
			</td>
		</tr>
		<tr>
			<td>
				<p>Data com componente: <input type="text" data-mask="data" data-date-type="default" /></p>
			</td>
			<td>
				<p>Data simples com icone para executar componente: <input type="text" data-mask="data" data-date-type="botao-calendario" /></p>
			</td>
		</tr>
		<tr>
			<td>
				<p>CPF: <input type="text" data-mask="cpf"  /></p>
			</td>
			<td>
				<p>CEP: <input type="text" data-mask="cep" /></p>
			</td>
		</tr>
		<tr>
			<td>
				<p>Telefone: <input type="text" data-mask="telefone" /></p>
			</td>
			<td>
				<p>Celular (considerando 9� d�gito para DDD 11): <input type="text" data-mask="celular" /></p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p>Input File: <input type="file" name="anexo" /></p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p>Contagem textarea: <textarea maxlength="400" rows="6" cols="40"></textarea></p>
			</td>
		</tr>
	</table>
	
	<p/>
	
	<table style="width: 60%" class="tabela_interna">
		<tr>
			<th colspan="2">Valida��es</th>
		</tr>
		<tr>
			<td>
				<p><label for="numeros">Sem obrigatoriedade:</label> <input type="text" name="numeros" /></p>
			</td>
			<td>
				<p><label for="obrigatorio">Obrigat�rio:</label> <input type="text" name="obrigatorio" data-required="Teste obrigatoriedade" /></p>
			</td>
		</tr>
		<tr>
			<td>
				<p><label for="email">E-mail:</label> <input type="text" name="email" data-required="E-mail da pessoa" data-email="true" /></p>
			</td>
			<td>
				<p>
					<label for="sexo">Sexo:</label>
					<input type="radio" name="sexo" value="Masculino" data-required="Sexo" /> Masculino
					<input type="radio" name="sexo" value="Feminino" data-required="Sexo" /> Feminino
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p>
					<label for="estado">Teste Combo:</label>
					<select name="estado" data-required="Estado">
						<option value="">Selecione</option>
						<option value="RJ">RJ</option>
					</select>
				</p>
			</td>
			
		</tr>
		<tr>
			<td colspan="2">
				<p>
					<label for="componentes">Componentes utilizados:</label>
					<input type="checkbox" name="componentes" value="jQuery" data-required="Componentes utilizados" /> jQuery
					<input type="checkbox" name="componentes" value="Prototype" data-required="Componentes utilizados" /> Prototype
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p>
					<label for="justificativa">Justificativa:</label><br />
					<textarea name="justificativa" cols="20" rows="2" data-required="Justificativa"></textarea>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input name="submit" type="submit" value="Submit" />
			</td>
		</tr>
	</table>
	
</s:form>
</body>
</html>