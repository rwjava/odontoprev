<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Corretor - Consultar" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
	
		<script type="text/javascript">
		$(document).ready(function(){
			$('#abaPlano').removeClass('tabact');
			$('#tab_plano').removeClass('open');
			$('#tab_historico').removeClass('open');	
			$('#abaCanal').removeClass('tabact');
			$('#tab_canal').removeClass('open');
			$('#abaCorretor').addClass('tabact');
			$('#tab_corretor').addClass('open');
			
			//Bloqueia o CTRL + V  no input
			$("#sucursal").bind('paste', function(e) {
		        e.preventDefault();
		    });
		});
		</script>
	</head>
	<body>
		<s:form action="listarPorFiltroCorretor" id="form">
			<div class="container painel" >
				<div class="col-md-12 col-sm-12 col-xl-12 col-xs-12 col-lg-12 box">
					<h4 id="plano">Dados para Consulta de Corretor</h4>
					<div class="row">
					<br><br>
						<div class="row">
							<div id="formulario" style="margin: 0px 0px;" class="col-md-6 col-sm-6 col-xl-6 col-xs-6 col-lg-6">
								<label>SUCURSAL:</label>
								<span class="ui-required"><em>(Obrigat�rio)</em></span>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-address-card" aria-hidden="true"></i></span>
									<s:textfield name="cadastroCorretorViewVO.sucursal" id="sucursal" cssClass="form-control" placeholder="Informe a Sucursal" aria-describedby="sizing-addon2" size="35" />
								</div>
								<br>
								<label>STATUS:</label>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-check-square" aria-hidden="true"></i></span>
									<s:select name="cadastroCorretorViewVO.status" cssClass="form-control" id="status" value="cadastroCorretorViewVO.status" list="listaDeStatus" headerKey="0" headerValue="Selecione o status" listKey="codigo" listValue="descricao" >
									</s:select>	
								</div>								
									<br><br><br><br>
								<!-- Standard button -->
								<div class="row">
									<div align="center">
										<button type="button" class="btn btn-primary btn-sm ui-margin-bottom"  id="consultarSucursal">Consultar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="limparDadosCorretor">Limpar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="novoCorretor">Novo</button>
									</div>
								</div>
							</div>
							<div class="col-md-1 col-sm-1 col-xl-1 col-xs-1 col-lg-1 divisao"></div>
							<div id="descricao" style="" class="col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5">
								<br>
								<p style="text-align: justify; margin-top: -6px;">
									Prezado, aqui ser� explicado o funcionamento para realizar a consulta de corretor(es). Como pode ser observado, o formul�rio para consulta cont�m dois campos (1� - input e 2� - combobox), 'SUCURSAL' sendo seu preenchimento OBRIGAT�RIO
 									e 'STATUS com preenchimento opcional. Quando somente o campo 'SUCURSAL' � preenchido e realizado uma consulta, ser� retornado todos os corretores cadastrados para aquela SUCURSAL informada, independente 
 									do STATUS. Para filtrar por STATUS: Expirado, Programado e Vigente, ter� que ser informado a SUCURSAL e, selecionado no combobox um STATUS, logo ser�o retornados todos os corretores com o STATUS informado 
 									que est�o cadastrados naquela SUCURSAL.
								</p>
							</div>
						</div>
						<br><br>
				  	</div>
				</div>
					<br/><br/><br/>		
				<div class="col-md-12 col-sm-12 col-xl-12 col-xs-12 col-lg-12  box">
					<h4 id="canal">Corretores cadastrados</h4>
					<div class="row"><!-- lista de canais de venda -->
						<display:table name="listarCorretores" id="corretor" style="width: 100%" class="table table-striped table-hover table-pendencias " requestURI="listarPorFiltro.do" pagesize="100" >
							<display:setProperty name="basic.msg.empty_list"  value="Nenhum corretor cadastrado."/> 
							<display:column title="C�digo" style="width: 10%;">
								${corretor.cnpj} 
							</display:column>
							<display:column title="Nome" style="width: 35%;">
								 ${corretor.nome}	
							</display:column>
							<display:column title="Sucursal" style="width: 35%;">
				 				 ${corretor.sucursal}<br/>
							</display:column>
							<display:column title="Status" style="width: 15%;">
								 ${corretor.descricaoStatus}	
							</display:column>
							<display:column title="A��es" style="width:5%;">
								<div class="dropdown">
									<button class="btn btn-default btn-xs btn-block btn-default" data-toggle="dropdown">
										<span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>
									</button>							
									<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">		
										<li class="breaker">
											<s:a action="visualizar.do" namespace="/corretor">
												<s:param name="cadastroCorretorVO.cnpj">
													${corretor.cnpj}
												</s:param>
												<span>Visualizar</span>
											</s:a>  
										</li>	
										<!--<li>
											<s:a action="iniciarAlterar.do" namespace="/corretor">
												<s:param name="cadastroCorretorVO.cnpj">
													${corretor.cnpj}
												</s:param>
												<span>Alterar</span>
											</s:a>  
										</li>-->
										<s:if test="#attr.corretor.podeAlterarCorretor > 0">
										    <li>
												<s:a action="iniciarAlterar.do" namespace="/corretor">
													<s:param name="cadastroCorretorVO.cnpj">
														${corretor.cnpj}
													</s:param>
													<span>Alterar</span>
												</s:a>	
										    </li>
									    </s:if>
									    <li>
											<s:a action="iniciarHistoricoCorretor.do" namespace="/historico">
												<s:param name="cadastroCorretorVO.cnpj">
													${corretor.cnpj}
												</s:param>
												<span>Hist�rico</span>
											</s:a>
										</li>
									</ul>
								</div>			
							</display:column>
						</display:table>
					</div><!-- fim row lista de canais de venda -->
				</div>
			</div><!-- fim container painel -->
		</s:form>
	</body>
</html>