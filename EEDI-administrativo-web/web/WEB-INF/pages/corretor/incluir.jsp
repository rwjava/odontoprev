<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Corretor - Incluir" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
	
		<script type="text/javascript">
		$(document).ready(function(){
			$('#abaPlano').removeClass('tabact');
			$('#tab_plano').removeClass('open');
			$('#tab_historico').removeClass('open');	
			$('#abaCanal').removeClass('tabact');
			$('#tab_canal').removeClass('open');
			$('#abaCorretor').addClass('tabact');
			$('#tab_corretor').addClass('open');
			
			//Bloqueia o CTRL + V  no input
			$("#cnpjCpf").bind('paste', function(e) {
		        e.preventDefault();
		    });
			$("#sucursal").bind('paste', function(e) {
		        e.preventDefault();
		    });
			$("#nome").bind('paste', function(e) {
		        e.preventDefault();
		    });
			$("#datetimepicker4").bind('paste', function(e) {
		        e.preventDefault();
		    });
			$("#datetimepickerFim").bind('paste', function(e) {
		        e.preventDefault();
		    });
			
			
		});
		</script>
	</head>
	<body>
		<s:form action="listarPorFiltroCorretor" id="form">
			<div class="container painel" >
				<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xs-12 box">
					<h4 id="plano">DADOS PARA INCLUS�O DE CORRETOR</h4>
					<div class="row">
					<br><br>
						<div class="row">
							<div id="formulario" style="margin: 0px 0px;" class="col-sm-6 col-md-6 col-lg-6 col-xl-6 col-xs-6">
							
								<label>CNPJ/CPF:</label>
								<span class="ui-required"><em>(Obrigat�rio)</em></span>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-address-card" aria-hidden="true"></i></span>
									<s:textfield name="cadastroCorretorViewVO.cnpj" id="cnpjCpf" cssClass="form-control" placeholder="Informe o CNPJ ou CPF" aria-describedby="sizing-addon2" size="35"/>
								</div><br>
								
								<label>SUCURSAL:</label>
								<span class="ui-required"><em>(Obrigat�rio)</em></span>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-check-square" aria-hidden="true"></i></span>
									<s:textfield name="cadastroCorretorViewVO.sucursal" id="sucursal" cssClass="form-control" placeholder="Informe a Sucursal" aria-describedby="sizing-addon2" size="35"/>
								</div><br>
								
								<label>NOME:</label>
								<span class="ui-required"><em>(Obrigat�rio)</em></span>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-user" aria-hidden="true"></i></span>
									<s:textfield name="cadastroCorretorViewVO.nome" id="nome" cssClass="form-control" placeholder="Informe o Nome" aria-describedby="sizing-addon2" size="80" onkeypress="return soLetras();" style="text-transform: uppercase;"/>
								</div><br>
								
								<div class="form-group col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" style="margin: 0 auto; padding-left: 0px;">
									<label>DATA INICIO:</label>
									<span class="ui-required"><em>(Obrigat�rio)</em></span>
									<div class="input-group" style="width: 250px;">
										<span class="input-group-addon" id="" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										<s:textfield name="cadastroCorretorViewVO.dataInicioContratoTela"  data-date-type="default"  size="12"  id="dataInicioVigencia" cssClass="form-control" data-mask="data" />
									</div>
								</div>	
								
								<div class="form-group col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xs-1" style="width: 48px; margin-left: 20px">
									<label style="margin-top: 27px;">AT�</label>
								</div>	
								
								<div class="form-group col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5" style="margin: 0 auto; padding-left: 0px;">
									<label>DATA FIM:</label>
									<span class="ui-required"><em>(Obrigat�rio)</em></span>
									<div class="input-group" style="width: 250px;">
										<span class="input-group-addon" id="" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										<s:textfield name="cadastroCorretorViewVO.dataFimContratoTela"  data-date-type="default"  size="12" id="dataFimVigencia" cssClass="form-control" data-mask="data" style=""/>
									</div>
								</div>		
				
								<br><br><br><br>
									
								<br><br>
									<!-- Standard button -->
									<div class="row">
										<div align="center">
											<button type="button" class="btn btn-primary btn-sm ui-margin-bottom"  id="incluirCorretor">Incluir</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="voltarConsultarCorretor">Voltar</button>
										</div>
									</div>
							</div>
							<div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xs-1 divisao"></div>
							<div id="descricao" style="" class="col-sm-5 col-md-5 col-lg-5 col-xl-5 col-xs-5">
								<br>
								<p style="text-align: justify;">
									Prezados, est� tela de inclus�o � composta por 5 (Cinco) input's e todos eles obrigat�rios.<br><br>
									Abaixo ser� descrito detalhadamente cada input.<br><br>
									1� - CNPJ/CPF    - Somente n�meros.<br>
									2� - SUCURSAL    - Somente n�meros.<br>
									3� - NOME        - Somente letras<br>
									4� - DATA INICIO - � disponibilizado um calendario para a escolha da data.<br>
									5� - DATA FIM    - � disponibilizado um calendario para a escolha da data.<br><br>
									Observa��o: Nenhum input est� habilitado a receber o comando Ctrl + v.<br>
								</p>
							</div>
						</div>
						<br><br>
				  	</div>
				</div>
				<br/><br/><br/>		
			</div><!-- fim container painel -->
		</s:form>
	</body>
</html>