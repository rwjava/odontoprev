<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Corretor - Visualizar" />
		
		<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#abaPlano').removeClass('tabact');
			$('#tab_plano').removeClass('open');
			$('#tab_historico').removeClass('open');	
			$('#abaCanal').removeClass('tabact');
			$('#tab_canal').removeClass('open');
			$('#abaCorretor').addClass('tabact');
			$('#tab_corretor').addClass('open');
		});
		</script>
	</head>
	<body>
		<s:form action="incluir" id="form">
			<div class="container painel">
				<div class="row">
					<!--  <h3 id="canal">Canal de Venda</h3> -->
					<div class="col-md-12 col-sm-12 col-xl-12 col-xs-12 col-lg-12 box">
						<h4>Dados do Corretor</h4>
						<div class="row">
							<div class="form-group col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5">	
								<label>CNPJ/CPF:</label>
								<br>
								<p >
									<strong>
										<s:property value="cadastroCorretorViewVO.cnpj" />
									</strong>
								</p>
							</div>
						</div>
		      				
		      			<div class="row">
		      				<div class="form-group col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5">
								<label>SUCURSAL:</label>
								<br>
								<p>
									<strong>
										<s:property value="cadastroCorretorViewVO.sucursal" />
									</strong>
								</p>
							</div>
		      			</div>
		      				
		      			<div class="row">
		      				<div class="form-group col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5">
								<label>NOME:</label>
								<br>
								<p>
									<strong>
										<s:property value="cadastroCorretorViewVO.nome" />
									</strong>
								</p>
							</div>
		      			</div>
		      			
		      			<div class="row">
		      				<div class="form-group col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5">
								<label>DATA INICIO:</label>
								<br>
								<p>
									<strong>
										<s:property value="cadastroCorretorViewVO.dataInicioContratoTela" />
									</strong>
								</p>
							</div>
		      			</div>
		      			
		      			<div class="row">
		      				<div class="form-group col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5">
								<label>DATA FIM:</label>
								<br>
								<p>
									<strong>
										<s:property value="cadastroCorretorViewVO.dataFimContratoTela" />
									</strong>
								</p>
							</div>
		      			</div>
		    
		      			<div class="row">
		      				<div class="form-group col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5">
								<label>STATUS DESCRI��O:</label>
								<br>
								<p>
									<strong>
										<s:property value="cadastroCorretorViewVO.descricaoStatus" />
									</strong>
								</p>
							</div>
		      			</div>
		      					
		      			<div class="row">
							<div align="center">
		        				<button class="btn btn-primary ui-margin-bottom-sm"  id="voltarConsultarCorretor">Voltar</button>
		        			</div>
		      			</div>
		      			
		      		</div><!-- box -->
		      	</div>
			</div>
				
			<!--<div class="container painel" >
				<div class="row">
					<div class="col-md-12 box">
				    	<h4>Hist�rico</h4>
				        <div class="row">
				        	<div class="form-group col-md-12 ">
								<p >
									<strong>
										Usu�rio: x000000 Cadastrou: Canal 'SITE 100% CORRETOR' C�digo 169 no dia 00/00/2015 �s 00:00.
									</strong>
								</p>
							</div>
				        </div>
					</div>
				</div>
			</div>-->
		</s:form>
	</body>
</html>