<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<html>
	<head>
		<meta name="subtitulo" content="Corretor - Cadastro" />
    	<script type="text/javascript" src="<s:url value='/includes/js/navegacao.js'/>"></script>
    	<script type="text/javascript" src="<s:url value='/includes/js/altera.js'/>"></script> 
    	
		<script type="text/javascript">
			$(document).ready(function(){
				$('#abaPlano').removeClass('tabact');
				$('#tab_plano').removeClass('open');
				$('#tab_historico').removeClass('open');	
				$('#abaCanal').removeClass('tabact');
				$('#tab_canal').removeClass('open');
				$('#abaCorretor').addClass('tabact');
				$('#tab_corretor').addClass('open');
				
				$('#multiselect').multiselect();
				$('#multiselectOculto').multiselect();		
				    
			    $("#multiselect > option").each(function() {
					var codigoCanal = this.value;
				   		//alert('multiselect '+ this.text + ' ' + this.value);
				        
						$("#multiselectOculto > option").each(function() {
					        //alert('multiselectOculto '+this.text + ' ' + this.value);					        
					        if(codigoCanal == this.value){
					        
					        	$('li input').each(function(){
							    	
					        		if(this.value == codigoCanal){
							    		//alert('ola'+ this.value);
							    		this.click();
					        		}
						    	});							
							}
						});					        
					});		
				});
		</script>
	</head>
	
	<body>
		<s:form action="incluir" id="form">
			<div class="container painel" >
				<div class="col-md-12 col-sm-12 col-xl-12 col-xs-12 col-lg-12 box">
					<h4 id="plano">DADOS PARA ALTERA��O DO CORRETOR</h4>
					<div class="row"><br><br>
						<div class="row">
						
							<div id="formulario" style="margin: 0px 0px;" class="col-md-6 col-sm-6 col-xl-6 col-xs-6 col-lg-6">
								<label>CNPJ/CPF:</label>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-address-card" aria-hidden="true"></i></span>
									<s:textfield name="cadastroCorretorViewVO.cnpj" cssClass="form-control" aria-describedby="sizing-addon2" size="35" readonly="true" class="readonly" />
								</div><br>
								
								<label>SUCURSAL:</label>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-check-square" aria-hidden="true"></i></span>
									<s:textfield name="cadastroCorretorViewVO.sucursal" cssClass="form-control" placeholder="Informe a Sucursal" aria-describedby="sizing-addon2" size="35" readonly="true" class="readonly" />
								</div><br>
								
								<label>NOME:</label>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-user" aria-hidden="true"></i></span>
									<s:textfield name="cadastroCorretorViewVO.nome" cssClass="form-control" placeholder="Informe o Nome" aria-describedby="sizing-addon2" size="35" readonly="true" class="readonly" />
								</div><br>
								
								<div class="form-group col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5" style="margin: 0 auto; padding-left: 0px;">
									<label>DATA INICIO:</label>
									<span class="ui-required"><em>(Obrigat�rio)</em></span>
									<div class="input-group" style="width: 250px;">
										<span class="input-group-addon" id="" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										<s:textfield name="cadastroCorretorViewVO.dataInicioContratoTela"  data-date-type="default"  size="12" data-required="Data Fim de Vig�ncia"  id="dataInicioVigencia" cssClass="form-control" data-mask="data" readonly="true" class="readonly">
									      	<s:param name="value">
										    	<s:date name="cadastroCorretorViewVO.dataInicioContratoTela" format="dd/MM/yyyy"/>
											</s:param>
										</s:textfield>
									</div>
								</div>	
								
								<div class="form-group col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xs-1" style="width: 48px; margin-left: 20px">
									<label style="margin-top: 27px;">AT�</label>
								</div>	
								
								<div class="form-group col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5" style="margin: 0 auto; padding-left: 0px;">
									<label>DATA FIM:</label>
									<span class="ui-required"><em>(Obrigat�rio)</em></span>
									<div class="input-group" style="width: 250px;">
										<span class="input-group-addon" id="" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										<s:textfield name="cadastroCorretorViewVO.dataFimContratoTela"  data-date-type="default"  size="12" data-required="Data Fim de Vig�ncia"  id="dataFimVigencia" cssClass="form-control" data-mask="data" style="">
									      	<s:param name="value">
										    	<s:date name="cadastroCorretorViewVO.dataFimContratoTela" format="dd/MM/yyyy"/>
											</s:param>
										</s:textfield>
									</div>
								</div>					
								<br><br><br><br>
								<label>STATUS:</label>
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="background-color: #00539F; color: #FFFFFF;"><i class="fa fa-check-square" aria-hidden="true"></i></span>
									<s:select name="cadastroCorretorViewVO.status" cssClass="form-control" id="status" value="cadastroCorretorViewVO.status" list="listaDeStatus" headerKey="0" listKey="codigo" listValue="descricao" >
									</s:select>	
								</div><br>
								
								<script type="text/javascript">
								$(function() {
									$('#datetimepicker4').datetimepicker();
								});
								$(function() {
									$('#datetimepickerFim').datetimepicker();
								});
								</script>
							</div>
							<div class="col-md-1 col-sm-1 col-xl-1 col-xs-1 col-lg-1 divisao">
							</div>
							<div id="descricao" style="" class="col-md-5 col-sm-5 col-xl-5 col-xs-5 col-lg-5">
								<br>
								<p style="text-align: justify; margin-top: -6px;">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet viverra nibh, ac tincidunt leo. Donec ut massa varius, posuere eros vel, egestas nibh. Nullam purus est, molestie vel diam eu, congue varius ipsum. Etiam sit amet sapien massa. Vestibulum convallis sem non magna vehicula sollicitudin. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin eu erat ornare nulla sollicitudin suscipit et id lacus. Sed suscipit commodo elementum. Cras facilisis tincidunt nisl. Maecenas malesuada vulputate purus, in euismod nisi. Proin sodales porttitor orci at blandit. Vivamus consectetur eu orci ac vestibulum.
									Suspendisse eu nunc est. Integer sem mauris, feugiat non nunc iaculis, accumsan posuere risus. Aliquam sollicitudin fringilla dui sit amet molestie.
								</p>
							</div>
						</div><br><br>
				  	</div>
				</div><br/><br/><br/>		
			</div><!-- fim container principal --><br/><br/>
			
			<div class="row">
				<div align="center">
					<button type="button" class="btn btn-primary btn-sm ui-margin-bottom"  id="alterarCorretor">Alterar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="button" class="btn btn-primary btn-sm ui-margin-bottom" id="voltarConsultarCorretor" onclick="location.href='../corretor/listarPorFiltro.do';">Voltar</button>
				</div>
			</div><br/><br/><br/><br/>
		</s:form>
	</body>
</html>