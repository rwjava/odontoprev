<script type="text/javascript">
		function marcarCanalVenda(){
			
			$('#abaPlano').removeClass('tabact');
			$('#abaHistorico').removeClass('tabact');
			$('#abaCorretor').removeClass('tabact');
			$('#abaCanal').addClass('tabact');

		};
		
		function marcarPlano(){
			$('#abaCanal').removeClass('tabact');
			$('#abaHistorico').removeClass('tabact');
			$('#abaCorretor').removeClass('tabact');
			$('#abaPlano').addClass('tabact');
		};
		
		function marcarAbaCorretor(){
			$('#abaCanal').removeClass('tabact');
			$('#abaHistorico').removeClass('tabact');
			$('#abaPlano').removeClass('tabact');
			$('#abaCorretor').addClass('tabact');
		};		
		
		function marcarHistorico(){
			$('#abaPlano').removeClass('tabact');
			$('#abaCanal').removeClass('tabact');
			$('#abaCorretor').removeClass('tabact');
			$('#abaHistorico').addClass('tabact');
		};
		
		</script>

<table width="100%" id="tabela_interna">
	<div class="tabbar">
		<ul>
			<li style="text-align: center;"><a href="../plano/listarPorFiltro.do" id ="abaPlano" onclick='marcarPlano()'>Plano</a></li>
			<li><a href="../canal/listarPorFiltroCanal.do" id="abaCanal" onclick='marcarCanalVenda()'>Canal de Venda</a></li>
			<li><a href="../cadastro/listarPorFiltroCorretor.do" id="abaCorretor" onclick='marcarAbaCorretor()'>Cadastro Corretor</a></li>
			<li><a href="../historico/iniciarHistorico.do" id="abaHistorico" onclick='marcarHistorico()'>Histórico</a></li>
<!-- 		<li><a href="../plano/iniciarAssociarPlanoCanal.do" class="tabact">Associar</a></li> -->
<!-- 			<li><a href="#" >Listar Propostas</a></li> -->
<!-- 			<li><a href="#" >Relatório de Acompanhamento</a></li> -->
		</ul>
	</div>
</table>