<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title><decorator:title default="EEDI - EMISS�O EXPRESSA DENTAL INDIVIDUAL"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- External styles -->
	<link type="text/css" rel="stylesheet" href="<s:url value='/includes/css/vendor/jquery-ui.min.css'/>" />
	
	<!-- Bradesco styles -->
	<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/master.css'/>" />
	<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/tabs.css'/>" />
       
	
	<!-- WDEV styles -->
	<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/wdev-required.css'/>" />

	
	    <!-- JS NOVOS -->    
        <script type="text/javascript" src="<s:url value='/includes/js/lib.js'/>"></script>
        <script type="text/javascript" src="<s:url value='/includes/js/bootstrap.js'/>"></script>
        <script type="text/javascript" src="<s:url value='/includes/js/iframeResizer.contentWindow.min.js'/>"></script>
        <script type="text/javascript" src="<s:url value='/includes/js/jquery-ui-1.10.1.custom.min.js'/>"></script>
        <script type="text/javascript" src="<s:url value='/includes/js/scriptPrototipo.js'/>"></script>
        <script type="text/javascript" src="<s:url value='/includes/js/actions.js'/>"></script>
   
   <!--[if IE]>
   <link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/ie8css.css'/>"  />
   <![endif]-->
	
	<!--[if gte IE 9]><!-->
        <!-- link rel="stylesheet" media="screen" href="<s:url value='includes/css/glyphicons/glyphicons.min.css'/>"/-->
<!--<![endif]-->
	
     <!-- BOOSTRAP CSS-->      
       <link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/custom.css'/>"/>
       <link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/bootstrap.css'/>"/>	

	   <link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/jquery-ui-1.10.3.custom.min.css'/>"/>
       <link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/components.css'/>"/>
       
       <!-- CSS FONT-AWESOME -->
       <link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/font-awesome-4.7.0/css/font-awesome.min.css'/>">
       
       <!-- CSS BOOTSTRAP DATA PICKER -->
       <link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/bootstrap-datetimepicker.min.css'/>"/>

	
	<!--[if IE 9]>
        <html class="ie9">
        <![endif]-->
        <!--[if IE 8]>
        <html class="ie8">
        <![endif]-->
        <!--[if IE 7]>
        <html class="ie7">
        <![endif]-->
        <!--[if IE 6]>
        <html class="ie6">
        <![endif]-->
        <!--[if !IE]>
        <html>
        <![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script type="text/javascript" src="<s:url value='/includes/js/html5shiv.js'/>"></script>
     	<script type="text/javascript" src="<s:url value='/includes/js/respond.min.js'/>"></script>
		<![endif]-->
	
        


	<!-- External plugins -->
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.inputmask.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-ui.custom.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.ui.datepicker-pt-BR.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.blockUI.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.placeholder.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-maskmoney.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-url.js'/>"></script>
	
	<script type="text/javascript" src="<s:url value='/includes/js/bootstrap-multiselect.js'/>"></script>	
	<script type="text/javascript" src="<s:url value='/includes/css/bootstrap-multiselect.css'/>"></script>
	<!-- script type="text/javascript" src="<s:url value='/includes/css/custom2.css'/>"></script-->
		  <!-- WDEV plugins -->
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-datatypes.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-ajax.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-required.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-inputcontrol.js'/>"></script>
		
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-block.js'/>"></script>
	
	<!-- JavaScript Moment.js para o datapicker -->
	<script type="text/javascript" src="<s:url value='/includes/js/moment.min.js'/>"></script>
	
	<!-- JavaScript data picker -->
	<script type="text/javascript" src="<s:url value='/includes/js/bootstrap-datetimepicker.min.js'/>"></script>
	
	<s:url var="baseURL" value="/"/>
	<script>window.baseURL = "${baseURL}";</script>
	
	<decorator:head />
</head>
<body>
<div class="mainNav">
    <div class="container">
        <header class="navbar navbar-default" role="navigation">
            <div class="container painel">
                <div class="col-md-12" >
                    <ul class="nav navbar-nav">            
                        <li id="tab_plano" onclick="activeTab(this);" style="width: 150px; text-align: center;">
                            <a href="#"   onclick="location.href='/EEDI-Administrativo/plano/listarPorFiltro.do';"  id ="abaPlano" class="dropdown-toggle">Plano</a>
                        </li>
                        <li id="tab_canal"  onclick="activeTab(this);" style="width: 150px;text-align: center;">
                            <a href="#"  onclick="location.href='/EEDI-Administrativo/canal/listarPorFiltroCanal.do';"  id="abaCanal" class="dropdown-toggle">Canal de Venda</a>
                        </li>
                        <li id="tab_corretor"  onclick="activeTab(this);" style="width: 200px;text-align: center;">
                            <a href="#"  onclick="location.href='/EEDI-Administrativo/corretor/listarPorFiltroCorretor.do';"  id="abaCorretor" class="dropdown-toggle">Cadastro Corretor</a>
                        </li>
                        <li id="tab_historico" onclick="activeTab(this);" style="width: 150px;text-align: center;">
                            <a href="#"  onclick="location.href='../historico/listarPorFiltro.do';"  id="abaHistorico"     class="dropdown-toggle" >Hist�rico</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    </div>
</div>
<div class="container painel" >
	<table id="table box">
		<tr>
			<td>
				<s:if test="hasFieldErrors()">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="alert alert-danger" role="alert" id="msgErros">
						<tbody>
							<tr>
								<td><s:iterator value="fieldErrors">
									<li><s:property value="value[0]" /></li>
								</s:iterator></td>
							</tr>
						</tbody>
					</table>
				</s:if>
				<s:if test="hasActionErrors()">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="alert alert-danger" id="msgErros">
						<tbody>
							<tr>
								<td><s:iterator value="actionErrors">
									<li><s:property /></li>
								</s:iterator></td>
							</tr>
						</tbody>
					</table>
				</s:if>
				<s:if test="hasActionMessages()">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="alert alert-success" id="msgSucesso">
						<tbody>
							<tr>
								<td><s:iterator value="actionMessages">
									<li><s:property /></li>
								</s:iterator></td>
							</tr>
						</tbody>
					</table>
				</s:if>
				<decorator:body />
			</td>
		</tr>
	</table>
</div>
<s:if test="hasActionMessages()">
	<script>
		$("#msgSucesso").fadeOut(10000);
	</script>
</s:if>

</body>
</html>