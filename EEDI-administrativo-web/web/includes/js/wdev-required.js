/**
 * Adiciona o asteristico para todos os campos obrigatorios e faz sua validacao (se foi preenchido):
 */
 $(document).ready(function(){
	/**
	 * Adiciona uma div para a listagem dos erros de validacao acima do form.
	 */
	 $("form").before("<div id='list-errors' class='tbl_msg_erro' style='display: none;'></div><br/>");
	
	/**
	 * Adiciona nos campos obrigat�rios um * para indicar sua obrigatoriedade.
	 */
	$("[data-required]").each(function(){
		var input = $(this);
		var required = input.attr('data-required');
		var obrigatoriedade = "<span class='obrigatorio'>*</span>";
		var label = $("label[for="+input.attr('id')+"]");
//		var width = input.css('width');
//		var height = input.css('height');
//		var idError = 'required-'+input.attr('id');
		
		if(required != "" && label.text().charAt(label.text().length-1) != "*"){
			label.append(obrigatoriedade);
		}
		
		input.bind('blur', function(){
			if(input.val() == "" || input.val() == 0 || input.val() == '0,00'){
				input.addClass('required');
			} else {
				input.removeClass('required');
			}
		});
	});
	
	/**
	 * Valida se o e-mail � inv�lido
	 */
	$("[data-email=true]").each(function(){
		var input = $(this);
		
		input.bind('keypress', function(){
			var value = input.val();
			var valueLength = value.length;
			
			if(valueLength >= 7){
				var emailFilter=/^.+@.+\..{2,}$/;
				var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/;
				
				if(!(emailFilter.test(value))||value.match(illegalChars)){
					input.addClass('required');
				} else {
					input.removeClass('required');
				}
			}
		});
		
		input.bind('blur', function(){
			var value = input.val();
			
			var emailFilter=/^.+@.+\..{2,}$/;
			var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/;
			
			if(!(emailFilter.test(value))||value.match(illegalChars)){
				input.addClass('required');
			} else {
				input.removeClass('required');
			}
		});
	});
	
	/**
	 * Verifica se os campos com o atributo data-required foram preenchidos.
	 */
	 $("form").submit(function(){
		var hasError = false;
		var listErrors = "";
		var errors = $("div#list-errors");
		
		$("[data-required]").each(function(){
			var input = $(this);
			var type = input.attr('type');
			var required = input.attr('data-required');
			var value = input.val();
			//var label = $("label[for="+input.attr('id')+"]");
			
			// Verifica o valor de campos do tipo Radio e Checkbox
			if(type == 'radio' || type == 'checkbox') {
				var checkedValue = $('input[name=' + input.attr('id') + ']:checked').val();
				value = checkedValue == undefined ? '' : checkedValue;
			}
			
			if(required != "") {
				if((value == "" || value == 0)){
					if(listErrors.indexOf(required) < 1){
						input.addClass('required');
						listErrors += "<li>O campo " + required + " � obrigat�rio.</li>";
						hasError = true;
					}
				}else if(value == '0,00'){
					if(listErrors.indexOf(required) < 1){
						input.addClass('required');
						listErrors += "<li>O campo " + required + " n�o pode ser 0,00.</li>";
						hasError = true;
					}
				} else {
					input.removeClass('required');
				}
			}
		});
		
		/**
		 * Verifica se os campos com o atributo data-email="true" est�o com o valor no padr�o nome@dominio.com
		 */
		$("[data-email=true]").each(function(){
			var input = $(this);
			var required = input.attr('data-required');
		
			var value = input.val();
			//var valueLength = value.length;
			var emailFilter=/^.+@.+\..{2,}$/;
			var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/;
			
			if(value != '') {
				if(!(emailFilter.test(value))||value.match(illegalChars)){
					input.addClass('required');
					listErrors += "<li>" + required + " � inv�lido.</li>";
					hasError = true;
				} else {
					input.removeClass('required');
				}
			}
		});
		
		if(hasError){
			errors.show();
			errors.html(listErrors);
			$('msgErros').hide();
			return !hasError;
		}
	 });
 });
 
jQuery.fn.removerAtributosRequired = function(){
	$("[data-required]").each(function(){
		var input = $(this);
		input.removeAttr('data-required');
	});
		

};
