$(document).ready(function(){
	
	/**
	 * Urls usadas no js de plano. 
	 */
	$.url.add({'urlNovo':'/plano/iniciarIncluir.do'});
	$.url.add({'urlIncluir':'/plano/incluir.do'});
	
	$.url.add({'urlAlterar':'/plano/alterar.do'});
	$.url.add({'urlAssociar':'/plano/associarPlanoCanal.do'});	
	$.url.add({'urlIniciarIncluirCanal':'/canal/iniciarIncluirCanal.do'});
	$.url.add({'urlIncluirCanal':'/canal/incluirCanal.do'});
	$.url.add({'urlConsultarCanal':'/canal/listarPorFiltroCanal.do'});
	$.url.add({'urlAlterarCanal':'/canal/alterarCanal.do'});
	$.url.add({'urlVoltarCanal':'/canal/listarCanais.do'});
	$.url.add({'verificarSeTemVigenciasAmarradas':'/plano/verificarSeTemVigenciasAmarradas.do'});
	

	
	
	$.url.add({'urlRecarregarListaCanais':'/plano/recarregarListaCanais.do'});

	
	
	
	/**
	 * 
	 */
	$('#consultarHistorico').click(function() {
		$('#form').attr('action', $.url.get('urlConsultarHistorico'));
		$('#form').submit();
	});
	
	/**
	 * Fun��o para limpar os campos do filtro da tela de consulta de plano.
	 */
	$('#limpar').click(function() {
		$('#id').val('');
		$('#dataInicioPesquisa').val('');
		$('#dataFimPesquisa').val('');
		$('#canal').val('0');
	});
	
	/**
	 * Fun��o que redireciona a aplica��o para o cadastro de um novo plano.
	 */
	$('#novo').click(function() {
		$("#form").attr('action', $.url.get('urlNovo'));
		$('#form').submit();
	});

	
	/**
	 * Fun��o que redireciona a aplica��o para o cadastro de um novo canal de venda.
	 */
	$('#novoCanal').click(function() {
		$("#form").attr('action', $.url.get('urlIniciarIncluirCanal'));
		$('#form').submit();
	});
	
	
	/**
	 * Fun��o que redireciona a aplica��o para a associação de plano e canal.
	 */
	$('#associar').click(function() {
		$("#form").attr('action', $.url.get('urlAssociar'));
		$('#form').submit();
	});	

	/**
	 * 
	 */
	$('#incluir').click(function() {
		$('#form').attr('action', $.url.get('urlIncluir'));
		$('#form').submit();
	});
	
	
	/**
	 * 
	 */
	$('#incluirCanal').click(function() {
		$('#form').attr('action', $.url.get('urlIncluirCanal'));
		$('#form').submit();
	});
	

	
	/**
	 * 
	 */
	$('#alterar').click(function() {
		$('#form').attr('action', $.url.get('urlAlterar'));
		 
		
		  /**
		  * TODO
		  *Temos que fazer com que verifique as vigências 
		  *para que se houver alguma vigência amarrada a outras,
		  *fique avisado que haverá alteração de outras propostas  
		  */
		  
		   /**
		   * TODO 
		   * ajax só consegue pegar um plano e uma vigência. 
		   * Poderia pegar toda a lista de vigências?
		   * 
		   */
		
		   /**
		    * TODO
		    *  Conseguimos fazer aparecer a janela de confirmação, 
			* mas para todas as alterações.
			* Teria como fazermos para apenas quando houver planos amarrados?
			*/
		//$('#form').submit();
		
		  // alert('codigo plano: '+$('#codigoPlano').val()+ ' lista: '+$('#vigenciasIterator').val());
		/*   $('#') */
		  $.ajax({
			  type: "POST",
			  url: $.url.get('verificarSeTemVigenciasAmarradas'),
			  data: { 
				      'planoVO.codigo':$('#codigoPlano').val()
				      
				      },
			  beforeSend: function(){
				  bloquearTela();
			  },
			  complete: function(){
				  desbloquearTela();
			  },
			  success: function(data){
				  
				  console.log(data);
				  //$('#codigoPlano').val();
				  //TODO se houver algum canal com vigência amarrada, adicionar o nome a tabela
				  
				  var lista1 = data.listaCanaisAmarrados;

				  if(lista1 != null && lista1 != undefined && lista1.length > 0){
					  
					  $('#tabelaConfirmacaoAlteracao').show();
					  $('#tabelaConfirmacaoAlteracaoBotao').show();	
					
					  if(lista1.length == 1){
						
						  $('.mensagem_canal').show();
						  $('.mensagem_canais').hide();
						  
					  }
					  else if(lista1.length > 1){
						  
						  $('.mensagem_canal').hide();
						  $('.mensagem_canais').show();
						  
					  }
				  }
				  else{
					  
					  $('#form').submit();
					  return ;
					  
				  }
				  
				  for ( var obj  in lista1) {
					 console.log(obj);
					  	var coluna = "<tr class='nova_linha'><td>"+lista1[obj]+"</td></tr>";
					  	$('#tabelaConfirmacaoAlteracao').append(coluna);
					  	
				  }
			  },
			  error: function(data){
				  alert('error');
			  }
			  
		  });
		   
		  
		
		//d0ata;
		//$('#vigenciasIterator').val(); 
		//$('#codigoPlano').val();
		
		//$('#dialog').dialog('open');
		
      
	});
	
	$('.podeAlterar').on('change',function(){
		
		if($(this).val() == 'true'){
			
			$('#form').submit();
			
		}
		else if($(this).val() == 'false'){
			
			$('#tabelaConfirmacaoAlteracao').hide();
			$('#tabelaConfirmacaoAlteracaoBotao').hide();	
			$('.nova_linha').remove();
			//$(this).val(null);
			$(this).removeAttr('checked');
		}
		
	});
	
	 $('#dialog').dialog({
         autoOpen: false,
         width: 600,
         dialogClass: 'tabela_interna',
         color: "black",
          
         buttons: {
             "Sim": function() {
            	 $('#form').submit();
             },
             "Nao": function() {
                 $(this).dialog("close");
             }
         }
     });
	 
//	 $div.dialog({
//		    modal: true,
//		    maxHeight:500,
//		}).prev(".ui-dialog-titlebar").css("background","red");
//
//	 
//	 $('.ui-dialog-titlebar').addClass("tabela_interna");
//	    $('.ui-dialog-title').addClass("tabela_interna");
//	    $('.ui-dialog-content').addClass("tabela_interna");
//	    $('.ui-dialog-buttonpane').addClass("tabela_interna");
//	    $('#dialog').addClass("tabela_interna");
	
	/**
	 * 
	 */
	$('#alterarCanal').click(function() {
		$('#form').attr('action', $.url.get('urlAlterarCanal'));
		$('#form').submit();
	});
	
	
	/**
	 * 
	 */
	$('#voltar').click(function() {
		$('#form').removerAtributosRequired();
		$('#form').attr('action', $.url.get('urlConsultar'));
		$('#form').submit();
	});
	
	
	/**
	 * 
	 */
//	$('#voltarConsultarCanal').click(function() {
//		$('#form').removerAtributosRequired();
//		$('#form').attr('action', $.url.get('urlVoltarCanal'));
//		$('#sigla').val('');
//		$('#nome').val('');
//		$('#form').submit();
//	});
	
	
	
	
	/**
	 * 
	 */
	$('#voltarConsultarCanalVenda').click(function() {
		$('#form').removerAtributosRequired();
		$('#form').attr('action', $.url.get('urlVoltarCanal'));
		$('#form').submit();
	});
	
	/**
	 * 
	 */
	
	$('#consultarcanal').click(function() {
		//alert('estou aqui');
		$('#form').removerAtributosRequired();
		$('#form').attr('action', $.url.get('urlConsultarCanal'));
		$('#form').submit();
	});
	
	$('#limparCanalVenda').click(function() {
		$('#sigla').val('');
		$('#nome').val('');
	});
	


//	function confirmarAlteracao(confirmar) {
//		if (confirmar) {
//			$("#alteracaoConfirmada").val("S");
//		}
//		else {
//			$("#alteracaoConfirmada").val("N");
//		}
//		submitForm('alterarModeloTextoComplexo.do','modeloTextoComplexoForm');
//	}
	
});