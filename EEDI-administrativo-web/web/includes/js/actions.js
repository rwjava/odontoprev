// Abas Histórico
function actionHistorico(f) {
	$(f).parent().parent().children().removeClass("active");
	$(f).parent().addClass("active");
}

// Editar Campos
function editFields(f) {
	$(f).parent().append('<span class="label label-default pull-right">Alterando</span>');
	$(f).hide();
	var button = $(f).parent();
	var target = $(f).parent().next();
	target.find("[disabled]").removeAttr("disabled").addClass("editItemDisabled");
	target.find("[readonly]").removeAttr("readonly").addClass("editItem");
	target.find("button").show();
	if(target.hasClass("ui-addItem")) {
		target.last().prev().show();
		target.find("h4").fadeIn();
	}
	target.next().show();
}
// Finalizar Edição
function doneEditFields(f) {
	var target = $(f).parent().parent().prev();
	target.prev().children().last().prev().show();
	target.prev().children().last().remove();
	target.find(".editItemDisabled").prop("disabled", "disabled").removeClass("editItemDisabled");
	target.find(".editItem").prop("readonly","readonly").removeClass("editItem");
	target.find("button").hide()
	if(target.hasClass("ui-addItem")) {
		target.children().last().hide();
		target.find("h4").hide();
	}
	$(f).parent().parent().hide();
}

//Lista com Adição de Itens
function uiAddItem(f) {
	$(f).children().last().click(function() {
		var html = $(this).parent().find(".example").html()
		$(this).before("<div class='col-md-12 box' style='display:none;'>" + html + "</div>");
		$(this).prev().find("h4").append("<span onClick='removeAddedItem(this);' title='excluir' class='glyphicon glyphicon-remove pull-right'></span>");
		$(this).prev().fadeIn(300);
		$(this).prev().find("input").first().focus();
	});
}

function removeAddedItem(f) {
	$(f).parent().parent().fadeOut(150, function() {
		$(this).remove();
	});
}

//
function openDropDown (f) {
	if ($(f).next().css("display") == "none") {
		$(f).next().fadeIn(150);
	} else {
		$(f).next().fadeOut(150);
	}
}
$(document).ready(function() {
	$(".required").each(function() {
		$(this).children("label").append('&nbsp;<span class="ui-required"><em>(Obrigatório)</em></span>');
	});
	$(".optional").each(function() {
		$(this).children("label").append('&nbsp;<span class="ui-optional"><em>(Opcional)</em></span>');
	});
	$(".datepicker").each(function() {
		$(this).datepicker();
	});
	$(".ui-addItem").each(function() {
		uiAddItem(this);
	});
	$(".ui-search  > .dropdown-menu").each(function() {
		$(this).width($(this).parent().parent().width() - 32);
	});
	$(".steps").each(function(){
		var space = Math.ceil($(this).width()/($(this).children(".step").size() - 1));
		var pos = 0;
		$(this).children(".step").each(function(){
			$(this).css("left", pos + "px");
			pos = pos + space;
		});
	});
	$(".slideContent").each(function(){
		$(this).click(function(){
			if($(this).children("ul").css("display") == "none") {
				$(this).children("ul").fadeIn("fast");
			} else {
				$(this).children("ul").fadeOut("fast");
			}
		})
	});
	
	$(".ui-tooltip").each(function() {
		$(this).tooltip();
	});
	$(".ui-popover").each(function() {
		$(this).popover({html : true, content: function() {return $("#popoverContent").html();}});
	});
	$(".ui-popproposta").each(function() {
		$(this).popover({html : true, content: function() {return $("#popoverProposta").html();}});
	});
	$(".ui-popproposta2").each(function() {
		$(this).popover({html : true, content: function() {return $("#popoverProposta2").html();}});
	});
	$(window).resize(function() {
		$(".steps").each(function(){
		var space = Math.ceil($(this).width()/($(this).children(".step").size() - 1));
		var pos = 0;
		$(this).children(".step").each(function(){
			$(this).css("left", pos + "px");
			pos = pos + space;
		});
	});	
	});
})

function additem6(f) {
	$(f).parent().parent().before("<div class='row'><div class='form-group col-md-2'><label>Homologado</label><br/><span class='radio-inline'><input type='radio' checked='checked' name='homologadoOptions' id='' value='option1'> Sim </span> <span class='radio-inline'><input type='radio' name='homologadoOptions' id='' value='option2'> Não </span></div><div class='form-group col-md-7'><label>Nome</label><select class='form-control'><option>Selecione ...</option></select></div><a href='javascript:void(0);' onclick='$(this).parent().remove()' style='display:block; font-size:0.7em; margin-top:30px;'><span class='glyphicon glyphicon-remove'></span></a></div>");
}
function additem(f) {
	$(f).parent().parent().before("<div class='row'><div class='col-md-6 form-group'><label>Item de Contratação</label><select class='form-control'><option>Selecione...</option></select></div><a href='javascript:void(0);' onclick='$(this).parent().remove()' style='display:block; font-size:0.7em; margin-top:30px;'><span class='glyphicon glyphicon-remove'></span></a></div>");
}
function additem5(f) {
	$(f).parent().parent().before("<div class='row'><div class='form-group col-md-3'><label>ID Orçamentário</label><input type='text' class='form-control'/></div><div class='form-group col-md-3'><label>Valor</label><input type='text' class='form-control'/></div><div class='form-group col-md-3'><label>Saldo / Resevado</label><input type='text' class='form-control'/></div><div class='form-group col-md-2'><label>Rateio</label><input type='text' class='form-control'/></div><a href='javascript:void(0);' onclick='$(this).parent().remove()' style='display:block; font-size:0.7em; margin-top:30px;'><span class='glyphicon glyphicon-remove'></span></a></div>");
}
function additem3(f) {
	$(f).parent().parent().before("<div class='row'><div class='form-group col-md-4 required'><label>Tipo de Demanda</label><select class='form-control'><option>Selecione...</option><option>GDBS</option><option>Avulsa</option></select></div><div class='form-group col-md-3 required'><label>Nº da GD</label><input type='text' class='form-control' /></div><div class='form-group col-md-4 required'><label>Quantidade de Horas</label><input type='text' class='form-control' /></div><a href='javascript:void(0);' onclick='$(this).parent().remove()' style='display:block; font-size:0.7em; margin-top:30px;'><span class='glyphicon glyphicon-remove'></span></a></div>");
}
function additem4(f) {
	$(f).parent().parent().before("<div><div class='row ui-margin-top'><div class='form-group col-md-4'><label>Nome</label><input type='text' class='form-control'/></div><div class='form-group col-md-4'><label>E-mail</label><input type='text' class='form-control'/></div><div class='form-group col-md-3'><label>Celular</label><input type='text' class='form-control'/></div><a href='javascript:void(0);' onclick='$(this).parent().parent().remove()' style='display:block; font-size:0.7em; margin-top:30px;'><span class='glyphicon glyphicon-remove'></span></a></div><div class='row'><div class='form-group col-md-3'><label>Telefone Comercial</label><input type='text' class='form-control'/></div><div class='form-group col-md-1'><label>Ramal</label><input type='text' class='form-control'/></div></div></div>")
}
function additem2(f) {
	$(f).parent().parent().before("<div><div class='row ui-margin-top'><div class='col-md-3 form-group'><label>Gestor (TI)</label><input type='text' class='form-control'/></div><div class='col-md-4 form-group'><label>Nome</label><input type='text' class='form-control'/></div><div class='col-md-2 form-group'><label>Telefone</label><input type='text' class='form-control'/></div><div class='col-md-2 form-group'><label>Ramal</label><input type='text' class='form-control'/></div><a href='javascript:void(0);' onclick='$(this).parent().parent().remove()' style='display:block; font-size:0.7em; margin-top:30px;'><span class='glyphicon glyphicon-remove'></span></a></div></div>");
}

function saveItem(f) {
	if($(f).parent().parent().parent().parent().children().find("table tbody tr td:first-child").text() == "Nenhum item foi adicionado.") {
		$(f).parent().parent().parent().parent().children().find("table tbody tr").remove();
	}
	$(f).parent().parent().parent().parent().children().find("table").children("tbody").append("<tr><td>Nome do Aqruivo</td><td class='text-right'>125kb</td><td><a href='javascript:void(0);' onclick='$(this).parent().parent().remove();'><span class='glyphicon glyphicon-remove'></span></a></td></tr>");
	$(f).parent().parent().parent().remove();
}

function activeTab(f) {
	$(f).parent().children().removeClass("active");
	$(f).addClass("active");
}

function proporFunc(f) {
	var qtd = $(f).val();
	var filho = $(f).parent().parent().parent().find(".proporfunc").children("div").size();
	if (qtd > filho) {
		$(f).parent().parent().parent().find(".glyphicon-remove").parent().hide();
		var conteudo = "<div class='row'><div class='col-md-4 form-group'><label>Nome do Recurso Indicado</label><input placeholder='Sem indicação' type='text' class='form-control nome'/></div> <div class='col-md-2 form-group'><label>CPF</label><input type='text' class='form-control'/></div><div class='col-md-2 form-group'><label>Data Início</label><input type='text' class='form-control'/></div><div class='col-md-2 form-group'><label>Data Fim</label><input type='text' class='form-control'/></div><div class='col-md-2 form-group'><label>Qtd Horas</label><input type='text' class='form-control'/></div><a href='javascript:void(0);' title='Limpar' onclick='removeProporFunc(this);' style='display:none; font-size:0.7em; margin-top:30px;'><span class='glyphicon glyphicon-remove'></span></a></div>"
		var i;
		var j = qtd - filho;
		for (i = 0; i < j; i++) {
			$(f).parent().parent().parent().find(".proporfunc").append(conteudo);
		}
	}
	if (qtd < filho) {
		var n = filho - qtd;
		var d;
		for (d = 0; d < n; d++){
			if ($(f).parent().parent().parent().find(".proporfunc").children().last().find("input.nome").val() == "") {
				$(f).parent().parent().parent().find(".proporfunc").children().last().remove();
			} else {
				$(".n").text(n-d);
				$(".alerta").show();
				$(f).parent().parent().parent().find(".proporfunc .form-group").addClass("has-error")
				$(f).parent().parent().parent().find(".glyphicon-remove").parent().css("display", "block");	
				break;	
			}
		}
	}
}

function removeProporFunc(f) {
	var filho = $(f).parent().parent().children().size() - 1;
	if($(f).parent().parent().parent().find("input.number").val() == filho) {
		$(f).parent().parent().find(".has-error").removeClass("has-error");
		$(f).parent().parent().find(".glyphicon-remove").parent().hide();
		$(".alerta").hide("fast");
	}
	$(f).parent().parent().remove();	
	
}
function addExemple (f) {
	var conteudo = $("#exemplo").html();
	$(f).parent().parent().before("<div class='row'>" + conteudo + "<a href='javascript:void(0);' onclick='$(this).parent().remove()' style='display:block; font-size:0.7em; margin-top:30px;'><span class='glyphicon glyphicon-remove'></span></a></div>")
}

function openClose (f) {
	if ($(f).hasClass("glyphicon-minus-sign")) {
		$(f).parent().parent().next().hide("fast");
		$(f).removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
	} else {
		$(f).parent().parent().next().show("fast");
		$(f).removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
	}
}
