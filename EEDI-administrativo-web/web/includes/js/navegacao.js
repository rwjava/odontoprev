//Função para ser aceito apenas números nos inputs selecionados
function verificaNumero(e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
    }
}

//Função para ser aceito apenas letras
function soLetras(obj){
     var tecla = (window.event) ? event.keyCode : obj.which;
     if((tecla >= 65 && tecla <= 90 || tecla >= 97 && tecla <= 122) || (tecla > 97 && tecla < 122))
               return true;
     else{
          if (tecla != 32) return false;
          else return true;
     }
}

$(document).ready(function(){
	/*************************************************
	 *       Inputs que aceitam somente números      *
	 *************************************************/
	$('#sucursal').keypress(verificaNumero);
	$('#cnpjCpf').keypress(verificaNumero);
	
	/*************************************************
	 *     Fim Inputs que aceitam somente números    *
	 *************************************************/
	
	/**********************************************************************************
	 *                          Urls usadas no js de plano.                           *
	 **********************************************************************************/
	$.url.add({'urlNovo':'/plano/iniciarIncluir.do'});
	$.url.add({'urlIncluir':'/plano/incluir.do'});
	$.url.add({'urlVerificarSePodeIncluir':'/plano/verificarSePodeIncluir.do'});
	$.url.add({'urlConsultar':'/plano/listarPorFiltro.do'});
	$.url.add({'urlAlterar':'/plano/alterar.do'});
	$.url.add({'urlAssociar':'/plano/associarPlanoCanal.do'});	
	$.url.add({'urlIniciarAlterarPlano':'/plano/iniciarAlterar.do'});	
	$.url.add({'urlVoltarConsultarPlano':'/plano/listarPorFiltro.do'});  
	$.url.add({'urlOcultarExibirTipoCobranca':'/plano/ocultarExibirTipoCobranca.do'});	
	$.url.add({'urlAlterarPlano':'/plano/alterar.do'});
	$.url.add({'salvarRelacaoCanalVendaPlano':'/plano/salvarRelacaoCanalVendaPlano.do'});
	$.url.add({'verificarSeTemVigenciasAmarradas':'/plano/verificarSeTemVigenciasAmarradas.do'});
	$.url.add({'salvarRelacaoCanalVendaPlanoAlteracao':'/plano/salvarRelacaoCanalVendaPlanoAlteracao.do'});
	$.url.add({'removerVigenciaCanalVendaPlano':'/plano/removerVigenciaCanalVendaPlano.do'});
	$.url.add({'alterarDataFimVigencia':'/plano/alterarDataFimVigencia.do'});
	$.url.add({'removerVigenciaProgramadaCanalVendaPlano':'/plano/removerVigenciaProgramadaCanalVendaPlano.do'});
	
	
	
	/**********************************************************************************
	 *                          Urls usadas no js de canal.                           *
	 **********************************************************************************/
	$.url.add({'urlIniciarIncluirCanal':'/canal/iniciarIncluirCanal.do'});
	$.url.add({'urlIncluirCanal':'/canal/incluirCanal.do'});
	$.url.add({'urlConsultarCanal':'/canal/listarPorFiltroCanal.do'});
	$.url.add({'urlAlterarCanal':'/canal/alterarCanal.do'});
	$.url.add({'urlVoltarCanal':'/canal/listarCanais.do'});	
    $.url.add({'urlConsultarCanalVenda':'/canal/listarPorFiltroCanal.do'});
    $.url.add({'urlVoltarConsultarCanal':'/canal/listarCanais.do'});  
	$.url.add({'urlIniciarIncluirCanal':'/canal/iniciarIncluirCanal.do'});
	$.url.add({'urlIncluirCanal':'/canal/incluirCanal.do'});
	
	
	/**********************************************************************************
	 *                        Urls usadas no js de historico.                         *
	 **********************************************************************************/
	$.url.add({'urlConsultarHistorico':'/historico/listarPorFiltro.do'});
	$.url.add({'urlConsultarNovoHistorico':'/historico/listarHistoricoPorFiltro.do'});
	


	/*********************************************************************************
	 *                        Url's usadas no js de corretor.                        *
	 *********************************************************************************/
	$.url.add({'urlConsultarAcessoCorretor':'/corretor/listarPorFiltroCorretor.do'});
	$.url.add({'urlIniciarIncluirCorretor':'/corretor/iniciarIncluirCorretor.do'});
	$.url.add({'urlIncluirCorretor':'/corretor/incluirCorretor.do'});
	$.url.add({'urlConsultarSucursal':'/corretor/iniciarConsultarSucursal.do'});
	$.url.add({'urlAlterarCorretor':'/corretor/alterarCorretor.do'});
	$.url.add({'urlVoltarConsultarCorretor':'/corretor/listarPorFiltroCorretor.do'});  
	/*********************************************************************************
	 *                       Fim Url's usadas no js de corretor.                     *
	 *********************************************************************************/
	
	
	
	$('#abaPlano').click(function() {
		$('#form').attr('action', $.url.get('urlConsultar'));	
		$('#form').submit();
	});

	$('#abaCanal').click(function() {
		$('#form').attr('action', $.url.get('urlConsultarCanal'));	
		$('#form').submit();
	});
	
	$('#abaHistorico').click(function() {
		$('#form').attr('action', $.url.get('urlConsultarHistorico'));	
		$('#form').submit();
	});
	
	$('#abaNovoHistorico').click(function() {
		$('#form').attr('action', $.url.get('urlConsultarNovoHistorico'));	
		$('#form').submit();
	});
	
	$('#voltarConsultarCanal').click(function() {
    	$('#form').removerAtributosRequired();
		$('#form').attr('action', $.url.get('urlVoltarConsultarCanal'));	
		$('#form').submit();
	});	
	
	$('#voltarConsultarPlano').click(function() {
		$('#form').attr('action', $.url.get('urlVoltarConsultarPlano'));	
		$('#form').submit();
		  
	});	

	$('#consultar').click(function() {
		$('#form').attr('action', $.url.get('urlConsultar'));
		$('#form').submit();
	});
	
	$('#consultarHistorico').click(function() {
		$('#form').attr('action', $.url.get('urlConsultarHistorico'));
		$('#form').submit();
	});	
	
	/**
	 * Fun��o para limpar os campos do filtro da tela de consulta de plano.
	 */
	$('#limpar').click(function() {
		$('#nome').val('');
		$('#canal').val('0');
		$('#status').val('0');
	});
	
	/**
	 * Fun��o que redireciona a aplica��o para o cadastro de um novo plano.
	 */
	$('#novo').click(function() {
		$("#form").attr('action', $.url.get('urlNovo'));
		$('#form').submit();
	});
	
	$('#voltar').click(function() {
		$("#form").attr('action', $.url.get('urlConsultar'));
		$('#form').submit();
	});
		
	$('#incluir').click(function() {
		//console.log('Aqui: '+$('#multiselect').val() +' : '+$('#multiselect').text());
		$('#form').attr('action', $.url.get('urlIncluir'));	
        $('#form').submit();
	});
	
	$('#alterar').click(function() {
		//console.log('Aqui: '+$('#multiselect').val() +' : '+$('#multiselect').text());
		$('div').find(':input').prop('disabled', false);
		$('#form').attr('action', $.url.get('urlAlterarPlano'));	
        $('#form').submit();
	});
	
	
	/**
	 * Fun��o que redireciona a aplica��o para o cadastro de um novo canal de venda.
	 */
	$('#novoCanal').click(function() {
		$("#form").attr('action', $.url.get('urlIniciarIncluirCanal'));
		$('#form').submit();
	});
	
	$('#incluirCanal').click(function() {
			$('#form').removerAtributosRequired();
			$('#form').attr('action', $.url.get('urlIncluirCanal'));
			$('#form').submit();
	});
	
	$('#limparCanalVenda').click(function() {
		$('#sigla').val('');
		$('#nome').val('');
	});
	
	$('#consultarcanal').click(function() {
		$('#form').removerAtributosRequired();
		$('#form').attr('action', $.url.get('urlConsultarCanalVenda'));
		
		$('#form').submit();
	});

    $('#alterarCanal').click(function() {
    	$('#form').attr('action', $.url.get('urlAlterarCanal'));
		$('#form').submit();
	});

	$('#tipoCobranca').on('change',function(){
			
			//alert($('#banco').val()+" "+ $('#banco').text() );
			$('#form').removerAtributosRequired();
			
			if($('#tipoCobranca').val() == 'MENSAL' || $('#tipoCobranca').val() == 1){
				
				$('.anual').each(function(){
					var input = $(this);
					input.removeAttr('data-required');
	//				input.removeAttr('required');
				});
					$('#div_tipo_cobranca_mensal').show();
					$('#div_tipo_cobranca_anual').hide();
					$('.anual').removeClass('data-required');
					
					//$('.anual').removeAttr('data-required');
					//console.log('banco selecionado: '+$('#banco').val());
				
			}else if($('#tipoCobranca').val() == 'ANUAL' || $('#tipoCobranca').val() == 2){
				
				$('.mensal').each(function(){
					var input = $(this);
					input.removeAttr('data-required');
//					input.removeAttr('required');
				});
				$('#div_tipo_cobranca_mensal').hide();
				$('#div_tipo_cobranca_anual').show();
				                            
			}else if($('#tipoCobranca').val() == 'MENSAL_E_ANUAL' || $('#tipoCobranca').val() == 3){
				
				$('.mensal').each(function(){
					var input = $(this);
					input.attr('data-required');
				});

				$('#div_tipo_cobranca_mensal').show();
				$('#div_tipo_cobranca_anual').show();
				
				$('.anual').each(function(){
					var input = $(this);
					input.attr('data-required');
				});
				
				$('.mensal').attr('data-required');
				$('.anual').attr('data-required');

			}else{
				$('#div_tipo_cobranca_mensal').hide();
				$('#div_tipo_cobranca_anual').hide();
			}
			
		});
	
	$('#novoTipoCobranca').on('change',function(){
		
		//alert($('#banco').val()+" "+ $('#banco').text() );
		$('#form').removerAtributosRequired();
		
		if($('#novoTipoCobranca').val() == 'MENSAL' || $('#novoTipoCobranca').val() == 1){
			
			$('.anual').each(function(){
					var input = $(this);
					input.removeAttr('data-required');
				});
				$('#nova_div_tipo_cobranca_mensal').show();
				$('#nova_div_tipo_cobranca_anual').hide();
				$('.anual').removeClass('data-required');
				
				if($('#novoTipoCobranca').val() == $('#tipoCobranca').val() || ($('#tipoCobranca').val() == 'MENSAL_E_ANUAL' || $('#tipoCobranca').val() == 3)){
					
					$('#novaDescricaoCarenciaPeriodoMensal').text($('#descricaoCarenciaPeriodoMensal').val());
					
					$('#labelNovaDescricaoCarenciaPeriodoMensal').hide();
					$('#novaDescricaoCarenciaPeriodoMensal').prop('disabled', true);
				}
			
		}else if($('#novoTipoCobranca').val() == 'ANUAL' || $('#novoTipoCobranca').val() == 2){
			
			$('.mensal').each(function(){
				var input = $(this);
				input.removeAttr('data-required');
			});
			$('#nova_div_tipo_cobranca_mensal').hide();
			$('#nova_div_tipo_cobranca_anual').show();
			
			if($('#novoTipoCobranca').val() == $('#tipoCobranca').val()  || ($('#tipoCobranca').val() == 'MENSAL_E_ANUAL' || $('#tipoCobranca').val() == 3)){
				
				$('#novaDescricaoCarenciaPeriodoAnual').text($('#descricaoCarenciaPeriodoAnual').val());
				
				$('#labelNovaDescricaoCarenciaPeriodoAnual').hide();
				$('#novaDescricaoCarenciaPeriodoAnual').prop('disabled', true);
			}
			                            
		}else if($('#novoTipoCobranca').val() == 'MENSAL_E_ANUAL' || $('#novoTipoCobranca').val() == 3){
			
				$('.mensal').each(function(){
					var input = $(this);
					input.attr('data-required');
				});
				
				$('#nova_div_tipo_cobranca_mensal').show();
				$('#nova_div_tipo_cobranca_anual').show();
				
				
				$('.anual').each(function(){
					var input = $(this);
					input.attr('data-required');
//					input.attr('required');
				});
				
				$('.mensal').attr('data-required');
				$('.anual').attr('data-required');
				
				if($('#novoTipoCobranca').val() == $('#tipoCobranca').val()){
					$('#novaDescricaoCarenciaPeriodoMensal').prop('disabled', true);
					$('#novaDescricaoCarenciaPeriodoAnual').prop('disabled', true);
					
					$('#labelNovaDescricaoCarenciaPeriodoMensal').hide();
					$('#labelNovaDescricaoCarenciaPeriodoAnual').hide();
					
					$('#novaDescricaoCarenciaPeriodoMensal').text($('#descricaoCarenciaPeriodoMensal').val());
					$('#novaDescricaoCarenciaPeriodoAnual').text($('#descricaoCarenciaPeriodoAnual').val());
				}

		}else{
				
				$('#nova_div_tipo_cobranca_mensal').hide();
				$('#nova_div_tipo_cobranca_anual').hide();
		}
		
	});

		

	
	

	$('#iniciarAlterar').click(function() {
	
		$("#form").attr('action', $.url.get('urlIniciarAlterarPlano'));
	    $('#form').submit();	


	});
	
	$('#adicionar_novo_valor').click(function() {
		$('#div_novo_valor').show(); 
		$('#adicionar_novo_valor').hide();	
		$('#existeValorProgramado').val('true');
		$('#valorProgramadoExcluido').val('false');
	});	
	
	$('#remover_novo_valor').click(function() {
		$('#div_novo_valor').hide(); 
		$('#adicionar_novo_valor').show();	
		$('#existeValorProgramado').val('false');
		$('#valorProgramadoExcluido').val('true');
	});	
	
	
	
	
/*******************************************************
 *            JavaScript/JQuery do Corretor            *
 *            Autor: Hernandes Andrade			       *
 *            Ebix © 2017                              *
 *******************************************************/
	
	/************************
	 *     Aba Corretor     *
	 ************************/
	$('#abaCorretor').click(function() {
		$('#form').attr('action', $.url.get('urlConsultarAcessoCorretor'));
		$('#form').submit();
	});
	/****************************
	 *     Fim Aba Corretor     *
	 ****************************/
	
	/**
	 * Funçãoo para limpar os campos do filtro da tela de consulta de corretor.
	 */
	$('#limparDadosCorretor').click(function() {
		$('#sucursal').val('');
		$('#status').val('Selecione o status');
		
	});
	
	/**
	 * Função que retorna aplicação para página de consulta de corretor
	 */
	$('#voltarConsultarCorretor').click(function() {
		$("#form").attr('action', $.url.get('urlConsultarAcessoCorretor'));
		$('#form').submit();
	});
	
	/**
	 * Função que redireciona a aplicação para o cadastro de um novo corretor.
	 */
	$('#novoCorretor').click(function() {
		$("#form").attr('action', $.url.get('urlIniciarIncluirCorretor'));
		$('#form').submit();
	});
	
	/**
	 * Função para o cadastro de um novo corretor.
	 */
	$('#incluirCorretor').click(function() {
		//console.log('Aqui: '+$('#multiselect').val() +' : '+$('#multiselect').text());
		$('#form').attr('action', $.url.get('urlIncluirCorretor'));	
        $('#form').submit();
	});
	
	/**
	 * Função para realizar a ação para consulta de corretor(es).
	 */
	$('#consultarSucursal').click(function() {
		$('#form').attr('action', $.url.get('urlConsultarSucursal'));
		$('#form').submit();
	});
	
	$('#alterarCorretor').click(function(){
		$('#form').attr('action', $.url.get('urlAlterarCorretor'));
		$('#form').submit();
	});
	
});
	
