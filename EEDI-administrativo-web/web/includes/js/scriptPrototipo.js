var smf = 0;
$(document).ready(function() {

	$('#buscarPainelPorData').on('click',function(){

	    var dataBusca = $('#dataPesquisada').val();
		buscarPorData(dataBusca);

	});

});



function buscarPorData(data){
	
	$('#dataPainel').text(data);

    if(smf == 0 ){

    	//T1
		$('#avisoSinistroEnviados').text(12221);
		$('#avisoSinistroErrosTransmissao').text(12);
		$('#avisoSinistroPendentesEnvio').text(9);
		$('#avisoSinistroPendentesRetorno').text(19);
		$('#avisoSinistroRecebidosSucesso').text(12000);
		$('#avisoSinistroRecebidosErro').text(7);
		$('#avisoSinistroErrosNegocio').text(100);


		//T3
		$('#avisoSinistroT3Enviados').text(660);
		$('#avisoSinistroT3ErroTransmissao').text(10);
		$('#avisoSinistroT3PendentesRetorno').text(2);
		$('#avisoSinistroT3PendenteImagem').text(4);
		$('#avisoSinistroT3ErroNegocio').text(6);
		$('#avisoSinistroT3RecebidosComSucesso').text(606);

		//FornecimentoPecas
		$('#avisoSinistroFornecimentoPecasEnviados').text(469);
		$('#avisoSinistroFornecimentoPecasErrosTransmissao').text(2);
		$('#avisoSinistroFornecimentoPecasPendentesRetorno').text(4);
		$('#avisoSinistroFornecimentoPecasErrosNegocio').text(6);
		$('#avisoSinistroFornecimentoPecasRecebidosSucesso').text(460);

		smf = 1;
	}
	else if(smf == 1){
		//T1
		$('#avisoSinistroEnviados').text(1669);
		$('#avisoSinistroErrosTransmissao').text(19);
		$('#avisoSinistroPendentesEnvio').text(6);
		$('#avisoSinistroPendentesRetorno').text(190);
		$('#avisoSinistroRecebidosSucesso').text(1500);
		$('#avisoSinistroRecebidosErro').text(6);
		$('#avisoSinistroErrosNegocio').text(20);

		//T3
		$('#avisoSinistroT3Enviados').text(600);
		$('#avisoSinistroT3ErroTransmissao').text(20);
		$('#avisoSinistroT3PendentesRetorno').text(12);
		$('#avisoSinistroT3PendenteImagem').text(20);
		$('#avisoSinistroT3ErroNegocio').text(2);
		$('#avisoSinistroT3RecebidosComSucesso').text(590);

		//FornecimentoPecas
		$('#avisoSinistroFornecimentoPecasEnviados').text(460);
		$('#avisoSinistroFornecimentoPecasErrosTransmissao').text(0);
		$('#avisoSinistroFornecimentoPecasPendentesRetorno').text(6);
		$('#avisoSinistroFornecimentoPecasErrosNegocio').text(2);
		$('#avisoSinistroFornecimentoPecasRecebidosSucesso').text(452);

		//T4
		$('#avisoSinistroT4Enviados').text(669);
		$('#avisoSinistroT4ErrosTransmissao').text(2);
		$('#avisoSinistroT4PendentesRetorno').text(6);
		$('#avisoSinistroT4ErrosNegocio').text(4);
		$('#avisoSinistroT4RecebidosSucesso').text(660);

		smf = 2;
	}
	else {

		//T1
		$('#avisoSinistroEnviados').text(1909);
		$('#avisoSinistroErrosTransmissao').text(2);
		$('#avisoSinistroPendentesEnvio').text(66);
		$('#avisoSinistroPendentesRetorno').text(46);
		$('avisoSinistroRecebidosSucesso').text(1600);
		$('#avisoSinistroRecebidosErro').text(9);
		$('#avisoSinistroRecebidosErroNegocio').text(0);

		//T3
		$('#avisoSinistroT3Enviados').text(670);
		$('#avisoSinistroT3ErroTransmissao').text(12);
		$('#avisoSinistroT3PendentesRetorno').text(3);
		$('#avisoSinistroT3PendenteImagem').text(2);
		$('#avisoSinistroT3ErroNegocio').text(3);
		$('#avisoSinistroT3RecebidosComSucesso').text(600);

		//FornecimentoPecas
		$('#avisoSinistroFornecimentoPecasEnviados').text(600);
		$('#avisoSinistroFornecimentoPecasErrosTransmissao').text(2);
		$('#avisoSinistroFornecimentoPecasPendentesRetorno').text(4);
		$('#avisoSinistroFornecimentoPecasErrosNegocio').text(6);
		$('#avisoSinistroFornecimentoPecasRecebidosSucesso').text(590);

		//T4
		$('#avisoSinistroT4Enviados').text(669);
		$('#avisoSinistroT4ErrosTransmissao').text(2);
		$('#avisoSinistroT4PendentesRetorno').text(6);
		$('#avisoSinistroT4ErrosNegocio').text(4);
		$('#avisoSinistroT4RecebidosSucesso').text(629);
		smf = 0;
	}

}