
	
$(document).ready(function(){ 
	
	$.url.add({'urlConsultarCanalVenda':'/canal/iniciarConsultarCanal.do'});
	
	$.url.add({'urlIniciarIncluirCanal':'/canal/iniciarIncluirCanal.do'});
	$.url.add({'urlIncluirCanal':'/canal/incluirCanal.do'});
	$.url.add({'salvarRelacaoCanalVendaPlano':'/plano/salvarRelacaoCanalVendaPlano.do'});
	$.url.add({'salvarRelacaoCanalVendaPlanoAlteracao':'/plano/salvarRelacaoCanalVendaPlanoAlteracao.do'});
	
	$.url.add({'removerVigenciaCanalVendaPlano':'/plano/removerVigenciaCanalVendaPlano.do'});
	$.url.add({'alterarDataFimVigencia':'/plano/alterarDataFimVigencia.do'});
	$.url.add({'removerVigenciaProgramadaCanalVendaPlano':'/plano/removerVigenciaProgramadaCanalVendaPlano.do'});
	
	$.exibirMensagemErroAoAdicionarCanalVenda = function(data) {
		$('#msgErrosAjax').show();
		$('#ajaxErrors').html('<li>É necessário informar todas as informações.</li>');
		console.log(data.responseText);
	};

	if($("#canal").children().length == 1){
		
		$('.botao_cinza').show();
		$('.botao_verde').hide();
		
		
	}

	
	  $('.dataFimVigenciaPlanoVigente').on('change', function(e){
		  var $this = $(this);
		
//		  var dataFimVigenciaPlanoVigente = $this.parent('td').find('.dataFimVigenciaPlanoVigente').val(); //( $('#dataFimVigenciaPlanoVigente').val();
//		  var nomeCanalVenda =  $this.parent('td').find('.nomeCanalVenda').val();
//		  var dataInicioVigenciaPlanoVigente =$this.parent('td').find('#dataInicioVigenciaAssociacaoPlano').val();
//		  var codigoCanalVenda = $this.parent('td').find('.codigoCanalVenda').val();
//		  var dataInicioAntesDeAlteracao = $this.parent('td').find('.dataInicioVigenciaAssociacaoPlano').val();
//		  var dataFimAntesDeAlteracao = $this.parent('td').find('.dataFimAntesDeAlteracao').val();
//		 
		  var dataFimVigenciaPlanoVigente    = $this.val(),
		    nomeCanalVenda                 = $this.siblings('.nomeCanalVenda').val(),
		    dataInicioVigenciaPlanoVigente = $this.siblings('.dataInicioVigenciaAssociacaoPlano').val(),
		    codigoCanalVenda               = $this.siblings('.codigoCanalVenda').val(),
		    dataInicioAntesDeAlteracao     = $this.siblings('.dataInicioAntesDeAlteracao').val(),
		    dataFimAntesDeAlteracao        = $this.siblings('.dataFimAntesDeAlteracao').val();
		  	
		 // alert(dataFimVigenciaPlanoVigente);
		//  alert('data início: '+dataInicioVigenciaPlanoVigente);
		//  alert('nome Canal Venda: '+nomeCanalVenda);
		  //alert('codigoCanalVenda');
		  $.ajax({
			  type: "POST",
			  url: $.url.get('alterarDataFimVigencia'),
			  data: { 'vigenciaCanalVendaVO.canalVendaVO.codigo':codigoCanalVenda,	
				  	  'vigenciaCanalVendaVO.dataFimVigenciaAssociacaoPlano':dataFimVigenciaPlanoVigente,
				      'vigenciaCanalVendaVO.canalVendaVO.nome':nomeCanalVenda,
				      'vigenciaCanalVendaVO.dataInicioVigenciaAssociacaoPlano':dataInicioVigenciaPlanoVigente,
				      'vigenciaCanalVendaVO.dataInicioAntesDeAlteracao':dataInicioAntesDeAlteracao,
				      'vigenciaCanalVendaVO.dataFimAntesDeAlteracao':dataFimAntesDeAlteracao
				      },
			  beforeSend: function(){
				  bloquearTela();
			  },
			  complete: function(){
				  desbloquearTela();
			  },
			  success: function(data){
				  
			  },
			  error: function(data){
				  alert('error');
			  }
			  
		  });
		  
		  });
	  
	
	  
	 
		$('#tabelaCanais').on('click', ".remover", function(e){
			
			// e.preventDefault();
				var codigo = $('#canal').val();
				var codigo = $('#canal').val();	
				
				var hasError = false;
				var listErrors = "";
				var errors = $("div#list-errors");
				
				var id = $("#canal option:selected").text();
			//alert($('.remover').val());
	        	var $this = $(this);
	        	var nomeCanalVenda  = $this.closest('tr').find('.nomeCanalVenda').val();
			    var codigoCanalVenda = $this.closest('tr').find('.codigoCanalVenda').val();
//	        	var nomeCanalVenda  = $this.closest('tr').find('.nomeCanalVenda').val();
//				var codigoCanalVenda = $this.closest('tr').find('.codigoCanalVenda').val();
	        	//alert($this.attr('id'));
	        	$	.ajax({
	        		type: "POST",
	        		url: $.url.get('removerVigenciaCanalVendaPlano'),
	        		data:{'vigenciaCanalVendaVO.canalVendaVO.nome':$this.attr('id')},
					beforeSend: function() {
						bloquearTela();
				},
				complete: function() {
						desbloquearTela();
						
				} ,success: function(data) {
					//alert('success');
					//TODO verificar por que chama duas vezes
					$this.closest('tr').remove();
					$('#canal').append('<option value="'+codigoCanalVenda+'">'+nomeCanalVenda+'</option>');
					console.log($('#canal'));
					console.log('estou aqui ');
					
				},
				error: function(data){
					alert('error');
				}
	        	});
	        });		   
	  
		
		function adicionarNovoCanal(){
			
			var nova_linha = "<tr></tr>";
			($('#tabelaCanais')).after(coluna);
		}
		
		$(".associarCanalDeVendaAPlano").on("click", function(e){
			
			var codigo = $('#canal').val();
			var codigo = $('#canal').val();	
			
			var hasError = false;
			var listErrors = "";
			var errors = $("div#list-errors");
			
			var id = $("#canal option:selected").text();
			
			
			
			$.ajax({
	        	type: "POST",
				url:  $.url.get('salvarRelacaoCanalVendaPlano'),
				data: {'vigenciaCanalVendaVO.canalVendaVO.codigo':$("#canal").val(), 'vigenciaCanalVendaVO.dataInicioVigenciaAssociacaoPlano' : $("#dataInicioVigencia").val(),
					'vigenciaCanalVendaVO.dataFimVigenciaAssociacaoPlano':$("#dataFimVigencia").val(),'vigenciaCanalVendaVO.canalVendaVO.nome':$("#canal option:selected").text()},
				beforeSend: function() {
						bloquearTela();
				},
				complete: function() {
						desbloquearTela();
						
				} ,success: function(data) {
					//alert('data inicio: '+$('#dataInicioVigencia').val()+' data fim: '+ $('#dataFimVigencia').val());
					console.log(data);
					var erros = $(data).find('#msgErros');
					console.log(erros);
					if(erros.text() != null && erros.text() != ""){
						hasError = true;
					}
					if(hasError){
						errors.show();
						errors.html(erros);
						//$('#msgErros').hide();
						return !hasError;
					}
					if($('#canal').val() != null && $('#canal').val() != -1  && $('#dataInicioVigencia').val() != "" && $('#dataFimVigencia').val() != ""){
					
						var coluna = 			        "<tr>"
							coluna +=					"<td align='center' colspan='2'> "
						    coluna +=                   "<label>"+$("#canal option:selected").text()+"<label/>" 
						    coluna +=                   "</td>"
							coluna +=					"<td align='center' colspan='2'>"
							coluna +=					"<img src='/EEDI-Administrativo/includes/images/lixeira.gif' class='remover' id='"+id+"' style='cursor:pointer;cursor:hand;'/>"
							coluna +=					"<span class='remover' style='cursor:pointer;cursor:hand;'>Remover</span>"
							coluna +=					"</td>"
							coluna +=					"</tr>"
						
						$("#canal option[value='"+$("#canal").val() +"']").remove();
						 ($('#tabelaCanais tr:last')).after(coluna);						
					}	 					
					else{
						$.exibirMensagemErroAoAdicionarCanalVenda(data);
					}
					$('#msgErros').hide();
					errors.hide();
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert(jqXHR +"   "+textStatus+"    "+errorThrown);			
				}				
			});						
		});
		
		$(".associarCanalDeVendaAPlanoAlteracao").on("click", function(e){
			
			//e.preventDefault();
			var codigo = $('#canal').val();	
			var hasError = false;
			var listErrors = "";
			var errors = $("div#list-errors");
		        $.ajax({
		        	type: "POST",
					url:  $.url.get('salvarRelacaoCanalVendaPlanoAlteracao'),
					data: {'vigenciaCanalVendaVO.canalVendaVO.codigo':$("#canal").val(), 'vigenciaCanalVendaVO.dataInicioVigenciaAssociacaoPlano' : $("#dataInicioVigencia").val(),
						'vigenciaCanalVendaVO.dataFimVigenciaAssociacaoPlano':$("#dataFimVigencia").val(),'vigenciaCanalVendaVO.canalVendaVO.nome':$("#canal option:selected").text()},
					beforeSend: function() {
							bloquearTela();
					},
					complete: function() {
							desbloquearTela();
							
					} ,success: function(data) {
						//alert('data inicio: '+$('#dataInicioVigencia').val()+' data fim: '+ $('#dataFimVigencia').val());
						console.log(data);
						var erros = $(data).find('#msgErros');
						console.log(erros);
						if(erros.text() != null && erros.text() != ""){
							hasError = true;
						}
						if(hasError){
							errors.show();
							errors.html(erros);
							//$('#msgErros').hide();
							return !hasError;
						}
						if($('#canal').val() != null && $('#canal').val() != 0  && $('#dataInicioVigencia').val() != "" && $('#dataFimVigencia').val() != ""){
							//alert('sucess');
							//alert('Estou aqui'+$('#canal').val()+"  "+ $('#dataInicioVigencia').val()+"  "+$('#dataFimVigencia').val());

					      	 var coluna = "<tr>";
					         coluna += "<td width='40%'  align='center'><label>"+$('#canal option:selected').text()+"<label/></td>";
					         coluna +="<input type='hidden'   class='codigoCanalVenda' value='"+$('#canal option:selected').val()+"'/>";
					         coluna += "<input  type='hidden' name='canalVendaVO.nome' class='nomeCanalVenda' value = '"+$("#canal option:selected").text()+"' />";
					         coluna +="<td width='40%'  align='center'>";
					         //coluna +="<input type='text' data-mask='data' data-date-type='default' class='dataInicioVigenciaAssociacaoPlano'  size='12' maxlength='10'  value="+$("#dataInicioVigencia").val()+" name='vigenciaCanalVendaVO.dataInicioVigenciaAssociacaoPlano'/>"
					         coluna +="<label>"+$('#dataInicioVigencia').val()+"</label>";
					         coluna +=" &nbsp;&nbsp;&nbsp;&nbsp;At&eacute;&nbsp;&nbsp;&nbsp;&nbsp; <input type='text'  data-mask='data' data-date-type='default' class='dataFimVigenciaPlanoVigente' size='12' maxlength='10'  value="+$("#dataFimVigencia").val()+" name='vigenciaCanalVendaVO.dataFimVigenciaAssociacaoPlano'/></td>";
					         coluna +="<td align='center'><img src='/EEDI-Administrativo/includes/images/lixeira.gif' class='remover'  title='Remover canal de venda' style='cursor:pointer;cursor:hand;'/><span class='remover' style='cursor:pointer;cursor:hand;'>Remover</span></td>";
		 					 coluna +="<input type='hidden' name='dataInicioAntesDeAlteracao' class='dataInicioAntesDeAlteracao' value='"+$("#dataInicioVigencia").val()+"'/>";
		 					 coluna +="<input type='hidden' name='dataFimAntesDeAlteracao' class='dataFimAntesDeAlteracao' value='"+$("#dataFimVigencia").val()+"'/>";
		 					 coluna += "</tr>";
					         
		 					$("#canal option[value='"+$("#canal").val() +"']").remove();
		 					
		 					$('#canal').val();
		 					$('#canal').text();
		 					
		 					if($("#canal").children().length == 1){
		 						
		 						$('.botao_cinza').show();
		 						$('.botao_verde').hide();
		 						
		 						
		 					}else{
		 					
		 						$('.botao_cinza').hide();
		 						$('.botao_verde').show();
		 						
		 					}

		 					  
					         //alert(coluna);    
					         ($('#tabelaVigencias')).append(coluna);
						}else{
							$.exibirMensagemErroAoAdicionarCanalVenda(data);
						}
						$('#msgErros').hide();
						errors.hide();
					},
					error: function(jqXHR, textStatus, errorThrown){
						alert(jqXHR +"   "+textStatus+"    "+errorThrown);
						
					}
						
				});
		        
		
	});
		
		
 
    	$('#tabelaVigencias').on('click', '.remover', function(e){
    		e.preventDefault();
    		 var $this = $(this);     
    	    var nomeCanalVenda  = $this.closest('tr').find('.nomeCanalVenda').val(),
		    codigoCanalVenda = $this.closest('tr').find('.codigoCanalVenda').val(),
		    dataInicioAntesDeAlteracao = $this.closest('tr').find('.dataInicioAntesDeAlteracao').val(),
		    dataFimAntesDeAlteracao = $this.closest('tr').find('.dataFimAntesDeAlteracao').val(),
		    status = $this.closest('tr').find('.status').val();
    	   
    	    
			//alert('nomeCanal: '+nomeCanalVenda+'codigo:' +codigoCanalVenda);
			
			$.ajax({	
        		type: "POST",
        		url: $.url.get('removerVigenciaProgramadaCanalVendaPlano'),
        		data:{
        			'vigenciaCanalVendaVO.canalVendaVO.nome': nomeCanalVenda,
        			'vigenciaCanalVendaVO.canalVendaVO.codigo ':codigoCanalVenda,
        			'vigenciaCanalVendaVO.dataInicioVigenciaAssociacaoPlano' :dataInicioAntesDeAlteracao,
        			'vigenciaCanalVendaVO.dataFimVigenciaAssociacaoPlano' :dataFimAntesDeAlteracao
        			},
				beforeSend: function() {
					bloquearTela();
			},
			complete: function() {
					desbloquearTela();
					
			} ,success: function(data) {
				//alert('success');
				//alert(data);
				if(status == 'Programado'  || status == undefined){
					//$('#tabelaVigencias').html(data);
					
					//console.log($(data).find('#tabelaVigencias'));
					//$('#tabelaVigencias').val($(data).find('#tabelaVigencias').val());
					//$('#tabelaVigencias').reload();
					
					var novaTabela = $(data).find('#tabelaVigencias');
					console.log('********************************');
					console.log($('#tabelaVigencias'));
					//$('#tabelaVigencias').val(novaTabela);
					$this.closest('tr').remove();
					//$('#canal').append('<option value="'+codigoCanalVenda+'">'+nomeCanalVenda+'</option>');
					
					if($("#canal").children().length == 1){
 						
 						$('.botao_cinza').show();
 						$('.botao_verde').hide();
 						
 						
 					}else{
 					
 						$('.botao_cinza').hide();
 						$('.botao_verde').show();
 						
 					}
					
					
					//alert('remover vigencia '+nomeCanalVenda);
				}
			},
			 error : function(data, errorThrown) {
                    console.log(errorThrown);
                 
             }
        	});
			
    	});
   

		
		
		/**
		 * 
		 */
		$('#voltarConsultarCanalVenda').click(function() {
			$('#form').removerAtributosRequired();
			$('#form').attr('action', $.url.get('urlConsultarCanalVenda'));
			$('#nomePlano').val('');
			$('#canal').val('0');
			$('#form').submit();
		});
		
		/**
		 * 
		 */
		$('#iniciarIncluirCanal').click(function() {
			$('#form').removerAtributosRequired();
			$('#form').attr('action', $.url.get('urlIniciarIncluirCanal'));
			$('#form').submit();
		});
		
		
		/**
		 * 
		 */
		$('#incluirCanal').click(function() {
			$('#form').removerAtributosRequired();
			$('#form').attr('action', $.url.get('urlIncluirCanal'));
			
			$('#form').submit();
		});
		
});
		
		
		
		 

	
		
		
		

