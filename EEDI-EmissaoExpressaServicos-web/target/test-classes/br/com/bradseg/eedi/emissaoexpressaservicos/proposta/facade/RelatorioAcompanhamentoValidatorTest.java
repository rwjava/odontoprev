package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.emissaoexpressaservicos.validator.BaseValidatorTest;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PessoaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;

public class RelatorioAcompanhamentoValidatorTest extends BaseValidatorTest {

	private RelatorioAcompanhamentoValidator validator;

	private FiltroRelatorioAcompanhamentoVO filtro;

	@Before
	public void setUp() {
		validator = new RelatorioAcompanhamentoValidator();

		filtro = new FiltroRelatorioAcompanhamentoVO();
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoNaoInformarNenhumFiltro() throws Exception {
		try {
			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test
	public void naoDeveValidarCamposObrigatoriosQuandoInformarCodigoProposta() throws Exception {
		// Dado
		filtro.setCodigoProposta("BDA9939393-1");

		// Quando
		validator.validate(filtro);
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoPeriodoMaiorQueTrintaDias() throws Exception {
		try {
			// Dado
			filtro.setDataInicio(new LocalDate(2012, 05, 12));
			filtro.setDataFim(new LocalDate(2012, 10, 20));

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.lista.proposta.periodo.maior.que.trinta.dias");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoDataFimMenorQueDataInicio() throws Exception {
		try {
			// Dado
			filtro.setDataInicio(new LocalDate(2012, 10, 25));
			filtro.setDataFim(new LocalDate(2012, 10, 20));

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.lista.proposta.dataFim.menorQue.DataInicio");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarSucursalSemPeriodo() throws Exception {
		try {
			// Dado
			CorretorVO corretor = new CorretorVO();
			corretor.setSucursalSeguradora(new SucursalSeguradoraVO(301));
			filtro.setCorretor(corretor);

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarCpdCorretorSemPeriodo() throws Exception {
		try {
			// Dado
			CorretorVO corretor = new CorretorVO();
			corretor.setCpd(100366);
			filtro.setCorretor(corretor);

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarCpfCnpjCorretorSemPeriodo() throws Exception {
		try {
			// Dado
			CorretorVO corretor = new CorretorVO();
			corretor.setCpfCnpj("88863687000130");
			filtro.setCorretor(corretor);

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarAgenciaDebitoSemPeriodo() throws Exception {
		try {
			// Dado
			filtro.setCodigoAgenciaDebito(2256);

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarAgenciaProdutoraSemPeriodo() throws Exception {
		try {
			// Dado
			filtro.setCodigoAgenciaProdutora(2256);

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarMatriculaAssistenteBsSemPeriodo() throws Exception {
		try {
			// Dado
			filtro.setMatriculaAssistenteBS("17272727");

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarMatriculaGerenteBvpSemPeriodo() throws Exception {
		try {
			// Dado
			filtro.setMatriculaGerente("17272727");

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarSituacaoPropostaSemPeriodo() throws Exception {
		try {
			// Dado
			filtro.setSituacaoProposta(SituacaoProposta.PENDENTE);

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test
	public void naoDeveValidarCamposObrigatoriosQuandoInformarCpfTitular() throws Exception {
		// Dado
		filtro.setPessoa(new PessoaVO());
		filtro.getPessoa().setCpf("454.718.217-50");

		// Quando
		validator.validate(filtro);
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarSucursalInvalida() throws Exception {
		try {
			// Dado
			filtro.setCodigoSucursalSeguradora(171);

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.sucursal.invalida");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarCpfTitularForZero() throws Exception {
		try {
			// Dado
			filtro.getPessoa().setCpf("000.000.000-00");

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.cpf.invalido", "CPF do benefici�rio titular");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarCpfTitularInvalido() throws Exception {
		try {
			// Dado
			filtro.getPessoa().setCpf("123.456.789-10");

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.cpf.invalido", "CPF do benefici�rio titular");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarCpfCnpjCorpoZero() throws Exception {
		try {
			// Dado
			filtro.setCorretor(new CorretorVO());
			filtro.getCorretor().setCpfCnpj("00000000000000");

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.cnpj.invalido", "CPF/CNPJ Corpo");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarCpfCnpjCorpoInvalido() throws Exception {
		try {
			// Dado
			filtro.setCorretor(new CorretorVO());
			filtro.getCorretor().setCpfCnpj("89392219921022");

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.cnpj.invalido", "CPF/CNPJ Corpo");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarApenasDataInicio() throws Exception {
		try {
			// Dado
			filtro.setDataInicio(new LocalDate());

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo final");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarApenasDataFim() throws Exception {
		try {
			// Dado
			filtro.setDataFim(new LocalDate());

			// Quando
			validator.validate(filtro);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Per�odo inicial");
			throw e;
		}
	}

}