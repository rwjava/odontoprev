package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

public class SexoTest {

	@Test
	public void deveRetornarSexoQuandoInformado() throws Exception {

		//Dado
		BeneficiarioVO beneficiario = new DependenteVO();

		//Quando
		beneficiario.setSexo(Sexo.M);

		//Entao		
		assertNotNull(beneficiario.getSexo());
		assertEquals(Sexo.M, beneficiario.getSexo());
	}

}
