package br.com.bradseg.eedi.emissaoexpressaservicos.support;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.ValidadorCNS;

public class ValidadorCNSTest {

	@Test
	public void deveValidarCns() throws Exception {
		assertFalse(ValidadorCNS.validar(123456789L));
		assertFalse(ValidadorCNS.validar(823456789L));

		assertTrue(ValidadorCNS.validar(100113736920003L));
		assertTrue(ValidadorCNS.validar(898000000043208L));
		assertTrue(ValidadorCNS.validar(190129759240018L));
		assertTrue(ValidadorCNS.validar(898001038954985L));
		assertTrue(ValidadorCNS.validar(898003701430618L));
		assertTrue(ValidadorCNS.validar(898003701418022L));
		assertTrue(ValidadorCNS.validar(898000752885083L));
		assertTrue(ValidadorCNS.validar(898003708105771L));
		assertTrue(ValidadorCNS.validar(201597135300005L));
		assertTrue(ValidadorCNS.validar(801434364369875L));
		assertTrue(ValidadorCNS.validar(207015780540004L));
		assertTrue(ValidadorCNS.validar(209845719350003L));
		assertTrue(ValidadorCNS.validar(209868015390000L));
		assertTrue(ValidadorCNS.validar(801434363411304L));
		assertTrue(ValidadorCNS.validar(706805267673923L));
		assertTrue(ValidadorCNS.validar(700805904091181L));
		assertTrue(ValidadorCNS.validar(210226354000008L));
		assertTrue(ValidadorCNS.validar(203478579780008L));
		assertTrue(ValidadorCNS.validar(206139627900002L));
		assertTrue(ValidadorCNS.validar(170121420980005L));
		assertTrue(ValidadorCNS.validar(209845737170003L));
		assertTrue(ValidadorCNS.validar(102274884040008L));
		assertTrue(ValidadorCNS.validar(102528846360018L));
		assertTrue(ValidadorCNS.validar(125214400290018L));
		assertTrue(ValidadorCNS.validar(123203468660018L));
		assertTrue(ValidadorCNS.validar(170151402820018L));
		assertTrue(ValidadorCNS.validar(108377694740018L));
		assertTrue(ValidadorCNS.validar(122857340360018L));
	}

}