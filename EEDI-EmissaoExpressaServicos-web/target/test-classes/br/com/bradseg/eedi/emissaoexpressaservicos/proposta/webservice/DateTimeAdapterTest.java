package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;

public class DateTimeAdapterTest {

	@Test
	public void testeMarshalDateTime() throws Exception {
		// Dado
		DateTime dataHora = new DateTime();

		// Quando
		String marshal = new DateTimeAdapter().marshal(dataHora);

		// Entao
		assertEquals(dataHora.toString(Constantes.FORMATO_DATE_TIME_WEB_SERVICE), marshal);
	}

	@Test
	public void testeUnmarshalDateTime() throws Exception {
		// Dado
		String dataHora = new DateTime().toString(Constantes.FORMATO_DATE_TIME_WEB_SERVICE);

		// Quando
		DateTime unmarshal = new DateTimeAdapter().unmarshal(dataHora);

		// Entao
		assertEquals(DateTimeFormat.forPattern(Constantes.FORMATO_DATE_TIME_WEB_SERVICE).parseDateTime(dataHora), unmarshal);
	}

}