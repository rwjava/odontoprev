package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

public class TipoCobrancaTest {

	@Test
	public void deveRetornarDescricaoQuandoInformadoOCodigo() throws Exception {
		//Dado
		PropostaVO proposta = new PropostaVO();

		//Quando
		proposta.setTipoCobranca(TipoCobranca.buscaPor(1));

		//Entao
		assertNotNull(proposta.getTipoCobranca());
		assertEquals(TipoCobranca.MENSAL, proposta.getTipoCobranca());
		assertEquals("Mensal", proposta.getTipoCobranca().getDescricao());
	}

	@Test
	public void deveRetornarNullQuandoInformadoCodigoInexistente() throws Exception {
		//Dado
		PropostaVO proposta = new PropostaVO();

		//Quando
		proposta.setTipoCobranca(TipoCobranca.buscaPor(3));

		//Entao
		assertEquals(null, proposta.getTipoCobranca());
	}

}