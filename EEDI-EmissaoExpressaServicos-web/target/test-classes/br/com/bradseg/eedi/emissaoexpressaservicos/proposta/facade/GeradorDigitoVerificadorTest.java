package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GeradorDigitoVerificadorTest {

	@Test
	public void deveGerarDigitoVerificador() throws Exception {
		assertEquals("BDA000000001641", GeradorCodigoProposta.gerar(164L));
		assertEquals("BDA000000001666", GeradorCodigoProposta.gerar(166L));
		assertEquals("BDA000000001674", GeradorCodigoProposta.gerar(167L));
		assertEquals("BDA000000001682", GeradorCodigoProposta.gerar(168L));
		assertEquals("BDA000000001708", GeradorCodigoProposta.gerar(170L));
		assertEquals("BDA000000001716", GeradorCodigoProposta.gerar(171L));
		assertEquals("BDA000000340890", GeradorCodigoProposta.gerar(34089L));
	}

}