package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class GrauParentescoTest {

	@Test
	public void deveRetornarDescricaoQuandoInformadoOCodigo() throws Exception {
		// Dado
		GrauParentesco grauParentesco = GrauParentesco.buscaPorCodigo(3);

		// Entao
		assertEquals(GrauParentesco.FILHA, grauParentesco);
		assertEquals("Filha", grauParentesco.getDescricao());
	}

	@Test
	public void deveRetornarNullQuandoInformadoCodigoInexistente() throws Exception {
		// Dado
		GrauParentesco grauParentesco = GrauParentesco.buscaPorCodigo(6);

		// Entao
		assertNull(grauParentesco);
	}

}