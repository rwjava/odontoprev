package br.com.bradseg.eedi.emissaoexpressaservicos.support;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.ValidadorDataNascimento;

public class ValidadorDataNascimentoTest {

	private ValidadorDataNascimento validador;

	@Before
	public void setUp() {
		validador = new ValidadorDataNascimento();
	}

	@Test
	public void deveSerInvalidoQuandoForNulo() throws Exception {
		assertFalse(validador.validar(null));
	}

	@Test
	public void deveSerValidoQuandoForDataAtual() throws Exception {
		assertTrue(validador.validar(new LocalDate()));
	}

	@Test
	public void deveSerInvalidoQuandoDataMaiorQueCentoECinquentaAnos() throws Exception {
		LocalDate dataNascimento = new LocalDate();
		assertFalse(validador.validar(dataNascimento.minusYears(150)));
	}

	@Test
	public void deveSerValidoQuandoDataMenorQueCentoECinquentaAnos() throws Exception {
		LocalDate dataNascimento = new LocalDate();
		assertTrue(validador.validar(dataNascimento.minusYears(84)));
	}

}