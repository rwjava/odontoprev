package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

public class TipoTelefoneTest {

	@Test
	public void deveRetornarDescricaoQuandoInformadoOCodigo() throws Exception {

		//Dado
		TelefoneVO telefone = new TelefoneVO();

		//Quando
		telefone.setTipo(TipoTelefone.buscaPorCodigo(1));

		//Entao
		assertNotNull(telefone.getTipo());
		assertEquals(TipoTelefone.RESIDENCIAL, telefone.getTipo());
		assertEquals("Residencial", telefone.getTipo().getDescricao());

	}

	@Test
	public void deveRetornarNullQuandoInformadoCodigoInexistente() throws Exception {

		//Dado
		TelefoneVO telefone = new TelefoneVO();

		//Quando
		telefone.setTipo(TipoTelefone.buscaPorCodigo(4));

		//Entao
		assertEquals(null, telefone.getTipo());

	}
}
