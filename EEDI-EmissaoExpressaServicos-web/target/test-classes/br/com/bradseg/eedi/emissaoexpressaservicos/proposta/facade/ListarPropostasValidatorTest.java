package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.emissaoexpressaservicos.validator.BaseValidatorTest;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;

public class ListarPropostasValidatorTest extends BaseValidatorTest {

	private FiltroPropostaVO filtroProposta;
	private ListarPropostasValidator validator;

	@Before
	public void setUp() {
		// Dado
		filtroProposta = new FiltroPropostaVO();
		validator = new ListarPropostasValidator();
	}

	@Test
	public void naoDeveOcorrerErroQuandoFiltroPropostaForValido() throws Exception {
		// Dado
		filtroProposta.setCorretor(new CorretorVO());
		filtroProposta.getCorretor().setSucursalSeguradora(new SucursalSeguradoraVO(300));
		filtroProposta.getCorretor().setCpd(188818);
		filtroProposta.setSituacaoProposta(SituacaoProposta.TODAS);
		filtroProposta.setDataInicio(new LocalDate());
		filtroProposta.setDataFim(new LocalDate());

		// Quando
		validator.validate(filtroProposta);
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoNaoInformarCamposObrigatorios() throws Exception {
		try {
			// Quando
			validator.validate(new FiltroPropostaVO());

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "CPD/Sucursal");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Situa��o da proposta");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Data In�cio do Per�odo");
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Data Fim do Per�odo");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoPeriodoMaiorQueTrintaDias() throws Exception {
		try {
			// Dado
			filtroProposta.setDataInicio(new LocalDate(2012, 05, 12));
			filtroProposta.setDataFim(new LocalDate(2012, 10, 20));

			// Quando
			validator.validate(filtroProposta);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.lista.proposta.periodo.maior.que.trinta.dias");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoDataFimMenorQueDataInicio() throws Exception {
		try {
			// Dado
			filtroProposta.setDataInicio(new LocalDate(2012, 10, 25));
			filtroProposta.setDataFim(new LocalDate(2012, 10, 20));

			// Quando
			validator.validate(filtroProposta);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.lista.proposta.dataFim.menorQue.DataInicio");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroNaoInformarSucursal() throws Exception {
		try {
			// Dado
			filtroProposta.setCorretor(new CorretorVO());
			filtroProposta.getCorretor().setSucursalSeguradora(null);

			// Quando
			validator.validate(filtroProposta);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "CPD/Sucursal");
			throw e;
		}
	}


	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarApenasDataInicio() throws Exception {
		try {
			// Dado
			filtroProposta.setDataInicio(new LocalDate());

			// Quando
			validator.validate(filtroProposta);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Data Fim do Per�odo");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarApenasDataFim() throws Exception {
		try {
			// Dado
			filtroProposta.setDataFim(new LocalDate());

			// Quando
			validator.validate(filtroProposta);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.campo.obrigatorio", "Data In�cio do Per�odo");
			throw e;
		}
	}

	@Test(expected = BusinessException.class)
	public void deveOcorrerErroQuandoInformarNomeInvalido() throws Exception {
		try {
			// Dado
			filtroProposta.setNomeProponente("32908478923980432");

			// Quando
			validator.validate(filtroProposta);

			// Entao
		} catch (BusinessException e) {
			assertHasMessage(e, "msg.erro.nome.caracteres.invalido", "Nome");
			throw e;
		}
	}

	@Test
	public void naoDeveOcorrerErroQuandoInformarApenasCodigoProposta() throws Exception {
		// Dado
		filtroProposta.setCodigoProposta("BDA000000338555");

		// Quando
		validator.validate(filtroProposta);
	}

}