package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static org.junit.Assert.assertEquals;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PropostaTest {

	private PropostaVO proposta;


	@Before
	public void setUp() {
		proposta = new PropostaVO();
		proposta.setCodigo("BDA983893892");
		
	}

	@After
	public void tearDown() {
		DateTimeUtils.setCurrentMillisSystem();
	}

	@Test
	public void deveRetornarTipoComissaoCorretorMasterBVP() throws Exception {
		// Dado
		proposta.setCorretorMaster(new CorretorMasterVO());
		proposta.getCorretorMaster().setCpd(800136);
		proposta.setSucursalSeguradora(new SucursalSeguradoraVO(701));

		// Entao
		assertEquals(TipoComissao.COMISSAO_CORRETOR_MASTER_BVP, proposta.getTipoComissao());
	}

	@Test
	public void deveRetornarTipoComissaoCorretorGerenteBVP() throws Exception {
		//Dado
		proposta.setSucursalSeguradora(new SucursalSeguradoraVO(701));
		proposta.setCodigoMatriculaGerente("123");
		//Entao
		assertEquals(TipoComissao.COMISSAO_GERENTE_BVP, proposta.getTipoComissao());
	}

	@Test
	public void deveRetornarTipoComissaoNormalQuandoSucursalForEmissora() throws Exception {
		//Dado
		proposta.setCorretor(new CorretorVO());
		proposta.getCorretor().setCpd(800136);
		proposta.setSucursalSeguradora(new SucursalSeguradoraVO(301));

		//Entao
		assertEquals(TipoComissao.COMISSAO_NORMAL, proposta.getTipoComissao());
	}

	@Test
	public void deveRetornarTipoComissaoTipo4QuandoSucursalForEmissoraComCPD250000() throws Exception {
		//Dado
		proposta.setAngariador(new AngariadorVO());
		proposta.getAngariador().setCpd(250000);
		proposta.setSucursalSeguradora(new SucursalSeguradoraVO(301));

		//Entao
		assertEquals(TipoComissao.COMISSAO_TIPO4, proposta.getTipoComissao());
	}
	
}