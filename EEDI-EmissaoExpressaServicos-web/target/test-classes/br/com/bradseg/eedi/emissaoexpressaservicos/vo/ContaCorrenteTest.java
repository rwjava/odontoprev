package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

public class ContaCorrenteTest {

	@Test
	public void deveTransformarNumeroContaCompletoEmContaCorrente() throws Exception {
		//Dado
		String numeroContaCompleto = "054231";

		//Quando
		ContaCorrenteVO contaCorrente = new ContaCorrenteVO(numeroContaCompleto);

		//Entao
		assertNotNull(contaCorrente);
		assertEquals(Long.valueOf("05423"), contaCorrente.getNumero());
		assertEquals("1", contaCorrente.getDigitoVerificador());
	}

}