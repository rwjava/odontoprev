package br.com.bradseg.eedi.emissaoexpressaservicos.support;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;

public class StringsTest {

	@Test
	public void deveRetornarNullQuandoCepInformadoForNull() throws Exception {
		//Dado
		String cep = null;

		//Quando
		String cepNormalizado = Strings.normalizarCep(cep);

		//Entao
		assertNull(cepNormalizado);
	}

	@Test
	public void deveRetornarNullQuandoCpfInformadoForNull() throws Exception {
		//Dado
		String cpf = null;

		//Quando
		String cpfNormalizado = Strings.normalizarCPF(cpf);

		//Entao
		assertNull(cpfNormalizado);
	}

	@Test
	public void deveRetornarNullQuandoCpfInformadoForZero() throws Exception {
		//Dado
		String cpf = "00000000000";

		//Quando
		String cpfNormalizado = Strings.normalizarCPFMaiorQueZero(cpf);

		//Entao
		assertNull(cpfNormalizado);
	}

	@Test
	public void deveRetornarNullQuandoCNpjInformadoForNull() throws Exception {
		//Dado
		String cnpj = null;

		//Quando
		String cnpjNormalizado = Strings.normalizarCNPJ(cnpj);

		//Entao
		assertNull(cnpjNormalizado);
	}

	@Test
	public void deveRetornarNullQuandoCpfCnpjInformadoForNull() throws Exception {
		//Dado
		String cpfCnpj = null;

		//Quando
		String cpfCnpjNormalizado = Strings.normalizarCpfCnpj(cpfCnpj);

		//Entao
		assertNull(cpfCnpjNormalizado);
	}

	@Test
	public void deveFormalizarCpfQuandoInformadoNoveCaracteres() throws Exception {
		//Dado
		String cpfCnpj = "82658345518";

		//Quando
		String cpfCnpjNormalidado = Strings.normalizarCpfCnpj(cpfCnpj);

		//Entao
		assertNotNull(cpfCnpjNormalidado);
	}

	@Test
	public void deveFormalizarCnpjQuandoInformadoQuatorzeCaracteres() throws Exception {
		//Dado
		String cpfCnpj = "52337860000119";

		//Quando
		String cpfCnpjNormalidado = Strings.normalizarCpfCnpj(cpfCnpj);

		//Entao
		assertNotNull(cpfCnpjNormalidado);
	}

}