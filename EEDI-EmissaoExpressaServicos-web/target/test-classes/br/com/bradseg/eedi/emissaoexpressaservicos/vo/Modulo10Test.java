package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.containsString;

import org.junit.Test;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Modulo10;

public class Modulo10Test {

	@Test
	public void deveGerarDigitoVerificador() throws Exception {
		//Dado
		String bloco = "6000000038";

		//Quando
		Integer digitoVerificador = Modulo10.gerarDigitoVerificador(bloco);

		//Entao
		assertEquals("4", String.valueOf(digitoVerificador));
	}

	@Test
	public void deveRetornarDigitoVerificadorZero() throws Exception {
		//Dado
		String bloco = "0000000000";

		//Quando
		Integer digitoVerificador = Modulo10.gerarDigitoVerificador(bloco);

		//Entao
		assertEquals("0", String.valueOf(digitoVerificador));
	}

	@Test(expected = IllegalArgumentException.class)
	public void deveOcorrerErroAoInformarBlocoEmBranco() throws Exception {
		try {
			//Dado
			String bloco = "";

			//Quando
			Modulo10.gerarDigitoVerificador(bloco);

			// Entao
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage(), containsString("O valor n�o pode estar vazio"));
			throw e;
		}
	}

}