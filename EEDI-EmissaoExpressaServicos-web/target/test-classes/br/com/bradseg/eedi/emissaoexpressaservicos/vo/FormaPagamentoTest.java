package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

public class FormaPagamentoTest {

	@Test
	public void deveRetornarDescricaoQuandoInformadoOCodigo() throws Exception {
		//Dado
		PropostaVO proposta = new PropostaVO();

		//Quando
		proposta.setFormaPagamento(FormaPagamento.obterPorCodigo(2));

		//Entao
		assertNotNull(proposta.getFormaPagamento());
		assertEquals(FormaPagamento.BOLETO_DEMAIS_DEBITO, proposta.getFormaPagamento());
		assertEquals("1� parcela em boleto e demais em d�bito autom�tico", proposta.getFormaPagamento().getDescricao());

	}

	@Test
	public void deveRetornarNullQuandoInformadoCodigoInexistente() throws Exception {
		//Dado
		PropostaVO proposta = new PropostaVO();

		//Quando
		proposta.setFormaPagamento(FormaPagamento.obterPorCodigo(-1));

		//Entao
		assertEquals(null, proposta.getFormaPagamento());

	}
}
