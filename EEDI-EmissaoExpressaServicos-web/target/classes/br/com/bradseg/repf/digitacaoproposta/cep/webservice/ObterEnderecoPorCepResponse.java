
package br.com.bradseg.repf.digitacaoproposta.cep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for obterEnderecoPorCepResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="obterEnderecoPorCepResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://webservice.cep.digitacaoproposta.repf.bradseg.com.br/}enderecoConsultaCepVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obterEnderecoPorCepResponse", propOrder = {
    "_return"
})
public class ObterEnderecoPorCepResponse {

    @XmlElement(name = "return")
    protected EnderecoConsultaCepVO _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link EnderecoConsultaCepVO }
     *     
     */
    public EnderecoConsultaCepVO getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnderecoConsultaCepVO }
     *     
     */
    public void setReturn(EnderecoConsultaCepVO value) {
        this._return = value;
    }

}
