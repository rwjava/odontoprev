
package br.com.bradseg.repf.digitacaoproposta.cep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listarEnderecoPorRaizCep complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listarEnderecoPorRaizCep">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="raizCep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listarEnderecoPorRaizCep", propOrder = {
    "raizCep"
})
public class ListarEnderecoPorRaizCep {

    protected String raizCep;

    /**
     * Gets the value of the raizCep property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRaizCep() {
        return raizCep;
    }

    /**
     * Sets the value of the raizCep property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRaizCep(String value) {
        this.raizCep = value;
    }

}
