
package br.com.bradseg.repf.digitacaoproposta.cep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enderecoVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enderecoVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bairroLogradouro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cepLogradouro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cidadeLogradouro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoTipoEndereco" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="complementoLogradouro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="logradouro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroLogradouro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufLogradouro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enderecoVO", propOrder = {
    "bairroLogradouro",
    "cepLogradouro",
    "cidadeLogradouro",
    "codigoTipoEndereco",
    "complementoLogradouro",
    "logradouro",
    "numeroLogradouro",
    "ufLogradouro"
})
@XmlSeeAlso({
    EnderecoConsultaCepVO.class
})
public class EnderecoVO {

    protected String bairroLogradouro;
    protected String cepLogradouro;
    protected String cidadeLogradouro;
    protected Long codigoTipoEndereco;
    protected String complementoLogradouro;
    protected String logradouro;
    protected String numeroLogradouro;
    protected String ufLogradouro;

    /**
     * Gets the value of the bairroLogradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairroLogradouro() {
        return bairroLogradouro;
    }

    /**
     * Sets the value of the bairroLogradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairroLogradouro(String value) {
        this.bairroLogradouro = value;
    }

    /**
     * Gets the value of the cepLogradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCepLogradouro() {
        return cepLogradouro;
    }

    /**
     * Sets the value of the cepLogradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCepLogradouro(String value) {
        this.cepLogradouro = value;
    }

    /**
     * Gets the value of the cidadeLogradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidadeLogradouro() {
        return cidadeLogradouro;
    }

    /**
     * Sets the value of the cidadeLogradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidadeLogradouro(String value) {
        this.cidadeLogradouro = value;
    }

    /**
     * Gets the value of the codigoTipoEndereco property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoTipoEndereco() {
        return codigoTipoEndereco;
    }

    /**
     * Sets the value of the codigoTipoEndereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoTipoEndereco(Long value) {
        this.codigoTipoEndereco = value;
    }

    /**
     * Gets the value of the complementoLogradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplementoLogradouro() {
        return complementoLogradouro;
    }

    /**
     * Sets the value of the complementoLogradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplementoLogradouro(String value) {
        this.complementoLogradouro = value;
    }

    /**
     * Gets the value of the logradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * Sets the value of the logradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogradouro(String value) {
        this.logradouro = value;
    }

    /**
     * Gets the value of the numeroLogradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLogradouro() {
        return numeroLogradouro;
    }

    /**
     * Sets the value of the numeroLogradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLogradouro(String value) {
        this.numeroLogradouro = value;
    }

    /**
     * Gets the value of the ufLogradouro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfLogradouro() {
        return ufLogradouro;
    }

    /**
     * Sets the value of the ufLogradouro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfLogradouro(String value) {
        this.ufLogradouro = value;
    }

}
