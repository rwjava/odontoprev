
package br.com.bradseg.repf.digitacaoproposta.cep.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.repf.digitacaoproposta.cep.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObterEnderecoPorCep_QNAME = new QName("http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", "obterEnderecoPorCep");
    private final static QName _BusinessException_QNAME = new QName("http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", "BusinessException");
    private final static QName _ListarEnderecoPorLogradouroResponse_QNAME = new QName("http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", "listarEnderecoPorLogradouroResponse");
    private final static QName _ListarEnderecoPorRaizCep_QNAME = new QName("http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", "listarEnderecoPorRaizCep");
    private final static QName _ObterEnderecoPorCepResponse_QNAME = new QName("http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", "obterEnderecoPorCepResponse");
    private final static QName _ListarEnderecoPorRaizCepResponse_QNAME = new QName("http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", "listarEnderecoPorRaizCepResponse");
    private final static QName _ListarEnderecoPorLogradouro_QNAME = new QName("http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", "listarEnderecoPorLogradouro");
    private final static QName _IntegrationException_QNAME = new QName("http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", "IntegrationException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.repf.digitacaoproposta.cep.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BusinessException }
     * 
     */
    public BusinessException createBusinessException() {
        return new BusinessException();
    }

    /**
     * Create an instance of {@link ObterEnderecoPorCep }
     * 
     */
    public ObterEnderecoPorCep createObterEnderecoPorCep() {
        return new ObterEnderecoPorCep();
    }

    /**
     * Create an instance of {@link ListarEnderecoPorLogradouroResponse }
     * 
     */
    public ListarEnderecoPorLogradouroResponse createListarEnderecoPorLogradouroResponse() {
        return new ListarEnderecoPorLogradouroResponse();
    }

    /**
     * Create an instance of {@link ListarEnderecoPorRaizCep }
     * 
     */
    public ListarEnderecoPorRaizCep createListarEnderecoPorRaizCep() {
        return new ListarEnderecoPorRaizCep();
    }

    /**
     * Create an instance of {@link ObterEnderecoPorCepResponse }
     * 
     */
    public ObterEnderecoPorCepResponse createObterEnderecoPorCepResponse() {
        return new ObterEnderecoPorCepResponse();
    }

    /**
     * Create an instance of {@link ListarEnderecoPorRaizCepResponse }
     * 
     */
    public ListarEnderecoPorRaizCepResponse createListarEnderecoPorRaizCepResponse() {
        return new ListarEnderecoPorRaizCepResponse();
    }

    /**
     * Create an instance of {@link IntegrationException }
     * 
     */
    public IntegrationException createIntegrationException() {
        return new IntegrationException();
    }

    /**
     * Create an instance of {@link ListarEnderecoPorLogradouro }
     * 
     */
    public ListarEnderecoPorLogradouro createListarEnderecoPorLogradouro() {
        return new ListarEnderecoPorLogradouro();
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link EnderecoVO }
     * 
     */
    public EnderecoVO createEnderecoVO() {
        return new EnderecoVO();
    }

    /**
     * Create an instance of {@link EnderecoConsultaCepVO }
     * 
     */
    public EnderecoConsultaCepVO createEnderecoConsultaCepVO() {
        return new EnderecoConsultaCepVO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterEnderecoPorCep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", name = "obterEnderecoPorCep")
    public JAXBElement<ObterEnderecoPorCep> createObterEnderecoPorCep(ObterEnderecoPorCep value) {
        return new JAXBElement<ObterEnderecoPorCep>(_ObterEnderecoPorCep_QNAME, ObterEnderecoPorCep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", name = "BusinessException")
    public JAXBElement<BusinessException> createBusinessException(BusinessException value) {
        return new JAXBElement<BusinessException>(_BusinessException_QNAME, BusinessException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEnderecoPorLogradouroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", name = "listarEnderecoPorLogradouroResponse")
    public JAXBElement<ListarEnderecoPorLogradouroResponse> createListarEnderecoPorLogradouroResponse(ListarEnderecoPorLogradouroResponse value) {
        return new JAXBElement<ListarEnderecoPorLogradouroResponse>(_ListarEnderecoPorLogradouroResponse_QNAME, ListarEnderecoPorLogradouroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEnderecoPorRaizCep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", name = "listarEnderecoPorRaizCep")
    public JAXBElement<ListarEnderecoPorRaizCep> createListarEnderecoPorRaizCep(ListarEnderecoPorRaizCep value) {
        return new JAXBElement<ListarEnderecoPorRaizCep>(_ListarEnderecoPorRaizCep_QNAME, ListarEnderecoPorRaizCep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObterEnderecoPorCepResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", name = "obterEnderecoPorCepResponse")
    public JAXBElement<ObterEnderecoPorCepResponse> createObterEnderecoPorCepResponse(ObterEnderecoPorCepResponse value) {
        return new JAXBElement<ObterEnderecoPorCepResponse>(_ObterEnderecoPorCepResponse_QNAME, ObterEnderecoPorCepResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEnderecoPorRaizCepResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", name = "listarEnderecoPorRaizCepResponse")
    public JAXBElement<ListarEnderecoPorRaizCepResponse> createListarEnderecoPorRaizCepResponse(ListarEnderecoPorRaizCepResponse value) {
        return new JAXBElement<ListarEnderecoPorRaizCepResponse>(_ListarEnderecoPorRaizCepResponse_QNAME, ListarEnderecoPorRaizCepResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEnderecoPorLogradouro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", name = "listarEnderecoPorLogradouro")
    public JAXBElement<ListarEnderecoPorLogradouro> createListarEnderecoPorLogradouro(ListarEnderecoPorLogradouro value) {
        return new JAXBElement<ListarEnderecoPorLogradouro>(_ListarEnderecoPorLogradouro_QNAME, ListarEnderecoPorLogradouro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntegrationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://webservice.cep.digitacaoproposta.repf.bradseg.com.br/", name = "IntegrationException")
    public JAXBElement<IntegrationException> createIntegrationException(IntegrationException value) {
        return new JAXBElement<IntegrationException>(_IntegrationException_QNAME, IntegrationException.class, null, value);
    }

}
