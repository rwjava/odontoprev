package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;

import com.google.common.collect.Lists;

/**
 * Dados da proposta
 * 
 * @author WDEV
 */
public class PropostaVO implements Serializable {

	private static final long serialVersionUID = -4896727889361488415L;

	private Long numeroSequencial;

	private String codigo;

	private String codigoFormatado;

	private ProponenteVO proponente;

	private FormaPagamento formaPagamento;

	private BancoVO banco;

	private AgenciaBancariaVO agenciaProdutora;

	private String codigoMatriculaAssistente;

	private SucursalSeguradoraVO sucursalSeguradora;

	private CanalVendaVO canalVenda;

	private String codigoMatriculaGerente;

	private TipoCobranca tipoCobranca;

	private DateTime dataUltimaAtualizacao;

	private String codigoResponsavelUltimaAtualizacao;

	private Subsegmentacao subsegmentacao;

	private TitularVO titular;

	private List<DependenteVO> dependentes;

	private MovimentoPropostaVO movimento;

	private AngariadorVO angariador;

	private CorretorVO corretor;

	private CorretorMasterVO corretorMaster;

	private ResponsavelLegalVO responsavelLegal;

	private LocalDate dataCriacao;

	private Origem origem;
	
	private LocalDate dataAdesao;
    
	private LocalDate dataEmissao;
	
	private LocalDate dataInicioCobranca;
	
	private Integer diaVencimento;
	
	private String protocolo;

    private PlanoVO planoVO;
    
    private Integer setTipoCobrancaCanalNovo;
   
	/**
	 * @return the setTipoCobrancaCanalNovo
	 */
	public Integer getSetTipoCobrancaCanalNovo() {
		return setTipoCobrancaCanalNovo;
	}

	/**
	 * @param setTipoCobrancaCanalNovo the setTipoCobrancaCanalNovo to set
	 */
	public void setTipoCobrancaCanalNovo(Integer setTipoCobrancaCanalNovo) {
		this.setTipoCobrancaCanalNovo = setTipoCobrancaCanalNovo;
	}

	public LocalDate getDataEmissao() {	
		dataEmissao = new LocalDate();
		return dataEmissao;
	}

	public boolean isPagamentoDebitoAutomatico() {
		return FormaPagamento.BOLETO_DEMAIS_DEBITO.equals(getFormaPagamento()) || FormaPagamento.DEBITO_AUTOMATICO.equals(getFormaPagamento()) || FormaPagamento.DEBITO_DEMAIS_BOLETO.equals(getFormaPagamento());
	}

	public boolean isPagamentoBoleto() {
		return FormaPagamento.BOLETO_BANCARIO.equals(getFormaPagamento());
	}

	public void setProdutores(List<ProdutorVO> produtores) {
		for (ProdutorVO produtor : produtores) {
			setProdutor(produtor);
		}
	}

	public void setProdutor(ProdutorVO produtor) {
		if (produtor instanceof CorretorVO) {
			setCorretor((CorretorVO) produtor);
		} else if (produtor instanceof AngariadorVO) {
			setAngariador((AngariadorVO) produtor);
		} else if (produtor instanceof CorretorMasterVO) {
			setCorretorMaster((CorretorMasterVO) produtor);
		}
	}

//	public PlanoVO getPlano() {
//		if (getTitular() != null && getTitular().getPlano() != null) {
//			return getTitular().getPlano();
//		}
//		return new PlanoVO();
//	}
	

//	public OrcamentoVO getOrcamento() {
//		return new OrcamentoVO(this, getPlano());
//	}
	
	

	/**
	 * Retorna a quantidade total de beneficiários de uma proposta: quantidade de dependente + 1 (titular) 
	 * @return Quantidade total de dependentes da proposta
	 */
	public BigDecimal getQuantidadeBeneficiarios() {
		return getQuantidadeDependentes().add(BigDecimal.ONE);
	}

	public PlanoVO getPlanoVO() {
		return planoVO;
	}

	public void setPlanoVO(PlanoVO planoVO) {
		this.planoVO = planoVO;
	}

	/**
	 * Retorna a quantidade de dependentes de uma proposta. Se o atributo quantidadeVidas estiver preenchido, utiliza esse valor - 1 (de forma que remova o titular),
	 * caso contrário retorna o tamanho da lista de dependentes.
	 * @return Total de dependentes da proposta
	 */
	public BigDecimal getQuantidadeDependentes() {
		if (getDependentes() == null) {
			return BigDecimal.ZERO;
		}
		return new BigDecimal(getDependentes().size());
	}

	public TipoComissao getTipoComissao() {
		if (canalVenda != null && Constantes.CODIGO_CANAL_VENDA_CALL_CENTER.equals(canalVenda.getCodigo())) {
			return TipoComissao.COMISSAO_CALL_CENTER;
		}else if(canalVenda != null && Constantes.CODIGO_CANAL_VENDA_FAMILIA_BRADESCO.equals(canalVenda.getCodigo())){
			return TipoComissao.COMISSAO_FAMILIA_BRADESCO;
		}else if(canalVenda != null && Constantes.CODIGO_CANAL_VENDA_INTERNET_BANKING.equals(canalVenda.getCodigo())){
			return TipoComissao.COMISSAO_INTERNET_BANKING;
		}else if(canalVenda != null && Constantes.CODIGO_CANAL_VENDA_WORKSITE_CALL.equals(canalVenda.getCodigo())){
			return TipoComissao.COMISSAO_GERENTE_BVP;
		}else if(canalVenda != null && Constantes.CODIGO_CANAL_VENDA_SHOPPING_SEGUROS.equals(canalVenda.getCodigo())){
			return TipoComissao.COMISSAO_SHOPPING_SEGUROS; //Tipo de comissao 11 para shopping de seguros
		}
		else if (corretorMaster != null && corretorMaster.isCpdPreenchido()) {
			return corretorMaster.getTipoComissao();
		} else if (isCodigoMatriculaGerentePreenchido()) {
			return new GerenteVO().getTipoComissao();
		} else if (angariador != null && angariador.isCpdPreenchido()) {
			return angariador.getTipoComissao();
		} else if (corretor != null && corretor.isCpdPreenchido()) {
			return corretor.getTipoComissao();
		}
		return null;
	}

	public Long getNumeroSequencial() {
		return numeroSequencial;
	}

	public void setNumeroSequencial(Long numeroSequencial) {
		this.numeroSequencial = numeroSequencial;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public ProponenteVO getProponente() {
		return proponente;
	}

	public void setProponente(ProponenteVO proponente) {
		this.proponente = proponente;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public BancoVO getBanco() {
		return banco;
	}

	public void setBanco(BancoVO banco) {
		this.banco = banco;
	}

	public AgenciaBancariaVO getAgenciaProdutora() {
		return agenciaProdutora;
	}

	public void setAgenciaProdutora(AgenciaBancariaVO agenciaProdutora) {
		this.agenciaProdutora = agenciaProdutora;
	}

	public String getCodigoMatriculaAssistente() {
		return codigoMatriculaAssistente;
	}

	public void setCodigoMatriculaAssistente(String codigoMatriculaAssistente) {
		this.codigoMatriculaAssistente = codigoMatriculaAssistente;
	}

	public String getCodigoMatriculaGerente() {
		return codigoMatriculaGerente;
	}

	public void setCodigoMatriculaGerente(String codigoMatriculaGerente) {
		this.codigoMatriculaGerente = codigoMatriculaGerente;
	}

	public TipoCobranca getTipoCobranca() {
		return tipoCobranca;
	}

	public void setTipoCobranca(TipoCobranca tipoCobranca) {
		this.tipoCobranca = tipoCobranca;
	}

	@XmlJavaTypeAdapter(type = org.joda.time.LocalDate.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDateAdapter.class)
	public LocalDate getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDate dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@XmlJavaTypeAdapter(type = org.joda.time.DateTime.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DateTimeAdapter.class)
	public DateTime getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(DateTime dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public String getCodigoResponsavelUltimaAtualizacao() {
		return codigoResponsavelUltimaAtualizacao;
	}

	public void setCodigoResponsavelUltimaAtualizacao(String codigoResponsavelUltimaAtualizacao) {
		this.codigoResponsavelUltimaAtualizacao = codigoResponsavelUltimaAtualizacao;
	}

	public Subsegmentacao getSubsegmentacao() {
		return subsegmentacao;
	}

	public void setSubsegmentacao(Subsegmentacao subsegmentacao) {
		this.subsegmentacao = subsegmentacao;
	}

	public boolean isCriticada() {
		return isSituacao(SituacaoProposta.CRITICADA);
	}

	public boolean isCancelada() {
		return isSituacao(SituacaoProposta.CANCELADA);
	}

	public boolean isEmProcessamento() {
		return isSituacao(SituacaoProposta.EM_PROCESSAMENTO);
	}

	public boolean isPendente() {
		return isSituacao(SituacaoProposta.PENDENTE);
	}

	public boolean isRascunho() {
		return isSituacao(SituacaoProposta.RASCUNHO);
	}

	public boolean isNova() {
		return getMovimento() == null || getMovimento().getSituacao() == null;
	}

	private boolean isSituacao(SituacaoProposta situacao) {
		if (getMovimento() == null) {
			return false;
		}
		return situacao.equals(getMovimento().getSituacao());
	}

	public MovimentoPropostaVO getMovimento() {
		return movimento;
	}

	public void setMovimento(MovimentoPropostaVO movimento) {
		this.movimento = movimento;
	}

	public AngariadorVO getAngariador() {
		return angariador;
	}

	public void setAngariador(AngariadorVO angariador) {
		this.angariador = angariador;
	}

	public CorretorVO getCorretor() {
		return corretor;
	}

	public void setCorretor(CorretorVO corretor) {
		this.corretor = corretor;
	}

	public CorretorMasterVO getCorretorMaster() {
		return corretorMaster;
	}

	public void setCorretorMaster(CorretorMasterVO corretorMaster) {
		this.corretorMaster = corretorMaster;
	}

	public ResponsavelLegalVO getResponsavelLegal() {
		return responsavelLegal;
	}

	public void setResponsavelLegal(ResponsavelLegalVO responsavelLegal) {
		this.responsavelLegal = responsavelLegal;
	}

	public SucursalSeguradoraVO getSucursalSeguradora() {
		return sucursalSeguradora;
	}

	public void setSucursalSeguradora(SucursalSeguradoraVO sucursalSeguradora) {
		this.sucursalSeguradora = sucursalSeguradora;
	}

	public String getCodigoFormatado() {
		return codigoFormatado;
	}

	public void setCodigoFormatado(String codigoFormatado) {
		this.codigoFormatado = codigoFormatado;
	}

	public TitularVO getTitular() {
		return titular;
	}

	public void setTitular(TitularVO titular) {
		this.titular = titular;
	}

	public Origem getOrigem() {
		return origem;
	}

	public void setOrigem(Origem origem) {
		this.origem = origem;
	}

	public List<DependenteVO> getDependentes() {
		return dependentes;
	}

	public void setDependentes(List<DependenteVO> dependentes) {
		this.dependentes = dependentes;
	}

	public CanalVendaVO getCanalVenda() {
		return canalVenda;
	}

	public void setCanalVenda(CanalVendaVO canalVenda) {
		this.canalVenda = canalVenda;
	}

	public boolean isCodigoPreenchido() {
		return StringUtils.isNotBlank(getCodigo());
	}

	public boolean isCodigoMatriculaGerentePreenchido() {
		return StringUtils.isNotBlank(getCodigoMatriculaGerente());
	}

	public boolean isCodigoMatriculaAssistentePreenchido() {
		return StringUtils.isNotBlank(getCodigoMatriculaAssistente());
	}

	public boolean isFormaPagamentoPreenchida() {
		return getFormaPagamento() != null;
	}

	public boolean isTipoCobrancaPreenchido() {
		return getTipoCobranca() != null;
	}

	public boolean isPossuiResponsavelLegal() {
		return responsavelLegal != null && responsavelLegal.getCodigo() != null;
	}

	public void setBeneficiarios(List<BeneficiarioVO> beneficiarios) {
		if (beneficiarios != null) {
			List<DependenteVO> dependentes = Lists.newArrayList();
			for (BeneficiarioVO beneficiarioVO : beneficiarios) {
				if (beneficiarioVO.isTitular()) {
					setTitular((TitularVO) beneficiarioVO);
				} else {
					dependentes.add((DependenteVO) beneficiarioVO);
				}
			}
			setDependentes(dependentes);
		}
	}
	

//	public boolean isPossuiOrcamento() {
//		return getOrcamento() != null && getOrcamento().isPlanoPreenchido();
//	}

	@XmlJavaTypeAdapter(type = org.joda.time.LocalDate.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDateAdapter.class)
	public LocalDate getDataAdesao() {
		return dataAdesao;
	}

	public void setDataAdesao(LocalDate dataAdesao) {
		this.dataAdesao = dataAdesao;
	}

	public Integer getDiaVencimento() {
		return diaVencimento;
	}

	public void setDiaVencimento(Integer diaVencimento) {
		this.diaVencimento = diaVencimento;
	}

	

	@Override
	public String toString() {
		return "PropostaVO [numeroSequencial=" + numeroSequencial + ", codigo=" + codigo + ", codigoFormatado="
				+ codigoFormatado + ", proponente=" + proponente + ", formaPagamento=" + formaPagamento + ", banco="
				+ banco + ", agenciaProdutora=" + agenciaProdutora + ", codigoMatriculaAssistente="
				+ codigoMatriculaAssistente + ", sucursalSeguradora=" + sucursalSeguradora + ", canalVenda="
				+ canalVenda + ", codigoMatriculaGerente=" + codigoMatriculaGerente + ", tipoCobranca=" + tipoCobranca
				+ ", dataUltimaAtualizacao=" + dataUltimaAtualizacao + ", codigoResponsavelUltimaAtualizacao="
				+ codigoResponsavelUltimaAtualizacao + ", subsegmentacao=" + subsegmentacao + ", titular=" + titular
				+ ", dependentes=" + dependentes + ", movimento=" + movimento + ", angariador=" + angariador
				+ ", corretor=" + corretor + ", corretorMaster=" + corretorMaster + ", responsavelLegal="
				+ responsavelLegal + ", dataCriacao=" + dataCriacao + ", origem=" + origem + ", dataAdesao="
				+ dataAdesao + ", diaVencimento=" + diaVencimento + ", protocolo=" + protocolo + "]";
	}

	/**
	 * Retorna dataInicioCobranca
	 *
	 * @return dataInicioCobranca - dataInicioCobranca
	 */
	@XmlJavaTypeAdapter(type = org.joda.time.LocalDate.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDateAdapter.class)
	public LocalDate getDataInicioCobranca() {
		return dataInicioCobranca;
	}

	/**
	 * Especifica dataInicioCobranca
	 *
	 * @param dataInicioCobranca - dataInicioCobranca
	 */
	public void setDataInicioCobranca(LocalDate dataInicioCobranca) {
		this.dataInicioCobranca = dataInicioCobranca;
	}

	/**
	 * Retorna protocolo.
	 *
	 * @return protocolo - protocolo
	 */
	public String getProtocolo() {
		return protocolo;
	}

	/**
	 * Especifica protocolo.
	 *
	 * @param protocolo - protocolo
	 */
	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

/*	*//**
	 * @return the corretorLista
	 *//*
	public List<CorretorVO> getCorretorLista() {
		return corretorLista;
	}

	*//**
	 * @param corretorLista the corretorLista to set
	 *//*
	public void setCorretorLista(List<CorretorVO> corretorLista) {
		this.corretorLista = corretorLista;
	}*/

}
