package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import br.com.bradseg.eedi.emissaoexpressaservicos.support.ReportFile;

/**
 * Servi�o para gerar o relat�rio da proposta
 * @author WDEV
 */
public interface RelatorioPropostaServiceFacade {

	/**
	 * Gera o relat�rio de uma proposta completa
	 * @param codigoProposta C�digo da proposta
	 * @return Relatorio da proposta
	 */
	public ReportFile gerarRelatorioPropostaCompleta(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto);
	
	/**
	 * Gerar o pdf com o termo de ades�o.
	 * 
	 * @param codigoProposta - c�digo de proposta.
	 * 
	 * @return ReportFile - Termo de Ades�o em PDF.
	 */
	public ReportFile gerarTermoDeAdesao(Long codigoProposta);
	
	/**
	 * Metodo responsavel por gerar boleto referente a uma proposta.
	 * 
	 * @param codigoProposta - codigo da proposta.
	 * 
	 * @return ReportFile - PDF com o boleto.
	 */
	public ReportFile gerarBoleto(Long codigoProposta);

}