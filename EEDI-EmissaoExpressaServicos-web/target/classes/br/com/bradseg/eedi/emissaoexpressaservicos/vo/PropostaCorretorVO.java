package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

public class PropostaCorretorVO extends PropostaVO{


	private static final long serialVersionUID = -4107426935652515734L;
	private CartaoCreditoVO cartaoCreditoVO;
	
	public PropostaCorretorVO() {
		super();
		this.cartaoCreditoVO = new CartaoCreditoVO();
	}

	public CartaoCreditoVO getCartaoCreditoVO() {
		return cartaoCreditoVO;
	}

	public void setCartaoCreditoVO(CartaoCreditoVO cartaoCreditoVO) {
		this.cartaoCreditoVO = cartaoCreditoVO;
	}
	
}
