package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import javax.inject.Named;



import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.Logger;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.CanalVendaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.PropostaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BancoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FormaPagamento;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca;

/**
 * Definições de uma proposta
 * 
 * @author WDEV
 */
public class DefinicaoProposta {

	/**
	 * Responsável por criar as definições da proposta
	 * 
	 * @author WDEV
	 */
	@Named("DefinicaoPropostaFactory")
	public static class DefinicaoPropostaFactory {

		@Autowired
		private PropostaDAO propostaDAO;

		@Autowired
		private CanalVendaDAO canalVendaDAO;
		
		private static final Logger LOGGER = LoggerFactory.getLogger(DefinicaoPropostaFactory.class);

		public DefinicaoProposta definicaoProposta(PropostaVO proposta) {
			DefinicaoProposta definicaoProposta = new DefinicaoProposta();

			if (proposta.isCodigoPreenchido()) {
				definicaoProposta.sequencial = proposta.getNumeroSequencial();
				definicaoProposta.codigo = proposta.getCodigo();
			} else {
				definicaoProposta.sequencial = propostaDAO.obterSequencialProposta();

				if (proposta.getCanalVenda() != null) {
					proposta.setCanalVenda(canalVendaDAO.consultarPorCodigo(proposta.getCanalVenda().getCodigo()));
				}

				definicaoProposta.codigo = GeradorCodigoProposta.gerar(definicaoProposta.sequencial);
			}

			definicaoProposta.sucursalSeguradora = proposta.getSucursalSeguradora();
			
			if(FormaPagamento.DEBITO_AUTOMATICO.equals(proposta.getFormaPagamento()) || FormaPagamento.BOLETO_DEMAIS_DEBITO.equals(proposta.getFormaPagamento()) || FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento()) || FormaPagamento.DEBITO_DEMAIS_BOLETO.equals(proposta.getFormaPagamento())){
				definicaoProposta.banco = new BancoVO(proposta.getProponente().getContaCorrente().getAgenciaBancaria().getBanco().getCodigo());
			}
			//estava apenas mensal. Passando a pegar do objeto
			//definicaoProposta.tipoCobranca = TipoCobranca.MENSAL;
			definicaoProposta.tipoCobranca = proposta.getTipoCobranca();
			if (proposta.getTitular() != null) {
				definicaoProposta.titularMenorIdade = proposta.getTitular().isMenorDeIdade();
			}
			LOGGER.error("INFO: OBJETO DEFINIÇÃO PROPOSTA: "+definicaoProposta);
			return definicaoProposta;
		}
	}

	private Long sequencial;
	private String codigo;
	private BancoVO banco;
	private TipoCobranca tipoCobranca;
	private SucursalSeguradoraVO sucursalSeguradora;
	private boolean titularMenorIdade;

	public Long getSequencial() {
		return sequencial;
	}

	public String getCodigo() {
		return codigo;
	}

	public BancoVO getBanco() {
		return banco;
	}

	public TipoCobranca getTipoCobranca() {
		return tipoCobranca;
	}

	public SucursalSeguradoraVO getSucursalSeguradora() {
		return sucursalSeguradora;
	}

	public boolean isTitularMenorIdade() {
		return titularMenorIdade;
	}

	@Override
	public String toString() {
		return "DefinicaoProposta [sequencial=" + sequencial + ", codigo=" + codigo + ", banco=" + banco
				+ ", tipoCobranca=" + tipoCobranca + ", sucursalSeguradora=" + sucursalSeguradora
				+ ", titularMenorIdade=" + titularMenorIdade + "]";
	}
	
	

}