package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.endereco.dao.EnderecoRowMapper;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.DefinicaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.facade.TipoComissaoServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AgenciaBancariaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BancoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DadosProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FormaPagamento;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaExpiradaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.StatusPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.Subsegmentacao;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoProdutor;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UsuarioUltimaAtualizacao;




//@Autowired
//private transient br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.facade.TipoComissaoServiceFacade tipoComissaoServiceFacade;
import com.google.common.collect.Lists;

/**
 * Implementa��o do acesso a dados da proposta
 * 
 * @author WDEV
 */
@Repository
public class PropostaDAOImpl extends JdbcDao implements PropostaDAO {

	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private transient TipoComissaoServiceFacade tipoComissaoServiceFacade;

	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaDAOImpl.class);

	public List<PropostaVO> listarPorFiltro(FiltroPropostaVO filtroProposta, LoginVO login) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL, PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD, PROTR_PPSTA_DNTAL.NCPF_CNPJ_PROTR,");
		sql.append(" BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL, BNEFC_DNTAL_INDVD.IBNEFC_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD, CANAL.RCANAL_VDA_DNTAL_INDVD,");
		sql.append(" MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD, MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA,");
		sql.append(" RETOR_MOVTO_PPSTA.RRETOR_MOVTO_PPSTA,");
		sql.append(" PROPN_DNTAL_INDVD.NCPF_PROPN_DNTAL");
		sql.append(" FROM DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD");
		sql.append(" INNER JOIN DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.CANAL_VDA_DNTAL_INDVD CANAL ON (PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD = CANAL.CCANAL_VDA_DNTAL_INDVD)");
		sql.append(" LEFT OUTER JOIN DBPROD.PROTR_PPSTA_DNTAL PROTR_PPSTA_DNTAL ON (PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.PROPN_DNTAL_INDVD PROPN_DNTAL_INDVD ON (PROPN_DNTAL_INDVD.CPROPN_DNTAL_INDVD = PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD)");
		sql.append(" LEFT OUTER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD ON (PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL = BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.RETOR_MOVTO_PPSTA RETOR_MOVTO_PPSTA ON (RETOR_MOVTO_PPSTA.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL AND RETOR_MOVTO_PPSTA.CSIT_DNTAL_INDVD = MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA = RETOR_MOVTO_PPSTA.DINIC_MOVTO_PPSTA)");
		sql.append(" WHERE PROTR_PPSTA_DNTAL.CTPO_PROTR_PPSTA = :tipoProdutor");
		sql.append(" AND PROTR_PPSTA_DNTAL.NCPF_CNPJ_PROTR = :corretorLogado");

		if (filtroProposta.getCanalVenda() != null && filtroProposta.getCanalVenda().getCodigo() != null
				&& filtroProposta.getCanalVenda().getCodigo().intValue() != 0) {
			sql.append(" AND PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD = :canal");
		}

		if (filtroProposta.isCpdCorretorPreenchido()) {
			sql.append(" AND PROTR_PPSTA_DNTAL.CCPD_PROTR = :codigoCPD");
		}

		if (filtroProposta.isCodigoPropostaPreenchido()) {
			sql.append(" AND PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD LIKE :codigoProposta");
		}

		CorretorVO corretor = filtroProposta.getCorretor();
		if (filtroProposta.isCorretorPreenchido()) {
			SucursalSeguradoraVO sucursalSeguradora = corretor.getSucursalSeguradora();
			if (corretor.isSucursalSeguradoraPreenchida()) {
				if (sucursalSeguradora.isBVP()) {
					sql.append(" AND PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP = :codigoSucursal");
				} else if (sucursalSeguradora.isEmissora()) {
					sql.append(" AND PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR = :codigoSucursal");
				}
			}
		}

		if (filtroProposta.isPeriodoPreenchido()) {
			sql.append(" AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA  >= :dataCriacaoPropostaMenor and MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA  <= :dataCriacaoPropostaMaior");
		}

		sql.append(" AND (BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL = 1 or BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL is null)");
		if (filtroProposta.isNomeProponentePreenchido()) {
			sql.append(" AND TRANSLATE(UPPER(BNEFC_DNTAL_INDVD.IBNEFC_DNTAL_INDVD),'���������������������','���������������������') LIKE :nomeProponente");
		}

		if (filtroProposta.isCpfProponentePreenchido()) {
			sql.append(" AND PROPN_DNTAL_INDVD.NCPF_PROPN_DNTAL = :cpfProponente");
		}

		if (filtroProposta.isSituacaoPreenchida()
				&& !SituacaoProposta.TODAS.equals(filtroProposta.getSituacaoProposta())) {
			// Se a situa��o escolhida n�o for TODAS, pesquisa utilizando a situa��o informada
			sql.append(" AND MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD = :codigoSituacao");
		}

		sql.append(" AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA = (SELECT MAX(MOVTO_PPSTA_DNTAL_MAX.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL_MAX WHERE MOVTO_PPSTA_DNTAL_MAX.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" ORDER BY PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL");

		MapSqlParameterSource params = new MapSqlParameterSource();

		if (filtroProposta.getCanalVenda() != null && filtroProposta.getCanalVenda().getCodigo() != null
				&& filtroProposta.getCanalVenda().getCodigo().intValue() != 0) {
			params.addValue("canal", filtroProposta.getCanalVenda().getCodigo());
		}

		if (filtroProposta.isCorretorPreenchido()) {
			params.addValue("codigoCPD", corretor.getCpd());
			if (corretor.isSucursalSeguradoraPreenchida()) {
				params.addValue("codigoSucursal", corretor.getSucursalSeguradora().getCodigo());
			}
		}

		params.addValue("tipoProdutor", filtroProposta.getTipoProdutor().getCodigo());

		if (filtroProposta.isPeriodoPreenchido()) {
			params.addValue("dataCriacaoPropostaMenor", filtroProposta.getDataInicio().toDateTimeAtStartOfDay()
					.toDate());
			params.addValue("dataCriacaoPropostaMaior", filtroProposta.getDataFim().toDateTime(
					new LocalTime(23, 59, 59)).toDate());
		}

		if (filtroProposta.isNomeProponentePreenchido()) {
			params.addValue("nomeProponente", "%" + filtroProposta.getNomeProponente().toUpperCase() + "%");
		}

		if (filtroProposta.isCpfProponentePreenchido()) {
			params.addValue("cpfProponente", Strings.desnormalizarCPF(filtroProposta.getCpfProponente()));
		}

		if (filtroProposta.isCodigoPropostaPreenchido()) {
			params.addValue("codigoProposta", "%" + filtroProposta.getCodigoProposta() + "%");
		}
		if (filtroProposta.isSituacaoPreenchida()) {
			params.addValue("codigoSituacao", filtroProposta.getSituacaoProposta().getCodigo());
		}
			
		params.addValue("corretorLogado", login.getId());
		
		LOGGER.error("INFO: LISTAR POR FILTRO: " + sql.toString() + " PAR�METROS: " + params.getValues());
		return getJdbcTemplate().query(sql.toString(), params, new PropostaRowMapper());
	}

	public List<PropostaVO> listarPropostasIntranetFiltro(FiltroPropostaVO filtroProposta, LoginVO login) {

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL, PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD, PROTR_PPSTA_DNTAL.NCPF_CNPJ_PROTR,");
		sql.append(" BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL, BNEFC_DNTAL_INDVD.IBNEFC_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD, CANAL.RCANAL_VDA_DNTAL_INDVD,");
		sql.append(" MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD, MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA,");
		sql.append(" RETOR_MOVTO_PPSTA.RRETOR_MOVTO_PPSTA,");
		sql.append(" PROPN_DNTAL_INDVD.NCPF_PROPN_DNTAL");
		sql.append(" FROM DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD");
		sql.append(" INNER JOIN DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.CANAL_VDA_DNTAL_INDVD CANAL ON (PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD = CANAL.CCANAL_VDA_DNTAL_INDVD)");
		sql.append(" LEFT OUTER JOIN DBPROD.PROTR_PPSTA_DNTAL PROTR_PPSTA_DNTAL ON (PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.PROPN_DNTAL_INDVD PROPN_DNTAL_INDVD ON (PROPN_DNTAL_INDVD.CPROPN_DNTAL_INDVD = PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD)");
		sql.append(" LEFT OUTER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD ON (PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL = BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.RETOR_MOVTO_PPSTA RETOR_MOVTO_PPSTA ON (RETOR_MOVTO_PPSTA.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL AND RETOR_MOVTO_PPSTA.CSIT_DNTAL_INDVD = MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA = RETOR_MOVTO_PPSTA.DINIC_MOVTO_PPSTA)");
		sql.append(" WHERE PROTR_PPSTA_DNTAL.CTPO_PROTR_PPSTA = :tipoProdutor");

		if (filtroProposta.getCanalVenda() != null && filtroProposta.getCanalVenda().getCodigo() != null
				&& filtroProposta.getCanalVenda().getCodigo().intValue() != 0) {
			sql.append(" AND PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD = :canal");
		}

		if (filtroProposta.isCpdCorretorPreenchido()) {
			sql.append(" AND PROTR_PPSTA_DNTAL.CCPD_PROTR = :codigoCPD");
		}

		if (filtroProposta.isCodigoPropostaPreenchido()) {
			sql.append(" AND PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD LIKE :codigoProposta");
		}

		CorretorVO corretor = filtroProposta.getCorretor();
		if (filtroProposta.isCorretorPreenchido()) {
			SucursalSeguradoraVO sucursalSeguradora = corretor.getSucursalSeguradora();
			if (corretor.isSucursalSeguradoraPreenchida()) {
				if (sucursalSeguradora.isBVP()) {
					sql.append(" AND PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP = :codigoSucursal");
				} else if (sucursalSeguradora.isEmissora()) {
					sql.append(" AND PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR = :codigoSucursal");
				}
			}
		}

		if (filtroProposta.isPeriodoPreenchido()) {
			sql.append(" AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA  >= :dataCriacaoPropostaMenor and MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA  <= :dataCriacaoPropostaMaior");
		}

		sql.append(" AND (BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL = 1 or BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL is null)");
		if (filtroProposta.isNomeProponentePreenchido()) {
			sql.append(" AND TRANSLATE(UPPER(BNEFC_DNTAL_INDVD.IBNEFC_DNTAL_INDVD),'���������������������','���������������������') LIKE :nomeProponente");
		}

		if (filtroProposta.isCpfProponentePreenchido()) {
			sql.append(" AND PROPN_DNTAL_INDVD.NCPF_PROPN_DNTAL = :cpfProponente");
		}

		if (filtroProposta.isSituacaoPreenchida()
				&& !SituacaoProposta.TODAS.equals(filtroProposta.getSituacaoProposta())) {
			// Se a situa��o escolhida n�o for TODAS, pesquisa utilizando a situa��o informada
			sql.append(" AND MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD = :codigoSituacao");
		}

		sql.append(" AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA = (SELECT MAX(MOVTO_PPSTA_DNTAL_MAX.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL_MAX WHERE MOVTO_PPSTA_DNTAL_MAX.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" ORDER BY PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL");

		MapSqlParameterSource params = new MapSqlParameterSource();

		if (filtroProposta.getCanalVenda() != null && filtroProposta.getCanalVenda().getCodigo() != null
				&& filtroProposta.getCanalVenda().getCodigo().intValue() != 0) {
			params.addValue("canal", filtroProposta.getCanalVenda().getCodigo());
		}

		if (filtroProposta.isCorretorPreenchido()) {
			params.addValue("codigoCPD", corretor.getCpd());
			if (corretor.isSucursalSeguradoraPreenchida()) {
				params.addValue("codigoSucursal", corretor.getSucursalSeguradora().getCodigo());
			}
		}

		params.addValue("tipoProdutor", filtroProposta.getTipoProdutor().getCodigo());

		if (filtroProposta.isPeriodoPreenchido()) {
			params.addValue("dataCriacaoPropostaMenor", filtroProposta.getDataInicio().toDateTimeAtStartOfDay()
					.toDate());
			params.addValue("dataCriacaoPropostaMaior", filtroProposta.getDataFim().toDateTime(
					new LocalTime(23, 59, 59)).toDate());
		}

		if (filtroProposta.isNomeProponentePreenchido()) {
			params.addValue("nomeProponente", "%" + filtroProposta.getNomeProponente().toUpperCase() + "%");
		}

		if (filtroProposta.isCpfProponentePreenchido()) {
			params.addValue("cpfProponente", Strings.desnormalizarCPF(filtroProposta.getCpfProponente()));
		}

		if (filtroProposta.isCodigoPropostaPreenchido()) {
			params.addValue("codigoProposta", "%" + filtroProposta.getCodigoProposta() + "%");
		}
		if (filtroProposta.isSituacaoPreenchida()) {
			params.addValue("codigoSituacao", filtroProposta.getSituacaoProposta().getCodigo());
		}

		LOGGER.error("INFO: LISTAR PROPOSTA INTRANET POR FILTRO: " + sql.toString() + " PAR�METROS: "
				+ params.getValues());
		return getJdbcTemplate().query(sql.toString(), params, new PropostaRowMapper());
	}

	/**
	 * Mapeamento de dados da listagem de proposta
	 * 
	 * @author WDEV
	 */
	private static final class PropostaRowMapper implements RowMapper<PropostaVO> {
		public PropostaVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
			PropostaVO proposta = new PropostaVO();
			proposta.setNumeroSequencial(resultSet.getLong("NSEQ_PPSTA_DNTAL"));
			proposta.setCodigo(Strings.maiusculas(resultSet.getString("CPPSTA_DNTAL_INDVD")));

			proposta.setCanalVenda(new CanalVendaVO());
			proposta.getCanalVenda().setCodigo(resultSet.getInteger("CCANAL_VDA_DNTAL_INDVD"));
			proposta.getCanalVenda().setNome(resultSet.getString("RCANAL_VDA_DNTAL_INDVD"));

			proposta.setTitular(new TitularVO());
			proposta.getTitular().setNome(Strings.maiusculas(resultSet.getString("IBNEFC_DNTAL_INDVD")));

			proposta.setMovimento(new MovimentoPropostaVO());
			proposta.getMovimento().setSituacao(
					SituacaoProposta.obterPorCodigo(resultSet.getInteger("CSIT_DNTAL_INDVD")));
			proposta.getMovimento().setDataInicio(resultSet.getDateTime("DINIC_MOVTO_PPSTA"));
			proposta.getMovimento().setDescricao(Strings.maiusculas(resultSet.getString("RRETOR_MOVTO_PPSTA")));

			proposta.setProponente(new ProponenteVO());
			proposta.getProponente().setCpf(Strings.normalizarCPF(resultSet.getString("NCPF_PROPN_DNTAL")));

			proposta.setCorretor(new CorretorVO());
			proposta.getCorretor().setCpfCnpj(resultSet.getString("NCPF_CNPJ_PROTR"));

			return proposta;
		}
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public PropostaVO consultarPorCodigo(String codigoProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT																																						   \n");
		sql.append(" PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL, PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CIND_FORMA_PGTO, PPSTA_DNTAL_INDVD.DINIC_COBR_PPSTA_INDVD,          \n");
		sql.append(" PPSTA_DNTAL_INDVD.CBCO, PPSTA_DNTAL_INDVD.CAG_BCRIA, PPSTA_DNTAL_INDVD.NCTA_CORR, PPSTA_DNTAL_INDVD.CBCO_PROTR_PLANO, PPSTA_DNTAL_INDVD.CAG_PROTR_PLANO,      \n");
		sql.append(" PPSTA_DNTAL_INDVD.CMATR_ASSTT_PROTR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP, PPSTA_DNTAL_INDVD.CMATR_GER_PRODT,             \n");
		sql.append(" PPSTA_DNTAL_INDVD.CTPO_COBR_PLANO, PPSTA_DNTAL_INDVD.CTPO_COMIS_PROTR, PPSTA_DNTAL_INDVD.DULT_ATULZ_REG, PPSTA_DNTAL_INDVD.CRESP_ULT_ATULZ,                   \n");
		sql.append(" PPSTA_DNTAL_INDVD.CSUB_SEGMT, PPSTA_DNTAL_INDVD.CPLANO_DNTAL_INDVD,																																   \n");
		sql.append(" BNEFC_DNTAL_INDVD.CBNEFC_DNTAL_INDVD, BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL, 																					   \n");
		sql.append(" PROPN_DNTAL_INDVD.IPROPN_DNTAL_INDVD, PROPN_DNTAL_INDVD.NCPF_PROPN_DNTAL, PROPN_DNTAL_INDVD.DNASC_PROPN_DNTAL,												   \n");
		sql.append(" PROPN_DNTAL_INDVD.REMAIL_PROPN_DNTAL, PPSTA_DNTAL_INDVD.DADSAO_PLANO_INDVD, PPSTA_DNTAL_INDVD.NDIA_VCTO_PPSTA_INDVD,PPSTA_DNTAL_INDVD.CPROT_PPSTA_CANAL_VDA,  \n");
		sql.append(" (SELECT MIN(movt.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL movt                                                                                        \n");
		sql.append(" WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL) AS DT_CRIACAO_PROPOSTA, 																	   \n");
		sql.append("  PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD  																												   \n");
		sql.append(" FROM DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD																											   \n");
		sql.append(" INNER JOIN DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)							   \n");
		sql.append(" LEFT OUTER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD ON (BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)					   \n");
		sql.append(" LEFT OUTER JOIN DBPROD.PROPN_DNTAL_INDVD PROPN_DNTAL_INDVD ON (PROPN_DNTAL_INDVD.CPROPN_DNTAL_INDVD = PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD)				   \n");
		sql.append(" WHERE PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD = :codigoProposta																								   \n");
		sql.append(" ORDER BY MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA DESC																											   \n");
		sql.append(" FETCH FIRST 1 ROWS ONLY FOR FETCH ONLY																														   \n");

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoProposta", codigoProposta);

		LOGGER.error("INFO: CONSULTAR POR C�DIGO: " + sql.toString() + " PAR�METROS: " + param.getValues());
		try {
			return getJdbcTemplate().queryForObject(sql.toString(), param, new DetalhePropostaRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Proposta n�o encontrada", e);
			return null;
		}
	}

	public PropostaVO consultarPorSequencial(Long sequencialProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL, PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CIND_FORMA_PGTO, PPSTA_DNTAL_INDVD.DINIC_COBR_PPSTA_INDVD,");
		sql.append(" PPSTA_DNTAL_INDVD.CBCO, PPSTA_DNTAL_INDVD.CAG_BCRIA, PPSTA_DNTAL_INDVD.NCTA_CORR, PPSTA_DNTAL_INDVD.CBCO_PROTR_PLANO, PPSTA_DNTAL_INDVD.CAG_PROTR_PLANO,");
		sql.append(" PPSTA_DNTAL_INDVD.CMATR_ASSTT_PROTR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP, PPSTA_DNTAL_INDVD.CMATR_GER_PRODT,");
		sql.append(" PPSTA_DNTAL_INDVD.CTPO_COBR_PLANO, PPSTA_DNTAL_INDVD.CTPO_COMIS_PROTR, PPSTA_DNTAL_INDVD.DULT_ATULZ_REG, PPSTA_DNTAL_INDVD.CRESP_ULT_ATULZ, PPSTA_DNTAL_INDVD.CSUB_SEGMT,");
		sql.append(" BNEFC_DNTAL_INDVD.CBNEFC_DNTAL_INDVD, BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL, PPSTA_DNTAL_INDVD.CPLANO_DNTAL_INDVD, ");
		sql.append(" PROPN_DNTAL_INDVD.IPROPN_DNTAL_INDVD, PROPN_DNTAL_INDVD.NCPF_PROPN_DNTAL, PROPN_DNTAL_INDVD.DNASC_PROPN_DNTAL, PROPN_DNTAL_INDVD.REMAIL_PROPN_DNTAL,DADSAO_PLANO_INDVD,"
				+ "PPSTA_DNTAL_INDVD.NDIA_VCTO_PPSTA_INDVD, PPSTA_DNTAL_INDVD.CPROT_PPSTA_CANAL_VDA, PPSTA_DNTAL_INDVD.CPROT_PPSTA_CANAL_VDA,");
		sql.append(" (SELECT MIN(movt.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL movt WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL) AS DT_CRIACAO_PROPOSTA, ");
		sql.append(" PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD  ");
		sql.append(" FROM DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD");
		sql.append(" INNER JOIN DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD ON (BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.PROPN_DNTAL_INDVD PROPN_DNTAL_INDVD ON (PROPN_DNTAL_INDVD.CPROPN_DNTAL_INDVD = PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD)");
		sql.append(" WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = :sequencialProposta");
		sql.append(" ORDER BY MOVTO_PPSTA_DNTAL.DULT_ATULZ_REG DESC");
		sql.append(" FETCH FIRST 1 ROWS ONLY FOR FETCH ONLY");

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("sequencialProposta", sequencialProposta);

		LOGGER.error("INFO: CONSULTAR POR SEQUENCIAL: " + sql.toString() + " PAR�METROS: " + param.getValues());
		try {
			return getJdbcTemplate().queryForObject(sql.toString(), param, new DetalhePropostaRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Proposta n�o encontrada", e);
			return null;
		}
	}

	public PropostaVO buscarPropostaPorCpfTitularECanalVenda(String cpfBeneficiarioTitular, Integer canalVenda) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		//		
		//		select p.* from dbprod.ppsta_dntal_indvd p, dbprod.bnefc_dntal_indvd b where b.cbnefc_dntal_indvd = 1 
		//				 and b.ncpf_bnefc_dntal =  60231126689 and b.nseq_ppsta_dntal = p.nseq_ppsta_dntal and p.ccanal_dntal_indvd = null 
		sql.append(" SELECT");
		sql.append("PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL, PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CIND_FORMA_PGTO,");
		sql.append("PPSTA_DNTAL_INDVD.CBCO, PPSTA_DNTAL_INDVD.CAG_BCRIA, PPSTA_DNTAL_INDVD.NCTA_CORR, PPSTA_DNTAL_INDVD.CBCO_PROTR_PLANO, PPSTA_DNTAL_INDVD.CAG_PROTR_PLANO,");
		sql.append("PPSTA_DNTAL_INDVD.CMATR_ASSTT_PROTR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP, PPSTA_DNTAL_INDVD.CMATR_GER_PRODT,");
		sql.append(" PPSTA_DNTAL_INDVD.CTPO_COBR_PLANO, PPSTA_DNTAL_INDVD.CTPO_COMIS_PROTR, PPSTA_DNTAL_INDVD.DULT_ATULZ_REG, PPSTA_DNTAL_INDVD.CRESP_ULT_ATULZ, PPSTA_DNTAL_INDVD.CSUB_SEGMT,");
		sql.append(" BNEFC_DNTAL_INDVD.CBNEFC_DNTAL_INDVD, BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL,PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd,");
		sql.append(" PPSTA_DNTAL_INDVD.DADSAO_PLANO_INDVD, ");
		sql.append("PPSTA_DNTAL_INDVD.NDIA_VCTO_PPSTA_INDVD,PPSTA_DNTAL_INDVD.CPROT_PPSTA_CANAL_VDA,");
		sql.append(" (SELECT MIN(movt.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL movt WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL) AS DT_CRIACAO_PROPOSTA");
		sql.append(" FROM DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD, DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD, DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL");
		sql.append(" WHERE BNEFC_DNTAL_INDVD.ncpf_bnefc_dntal = :cpfBeneficiarioTitular and BNEFC_DNTAL_INDVD.cbnefc_dntal_indvd = 1");
		sql.append("  AND");
		if (canalVenda == 1) {
			sql.append(" (PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd = :canalVenda OR PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd is null) ");
		} else {
			sql.append(" PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd = :canalVenda ");
		}
		sql.append(" AND ");
		sql.append(" PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL");
		sql.append(" AND");
		sql.append(" PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("cpfBeneficiarioTitular", cpfBeneficiarioTitular);
		params.addValue("canalVenda", canalVenda);

		LOGGER.error("INFO: BUSCAR PROPOSTA POR CPF TITULAR E CANAL DE VENDA: " + sql.toString() + " PAR�METROS: "
				+ params.getValues());
		try {
			return getJdbcTemplate().queryForObject(sql.toString(), params,
					new PropostaBuscadaPorCpfTitularECanalVendaRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Proposta n�o encontrada", e);
			return null;
		}

	}

	/**
	 * Mapemanto de dados da consulta da proposta
	 * 
	 * @author WDEV
	 */
	//	
	//	sql.append("SELECT distinct");
	//	sql.append("PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL, PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CIND_FORMA_PGTO,");
	//	sql.append("PPSTA_DNTAL_INDVD.CBCO, PPSTA_DNTAL_INDVD.CAG_BCRIA, PPSTA_DNTAL_INDVD.NCTA_CORR, PPSTA_DNTAL_INDVD.CBCO_PROTR_PLANO, PPSTA_DNTAL_INDVD.CAG_PROTR_PLANO,");
	//	sql.append("PPSTA_DNTAL_INDVD.CMATR_ASSTT_PROTR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP, PPSTA_DNTAL_INDVD.CMATR_GER_PRODT,");
	//	sql.append(" PPSTA_DNTAL_INDVD.CTPO_COBR_PLANO, PPSTA_DNTAL_INDVD.CTPO_COMIS_PROTR, PPSTA_DNTAL_INDVD.DULT_ATULZ_REG, PPSTA_DNTAL_INDVD.CRESP_ULT_ATULZ, PPSTA_DNTAL_INDVD.CSUB_SEGMT,");
	//	sql.append(" PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd, ");
	//	sql.append("  PPSTA_DNTAL_INDVD.DADSAO_PLANO_INDVD, ");
	//	sql.append(" PPSTA_DNTAL_INDVD.NDIA_VCTO_PPSTA_INDVD,PPSTA_DNTAL_INDVD.CPROT_PPSTA_CANAL_VDA, ");
	//	sql.append(" (SELECT MIN(movt.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL movt WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL) AS DT_CRIACAO_PROPOSTA,");
	//	sql.append(" (select MAX(DINIC_MOVTO_PPSTA) csit_dntal_indvd from dbprod.MOVTO_PPSTA_DNTAL where  nseq_ppsta_dntal = MOVTO_PPSTA_DNTAL.nseq_ppsta_dntal ORDER BY MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA DESC fetch first 1 rows only ) as situacao");
	//	sql.
	private static final class PropostaBuscadaPorCpfTitularECanalVendaRowMapper implements RowMapper<PropostaVO> {
		public PropostaVO mapRow(ResultSet es, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(es);
			PropostaVO proposta = new PropostaVO();
			proposta.setNumeroSequencial(resultSet.getLong("NSEQ_PPSTA_DNTAL"));
			proposta.setCodigo(Strings.maiusculas(resultSet.getString("CPPSTA_DNTAL_INDVD")));
			proposta.setDataCriacao(resultSet.getLocalDate("DT_CRIACAO_PROPOSTA"));
			proposta.setDataAdesao(resultSet.getLocalDate("DADSAO_PLANO_INDVD"));
			proposta.setDiaVencimento(resultSet.getInteger("NDIA_VCTO_PPSTA_INDVD"));
			proposta.setProtocolo(resultSet.getString("CPROT_PPSTA_CANAL_VDA"));
			proposta.setProponente(new ProponenteVO());
			proposta.getProponente().setCodigo(resultSet.getLong("CPROPN_DNTAL_INDVD"));
			proposta.getProponente().setCpf(Strings.normalizarCPFMaiorQueZero(resultSet.getString("NCPF_PROPN_DNTAL")));
			proposta.getProponente().setDataNascimento(resultSet.getLocalDate("DNASC_PROPN_DNTAL"));
			proposta.getProponente().setEmail(Strings.maiusculas(resultSet.getString("REMAIL_PROPN_DNTAL")));
			proposta.getProponente().setNome(Strings.maiusculas(resultSet.getString("IPROPN_DNTAL_INDVD")));

			if (resultSet.getInteger("CBNEFC_DNTAL_INDVD") != null) {

				proposta.setTitular(new TitularVO());
				proposta.getTitular().setCodigo(resultSet.getInteger("CBNEFC_DNTAL_INDVD"));

				if (resultSet.getString("IBNEFC_DNTAL_INDVD") != null) {
					proposta.getTitular().setNome(resultSet.getString("IBNEFC_DNTAL_INDVD"));
				}
			}

			if (resultSet.getInteger("CSUCUR_SEGDR_EMISR") != null) {
				proposta.setSucursalSeguradora(new SucursalSeguradoraVO(resultSet.getInteger("CSUCUR_SEGDR_EMISR")));
			}

			if (resultSet.getInteger("CSUCUR_SEGDR_BVP") != null) {
				proposta.setSucursalSeguradora(new SucursalSeguradoraVO(resultSet.getInteger("CSUCUR_SEGDR_BVP")));
			}

			proposta.setFormaPagamento(FormaPagamento.obterPorCodigo(resultSet.getInteger("CIND_FORMA_PGTO")));

			if (resultSet.getInteger("CAG_BCRIA") != null) {
				proposta.getProponente().setContaCorrente(new ContaCorrenteVO());
				proposta.getProponente().getContaCorrente().setAgenciaBancaria(new AgenciaBancariaVO());
				proposta.getProponente().getContaCorrente().getAgenciaBancaria().setCodigo(
						resultSet.getInteger("CAG_BCRIA"));
			}

			proposta.setBanco(new BancoVO(resultSet.getInteger("CBCO")));
			proposta.setAgenciaProdutora(new AgenciaBancariaVO());
			proposta.getAgenciaProdutora().setCodigo(resultSet.getInteger("CAG_PROTR_PLANO"));
			proposta.getAgenciaProdutora().setBanco(new BancoVO(resultSet.getInteger("CBCO_PROTR_PLANO")));
			proposta.setCodigoMatriculaAssistente(resultSet.getString("CMATR_ASSTT_PROTR"));

			proposta.setCodigoMatriculaGerente(resultSet.getString("CMATR_GER_PRODT"));
			proposta.setTipoCobranca(TipoCobranca.buscaPor(resultSet.getInteger("CTPO_COBR_PLANO")));

			proposta.setSubsegmentacao(Subsegmentacao.buscaSubsegmentacaoPor(resultSet.getInteger("CSUB_SEGMT")));
			return proposta;
		}
	}

	/**
	 * Mapemanto de dados da consulta da proposta
	 * 
	 * @author WDEV
	 */
	private static final class DetalhePropostaRowMapper implements RowMapper<PropostaVO> {
		public PropostaVO mapRow(ResultSet es, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(es);
			PropostaVO proposta = new PropostaVO();
			proposta.setNumeroSequencial(resultSet.getLong("NSEQ_PPSTA_DNTAL"));
			proposta.setCanalVenda(new CanalVendaVO());
			proposta.getCanalVenda().setCodigo(resultSet.getInteger("CCANAL_VDA_DNTAL_INDVD"));
			proposta.setCodigo(Strings.maiusculas(resultSet.getString("CPPSTA_DNTAL_INDVD")));
			proposta.setDataCriacao(resultSet.getLocalDate("DT_CRIACAO_PROPOSTA"));
			proposta.setDataInicioCobranca(resultSet.getLocalDate("DINIC_COBR_PPSTA_INDVD"));
			proposta.setDataAdesao(resultSet.getLocalDate("DADSAO_PLANO_INDVD"));
			proposta.setDiaVencimento(resultSet.getInteger("NDIA_VCTO_PPSTA_INDVD"));
			proposta.setProtocolo(resultSet.getString("CPROT_PPSTA_CANAL_VDA"));
			proposta.setProponente(new ProponenteVO());
			proposta.getProponente().setCodigo(resultSet.getLong("CPROPN_DNTAL_INDVD"));
			proposta.getProponente().setCpf(Strings.normalizarCPFMaiorQueZero(resultSet.getString("NCPF_PROPN_DNTAL")));
			proposta.getProponente().setDataNascimento(resultSet.getLocalDate("DNASC_PROPN_DNTAL"));
			proposta.getProponente().setEmail(Strings.maiusculas(resultSet.getString("REMAIL_PROPN_DNTAL")));
			proposta.getProponente().setNome(Strings.maiusculas(resultSet.getString("IPROPN_DNTAL_INDVD")));
			proposta.getProponente().setContaCorrente(new ContaCorrenteVO(resultSet.getString("NCTA_CORR")));

			if (resultSet.getInteger("CBNEFC_DNTAL_INDVD") != null) {
				proposta.setTitular(new TitularVO());
				proposta.getTitular().setCodigo(resultSet.getInteger("CBNEFC_DNTAL_INDVD"));
			}

			if (resultSet.getInteger("CSUCUR_SEGDR_EMISR") != null) {
				proposta.setSucursalSeguradora(new SucursalSeguradoraVO(resultSet.getInteger("CSUCUR_SEGDR_EMISR")));
			}

			if (resultSet.getInteger("CSUCUR_SEGDR_BVP") != null) {
				proposta.setSucursalSeguradora(new SucursalSeguradoraVO(resultSet.getInteger("CSUCUR_SEGDR_BVP")));
			}

			proposta.setFormaPagamento(FormaPagamento.obterPorCodigo(resultSet.getInteger("CIND_FORMA_PGTO")));

			if (resultSet.getInteger("CAG_BCRIA") != null) {
				proposta.getProponente().getContaCorrente().setAgenciaBancaria(new AgenciaBancariaVO());
				proposta.getProponente().getContaCorrente().getAgenciaBancaria().setCodigo(
						resultSet.getInteger("CAG_BCRIA"));
			}

			proposta.setBanco(new BancoVO(resultSet.getInteger("CBCO")));
			proposta.setAgenciaProdutora(new AgenciaBancariaVO());
			proposta.getAgenciaProdutora().setCodigo(resultSet.getInteger("CAG_PROTR_PLANO"));
			proposta.getAgenciaProdutora().setBanco(new BancoVO(resultSet.getInteger("CBCO_PROTR_PLANO")));
			proposta.setCodigoMatriculaAssistente(resultSet.getString("CMATR_ASSTT_PROTR"));

			proposta.setCodigoMatriculaGerente(resultSet.getString("CMATR_GER_PRODT"));
			proposta.setTipoCobranca(TipoCobranca.buscaPor(resultSet.getInteger("CTPO_COBR_PLANO")));
			proposta.setDataUltimaAtualizacao(resultSet.getDateTime("DULT_ATULZ_REG"));
			proposta.setCodigoResponsavelUltimaAtualizacao(resultSet.getString("CRESP_ULT_ATULZ"));
			proposta.setSubsegmentacao(Subsegmentacao.buscaSubsegmentacaoPor(resultSet.getInteger("CSUB_SEGMT")));

			proposta.setPlanoVO(new PlanoVO());
			proposta.getPlanoVO().setCodigo(resultSet.getLong("CPLANO_DNTAL_INDVD"));

			return proposta;
		}
	}

	public void salvar(PropostaVO proposta, DefinicaoProposta definicaoProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT                              \n");
		sql.append(" INTO                                \n");
		sql.append("     DBPROD.PPSTA_DNTAL_INDVD        \n");
		sql.append("     (                               \n");
		sql.append("         NSEQ_PPSTA_DNTAL,           \n");
		sql.append("         CPPSTA_DNTAL_INDVD,         \n");
		sql.append("         CPROPN_DNTAL_INDVD,         \n");
		sql.append("         CIND_FORMA_PGTO,            \n");
		sql.append("         CBCO,                       \n");
		sql.append("         CAG_BCRIA,                  \n");
		sql.append("         NCTA_CORR,                  \n");
		sql.append("         CAG_PROTR_PLANO,            \n");
		sql.append("         CCANAL_VDA_DNTAL_INDVD,     \n");
		sql.append("         CPLANO_DNTAL_INDVD,         \n");
		sql.append("         DINIC_VGCIA,                \n");
		sql.append("         CMATR_ASSTT_PROTR,          \n");
		sql.append("         CSUCUR_SEGDR_EMISR,         \n");
		sql.append("         CSUCUR_SEGDR_BVP,           \n");
		sql.append("         CMATR_GER_PRODT,            \n");
		sql.append("         CTPO_COBR_PLANO,            \n");
		sql.append("         CTPO_COMIS_PROTR,           \n");
		sql.append("         CRESP_LEGAL_DNTAL,          \n");
		sql.append("         DULT_ATULZ_REG,             \n");
		sql.append("         CRESP_ULT_ATULZ,            \n");
		sql.append("         DADSAO_PLANO_INDVD,         \n");
		sql.append("         NDIA_VCTO_PPSTA_INDVD,      \n");
		sql.append("         CPROT_PPSTA_CANAL_VDA,      \n");
		sql.append("         CPOSTO_ATDMT_BCRIO,         \n");
		sql.append("         CSGL_TPO_POSTO,             \n");
		sql.append("         DINIC_COBR_PPSTA_INDVD,     \n");
		sql.append("         DEMIS_PPSTA_DNTAL_INDVD     \n");
		sql.append("     )                               \n");
		sql.append("     VALUES                          \n");
		sql.append("     (                               \n");
		sql.append("         :numeroSequencial,          \n");
		sql.append("         :codigo,                    \n");
		sql.append("         :proponente,                \n");
		sql.append("         :formaPagamento,            \n");
		sql.append("         :banco,                     \n");
		sql.append("         :agenciaBancaria,           \n");
		sql.append("         :contaCorrente,             \n");
		sql.append("         :agenciaProdutora,          \n");
		sql.append("         :canalVenda,                \n");
		sql.append("         :codigoPlano,               \n");
		sql.append("         :dataInicioVigencia,        \n");
		sql.append("         :matriculaAssistente,       \n");
		sql.append("         :sucursalEmissora,          \n");
		sql.append("         :sucursalBVP,               \n");
		sql.append("         :matriculaGerente,          \n");
		sql.append("         :tipoCobranca,              \n");
		sql.append("         :tipoComissao,              \n");
		sql.append("         :responsavelLegalTitular,   \n");
		sql.append("         CURRENT_TIMESTAMP,          \n");
		sql.append("         :usuarioUltimaAtualizacao , \n");
		sql.append("         :dataAdesao,                \n");
		sql.append("         :diaVencimento,             \n");
		sql.append("         :protocolo,                 \n");
		sql.append("         :codigoPostoAtendimento,    \n");
		sql.append("         :siglaPostoAtendimento,     \n");
		sql.append("         :dataInicioCobranca,        \n");
		sql.append("         CURRENT_DATE                \n");
		sql.append("      )                              \n");

		MapSqlParameterSource params = preencherParametrosProposta(proposta);
		params.addValue("numeroSequencial", definicaoProposta.getSequencial());
		params.addValue("codigo", definicaoProposta.getCodigo());
		LOGGER.error("INFO: INSERT SALVAR PROPOSTA : " + sql.toString() + "PAR�METROS: " + params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public Long obterSequencialProposta() {
		return getJdbcTemplate().queryForLong(
				"SELECT NEXT VALUE FOR DBPROD.SQ01_PPSTA_DNTAL_INDVD FROM SYSIBM.SYSDUMMY1",
				new MapSqlParameterSource());
		//TODO postgres
		//return getJdbcTemplate().queryForLong("SELECT COALESCE(MAX(nseq_ppsta_dntal),0)+1 FROM dbprod.ppsta_DNTAL_indvd", new MapSqlParameterSource());

	}

	public Integer consultarQuantidadePropostasPorDataAtualParaBeneficiario(String cpf) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD)");
		sql.append(" FROM DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL");
		sql.append(" INNER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD ON (BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)");
		sql.append(" WHERE DATE(MOVTO_PPSTA_DNTAL.DULT_ATULZ_REG) = CURRENT DATE");
		sql.append(" AND (MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD = :situacaoRascunho or MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD = :situacaoPendente)");
		sql.append(" AND BNEFC_DNTAL_INDVD.NCPF_BNEFC_DNTAL = :cpfBeneficiario");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("situacaoRascunho", SituacaoProposta.RASCUNHO.getCodigo());
		params.addValue("situacaoPendente", SituacaoProposta.PENDENTE.getCodigo());
		params.addValue("cpfBeneficiario", Strings.desnormalizarCPF(cpf));

		return getJdbcTemplate().queryForInt(sql.toString(), params);
	}

	public boolean possuiStatusCancelada(Long numeroSequencial) {
		boolean possuiStatusCancelada = false;
		Integer quantidadeStatusCancelada = 0;

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(*) FROM DBPROD.MOVTO_PPSTA_DNTAL WHERE NSEQ_PPSTA_DNTAL = :numeroSequencial AND (CSIT_DNTAL_INDVD = 5 OR CSIT_DNTAL_INDVD = 7)");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("numeroSequencial", numeroSequencial);

		quantidadeStatusCancelada = getJdbcTemplate().queryForInt(sql.toString(), params);
		if (quantidadeStatusCancelada > 0) {
			possuiStatusCancelada = true;
		}
		return possuiStatusCancelada;
	}

	public boolean possuiStatusImplantada(Long numeroSequencial) {

		boolean possuiStatusImplantada = false;
		Integer quantidadeStatusImplantada = 0;
		
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(*) FROM DBPROD.MOVTO_PPSTA_DNTAL WHERE NSEQ_PPSTA_DNTAL = :numeroSequencial AND (CSIT_DNTAL_INDVD = 6)");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("numeroSequencial", numeroSequencial);

		quantidadeStatusImplantada = getJdbcTemplate().queryForInt(sql.toString(), params);
		if (quantidadeStatusImplantada > 0) {
			possuiStatusImplantada = true;
			
		}
		return possuiStatusImplantada;
	}
	public boolean possuiStatusImplantadaProponente(Long numeroSequencial) {

		boolean possuiStatusImplantada = false;
		Integer quantidadeStatusImplantada = 0;
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(*) FROM DBPROD.MOVTO_PPSTA_DNTAL WHERE NSEQ_PPSTA_DNTAL = :numeroSequencial AND (CSIT_DNTAL_INDVD = 6) OR (CSIT_DNTAL_INDVD = 4)   ");
				sql.append(" OR (CSIT_DNTAL_INDVD = 1) OR (CSIT_DNTAL_INDVD = 3)                  ");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("numeroSequencial", numeroSequencial);

		quantidadeStatusImplantada = getJdbcTemplate().queryForInt(sql.toString(), params);
		if (quantidadeStatusImplantada > 0) {
			possuiStatusImplantada = true;
		}
		return possuiStatusImplantada;
	}
	
	public Integer consultaMovimentacao(Long numeroSequencial) {
		// TODO Auto-generated method stub
		Integer valTipoPrentesco = null;
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT  CSIT_DNTAL_INDVD FROM DBPROD.MOVTO_PPSTA_DNTAL WHERE NSEQ_PPSTA_DNTAL = :numeroSequencial "
				+ "ORDER BY DULT_ATULZ_REG DESC FETCH FIRST 1 ROWS ONLY  ");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("numeroSequencial", numeroSequencial);
		
		valTipoPrentesco = getJdbcTemplate().queryForInt(sql.toString(), params);
	
		if(valTipoPrentesco != null){
			return valTipoPrentesco;
		}else{
			return valTipoPrentesco;
		}
	}
	
	public Integer consultarParentescoAtivo(Long numeroSequencial) {
		// TODO Auto-generated method stub
		Integer valTipoPrentesco = null;
		//boolean tipoParentesco = false;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(*) CGRAU_PARNT_BNEFC_PROPN FROM DBPROD.BNEFC_DNTAL_INDVD WHERE NSEQ_PPSTA_DNTAL = :numeroSequencial AND CTPO_BNEFC_DNTAL = :codigoBeneficiario ");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoBeneficiario", 1);
		params.addValue("numeroSequencial", numeroSequencial);
		
		valTipoPrentesco = getJdbcTemplate().queryForInt(sql.toString(), params);
	
		if(valTipoPrentesco != null){
			return valTipoPrentesco;
		}else{
			return valTipoPrentesco;
		}
	}
	
	public Integer consultarTipoParentesco(Long numeroSequencial) {
		// TODO Auto-generated method stub
		Integer valTipoPrentesco = null;
		//boolean tipoParentesco = false;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT CGRAU_PARNT_BNEFC_PROPN FROM DBPROD.BNEFC_DNTAL_INDVD WHERE NSEQ_PPSTA_DNTAL = :numeroSequencial AND CTPO_BNEFC_DNTAL = :codigoBeneficiario ");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoBeneficiario", 1);
		params.addValue("numeroSequencial", numeroSequencial);
		
		valTipoPrentesco = getJdbcTemplate().queryForInt(sql.toString(), params);
	
		if(valTipoPrentesco != null){
			return valTipoPrentesco;
		}else{
			return valTipoPrentesco;
		}
		
		
		
	}
	public Integer consultarTipoParentescoDepen(Long numeroSequencial) {
		// TODO Auto-generated method stub
		Integer valTipoPrentesco = null;
		//boolean tipoParentesco = false;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT CGRAU_PARNT_BNEFC_PROPN FROM DBPROD.BNEFC_DNTAL_INDVD WHERE NSEQ_PPSTA_DNTAL = :numeroSequencial AND CTPO_BNEFC_DNTAL = :codigoBeneficiario");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("numeroSequencial", numeroSequencial);
		params.addValue("codigoBeneficiario", 2);
		valTipoPrentesco = getJdbcTemplate().queryForInt(sql.toString(), params);
		
		if(valTipoPrentesco != null){
			return valTipoPrentesco;
		}else{
			return valTipoPrentesco;
		}
		
	}

	public Long obterSequencialPorProtocolo(String protocolo, Integer canalVenda) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT NSEQ_PPSTA_DNTAL FROM DBPROD.PPSTA_DNTAL_INDVD WHERE CPROT_PPSTA_CANAL_VDA = :protocolo ");
		sql.append(" AND CCANAL_VDA_DNTAL_INDVD = :canalVenda FETCH FIRST 1 ROWS ONLY ");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("protocolo", protocolo);
		params.addValue("canalVenda", canalVenda);
		try {
			return getJdbcTemplate().queryForLong(sql.toString(), params);

		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma proposta encontrada para protocolo e canal de venda informados", e);
			return null;
		}

	}

	public void atualizarResponsavelPorAtualizacao(Long numeroSequencial, String responsavelAtualizacao) {

		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE DBPROD.PPSTA_DNTAL_INDVD SET");
		sql.append("  CRESP_ULT_ATULZ = :usuarioUltimaAtualizacao");
		sql.append(" WHERE NSEQ_PPSTA_DNTAL = :sequencialProposta");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("usuarioUltimaAtualizacao", responsavelAtualizacao);
		params.addValue("sequencialProposta", numeroSequencial);
		LOGGER.error("INFO: ATUALIZAR RESPONS�VEL POR ATUALIZA��O: " + sql.toString() + " PAR�METROS: "
				+ params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public void atualizar(PropostaVO proposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE DBPROD.PPSTA_DNTAL_INDVD SET");
		sql.append(" CPROPN_DNTAL_INDVD = :proponente");
		sql.append(" , CIND_FORMA_PGTO = :formaPagamento");
		sql.append(" , CBCO = :banco");
		sql.append(" , CAG_BCRIA = :agenciaBancaria");
		sql.append(" , NCTA_CORR = :contaCorrente");
		sql.append(" , CBCO_PROTR_PLANO = :bancoProdutor");
		sql.append(" , CAG_PROTR_PLANO = :agenciaProdutora");
		sql.append(" , CMATR_ASSTT_PROTR = :matriculaAssistente");
		sql.append(" , CSUCUR_SEGDR_EMISR = :sucursalEmissora");
		sql.append(" , CSUCUR_SEGDR_BVP = :sucursalBVP");
		sql.append(" , CMATR_GER_PRODT = :matriculaGerente");
		sql.append(" , CTPO_COBR_PLANO = :tipoCobranca");
		sql.append(" , CTPO_COMIS_PROTR = :tipoComissao");
		sql.append(" , DULT_ATULZ_REG = CURRENT_TIMESTAMP");
		sql.append(" , CRESP_LEGAL_DNTAL = :responsavelLegalTitular");
		sql.append(" , CSUB_SEGMT = :subsegmentacao");
		if (proposta.getDataInicioCobranca() != null) {
			sql.append(", DINIC_COBR_PPSTA_INDVD = :dataInicioCobranca");
		}
		sql.append(" , CRESP_ULT_ATULZ = :usuarioUltimaAtualizacao");

		sql.append(" WHERE NSEQ_PPSTA_DNTAL = :sequencialProposta");

		MapSqlParameterSource params = preencherParametrosProposta(proposta);
		params.addValue("usuarioUltimaAtualizacao", "");
		params.addValue("sequencialProposta", proposta.getNumeroSequencial());

		LOGGER.error("INFO: ATUALIZAR PROPOSTA: " + sql.toString() + " PAR�METROS: " + params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	private MapSqlParameterSource preencherParametrosProposta(PropostaVO proposta) {
		MapSqlParameterSource params = new MapSqlParameterSource();

		if (proposta.getProponente() != null && proposta.getProponente().getCodigo() != null) {
			params.addValue("proponente", proposta.getProponente().getCodigo());
		} else {
			params.addValue("proponente", null);
		}

		if (proposta.getFormaPagamento() != null) {
			params.addValue("formaPagamento", proposta.getFormaPagamento().getCodigo());
		} else {
			params.addValue("formaPagamento", null);
		}
		if (proposta.getBanco() != null && proposta.getBanco().getCodigo() != null) {
			params.addValue("banco", proposta.getBanco().getCodigo());
		} else {
			params.addValue("banco", null);
		}

		params.addValue("agenciaProdutora", null);
		params.addValue("bancoProdutor", null);
		AgenciaBancariaVO agenciaProdutora = proposta.getAgenciaProdutora();
		if (agenciaProdutora != null) {
			if (agenciaProdutora.getCodigo() != null && agenciaProdutora.getCodigo().intValue() != 0) {
				params.addValue("agenciaProdutora", agenciaProdutora.getCodigo());
				if (agenciaProdutora.getBanco() != null && 
					agenciaProdutora.getBanco().getCodigo()!= null && 
					agenciaProdutora.getBanco().getCodigo().intValue() != 0) {
					params.addValue("bancoProdutor", agenciaProdutora.getBanco().getCodigo());
				}
			}
		}

		params.addValue("matriculaAssistente", proposta.getCodigoMatriculaAssistente());
		params.addValue("matriculaGerente", proposta.getCodigoMatriculaGerente());
		if (proposta.getTipoCobranca() != null && proposta.getTipoCobranca().getCodigo() != null) {
			params.addValue("tipoCobranca", proposta.getTipoCobranca().getCodigo());
		} else {
			params.addValue("tipoCobranca", null);
		}
		if (proposta.getCanalVenda() == null || proposta.getCanalVenda().getCodigo() == 1) {
			params.addValue("tipoComissao", obterTipoComissao100PorCentoCorretor(proposta));
		}else if(proposta.getCanalVenda().getCodigo() == 11){
			params.addValue("tipoComissao", tipoComissaoServiceFacade.obterTipoComissao(proposta));
		}else if(proposta.getCanalVenda().getCodigo() == 8){
			params.addValue("tipoComissao", tipoComissaoServiceFacade.obterTipoComissao(proposta));
		}else {
			params.addValue("tipoComissao", proposta.getTipoComissao().getCodigo());
		}
		SucursalSeguradoraVO sucursalSeguradora = proposta.getSucursalSeguradora();

		params.addValue("sucursalEmissora", null);
		params.addValue("sucursalBVP", null);

		if (sucursalSeguradora.isEmissora() || sucursalSeguradora.isCorporate()) {
			params.addValue("sucursalEmissora", sucursalSeguradora.getCodigo());
		} else if (sucursalSeguradora.isBVP()) {
			params.addValue("sucursalBVP", sucursalSeguradora.getCodigo());
		}

		if (FormaPagamento.BOLETO_BANCARIO.equals(proposta.getFormaPagamento())
				|| FormaPagamento.BOLETO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento())
				|| FormaPagamento.CARTAO_CREDITO.equals(proposta.getFormaPagamento())) {
			params.addValue("agenciaBancaria", null);
			params.addValue("contaCorrente", null);

			params.addValue("codigoPostoAtendimento", null);
			params.addValue("siglaPostoAtendimento", null);
		} else if (FormaPagamento.DEBITO_AUTOMATICO.equals(proposta.getFormaPagamento())
				|| FormaPagamento.BOLETO_DEMAIS_DEBITO.equals(proposta.getFormaPagamento())
				|| FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento())
				|| FormaPagamento.DEBITO_DEMAIS_BOLETO.equals(proposta.getFormaPagamento())) {
			params.addValue("agenciaBancaria", proposta.getProponente().getContaCorrente().getAgenciaBancaria()
					.getCodigo());
			params.addValue("contaCorrente", proposta.getProponente().getContaCorrente()
					.getNumeroComDigitoVerificador());

			params.addValue("codigoPostoAtendimento", proposta.getProponente().getContaCorrente()
					.getCodigoPostoAtendimento());
			params.addValue("siglaPostoAtendimento", proposta.getProponente().getContaCorrente()
					.getSiglaPostoAtendimento());
		} else {
			params.addValue("agenciaBancaria", null);
			params.addValue("contaCorrente", null);
			params.addValue("codigoPostoAtendimento", null);
			params.addValue("siglaPostoAtendimento", null);
		}

		if (proposta.getResponsavelLegal() != null) {
			params.addValue("responsavelLegalTitular", proposta.getResponsavelLegal().getCodigo());
		} else {
			params.addValue("responsavelLegalTitular", null);
		}

		if (proposta.getSubsegmentacao() != null) {
			params.addValue("subsegmentacao", proposta.getSubsegmentacao().getCodigo());
		} else {
			params.addValue("subsegmentacao", null);
		}

		params.addValue("usuarioUltimaAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());

		if (proposta.getCanalVenda() != null) {
			params.addValue("canalVenda", proposta.getCanalVenda().getCodigo());
		} else {
			params.addValue("canalVenda", null);
		}

		if (proposta.getPlanoVO() != null) {
			params.addValue("codigoPlano", proposta.getPlanoVO().getCodigo());
			params.addValue("dataInicioVigencia", new Date(proposta.getPlanoVO().getDataInicioVigencia().getMillis()));
		} else {
			params.addValue("codigoPlano", null);
			params.addValue("dataInicioVigencia", null);
		}

		if (proposta.getDataAdesao() != null) {
			params.addValue("dataAdesao", String.valueOf(proposta.getDataAdesao()));
		} else {
			params.addValue("dataAdesao", null);
		}

		if (proposta.getDiaVencimento() != null) {
			params.addValue("diaVencimento", proposta.getDiaVencimento());
		} else {
			params.addValue("diaVencimento", null);
		}

		params.addValue("protocolo", proposta.getProtocolo());

		if (proposta.getDataInicioCobranca() != null) {
			params.addValue("dataInicioCobranca", String.valueOf(proposta.getDataInicioCobranca()));
		} else {
			params.addValue("dataInicioCobranca", new java.util.Date());
		}
		return params;
	}

	private Integer obterTipoComissao100PorCentoCorretor(PropostaVO proposta) {

		Integer tipoComissao = null;

		if (proposta.getSucursalSeguradora().getTipo() == null) {
			return tipoComissao;
		}
		

		if (br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoSucursalSeguradora.EMISSORA.name().equalsIgnoreCase(
				proposta.getSucursalSeguradora().getTipo().name())
				|| br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoSucursalSeguradora.CORPORATE.name()
						.equalsIgnoreCase(proposta.getSucursalSeguradora().getTipo().name())) {

			if (proposta.getTipoCobranca() != null && proposta.getTipoCobranca().getCodigo() != null) {
				if (proposta.getTipoCobranca().getCodigo().equals(1)) {
					if(proposta.getCorretor().getCpfCnpj().equals("24878265000155") || proposta.getCorretor().getCpfCnpj().equals("19914513000136")){
						tipoComissao = 28;
					}else{
						tipoComissao = 1;
					}
					
				} else if (proposta.getTipoCobranca().getCodigo().equals(2)) {
					tipoComissao = 5;
				}
			}
		} else if (br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoSucursalSeguradora.BVP.name().equalsIgnoreCase(
				proposta.getSucursalSeguradora().getTipo().name())) {

			if (proposta.getTipoCobranca() != null && proposta.getTipoCobranca().getCodigo() != null) {

				if (proposta.getCorretorMaster() != null && proposta.getCorretorMaster().getCpd() != null
						&& proposta.getCorretorMaster().getCpd() > 0L) {
					if (proposta.getTipoCobranca().getCodigo().equals(1)) {
						tipoComissao = 2;
					} else if (proposta.getTipoCobranca().getCodigo().equals(2)) {
						tipoComissao = 7;
					}
				} else {
					if (proposta.getTipoCobranca().getCodigo().equals(1)) {
						tipoComissao = 3;
					} else if (proposta.getTipoCobranca().getCodigo().equals(2)) {
						tipoComissao = 6;
					}
				}
			}
		}

		return tipoComissao;
	}

	public List<PropostaVO> buscarPropostaParaArquivo(LocalDate dataInicialPesquisaMovimento,
			SituacaoProposta situacaoProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT DISTINCT PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL,");
		sql.append(" PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD,");
		sql.append(" PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD,");
		sql.append(" PPSTA_DNTAL_INDVD.CIND_FORMA_PGTO,");
		sql.append(" PPSTA_DNTAL_INDVD.CBCO,");
		sql.append(" PPSTA_DNTAL_INDVD.CAG_BCRIA,");
		sql.append(" PPSTA_DNTAL_INDVD.NCTA_CORR,");
		sql.append(" PPSTA_DNTAL_INDVD.CBCO_PROTR_PLANO,");
		sql.append(" PPSTA_DNTAL_INDVD.CAG_PROTR_PLANO,");
		sql.append(" PPSTA_DNTAL_INDVD.CMATR_ASSTT_PROTR,");
		sql.append(" CASE WHEN PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR IS NOT NULL THEN PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR");
		sql.append(" WHEN PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP IS NOT NULL THEN PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP");
		sql.append(" END AS SUCURSAL,");
		sql.append(" PPSTA_DNTAL_INDVD.CMATR_GER_PRODT,");
		sql.append(" PPSTA_DNTAL_INDVD.CTPO_COBR_PLANO,");
		sql.append(" PPSTA_DNTAL_INDVD.DULT_ATULZ_REG,");
		sql.append(" PPSTA_DNTAL_INDVD.CRESP_ULT_ATULZ,");
		sql.append(" PPSTA_DNTAL_INDVD.CSUB_SEGMT,");
		sql.append(" MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD,");
		sql.append(" MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA,");
		sql.append(" PROPN_DNTAL_INDVD.IPROPN_DNTAL_INDVD,");
		sql.append(" PROPN_DNTAL_INDVD.NCPF_PROPN_DNTAL,");
		sql.append(" PROPN_DNTAL_INDVD.REMAIL_PROPN_DNTAL,");
		sql.append(" PROTR_PPSTA_DNTAL.CCPD_PROTR,");
		sql.append(" RESP_LEGAL_DNTAL.NCPF_RESP_LEGAL,");
		sql.append(" RESP_LEGAL_DNTAL.DNASC_RESP_LEGAL,");
		sql.append(" RESP_LEGAL_DNTAL.IRESP_LEGAL_DNTAL,");
		sql.append(" RESP_LEGAL_DNTAL.CRESP_LEGAL_DNTAL,");
		sql.append(" ENDER_DNTAL_INDVD.CENDER_DNTAL_INDVD,");
		sql.append(" ENDER_DNTAL_INDVD.ILOGDR_ENDER_COTAC,");
		sql.append(" ENDER_DNTAL_INDVD.NLOGDR_ENDER_COTAC,");
		sql.append(" ENDER_DNTAL_INDVD.RCOMPL_ENDER_COTAC,");
		sql.append(" ENDER_DNTAL_INDVD.IBAIRO,");
		sql.append(" ENDER_DNTAL_INDVD.ICIDDE,");
		sql.append(" ENDER_DNTAL_INDVD.CSGL_UF,");
		sql.append(" ENDER_DNTAL_INDVD.CNRO_CEP");
		sql.append(" FROM DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD");
		sql.append(" JOIN DBPROD.PROPN_DNTAL_INDVD PROPN_DNTAL_INDVD ON PROPN_DNTAL_INDVD.CPROPN_DNTAL_INDVD = PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD");
		sql.append(" JOIN DBPROD.PROTR_PPSTA_DNTAL PROTR_PPSTA_DNTAL ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL AND PROTR_PPSTA_DNTAL.CTPO_PROTR_PPSTA = :tipoProdutor)");
		sql.append(" JOIN DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL ON PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL");
		sql.append(" LEFT JOIN DBPROD.RESP_LEGAL_DNTAL RESP_LEGAL_DNTAL ON PPSTA_DNTAL_INDVD.CRESP_LEGAL_DNTAL = RESP_LEGAL_DNTAL.CRESP_LEGAL_DNTAL");
		sql.append(" LEFT JOIN DBPROD.ENDER_RESP_PPSTA ENDER_RESP_PPSTA on ENDER_RESP_PPSTA.CRESP_LEGAL_DNTAL = RESP_LEGAL_DNTAL.CRESP_LEGAL_DNTAL");
		sql.append(" LEFT JOIN DBPROD.ENDER_DNTAL_INDVD ENDER_DNTAL_INDVD on ENDER_DNTAL_INDVD.CENDER_DNTAL_INDVD = ENDER_RESP_PPSTA.CENDER_DNTAL_INDVD");
		sql.append(" WHERE MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD = :codigoSituacaoProposta");
		sql.append(" AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA >= :dataInicialPesquisaMovimento");
		sql.append(" AND PROTR_PPSTA_DNTAL.ncpf_cnpj_protr <> 404040404040404");
		sql.append(" AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA =");
		sql.append(" (SELECT MAX(movimento.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL movimento WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = movimento.NSEQ_PPSTA_DNTAL )");

		//TODO Verificar o filtro produtor.ncpf_cnpj_protr <> 404040404040404

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoSituacaoProposta", situacaoProposta.getCodigo());
		params.addValue("dataInicialPesquisaMovimento", dataInicialPesquisaMovimento.toDateTimeAtStartOfDay().toDate());
		params.addValue("tipoProdutor", TipoProdutor.CORRETOR.getCodigo());

		LOGGER.error("INFO: BUSCAR PROPOSTA PARA ARQUIVO: " + sql.toString() + " PAR�METROS: " + params.getValues());
		return getJdbcTemplate().query(sql.toString(), params, new DetalheCompletoMovimentoPropostaRowMapper());
	}

	/**
	 * Mapeamento de dados da listagem de proposta
	 * 
	 * @author WDEV
	 */
	private static final class DetalheCompletoMovimentoPropostaRowMapper implements RowMapper<PropostaVO> {
		public PropostaVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
			PropostaVO proposta = new PropostaVO();
			proposta.setNumeroSequencial(resultSet.getLong("NSEQ_PPSTA_DNTAL"));
			proposta.setCodigo(Strings.maiusculas(resultSet.getString("CPPSTA_DNTAL_INDVD")));

			proposta.setProponente(new ProponenteVO());
			proposta.getProponente().setCodigo(resultSet.getLong("CPROPN_DNTAL_INDVD"));
			proposta.getProponente().setContaCorrente(new ContaCorrenteVO(resultSet.getString("NCTA_CORR")));
			proposta.getProponente().getContaCorrente().setAgenciaBancaria(new AgenciaBancariaVO());
			proposta.getProponente().getContaCorrente().getAgenciaBancaria().setCodigo(
					resultSet.getInteger("CAG_BCRIA"));
			proposta.getProponente().setNome(Strings.maiusculas(resultSet.getString("IPROPN_DNTAL_INDVD")));
			proposta.getProponente().setCpf(resultSet.getString("NCPF_PROPN_DNTAL"));
			proposta.getProponente().setEmail(Strings.maiusculas(resultSet.getString("REMAIL_PROPN_DNTAL")));

			proposta.setResponsavelLegal(new ResponsavelLegalVO());
			proposta.getResponsavelLegal().setCodigo(resultSet.getLong("CRESP_LEGAL_DNTAL"));
			proposta.getResponsavelLegal().setNome(Strings.maiusculas(resultSet.getString("IRESP_LEGAL_DNTAL")));
			proposta.getResponsavelLegal().setCpf(resultSet.getString("NCPF_RESP_LEGAL"));
			proposta.getResponsavelLegal().setDataNascimento(resultSet.getLocalDate("DNASC_RESP_LEGAL"));

			if (resultSet.getLong("CENDER_DNTAL_INDVD") != null) {
				proposta.getResponsavelLegal().setEndereco(new EnderecoRowMapper().mapRow(rs, rowNum));
			}

			proposta.setFormaPagamento(FormaPagamento.obterPorCodigo(resultSet.getInteger("CIND_FORMA_PGTO")));

			proposta.setBanco(new BancoVO(resultSet.getInteger("CBCO")));

			proposta.setAgenciaProdutora(new AgenciaBancariaVO());
			proposta.getAgenciaProdutora().setCodigo(resultSet.getInteger("CAG_PROTR_PLANO"));
			proposta.getAgenciaProdutora().setBanco(new BancoVO(resultSet.getInteger("CBCO_PROTR_PLANO")));

			proposta.setCodigoMatriculaAssistente(resultSet.getString("CMATR_ASSTT_PROTR"));
			proposta.setSucursalSeguradora(new SucursalSeguradoraVO(resultSet.getInteger("SUCURSAL")));
			proposta.setCodigoMatriculaGerente(resultSet.getString("CMATR_GER_PRODT"));
			proposta.setTipoCobranca(TipoCobranca.buscaPor(resultSet.getInteger("CTPO_COBR_PLANO")));
			proposta.setDataUltimaAtualizacao(resultSet.getDateTime("DULT_ATULZ_REG"));
			proposta.setCodigoResponsavelUltimaAtualizacao(resultSet.getString("CRESP_ULT_ATULZ"));
			proposta.setSubsegmentacao(Subsegmentacao.buscaSubsegmentacaoPor(resultSet.getInteger("CSUB_SEGMT")));

			proposta.setMovimento(new MovimentoPropostaVO());
			proposta.getMovimento().setSituacao(
					SituacaoProposta.obterPorCodigo(resultSet.getInteger("CSIT_DNTAL_INDVD")));
			proposta.getMovimento().setDataInicio(resultSet.getDateTime("DINIC_MOVTO_PPSTA"));

			proposta.setCorretor(new CorretorVO());
			proposta.getCorretor().setCpd(resultSet.getInteger("CCPD_PROTR"));

			return proposta;
		}
	}

	public void removerResponsavelLegal(PropostaVO proposta) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE DBPROD.PPSTA_DNTAL_INDVD SET CRESP_LEGAL_DNTAL = null WHERE NSEQ_PPSTA_DNTAL = :sequencialProposta");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequencialProposta", proposta.getNumeroSequencial());

		LOGGER.error("INFO: REMOVER RESPONS�VEL LEGAL: " + sql.toString() + " PAR�METROS: " + params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public List<PropostaVO> listarPropostasPorProponente(String cpfProponente, Integer canalVenda,
			DateTime periodoInicial, DateTime periodoFinal) {

		List<PropostaVO> listaDePropostas = null;

		StringBuilder sql = new StringBuilder();
		sql.append(
				"SELECT                                                                                                      \n")
				.append("    PROPOSTA.NSEQ_PPSTA_DNTAL,                                                                             \n")
				.append("    PROPOSTA.CPPSTA_DNTAL_INDVD,                                                                           \n")
				.append("    PROPOSTA.CPROPN_DNTAL_INDVD,                                                                           \n")
				.append("    PROPOSTA.CIND_FORMA_PGTO,                                                                              \n")
				.append("    PROPOSTA.CBCO,                                                                                         \n")
				.append("    PROPOSTA.CAG_BCRIA,                                                                                    \n")
				.append("    PROPOSTA.NCTA_CORR,                                                                                    \n")
				.append("    PROPOSTA.CBCO_PROTR_PLANO,                                                                             \n")
				.append("    PROPOSTA.CAG_PROTR_PLANO,                                                                              \n")
				.append("    PROPOSTA.CMATR_ASSTT_PROTR,                                                                            \n")
				.append("    PROPOSTA.CSUCUR_SEGDR_EMISR,                                                                           \n")
				.append("    PROPOSTA.CSUCUR_SEGDR_BVP,                                                                             \n")
				.append("    PROPOSTA.CMATR_GER_PRODT,                                                                              \n")
				.append("    PROPOSTA.CTPO_COBR_PLANO,                                                                              \n")
				.append("    PROPOSTA.CTPO_COMIS_PROTR,                                                                             \n")
				.append("    PROPOSTA.CSUB_SEGMT,                                                                                   \n")
				.append("    PROPOSTA.NDIA_VCTO_PPSTA_INDVD,                                                                        \n")
				.append("    PROPOSTA.CPROT_PPSTA_CANAL_VDA,                                                                        \n")
				.append("    PROPOSTA.DADSAO_PLANO_INDVD,                                                                           \n")
				.append("    BENEFICIARIO.CBNEFC_DNTAL_INDVD,                                                                       \n")
				.append("    BENEFICIARIO.IBNEFC_DNTAL_INDVD,                                                                       \n")
				.append("    PROPONENTE.NCPF_PROPN_DNTAL,                                                                           \n")
				.append("    PROPONENTE.DNASC_PROPN_DNTAL,                                                                          \n")
				.append("    PROPONENTE.REMAIL_PROPN_DNTAL,                                                                         \n")
				.append("    PROPONENTE.IPROPN_DNTAL_INDVD,                                                                         \n")
				.append("    CANAL.CCANAL_VDA_DNTAL_INDVD,                                                                          \n")
				.append("    CANAL.RCANAL_VDA_DNTAL_INDVD,                                                                          \n")
				.append("    (                                                                                                      \n")
				.append("        SELECT                                                                                             \n")
				.append("            MIN(MOVIMENTO.DULT_ATULZ_REG)                                                                  \n")
				.append("        FROM                                                                                               \n")
				.append("            DBPROD.MOVTO_PPSTA_DNTAL MOVIMENTO                                                             \n")
				.append("        WHERE                                                                                              \n")
				.append("            PROPOSTA.NSEQ_PPSTA_DNTAL = MOVIMENTO.NSEQ_PPSTA_DNTAL) AS DT_CRIACAO_PROPOSTA                 \n")
				//			.append("    (                                                                                                      \n")
				//			.append("        SELECT                                                                                             \n")
				//			.append("            CSIT_DNTAL_INDVD                                                                               \n")
				//			.append("        FROM                                                                                               \n")
				//			.append("            DBPROD.MOVTO_PPSTA_DNTAL MOVIMENTO                                                             \n")
				//			.append("        WHERE                                                                                              \n")
				//			.append("            PROPOSTA.NSEQ_PPSTA_DNTAL = MOVIMENTO.NSEQ_PPSTA_DNTAL                                         \n")
				//			.append("        ORDER BY                                                                                           \n")
				//			.append("            MOVIMENTO.DULT_ATULZ_REG DESC                                                                  \n")
				//			.append("        FETCH                                                                                              \n")
				//			.append("            FIRST 1 ROWS ONLY ) AS SITUACAO                                                                \n")
				.append("FROM                                                                                                       \n")
				.append("    DBPROD.PROPN_DNTAL_INDVD PROPONENTE                                                                    \n")
				.append("LEFT OUTER JOIN                                                                                            \n")
				.append("    DBPROD.PPSTA_DNTAL_INDVD PROPOSTA                                                                      \n")
				.append("ON                                                                                                         \n")
				.append("    PROPOSTA.CPROPN_DNTAL_INDVD = PROPONENTE.CPROPN_DNTAL_INDVD                                            \n")
				.append("LEFT OUTER JOIN                                                                                            \n")
				.append("    DBPROD.BNEFC_DNTAL_INDVD BENEFICIARIO                                                                  \n")
				.append("ON                                                                                                         \n")
				.append("    BENEFICIARIO.NSEQ_PPSTA_DNTAL = PROPOSTA.NSEQ_PPSTA_DNTAL                                              \n")
				.append("AND BENEFICIARIO.CTPO_BNEFC_DNTAL = 1                                                                      \n")
				.append("LEFT OUTER JOIN                                                                                            \n")
				.append("    DBPROD.CANAL_VDA_DNTAL_INDVD CANAL                                                                     \n")
				.append("ON                                                                                                         \n")
				.append("    PROPOSTA.CCANAL_VDA_DNTAL_INDVD = CANAL.CCANAL_VDA_DNTAL_INDVD                                         \n")
				.append("WHERE                                                                                                      \n")
				.append("    PROPONENTE.NCPF_PROPN_DNTAL = :CPF_PROPONENTE                                                          \n")
				.append("AND PROPOSTA.CCANAL_VDA_DNTAL_INDVD = :CANAL                                                               \n")
				.append("AND (                                                                                                      \n")
				.append("        SELECT                                                                                             \n")
				.append("            MIN(MOVIMENTO.DULT_ATULZ_REG)                                                                  \n")
				.append("        FROM                                                                                               \n")
				.append("            DBPROD.MOVTO_PPSTA_DNTAL MOVIMENTO                                                             \n")
				.append("        WHERE                                                                                              \n")
				.append("            PROPOSTA.NSEQ_PPSTA_DNTAL = MOVIMENTO.NSEQ_PPSTA_DNTAL) >= :PERIODO_INICIAL                    \n")
				.append("AND (                                                                                                      \n")
				.append("        SELECT                                                                                             \n")
				.append("            MIN(MOVIMENTO.DULT_ATULZ_REG)                                                                  \n")
				.append("        FROM                                                                                               \n")
				.append("            DBPROD.MOVTO_PPSTA_DNTAL MOVIMENTO                                                             \n")
				.append("        WHERE                                                                                              \n")
				.append("            PROPOSTA.NSEQ_PPSTA_DNTAL = MOVIMENTO.NSEQ_PPSTA_DNTAL) <= :PERIODO_FINAL                      \n");

		//		sql.append(" SELECT distinct 																																							\n");
		//		sql.append(" PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL, PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CIND_FORMA_PGTO, 						\n");
		//		sql.append(" PPSTA_DNTAL_INDVD.CBCO, PPSTA_DNTAL_INDVD.CAG_BCRIA, PPSTA_DNTAL_INDVD.NCTA_CORR, PPSTA_DNTAL_INDVD.CBCO_PROTR_PLANO, PPSTA_DNTAL_INDVD.CAG_PROTR_PLANO, 					\n");
		//		sql.append(" PPSTA_DNTAL_INDVD.CMATR_ASSTT_PROTR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_EMISR, PPSTA_DNTAL_INDVD.CSUCUR_SEGDR_BVP, PPSTA_DNTAL_INDVD.CMATR_GER_PRODT,							\n");
		//		sql.append(" PPSTA_DNTAL_INDVD.CTPO_COBR_PLANO, PPSTA_DNTAL_INDVD.CTPO_COMIS_PROTR,  PPSTA_DNTAL_INDVD.CSUB_SEGMT, PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD, 																		\n");
		//		sql.append(" PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd, propn.NCPF_PROPN_DNTAL, propn.DNASC_PROPN_DNTAL , propn.REMAIL_PROPN_DNTAL,  propn.IPROPN_DNTAL_INDVD , 							\n");
		//		sql.append(" PPSTA_DNTAL_INDVD.DADSAO_PLANO_INDVD, BNEFC_DNTAL_INDVD.CBNEFC_DNTAL_INDVD,BNEFC_DNTAL_INDVD.IBNEFC_DNTAL_INDVD,  															\n");
		//		sql.append(" PPSTA_DNTAL_INDVD.NDIA_VCTO_PPSTA_INDVD,PPSTA_DNTAL_INDVD.CPROT_PPSTA_CANAL_VDA, CANAL.RCANAL_VDA_DNTAL_INDVD,																							\n");
		//		sql.append(" (SELECT MIN(movt.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL movt WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL) AS DT_CRIACAO_PROPOSTA, 			\n");
		//		sql.append(" (select MAX(DINIC_MOVTO_PPSTA) csit_dntal_indvd from dbprod.MOVTO_PPSTA_DNTAL																								\n");
		//		sql.append(" where  nseq_ppsta_dntal = MOVTO_PPSTA_DNTAL.nseq_ppsta_dntal ORDER BY MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA DESC fetch first 1 rows only ) as situacao 						\n");
		//		sql.append(" FROM DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD, DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD, DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL, DBPROD.PROPN_DNTAL_INDVD propn, DBPROD.CANAL_VDA_DNTAL_INDVD CANAL	\n");
		//		sql.append(" WHERE  propn.ncpf_propn_dntal = :cpfProponente 																															\n");
		//		if(canalVenda == 1){                                                                                                                                                                    
		//			                                                                                                                                                                                    
		//			sql.append(" AND  (PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd = :canalVenda  OR PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd is null) ");
		//			
		//		}else{
		//		
		//			sql.append(" AND  (PPSTA_DNTAL_INDVD.ccanal_vda_dntal_indvd = :canalVenda ) ");
		//			
		//		}
		//		sql.append("and propn.CPROPN_DNTAL_INDVD = PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD ");
		//		sql.append("AND PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL ");
		//		sql.append(" AND PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL ");
		//		sql.append("and  (SELECT MIN(movt.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL movt WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL) >= :periodoInicial ");
		//		sql.append("and  (SELECT MIN(movt.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL movt WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL) <= :periodoFinal ");
		//		sql.append( "  and BNEFC_DNTAL_INDVD.CBNEFC_DNTAL_INDVD = 1");

		MapSqlParameterSource params = new MapSqlParameterSource();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		String dataInicialFormatada = formatter.print(periodoInicial);
		periodoFinal = periodoFinal.plusHours(23);
		periodoFinal = periodoFinal.plusMinutes(59);
		periodoFinal = periodoFinal.plusSeconds(59);
		String dataFinalFormatada = formatter.print(periodoFinal);

		params.addValue("CPF_PROPONENTE", cpfProponente);
		params.addValue("CANAL", canalVenda);
		params.addValue("PERIODO_INICIAL", dataInicialFormatada);
		params.addValue("PERIODO_FINAL", dataFinalFormatada);

		LOGGER.error("INFO: LISTAR PROPOSTA POR PROPONENTE: " + sql.toString() + " PAR�METROS: " + params.getValues());
		try {
			listaDePropostas = getJdbcTemplate().query(sql.toString(), params,
					new PropostaBuscadaPorCpfTitularECanalVendaRowMapper());
		} catch (Exception e) {
			LOGGER.error("INFO: Propostas n�o encontradas", e);
		}

		return listaDePropostas;
	}

	/**
	 * Metodo responsavel por listar as propostas do shopping de seguros que foram atualizadas.
	 * 
	 * @return List<StatusPropostaVO> - lista das propostas com os respectivos status.
	 */
	public List<StatusPropostaVO> listarPropostasAtualizadasDoShoppingDeSeguros() {

		StringBuilder sql = new StringBuilder().append("SELECT                                                     \n")
				.append("    MOVTO.CSIT_DNTAL_INDVD,                                \n").append(
						"    PPSTA.CPPSTA_DNTAL_INDVD                               \n").append(
						"FROM                                                       \n").append(
						"    DBPROD.MOVTO_PPSTA_DNTAL MOVTO                         \n").append(
						"INNER JOIN                                                 \n").append(
						"    DBPROD.PPSTA_DNTAL_INDVD PPSTA                         \n").append(
						"ON                                                         \n").append(
						"    PPSTA.NSEQ_PPSTA_DNTAL = MOVTO.NSEQ_PPSTA_DNTAL        \n").append(
						"WHERE                                                      \n").append(
						"    DATE(MOVTO.DULT_ATULZ_REG) = CURRENT_DATE              \n").append(
						"AND PPSTA.CCANAL_VDA_DNTAL_INDVD = 7                       \n").append(
						"ORDER BY                                                   \n").append(
						"    MOVTO.DULT_ATULZ_REG DESC                              \n");

		MapSqlParameterSource params = new MapSqlParameterSource();

		return getJdbcTemplate().query(sql.toString(), params, new StatusPropostaRowMapper());
	}

	public List<PropostaVO> listarPropostasPorBeneficiario(String cpfBeneficiario, Integer canal) {

		//List<PropostaVO> listaDePropostas = null;

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" DISTINCT PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL, PPSTA_DNTAL_INDVD.CPPSTA_DNTAL_INDVD, PROTR_PPSTA_DNTAL.NCPF_CNPJ_PROTR,");
		sql.append(" BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL, BNEFC_DNTAL_INDVD.IBNEFC_DNTAL_INDVD, PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD, CANAL.RCANAL_VDA_DNTAL_INDVD,");
		sql.append(" MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD, MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA,");
		sql.append(" RETOR_MOVTO_PPSTA.RRETOR_MOVTO_PPSTA,");
		sql.append(" PROPN_DNTAL_INDVD.NCPF_PROPN_DNTAL");
		sql.append(" FROM DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD");
		sql.append(" INNER JOIN DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.CANAL_VDA_DNTAL_INDVD CANAL ON (PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD = CANAL.CCANAL_VDA_DNTAL_INDVD)");
		sql.append(" LEFT OUTER JOIN DBPROD.PROTR_PPSTA_DNTAL PROTR_PPSTA_DNTAL ON (PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.PROPN_DNTAL_INDVD PROPN_DNTAL_INDVD ON (PROPN_DNTAL_INDVD.CPROPN_DNTAL_INDVD = PPSTA_DNTAL_INDVD.CPROPN_DNTAL_INDVD)");
		sql.append(" LEFT OUTER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" LEFT OUTER JOIN DBPROD.RETOR_MOVTO_PPSTA RETOR_MOVTO_PPSTA ON (RETOR_MOVTO_PPSTA.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL AND RETOR_MOVTO_PPSTA.CSIT_DNTAL_INDVD = MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA = RETOR_MOVTO_PPSTA.DINIC_MOVTO_PPSTA)");
		sql.append(" WHERE BNEFC_DNTAL_INDVD.NCPF_BNEFC_DNTAL = :cpfBeneficiario");

		//sql.append(" AND BNEFC_DNTAL_INDVD.NCPF_BNEFC_DNTAL = :cpfBeneficiario ");

		sql.append(" AND MOVTO_PPSTA_DNTAL.DINIC_MOVTO_PPSTA = (SELECT MAX(MOVTO_PPSTA_DNTAL_MAX.DINIC_MOVTO_PPSTA) FROM DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL_MAX WHERE MOVTO_PPSTA_DNTAL_MAX.NSEQ_PPSTA_DNTAL = PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)");
		sql.append(" ORDER BY PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("cpfBeneficiario", cpfBeneficiario);
		//params.addValue("cpfBeneficiario", canal);

		LOGGER.error("INFO: LISTAR POR FILTRO: " + sql.toString() + " PAR�METROS: " + params.getValues());
		return getJdbcTemplate().query(sql.toString(), params, new PropostaRowMapper());
	}

	public void incluirProtocoloDeCancelamentoNaProposta(Long sequencialProposta, String protocolo) {
		StringBuilder sql = new StringBuilder().append(" UPDATE DBPROD.PPSTA_DNTAL_INDVD ").append(
				" SET CPROT_CANCT_PPSTA = :PROTOCOLO").append(" WHERE NSEQ_PPSTA_DNTAL = :SEQUENCIAL_PROPOSTA");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("SEQUENCIAL_PROPOSTA", sequencialProposta);
		params.addValue("PROTOCOLO", protocolo);

		LOGGER.error("INFO: INCLUIR PROTOCOLO DE CANCELAMENTO NA PROPOSTA: " + sql.toString() + " PAR�METROS: "
				+ params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public String obterCodigoPropostaPorSequencial(Long sequencialProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT p.CPPSTA_DNTAL_INDVD FROM DBPROD.PPSTA_DNTAL_INDVD p WHERE p.NSEQ_PPSTA_DNTAL = :sequencialProposta ");
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("sequencialProposta", sequencialProposta);

		try {
			return getJdbcTemplate().queryForObject(sql.toString(), param, String.class);

		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(String.format("INFO: Nenhuma proposta encontrada para sequencial informado.[%s] [%s] ",
					sequencialProposta, e));
			return null;
		}

	}

	public Long obterSequencialPorCodigoProposta(String codigoProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT NSEQ_PPSTA_DNTAL FROM DBPROD.PPSTA_DNTAL_INDVD WHERE CPPSTA_DNTAL_INDVD = :codigoProposta ");
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoProposta", codigoProposta);

		try {
			return getJdbcTemplate().queryForLong(sql.toString(), params);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma proposta encontrada para codigo de proposta informado", e);
			return null;
		}

	}

	public String descricaoRetornoMovimentoProposta(Long sequencialProposta, Integer movimento) {

		String descricaoMovimento = null;
		try {
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("sequencial", sequencialProposta);
			params.addValue("movimento", movimento);

			StringBuilder sql = new StringBuilder().append("  select RRETOR_MOVTO_PPSTA               ").append(
					" 	 from DBPROD.RETOR_MOVTO_PPSTA        ").append(" 	 where NSEQ_PPSTA_DNTAL = :sequencial ")
					.append(" 	 and CSIT_DNTAL_INDVD = :movimento    ").append(
							" 	 order by DINIC_MOVTO_PPSTA desc      ").append(
							" 	 fetch first 1 rows only              ");

			descricaoMovimento = getJdbcTemplate().queryForObject(sql.toString(), params, String.class);

		} catch (Exception e) {
			LOGGER.error(String
					.format("INFO: Nenhuma retorno de movimento encontrado[%s] [%s] ", sequencialProposta, e));
		}

		return descricaoMovimento;
	}

	public String descricaoRetornoMovimento(Long sequencialProposta) {
		String descricaoMovimento = null;
		try {
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("sequencial", sequencialProposta);

			StringBuilder sql = new StringBuilder().append("  select RRETOR_MOVTO_PPSTA               ").append(
					" 	 from DBPROD.RETOR_MOVTO_PPSTA        ").append(" 	 where NSEQ_PPSTA_DNTAL = :sequencial ")
					.append(" 	 order by DINIC_MOVTO_PPSTA desc      ").append(
							" 	 fetch first 1 rows only              ");

			descricaoMovimento = getJdbcTemplate().queryForObject(sql.toString(), params, String.class);

		} catch (Exception e) {
			LOGGER.error(String
					.format("INFO: Nenhuma retorno de movimento encontrado[%s] [%s] ", sequencialProposta, e));
		}

		return descricaoMovimento;

	}

	public List<DadosProdutorVO> verificaProdutoresDeCadaProposta(String codigoProposta) {

		List<DadosProdutorVO> dadosProdutor = null;
		StringBuilder sql = new StringBuilder()

				.append(" Select IPROTR_PPSTA_DNTAL As Nome_Produtor,                                                          ")
				.append(" 	CCPD_PROTR As CPD_Produtor, NCPF_CNPJ_PROTR As CpfCnpjProdutor, CTPO_PROTR_PPSTA As Tipo_Produtor  ")
				.append(" 	From DBPROD.ppsta_dntal_indvd proposta join DBPROD.protr_ppsta_dntal corretor                      ")
				.append(" 	on proposta.NSEQ_PPSTA_DNTAL = corretor.NSEQ_PPSTA_DNTAL                                           ")
				.append(" 	where proposta.cppsta_dntal_indvd = :codigoProposta                                                ");

		try {
			if (codigoProposta != null) {
				MapSqlParameterSource params = new MapSqlParameterSource();
				params.addValue("codigoProposta", codigoProposta);
				dadosProdutor = getJdbcTemplate().query(sql.toString(), params, new DadosProdutorRowMapper());
			}
		} catch (Exception e) {
			LOGGER.error("N�o foi poss�vel buscar os corretores da Proposta.", e);
			throw new IntegrationException("N�o foi poss�vel buscar os corretores da Proposta.");
		}
		return dadosProdutor;
	}

	public List<String> consultarProposta(String consulta) {

		List<String> retorno = Lists.newArrayList();
		Integer linhasAfetadas = 0;
		try {

			if (consulta.toUpperCase().contains("UPDATE") || consulta.toUpperCase().contains("INSERT")) {
				linhasAfetadas = getJdbcTemplate().getJdbcOperations().update(consulta);
				LOGGER.error(String.format("Linhas afetadas pela query [%s] -> [%s] linhas", consulta, linhasAfetadas));
				retorno.add(linhasAfetadas.toString());
			} else {
				SqlRowSet obj = getJdbcTemplate().getJdbcOperations().queryForRowSet(consulta);
				String[] columnNames = obj.getMetaData().getColumnNames();

				retorno = Lists.newArrayList(); // Eduardo fez isso para obter duas ou mais listas de objeto dentro de uma lista de objetos. 
				List<String> row = Lists.newArrayList(); // Aqui � apenas uma lista de objetos // Esta representa as linhas da tabela . 
				for (String coluna : columnNames) {//***Aqui eu buscaria todos os canais
					row.add(coluna); // lista de objetos adicionando uma string que � a string do nome das colunas da tabela.//***Adicionaria na lista simples  
				}
				retorno.add(row.toString()); // Lista de lista de objetos adicionando uma ista de objetos. //*** Adicionaria na lista da lista
				while (obj.next()) { // percorrendo as linhas da tabela.
					row = Lists.newArrayList(); // aqui a lista de objetos � instanciada de novo, ou seja zerada, mas a lista de lista de objetos j� adicionou . 
					for (String coluna : columnNames) { // aqui ele percorre os dados da tabela
						if (obj.getObject(coluna) == null) { // verificando se aquele campo ou coluna � nulo 
							row.add("");//*** Ia adicionando VO por VO na lista simples
						} else {
							row.add(obj.getObject(coluna).toString());//*** Ia adicionando VO por VO na lista simples
						}
					}
					retorno.add(row.toString());//
				}
			}
		} catch (Exception e) {
			LOGGER.error(String.format("Erro ao executar query [%s] -> [%s]", consulta, e.getMessage()));
		}
		return retorno;
	}

	/**
	 * M�todo respons�vel por listar as propostas do site 100% corretor com data de validade expirada.
	 * 
	 * @param data - cancelar propostas a partir desta data.
	 * @return List<PropostaExpiradaVO> - lista com as propostas expiradas a serem canceladas.
	 */
	public List<PropostaExpiradaVO> listarPropostasDoSite100CorretorComDataDeValidadeExpirada() {

		MapSqlParameterSource parametros = new MapSqlParameterSource();

		StringBuilder sql = new StringBuilder(500).append("SELECT																\n").append(
				"	PROPOSTA.NSEQ_PPSTA_DNTAL,										\n").append("	PROPOSTA.DEMIS_PPSTA_DNTAL_INDVD								\n")
				.append("FROM																\n").append("	DBPROD.PPSTA_DNTAL_INDVD AS PROPOSTA							\n").append(
						"WHERE																\n").append(
						"	PROPOSTA.DEMIS_PPSTA_DNTAL_INDVD IS NOT NULL            		\n")
				.append("AND																\n").append("	(PROPOSTA.CCANAL_VDA_DNTAL_INDVD = 1							\n").append(
						"		OR                                                          \n").append(
						"	 PROPOSTA.CCANAL_VDA_DNTAL_INDVD IS NULL)                       \n").append(
						"AND																\n").append(
						"	CURRENT_DATE > (proposta.DEMIS_PPSTA_DNTAL_INDVD + 90 DAYS)		\n").append(
						"AND																\n").append("	(SELECT															\n").append(
						"		MOV.CSIT_DNTAL_INDVD										\n").append("	 FROM															\n").append(
						"		DBPROD.MOVTO_PPSTA_DNTAL AS MOV								\n").append("	 WHERE															\n").append(
						"		MOV.NSEQ_PPSTA_DNTAL = PROPOSTA.NSEQ_PPSTA_DNTAL			\n").append("	 ORDER BY														\n")
				.append("		MOV.DINIC_MOVTO_PPSTA DESC									\n").append("	 FETCH															\n").append(
						"	 	FIRST 1 ROWS ONLY) IN (1,2)									\n").append("	 FETCH															\n").append(
						"	 	FIRST 5000 ROWS ONLY                                        \n");

		return getJdbcTemplate().query(sql.toString(), parametros, new PropostaExpiradaRowMapper());
	}

	
}
