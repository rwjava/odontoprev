package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;

/**
 * Adaptador para converter uma String representando uma data no formato yyyyMMdd para um LocalDate
 * 
 * @author WDEV
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LocalDateAdapter.class);
	
	@Override
	public LocalDate unmarshal(String data) {
		try {
			return new LocalDate(new SimpleDateFormat("dd/MM/yyyy").parse(data));
		} catch (ParseException e) {
			LOGGER.error("INFO: Erro ao converter string para LocalDate. Formato inv�lido", e);
			return null;
		}
		//return DateTimeFormat.forPattern(Constantes.FORMATO_LOCAL_DATE_WEB_SERVICE).parseDateTime(data).toLocalDate();
	}

	@Override
	public String marshal(LocalDate data) {
		return data.toString(Constantes.FORMATO_LOCAL_DATE_WEB_SERVICE);
	}

}