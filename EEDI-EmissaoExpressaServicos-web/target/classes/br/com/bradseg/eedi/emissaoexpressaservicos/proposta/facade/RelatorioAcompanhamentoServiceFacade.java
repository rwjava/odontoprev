package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import br.com.bradseg.eedi.emissaoexpressaservicos.support.ReportFile;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Arquivo;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.RelatorioAcompanhamentoVO;

/**
 * Servi�os para listar propostas e gerar relat�rios de acompanhamento
 * @author WDEV
 */
public interface RelatorioAcompanhamentoServiceFacade {

	/**
	 * Lista as propostas a partir de filtro de pesquisa para gerar o relat�rio de acompanhamento
	 * @param filtro Filtro de pesquisa de relat�rio de acompanhamento
	 * @param login do usu�rio utilizando o sistema
	 * @return Relatorio de Acompanhamento com a Lista de propostas
	 */
	public RelatorioAcompanhamentoVO listarPropostasPorFiltro(FiltroRelatorioAcompanhamentoVO filtro, LoginVO login);

	/**
	 * Lista as propostas e gera o relat�rio em PDF de acompanhamento.
	 * @param filtro Filtro de pesquisa de relat�rio de acompanhamento
	 * @param login do usu�rio utilizando o sistema
	 * @return Relatorio de Acompanhamento com a Lista de propostas
	 */
	public ReportFile gerarPDF(FiltroRelatorioAcompanhamentoVO filtro, LoginVO login);

	/**
	 * Lista as propostas e gera um arquivo em CSV
	 * @param filtro Filtro de pesquisa de relat�rio de acompanhamento
	 * @param login do usu�rio utilizando o sistema
	 * @return Relatorio de Acompanhamento com a Lista de propostas
	 */
	public Arquivo gerarCSV(FiltroRelatorioAcompanhamentoVO filtro, LoginVO login);

	public ReportFile gerarPDFRelatorioAcompanhamento(FiltroAcompanhamentoVO filtro, LoginVO login);
	

}