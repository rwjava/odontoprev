package br.com.bradseg.eedi.emissaoexpressaservicos.endereco.facade;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;

/**
 * Servi�o respons�vel por consultar o endere�o
 * 
 * @author WDEV
 */
public interface EnderecoServiceFacade {

 
	/**
	 * Consulta o endere�o a partir do cep
	 * 
	 * @param cep Cep
	 * @return EnderecoVO do cep informado
	 */
	public EnderecoVO obterEnderecoPorCep(String enderecoCep);

}
