package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipos de formas de pagamento
 * 
 * @author WDEV
 */
public enum FormaPagamento {

	//@formatter:off
	BOLETO_BANCARIO(1, "Boleto"), 
	BOLETO_DEMAIS_DEBITO(2, "1� parcela em boleto e demais em d�bito autom�tico"), 
	DEBITO_AUTOMATICO(3, "D�bito em conta corrente"),
	DEBITO_DEMAIS_BOLETO(4, "1� parcela em d�bito autom�tico e demais em boleto"),
	CARTAO_CREDITO(5, "Cart�o de Cr�dito"),
	BOLETO_E_CARTAO_CREDITO(6, "1� parcela em boleto e demais em cart�o de cr�dito"),
	DEBITO_AUTOMATICO_E_CARTAO_CREDITO(7, "1� parcela em d�bito autom�tico e demais em cart�o de cr�dito");	
	
	//@formatter:on

	private Integer codigo;
	private String descricao;

	private FormaPagamento(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * Busca por codigo da forma de pagamento.
	 * 
	 * @param codigo - codigo da Forma de Pagamento
	 * @return a forma de pagamento
	 */
	public static FormaPagamento obterPorCodigo(Integer codigo) {
		for (FormaPagamento forma : FormaPagamento.values()) {
			if (forma.getCodigo().equals(codigo)) {
				return forma;
			}
		}
		return null;
	}

}