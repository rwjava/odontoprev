package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.rest;


import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.PropostaWebService;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaCorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;


/**
 * WebService REST responsável por cadastrar, finalizar, cancelar, pesquisar e emitir propostas
 *  
 * @author WDEV
 */
@Service("propostaDental")
public class PropostaDentalIndividualWebServiceRest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaDentalIndividualWebServiceRest.class);

	@Autowired
	private PropostaServiceFacade propostaServiceFacade;
	
	@RequestMapping(value = "/salvarRascunho{proposta}", method = RequestMethod.POST)
	@ResponseBody
	public  Long salvarRascunho (@RequestParam("proposta") PropostaVO proposta) throws IntegrationException, BusinessException {
		return propostaServiceFacade.salvarRascunho(proposta).getNumeroSequencial();
	}
	
	@RequestMapping(value = "/finalizar{proposta}", method = RequestMethod.POST)
	@ResponseBody
	public Long finalizar(@RequestParam("proposta") PropostaVO proposta) throws IntegrationException, BusinessException {
		LOGGER.error("INFO: FINALIZAR PROPOSTA: "+proposta);
		Long numeroSequencial = propostaServiceFacade.salvarPendente(proposta).getNumeroSequencial();
		LOGGER.error("INFO: SEQUENCIAL RETORNADO: "+ numeroSequencial);
		return numeroSequencial;
	}
	
	@RequestMapping(value = "/finalizarProposta100Corretor{proposta}", method = RequestMethod.POST)
	@ResponseBody
	public Long finalizarProposta(@RequestParam("proposta") PropostaCorretorVO proposta) throws IntegrationException, BusinessException {
		LOGGER.error("INFO: FINALIZAR PROPOSTA 100 CORRETOR: "+proposta);
		Long numeroSequencial = propostaServiceFacade.salvarProposta100CorretorPendente(proposta).getNumeroSequencial();
		LOGGER.error("INFO: SEQUENCIAL RETORNADO: "+ numeroSequencial);
		return numeroSequencial;
	}
	
	@RequestMapping(value = "/salvarPropostaVazia{proposta}", method = RequestMethod.POST)
	@ResponseBody
	public Long salvarPropostaVazia(@RequestParam("proposta") PropostaVO proposta) throws IntegrationException, BusinessException {
		LOGGER.error("INFO: FINALIZAR PROPOSTA VAZIA: "+proposta);
		Long numeroSequencial = propostaServiceFacade.salvarPropostaVazia(proposta).getNumeroSequencial();
		LOGGER.error("INFO: SEQUENCIAL RETORNADO: "+ numeroSequencial);
		return numeroSequencial;
	}
	
	@RequestMapping(value = "/cancelar{sequencialProposta}", method = RequestMethod.POST)
	@ResponseBody
	public PropostaVO cancelar(@RequestParam("sequencialProposta") Long sequencialProposta) throws IntegrationException, BusinessException {
		LOGGER.error("INFO: CANCELAR PROPOSTA: "+sequencialProposta);
		return propostaServiceFacade.cancelar(sequencialProposta); 
	}
	
	@RequestMapping(value = "/cancelarPropostaPorProtocolo{protocolo}/{canalVenda}", method = RequestMethod.POST)
	public void cancelarPropostaPorProtocolo(@PathVariable("protocolo") String protocolo, @PathVariable("canalVenda") Integer canalVenda) throws IntegrationException, BusinessException {
		propostaServiceFacade.cancelarPropostaPorProtocolo(protocolo, canalVenda);
	}
	
	@RequestMapping(value = "/listarPropostasPorProponente{cpfProponente}/{canalVenda}/{periodoInicial}/{periodoFinal}", method = RequestMethod.POST)
	public  List<PropostaVO> listarPropostasPorProponente(
	@RequestParam("cpfProponente")String cpfProponente,
	@RequestParam("canalVenda") Integer canalVenda,
	@RequestParam("periodoInicial")
	@XmlJavaTypeAdapter(type = org.joda.time.DateTime.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DateTimeAdapter.class)
	DateTime periodoInicial, 
	@RequestParam("periodoFinal") 
	@XmlJavaTypeAdapter(type = org.joda.time.DateTime.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DateTimeAdapter.class)
	DateTime periodoFinal ) throws IntegrationException, BusinessException {
		return propostaServiceFacade.listarPropostasPorProponente(cpfProponente, canalVenda, periodoInicial, periodoFinal);
	}
	
	@RequestMapping(value = "/listarPorFiltro", method = RequestMethod.GET, headers="Accept=application/json")
	@ResponseBody
	public List<PropostaVO> listarPorFiltro(
			@RequestParam("filtroProposta") FiltroPropostaVO filtroProposta,
			@RequestParam("login") LoginVO login) throws IntegrationException, BusinessException {
		return propostaServiceFacade.listarPorFiltro(filtroProposta, login);
	}
	
	@RequestMapping(value = "/listarPropostasIntranetPorFiltro", method = RequestMethod.GET, headers="Accept=application/json")
	@ResponseBody
	public List<PropostaVO> listarPropostasIntranetPorFiltro(
			@RequestParam("filtroProposta") FiltroPropostaVO filtroProposta,  
			@RequestParam("login") LoginVO login) throws IntegrationException, BusinessException {
		return propostaServiceFacade.listarPropostasIntranetFiltro(filtroProposta, login);
	}
	

	
}
