package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

public class CartaoCreditoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7245148319502544086L;
	private String nomeCartao;
	private String cpfCartao;
	private String numeroCartao;
	private String validade;
	private String bandeira;
	private String paymentToken;
	private String accessToken;
	private String mensagemRetorno;
	public String getNomeCartao() {
		return nomeCartao;
	}
	public void setNomeCartao(String nomeCartao) {
		this.nomeCartao = nomeCartao;
	}
	public String getCpfCartao() {
		return cpfCartao;
	}
	public void setCpfCartao(String cpfCartao) {
		this.cpfCartao = cpfCartao;
	}
	public String getNumeroCartao() {
		return numeroCartao;
	}
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	public String getValidade() {
		return validade;
	}
	public void setValidade(String validade) {
		this.validade = validade;
	}
	public String getBandeira() {
		return bandeira;
	}
	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}
	public String getPaymentToken() {
		return paymentToken;
	}
	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getMensagemRetorno() {
		return mensagemRetorno;
	}
	public void setMensagemRetorno(String mensagemRetorno) {
		this.mensagemRetorno = mensagemRetorno;
	}

	
	
}

