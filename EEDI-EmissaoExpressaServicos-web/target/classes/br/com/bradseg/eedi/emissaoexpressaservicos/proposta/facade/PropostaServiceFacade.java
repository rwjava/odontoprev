package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import org.joda.time.DateTime;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaCorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;

/**
 * Servi�os respons�veis para consulta e cadastro deproposta
 * 
 * @author WDEV
 */
public interface PropostaServiceFacade {

	/**
	 * Lista as propostas a partir de filtro de pesquisa informado
	 * @param filtroProposta Filtro para consulta de proposta
	 * @param login Informa��es do usu�rio logado
	 * 
	 * @return Lista de propostas encontradas
	 */
	public List<PropostaVO> listarPorFiltro(FiltroPropostaVO filtroProposta, LoginVO login);
	
	/**
	 * Lista as propostas a partir de filtro de pesquisa informado
	 * @param filtroProposta Filtro para consulta de proposta
	 * @param login Informa��es do usu�rio logado
	 * 
	 * @return Lista de propostas encontradas
	 */
	public List<PropostaVO> listarPropostasIntranetFiltro(FiltroPropostaVO filtroProposta, LoginVO login);
	
	/**
	 * Lista as propostas 
	 * @param cpfProponente
	 * @param canalVenda
	 * @param periodoInicial
	 * @param periodoFinal
	 * @return
	 */
	public List<PropostaVO> listarPropostasPorProponente(String cpfProponente, Integer canalVenda,  DateTime periodoInicial, DateTime periodoFinal ) ;

	/**
	 * Consulta a proposta a partir do seu c�digo
	 * 
	 * @param codigoProposta C�digo da proposta
	 * @return Proposta encontrada
	 */
	public PropostaVO consultarPorCodigo(String codigoProposta);

	/**
	 * Cria uma nova proposta com situa��o Rascunho
	 * @param proposta Proposta rascunho
	 * @return Proposta criada no sistema
	 */
	public PropostaVO salvarRascunho(PropostaVO proposta);

	/**
	 * Cria uma nova proposta com situa��o Pendente
	 * @param proposta PropostaVO pendente
	 * @return PropostaVO criada no sistema
	 */
	public PropostaVO salvarPendente(PropostaVO proposta);
	
	public PropostaVO salvarProposta100CorretorPendente(PropostaCorretorVO proposta);

	/**
	 * Consulta a quantidade de propostas com situa��o Rascunho ou Pendente existentes na data atual para o CPF do benefici�rio informado 
	 *  
	 * @param cpf CPF do benefici�rio
	 * @return Quantidade de propostas
	 */
	public Integer consultarQuantidadePropostasPorDataAtualParaBeneficiario(String cpf);

	/**
	 * Consulta a �ltima situa��o da proposta
	 * 
	 * @param proposta Proposta
	 * @return �ltima situa��o da proposta
	 */
	public SituacaoProposta consultarUltimaSituacaoProposta(PropostaVO proposta);

	public SituacaoProposta consultarUltimaSituacaoProposta(Long numeroSequencial);
	/**
	 * Cancela a proposta
	 * 
	 * @param numeroSequencial N�mero sequencial da proposta
	 * @return Proposta cancelada
	 */
	public PropostaVO cancelar(Long numeroSequencial);
	
	/**
	 * Cancela a proposta Canal IB
	 * 
	 * @param protocolo n�mero de protocolo da proposta
	 * @param canalVenda c�digo do canal de venda da proposta
	 * @return Proposta cancelada
	 */
	public PropostaVO cancelarPropostaPorProtocolo(String protocolo, Integer canalVenda);
	
	


	/**
	 * Cria uma nova proposta vazia com situa��o rascunho
	 * @param proposta Proposta vazia rascunho
	 * @return Proposta com o c�digo gerado
	 */

	public PropostaVO salvarPropostaVazia(PropostaVO proposta);

	/**
	 * Adiciona um novo dependente na proposta
	 * @param proposta Proposta
	 */
	public void adicionarDependente(PropostaVO proposta);

	/**
	 * Adiciona um novo telefone na proposta
	 * @param proposta Proposta
	 */
	public void adicionarTelefone(PropostaVO proposta);
	
	/**
	 * Metodo resposanvel por atualizar o status das propostas na base do shopping de seguros.
	 */
	public void atualizarStatusDaPropostaNoShoppingDeSeguros();
	
	public PropostaVO consultarPorSequencial(Long sequencialProposta);

	public List<PropostaVO> listarPropostasPorBeneficiario(String cpfBeneficiario, Integer canal);
	
	public void cancelarPropostaShoppingSeguros(Long sequencialProposta, String protocolo);

	
	public String obterCodigoPropostaPorSequencial(Long sequencialProposta);
			
	public Long obterSequencialPorCodigoProposta(String codigoProposta);
	/**
	 * <ul>Metodo responsavel por obter a situacao de proposta.</ul>
	 * <li>Caso possua status de cancelamento, o status e de cancelamento.</li>
	 * <li>Caso nao possua status de cancelamento e possua status de implantada, o status e de implantada.</li>
	 * <li>Caso nao possua status de cancelamento nem de implantacao, o status e o mais recente</li>
	 * 
	 * @param codigoProposta - codigo de proposta a ser buscado.
	 * @return Integer - situacao da proposta.
	 */
	public Integer obterSituacaoProposta(String codigoProposta, Integer codigoCanal);

	public List<String> consultarProposta(String consulta);
	
	public String cancelarPropostasDoSite100CorretorComDataDeValidadeExpirada();
	
	public void salvarMovimento(Long sequencialProposta,  SituacaoProposta situacao, String responsavelAtualizacao);

	public List<MovimentoPropostaVO> listarMovimentoPropostaPorSequencial(Long sequencialProposta);
	
	public boolean propostaPossuiStatusCancelada(Long numeroSequencial);
	
	public boolean possuiStatusImplantada(Long numeroSequencial);
	
	
	public boolean possuiStatusImplantadaProponente(Long numeroSequencial);
	
	public Integer consultarParentescoAtivo(Long numeroSequencial);
	
	public Integer consultaMovimentacao(Long numeroSeuqencial);
	
	public Integer consultarTipoParentesco(Long numeroSequencial);
	
	public Integer consultarTipoParentescoDepen(Long numeroSequencial);



}
