package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Beneficiário titular de uma proposta
 * @author WDEV
 */
public class TitularVO extends BeneficiarioVO {

	private static final long serialVersionUID = 6962144693101360161L;

	private EnderecoVO endereco;

	public EnderecoVO getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoVO endereco) {
		this.endereco = endereco;
	}

	@Override
	public TipoBeneficiario getTipoBeneficiario() {
		return TipoBeneficiario.TITULAR;
	}

	@Override
	public String toString() {
		return "TitularVO [endereco=" + endereco + "]";
	}

	
}