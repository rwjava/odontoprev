package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DadosProdutorVO;


/**
 * Classe para implementar um rowMapper para a classe DadosProdutor
 * 
 * @author Bradesco Seguros
 */

public class DadosProdutorRowMapper implements RowMapper<DadosProdutorVO> {

	/**
	 * Mapeia row a um objeto
	 * 
	 * @param rs ResultSet
	 * @param index indice da linha
	 * @return objeto mapeado
	 */

	public DadosProdutorVO mapRow(ResultSet rs, int arg1) throws SQLException {

		DadosProdutorVO dadosProdutor = new DadosProdutorVO();

		dadosProdutor.setNomeProdutor(rs.getString("Nome_Produtor"));
		dadosProdutor.setCpdProdutor(rs.getString("CPD_Produtor"));
		dadosProdutor.setCpfCnpjProdutor(rs.getString("CpfCnpjProdutor"));
		dadosProdutor.setTipoProdutor(rs.getString("Tipo_Produtor"));

		return dadosProdutor;
	}
}