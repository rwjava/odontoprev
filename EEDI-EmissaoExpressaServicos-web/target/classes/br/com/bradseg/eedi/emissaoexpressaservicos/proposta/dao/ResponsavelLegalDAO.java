package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;

/**
 * Descri��o do DAO da proposta
 * 
 * @author WDEV
 */
public interface ResponsavelLegalDAO {

	/**
	 * Lista o responsavel legal do beneficiario 
	 * 
	 * @param proposta PropostaVO
	 * @return ResponsavelLegalVO do beneficiario
	 */
	public ResponsavelLegalVO listarPorProposta(PropostaVO proposta);

	/**
	 * Salva as informa��es do respons�vel legal
	 * 
	 * @param responsavelLegal Respons�vel legal do titular
	 * @return Respons�vel legal do titular
	 */
	public ResponsavelLegalVO salvar(ResponsavelLegalVO responsavelLegal);

	/**
	 * Remove o respons�vel legal do titular da proposta
	 * 
	 * @param responsavelLegal Respons�vel legal do titular
	 */
	public void remover(ResponsavelLegalVO responsavelLegal);

	/**
	 * Associa o endere�o ao respons�vel legal do titular
	 * 
	 * @param responsavelLegal Respons�vel legal do titular
	 * @param endereco Endere�o do respons�vel legal
	 */
	public void associarEndereco(ResponsavelLegalVO responsavelLegal, EnderecoVO endereco);

	/**
	 * Obt�m o c�digo sequencial do respons�vel legal
	 * 
	 * @return C�digo sequencial gerado
	 */
	public Long obterCodigoSequencial();

	/**
	 * Desassocia o endere�o ao respons�vel legal do titular
	 * 
	 * @param responsavelLegal Respons�vel legal do titular
	 * @param endereco Endere�o do respons�vel legal
	 */
	public void desassociarEndereco(ResponsavelLegalVO responsavelLegal, EnderecoVO endereco);

}