package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Dados do proponente
 * 
 * @author WDEV
 */
public class ProponenteVO extends PessoaVO implements Serializable {

	private static final long serialVersionUID = 5197650293954642120L;

	private Long codigo;

	private ContaCorrenteVO contaCorrente;

	private List<TelefoneVO> telefones;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public List<TelefoneVO> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<TelefoneVO> telefones) {
		this.telefones = telefones;
	}

	public ContaCorrenteVO getContaCorrente() {
		return contaCorrente;
	}

	public void setContaCorrente(ContaCorrenteVO contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public boolean isEmailPreenchido() {
		return StringUtils.isNotBlank(getEmail());
	}

	public boolean isCodigoPreenchido() {
		return codigo != null;
	}

	public boolean isTelefonePreenchido() {
		return telefones != null && !telefones.isEmpty();
	}

	@Override
	public String toString() {
		return "ProponenteVO [codigo=" + codigo + ", contaCorrente=" + contaCorrente + ", telefones=" + telefones + "]";
	}

	
}