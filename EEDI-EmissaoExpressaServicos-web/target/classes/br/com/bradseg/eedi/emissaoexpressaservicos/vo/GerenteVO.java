package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Produtor gerente da proposta
 * @author WDEV
 */
public class GerenteVO extends ProdutorVO implements Serializable {

	private static final long serialVersionUID = 1682653452860041470L;

	public GerenteVO() {
		super(TipoProdutor.GERENTE);
	}

	public TipoComissao getTipoComissao() {
		return TipoComissao.COMISSAO_GERENTE_BVP;
	}

}