package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.StatusPropostaVO;


	public class StatusPropostaRowMapper implements RowMapper<StatusPropostaVO>{

		public StatusPropostaVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
			
			StatusPropostaVO statusPropostaVO = new StatusPropostaVO();
			
			statusPropostaVO.setCodigoProposta(resultSet.getString("CPPSTA_DNTAL_INDVD"));
			statusPropostaVO.setSituacaoProposta(SituacaoProposta.obterPorCodigo(rs.getInt("CSIT_DNTAL_INDVD")));

			return statusPropostaVO;
			
		}
}
