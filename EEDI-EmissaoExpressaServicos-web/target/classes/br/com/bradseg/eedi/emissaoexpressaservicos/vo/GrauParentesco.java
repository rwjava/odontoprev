package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipos de Grau de parentesco
 * 
 * @author WDEV
 *
 */
public enum GrauParentesco {

	FILHA(3, "Filha"), FILHO(4, "Filho"), CONJUGE(5, "Conjuge"), OUTRO(18, "Outro"), PROPRIO(19, "Proprio"), PAI(1, "Pai"), MAE(2, "M�e");

	private Integer codigo;
	private String descricao;

	 GrauParentesco(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * Busca o grau de parentesco pelo codigo.
	 * 
	 * @param codigo - codigo do grau de parentesco
	 * @return o grau de parentesco
	 */
	public static GrauParentesco buscaPorCodigo(Integer codigo) {
		for (GrauParentesco grauParentesco : GrauParentesco.values()) {
			if (grauParentesco.getCodigo().equals(codigo)) {
				return grauParentesco;
			}
		}
		return null;
	}
}
