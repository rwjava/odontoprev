package br.com.bradseg.eedi.emissaoexpressaservicos.support;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * Arquivo de relatório. Possui um nome, um tipo e pode conter vários relatórios (reports)
 * @author WDEV
 */
public class ReportFile implements Serializable {

	private static final long serialVersionUID = -3421878692896349602L;

	private String fileName;
	private String format = "pdf";
	private List<Report> reports = Lists.newArrayList();

	public List<Report> getReports() {
		return reports;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFormat() {
		return format;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void adicionarRelatorio(Report report) {
		reports.add(report);
	}

}
