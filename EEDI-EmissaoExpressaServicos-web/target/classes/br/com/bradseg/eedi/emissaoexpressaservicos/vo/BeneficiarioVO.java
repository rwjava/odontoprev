package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Beneficiário da proposta
 * 
 * @author WDEV
 */
public abstract class BeneficiarioVO extends PessoaVO implements Serializable {

	private static final long serialVersionUID = 4587413943456627468L;

	private Integer codigo;

	private PropostaVO proposta;

	private EstadoCivil estadoCivil;
	
	private GrauParentesco grauParentescoTitulaProponente;

	private String nomeMae;

	private Sexo sexo;

	private Long numeroDeclaracaoNascidoVivo;

	private Long numeroCartaoNacionalSaude;

	private Long numeroCarteiraOdontoprev;

	private GrauParentesco grauParentesco; 

	public abstract TipoBeneficiario getTipoBeneficiario();

	public boolean isTitular() {
		return TipoBeneficiario.TITULAR == getTipoBeneficiario();
	}

	public boolean isCasado() {
		return isEstadoCivil(EstadoCivil.CASADO);
	}

	public boolean isDivorciado() {
		return isEstadoCivil(EstadoCivil.DIVORCIADO);
	}

	public boolean isSeparado() {
		return isEstadoCivil(EstadoCivil.SEPARADO);
	}

	public boolean isSolteiro() {
		return isEstadoCivil(EstadoCivil.SOLTEIRO);
	}

	public boolean isViuvo() {
		return isEstadoCivil(EstadoCivil.VIUVO);
	}

	private boolean isEstadoCivil(EstadoCivil estadoCivil) {
		return estadoCivil.equals(this.estadoCivil);
	}
	
	public boolean isFilha() {
		return isGrauParentescoTitulaProponente(GrauParentesco.FILHA);
	}
	public boolean isPai() {
		return isGrauParentescoTitulaProponente(GrauParentesco.PAI);
	}
	public boolean isMae() {
		return isGrauParentescoTitulaProponente(GrauParentesco.MAE);
	}
	public boolean isFilho() {
		return isGrauParentescoTitulaProponente(GrauParentesco.FILHO);
	}

	public boolean isConjuge() {
		return isGrauParentescoTitulaProponente(GrauParentesco.CONJUGE);
	}

	public boolean isParentescoOutros() {
		return isGrauParentescoTitulaProponente(GrauParentesco.OUTRO);
	}
	
	public boolean isParentescoProprio() {
		return isGrauParentescoTitulaProponente(GrauParentesco.PROPRIO);
	}
	
	private boolean isGrauParentescoTitulaProponente(GrauParentesco grauParentescoTitulaProponente) {
		return grauParentescoTitulaProponente.equals(this.grauParentescoTitulaProponente);
	}

	public boolean isSexoMasculino() {
		return isSexo(Sexo.M);
	}

	public boolean isSexoFeminino() {
		return isSexo(Sexo.F);
	}

	private boolean isSexo(Sexo sexo) {
		return sexo.equals(this.sexo);
	}

	public boolean isDependente() {
		return !isTitular();
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public PropostaVO getProposta() {
		return proposta;
	}

	public void setProposta(PropostaVO proposta) {
		this.proposta = proposta;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	
	public GrauParentesco getGrauParentescoTitulaProponente() {
		return grauParentescoTitulaProponente;
	}

	public void setGrauParentescoTitulaProponente(GrauParentesco grauParentescoTitulaProponente) {
		this.grauParentescoTitulaProponente = grauParentescoTitulaProponente;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Long getNumeroDeclaracaoNascidoVivo() {
		return numeroDeclaracaoNascidoVivo;
	}

	public void setNumeroDeclaracaoNascidoVivo(Long numeroDeclaracaoNascidoVivo) {
		this.numeroDeclaracaoNascidoVivo = numeroDeclaracaoNascidoVivo;
	}

	public Long getNumeroCartaoNacionalSaude() {
		return numeroCartaoNacionalSaude;
	}

	public void setNumeroCartaoNacionalSaude(Long numeroCartaoNacionalSaude) {
		this.numeroCartaoNacionalSaude = numeroCartaoNacionalSaude;
	}


	public Long getNumeroCarteiraOdontoprev() {
		return numeroCarteiraOdontoprev;
	}

	public void setNumeroCarteiraOdontoprev(Long numeroCarteiraOdontoprev) {
		this.numeroCarteiraOdontoprev = numeroCarteiraOdontoprev;
	}

	public boolean isCnsPreenchido() {
		return numeroCartaoNacionalSaude != null;
	}
	/**
	 * @return the grauParentesco
	 */
	public GrauParentesco getGrauParentesco() {
		return grauParentesco;
	}

	/**
	 * @param grauParentesco the grauParentesco to set
	 */
	public void setGrauParentesco(GrauParentesco grauParentesco) {
		this.grauParentesco = grauParentesco;
	}

	@Override
	public String toString() {
		return "BeneficiarioVO [codigo=" + codigo + ", proposta=" + proposta + ", estadoCivil=" + estadoCivil
				+ ", nomeMae=" + nomeMae + ", grauParentesco=" + grauParentesco +",grauParentescoTitulaProponente="+ grauParentescoTitulaProponente +",sexo=" + sexo + ", numeroDeclaracaoNascidoVivo="
				+ numeroDeclaracaoNascidoVivo + ", numeroCartaoNacionalSaude=" + numeroCartaoNacionalSaude
				+ ", numeroCarteiraOdontoprev=" + numeroCarteiraOdontoprev +  "]";
	}



	
}