package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;

/**
 * Consulta as informa��es dos canais de venda dispon�veis
 * 
 * @author WDEV
 */
public interface CanalVendaDAO {

	/**
	 * Consulta as informa��es do canal de venda a partir do c�digo informado
	 * @param codigo C�digo do canal de venda
	 * @return Canal de venda encontrado
	 */
	public CanalVendaVO consultarPorCodigo(Integer codigo);
	
	public List<CanalVendaVO> listarCanaisVenda();

}