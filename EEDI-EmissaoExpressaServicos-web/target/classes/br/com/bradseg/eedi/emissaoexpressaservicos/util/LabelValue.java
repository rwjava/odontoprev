package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import com.google.common.base.Preconditions;

/**
 * Label/Value para ser utilizado em selects nas p�ginas
 * 
 * @author WDEV
 */
public class LabelValue {

	private String codigo;
	private String descricao;

	public LabelValue(String codigo, String descricao) {
		Preconditions.checkNotNull(codigo, "O c�digo n�o pode ser nulo");
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

}