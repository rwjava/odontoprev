package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.bradseg.bsad.framework.core.exception.IntegrationException;

/**
 * Arquivo que ser� gerado pelo sistema
 * @author WDEV
 */
public class Arquivo {

	private static final Logger LOGGER = LoggerFactory.getLogger(Arquivo.class);

	private String nome;
	private OutputStream outputStream;
	private String encoding;
	private String separador;
	private boolean inicioLinha = true;

	public Arquivo() {
		this("ISO-8859-1");
		this.outputStream = new ByteArrayOutputStream();
	}

	public Arquivo(String encoding) {
		this.encoding = encoding;
	}

	public Arquivo texto(String texto) {
		appendString(texto, 0);
		return this;
	}

	public Arquivo branco(int quantidadeCaracteres) {
		return texto("", quantidadeCaracteres);
	}

	public Arquivo texto(String texto, int quantidadeCaracteres) {
		appendString(texto, quantidadeCaracteres);
		return this;
	}

	public Arquivo texto(Number numeroComoTexto, int quantidadeCaracteres) {
		if (numeroComoTexto == null) {
			appendString("", quantidadeCaracteres);
		} else {
			appendString(numeroComoTexto.toString(), quantidadeCaracteres);
		}
		return this;
	}

	private void appendString(String texto, int quantidadeCaracteres) {
		append(StringUtils.rightPad(normalizarTexto(texto), quantidadeCaracteres, " "));
	}

	public Arquivo numero(Long numero) {
		append(toString(numero, 0));
		return this;
	}

	public Arquivo numero(Long numero, int quantidadeCaracteres) {
		append(toString(numero, quantidadeCaracteres));
		return this;
	}

	public Arquivo numero(String numeroComoTexto, int quantidadeCaracteres) {
		append(toString(numeroComoTexto, quantidadeCaracteres));
		return this;
	}

	public Arquivo numero(Integer numero) {
		append(toString(numero, 0));
		return this;
	}

	public Arquivo zeros(int quantidadeCaracteres) {
		return numero(0, quantidadeCaracteres);
	}

	public Arquivo numero(Integer numero, int quantidadeCaracteres) {
		append(toString(numero, quantidadeCaracteres));
		return this;
	}

	public Arquivo numero(BigDecimal numero) {
		if (numero == null) {
			numero = BigDecimal.ZERO;
		}
		append(numero.setScale(2, RoundingMode.HALF_EVEN).toString());
		return this;
	}

	public Arquivo numero(BigDecimal numero, BigDecimal valorPadrao) {
		if (numero != null) {
			append(numero.setScale(2, RoundingMode.HALF_EVEN).toString());
		}
		return this;
	}

	public Arquivo numero(BigInteger numero, int quantidadeCaracteres) {
		append(toString(numero, quantidadeCaracteres));
		return this;
	}

	public Arquivo data(DateTime dataHora, String padrao) {
		if (dataHora != null) {
			append(dataHora.toString(padrao));
		}
		return this;
	}

	public Arquivo data(LocalDate data, String padrao) {
		if (data != null) {
			append(data.toString(padrao));
		}
		return this;
	}

	public Arquivo quebraLinha() {
		append("\r\n");
		inicioLinha = true;
		return this;
	}

	private String toString(Number valor, int quantidadeCaracteres) {
		if (valor == null) {
			return toString("", quantidadeCaracteres);
		}
		return toString(valor.toString(), quantidadeCaracteres);
	}

	private String toString(String valor, int quantidadeCaracteres) {
		if (StringUtils.isBlank(valor)) {
			valor = "";
		}
		return StringUtils.leftPad(valor, quantidadeCaracteres, "0");
	}

	private void append(String value) {
		try {
			if (possuiSeparador() && !isInicioLinha()) {
				write(separador.getBytes(encoding));
			}

			write(value.getBytes(encoding));
			inicioLinha = false;
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("INFO: Erro ao adicionar texto com enconding informado", e);
			throw new IntegrationException("Erro ao adicionar texto com enconding informado", e);
		}
	}

	/**
	 * Verifica se a String est� nula, caso esteja, retorna um texto em branco
	 * @param texto Texto
	 * @return Texto original ou em branco caso esteja nulo
	 */
	private String normalizarTexto(String texto) {
		if (StringUtils.isBlank(texto)) {
			texto = "";
		}
		return texto.trim();
	}

	private boolean isInicioLinha() {
		return inicioLinha;
	}

	private boolean possuiSeparador() {
		return StringUtils.isNotBlank(separador);
	}

	private void write(byte[] bytes) {
		try {
			outputStream.write(bytes);
		} catch (Exception e) {
			LOGGER.error("INFO: Erro ao escrever dados em arquivo", e);
			throw new IntegrationException("Erro ao escrever dados em arquivo", e);
		}
	}

	public String asString() {
		if (outputStream == null) {
			return null;
		}

		ByteArrayOutputStream baos = (ByteArrayOutputStream) outputStream;
		try {
			return new String(baos.toByteArray(), encoding);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("INFO: Erro ao transformar stream de dados em string", e);
			throw new IntegrationException("Erro ao transformar stream de dados em string", e);
		}
	}

	public byte[] asByteArray() {
		ByteArrayOutputStream baos = (ByteArrayOutputStream) outputStream;
		return baos.toByteArray();
	}

	public void setSeparador(String separador) {
		this.separador = separador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}