package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Dados do BancoVO
 * 
 * @author WDEV
 */
public class BancoVO implements Serializable {

	private static final long serialVersionUID = -5236604267846234062L;

	private Integer codigo;

	public BancoVO() {
		// Construtor Default
		super();
	}

	public BancoVO(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return "BancoVO [codigo=" + codigo + "]";
	}

	
	

}
