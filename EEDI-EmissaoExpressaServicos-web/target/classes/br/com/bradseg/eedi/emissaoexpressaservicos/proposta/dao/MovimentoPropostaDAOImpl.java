package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UsuarioUltimaAtualizacao;

/**
 * Implementa��o dos servi�os de acesso a dados de movimento da proposta
 * 
 * @author WDEV
 */
@Repository
public class MovimentoPropostaDAOImpl extends JdbcDao implements MovimentoPropostaDAO {

	@Autowired
	private DataSource dataSource;

	private static final Logger LOGGER = LoggerFactory.getLogger(MovimentoPropostaDAOImpl.class);
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public MovimentoPropostaVO salvar(MovimentoPropostaVO movimentoProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO DBPROD.MOVTO_PPSTA_DNTAL (NSEQ_PPSTA_DNTAL, CSIT_DNTAL_INDVD, DINIC_MOVTO_PPSTA, DULT_ATULZ_REG, CRESP_ULT_ATULZ)");
		sql.append(" VALUES (:proposta, :situacaoProposta, :dataInicioMovimento, CURRENT_TIMESTAMP, :responsavelAtualizacao)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("proposta", movimentoProposta.getProposta().getNumeroSequencial());
		params.addValue("situacaoProposta", movimentoProposta.getSituacao().getCodigo());
		params.addValue("dataInicioMovimento", movimentoProposta.getDataInicio().toDate());
		params.addValue("responsavelAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());

	    LOGGER.error("INFO: SALVAR MOVIMENTO: "+sql.toString()+" PAR�METROS: "+params.getValues());
			getJdbcTemplate().update(sql.toString(), params);
	
		return movimentoProposta;
	}

	public MovimentoPropostaVO salvar(MovimentoPropostaVO movimentoProposta, String responsavelAtualizacao) {
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO DBPROD.MOVTO_PPSTA_DNTAL (NSEQ_PPSTA_DNTAL, CSIT_DNTAL_INDVD, DINIC_MOVTO_PPSTA, DULT_ATULZ_REG, CRESP_ULT_ATULZ)");
		sql.append(" VALUES (:proposta, :situacaoProposta, :dataInicioMovimento, CURRENT_TIMESTAMP, :responsavelAtualizacao)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("proposta", movimentoProposta.getProposta().getNumeroSequencial());
		params.addValue("situacaoProposta", movimentoProposta.getSituacao().getCodigo());
		params.addValue("dataInicioMovimento", movimentoProposta.getDataInicio().toDate());
		params.addValue("responsavelAtualizacao", responsavelAtualizacao);

	    LOGGER.error("INFO: SALVAR MOVIMENTO: "+sql.toString()+" PAR�METROS: "+params.getValues());
			getJdbcTemplate().update(sql.toString(), params);
	
		return movimentoProposta;
	}

	public MovimentoPropostaVO consultarUltimoMovimento(Long numeroSequencial) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT NSEQ_PPSTA_DNTAL, CSIT_DNTAL_INDVD, DINIC_MOVTO_PPSTA, DULT_ATULZ_REG, CRESP_ULT_ATULZ ");
		sql.append(" FROM DBPROD.MOVTO_PPSTA_DNTAL");
		sql.append(" WHERE NSEQ_PPSTA_DNTAL = :sequencialProposta");
		sql.append(" ORDER BY DULT_ATULZ_REG DESC");
		sql.append(" FETCH FIRST 1 ROWS ONLY");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequencialProposta", numeroSequencial);
	    LOGGER.error("INFO: CONSULTAR �LTIMO MOVIMENTO: "+sql.toString()+" PAR�METROS: "+params.getValues());
		return getJdbcTemplate().queryForObject(sql.toString(), params, new MovimentoPropostaRowMapper());
	}
	
	public List<MovimentoPropostaVO> obterListaDeMovimentosDeProposta(Long numeroSequencial) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT NSEQ_PPSTA_DNTAL, CSIT_DNTAL_INDVD, DINIC_MOVTO_PPSTA, DULT_ATULZ_REG, CRESP_ULT_ATULZ ");
		sql.append(" FROM DBPROD.MOVTO_PPSTA_DNTAL");
		sql.append(" WHERE NSEQ_PPSTA_DNTAL = :sequencialProposta");
//		sql.append(" ORDER BY DULT_ATULZ_REG DESC");


		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequencialProposta", numeroSequencial);
	    LOGGER.error("INFO: CONSULTAR LISTA DE MOVIMENTOS: "+sql.toString()+" PAR�METROS: "+params.getValues());
		return getJdbcTemplate().query(sql.toString(), params, new MovimentoPropostaRowMapper());
	}
	
	
	public boolean verificarSePossuiMovimentoDeCancelamento(Long numeroSequencial) {
		
		boolean movimentoCancelada = false;
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(*)  ");
		sql.append(" FROM DBPROD.MOVTO_PPSTA_DNTAL ");
		sql.append(" WHERE NSEQ_PPSTA_DNTAL = :sequencialProposta ");
		sql.append(" AND ( CSIT_DNTAL_INDVD = 5 OR CSIT_DNTAL_INDVD = 7 )  ");


		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequencialProposta", numeroSequencial);

		Integer quantidadeCancelada = getJdbcTemplate().queryForInt(sql.toString(), params);
		
		if(quantidadeCancelada != null && quantidadeCancelada > 0 ){
			movimentoCancelada = true;
		}
		return movimentoCancelada;
	}
	
	public Date obterDataPrimeiroMovimentoProposta(Long numeroSequencial){
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT  DULT_ATULZ_REG																		  \n");
		sql.append(" FROM DBPROD.MOVTO_PPSTA_DNTAL WHERE NSEQ_PPSTA_DNTAL =    :numeroSequencial                  \n");
		sql.append(" ORDER BY DULT_ATULZ_REG ASC FETCH FIRST 1 ROWS ONLY                                          \n");
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("numeroSequencial", numeroSequencial);
		
		try{
			
			return getJdbcTemplate().queryForObject(sql.toString(), params, Date.class);
		}catch(Exception e){
			LOGGER.error("INFO: N�O FOI POSSIVEL OBTER PRIMEIRO MOVIMENTO");
		}
		return null;
	}

	/**
	 * Mapeamento de dados da listagem de proposta
	 * @author WDEV
	 */
	private static final class MovimentoPropostaRowMapper implements RowMapper<MovimentoPropostaVO> {
		public MovimentoPropostaVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
			MovimentoPropostaVO movimentoProposta = new MovimentoPropostaVO();
			movimentoProposta.setSituacao(SituacaoProposta.obterPorCodigo(resultSet.getInteger("CSIT_DNTAL_INDVD")));
			movimentoProposta.setProposta(new PropostaVO());
			movimentoProposta.getProposta().setNumeroSequencial(resultSet.getLong("NSEQ_PPSTA_DNTAL"));
			movimentoProposta.setDataInicio(resultSet.getDateTime("DINIC_MOVTO_PPSTA"));

			return movimentoProposta;
		}
	}

	/**
	 * Metodo responsavel por verificar se proposta possui registro com o movimento informado.
	 * 
	 * @param numeroSequencial - sequencial da proposta informada.
	 * @param situacao - situacao da proposta.
	 * @return boolean - true se possui movimento, false se nao possui movimento.
	 */
	public boolean verificarSePossuiMovimentoInformado(Long numeroSequencial, SituacaoProposta situacao) {
		
		boolean movimentoConsultado = false;
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(NSEQ_PPSTA_DNTAL)                        ");
		sql.append(" FROM DBPROD.MOVTO_PPSTA_DNTAL                         ");
		sql.append(" WHERE NSEQ_PPSTA_DNTAL = :sequencialProposta          ");
		sql.append(" AND ( CSIT_DNTAL_INDVD = :movimento )                 ");


		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("movimento", situacao.getCodigo());
		params.addValue("sequencialProposta", numeroSequencial);

		Integer quantidadeMovimento = getJdbcTemplate().queryForInt(sql.toString(), params);
		
		if(quantidadeMovimento != null && quantidadeMovimento > 0 ){
			movimentoConsultado = true;
		}
		return movimentoConsultado;
	}
	
	public Integer obterUltimoCodigoMovimentoPorSequencial(Long sequencialProposta) {
		
		StringBuilder sql = new StringBuilder();
		
		Integer situacaoProposta = null;
		sql.append(" SELECT CSIT_DNTAL_INDVD                      ");
		sql.append(" FROM DBPROD.MOVTO_PPSTA_DNTAL                ");
		sql.append(" WHERE NSEQ_PPSTA_DNTAL = :sequencialProposta ");
		sql.append(" ORDER BY DULT_ATULZ_REG DESC                 ");
		sql.append(" FETCH FIRST 1 ROWS ONLY                      ");

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("sequencialProposta", sequencialProposta);
	    LOGGER.error("INFO: CONSULTAR �LTIMO MOVIMENTO: "+sql.toString()+" PAR�METROS: "+param.getValues());
	    try{
	    	situacaoProposta =  getJdbcTemplate().queryForInt(sql.toString(), param);
	    }catch(Exception e){
		    LOGGER.error("INFO: ERRO AO OBTER �LTIMO MOVIMENTO: " + e.getMessage());
	    }
	    return situacaoProposta;

	}
	
	public List<MovimentoPropostaVO> listarMovimentoProposta(Long sequencialProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT																							");
		sql.append("	NSEQ_PPSTA_DNTAL, CSIT_DNTAL_INDVD, DINIC_MOVTO_PPSTA, DULT_ATULZ_REG, CRESP_ULT_ATULZ		");
		sql.append(" FROM																							");
		sql.append("	DBPROD.MOVTO_PPSTA_DNTAL																	");
		sql.append(" WHERE																							");
		sql.append("	NSEQ_PPSTA_DNTAL = :sequencialProposta 														");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequencialProposta", sequencialProposta);
	    LOGGER.error("INFO: CONSULTAR MOVIMENTOS: " + sql.toString() + " PAR�METROS: " + params.getValues());
		return getJdbcTemplate().query(sql.toString(), params, new MovimentoPropostaRowMapper());
	}
	

}
