package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import org.apache.commons.lang.StringUtils;

/**
 * Verifica se o n�mero da CNS - Cart�o Nacional de Sa�de � v�lido
 * 
 * @author WDEV
 */
public class ValidadorCNS {

	/**
	 * Verifica se o n�mero do CNS - Cart�o Nacional de Sa�de � v�lido
	 * @param cns N�mero do cart�o nacional de sa�de
	 * @return verdadeiro se o CNS � v�lido
	 */
	public static boolean validar(Long cns) {
		if (cns == null) {
			return false;
		}
		return validar(String.valueOf(cns));
	}

	/**
	 * Verifica se o n�mero do CNS - Cart�o Nacional de Sa�de � v�lido
	 * @param cns N�mero do cart�o nacional de sa�de
	 * @return verdadeiro se o CNS � v�lido
	 */
	public static boolean validar(String cns) {
		if (StringUtils.isBlank(cns) || cns.trim().length() != 15) {
			return false;
		}

		if (cns.matches("[12]\\d{10}00[0-1]\\d") || cns.matches("[7-9]\\d{14}")) {
			char[] cs = cns.toCharArray();
			int soma = 0;
			for (int i = 0; i < cs.length; i++) {
				soma += Character.digit(cs[i], 10) * (15 - i);
			}
			return soma % 11 == 0;
		}

		return false;
	}

}