package br.com.bradseg.eedi.emissaoexpressaservicos.vo;


import java.io.Serializable;

/**
 * VO com dados do Produtor.
 * 
 * @author EEDI
 */

public class DadosProdutorVO implements Serializable {

	/**
	 * Construtor da classe DadosProdutor
	 */

	private static final long serialVersionUID = -1028444040988287475L;

	private String nomeProdutor;
	private String cpdProdutor;
	private String cpfCnpjProdutor;
	private String tipoProdutor;

	public String getNomeProdutor() {
		return nomeProdutor;
	}

	public void setNomeProdutor(String nomeProdutor) {
		this.nomeProdutor = nomeProdutor;
	}

	public String getCpdProdutor() {
		return cpdProdutor;
	}

	public void setCpdProdutor(String cpdProdutor) {
		this.cpdProdutor = cpdProdutor;
	}

	public String getCpfCnpjProdutor() {
		return cpfCnpjProdutor;
	}

	public void setCpfCnpjProdutor(String cpfCnpjProdutor) {
		this.cpfCnpjProdutor = cpfCnpjProdutor;
	}

	public String getTipoProdutor() {
		return tipoProdutor;
	}

	public void setTipoProdutor(String tipoProdutor) {
		this.tipoProdutor = tipoProdutor;
	}
}