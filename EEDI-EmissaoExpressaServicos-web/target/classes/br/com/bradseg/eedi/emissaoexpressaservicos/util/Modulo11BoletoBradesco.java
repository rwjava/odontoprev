package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;

public class Modulo11BoletoBradesco {

	public static Integer gerarDigitoVerificador(String valorSemDigitoVerificador) {
		Preconditions.checkNotNull(valorSemDigitoVerificador);
		if (StringUtils.isEmpty(valorSemDigitoVerificador)) {
			throw new IllegalArgumentException("O valor n�o pode estar vazio");
		}
		int i = 2;
		int soma = 0;
		for (char c : StringUtils.reverse(valorSemDigitoVerificador).toCharArray()) {
			soma += (new Integer(c + "")) * i;
			i++;
			if (i == 10) {
				i = 2;
			}
		}
		int digito = 11 - (soma % 11);
		if (digito == 0 || digito == 1 || digito > 9) {
			digito = 1;
		}
		return digito;
	}
}
