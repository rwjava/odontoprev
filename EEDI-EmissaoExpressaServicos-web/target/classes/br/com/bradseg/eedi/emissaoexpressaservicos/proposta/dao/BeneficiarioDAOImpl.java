package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.endereco.dao.EnderecoRowMapper;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DependenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EstadoCivil;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.GrauParentesco;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.Sexo;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoBeneficiario;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UsuarioUltimaAtualizacao;

import com.google.common.collect.Sets;

/**
 * Implementação do acesso a dados do beneficiario
 * 
 * @author WDEV
 */
@Repository
public class BeneficiarioDAOImpl extends JdbcDao implements BeneficiarioDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(BeneficiarioDAOImpl.class);

	@Autowired
	private DataSource dataSource;
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public List<BeneficiarioVO> listarPorProposta(PropostaVO proposta) {
		return listarPorPropostas(Sets.newHashSet(proposta.getNumeroSequencial()));
	}

	public List<BeneficiarioVO> listarPorPropostas(Set<Long> idsPropostas) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT                                                                                       \n");
		sql.append(" BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL,                                                          \n");
		sql.append(" BNEFC_DNTAL_INDVD.CBNEFC_DNTAL_INDVD,                                                        \n");
		sql.append(" BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL,                                                          \n");
		sql.append(" BNEFC_DNTAL_INDVD.IBNEFC_DNTAL_INDVD,                                                        \n");
		sql.append(" BNEFC_DNTAL_INDVD.NCPF_BNEFC_DNTAL,                                                          \n");
		sql.append(" BNEFC_DNTAL_INDVD.CGRAU_PARNT_DNTAL,                                                         \n");
		sql.append(" BNEFC_DNTAL_INDVD.CEST_CVIL_BNEFC,                                                           \n");
		sql.append(" BNEFC_DNTAL_INDVD.DNASC_BNEFC_DNTAL,                                                         \n");
		sql.append(" BNEFC_DNTAL_INDVD.IMAE_BNEFC_DNTAL,                                                          \n");
		sql.append(" BNEFC_DNTAL_INDVD.CSEXO_BNEFC_DNTAL,                                                         \n");
		sql.append(" BNEFC_DNTAL_INDVD.NDECLR_NASCD_VIVO,                                                         \n");
		sql.append(" BNEFC_DNTAL_INDVD.NCATAO_NACIO_SAUDE,                                                        \n");
		sql.append(" BNEFC_DNTAL_INDVD.CENDER_DNTAL_INDVD,                                                        \n");
		sql.append(" BNEFC_DNTAL_INDVD.REMAIL_BNEFC_DNTAL_INDVD,                                                  \n");
		sql.append(" BNEFC_DNTAL_INDVD.NCART_BNEFC_PLANO,                                                         \n");
		sql.append(" ENDER_DNTAL_INDVD.CENDER_DNTAL_INDVD,                                                        \n");
		sql.append(" ENDER_DNTAL_INDVD.ILOGDR_ENDER_COTAC,                                                        \n");
		sql.append(" ENDER_DNTAL_INDVD.NLOGDR_ENDER_COTAC,                                                        \n");
		sql.append(" ENDER_DNTAL_INDVD.RCOMPL_ENDER_COTAC,                                                        \n");
		sql.append(" ENDER_DNTAL_INDVD.IBAIRO,				                                                      \n");
		sql.append(" ENDER_DNTAL_INDVD.ICIDDE,					                                                  \n");
		sql.append(" ENDER_DNTAL_INDVD.CSGL_UF,																      \n");
		sql.append(" ENDER_DNTAL_INDVD.CNRO_CEP,																   \n");
		sql.append(" CGRAU_PARNT_BNEFC_PROPN                                                                      \n");
		sql.append(" FROM DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD                                              \n");
		sql.append(" LEFT OUTER JOIN DBPROD.ENDER_DNTAL_INDVD ENDER_DNTAL_INDVD                                   \n");
		sql.append(" ON BNEFC_DNTAL_INDVD.CENDER_DNTAL_INDVD = ENDER_DNTAL_INDVD.CENDER_DNTAL_INDVD               \n");
		sql.append(" LEFT OUTER JOIN DBPROD.UF UF ON UF.CSGL_UF = ENDER_DNTAL_INDVD.CSGL_UF                       \n");
		sql.append(" WHERE BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL in (:idSequencialProposta)                          \n");
		sql.append(" ORDER BY BNEFC_DNTAL_INDVD.CTPO_BNEFC_DNTAL DESC, BNEFC_DNTAL_INDVD.CBNEFC_DNTAL_INDVD ASC   \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("idSequencialProposta", idsPropostas);
		LOGGER.error("INFO: LISTAR POR PROPOSTA :"+sql.toString()+" PARÂMETROS: "+params.getValues());
		return getJdbcTemplate().query(sql.toString(), params, new BeneficiariosResultSetExtractor());

	}

	/**
	 * Realiza o mapeamento do result set encontrado na busca por beneficiários de uma proposta para um objeto Beneficiarios, do titular ou do dependente
	 *
	 * @author WDEV
	 */
	private static final class BeneficiariosResultSetExtractor implements RowMapper<BeneficiarioVO> {
		public BeneficiarioVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
			if (isBeneficiarioTitular(resultSet.getInteger("CTPO_BNEFC_DNTAL"))) {
				TitularVO titular = new TitularVO();

				if (resultSet.getLong("CENDER_DNTAL_INDVD") != null) {
					titular.setEndereco(new EnderecoRowMapper().mapRow(rs, rowNum));
				}
				
				if(resultSet.getInteger("CGRAU_PARNT_BNEFC_PROPN") != null){
					titular.setGrauParentescoTitulaProponente(GrauParentesco.buscaPorCodigo(resultSet.getInteger("CGRAU_PARNT_BNEFC_PROPN")));
				}

				preencherDadosBeneficiario(titular, resultSet);
				return titular;
			} else {
				DependenteVO dependente = new DependenteVO();
				dependente.setGrauParentesco(GrauParentesco.buscaPorCodigo(resultSet.getInteger("CGRAU_PARNT_DNTAL")));
				dependente.setGrauParentescoBeneficiario(GrauParentesco.buscaPorCodigo(resultSet.getInteger("CGRAU_PARNT_BNEFC_PROPN")));
				preencherDadosBeneficiario(dependente, resultSet);
				return dependente;
			}
		}

		private void preencherDadosBeneficiario(BeneficiarioVO beneficiario, ResultSetAdapter resultSet) throws SQLException {
			beneficiario.setCodigo(resultSet.getInteger("CBNEFC_DNTAL_INDVD"));
			beneficiario.setNome(Strings.maiusculas(resultSet.getString("IBNEFC_DNTAL_INDVD")));
			beneficiario.setCpf(Strings.normalizarCPFMaiorQueZero(resultSet.getString("NCPF_BNEFC_DNTAL")));
			beneficiario.setEstadoCivil(EstadoCivil.buscaPorCodigo(resultSet.getInteger("CEST_CVIL_BNEFC")));
			beneficiario.setDataNascimento(resultSet.getLocalDate("DNASC_BNEFC_DNTAL"));		
			beneficiario.setNomeMae(Strings.maiusculas(resultSet.getString("IMAE_BNEFC_DNTAL")));
			beneficiario.setSexo(Sexo.valueOf(resultSet.getString("CSEXO_BNEFC_DNTAL")));
			beneficiario.setEmail(Strings.maiusculas(resultSet.getString("REMAIL_BNEFC_DNTAL_INDVD")));			
			beneficiario.setNumeroDeclaracaoNascidoVivo(resultSet.getLong("NDECLR_NASCD_VIVO"));
			beneficiario.setNumeroCartaoNacionalSaude(resultSet.getLong("NCATAO_NACIO_SAUDE"));
			beneficiario.setNumeroCarteiraOdontoprev(resultSet.getLong("NCART_BNEFC_PLANO"));
			beneficiario.setProposta(new PropostaVO());
			beneficiario.getProposta().setNumeroSequencial(resultSet.getLong("NSEQ_PPSTA_DNTAL"));
			//beneficiario.setPlano(obterDadosPlano(resultSet));
		}

//		private PlanoVO obterDadosPlano(ResultSetAdapter resultSet) throws SQLException {
//			if (resultSet.getInteger("CPLANO_DNTAL_INDVD") == null) {
//				return null;
//			}
//			PlanoVO plano = new PlanoVO();
//			plano.setCodigo(resultSet.getInteger("CPLANO_DNTAL_INDVD"));
//			plano.setRegistro(resultSet.getLong("CREG_PLANO_DNTAL"));
//			plano.setDescricao(Strings.maiusculas(resultSet.getString("RPLANO_DNTAL_INDVD")));
//			plano.setValorMensalidadeTitular(resultSet.getBigDecimal("VMESD_PLANO_TTLAR"));
//			plano.setValorMensalidadeDependente(resultSet.getBigDecimal("VMESD_PLANO_DEPDT"));
//			return plano;
//		}

		private boolean isBeneficiarioTitular(Integer codigoTipoBeneficiario) {
			return TipoBeneficiario.TITULAR.equals(TipoBeneficiario.obterTipoBeneficiarioPorCodigo(codigoTipoBeneficiario));
		}
	}

	public void salvarTitular(TitularVO titular, Integer parentesco) {
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("tipo", TipoBeneficiario.TITULAR.getCodigo());
			params.addValue("grauParentescoBeneficiario", titular.getGrauParentescoTitulaProponente().getCodigo());
			params.addValue("grauParentesco", null);
		if (titular.getEndereco() != null) {
			params.addValue("codigoEndereco", titular.getEndereco().getCodigo());
		}
		salvarBeneficiario(titular, params);
	}
	
public void salvarTitularServicos(TitularVO titular) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("tipo", TipoBeneficiario.TITULAR.getCodigo());
			params.addValue("grauParentesco", null);
			params.addValue("grauParentescoBeneficiario", null);
		if (titular.getEndereco() != null) {
			params.addValue("codigoEndereco", titular.getEndereco().getCodigo());
		}
		salvarBeneficiario(titular, params);
	}

	public void salvarDependentes(List<DependenteVO> dependentes, TitularVO titular) {
		if (dependentes != null) {
			for (DependenteVO dependenteVO : dependentes) {
				salvarDependente(dependenteVO, titular);
			}
		}
	}

	public void salvarDependente(DependenteVO dependente, TitularVO titular) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("tipo", TipoBeneficiario.DEPENDENTE.getCodigo());
		if(null != dependente.getGrauParentesco()){
			params.addValue("grauParentesco", dependente.getGrauParentesco().getCodigo());	
		}else{
			params.addValue("grauParentesco", null);	
		}
		if(null != dependente.getGrauParentescoBeneficiario()){
			params.addValue("grauParentescoBeneficiario", dependente.getGrauParentescoBeneficiario().getCodigo());	
		}else{
			params.addValue("grauParentescoBeneficiario", null);
		}
		
		params.addValue("codigoEndereco", null);
		salvarBeneficiario(dependente, params);
	}

	protected void salvarBeneficiario(BeneficiarioVO beneficiario, MapSqlParameterSource parameters) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT                                \n");
		sql.append("INTO                                  \n");
		sql.append("    DBPROD.BNEFC_DNTAL_INDVD          \n");
		sql.append("    (                                 \n");
		sql.append("        NSEQ_PPSTA_DNTAL,             \n");
		sql.append("        CBNEFC_DNTAL_INDVD,           \n");
		sql.append("        CTPO_BNEFC_DNTAL,             \n");
		sql.append("        IBNEFC_DNTAL_INDVD,           \n");
		sql.append("        NCPF_BNEFC_DNTAL,             \n");
		sql.append("        CGRAU_PARNT_DNTAL,            \n");
		sql.append("        CEST_CVIL_BNEFC,              \n");
		sql.append("        DNASC_BNEFC_DNTAL,            \n");
		sql.append("        IMAE_BNEFC_DNTAL,             \n");
		sql.append("        CSEXO_BNEFC_DNTAL,            \n");
		sql.append("        NDECLR_NASCD_VIVO,            \n");
		sql.append("        NCATAO_NACIO_SAUDE,           \n");
		sql.append("        CENDER_DNTAL_INDVD,           \n");
		sql.append("        DULT_ATULZ_REG,               \n");
		sql.append("        REMAIL_BNEFC_DNTAL_INDVD,     \n");
		sql.append("        CRESP_ULT_ATULZ,               \n");
		sql.append("        CGRAU_PARNT_BNEFC_PROPN      \n");
		sql.append("    )                                 \n");
		sql.append("    VALUES                            \n");
		sql.append("    (                                 \n");
		sql.append("        :sequencialProposta,          \n");
		sql.append("        :codigoBeneficiario,          \n");
		sql.append("        :tipo,                        \n");
		sql.append("        :nome,                        \n");
		sql.append("        :cpf,                         \n");
		sql.append("        :grauParentesco,              \n");
		sql.append("        :estadoCivil,                 \n");
		sql.append("        :dataNascimento,              \n");
		sql.append("        :nomeMae,                     \n");
		sql.append("        :sexo,                        \n");
		sql.append("        :dnv,                         \n");
		sql.append("        :cns,                         \n");
		sql.append("        :codigoEndereco,              \n");
		sql.append("        CURRENT_TIMESTAMP,            \n");
		sql.append("        :email,                       \n");
		sql.append("        :usuarioUltimaAtualizacao,     \n");
		sql.append("        :grauParentescoBeneficiario   \n");
		sql.append("    )                                 \n");
		

		preencherParametrosBeneficiario(beneficiario, parameters);
		LOGGER.error("INFO: SALVAR BENEFICIÁRIO :"+sql.toString()+" PARÂMETROS: "+parameters.getValues());

		getJdbcTemplate().update(sql.toString(), parameters);
	}

	private MapSqlParameterSource preencherParametrosBeneficiario(BeneficiarioVO beneficiario, MapSqlParameterSource params) {
		params.addValue("sequencialProposta", beneficiario.getProposta().getNumeroSequencial());
		params.addValue("codigoBeneficiario", beneficiario.getCodigo());
		params.addValue("nome", Strings.maiusculas(beneficiario.getNome()));
		String cpf = Strings.desnormalizarCPF(beneficiario.getCpf());
		if (cpf == null) {
			params.addValue("cpf", 0);
		} else {
			params.addValue("cpf", cpf);
		}
		params.addValue("estadoCivil", beneficiario.getEstadoCivil().getCodigo());
		params.addValue("dataNascimento", beneficiario.getDataNascimento().toDateTimeAtStartOfDay().toDate());
		params.addValue("nomeMae", Strings.maiusculas(beneficiario.getNomeMae()));
		params.addValue("sexo", beneficiario.getSexo().name());
		params.addValue("dnv", beneficiario.getNumeroDeclaracaoNascidoVivo());
		params.addValue("cns", beneficiario.getNumeroCartaoNacionalSaude());
		//params.addValue("codigoPlano", beneficiario.getPlano().getCodigo());
		if(beneficiario.getTipoBeneficiario().equals(TipoBeneficiario.TITULAR)){
			params.addValue("email", beneficiario.getEmail());
		}else{
			params.addValue("email", null);
		}
		params.addValue("usuarioUltimaAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());

		return params;
	}

	public void remover(PropostaVO proposta) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM DBPROD.BNEFC_DNTAL_INDVD WHERE NSEQ_PPSTA_DNTAL = :proposta");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("proposta", proposta.getNumeroSequencial());
		LOGGER.error("INFO: REMOVER BENEFICIÁRIO :"+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public SituacaoProposta listarUltimaSituacaoPropostaBeneficiario(String cpf, CanalVendaVO canalVenda) {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD FROM                                \n");
		sql.append(" DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL                                    \n");
		sql.append(" INNER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD                         \n");
		sql.append(" ON (BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)  \n");
		sql.append(" INNER JOIN DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD                         \n");
		sql.append(" ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)  \n");
		sql.append(" WHERE BNEFC_DNTAL_INDVD.NCPF_BNEFC_DNTAL = :cpf                               \n");
		sql.append(" AND PPSTA_DNTAL_INDVD.CCANAL_VDA_DNTAL_INDVD = :canalVenda                    \n");
		sql.append(" ORDER BY MOVTO_PPSTA_DNTAL.DULT_ATULZ_REG DESC FETCH FIRST 1 ROWS ONLY        \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("cpf", Strings.desnormalizarCPFParaNumero(cpf));
		params.addValue("canalVenda", canalVenda.getCodigo());
		LOGGER.error("INFO: LISTAR ÚLTIMA SITUAÇÃO PROPOSTA BENEFICIÁRIO:"+sql.toString()+" PARÂMETROS: "+params.getValues());

		try {
			return SituacaoProposta.obterPorCodigo(getJdbcTemplate().queryForInt(sql.toString(), params));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma situação de proposta encontrada para o titular.");
			return null;
		}
	}
	
	public SituacaoProposta listarUltimaSituacaoPropostaBeneficiario(String cpf) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT																																  \n");
		sql.append(" MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD																									  \n");
		sql.append(" FROM                                        																						  \n");
		sql.append(" DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL																							  \n");
		sql.append(" INNER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD ON (BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)   \n");
		sql.append(" INNER JOIN DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)   \n");
		sql.append(" WHERE BNEFC_DNTAL_INDVD.NCPF_BNEFC_DNTAL = :cpf 																					  \n");
		sql.append(" ORDER BY MOVTO_PPSTA_DNTAL.DULT_ATULZ_REG DESC FETCH FIRST 1 ROWS ONLY																  \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("cpf", Strings.desnormalizarCPFParaNumero(cpf));
		LOGGER.error("INFO: LISTAR ÚLTIMA SITUAÇÃO PROPOSTA BENEFICIÁRIO:"+sql.toString()+" PARÂMETROS: "+params.getValues());

		try {
			return SituacaoProposta.obterPorCodigo(getJdbcTemplate().queryForInt(sql.toString(), params));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma situação de proposta encontrada para o titular");
			return null;
		}
	}
	
	public Boolean verificarSeBeneficiarioPossuiPropostaCancelada(String cpf) {
		boolean existePropostaCancelada = false;
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT																																  \n");
		sql.append(" count(MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD)																							  \n");
		sql.append(" FROM                                        																						  \n");
		sql.append(" DBPROD.MOVTO_PPSTA_DNTAL MOVTO_PPSTA_DNTAL																							  \n");
		sql.append(" INNER JOIN DBPROD.BNEFC_DNTAL_INDVD BNEFC_DNTAL_INDVD ON (BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = MOVTO_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL)   \n");
		sql.append(" INNER JOIN DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD ON (PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = BNEFC_DNTAL_INDVD.NSEQ_PPSTA_DNTAL)   \n");
		sql.append(" WHERE BNEFC_DNTAL_INDVD.NCPF_BNEFC_DNTAL = :cpf 																					  \n");
		sql.append(" AND MOVTO_PPSTA_DNTAL.CSIT_DNTAL_INDVD IN (5)															                              \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("cpf", Strings.desnormalizarCPFParaNumero(cpf));
		LOGGER.error("INFO: VERIFICAR SE BENEFICIARIO POSSUI PROPOSTA CANCELADA OU IMPLANTADA:"+sql.toString()+" PARÂMETROS: "+params.getValues());
		
		int quantidadePropostasCanceladas = getJdbcTemplate().queryForInt(sql.toString(), params);
		try {
			//Se 
			if(quantidadePropostasCanceladas > 0){
				existePropostaCancelada = true;
			}else{
				existePropostaCancelada = false;
			}
			
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma situação de proposta encontrada para o titular");
			return false;
		}
		
		return existePropostaCancelada;
	}

	public void atualizarCarteiraNacional(String numeroCarteiraNacional, String cpfBeneficiario, Long sequencialProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE                                          \n");
		sql.append(" DBPROD.BNEFC_DNTAL_INDVD                        \n");
		sql.append(" SET NCART_BNEFC_PLANO = :numeroCarteiraNacional \n");
		sql.append(" WHERE NCPF_BNEFC_DNTAL = :cpfBeneficiario       \n");
		sql.append(" AND NSEQ_PPSTA_DNTAL = :sequencialProposta      \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("numeroCarteiraNacional", numeroCarteiraNacional);
		params.addValue("cpfBeneficiario", Strings.desnormalizarCPF(cpfBeneficiario));
		params.addValue("sequencialProposta", sequencialProposta);
		LOGGER.error("INFO: ATUALIZAR CARTEIRA NACIONAL :"+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}
	
	public List<Long> listarPropostasBeneficionario(String cpf) {

		StringBuilder sql = new StringBuilder()
	
		.append(" SELECT DISTINCT                              \n")
		.append("     (S.NSEQ_PPSTA_DNTAL)                     \n")
		.append(" FROM                                         \n")
		.append("     DBPROD.PPSTA_DNTAL_INDVD S               \n")
		.append(" INNER JOIN                                   \n")
		.append("     DBPROD.BNEFC_DNTAL_INDVD B               \n")
		.append(" ON                                           \n")
		.append("     B.NSEQ_PPSTA_DNTAL = S.NSEQ_PPSTA_DNTAL  \n")
		.append(" AND B.NCPF_BNEFC_DNTAL = :cpfBeneficiario    \n");
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("cpfBeneficiario", Strings.desnormalizarCPFParaNumero(cpf));
		LOGGER.error(String.format("INFO: Verificar propostar para beneficiario de cpf [%s] ", cpf));
		List<Long> propostas = null;
		
		try{
			propostas =  getJdbcTemplate().queryForList(sql.toString(), param, Long.class);
		}catch(EmptyResultDataAccessException e){
			LOGGER.error(String.format("INFO: Nenhuma proposta encontrada [%s] ", e.getMessage()));
		}
		catch(Exception e){
			LOGGER.error(String.format("INFO: Erro ao obter lista de propostas [%s] ", e.getMessage()));
		}	
		return propostas;
	}
	
	public Boolean verificarStatusProposta(Long sequencialProposta, SituacaoProposta situacao) {
		boolean existePropostaCancelada = false;
		StringBuilder sql = new StringBuilder()
	    .append(" SELECT                                            \n")
	    .append("     COUNT(M.NSEQ_PPSTA_DNTAL)                     \n")
	    .append(" FROM                                              \n")
	    .append("     DBPROD.MOVTO_PPSTA_DNTAL M                    \n")
	    .append(" WHERE                                             \n")
	    .append("     M.CSIT_DNTAL_INDVD = :situacaoProposta        \n")
	    .append("     AND M.NSEQ_PPSTA_DNTAL = :sequencialProposta  \n");
	    
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequencialProposta", sequencialProposta);
		params.addValue("situacaoProposta", situacao.getCodigo());
		
		LOGGER.error("INFO: VERIFICAR SITUACAO DA PROPOSTA: "+sql.toString()+" PARÂMETROS: "+params.getValues());
		
		int quantidadePropostasCancelada = getJdbcTemplate().queryForInt(sql.toString(), params);
		try {
			//Se 
			if(quantidadePropostasCancelada > 0){
				existePropostaCancelada = true;
			}else{
				existePropostaCancelada = false;
			}
			
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma situação de proposta encontrada para o titular");
			return false;
		}
		
		return existePropostaCancelada;
	}
	

}