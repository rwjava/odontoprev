package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoBeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UsuarioUltimaAtualizacao;

/**
 * Implementação do acesso a dados do movimento do beneficiário.
 * 
 * @author WDEV
 */
@Repository
public class MovimentoBeneficiarioDAOImpl extends JdbcDao implements MovimentoBeneficiarioDAO {

	@Autowired
	private DataSource dataSource;

	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void salvar(MovimentoBeneficiarioVO movimentoBeneficiario) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO DBPROD.MOVTO_BNEFC_DNTAL");
		sql.append(" (NSEQ_PPSTA_DNTAL, CBNEFC_DNTAL_INDVD, CSIT_DNTAL_INDVD, DINIC_MOVTO_BNEFC, DULT_ATULZ_REG, CRESP_ULT_ATULZ)");
		sql.append(" VALUES");
		sql.append(" (:sequenciaProposta, :beneficiario, :situacaoProposta, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, :responsavelAtualizacao)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequenciaProposta", movimentoBeneficiario.getProposta().getNumeroSequencial());
		params.addValue("beneficiario", movimentoBeneficiario.getBeneficiario().getCodigo());
		params.addValue("situacaoProposta", movimentoBeneficiario.getSituacaoProposta().getCodigo());
		params.addValue("responsavelAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());

		getJdbcTemplate().update(sql.toString(), params);
	}

	public void removerPorProposta(PropostaVO proposta) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM DBPROD.MOVTO_BNEFC_DNTAL WHERE NSEQ_PPSTA_DNTAL = :proposta");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("proposta", proposta.getNumeroSequencial());

		getJdbcTemplate().update(sql.toString(), params);
	}

}