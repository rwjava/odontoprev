package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipos de cobran�a
 * 
 * @author WDEV
 */
public enum TipoCobranca {

	MENSAL(1, "Mensal"), ANUAL(2, "Anual");

	private Integer codigo;
	private String descricao;

	private TipoCobranca(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * Busca por codigo.
	 * 
	 * @param codigo - codigo do tipo de cobran�a
	 * @return o tipo cobran�a
	 */
	public static TipoCobranca buscaPor(Integer codigo) {
		for (TipoCobranca tipo : TipoCobranca.values()) {
			if (tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}
		return null;
	}

}