package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ValorPlanoVO;

/**
 * Row Mapper da classe Plano
 * 
 * @author Bradesco Seguros
 */
public class PlanoComValorRowMapper implements RowMapper<PlanoVO> {

	/**
	 * Mapeia row a um objeto
	 * 
	 * @param rs ResultSet
	 * @param arg1 indice da linha
	 * @return objeto mapeado
	 *
	 */
	
	
	public PlanoVO mapRow(ResultSet rs, int arg1) throws SQLException {
        PlanoVO plano = new PlanoVO();
        plano.setValorPlanoVO(new ValorPlanoVO());
        //plano.setPlanoAnsVO(new PlanoAnsVO());
        plano.setCodigo(rs.getLong("CPLANO_DNTAL_INDVD"));
        plano.setNome(rs.getString("RPLANO_DNTAL_INDVD"));
        plano.getValorPlanoVO().setValorMensalTitular(rs.getDouble("VMESD_PLANO_TTLAR"));
        plano.getValorPlanoVO().setValorMensalDependente(rs.getDouble("VMESD_PLANO_DEPDT"));
        plano.getValorPlanoVO().setValorAnualTitular(rs.getDouble("VANUDD_PLANO_DNTAL_TTLAR"));
        plano.getValorPlanoVO().setValorAnualDependente(rs.getDouble("VANUDD_PLANO_DNTAL_DEPDT"));
        plano.setDescricaoCarenciaPeriodoAnual(rs.getString("RPER_CAREN_PLANO_ANO"));
        plano.setDescricaoCarenciaPeriodoMensal(rs.getString("RPER_CAREN_PLANO_MES"));
        plano.setDataInicioVigencia(new DateTime(rs.getDate("DINIC_PLANO_INDVD")));

        return plano;
    }
}