package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Responsavel por conter o retorno do movimento da Proposta
 * 
 * @author WDEV
 *
 */
public class RetornoMovimentoPropostaVO implements Serializable {

	private static final long serialVersionUID = 7524897854200409627L;

	private Integer codigoRetornoMovimento;

	private String descricaoRetornoMovimento;

	private MovimentoPropostaVO movimentoProposta;

	public String getDescricaoRetornoMovimento() {
		return descricaoRetornoMovimento;
	}

	public void setDescricaoRetornoMovimento(String descricaoRetornoMovimento) {
		this.descricaoRetornoMovimento = descricaoRetornoMovimento;
	}

	public MovimentoPropostaVO getMovimentoProposta() {
		return movimentoProposta;
	}

	public void setMovimentoProposta(MovimentoPropostaVO movimentoProposta) {
		this.movimentoProposta = movimentoProposta;
	}

	public Integer getCodigoRetornoMovimento() {
		return codigoRetornoMovimento;
	}

	public void setCodigoRetornoMovimento(Integer codigoRetornoMovimento) {
		this.codigoRetornoMovimento = codigoRetornoMovimento;
	}

}
