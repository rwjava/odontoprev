package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;


public class PlanoAnsVO implements Serializable{
	
	private static final long serialVersionUID = 9064739460905338988L;
	
	private String nomePlanoAns;
	private Long codigoPlanoAns;
	private Long idPlano;
	private Long codigo;
	private String nome;
	private Long codigoRegistro;
	private String codigoResponsavel;
	private DateTime dataInicioVigencia;
	private DateTime dataFimVigencia;
	private String descricaoCarenciaPeriodoMensal;
	private String descricaoCarenciaPeriodoAnual;
	private Long codigoResponsavelUltimaAtualizacao;
	private LocalDate dataUltimaAtualizacao;
	private List<CanalVendaVO> listaDeCanais;
	private ValorPlanoVO valorPlanoVO;
	/**
	 * Construtor alternativo.
	 */
	public PlanoAnsVO(){
		
		
	}
	
	/**
	 * Retorna codigo.
	 *
	 * @return codigo.
	 */
	public Long getCodigo() {
		return codigo;
	}
	/**
	 * Especifica codigo.
	 *
	 * @param codigo - codigo.
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	
	/**
	 * Retorna codigoRegistro.
	 *
	 * @return codigoRegistro.
	 */
	public Long getCodigoRegistro() {
		return codigoRegistro;
	}
	/**
	 * Especifica codigoRegistro.
	 *
	 * @param codigoRegistro - codigoRegistro.
	 */
	public void setCodigoRegistro(Long codigoRegistro) {
		this.codigoRegistro = codigoRegistro;
	}
	
	public String getCodigoResponsavel() {
		return codigoResponsavel;
	}

	public void setCodigoResponsavel(String codigoResponsavel) {
		this.codigoResponsavel = codigoResponsavel;
	}



	public String getDescricaoCarenciaPeriodoMensal() {
		return descricaoCarenciaPeriodoMensal;
	}

	public void setDescricaoCarenciaPeriodoMensal(
			String descricaoCarenciaPeriodoMensal) {
		this.descricaoCarenciaPeriodoMensal = descricaoCarenciaPeriodoMensal;
	}

	public String getDescricaoCarenciaPeriodoAnual() {
		return descricaoCarenciaPeriodoAnual;
	}

	public void setDescricaoCarenciaPeriodoAnual(
			String descricaoCarenciaPeriodoAnual) {
		this.descricaoCarenciaPeriodoAnual = descricaoCarenciaPeriodoAnual;
	}

	/**
	 * Retorna codigoResponsavelUltimaAtualizacao.
	 *
	 * @return codigoResponsavelUltimaAtualizacao.
	 */
	public Long getCodigoResponsavelUltimaAtualizacao() {
		return codigoResponsavelUltimaAtualizacao;
	}
	/**
	 * Especifica codigoResponsavelUltimaAtualizacao.
	 *
	 * @param codigoResponsavelUltimaAtualizacao - codigoResponsavelUltimaAtualizacao.
	 */
	public void setCodigoResponsavelUltimaAtualizacao(Long codigoResponsavelUltimaAtualizacao) {
		this.codigoResponsavelUltimaAtualizacao = codigoResponsavelUltimaAtualizacao;
	}
	
	/**
	 * Retorna dataUltimaAtualizacao.
	 *
	 * @return dataUltimaAtualizacao.
	 */
	
	public LocalDate getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	/**
	 * Especifica dataUltimaAtualizacao.
	 *
	 * @param dataUltimaAtualizacao - dataUltimaAtualizacao.
	 */
	public void setDataUltimaAtualizacao(LocalDate dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	/**
	 * @return the valorPlanoVO
	 */
	public ValorPlanoVO getValorPlanoVO() {
		return valorPlanoVO;
	}

	/**
	 * @param valorPlanoVO the valorPlanoVO to set
	 */
	public void setValorPlanoVO(ValorPlanoVO valorPlanoVO) {
		this.valorPlanoVO = valorPlanoVO;
	}
	
	/**
	 * @return the listaDeCanais
	 */
	public List<CanalVendaVO> getListaDeCanais() {
		if(listaDeCanais == null){
			return new ArrayList<CanalVendaVO>();
		}
		return listaDeCanais;
	}

	/**
	 * @param listaDeCanais the listaDeCanais to set
	 */
	public void setListaDeCanais(List<CanalVendaVO> listaDeCanais) {
		this.listaDeCanais = listaDeCanais;
	}

	public DateTime getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(DateTime dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public DateTime getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(DateTime dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	@Override
	public String toString() {
		return "PlanoVO [codigo=" + codigo + ", nome=" + nome + ", codigoRegistro=" + codigoRegistro
				+ ", codigoResponsavel=" + codigoResponsavel + ", dataInicioVigencia=" + dataInicioVigencia
				+ ", dataFimVigencia=" + dataFimVigencia + ", descricaoCarenciaPeriodoMensal="
				+ descricaoCarenciaPeriodoMensal + ", descricaoCarenciaPeriodoAnual=" + descricaoCarenciaPeriodoAnual
				+ ", codigoResponsavelUltimaAtualizacao=" + codigoResponsavelUltimaAtualizacao
				+ ", dataUltimaAtualizacao=" + dataUltimaAtualizacao + ", listaDeCanais=" + listaDeCanais
				+ ", valorPlanoVO=" + valorPlanoVO + "]";
	}

	
	
	public String getNomePlanoAns() {
		return nomePlanoAns;
	}
	public void setNomePlanoAns(String nomePlanoAns) {
		this.nomePlanoAns = nomePlanoAns;
	}
	public Long getcodigoPlanoAns() {
		return codigoPlanoAns;
	}
	public void setcodigoPlanoAns(Long planoAns) {
		this.codigoPlanoAns = planoAns;
	}
	public Long getidPlano() {
		return idPlano;
	}
	public void setidPlano(Long idPlanoAns) {
		this.idPlano = idPlanoAns;
	}
}
	
	
