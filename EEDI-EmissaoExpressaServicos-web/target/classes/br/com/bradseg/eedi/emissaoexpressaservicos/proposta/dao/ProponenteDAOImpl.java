package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UsuarioUltimaAtualizacao;

/**
 * Implementação do acesso a dados do proponente.
 * 
 * @author WDEV
 */
@Repository
public class ProponenteDAOImpl extends JdbcDao implements ProponenteDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(BeneficiarioDAOImpl.class);
	
	@Autowired
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public ProponenteVO salvar(ProponenteVO proponente) {
		Long codigoProponente = getJdbcTemplate().queryForLong("SELECT NEXT VALUE FOR DBPROD.SQ01_PROPN_DNTAL_INDVD FROM SYSIBM.SYSDUMMY1", new MapSqlParameterSource());
		//TODO postgres
		//Long codigoProponente = getJdbcTemplate().queryForLong("SELECT COALESCE(MAX(cpropn_dntal_indvd),0)+1 FROM dbprod.PROPN_DNTAL_INDVD", new MapSqlParameterSource());

		proponente.setCodigo(codigoProponente);

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO DBPROD.PROPN_DNTAL_INDVD");
		sql.append(" (CPROPN_DNTAL_INDVD, IPROPN_DNTAL_INDVD, NCPF_PROPN_DNTAL, DNASC_PROPN_DNTAL, REMAIL_PROPN_DNTAL, DULT_ATULZ_REG, CRESP_ULT_ATULZ)");
		sql.append(" VALUES");
		sql.append(" (:codigo, :nome, :cpf, :dataNascimento, :email, CURRENT_TIMESTAMP, :usuarioUltimaAtualizacao)");

		MapSqlParameterSource params = parametrosProponente(proponente);

		getJdbcTemplate().update(sql.toString(), params);

		return proponente;
	}

	private MapSqlParameterSource parametrosProponente(ProponenteVO proponente) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigo", proponente.getCodigo());

		if (proponente.isNomePreenchido()) {
			params.addValue("nome", Strings.maiusculas(proponente.getNome()));
		} else {
			params.addValue("nome", "");
		}

		if (proponente.isCpfPreenchido()) {
			params.addValue("cpf", Strings.desnormalizarCPF(proponente.getCpf()));
		} else {
			params.addValue("cpf", "0");
		}

		if (proponente.getDataNascimento() != null) {
			params.addValue("dataNascimento", proponente.getDataNascimento().toDateTimeAtStartOfDay().toDate());
		} else {
			params.addValue("dataNascimento", null);
		}
		params.addValue("email", Strings.maiusculas(proponente.getEmail()));
		params.addValue("usuarioUltimaAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());
		return params;
	}

	public void atualizar(ProponenteVO proponente) {
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE DBPROD.PROPN_DNTAL_INDVD");
		sql.append(" SET");
		sql.append(" IPROPN_DNTAL_INDVD = :nome");
		sql.append(" , NCPF_PROPN_DNTAL = :cpf");
		sql.append(" , DNASC_PROPN_DNTAL = :dataNascimento");
		sql.append(" , REMAIL_PROPN_DNTAL = :email");
		sql.append(" , DULT_ATULZ_REG = CURRENT_TIMESTAMP");
		sql.append(" , CRESP_ULT_ATULZ = :usuarioUltimaAtualizacao");
		sql.append(" WHERE CPROPN_DNTAL_INDVD = :codigo");

		MapSqlParameterSource params = parametrosProponente(proponente);

		getJdbcTemplate().update(sql.toString(), params);
	}

	public List<Long> listarPropostasProponente(String cpf) {
		// Listar todos as proposta com proponentes de cpf informado.
		StringBuilder sql = new StringBuilder()
		.append(" SELECT DISTINCT                                         \n")
		.append(" (S.NSEQ_PPSTA_DNTAL)                                    \n")
		.append("   FROM                                                  \n")
		.append(" DBPROD.PPSTA_DNTAL_INDVD S, DBPROD.PROPN_DNTAL_INDVD P  \n")
		.append(" WHERE S.CPROPN_DNTAL_INDVD = P.CPROPN_DNTAL_INDVD       \n")
		.append(" AND NCPF_PROPN_DNTAL = :cpfProponente                   \n");
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("cpfProponente", Strings.desnormalizarCPFParaNumero(cpf));
		LOGGER.error(String.format("INFO: Verificar propostar para beneficiario de cpf [%s] ", cpf));
		List<Long> propostas = null;
		
		try{
			propostas =  getJdbcTemplate().queryForList(sql.toString(), param, Long.class);
		}catch(EmptyResultDataAccessException e){
			LOGGER.error(String.format("INFO: Nenhuma proposta encontrada [%s] ", e.getMessage()));
		}
		catch(Exception e){
			LOGGER.error(String.format("INFO: Erro ao obter lista de propostas [%s] ", e.getMessage()));
		}
		
		return propostas;
	}

}