package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Beneficiário Dependente de uma propsota
 * @author WDEV
 */
public class DependenteVO extends BeneficiarioVO {

	private static final long serialVersionUID = 5911351464459959302L;

	private GrauParentesco grauParentesco;
	private GrauParentesco grauParentescoBeneficiario;

	public boolean isFilha() {
		return isGrauParentesco(GrauParentesco.FILHA);
	}
	public boolean isPai() {
		return isGrauParentesco(GrauParentesco.PAI);
	}
	public boolean isMae() {
		return isGrauParentesco(GrauParentesco.MAE);
	}
	public boolean isFilho() {
		return isGrauParentesco(GrauParentesco.FILHO);
	}

	public boolean isConjuge() {
		return isGrauParentesco(GrauParentesco.CONJUGE);
	}

	public boolean isParentescoOutros() {
		return isGrauParentesco(GrauParentesco.OUTRO);
	}
	
	public boolean isParentescoProprio() {
		return isGrauParentescoBeneficiario(GrauParentesco.PROPRIO);
	}

	private boolean isGrauParentescoBeneficiario(GrauParentesco grauParentescoBeneficiario) {
		return grauParentescoBeneficiario.equals(this.grauParentescoBeneficiario);
	}
	
	public boolean isPaiBeneficiario() {
		return isGrauParentescoBeneficiario(GrauParentesco.PAI);
	}
	public boolean isMaeBeneficiario() {
		return isGrauParentescoBeneficiario(GrauParentesco.MAE);
	}

	
	public boolean isFilhaBeneficiario() {
		return isGrauParentescoBeneficiario(GrauParentesco.FILHA);
	}

	public boolean isFilhoBeneficiario() {
		return isGrauParentescoBeneficiario(GrauParentesco.FILHO);
	}

	public boolean isConjugeBeneficiario() {
		return isGrauParentescoBeneficiario(GrauParentesco.CONJUGE);
	}

	public boolean isParentescoOutrosBeneficiario() {
		return isGrauParentescoBeneficiario(GrauParentesco.OUTRO);
	}
	
	public boolean isParentescoProprioBeneficiario() {
		return isGrauParentescoBeneficiario(GrauParentesco.PROPRIO);
	}

	private boolean isGrauParentesco(GrauParentesco grauParentesco) {
		return grauParentesco.equals(this.grauParentesco);
	}

	public GrauParentesco getGrauParentesco() {
		return grauParentesco;
	}

	public void setGrauParentesco(GrauParentesco grauParentesco) {
		this.grauParentesco = grauParentesco;
	}

	@Override
	public TipoBeneficiario getTipoBeneficiario() {
		return TipoBeneficiario.DEPENDENTE;
	}

	@Override
	public String toString() {
		return "DependenteVO [grauParentesco=" + grauParentesco + ", grauParentescoBeneficiario=" + grauParentescoBeneficiario +"]";
	}

	public GrauParentesco getGrauParentescoBeneficiario() {
		return grauParentescoBeneficiario;
	}

	public void setGrauParentescoBeneficiario(GrauParentesco grauParentescoBeneficiario) {
		this.grauParentescoBeneficiario = grauParentescoBeneficiario;
	}
	
	

}