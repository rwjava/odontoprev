package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.OrcamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Lista os orçamentos de planos de benefícios de uma proposta
 * @author WDEV
 */
public interface OrcamentoServiceFacade {

	/**
	 * Lista os orçamentos de planos de benefícios de uma proposta
	 * @param proposta proposta
	 * @return Lista de orçamentos vigentes para a data informada
	 */
	public List<OrcamentoVO> listar(PropostaVO proposta);

}