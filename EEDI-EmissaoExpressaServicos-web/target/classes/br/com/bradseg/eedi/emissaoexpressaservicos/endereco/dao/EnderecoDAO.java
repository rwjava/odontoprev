package br.com.bradseg.eedi.emissaoexpressaservicos.endereco.dao;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UnidadeFederativaVO;

/**
 * Servi�os de acesso a dados de Endere�o
 * 
 * @author WDEV
 */
public interface EnderecoDAO {

	/**
	 * Salva o endere�o
	 * 
	 * @param endereco Endere�o
	 * @return EnderecoVO salvo
	 */
	public EnderecoVO salvar(EnderecoVO endereco);

	/**
	 * Remove o endere�o
	 * 
	 * @param endereco Endere�o
	 */
	public void remover(EnderecoVO endereco);

	/**
	 * Lista as unidades federativas
	 * 
	 * @return Lista de unidades federativas existentes
	 */
	public List<UnidadeFederativaVO> listarUnidadeFederativa();

}