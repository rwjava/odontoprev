package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import org.apache.commons.lang3.StringUtils;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CaracterPrefixoCodigoProposta;

/**
 * Gerador utilizado para criar o c�digo da proposta
 * 
 * @author WDEV
 */
public class GeradorCodigoProposta {

	private static Integer calcularValorPrefixo(String prefixo) {
		int totalPrefixo = 0;
		for (int i = 0; i < prefixo.length(); i++) {
			CaracterPrefixoCodigoProposta caracterPrefixoCodigoProposta = CaracterPrefixoCodigoProposta.valueOf(prefixo.charAt(i));
			totalPrefixo += caracterPrefixoCodigoProposta.getCodigo() * (i + 1);
		}
		return totalPrefixo;
	}

	/**
	 * Calcula o digito verificador do c�digo da proposta
	 * @param valorPrefixo Prefixo do c�digo da proposta
	 * @param sequencial N�mero sequencial da proposta 
	 * @return D�gito verificador da proposta
	 */
	private static int calcularDigitoVerificador(Integer valorPrefixo, String sequencial) {
		StringBuffer resultado = new StringBuffer();
		for (int i = 0; i < sequencial.length(); i++) {
			Integer valor = Integer.parseInt(String.valueOf(sequencial.charAt(i)));
			if (i == 0 || i % 2 == 0) {
				valor *= 2;
			} else {
				valor *= 1;
			}

			resultado.append(valor.toString());
		}

		int valorSomado = 0;
		for (int i = 0; i < resultado.length(); i++) {
			valorSomado += Integer.parseInt(String.valueOf(resultado.charAt(i)));
		}

		int valorDAC = valorPrefixo + valorSomado;
		int restoDV = valorDAC % 10;
		int resultadoDAC = 10 - restoDV;

		if (resultadoDAC == 10) {
			return 0;
		}

		return resultadoDAC;
	}

	/**
	 * Gera um c�digo de proposta considerando o n�mero sequencial criado.
	 * @param numeroSequencial N�mero sequencial criado para a proposta
	 * @return C�digo da proposta 
	 */
	public static String gerar(Long numeroSequencial) {
		String prefixoCodigoProposta = Constantes.PREFIXO_CODIGO_PROPOSTA;
		Integer valorPrefixo = calcularValorPrefixo(prefixoCodigoProposta);

		String sequencial = StringUtils.leftPad(numeroSequencial.toString(), 11, '0');
		int digitoSequencial = calcularDigitoVerificador(valorPrefixo, sequencial);

		StringBuilder codigoProposta = new StringBuilder();
		codigoProposta.append(prefixoCodigoProposta);
		codigoProposta.append(sequencial);
		codigoProposta.append(digitoSequencial);
		return codigoProposta.toString();
	}
}