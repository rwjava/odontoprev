package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;

/**
 * Implementação do acesso a dados do arquivo ADS.
 * 
 * @author WDEV
 */
@Repository
public class ArquivoPropostaDAOImpl extends JdbcDao implements ArquivoPropostaDAO {

	@Autowired
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public Integer buscarSequencialArquivo() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT NARQ_PPSTA_DNTAL FROM DBPROD.NRO_ARQ_PPSTA;");

		return getJdbcTemplate().queryForInt(sql.toString(), new MapSqlParameterSource());
	}

	public void atualizarSequencialArquivo(Integer novoSequencial) {
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE DBPROD.NRO_ARQ_PPSTA SET narq_ppsta_dntal = :novoSequencial");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("novoSequencial", novoSequencial);

		getJdbcTemplate().update(sql.toString(), params);
	}

}
