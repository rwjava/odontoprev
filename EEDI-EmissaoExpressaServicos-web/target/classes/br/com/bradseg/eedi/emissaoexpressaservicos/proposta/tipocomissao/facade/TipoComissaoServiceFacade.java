package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.facade;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;


public interface TipoComissaoServiceFacade {
	
	public Integer obterTipoComissao(PropostaVO proposta);
}
