package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.util;

public enum ProdutoEnum {
	
	EXCLUSIVE_TERMOPROPOSTA(5, 1, "\\EXCLUSIVE\\Adesao.pdf"),
	EXCLUSIVE_ACEITE(5, 2, "\\EXCLUSIVE\\Aceite.pdf"),
	EXCLUSIVE_MANUAL(5, 3, "\\EXCLUSIVE\\Manual.pdf"),
	EXCLUSIVE_COBERTURA(5, 4, "\\EXCLUSIVE\\Manual.pdf"),
	
	EXCLUSIVE_PLUS_TERMOPROPOSTA(6, 1, "\\EXCLUSIVEPLUS\\Adesao.pdf"),
	EXCLUSIVE_PLUS_ACEITE(6, 2, "\\EXCLUSIVEPLUS\\Aceite.pdf"),
	EXCLUSIVE_PLUS_MANUAL(6, 3, "\\EXCLUSIVEPLUS\\Manual.pdf"),
	EXCLUSIVE_PLUS_COBERTURA(6, 4, "\\EXCLUSIVEPLUS\\Manual.pdf"),
			
	PRIME_TERMOPROPOSTA(7, 1, "\\PRIME\\Adesao.pdf"),
	PRIME_ACEITE(7, 2, "\\PRIME\\Aceite.pdf"),
	PRIME_MANUAL(7, 3, "\\PRIME\\Manual.pdf"),
	PRIME_COBERTURA(7, 4, "\\PRIME\\Manual.pdf"),
	
	PRIME_PLUS_TERMOPROPOSTA(8, 1, "\\PRIMEPLUS\\Adesao.pdf"),
	PRIME_PLUS_ACEITE(8, 2, "\\PRIMEPLUS\\Aceite.pdf"),
	PRIME_PLUS_MANUAL(8, 3, "\\PRIMEPLUS\\Manual.pdf"),
	PRIME_PLUS_COBERTURA(8, 4, "\\PRIMEPLUS\\Manual.pdf"),
	
	IDEAL_TERMO(3, 1, "\\IDEAL\\Termo.pdf"),
	IDEAL_ACEITE(3, 2, "\\IDEAL\\Aceite.pdf"),
	IDEAL_MANUAL(3, 3, "\\IDEAL\\Manual.pdf"),
	IDEAL_COBERTURA(3, 4, "\\IDEAL\\Cobertura.pdf"),
	
	DENTAL_JUNIOR_TERMOPROPOSTA(20, 1, "\\DENTAL_JUNIOR\\Adesao.pdf"),
	DENTAL_JUNIOR_ACEITE(20, 2, "\\DENTAL_JUNIOR\\Aceite.pdf"),
	DENTAL_JUNIOR_MANUAL(20, 3, "\\DENTAL_JUNIOR\\Manual.pdf"),
	DENTAL_JUNIOR_COBERTURA(20, 4, "\\DENTAL_JUNIOR\\Manual.pdf"),
	
	DENTAL_JUNIOR_EXCLUSIVE_TERMOPROPOSTA(21, 1, "\\DENTAL_JUNIOR\\Adesao.pdf"),
	DENTAL_JUNIOR_EXCLUSIVE_ACEITE(21, 2, "\\DENTAL_JUNIOR\\Aceite.pdf"),
	DENTAL_JUNIOR_EXCLUSIVE_MANUAL(21, 3, "\\DENTAL_JUNIOR\\Manual.pdf"),
	DENTAL_JUNIOR_EXCLUSIVE_COBERTURA(21, 4, "\\DENTAL_JUNIOR\\Manual.pdf"),
	
	DENTAL_JUNIOR_PRIME_TERMOPROPOSTA(22, 1, "\\DENTAL_JUNIOR\\Adesao.pdf"),
	DENTAL_JUNIOR_PRIME_ACEITE(22, 2, "\\DENTAL_JUNIOR\\Aceite.pdf"),
	DENTAL_JUNIOR_PRIME_MANUAL(22, 3, "\\DENTAL_JUNIOR\\Manual.pdf"),
	DENTAL_JUNIOR_PRIME_COBERTURA(22, 4, "\\DENTAL_JUNIOR\\Manual.pdf"),
	
	DENTAL_DE_LEITE_TERMOPROPOSTA(15, 1, "\\DENTAL_DE_LEITE\\Adesao.pdf"),
	DENTAL_DE_LEITE_ACEITE(15, 2, "\\DENTAL_DE_LEITE\\Aceite.pdf"),
	DENTAL_DE_LEITE_MANUAL(15, 3, "\\DENTAL_DE_LEITE\\Manual.pdf"),
	DENTAL_DE_LEITE_COBERTURA(15, 4, "\\DENTAL_DE_LEITE\\Manual.pdf"),
	
	DENTAL_DE_LEITE_EXCLUSIVE_TERMOPROPOSTA(16, 1, "\\DENTAL_DE_LEITE_EXCLUSIVE\\Adesao.pdf"),
	DENTAL_DE_LEITE_EXCLUSIVE_ACEITE(16, 2, "\\DENTAL_DE_LEITE_EXCLUSIVE\\Aceite.pdf"),
	DENTAL_DE_LEITE_EXCLUSIVE_MANUAL(16, 3, "\\DENTAL_DE_LEITE_EXCLUSIVE\\Manual.pdf"),
	DENTAL_DE_LEITE_EXCLUSIVE_COBERTURA(16, 4, "\\DENTAL_DE_LEITE_EXCLUSIVE\\Manual.pdf"),
	
	DENTAL_DE_LEITE_PRIME_TERMOPROPOSTA(17, 1, "\\DENTAL_DE_LEITE_PRIME\\Adesao.pdf"),
	DENTAL_DE_LEITE_PRIME_ACEITE(17, 2, "\\DENTAL_DE_LEITE_PRIME\\Aceite.pdf"),
	DENTAL_DE_LEITE_PRIME_MANUAL(17, 3, "\\DENTAL_DE_LEITE_PRIME\\Manual.pdf"),
	DENTAL_DE_LEITE_PRIME_COBERTURA(17, 4, "\\DENTAL_DE_LEITE_PRIME\\Manual.pdf");
	
	private Integer codigoPlano;
	private Integer codigoDocumento;
	private String nomeArquivo;
	
	private ProdutoEnum(Integer codigoPlano, Integer codigoDocumento, String nomeArquivo) {
		this.codigoPlano = codigoPlano;
		this.codigoDocumento = codigoDocumento;
		this.nomeArquivo = nomeArquivo;
	}

	
	public static ProdutoEnum obterArquivoPdf(Integer codigoPlano, Integer codigoDocumento){
		if(codigoPlano != null && codigoDocumento != null){
			for (ProdutoEnum produto : ProdutoEnum.values()) {
				if(produto.getCodigoPlano().equals(codigoPlano) && produto.getCodigoDocumento().equals(codigoDocumento)){
					return produto;
				}
			}
		}
		return null;
	}
	
	public Integer getCodigoPlano() {
		return codigoPlano;
	}

	public void setCodigoPlano(Integer codigoPlano) {
		this.codigoPlano = codigoPlano;
	}

	public Integer getCodigoDocumento() {
		return codigoDocumento;
	}

	public void setCodigoDocumento(Integer codigoDocumento) {
		this.codigoDocumento = codigoDocumento;
	}


	public String getNomeArquivo() {
		return nomeArquivo;
	}


	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	
}
