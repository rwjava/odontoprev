package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

/**
 * Descri��o do DAO do arquivo
 * 
 * @author WDEV
 */
public interface ArquivoPropostaDAO {

	/**
	 * Busca o sequencial do arquivo a ser gerado
	 * 
	 * @return sequencial do arquivo
	 */
	public Integer buscarSequencialArquivo();

	/**
	 * Atualiza o sequencial do arquivo em mais um, ap�s ter ocorrido a gera��o do arquivo
	 * 
	 * @param novoSequencial Novo Sequencial
	 */
	public void atualizarSequencialArquivo(Integer novoSequencial);

}
