package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

public class FiltroAcompanhamentoVO implements Serializable{

		private static final long serialVersionUID = -4774860078979211683L;
	
		private FiltroPeriodoVO filtroPeriodoVO;
		private String codigoProposta;
		private String cpfBeneficiarioTitular;
		private Integer sucursal;
		private Integer cpdCorretor;
		private String cpfCnpjCorretor;//cpf  corpo
		private String codigoAgenciaDebito;
		private String codigoAgenciaProdutora;
		private Integer codigoAssistente;
		private Integer codigoGerenteProdutoBVP;
		private Integer codigoStatusProposta;
		private String protocolo;
		
		
		public FiltroAcompanhamentoVO() {
		    	this.filtroPeriodoVO = new FiltroPeriodoVO();
		}
		
		public FiltroPeriodoVO getFiltroPeriodoVO() {
		    return filtroPeriodoVO;
		}
		public void setFiltroPeriodoVO(FiltroPeriodoVO filtroPeriodoVO) {
		    this.filtroPeriodoVO = filtroPeriodoVO;
		}
		public String getCodigoProposta() {
		    return codigoProposta;
		}
		public void setCodigoProposta(String codigoProposta) {
		    this.codigoProposta = codigoProposta;
		}
		public String getCpfBeneficiarioTitular() {
		    return cpfBeneficiarioTitular;
		}
		public void setCpfBeneficiarioTitular(String cpfBeneficiarioTitular) {
		    this.cpfBeneficiarioTitular = cpfBeneficiarioTitular;
		}
		public Integer getSucursal() {
		    return sucursal;
		}
		public void setSucursal(Integer sucursal) {
		    this.sucursal = sucursal;
		}
		public Integer getCpdCorretor() {
		    return cpdCorretor;
		}
		public void setCpdCorretor(Integer cpdCorretor) {
		    this.cpdCorretor = cpdCorretor;
		}
		public String getCpfCnpjCorretor() {
		    return cpfCnpjCorretor;
		}
		public void setCpfCnpjCorretor(String cpfCnpjCorretor) {
		    this.cpfCnpjCorretor = cpfCnpjCorretor;
		}
		public String getCodigoAgenciaDebito() {
		    return codigoAgenciaDebito;
		}
		public void setCodigoAgenciaDebito(String codigoAgenciaDebito) {
		    this.codigoAgenciaDebito = codigoAgenciaDebito;
		}
		public String getCodigoAgenciaProdutora() {
		    return codigoAgenciaProdutora;
		}
		public void setCodigoAgenciaProdutora(String codigoAgenciaProdutora) {
		    this.codigoAgenciaProdutora = codigoAgenciaProdutora;
		}
		public Integer getCodigoAssistente() {
		    return codigoAssistente;
		}
		public void setCodigoAssistente(Integer codigoAssistente) {
		    this.codigoAssistente = codigoAssistente;
		}
		public Integer getCodigoGerenteProdutoBVP() {
		    return codigoGerenteProdutoBVP;
		}
		public void setCodigoGerenteProdutoBVP(Integer codigoGerenteProdutoBVP) {
		    this.codigoGerenteProdutoBVP = codigoGerenteProdutoBVP;
		}
		public Integer getCodigoStatusProposta() {
		    return codigoStatusProposta;
		}
		public void setCodigoStatusProposta(Integer codigoStatusProposta) {
		    this.codigoStatusProposta = codigoStatusProposta;
		}

		public String getProtocolo() {
			return protocolo;
		}

		public void setProtocolo(String protocolo) {
			this.protocolo = protocolo;
		}

}
