package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.DefinicaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AngariadorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorMasterVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.GerenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PessoaProdutor;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoPessoaProdutor;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoProdutor;

/**
 * Implementa��o do acesso a dados do produtor
 * 
 * @author WDEV
 */
@Repository
public class ProdutorDAOImpl extends JdbcDao implements ProdutorDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProdutorDAOImpl.class);

	@Autowired
	private DataSource dataSource;

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	public List<ProdutorVO> listarPorProposta(PropostaVO proposta) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		sql.append("PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL, ");
		sql.append("PROTR_PPSTA_DNTAL.CCPD_PROTR, ");
		sql.append("PROTR_PPSTA_DNTAL.CTPO_PROTR_PPSTA, ");
		sql.append("PROTR_PPSTA_DNTAL.IPROTR_PPSTA_DNTAL, ");
		sql.append("PROTR_PPSTA_DNTAL.NCPF_CNPJ_PROTR ");
		sql.append("FROM ");
		sql.append("DBPROD.PROTR_PPSTA_DNTAL PROTR_PPSTA_DNTAL ");
		sql.append("WHERE ");
		sql.append("PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL = :sequencialProposta ");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequencialProposta", proposta.getNumeroSequencial());

		return getJdbcTemplate().query(sql.toString(), params, new ProdutorRowMapper());
	}

	/**
	 * Mapemanto de dados da lista do ProdutorVO
	 * @author WDEV
	 */
	private static final class ProdutorRowMapper implements RowMapper<ProdutorVO> {
		public ProdutorVO mapRow(ResultSet es, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(es);

			ProdutorVO produtor;
			switch (TipoProdutor.obterPorCodigo(resultSet.getInteger("CTPO_PROTR_PPSTA"))) {
			case ANGARIADOR:
				produtor = new AngariadorVO();
				break;
			case CORRETOR_MASTER:
				produtor = new CorretorMasterVO();
				break;
			case GERENTE:
				produtor = new GerenteVO();
				break;
			default:
				produtor = new CorretorVO();
				break;
			}

			produtor.setProposta(new PropostaVO());
			produtor.getProposta().setNumeroSequencial(resultSet.getLong("NSEQ_PPSTA_DNTAL"));
			produtor.setCpd(resultSet.getInteger("CCPD_PROTR"));
			produtor.setNome(Strings.maiusculas(resultSet.getString("IPROTR_PPSTA_DNTAL")));
			produtor.setCpfCnpj(resultSet.getString("NCPF_CNPJ_PROTR"));
			return produtor;
		}
	}

	public void salvar(ProdutorVO produtor) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO DBPROD.PROTR_PPSTA_DNTAL (NSEQ_PPSTA_DNTAL, CCPD_PROTR, CTPO_PROTR_PPSTA, IPROTR_PPSTA_DNTAL, NCPF_CNPJ_PROTR)");
		sql.append(" VALUES (:proposta, :cpd, :tipo, :nomeCorretor, :cpfCnpjCorretor)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("proposta", produtor.getProposta().getNumeroSequencial());
		params.addValue("cpd", produtor.getCpd());
		params.addValue("tipo", produtor.getTipo().getCodigo());
		params.addValue("nomeCorretor", Strings.maiusculas(produtor.getNome()));
		params.addValue("cpfCnpjCorretor", produtor.getCpfCnpj());

		getJdbcTemplate().update(sql.toString(), params);
	}

	public void removerPorProposta(DefinicaoProposta definicaoProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM DBPROD.PROTR_PPSTA_DNTAL WHERE NSEQ_PPSTA_DNTAL = :proposta");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("proposta", definicaoProposta.getSequencial());

		getJdbcTemplate().update(sql.toString(), params);
	}

	public Integer buscarCodigoPessoaPorCpd(Integer numeroProdutor) {
		StringBuilder sql = new StringBuilder();
		// TODO verificar se o filtro � pelo PROTR_CRRTG.NPROTR_CRRTG ou PROTR_CRRTG.CCPD_CRRTR, pois deveria ser o pelo CPD ja que existe na base
		sql.append("SELECT PROTR_CRRTG.CPSSOA_PROTR_SEGUR FROM DBPROD.PROTR_CRRTG PROTR_CRRTG WHERE PROTR_CRRTG.NPROTR_CRRTG = :numeroProdutor");

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("numeroProdutor", numeroProdutor);

		try {
			return getJdbcTemplate().queryForInt(sql.toString(), param);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma pessoa encontrada para o numero do produtor informado", e);
			return null;
		}

	}

	public PessoaProdutor buscarInformacoesCorretorPorCodigo(Integer codigoPessoa) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" PSSOA_PROTR.CPSSOA_PROTR_SEGUR");
		sql.append(" PSSOA_PROTR.IPSSOA,");
		sql.append(" PSSOA_PROTR.CTPO_PSSOA,");
		sql.append(" FROM DBPROD.PSSOA_PROTR PSSOA_PROTR");
		sql.append(" WHERE PSSOA_PROTR.CPSSOA_PROTR_SEGUR = :codigoPessoa");

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPessoa", codigoPessoa);

		try {
			return getJdbcTemplate().queryForObject(sql.toString(), param, new PessoaProdutorRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma informa��o encontrada para o c�digo da pessoa informado", e);
			return null;
		}
	}

	/**
	 * Mapemanto de dados da lista da Pessoa Corretor
	 * @author WDEV
	 */
	private static final class PessoaProdutorRowMapper implements RowMapper<PessoaProdutor> {
		public PessoaProdutor mapRow(ResultSet es, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(es);

			PessoaProdutor pessoaProdutor = new PessoaProdutor();
			pessoaProdutor.setTipoPessoaProdutor(TipoPessoaProdutor.buscaPor(resultSet.getString("CTPO_PSSOA")));
			return pessoaProdutor;
		}
	}

	public Long buscarCpfPorCodigoPessoa(Integer codigoPessoa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT PSSOA_FIS_PROTR.CNRO_CPF FROM DBPROD.PSSOA_FIS_PROTR PSSOA_FIS_PROTR WHERE PSSOA_FIS_PROTR.CPSSOA_PROTR_SEGUR = :codigoPessoa");

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPessoa", codigoPessoa);

		try {
			return getJdbcTemplate().queryForLong(sql.toString(), param);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhum cpf encontrado para o c�digo da pessoa informado", e);
			return null;
		}

	}

	public Long buscarCnpjPorCodigoPessoa(Integer codigoPessoa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT PSSOA_JURID_PROTR.CNRO_CNPJ FROM DBPROD.PSSOA_JURID_PROTR PSSOA_JURID_PROTR WHERE PSSOA_JURID_PROTR.CPSSOA_PROTR_SEGUR = :codigoPessoa");

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoPessoa", codigoPessoa);

		try {
			return getJdbcTemplate().queryForLong(sql.toString(), param);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhum cnpj encontrado para o c�digo da pessoa informado", e);
			return null;
		}
	}

	public Integer buscarCpdCorretorPorNumeroProdutor(Integer numeroProdutor) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT PROTR_CRRTG.CCPD_CRRTR FROM DBPROD.PROTR_CRRTG PROTR_CRRTG WHERE PROTR_CRRTG.NPROTR_CRRTG = :numeroProdutor");

		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("numeroProdutor", numeroProdutor);

		try {
			return getJdbcTemplate().queryForInt(sql.toString(), param);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhum cpd encontrado para o n�mero do produtor informado", e);
			return null;
		}
	}

	public List<ProdutorVO> listarPorSequenciaisProposta(Set<Long> sequenciaisProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT NSEQ_PPSTA_DNTAL, CCPD_PROTR, CTPO_PROTR_PPSTA, IPROTR_PPSTA_DNTAL, NCPF_CNPJ_PROTR ");
		sql.append("FROM DBPROD.PROTR_PPSTA_DNTAL PROTR_PPSTA_DNTAL ");
		sql.append("WHERE PROTR_PPSTA_DNTAL.NSEQ_PPSTA_DNTAL in (:sequenciaisProposta)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequenciaisProposta", sequenciaisProposta);

		return getJdbcTemplate().query(sql.toString(), params, new ProdutorRowMapper());
	}

}