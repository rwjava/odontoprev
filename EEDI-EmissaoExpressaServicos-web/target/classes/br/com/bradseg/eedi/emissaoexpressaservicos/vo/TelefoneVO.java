package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Dados do TelefoneVO
 * 
 * @author WDEV
 */
public class TelefoneVO implements Serializable {

	private static final long serialVersionUID = -2064453652961853243L;

	private Integer codigo;

	private String ddd;

	private String numero;

	private Integer ramal;

	private TipoTelefone tipo;

	private ProponenteVO proponente;

	private ResponsavelLegalVO responsavelLegal;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getRamal() {
		return ramal;
	}

	public void setRamal(Integer ramal) {
		this.ramal = ramal;
	}

	public TipoTelefone getTipo() {
		return tipo;
	}

	public void setTipo(TipoTelefone tipo) {
		this.tipo = tipo;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public ProponenteVO getProponente() {
		return proponente;
	}

	public void setProponente(ProponenteVO proponente) {
		this.proponente = proponente;
	}

	public ResponsavelLegalVO getResponsavelLegal() {
		return responsavelLegal;
	}

	public void setResponsavelLegal(ResponsavelLegalVO responsavelLegal) {
		this.responsavelLegal = responsavelLegal;
	}

	@Override
	public String toString() {
		return "TelefoneVO [codigo=" + codigo + ", ddd=" + ddd + ", numero=" + numero + ", ramal=" + ramal + ", tipo="
				+ tipo + "]";
	}
	
	

}