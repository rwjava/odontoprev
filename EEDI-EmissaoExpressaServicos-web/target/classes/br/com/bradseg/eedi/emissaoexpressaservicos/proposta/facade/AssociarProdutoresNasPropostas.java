package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;
import java.util.Set;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.ProdutorDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Associação de produtores nas propostas
 * @author WDEV
 */
@Scope("prototype")
@Named("AssociarProdutoresNasPropostas")
public class AssociarProdutoresNasPropostas extends AssociacaoEntidade<PropostaVO, Long, ProdutorVO, PropostaVO> {

	@Autowired
	private ProdutorDAO produtorDAO;

	@Override
	public Long extrairIdentificadorEntidadesRelacionadas(PropostaVO proposta) {
		if (proposta == null) {
			return null;
		}
		return proposta.getNumeroSequencial();
	}

	@Override
	public List<ProdutorVO> consultarEntidadesRelacionada(Set<Long> idsRelacionados) {
		return produtorDAO.listarPorSequenciaisProposta(idsRelacionados);
	}

	@Override
	public Long extrairIdentificadorEntidadePai(ProdutorVO produtor) {
		if (produtor == null || produtor.getProposta() == null) {
			return null;
		}
		return produtor.getProposta().getNumeroSequencial();
	}

	@Override
	public PropostaVO associarEntidade(PropostaVO proposta, ProdutorVO produtor) {
		proposta.setProdutor(produtor);
		return proposta;
	}

}