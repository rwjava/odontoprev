package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.PlanoDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.RelatorioAcompanhamentoDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.Report;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ReportFile;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Arquivo;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AcompanhamentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ItemRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.RelatorioAcompanhamentoVO;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Servi�os para listar propostas e gerar relat�rios de acompanhamento
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class RelatorioAcompanhamentoServiceFacadeImpl implements RelatorioAcompanhamentoServiceFacade {

	@Autowired
	private RelatorioAcompanhamentoDAO relatorioAcompanhamentoDAO;

	@Autowired
	private AssociarProdutoresNasPropostas associarProdutoresNasPropostas;
	
	@Autowired
	private RelatorioAcompanhamentoEEDIServiceFacadeImpl relatorioAcompanhamentoEEDIServiceFacade;
	
	@Autowired
	private PlanoDAO planoDao;

	public RelatorioAcompanhamentoVO listarPropostasPorFiltro(FiltroRelatorioAcompanhamentoVO filtro, LoginVO login) {
		new RelatorioAcompanhamentoValidator().validate(filtro);

		// Consulta as propostas por filtro
		List<ItemRelatorioAcompanhamentoVO> itensRelatorioAcompanhamento = relatorioAcompanhamentoDAO.listarPropostasPorFiltro(filtro, login);

		if (itensRelatorioAcompanhamento.isEmpty()) {
			throw new BusinessException(new Message("msg.erro.nenhuma.proposta.encontrada.para.filtro.informado", Message.ERROR_TYPE));
		}

		
		for(ItemRelatorioAcompanhamentoVO item :  itensRelatorioAcompanhamento){	
			item.getProposta().setPlanoVO(planoDao.obterPlanoPorDataEmissaoProposta(item.getProposta()));
		}
		
		// Associa os produtores na proposta
		List<PropostaVO> propostas = Lists.newArrayList(Iterables.transform(itensRelatorioAcompanhamento, new Function<ItemRelatorioAcompanhamentoVO, PropostaVO>() {
			public PropostaVO apply(ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento) {
				return itemRelatorioAcompanhamento.getProposta();
			}
		}));
		associarProdutoresNasPropostas.associar(propostas);


		
		RelatorioAcompanhamentoVO relatorioAcompanhamento = new RelatorioAcompanhamentoVO();
		relatorioAcompanhamento.setFiltroRelatorioAcompanhamento(filtro);
		relatorioAcompanhamento.setItensRelatorioAcompanhamento(itensRelatorioAcompanhamento);

		
		
		
		return relatorioAcompanhamento;
	}

	public ReportFile gerarPDF(FiltroRelatorioAcompanhamentoVO filtro, LoginVO login) {
		
		RelatorioAcompanhamentoVO relatorioAcompanhamento = listarPropostasPorFiltro(filtro, login);

		ReportFile reportFile = new ReportFile();
		reportFile.setFileName(getNomeRelatorio());
		reportFile.adicionarRelatorio(new Report("relatorio_acompanhamento.jrxml", relatorioAcompanhamento));
		return reportFile;
	}
	
	public ReportFile gerarPDFRelatorioAcompanhamento(FiltroAcompanhamentoVO filtro, LoginVO login){
		
		
		List<AcompanhamentoPropostaVO>  listaDePropostas = relatorioAcompanhamentoEEDIServiceFacade.listarPropostasAcompanhamento(filtro, login, false);

		ReportFile reportFile = new ReportFile();
		reportFile.setFileName(getNomeRelatorio());
		reportFile.adicionarRelatorio(new Report("rela_acompanhamento.jrxml", listaDePropostas));
		return reportFile;
	}

	public Arquivo gerarCSV(FiltroRelatorioAcompanhamentoVO filtro, LoginVO login) {
		RelatorioAcompanhamentoVO relatorioAcompanhamento = listarPropostasPorFiltro(filtro, login);

		GeradorArquivoRelatorioAcompanhamento gerador = new GeradorArquivoRelatorioAcompanhamento(relatorioAcompanhamento.getItensRelatorioAcompanhamento());
		return gerador.gerar(getNomeRelatorio() + ".csv");
	}

	private String getNomeRelatorio() {
		return new StringBuilder().append(Constantes.NOME_RELATORIO_ACOMPANHAMENTO).append(new LocalDate().toString("ddMMyyyy")).toString();
	}
}