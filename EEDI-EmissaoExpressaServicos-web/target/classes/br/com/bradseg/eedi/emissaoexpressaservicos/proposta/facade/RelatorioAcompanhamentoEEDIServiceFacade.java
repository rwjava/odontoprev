package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AcompanhamentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.RelatorioAcompanhamentoPropostaVO;

public interface RelatorioAcompanhamentoEEDIServiceFacade {

	public List<AcompanhamentoPropostaVO>  listarPropostasAcompanhamento(FiltroAcompanhamentoVO filtro,  LoginVO login, boolean isIntranet);
}
