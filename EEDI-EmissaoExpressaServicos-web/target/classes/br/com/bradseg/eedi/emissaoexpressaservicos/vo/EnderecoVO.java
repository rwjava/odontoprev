package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * Dados do endere�o
 * 
 * @author WDEV
 */
public class EnderecoVO implements Serializable {

	private static final long serialVersionUID = 1561713592845078797L;

	private Long codigo;

	private String logradouro;

	private Long numero;

	private String complemento;

	private String bairro;

	private String cidade;

	private UnidadeFederativaVO unidadeFederativa;

	private String cep;

	private String codigoUfIbge;

	private String siglaIbgeMunicipio;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public UnidadeFederativaVO getUnidadeFederativa() {
		return unidadeFederativa;
	}

	public void setUnidadeFederativa(UnidadeFederativaVO unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCodigoUfIbge() {
		return codigoUfIbge;
	}

	public void setCodigoUfIbge(String codigoUfIbge) {
		this.codigoUfIbge = codigoUfIbge;
	}

	public String getSiglaIbgeMunicipio() {
		return siglaIbgeMunicipio;
	}

	public void setSiglaIbgeMunicipio(String siglaIbgeMunicipio) {
		this.siglaIbgeMunicipio = siglaIbgeMunicipio;
	}

	public boolean isUnidadeFederativaPreenchida() {
		return unidadeFederativa != null && StringUtils.isNotBlank(unidadeFederativa.getSigla());
	}

	@Override
	public String toString() {
		return "EnderecoVO [codigo=" + codigo + ", logradouro=" + logradouro + ", numero=" + numero + ", complemento="
				+ complemento + ", bairro=" + bairro + ", cidade=" + cidade + ", unidadeFederativa="
				+ unidadeFederativa + ", cep=" + cep + ", codigoUfIbge=" + codigoUfIbge + ", siglaIbgeMunicipio="
				+ siglaIbgeMunicipio + "]";
	}

	
}
