package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * Utilitário para associar uma determinada entidade pai com suas entidades filhas
 * 
 * @author WDEV
 *
 * @param <P> Entidade Pai (base)
 * @param <ID> Tipo do identificador comum que relacione o pai com os filhos
 * @param <F> Entidade Filha
 * @param <R> Resultado da associação
 */
public abstract class AssociacaoEntidade<P, ID, F, R> {
	protected abstract ID extrairIdentificadorEntidadesRelacionadas(P entidade);

	protected abstract List<F> consultarEntidadesRelacionada(Set<ID> idsRelacionados);

	protected abstract ID extrairIdentificadorEntidadePai(F entidadeRelacionada);

	protected abstract R associarEntidade(P entidade, F entidadeRelacionada);

	public void associar(List<P> entidades) {
		// Extrair os ids relacionados
		Multimap<ID, P> entidadesPorId = ArrayListMultimap.create();
		for (P pai : entidades) {
			ID id = extrairIdentificadorEntidadesRelacionadas(pai);
			entidadesPorId.put(id, pai);
		}

		// Consultar as entidades relacionadas
		List<F> entidadesRelacionadas = consultarEntidadesRelacionada(entidadesPorId.keySet());
		Multimap<ID, F> entidadesRelacionadasPorId = ArrayListMultimap.create();
		for (F filho : entidadesRelacionadas) {
			ID id = extrairIdentificadorEntidadePai(filho);
			entidadesRelacionadasPorId.put(id, filho);
		}

		for (Map.Entry<ID, P> paiEntry : entidadesPorId.entries()) {
			P pai = paiEntry.getValue();
			Collection<F> filhos = entidadesRelacionadasPorId.get(paiEntry.getKey());
			for (F filho : filhos) {
				associarEntidade(pai, filho);
			}
		}
	}

}