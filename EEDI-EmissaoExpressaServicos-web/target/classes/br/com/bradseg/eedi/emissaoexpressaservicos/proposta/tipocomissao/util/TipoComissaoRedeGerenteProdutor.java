package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.util;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca;

public class TipoComissaoRedeGerenteProdutor {
		
		public Integer obterTipoComissao(Integer tipoCobranca) {
			
			Integer tipoComissao = null;
			if (TipoCobranca.MENSAL.getCodigo().equals(tipoCobranca)) {
				tipoComissao = 3;
			} else if (TipoCobranca.ANUAL.getCodigo().equals(tipoCobranca)) {
				tipoComissao = 6;
			}
			return tipoComissao;
			
		}
}
