package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.RetornoMovimentoPropostaVO;

/**
 * Implementação de acesso a dados do retorno do movimento da proposta
 * 
 * @author WDEV
 *
 */
@Repository
public class RetornoMovimentoPropostaDAOImpl extends JdbcDao implements RetornoMovimentoPropostaDAO {

	@Autowired
	private DataSource dataSource;

	private static final Logger LOGGER = LoggerFactory.getLogger(RetornoMovimentoPropostaDAOImpl.class);
	public DataSource getDataSource() {
		return dataSource;
	}

	public void salvar(RetornoMovimentoPropostaVO retornoMovimentoProposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT");
		sql.append(" INTO");
		sql.append(" DBPROD.RETOR_MOVTO_PPSTA");
		sql.append(" (NSEQ_PPSTA_DNTAL,");
		sql.append(" CSIT_DNTAL_INDVD,");
		sql.append(" DINIC_MOVTO_PPSTA,");
		sql.append(" CRETOR_MOVTO_PPSTA,");
		sql.append(" RRETOR_MOVTO_PPSTA)");
		sql.append(" VALUES");
		sql.append(" (:sequencialProposta,");
		sql.append(" :codigoSituacao,");
		sql.append(" :dataInicio,");
		sql.append(" :codigoRetorno,");
		sql.append(" :descricaoMovimento)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("sequencialProposta", retornoMovimentoProposta.getMovimentoProposta().getProposta().getNumeroSequencial());
		params.addValue("codigoSituacao", retornoMovimentoProposta.getMovimentoProposta().getSituacao().getCodigo());
		params.addValue("codigoRetorno", retornoMovimentoProposta.getCodigoRetornoMovimento());
		params.addValue("dataInicio", retornoMovimentoProposta.getMovimentoProposta().getDataInicio().toDate());
		params.addValue("descricaoMovimento", Strings.maiusculas(retornoMovimentoProposta.getDescricaoRetornoMovimento()));

		LOGGER.error("INFO: SALVAR RETORNO MOVIMENTO: "+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);

	}
}
