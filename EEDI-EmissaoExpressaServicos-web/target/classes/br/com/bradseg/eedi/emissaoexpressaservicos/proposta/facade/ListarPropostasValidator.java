package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import org.joda.time.Days;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.Validador;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroPropostaVO;

/**
 * Realiza valida��es no filtro de pesquisa de proposta
 * @author WDEV
 */
public class ListarPropostasValidator {

	/**
	 * Valida o filtro de listagem de proposta, segundo as seguintes regras:
	 * 
	 * <ul>
	 * 	<li>
	 * 		Se n�o for preenchido o c�digo da proposta:
	 * 		<ul>
	 * 			<li>� obrigat�rio informar a situa��o.</li>
	 * 			<li>� obrigat�rio informar o CPD/Sucursal do corretor.</li>
	 * 			<li>� obrigat�rio informar a data in�cio e data fim do per�odo.</li>
	 * 		</ul>
	 * 	</li>
	 * 	<li>
	 * 		Se o per�odo for preenchido (data �nicio e data fim), valida se:
	 * 		<ul>
	 * 			<li>N�o possui mais de 30 dias de intervalo.</li>
	 * 			<li>Data in�cio est� antes da data fim.</li>
	 * 		</ul>
	 * 	</li>
	 * 	<li>Se a sucursal for preenchida, deve ser v�lida (ser Emissora ou BVP {@link br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO#isValida()})</li>
	 * 	<li>Se o nome for preenchido, deve ser v�lido ({@link br.com.bradseg.eedi.emissaoexpressaservicos.util.Validador#nomeCompleto(String, String)}).</li>
	 * </ul>
	 * 
	 * @param filtroProposta Filtro de pesquisa de propostas
	 */
	@SuppressWarnings("unused")
	public void validate(FiltroPropostaVO filtroProposta) {
		Validador validador = new Validador();

		if (!filtroProposta.isCodigoPropostaPreenchido() && !filtroProposta.isCpfProponentePreenchido()) {
			boolean sucursalCPDCorretorPreenchido = validador.obrigatorio(filtroProposta.getCorretor(), "CPD/Sucursal") && validador.obrigatorio(filtroProposta.getCorretor().getSucursalSeguradora(), "CPD/Sucursal");
			validador.obrigatorio(filtroProposta.getSituacaoProposta(), "Situa��o da proposta");
			validador.obrigatorio(filtroProposta.getDataInicio(), "Data In�cio do Per�odo");
			validador.obrigatorio(filtroProposta.getDataFim(), "Data Fim do Per�odo");
		}

		if (filtroProposta.isDataInicioPreenchida() && filtroProposta.isDataFimPreenchida()) {
			Days intervaloData = Days.daysBetween(filtroProposta.getDataInicio(), filtroProposta.getDataFim());
			validador.falso(intervaloData.isGreaterThan(Days.days(30)), "msg.erro.lista.proposta.periodo.maior.que.trinta.dias");
			validador.falso(filtroProposta.getDataFim().isBefore(filtroProposta.getDataInicio()), "msg.erro.lista.proposta.dataFim.menorQue.DataInicio");
		}

//		if (filtroProposta.isSucursalCorretorPreenchida()) {
//			validador.verdadeiro(filtroProposta.getCorretor().getSucursalSeguradora().isValida(), "msg.erro.lista.proposta.sucursal.invalida");
//		}

		if (filtroProposta.isNomeProponentePreenchido()) {
			validador.nome(filtroProposta.getNomeProponente(), "Nome");
		}

		if (filtroProposta.isCpfProponentePreenchido()) {
			validador.cpf(filtroProposta.getCpfProponente(), "CPF do proponente");
			//validador.obrigatorio(filtroProposta.getDataInicio(), "Data In�cio do Per�odo");
			//validador.obrigatorio(filtroProposta.getDataFim(), "Data Fim do Per�odo");
			//boolean canalVendaPreenchido = validador.obrigatorio(filtroProposta.getCanalVenda(), "Canal de venda") && validador.obrigatorio(filtroProposta.getCanalVenda().getCodigo(), "Canal de venda");
		}

		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}
	}
}