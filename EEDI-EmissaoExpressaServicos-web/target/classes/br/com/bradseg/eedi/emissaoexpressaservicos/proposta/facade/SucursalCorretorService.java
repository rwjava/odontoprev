package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

public interface SucursalCorretorService {

	/**
	 * Valida sucursal do corretor
	 * 
	 * @param codigo C�digo da sucursal
	 * @return True se for v�lida
	 */
	public boolean validarSucursalCorretor(Integer codigo);

	/**
	 * Valida surcursal mercado
	 * 
	 * @param codigo C�digo da sucursal
	 * @return True se for v�lida
	 */
	public boolean validarSucursalMercado(Integer codigo);

	/**
	 * Valida sucursal rede
	 * 
	 * @param codigo C�digo da sucursal
	 * @return True se for v�lida
	 */
	public boolean validarSucursalRede(Integer codigo);

	/**
	 * Valida sucursal corporate
	 * 
	 * @param codigo C�digo da sucursal
	 * @return True se for v�lida
	 */
	public boolean validarSucursalCorporate(Integer codigo);
}
