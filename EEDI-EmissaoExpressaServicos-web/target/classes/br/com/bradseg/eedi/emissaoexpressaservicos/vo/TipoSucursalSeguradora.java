package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.util.ArrayList;
import java.util.List;


/**
 * Tipo da sucursal
 * 
 * @author WDEV
 */
public enum TipoSucursalSeguradora {
	
	//BVP, EMISSORA, CORPORATE;
	//MERCADO(3, "Mercado"), REDE(2, "REDE"), CORPORATE(4, "Corporate"),
	
	 BVP(2, "BVP"), EMISSORA(3, "EMISSORA"), CORPORATE(4, "Corporate") ;

	private Integer codigo;
	private String nome;

	private TipoSucursalSeguradora(Integer codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoSucursalSeguradora obterTipoSucursalSeguradora(Integer codigo) {
		for (TipoSucursalSeguradora tipoSeguradora : TipoSucursalSeguradora.values()) {
			if (codigo.equals(tipoSeguradora.getCodigo())) {
				return tipoSeguradora;
			}
		}
		return null;
	}

	public static List<LabelValueVO> obterLista() {
		List<LabelValueVO> lista = new ArrayList<LabelValueVO>();
		for (TipoSucursalSeguradora tipoSeguradora : TipoSucursalSeguradora.values()) {
			lista.add(new LabelValueVO(String.valueOf(tipoSeguradora.getCodigo()), tipoSeguradora.getNome()));
		}
		return lista;
	}

}