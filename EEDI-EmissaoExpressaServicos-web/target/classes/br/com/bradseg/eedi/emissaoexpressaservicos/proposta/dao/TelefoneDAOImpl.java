package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TelefoneVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoTelefone;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UsuarioUltimaAtualizacao;

import com.google.common.collect.Sets;

/**
 * Implementação do acesso a dados do telefone
 * 
 * @author WDEV
 */
@Repository
public class TelefoneDAOImpl extends JdbcDao implements TelefoneDAO {

	@Autowired
	private DataSource datasource;

	private static final Logger LOGGER = LoggerFactory.getLogger(TelefoneDAOImpl.class);
	
	public List<TelefoneVO> listarPorProponente(ProponenteVO proponente) {
		return listarPorProponentes(Sets.newHashSet(proponente.getCodigo()));
	}

	public List<TelefoneVO> listarPorProponentes(Set<Long> codigoProponente) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" FONE_PROPN_DNTAL.CPROPN_DNTAL_INDVD,");
		sql.append(" FONE_PROPN_DNTAL.CFONE_PROPN_DNTAL,");
		sql.append(" FONE_PROPN_DNTAL.NDDD_FONE,");
		sql.append(" FONE_PROPN_DNTAL.NFONE,");
		sql.append(" FONE_PROPN_DNTAL.NRMAL_FONE,");
		sql.append(" FONE_PROPN_DNTAL.CTPO_FONE");
		sql.append(" FROM ");
		sql.append(" DBPROD.FONE_PROPN_DNTAL FONE_PROPN_DNTAL");
		sql.append(" WHERE ");
		sql.append(" FONE_PROPN_DNTAL.CPROPN_DNTAL_INDVD in (:codigoProponente)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigoProponente", codigoProponente);
		LOGGER.error("INFO: LISTAR POR PROPONENTE: "+sql.toString()+" PARÂMETROS: "+params.getValues());
		return getJdbcTemplate().query(sql.toString(), params, new TelefoneRowMapper());
	}

	/**
	 * Mapemanto de dados da lista de TelefoneVO
	 * @author WDEV
	 */
	private static final class TelefoneRowMapper implements RowMapper<TelefoneVO> {
		public TelefoneVO mapRow(ResultSet es, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(es);
			TelefoneVO telefone = new TelefoneVO();
			telefone.setCodigo(resultSet.getInteger("CFONE_PROPN_DNTAL"));
			telefone.setDdd(resultSet.getString("NDDD_FONE"));
			telefone.setNumero(resultSet.getString("NFONE"));
			telefone.setRamal(resultSet.getInteger("NRMAL_FONE"));
			telefone.setTipo(TipoTelefone.buscaPorCodigo(resultSet.getInteger("CTPO_FONE")));

			telefone.setProponente(new ProponenteVO());
			telefone.getProponente().setCodigo(resultSet.getLong("CPROPN_DNTAL_INDVD"));

			return telefone;
		}
	}

	public void salvar(TelefoneVO telefone) {
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO DBPROD.FONE_PROPN_DNTAL (CPROPN_DNTAL_INDVD, CFONE_PROPN_DNTAL, NDDD_FONE, NFONE, NRMAL_FONE, CTPO_FONE, DULT_ATULZ_REG, CRESP_ULT_ATULZ)");
		sql.append(" VALUES (:proponente, :tipo, :ddd, :numero, :ramal, :tipo, CURRENT_TIMESTAMP, :usuarioUltimaAtualizacao)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("proponente", telefone.getProponente().getCodigo());
		params.addValue("ddd", telefone.getDdd());
		params.addValue("numero", telefone.getNumero());
		params.addValue("ramal", telefone.getRamal());
		params.addValue("tipo", telefone.getTipo().getCodigo());
		params.addValue("usuarioUltimaAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());
		LOGGER.error("INFO: SALVAR TELEFONE: "+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public DataSource getDataSource() {
		return datasource;
	}

	public void salvarTelefoneResponsavelLegal(TelefoneVO telefone) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO DBPROD.FONE_RESP_LEGAL (CRESP_LEGAL_DNTAL, CFONE_RESP_LEGAL, NDDD_FONE, NFONE, NRMAL_FONE, CTPO_FONE, DULT_ATULZ_REG, CRESP_ULT_ATULZ)");
		sql.append("VALUES (:responsavelLegal, :tipo, :ddd, :numero, :ramal, :tipo, CURRENT_TIMESTAMP, :usuarioUltimaAtualizacao)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("responsavelLegal", telefone.getResponsavelLegal().getCodigo());
		params.addValue("ddd", telefone.getDdd());
		params.addValue("numero", telefone.getNumero());
		params.addValue("ramal", telefone.getRamal());
		params.addValue("tipo", telefone.getTipo().getCodigo());
		params.addValue("usuarioUltimaAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());
		LOGGER.error("INFO: SALVAR TELEFONE RESPONSÁVEL TITULAR: "+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public void removerPorProponente(ProponenteVO proponente) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM DBPROD.FONE_PROPN_DNTAL WHERE CPROPN_DNTAL_INDVD = :proponente");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("proponente", proponente.getCodigo());
		LOGGER.error("INFO: REMOVER TELEFONE: "+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public void removerTelefoneResponsavelLegal(ResponsavelLegalVO responsavelLegal) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM DBPROD.FONE_RESP_LEGAL WHERE CRESP_LEGAL_DNTAL = :responsavelLegal");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("responsavelLegal", responsavelLegal.getCodigo());
		LOGGER.error("INFO: REMOVER TELEFONE RESPONSÁVEL TITULAR: "+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}
}