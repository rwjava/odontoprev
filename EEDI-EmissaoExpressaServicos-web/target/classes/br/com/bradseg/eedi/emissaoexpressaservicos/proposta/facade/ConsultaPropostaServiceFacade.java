package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

public interface ConsultaPropostaServiceFacade {

	public List<List<String>> realizarConsulta(String consulta);
}
