package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoBeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Serviços de acesso a dados do movimento do beneficiário
 * 
 * @author WDEV
 */
public interface MovimentoBeneficiarioDAO {

	/**
	 * Salva um novo movimento do beneficiário
	 * 
	 * @param movimentoBeneficiario Movimento a ser salvo no sistema
	 */
	public void salvar(MovimentoBeneficiarioVO movimentoBeneficiario);

	/**
	 * Remove os movimentos do beneficiário de uma proposta
	 * 
	 * @param proposta Proposta
	 */
	public void removerPorProposta(PropostaVO proposta);

}