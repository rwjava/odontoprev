package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.TelefoneDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TelefoneVO;

/**
 * Associação de produtores nas propostas
 * @author WDEV
 */
@Scope("prototype")
@Named("AssociarTelefonesProponenteNasPropostas")
public class AssociarTelefonesProponenteNasPropostas extends AssociacaoEntidade<PropostaVO, Long, TelefoneVO, PropostaVO> {
	@Autowired
	private TelefoneDAO telefoneDAO;

	@Override
	protected Long extrairIdentificadorEntidadesRelacionadas(PropostaVO proposta) {
		if (proposta.getProponente() == null) {
			return null;
		}

		return proposta.getProponente().getCodigo();
	}

	@Override
	protected PropostaVO associarEntidade(PropostaVO proposta, TelefoneVO telefone) {
		proposta.getProponente().setTelefones(new ArrayList<TelefoneVO>());
		proposta.getProponente().getTelefones().add(telefone);
		return proposta;
	}

	@Override
	protected List<TelefoneVO> consultarEntidadesRelacionada(Set<Long> idsRelacionados) {
		return telefoneDAO.listarPorProponentes(idsRelacionados);
	}

	@Override
	protected Long extrairIdentificadorEntidadePai(TelefoneVO telefone) {
		if (telefone.getProponente() == null) {
			return null;
		}
		return telefone.getProponente().getCodigo();
	}
}
