package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoSucursalSeguradora;

import com.google.common.base.Predicate;

/**
 * Filtra a lista de corretores removendo os que pertencem a sucursal BVP.
 * @author WDEV
 */
public class SucursalSeguradoraEmissoraPredicate implements Predicate<CorretorVO> {

	public boolean apply(CorretorVO corretor) {
		return TipoSucursalSeguradora.EMISSORA.equals(corretor.getSucursalSeguradora().getTipo());
	}

}