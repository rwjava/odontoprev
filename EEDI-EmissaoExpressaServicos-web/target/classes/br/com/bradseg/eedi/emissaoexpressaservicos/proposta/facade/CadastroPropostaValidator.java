package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.text.MessageFormat;
import java.util.List;
import java.util.Set;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.validator.Validators;
import br.com.bradseg.eedi.emissaoexpressaservicos.contacorrente.facade.ContaCorrenteServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.endereco.facade.EnderecoServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.AgenciaBancariaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.AssistenteProducaoDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.BeneficiarioDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.CanalVendaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.ProponenteDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.Validador;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.ValidadorCNS;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.ValidadorDataNascimento;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AgenciaBancariaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AngariadorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.Banco;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorMasterVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DependenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FormaPagamento;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.GrauParentesco;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TelefoneVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;
import br.com.bradseg.repf.digitacaoproposta.cep.webservice.BusinessException_Exception;
import br.com.bradseg.repf.digitacaoproposta.cep.webservice.IntegrationException_Exception;

import com.google.common.collect.Lists;

/**
 * Valida se todos os dados necess�rios para o cadastro de uma proposta foram preenchidos e est�o v�lidos de acordo com as regras.
 * 
 * @author WDEV
 */
@Scope("prototype")
@Named("CadastroPropostaValidator")
public class CadastroPropostaValidator {

	@Autowired
	private PropostaServiceFacade propostaServiceFacade;

	@Autowired
	private AgenciaBancariaDAO agenciaBancariaDAO;

	@Autowired
	private ContaCorrenteServiceFacade contaCorrenteServiceFacade;

	@Autowired
	private AssistenteProducaoDAO assistenteProducaoDAO;

	@Autowired
	private BeneficiarioDAO beneficiarioDAO;
	
	@Autowired
	private ProponenteDAO proponenteDAO;

	@Autowired
	private EnderecoServiceFacade enderecoServiceFacade;

	@Autowired
	private CanalVendaDAO canalVendaDAO;
	
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CadastroPropostaValidator.class);
	
	/**
	 * Valida se todas as informa��es necess�rias para criar a proposta foram preenchidas e est�o de acordo com as regras
	 * 
	 * @param proposta Dados da proposta
	 */
	public void validate(PropostaVO proposta) {
		Validador validador = new Validador();

		validarCanalVenda(validador, proposta.getCanalVenda());
		validarFormaPagamento(validador, proposta);
		validarSituacaoProposta(validador, proposta);
		
		//TODO Agencia produtora n�o � obrigat�rio para os canais que utilizam o EEDI-Servicos
		//validarAgenciaProdutora(validador, proposta);
		
		validarCorretorSucursal(validador, proposta);
		validarAssistenteProducao(validador, proposta);
		validarBeneficiarios(validador, proposta);
		validarTelefonesProponente(validador, proposta.getProponente());
		if(proposta.getDependentes() == null && !com.google.common.base.Strings.isNullOrEmpty(proposta.getProponente().getCpf())){
			if(proposta.getCanalVenda().getCodigo() == 1 || proposta.getCanalVenda().getCodigo() == 8){
				validarProponente(validador, proposta.getProponente(), proposta.getTitular());
			}
		}
		
		//		validarPropostasNoDiaPorCPF(validador, proposta);

		if (validador.hasError()) {
			LOGGER.error("ERRO NA VALIDA��O: "+validador.getMessages());
			throw new BusinessException(validador.getMessages());
		}
	}

	/**
	 * Valida se o canal de venda da proposta foi preenchido
	 * 
	 * @param validador Validador
	 * @param canalVenda Canal de venda da proposta
	 */
	private void validarCanalVenda(Validador validador, CanalVendaVO canalVenda) {
		boolean canalVendaPreenchido = validador.obrigatorio(canalVenda, "Canal de Venda") && validador.obrigatorio(canalVenda.getCodigo(), "Canal de Venda");
		if (canalVendaPreenchido) {
			validador.verdadeiro(canalVendaDAO.consultarPorCodigo(canalVenda.getCodigo()) != null, "msg.erro.cadastro.proposta.canal.venda.invalido");
		}
	}

	/**
	 * Valida se a situa��o da proposta ainda permite a altera��o: 
	 * 
	 * <ul>
	 * 	<li>Consulta a �ltima situa��o da proposta</li>
	 * 	<li>Valida se a situa��o consultada est� na situa��o permitida</li>
	 * </ul>
	 * 
	 * @param validador Validador
	 * @param proposta Dados da proposta
	 */
	private void validarSituacaoProposta(Validador validador, PropostaVO proposta) {
		if (proposta.getNumeroSequencial() != null) {
			SituacaoProposta situacao = propostaServiceFacade.consultarUltimaSituacaoProposta(proposta);
			List<SituacaoProposta> situacoesPermitidas = Lists.newArrayList(SituacaoProposta.RASCUNHO, SituacaoProposta.PENDENTE, SituacaoProposta.CRITICADA);
			validador.verdadeiro(situacao == null || situacoesPermitidas.contains(situacao), "msg.erro.cadastro.proposta.situacao.nao.permitida");
		}
	}

	/**
	 * Valida se o cliente est� na lista de restri��o
	 * 
	 * @param validador validador
	 * @param proposta proposta
	 */
	private void validarClienteListaRestricao(Validador validador, PropostaVO proposta) {
		CorretorVO corretor = proposta.getCorretor();
		if (corretor != null && proposta.getSucursalSeguradora() != null) {
			corretor.setSucursalSeguradora(proposta.getSucursalSeguradora());
			validador.falso(contaCorrenteServiceFacade.isClienteEmListaRestricao(proposta.getProponente().getCpf(), corretor), "msg.erro.cadastro.proposta.cliente.invalido");
		}
	}

	/**
	 * Valida se foi escolhido pelo menos um tipo de sucursal (emissora ou bvp). Se for BVP, valida se foi preenchido o CPD do corretor master ou matr�cula do gerente bvp.
	 * Valida se a matr�cula do assistente n�o � igual a matr�cula do gerente.
	 * 
	 * @param validador Validador
	 * @param proposta Dados da proposta
	 */
	private void validarCorretorSucursal(Validador validador, PropostaVO proposta) {
		SucursalSeguradoraVO sucursalSeguradora = proposta.getSucursalSeguradora();
		boolean sucursalPreenchida = validador.verdadeiro(sucursalSeguradora != null, "msg.erro.cadastro.proposta.cpd.sucursal.obrigatorio");
		//verificar se o tipo de sucursal est� preenchido
		boolean tipoSucursalPreenchido = false;
		
		if(sucursalSeguradora != null){
			tipoSucursalPreenchido = validador.verdadeiro(sucursalSeguradora.getTipo() != null, "msg.erro.sucursal.tipo.invalido");
		}
		
		if (sucursalPreenchida && tipoSucursalPreenchido) {
			// Valida se os dados do corretor foram preenchido e est�o corretos
			boolean corretorPreenchido = validador.obrigatorio(proposta.getCorretor(), "CPD do Corretor") && validador.obrigatorio(proposta.getCorretor().getCpd(), "CPD do Corretor");
			if (corretorPreenchido) {
				validador.verdadeiro(proposta.getCorretor().isNomePreenchido(), "msg.erro.cadastro.proposta.corretor.inexistente");
			}

			// Valida os dados do angariador, caso tenha sido escolhido sucursal Emissora
			if (sucursalSeguradora.isEmissora()) {
				AngariadorVO angariador = proposta.getAngariador();
				boolean angariadorPreenchido = validador.obrigatorio(angariador, "CPD do Angariador") && validador.obrigatorio(angariador.getCpd(), "CPD do Angariador");
				if (angariadorPreenchido) {
					validador.verdadeiro(angariador.isNomePreenchido(), "msg.erro.cadastro.proposta.corretor.inexistente");
				}
			}

			// Valida os dados do corretor master ou gerente, caso tenha sido escolhido sucursal BVP
			if (sucursalSeguradora.isBVP() && (!proposta.getCanalVenda().getCodigo().equals(3)&& !proposta.getCanalVenda().getCodigo().equals(10) && !proposta.getCanalVenda().getCodigo().equals(7))) {
				CorretorMasterVO corretorMaster = proposta.getCorretorMaster();
				boolean corretorMasterPreenchido = corretorMaster != null && corretorMaster.isCpdPreenchido();
				validador.verdadeiro(corretorMasterPreenchido || proposta.isCodigoMatriculaGerentePreenchido(), "msg.erro.cadastro.proposta.bvp.gerente.ou.corretor.obrigatorio");

				if (corretorMasterPreenchido) {
					validador.verdadeiro(corretorMaster.isNomePreenchido(), "msg.erro.cadastro.proposta.corretor.inexistente");
				}
			}

		}
	}

	/**
	 * Valida os dados do gerente e assistente de produ��o da proposta
	 * 
	 * @param validador Validador
	 * @param proposta Dados da proposta
	 */
	private void validarAssistenteProducao(Validador validador, PropostaVO proposta) {
		boolean assistenteProducaoPreenchido = validador.obrigatorio(proposta.getCodigoMatriculaAssistente(), "Assistente de produ��o BS");
		if (assistenteProducaoPreenchido) {
			
			if(proposta.getCanalVenda().getCodigo() == null || proposta.getCanalVenda().getCodigo().equals(1)){
				validador.verdadeiro(assistenteProducaoDAO.verificarExistencia(proposta.getCodigoMatriculaAssistente()), "msg.erro.cadastro.proposta.matricula.assistente.invalido");
			}
			if (proposta.isCodigoMatriculaGerentePreenchido()) {
				validador.falso(proposta.getCodigoMatriculaAssistente().equals(proposta.getCodigoMatriculaGerente()), "msg.erro.cadastro.proposta.gerente.produto.igual.assistente.producao");
				if(proposta.getCanalVenda().getCodigo() == null || proposta.getCanalVenda().getCodigo().equals(1)){
					validador.verdadeiro(assistenteProducaoDAO.verificarExistencia(proposta.getCodigoMatriculaGerente()), "msg.erro.cadastro.proposta.matricula.gerente.invalido");
				}
			}
		}
	}

	/**
	 * Valida se a ag�ncia produtora foi preenchida e caso tenha sido, se ela existe.
	 * 
	 * @param validador Validador
	 * @param proposta Dados da proposta
	 */
	private void validarAgenciaProdutora(Validador validador, PropostaVO proposta) {
		CanalVendaVO canalVenda = proposta.getCanalVenda();
	    
		if (canalVenda == null || canalVenda.getCodigo() == null || (!Constantes.CODIGO_CANAL_VENDA_CALL_CENTER.equals(canalVenda.getCodigo()) 
				&& !Constantes.CODIGO_CANAL_VENDA_INTERNET_BANKING.equals(canalVenda.getCodigo()) && (!Constantes.CODIGO_CANAL_VENDA_FAMILIA_BRADESCO.equals(canalVenda.getCodigo())) )) {
			boolean agenciaProdutoraPreenchida = validador.obrigatorio(proposta.getAgenciaProdutora(), "Ag�ncia produtora") && validador.obrigatorio(proposta.getAgenciaProdutora().getCodigo(), "Ag�ncia produtora");
			if (agenciaProdutoraPreenchida) {
				AgenciaBancariaVO agenciaProdutora = agenciaBancariaDAO.consultarPorId(proposta.getAgenciaProdutora().getCodigo());
				validador.verdadeiro(agenciaProdutora != null, "msg.erro.cadastro.proposta.agencia.produtora.nao.encontrada");
			}
		}
	}

	/**
	 * Valida se a forma de pagamento foi preenchida (Boleto banc�rio ou D�bito autom�tico) e em caso de d�bito autom�tico,
	 * valida se todas as informa��es obrigat�rias foram preenchidas
	 * 
	 * @param validador Validador
	 * @param proposta Dados da proposta
	 */
	private void validarFormaPagamento(Validador validador, PropostaVO proposta) {
		// Valida se a forma de pagamento foi preenchida
		boolean formaPagamentoValida = validador.obrigatorio(proposta.getFormaPagamento(), "Forma de pagamento");

		// Se a forma de pagamento for preenchida e for d�bito autom�tico em conta, deve validar se as informa��es est�o preenchidas e corretas 
		if (formaPagamentoValida && (FormaPagamento.DEBITO_AUTOMATICO.equals(proposta.getFormaPagamento()) || FormaPagamento.BOLETO_DEMAIS_DEBITO.equals(proposta.getFormaPagamento()) || FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento()) || FormaPagamento.DEBITO_DEMAIS_BOLETO.equals(proposta.getFormaPagamento()))) {
			validarDebitoAutomatico(validador, proposta);
		}
	}

	/**
	 * Valida se foi associado pelo menos um telefone na proposta e se as informa��es do telefone informado est�o corretas.
	 * 
	 * @param validador Validador
	 * @param proponente Proponente da proposta
	 */
	private void validarTelefonesProponente(Validador validador, ProponenteVO proponente) {
		boolean telefoneValido = validador.verdadeiro(proponente != null, "msg.erro.cadastro.proposta.nenhum.telefone.associado") && validador.verdadeiro(proponente.getTelefones() != null, "msg.erro.cadastro.proposta.nenhum.telefone.associado") && validador.falso(proponente.getTelefones().isEmpty(), "msg.erro.cadastro.proposta.nenhum.telefone.associado");
		if (telefoneValido) {
			for (TelefoneVO telefone : proponente.getTelefones()) {
				validarTelefone(validador, telefone, "benefici�rio");
			}
			validarTipoTelefoneDuplicado(validador, proponente.getTelefones());
		}
	}
	
	private void validarProponente(Validador validador, ProponenteVO proponente, TitularVO titular){
		boolean proponentePagadorValido = validador.verdadeiro(proponente != null, "msg.erro.cadastro.proposta.beneficiario.proponente.nao.informado");
		if(proponentePagadorValido){
			validarSituacaoPropostasProponente(validador, "Proponente", proponente.getCpf(), titular.getGrauParentescoTitulaProponente());
			/*if(titular.getGrauParentescoTitulaProponente().getCodigo() == 19){
				validarGrauParentescoProprio(validador,proponente.getCpf(), titular.getCpf());
			}*/
		}
	}
	
	/*private void validarGrauParentescoProprio(Validador validador, String cpfProponente, String cpfTitular){
		validador.falso(validador.cpfIguais(cpfProponente, cpfTitular),"msg.erro.cadastro.proposta.cpfIguais");
	}*/

	/**
	 * Valida se os tipos de telefones do benefici�rio s�o iguais.
	 * 
	 * @param validador Validador
	 * @param telefones Telefones dos proponentes proposta
	 */
	private void validarTipoTelefoneDuplicado(Validador validador, List<TelefoneVO> telefones) {
		List<String> tiposTelefone = Lists.newArrayList();
		for (TelefoneVO telefone : telefones) {
			if (telefone.getTipo() != null) {
				tiposTelefone.add(telefone.getTipo().getDescricao());
			}
		}
		Set<String> tiposDuplicados = Strings.encontrarDuplicados(tiposTelefone);
		for (String tipo : tiposDuplicados) {
			validador.adicionarErro("msg.erro.cadastro.proposta.tipo.telefone.iguais", tipo.toLowerCase());
		}
	}

	/**
	 * Valida se as informa��es obrigat�rias do telefone foram preenchidas: Tipo, DDD e N�mero.
	 *
	 * @param validador Validador
	 * @param telefone Dados do telefone
	 * @param complementoRotulo Complemento do r�tulo
	 */
	private void validarTelefone(Validador validador, TelefoneVO telefone, String complementoRotulo) {
		String rotulo = "{0} do {1}";
		validador.obrigatorio(telefone.getTipo(), MessageFormat.format(rotulo, "Tipo do telefone", complementoRotulo));
		boolean dddPreenchido = validador.obrigatorio(telefone.getDdd(), MessageFormat.format(rotulo, "DDD do telefone", complementoRotulo));
		if (dddPreenchido) {
			validador.ddd(telefone.getDdd(), complementoRotulo);
		}
		validador.numeroTelefone(telefone.getNumero(), complementoRotulo);
	}

	/**
	 * Valida se as informa��es necess�rias para pagamento por D�bito Autom�tico foram informadas e est�o corretas.
	 * <ul>
	 * 	<li>Ag�ncia banc�ria precisa ser preenchida.</li>
	 * 	<li>N�mero da conta corrente precisa ser preenchido.</li>
	 * 	<li>D�gito verificador da conta corrente precisa ser preenchido.</li>
	 * 	<li>CPF do proponente precisa ser preenchido e v�lido.</li>
	 * 	<li>Nome do proponente precisa ser preenchido e v�lido.</li>
	 * </ul>
	 * 
	 * @param validador Validador
	 * @param proposta Proposta
	 */
	private void validarDebitoAutomatico(Validador validador, PropostaVO proposta) {
		ProponenteVO proponente = obterProponente(proposta);

		boolean cpfProponentePreenchido = validador.obrigatorio(proponente.getCpf(), "CPF do titular da conta");
		
		boolean bancoPreenchido = validador.obrigatorio(proposta.getProponente().getContaCorrente().getAgenciaBancaria().getBanco().getCodigo(), "Banco") && validador.verdadeiro(0 != proposta.getProponente().getContaCorrente().getAgenciaBancaria().getBanco().getCodigo().intValue(), "msg.erro.cadastro.proposta.proponente.banco.obrigatorio");

		boolean contaCorrenteValida = validarContaCorrente(validador, proponente);

		if (cpfProponentePreenchido) {
			boolean cpfValido = validador.verdadeiro(Validators.value(proponente.getCpf()).isCpf().validate(), "msg.erro.cadastro.proposta.proponente.cpf.invalido");
			if (cpfValido && bancoPreenchido) {
				validarTitularConta(validador, proponente);
				if(Banco.BRADESCO.getCodigo().equals(proposta.getProponente().getContaCorrente().getAgenciaBancaria().getBanco().getCodigo())){
					validarClienteListaRestricao(validador, proposta);
					validarContaCorrenteTitular(validador, proposta, proponente, contaCorrenteValida);
				}
			}
		}
	}

	/**
	 * Valida se as informa��es referentes a conta corrente do titular est�o corretas.
	 * <ul>
	 * 	<li>Conta precisa ser preenchida</li>
	 * 	<li>Dados Banc�rio precisam ser v�lidos</li>
	 * </ul>
	 * 
	 * @param validador validador
	 * @param proposta proposta
	 * @param proponente proponente
	 * @param contaCorrenteValida conta corrente v�lida
	 */
	private void validarContaCorrenteTitular(Validador validador, PropostaVO proposta, ProponenteVO proponente, boolean contaCorrenteValida) {
		if (contaCorrenteValida) {
			List<ContaCorrenteVO> contas = null;
			try{
				contas = contaCorrenteServiceFacade.listarContaCorrentePorCPF(proponente.getCpf());
			}catch(Exception e){
				LOGGER.error("INFO: Erro ao recuperar informacoes da conta para CPF : " + proponente.getCpf());
			}

			boolean contaCorrenteExistente = validador.verdadeiro(contas != null && !contas.isEmpty(), "msg.erro.cadastro.proposta.proponente.conta.nao.preenchido");
			if (contaCorrenteExistente && !Constantes.CODIGO_CANAL_VENDA_INTERNET_BANKING.equals(proposta.getCanalVenda().getCodigo())) {
				validador.verdadeiro(clientePossuiContaInformada(proposta.getProponente(), contas), "msg.erro.cadastro.proposta.dados.bancarios.invalido");
			}
		}
	}

	/**
	 * Valida se as informa��es do titular da conta foram informadas e est�o corretas
	 * 
	 * <ul>
	 * 	<li>Titular precisa ser v�lido</li>
	 * 	<li>Nome precisa estar completo</li>
	 * 	<li>Email precisa ser preenchido e v�lido</li>
	 * 	<li>Data de nascimento precisa ser v�lida</li>
	 * 	<li>N�o pode ser menor de idade</li>
	 * </ul>
	 * 
	 * @param validador validador
	 * @param proponente proponente
	 */
	private void validarTitularConta(Validador validador, ProponenteVO proponente) {		
		if(Banco.BRADESCO.getCodigo().equals(proponente.getContaCorrente().getAgenciaBancaria().getBanco().getCodigo())){
			
			boolean titularExistente = false;
			
			try{
				titularExistente  = validador.verdadeiro(contaCorrenteServiceFacade.obterTitularContaPorCPF(proponente.getCpf()) != null, "msg.erro.cadastro.proposta.correntista.nao.encontrado");
			}catch(Exception e){
				validador.adicionarErro("msg.erro.cadastro.proposta.correntista.nao.encontrado");
			}
			
			if (titularExistente) {
				validador.nomeCompleto(proponente.getNome(), "Nome do titular da conta");

				if (proponente.isEmailPreenchido()) {
					validador.email(proponente.getEmail(), "E-mail do titular da conta");
				}

				boolean dataNascimentoValida = validador.verdadeiro(new ValidadorDataNascimento().validar(proponente.getDataNascimento()), "msg.erro.cadastro.proposta.data.nascimento.invalida", "titular da conta");
				if (dataNascimentoValida) {
					validador.falso(proponente.isMenorDeIdade(), "msg.erro.cadastro.proposta.titular.conta.menor.de.idade");
				}
			}
		}else{
			validador.nomeCompleto(proponente.getNome(), "Nome do titular da conta");

			if (proponente.isEmailPreenchido()) {
				validador.email(proponente.getEmail(), "E-mail do titular da conta");
			}

			boolean dataNascimentoValida = validador.verdadeiro(new ValidadorDataNascimento().validar(proponente.getDataNascimento()), "msg.erro.cadastro.proposta.data.nascimento.invalida", "titular da conta");
			if (dataNascimentoValida) {
				validador.falso(proponente.isMenorDeIdade(), "msg.erro.cadastro.proposta.titular.conta.menor.de.idade");
			}
		}
	}

	/**
	 * Valida se as informa��es da conta corrente est�o preenchidas
	 * 
	 * <ul>
	 * 	<li>Ag�ncia precisa ser preenchida</li>
	 * 	<li>N�mero conta precisa ser preenchida</li>
	 *  <li>Digito conta ser preenchida</li>
	 * </ul>
	 * 
	 * @param validador validador
	 * @param proponente proponente
	 * @return conta corrente preenchida
	 */
	private boolean validarContaCorrente(Validador validador, ProponenteVO proponente) {
		ContaCorrenteVO contaCorrente = obterContaCorrenteProponente(proponente);
		boolean agenciaPreenchida = validador.obrigatorio(contaCorrente.getAgenciaBancaria(), "Ag�ncia banc�ria") && validador.obrigatorio(contaCorrente.getAgenciaBancaria().getCodigo(), "Ag�ncia banc�ria");
		boolean agenciaValida = false;
		if(agenciaPreenchida){
			agenciaValida = validador.verdadeiro(agenciaBancariaDAO.validarAgenciaBancariaComBanco(contaCorrente.getAgenciaBancaria().getBanco().getCodigo(), contaCorrente.getAgenciaBancaria().getCodigo()), "msg.erro.agencia.bancaria.invalida.para.banco.informado");
		}
		boolean numeroContaPreenchida = validador.obrigatorio(contaCorrente.getNumero(), "N�mero da conta corrente");
		boolean digitoContaPreenchida = validador.obrigatorio(contaCorrente.getDigitoVerificador(), "D�gito verificador da conta corrente");
		return agenciaPreenchida && agenciaValida && numeroContaPreenchida && digitoContaPreenchida;
	}

	/**
	 * Obtem o proponente da proposta, caso a proposta n�o possua, retorna um novo proponente vazio
	 * 
	 * @param proposta proposta
	 * @return proponente da proposta
	 */
	private ProponenteVO obterProponente(PropostaVO proposta) {
		if (proposta.getProponente() == null) {
			return new ProponenteVO();
		}
		return proposta.getProponente();
	}

	/**
	 * Obtem a conta corrente do proponente, caso o proponente n�o possua conta, retorna uma nova conta corrente vazia
	 * 
	 * @param proponente proponente
	 * @return Conta corrente do proponente
	 */
	private ContaCorrenteVO obterContaCorrenteProponente(ProponenteVO proponente) {
		if (proponente.getContaCorrente() == null) {
			return new ContaCorrenteVO();
		}
		return proponente.getContaCorrente();
	}

	/**
	 * Verifica se o cliente possui conta informada
	 * 
	 * <ul>
	 * 	<li>Obtem a conta corrente do proponente informada</li>
	 * 	<li>Verifica se o clinete possui a conta corrente informada</li>
	 * 
	 * @param proponente proponente
	 * @param contas contas do proponente informado
	 * @return cliente possui conta informada
	 */
	private boolean clientePossuiContaInformada(ProponenteVO proponente, List<ContaCorrenteVO> contas) {
		ContaCorrenteVO contaCorrenteProposta = obterContaCorrenteProponente(proponente);
		for (ContaCorrenteVO contaCorrente : contas) {
			if (contaCorrente.getNumero().equals(contaCorrenteProposta.getNumero()) && contaCorrente.getDigitoVerificador().equalsIgnoreCase(contaCorrenteProposta.getDigitoVerificador())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Valida as informa��es do benefici�rio titular e dependentes.
	 * 
	 * @param validador Validador
	 * @param proposta Dados da proposta
	 */
	private void validarBeneficiarios(Validador validador, PropostaVO proposta) {
		TitularVO titular = proposta.getTitular();

		validarTitular(validador, titular, proposta.getCanalVenda());

		if (titular != null && new ValidadorDataNascimento().validar(titular.getDataNascimento()) && titular.isMenorDeIdade()) {
			validarResponsavelLegalTitular(validador, proposta.getResponsavelLegal(), titular);
		}
		
		validarDependentes(validador, proposta.getDependentes(), proposta.getProponente().getCpf(), proposta.getCanalVenda(), proposta.getTitular());
		validarBeneficiariosComCPFDuplicado(validador, titular, proposta.getDependentes());
	}

	/**
	 * Valida as informa��es do respons�vel legal do titular, caso este seja menor de idade.
	 * 
	 * @param validador Validador
	 * @param responsavelLegal Respons�vel legal do titular da proposta 
	 */
	private void validarResponsavelLegalTitular(Validador validador, ResponsavelLegalVO responsavelLegal, TitularVO titular){
		boolean responsavelLegalPreenchido = validador.verdadeiro(responsavelLegal != null, "msg.erro.cadastro.proposta.responsavel.legal.obrigatorio");

		if (responsavelLegalPreenchido) {
			boolean cpfValido = validador.cpf(responsavelLegal.getCpf(), "CPF do respons�vel legal do titular");

			if (cpfValido) {
				List<String> cpfs = Lists.newArrayList();

				if (titular.isCpfPreenchido()) {
					cpfs.add(titular.getCpf());
				}

				cpfs.add(responsavelLegal.getCpf());

				Set<String> cpfsDuplicados = Strings.encontrarDuplicados(cpfs);
				if (!cpfsDuplicados.isEmpty()) {
					validador.adicionarErro("msg.erro.cadastro.proposta.cpf.duplicado.responsavel.legal", cpfsDuplicados.toString());
				}
			}

			validador.nomeCompleto(responsavelLegal.getNome(), "Nome do respons�vel legal do titular");

			boolean dataNascimentoPreenchido = validador.obrigatorio(responsavelLegal.getDataNascimento(), "Data de nascimento do respons�vel legal do titular");
			if (dataNascimentoPreenchido) {
				boolean dataNascimentoValida = validador.verdadeiro(new ValidadorDataNascimento().validar(responsavelLegal.getDataNascimento()), "msg.erro.cadastro.proposta.data.nascimento.invalida", "respons�vel legal do titular");

				if (dataNascimentoValida) {
					validador.falso(responsavelLegal.isMenorDeIdade(), "msg.erro.cadastro.proposta.responsavel.legal.menor.de.idade");
				}
			}

			boolean emailPreenchido = validador.obrigatorio(responsavelLegal.getEmail(), "E-mail do respons�vel legal do titular");
			if (emailPreenchido) {
				validador.email(responsavelLegal.getEmail(), "E-mail do respons�vel legal");
			}

			boolean telefoneValido = validador.obrigatorio(responsavelLegal.getTelefone(), "TelefoneVO do respons�vel legal do titular");
			if (telefoneValido) {
				validarTelefone(validador, responsavelLegal.getTelefone(), "respons�vel legal do titular");
			}

			boolean enderecoValido = validador.obrigatorio(responsavelLegal.getEndereco(), "Endere�o do respons�vel legal do titular");
			if (enderecoValido) {
				validarEndereco(validador, responsavelLegal.getEndereco(), "respons�vel legal do titular");
			}
		}
	}

	/**
	 * Valida se algum benefici�rio possui CPF duplicado
	 * 
	 * @param validador Validador
	 * @param titular Benefici�rio titular da proposta
	 * @param dependentes Dependentes da proposta
	 */
	private void validarBeneficiariosComCPFDuplicado(Validador validador, TitularVO titular, List<DependenteVO> dependentes) {
		List<String> cpfs = Lists.newArrayList();

		if (titular != null && titular.isCpfPreenchido()) {
			cpfs.add(titular.getCpf());
		}

		if (dependentes != null) {
			for (DependenteVO dependente : dependentes) {
				if (dependente.isCpfPreenchido()) {
					cpfs.add(dependente.getCpf());
				}
			}
		}

		Set<String> cpfsDuplicados = Strings.encontrarDuplicados(cpfs);
		if (!cpfsDuplicados.isEmpty()) {
			validador.adicionarErro("msg.erro.cadastro.proposta.cpf.duplicado", cpfsDuplicados.toString());
		}
	}
private void validarSituacaoPropostasBeneficiarioCanalDiferentes(Validador validador, String rotulo, String cpfBeneficiario){
		
		List<Long> listaDePropostasBeneficionario = beneficiarioDAO.listarPropostasBeneficionario(cpfBeneficiario);
		for(Long sequencialProposta : listaDePropostasBeneficionario){
			if(propostaServiceFacade.propostaPossuiStatusCancelada(sequencialProposta)){
				continue;
			}
			if(propostaServiceFacade.possuiStatusImplantada(sequencialProposta)){
				validador.adicionarErro("msg.erro.cadastro.proposta.beneficiario.ja.existe.proposta.numero",rotulo, cpfBeneficiario, sequencialProposta);
				break;
			}
			/*SituacaoProposta situacao = propostaServiceFacade.consultarUltimaSituacaoProposta(sequencialProposta);
			
			validador.verdadeiro(situacao == null || isSituacaoPermitida(situacao), "msg.erro.cadastro.proposta.beneficiario.ja.existe.proposta.numero", 
					 rotulo, cpfBeneficiario, String.valueOf(sequencialProposta));*/
		}
	}

	private void validaDependentesSituacao(GrauParentesco grauParentescoTipo, String cpfProponente, Validador validador){
		
		List<Long> listaDePropostasProponente = proponenteDAO.listarPropostasProponente(cpfProponente);
		//int resultado = 0;
		int qtdDeParentescoPermitico = 0;
		int qtdDeParentescoPermiticoFilha = 0;
		int qtdDeParentescoPermiticoFilho = 0;
		int qtdPropostasAtivas = 0;
		
		
		for(Long sequencialProposta : listaDePropostasProponente){
			
			Integer numeroMovimentacao = propostaServiceFacade.consultaMovimentacao(sequencialProposta);
			
			if(numeroMovimentacao == 1 || numeroMovimentacao == 3 || numeroMovimentacao == 6){
				qtdPropostasAtivas += 1; 
			}
		}
		
		if(qtdPropostasAtivas > 10){
			validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.proposta.numero", "",
					cpfProponente);
		}else{
		/*
		 * @return valida��o da lista de proposta vigentes com os graus de parentesco.
		 * 
		 */
			for (Long sequencialProposta : listaDePropostasProponente) {
				if (propostaServiceFacade.propostaPossuiStatusCancelada(sequencialProposta)) {
					continue;
				}
		
				Integer temParentesco = propostaServiceFacade.consultarParentescoAtivo(sequencialProposta);
				Integer numeroMovimentacao = propostaServiceFacade.consultaMovimentacao(sequencialProposta);
				
				if(numeroMovimentacao == 1 || numeroMovimentacao == 3 || numeroMovimentacao == 6){
					
					if (temParentesco == 1) {	
						Integer tipoPrentesco = propostaServiceFacade.consultarTipoParentesco(sequencialProposta);
						if (tipoPrentesco == 1 && grauParentescoTipo.getCodigo() == 1) {
							qtdDeParentescoPermitico += 1;
		
							if (qtdDeParentescoPermitico >= 1) {
								validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco",sequencialProposta , "Pai");
								break;
							}
		
						} else if (tipoPrentesco == 2 && grauParentescoTipo.getCodigo() == 2) {
		
							qtdDeParentescoPermitico += 1;
		
							if (qtdDeParentescoPermitico >= 1) {
								validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco", "M�e");
								break;
							}
						} else if (tipoPrentesco == 5 && grauParentescoTipo.getCodigo() == 5) {
		
							qtdDeParentescoPermitico += 1;
		
							if (qtdDeParentescoPermitico >= 1) {
								validador
										.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco", "Conjuge");
								break;
							}
		
						} else if (tipoPrentesco == 19 && grauParentescoTipo.getCodigo() == 19) {
							qtdDeParentescoPermitico += 1;
		
							if (qtdDeParentescoPermitico >= 1) {
								validador
										.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco", "Proprio");
								break;
							}
						} else if (tipoPrentesco == 3) {
		
							qtdDeParentescoPermiticoFilha += 1;
		
							if (qtdDeParentescoPermiticoFilha > 10) {
								validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco.Filha");
								break;
							}
		
						} else if (tipoPrentesco == 4) {
							qtdDeParentescoPermiticoFilho += 1;
		
							if (qtdDeParentescoPermiticoFilho > 10) {
								validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco.Filho ");
								break;
							}
						}
					} 
				}
			}
		}
	}
	/**
	 * Valida se foi associado pelo menos um benefici�rio titular e se as informa��es obrigat�rias do benefici�rio titular foram preenchidas.
	 * 
	 * @param validador Validador
	 * @param beneficiarioTitular Benefici�rio titular da proposta
	 * @param canalVenda Canal de venda
	 */
	private void validarTitular(Validador validador, TitularVO titular, CanalVendaVO canalVenda){
		boolean beneficiarioTitularValido = validador.verdadeiro(titular != null, "msg.erro.cadastro.proposta.beneficiario.titular.nao.informado");
		if (beneficiarioTitularValido) {
			boolean cpfValido = validador.cpf(titular.getCpf(), "CPF do titular");
			validarBeneficiario(validador, titular, "titular");

			validarEndereco(validador, titular.getEndereco(), "titular");

			if (cpfValido) {
//				Boolean existePropostaCancelada = beneficiarioDAO.verificarSeBeneficiarioPossuiPropostaCancelada(titular.getCpf());
//				if(!existePropostaCancelada){
//					SituacaoProposta situacao = beneficiarioDAO.listarUltimaSituacaoPropostaBeneficiario(titular.getCpf());
//					validador.verdadeiro(situacao == null || isSituacaoPermitida(situacao), "msg.erro.cadastro.proposta.beneficiario.ja.existe",titular.getCpf());
//				}
				validarSituacaoPropostasBeneficiario(validador, "titular", titular.getCpf());		
			
			}
			
			//boolean emailPreenchido = validador.obrigatorio(titular.getEmail(), "E-mail do titular");
			if (titular.getEmail() != null &&  !"".equals(titular.getEmail().trim())) {
				validador.email(titular.getEmail(), "E-mail do titular");
			}
		}
	}
	
	private void validarSituacaoPropostasBeneficiario(Validador validador, String rotulo, String cpfBeneficiario){
		
		List<Long> listaDePropostasBeneficionario = beneficiarioDAO.listarPropostasBeneficionario(cpfBeneficiario);
		for(Long sequencialProposta : listaDePropostasBeneficionario){
			if(propostaServiceFacade.propostaPossuiStatusCancelada(sequencialProposta)){
				continue;
			}
			if(propostaServiceFacade.possuiStatusImplantada(sequencialProposta)){
				validador.adicionarErro("msg.erro.cadastro.proposta.beneficiario.ja.existe.proposta.numero",rotulo, cpfBeneficiario, sequencialProposta);
				break;
			}
			SituacaoProposta situacao = propostaServiceFacade.consultarUltimaSituacaoProposta(sequencialProposta);
			validador.verdadeiro(situacao == null || 
					isSituacaoPermitida(situacao), "msg.erro.cadastro.proposta.beneficiario.ja.existe.proposta.numero", 
					 rotulo, cpfBeneficiario, String.valueOf(sequencialProposta));
			break;
		}
	}
	private void validarSituacaoPropostasProponente(Validador validador, String rotulo, String cpfProponente, GrauParentesco grauParentescoTitular){
		//int resultado = 0;
		int parentescoPermitido = 0;
		int parentescoPermitidoPai = 0;
		int parentescoPermitidoMae = 0;
		int qtdPropostasAtivas = 0;
		
		List<Long> listaDePropostasProponente = proponenteDAO.listarPropostasProponente(cpfProponente);
		
		for(Long sequencialProposta : listaDePropostasProponente){
			
			Integer numeroMovimentacao = propostaServiceFacade.consultaMovimentacao(sequencialProposta);
			
			if(numeroMovimentacao == 1 || numeroMovimentacao == 3 || numeroMovimentacao == 4 || numeroMovimentacao == 6){
				qtdPropostasAtivas += 1; 
			}
		}
		
		if(qtdPropostasAtivas > 10){
			validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.proposta.numero", rotulo,
					cpfProponente);
		}else{
		
			for (Long sequencialProposta : listaDePropostasProponente) {
				if (propostaServiceFacade.propostaPossuiStatusCancelada(sequencialProposta)) {
					continue;
				}
				Integer numeroMovimentacao = propostaServiceFacade.consultaMovimentacao(sequencialProposta);
				Integer temParentesco = propostaServiceFacade.consultarParentescoAtivo(sequencialProposta);
				
				if(numeroMovimentacao == 1 || numeroMovimentacao == 3 || numeroMovimentacao == 4 || numeroMovimentacao == 6){
					
					if (temParentesco == 1) {
						Integer tipoPrentesco = propostaServiceFacade.consultarTipoParentesco(sequencialProposta);
		
						if (tipoPrentesco == 1 || tipoPrentesco == 2 || tipoPrentesco == 5 || tipoPrentesco == 19) {
		
							parentescoPermitido += 1;
		
							if (tipoPrentesco == 1 && grauParentescoTitular.getCodigo() == 1) {
								if (parentescoPermitido >= 1) {
									validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco", "Pai");
									break;
								}
							} else if (tipoPrentesco == 2 && grauParentescoTitular.getCodigo() == 2) {
								if (parentescoPermitido >= 1) {
									validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco", "M�e");
									break;
								}
							} else if (tipoPrentesco == 5 && grauParentescoTitular.getCodigo() == 5) {
								if (parentescoPermitido >= 1) {
									validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco",
											"C�njuge");
									break;
								}
							} else if (tipoPrentesco == 19 && grauParentescoTitular.getCodigo() == 19) {
								if (parentescoPermitido >= 1) {
									validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco",
											"Pr�prio");
									break;
								}
							}
						} else if (tipoPrentesco == 3) {
							parentescoPermitidoPai += 1;
		
							if (parentescoPermitidoPai > 10) {
								validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco.Filha",
										rotulo);
								break;
							}
						} else if (tipoPrentesco == 4) {
							if (parentescoPermitidoMae > 10) {
								validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco.Filho",
										rotulo);
								break;
							}
						}
					} 
				}else if(numeroMovimentacao == 2){

					if (temParentesco == 1) {
						Integer tipoPrentesco = propostaServiceFacade.consultarTipoParentesco(sequencialProposta);
		
						if (tipoPrentesco == 1 || tipoPrentesco == 2 || tipoPrentesco == 5 || tipoPrentesco == 19) {
		
							parentescoPermitido += 1;
		
							if (tipoPrentesco == 1 && grauParentescoTitular.getCodigo() == 1) {
								if (parentescoPermitido >= 2) {
									validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco", "Pai");
									break;
								}
							} else if (tipoPrentesco == 2 && grauParentescoTitular.getCodigo() == 2) {
								if (parentescoPermitido >= 2) {
									validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco", "M�e");
									break;
								}
							} else if (tipoPrentesco == 5 && grauParentescoTitular.getCodigo() == 5) {
								if (parentescoPermitido >= 2) {
									validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco",
											"C�njuge");
									break;
								}
							} else if (tipoPrentesco == 19 && grauParentescoTitular.getCodigo() == 19) {
								if (parentescoPermitido >= 2) {
									validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco",
											"Pr�prio");
									break;
								}
							}
							
						} else if (tipoPrentesco == 3) {
							parentescoPermitidoPai += 1;
		
							if (parentescoPermitidoPai > 11) {
								validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco.Filha",
										rotulo);
								break;
							}
						} else if (tipoPrentesco == 4) {
							if (parentescoPermitidoMae > 11) {
								validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco.Filho",
										rotulo);
								break;
							}
						}
					} 
					
				}else {
					Integer tipoPrentesco = propostaServiceFacade.consultarTipoParentescoDepen(sequencialProposta);
	
					if (tipoPrentesco == 1 || tipoPrentesco == 2 || tipoPrentesco == 5 || tipoPrentesco == 19) {
						parentescoPermitido += 1;
						if (parentescoPermitido > 1 || parentescoPermitido == 0) {
							validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco", rotulo);
							break;
						}
					} else if (tipoPrentesco == 3) {
						parentescoPermitidoPai += 1;
	
						if (parentescoPermitidoPai > 10) {
							validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco.Pai",
									rotulo);
							break;
						}
					} else if(tipoPrentesco == 4) {
						
						if (parentescoPermitidoMae > 10) {
							validador.adicionarErro("msg.erro.cadastro.proposta.proponente.ja.existe.parentesco.Mae ",rotulo);
							break;
						}
					}
				} 
			}
		}
	}
	/**
	 * Indica se a �ltima situa��o da proposta do titular permite que ele possua novas propostas 
	 * @param situacao Ultima situacao da proposta do titular
	 * @return true se a situa��o for permitida
	 */
	private boolean isSituacaoPermitida(SituacaoProposta situacao) {
		return SituacaoProposta.CANCELADA.equals(situacao) || SituacaoProposta.RASCUNHO.equals(situacao) || SituacaoProposta.CRITICADA.equals(situacao)|| SituacaoProposta.PRE_CANCELADA.equals(situacao);
	}

	/**
	 * Valida as informa��es do endere�o titular / respons�vel legal
	 * 
	 * @param validador Validador
	 * @param endereco Endere�o do benefici�rio titular
	 * @param complementoRotulo Complemento do r�tulo
	 * @throws BusinessException_Exception 
	 * @throws IntegrationException_Exception 
	 */
	private void validarEndereco(Validador validador, EnderecoVO endereco, String complementoRotulo) {
		try{
			
		if (endereco != null) {
			String rotulo = "{0} do {1}";
			boolean cepPreenchido = validador.obrigatorio(endereco.getCep(), MessageFormat.format(rotulo, "CEP", complementoRotulo));
			if (cepPreenchido) {
				
				EnderecoVO enderecoRetornado = enderecoServiceFacade.obterEnderecoPorCep(endereco.getCep());
				enderecoRetornado.setNumero(endereco.getNumero());
				enderecoRetornado.setComplemento(endereco.getComplemento());
				
				if (enderecoRetornado != null &&  enderecoRetornado.isUnidadeFederativaPreenchida()) {
					
						validador.obrigatorio(endereco.getLogradouro(), MessageFormat.format(rotulo, "Logradouro", complementoRotulo));
						validador.obrigatorio(endereco.getNumero(), MessageFormat.format(rotulo, "N�mero do endere�o", complementoRotulo));
						validador.obrigatorio(endereco.getBairro(), MessageFormat.format(rotulo, "Bairro", complementoRotulo));
						validador.obrigatorio(endereco.getCidade(), MessageFormat.format(rotulo, "Municipio", complementoRotulo));
						
						
				} else {
						validador.adicionarErro("msg.erro.cadastro.proposta.endereco.incompleto", complementoRotulo);
					}
				
			}
		} 
		}catch (BusinessException be) {
			LOGGER.error("INFO: CEP Inv�lido!");
			LOGGER.error("INFO: ERRO : "+be.getMessage());
			validador.adicionarErro("msg.erro.cadastro.proposta.cep.invalido", complementoRotulo);
		} catch (IntegrationException be) {
			LOGGER.error("INFO: CEP Inv�lido!");
			LOGGER.error("INFO: ERRO : "+be.getMessage());
			validador.adicionarErro("msg.erro.cadastro.proposta.cep.invalido",  complementoRotulo);
		}
				
	}
	

	/**
	 * Valida se as informa��es obrigat�rias dos benefici�rios dependentes foram preenchidas.
	 * 
	 * @param validador Validador
	 * @param dependentes Dependentes da proposta
	 */
	private void validarDependentes(Validador validador, List<DependenteVO> dependentes, String cpfProponente, CanalVendaVO canalVenda, TitularVO titular) {
		if (dependentes != null) {
			String tipoBeneficiario = "dependente";
			for (DependenteVO dependente : dependentes) {
				validarBeneficiario(validador, dependente, tipoBeneficiario);
				if (!dependente.isMenorDeIdade() || dependente.isCpfPreenchido()) {
					boolean cpfValido = validador.cpf(dependente.getCpf(), "CPF do dependente");
					if (cpfValido) {
						validarSituacaoPropostasBeneficiario(validador,"dependente", dependente.getCpf());
					}
				}
				if(canalVenda.getCodigo() == 1 || canalVenda.getCodigo() == 8){
					validador.obrigatorio(dependente.getGrauParentesco(), "Grau de parentesco do dependente");
					validaDependentesSituacao(titular.getGrauParentescoTitulaProponente(), cpfProponente,validador);
				}/*else{
					validarSituacaoPropostasBeneficiarioCanalDiferentes(validador, "titular", titular.getCpf());
				}*/	
			}
		}
	}



	private void validarBeneficiario(Validador validador, BeneficiarioVO beneficiario, String tipoBeneficiario) {
		String rotulo = "{0} do {1}";

		validador.nomeCompleto(beneficiario.getNome(), MessageFormat.format(rotulo, "Nome", tipoBeneficiario));
		validador.nomeCompleto(beneficiario.getNomeMae(), MessageFormat.format(rotulo, "Nome da m�e", tipoBeneficiario));
		validador.obrigatorio(beneficiario.getSexo(), MessageFormat.format(rotulo, "Sexo", tipoBeneficiario));
		validador.obrigatorio(beneficiario.getEstadoCivil(), MessageFormat.format(rotulo, "Estado civil", tipoBeneficiario));

		boolean dataNascimentoPreenchido = validador.obrigatorio(beneficiario.getDataNascimento(), MessageFormat.format(rotulo, "Data de nascimento", tipoBeneficiario));
		if (dataNascimentoPreenchido) {
			validador.verdadeiro(new ValidadorDataNascimento().validar(beneficiario.getDataNascimento()), "msg.erro.cadastro.proposta.data.nascimento.invalida", tipoBeneficiario);
		}

		if (beneficiario.isCnsPreenchido()) {
			validador.verdadeiro(ValidadorCNS.validar(beneficiario.getNumeroCartaoNacionalSaude()), "msg.erro.cadastro.proposta.cns.invalido", tipoBeneficiario);
		}

	}
}