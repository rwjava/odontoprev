package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;

/**
 * Servi�os de acesso a dados de proponente
 * 
 * @author WDEV
 */
public interface ProponenteDAO {

	/**
	 * Salva um novo proponente no sistema
	 * 
	 * @param proponente Proponente da proposta
	 * @return Proponente salvo
	 */
	public ProponenteVO salvar(ProponenteVO proponente);

	/**
	 * Atualiza as informa��es do proponente da proposta
	 *  
	 * @param proponente Proponente da proposta
	 */
	public void atualizar(ProponenteVO proponente);
	
	public List<Long> listarPropostasProponente(String cpf);

}