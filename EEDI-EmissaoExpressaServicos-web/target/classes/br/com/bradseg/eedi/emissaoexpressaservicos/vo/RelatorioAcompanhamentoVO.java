package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Relatorio de acompanhamento de proposta
 * 
 * @author WDEV
 */
public class RelatorioAcompanhamentoVO implements Serializable {

	private static final long serialVersionUID = -1026323147488915618L;

	private List<ItemRelatorioAcompanhamentoVO> itensRelatorioAcompanhamento;

	private String nome;

	private FiltroRelatorioAcompanhamentoVO filtroRelatorioAcompanhamento;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public FiltroRelatorioAcompanhamentoVO getFiltroRelatorioAcompanhamento() {
		return filtroRelatorioAcompanhamento;
	}

	public void setFiltroRelatorioAcompanhamento(FiltroRelatorioAcompanhamentoVO filtroRelatorioAcompanhamento) {
		this.filtroRelatorioAcompanhamento = filtroRelatorioAcompanhamento;
	}

	public BigDecimal getValorTotal() {
		BigDecimal total = BigDecimal.ZERO;
		for (ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento : itensRelatorioAcompanhamento) {
			total = getValorTotalComTaxaDeAdesao(total, itemRelatorioAcompanhamento);
		}
		return total;
	}

	public BigDecimal getValorTotalPorSucursal() {
		BigDecimal total = BigDecimal.ZERO;
		for (ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento : itensRelatorioAcompanhamento) {
			if (itemRelatorioAcompanhamento.getProposta().getSucursalSeguradora() != null) {
				total = getValorTotalComTaxaDeAdesao(total, itemRelatorioAcompanhamento);
			}
		}
		return total;
	}

	public BigDecimal getValorTotalPorAgenciaProdutora() {
		BigDecimal total = BigDecimal.ZERO;
		for (ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento : itensRelatorioAcompanhamento) {
			if (itemRelatorioAcompanhamento.getProposta().getAgenciaProdutora() != null) {
				total = getValorTotalComTaxaDeAdesao(total, itemRelatorioAcompanhamento);
			}
		}
		return total;
	}

	public BigDecimal getValorTotalPorCorretor() {
		BigDecimal total = BigDecimal.ZERO;
		for (ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento : itensRelatorioAcompanhamento) {
			if (itemRelatorioAcompanhamento.getProposta().getCorretor() != null) {
				total = getValorTotalComTaxaDeAdesao(total, itemRelatorioAcompanhamento);
			}
		}
		return total;
	}

	public BigDecimal getValorTotalPorAssistente() {
		BigDecimal total = BigDecimal.ZERO;
		for (ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento : itensRelatorioAcompanhamento) {
			if (itemRelatorioAcompanhamento.getProposta().isCodigoMatriculaAssistentePreenchido()) {
				total = getValorTotalComTaxaDeAdesao(total, itemRelatorioAcompanhamento);
			}
		}
		return total;
	}

	public BigDecimal getValorTotalPorAgenciaDebito() {
		BigDecimal total = BigDecimal.ZERO;
		for (ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento : itensRelatorioAcompanhamento) {
			AgenciaBancariaVO agencia = itemRelatorioAcompanhamento.getProposta().getProponente().getContaCorrente().getAgenciaBancaria();
			if (agencia != null && agencia.isCodigoPreenchido()) {
				total = getValorTotalComTaxaDeAdesao(total, itemRelatorioAcompanhamento);
			}
		}
		return total;
	}

	public BigDecimal getValorTotalPorGerente() {
		BigDecimal total = BigDecimal.ZERO;
		for (ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento : itensRelatorioAcompanhamento) {
			if (itemRelatorioAcompanhamento.getProposta().isCodigoMatriculaGerentePreenchido()) {
				total = getValorTotalComTaxaDeAdesao(total, itemRelatorioAcompanhamento);
			}
		}
		return total;
	}
	
	private BigDecimal getValorTotalComTaxaDeAdesao(BigDecimal total, ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento) {
		return total.add(itemRelatorioAcompanhamento.getOrcamento().getValorTotalComTaxaDeAdesao());
	}

	public List<ItemRelatorioAcompanhamentoVO> getItensRelatorioAcompanhamento() {
		return itensRelatorioAcompanhamento;
	}

	public void setItensRelatorioAcompanhamento(List<ItemRelatorioAcompanhamentoVO> itensRelatorioAcompanhamento) {
		this.itensRelatorioAcompanhamento = itensRelatorioAcompanhamento;
	}

}