package br.com.bradseg.eedi.emissaoexpressaservicos.corretorsucursal.facade;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws.CorretorServiceWebService;
import br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws.WSBusinessException;
import br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws.WSIntegrationException;
import br.com.bradseg.ccrr.cadastrocorretores.model.vo.ws.WSCorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.contacorrente.facade.ContaCorrenteServiceFacadeImpl;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.ProdutorDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.Validador;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.CorretorComparator;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PessoaProdutor;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoPessoaProdutor;
import br.com.bradseg.inet.consultaexpressa.webservice.BusinessException_Exception;
import br.com.bradseg.inet.consultaexpressa.webservice.CorretorSucursal;
import br.com.bradseg.inet.consultaexpressa.webservice.CorretorSucursalWebService;
import br.com.bradseg.inet.consultaexpressa.webservice.IntegrationException_Exception;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;

/**
 * Servi�o respons�vel por consultar as informa��es do corretor
 * 
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CorretorSucursalServiceFacadeImpl implements CorretorSucursalServiceFacade {

	@Autowired
	protected CorretorSucursalWebService corretorSucursalWebService;

	@Autowired
	protected CorretorServiceWebService corretorServiceSessionFacade;

	@Autowired
	protected ProdutorDAO produtorDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(ContaCorrenteServiceFacadeImpl.class);

	private final LoadingCache<String, SortedSet<CorretorVO>> cacheCorretores;

	public CorretorSucursalServiceFacadeImpl() {
		cacheCorretores = CacheBuilder.newBuilder().maximumSize(1000).expireAfterAccess(10, TimeUnit.MINUTES).build(new CacheLoader<String, SortedSet<CorretorVO>>() {
			public SortedSet<CorretorVO> load(String cpfCnpj) {
				return consultarCorretoresPorCpfCnpj(cpfCnpj);
			}
		});
	}

	/**
	 * A partir do CPF/CNPJ, lista os corretores/sucursais dispon�veis para a proposta.
	 * Retorna um {@link SortedSet} de forma que ordene a partir do c�digo da sucursal e pelo cpd do corretor. Para ordena��o utiliza o {@link CorretorComparator}.
	 * Caso j� tenha sido feita uma consulta pelo cpf/cnpj informado, recupera o resultado do cache
	 * @param cpfCnpj CPF/CNPJ do corretor logado
	 * @return Corretores/Sucursais ordenados e sem duplicidade
	 */
	public SortedSet<CorretorVO> listarPorCpfCnpjUsuario(String cpfCnpj) {
		try {
			cpfCnpj = Strings.normalizarCpfCnpj(cpfCnpj);

			Validador validador = new Validador();
			validador.obrigatorio(cpfCnpj, "CPF/CNPJ");
			if (validador.hasError()) {
				throw new BusinessException(validador.getMessages());
			}

			return cacheCorretores.get(cpfCnpj);
		} catch (Exception e) {
			LOGGER.error("INFO: Erro ao listar corretores por cpf/cnpj do usuario", e);
			if (e.getCause() != null) {
				throw new BusinessException(e.getCause());
			}
			throw new BusinessException(e);
		}
	}

	/**
	 * Consulta os corretores/sucursais dispon�veis para o cpf/cnpj informado. O retorno � feito atrav�s de um {@link SortedSet}, de forma que 
	 * remova os dados duplicados e ordene de acordo com as regras do {@link CorretorComparator}
	 * @param cpfCnpj CPF/CNPJ do corretor logado
	 * @return Corretores/Sucursais ordenados e sem duplicidade
	 */
	private SortedSet<CorretorVO> consultarCorretoresPorCpfCnpj(String cpfCnpj) {
		try {
			SortedSet<CorretorVO> corretores = Sets.newTreeSet(new CorretorComparator());
			corretores.addAll(criarCorretoresValidos(corretorSucursalWebService.consultarListaCorretorSucursal(cpfCnpj, "0")));
			corretores.addAll(criarCorretoresValidos(corretorSucursalWebService.consultarListaCorretorSucursal(cpfCnpj, "1")));

			return corretores;
		} catch (BusinessException_Exception e) {
			LOGGER.error("INFO: Erro na chamada ao webservice para listar as informa��es do corretor", e);
			throw new BusinessException("Erro na chamada ao webservice para listar as informa��es do corretor", e);
		} catch (IntegrationException_Exception e) {
			LOGGER.error("INFO: Erro de integra��o na chamada ao webservice para listar as informa��es do corretor", e);
			throw new IntegrationException("Erro de integra��o na chamada ao webservice para listar as informa��es do corretor", e);
		}
	}

	/**
	 * Ap�s validar se o c�digo da sucursal e cpd do corretor foram preenchidos, consulta atrav�s de webservice as informa��es do corretor
	 * @param codigoSucursal C�digo da sucursal do corretor
	 * @param cpdCorretor CPD do corretor
	 * @return Informa��es do corretor
	 */
	public CorretorVO consultarCorretor(Integer codigoSucursal, Integer cpdCorretor) {
		
		WSCorretorVO wsCorretorVO = null;
		try {
			validarCorretorSucursal(codigoSucursal, cpdCorretor);

			wsCorretorVO = corretorServiceSessionFacade.obterDadosCorretorPorCpd(codigoSucursal, cpdCorretor);
			if (wsCorretorVO != null && wsCorretorVO.getCodigoCPD() != null) {
				return transformarCorretor(wsCorretorVO);
			}
			return null;
		} catch (WSBusinessException e) {
			LOGGER.error("INFO: Erro na chamada ao webservice para consultar as informa��es do corretor", e);
			throw new BusinessException("Erro na chamada ao webservice para consultar as informa��es do corretor", e);
		} catch (WSIntegrationException e) {
			LOGGER.error("INFO: Erro de integra��o na chamada ao webservice para consultar as informa��es do corretor", e);
			throw new IntegrationException("Erro de integra��o na chamada ao webservice para consultar as informa��es do corretor", e);
		}catch(Exception e){
			try {
				wsCorretorVO = corretorServiceSessionFacade.obterDadosCorretorPorCpd(codigoSucursal, cpdCorretor);
				if (wsCorretorVO != null && wsCorretorVO.getCodigoCPD() != null) {
					return transformarCorretor(wsCorretorVO);
				}
				return null;
			} catch (WSBusinessException e1) {
				LOGGER.error("INFO: Erro na chamada ao webservice para consultar as informa��es do corretor", e1);
				throw new BusinessException("Erro na chamada ao webservice para consultar as informa��es do corretor", e1);
			} catch (WSIntegrationException e1) {
				LOGGER.error("INFO: Erro de integra��o na chamada ao webservice para consultar as informa��es do corretor", e1);
				throw new IntegrationException("Erro de integra��o na chamada ao webservice para consultar as informa��es do corretor", e1);
			}
		
		}
	
	
	}

	/**
	 * Valida se o c�digo da sucursal e o CPD do corretor foram informados
	 * @param codigoSucursal C�digo da sucursal do corretor
	 * @param cpdCorretor CPD do corretor
	 */
	private void validarCorretorSucursal(Integer codigoSucursal, Integer cpdCorretor) {
		Validador validador = new Validador();
		validador.obrigatorio(codigoSucursal, "Codigo Sucursal");
		validador.obrigatorio(cpdCorretor, "CPD do corretor");

		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}
	}

	/**
	 * Transforma o resultado da consulta do webservice em um VO de Corretor
	 * @param cpdCorretor CPD do corretor
	 * @param wsCorretor Retorno do webservice com as informa��es do corretor
	 * @return Informa��es do corretor
	 */
	private CorretorVO transformarCorretor(WSCorretorVO wsCorretor) {
		CorretorVO corretor = new CorretorVO();
		corretor.setCpd(wsCorretor.getCodigoCPD().intValue());
		corretor.setNome(wsCorretor.getNomeCorretor());
		if (wsCorretor.getCodigoCpfCnpjCorretor() != null) {
			corretor.setCpfCnpj(Strings.normalizarCpfCnpj(String.valueOf(wsCorretor.getCodigoCpfCnpjCorretor())));
		}
		corretor.setSucursalSeguradora(new SucursalSeguradoraVO(wsCorretor.getCodigoSucursalSeguradora()));
		return corretor;
	}

	/**
	 * Verifica se os cacheCorretores informados est�o em sucursais v�lidas, adicionando os v�lidos em um {@link Set}
	 * 
	 * @param listaCorretorSucursal lista ordenada de cacheCorretores sem repeticao
	 * @return cacheCorretores v�lidos
	 */
	private Set<CorretorVO> criarCorretoresValidos(List<CorretorSucursal> listaCorretorSucursal) {
		Set<CorretorVO> corretores = Sets.newHashSet();

		for (CorretorSucursal corretorSucursal : listaCorretorSucursal) {
			SucursalSeguradoraVO sucursalSeguradora = new SucursalSeguradoraVO(corretorSucursal.getCodigoSucursal());
			if (sucursalSeguradora.isValida()) {
				CorretorVO corretor = new CorretorVO();
				corretor.setCpd(corretorSucursal.getCodigoCorretor());

				sucursalSeguradora.setDescricao(corretorSucursal.getNomeSucursal());
				corretor.setSucursalSeguradora(sucursalSeguradora);

				corretores.add(corretor);
			}
		}
		return corretores;
	}

	public CorretorVO detalharInformacoesPessoaCorretor(Integer numeroProdutor) {
		Integer codigoPessoa = produtorDAO.buscarCodigoPessoaPorCpd(numeroProdutor);
		CorretorVO corretor = new CorretorVO();

		if (codigoPessoa == null) {
			throw new BusinessException("N�o foi possivel gerar o arquivo ADS. N�o foi encontrado os detalhes do corretor");
		} else {
			PessoaProdutor pessoaProdutor = produtorDAO.buscarInformacoesCorretorPorCodigo(codigoPessoa);

			if (TipoPessoaProdutor.PESSOA_FISICA.getTipo().equals(pessoaProdutor.getTipoPessoaProdutor().getTipo())) {
				corretor.setCpfCnpj(String.valueOf(produtorDAO.buscarCpfPorCodigoPessoa(codigoPessoa)));
			} else {
				corretor.setCpfCnpj(String.valueOf(produtorDAO.buscarCnpjPorCodigoPessoa(codigoPessoa)));
			}

			corretor.setCpd(produtorDAO.buscarCpdCorretorPorNumeroProdutor(numeroProdutor));
		}

		return corretor;
	}
}