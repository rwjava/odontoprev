package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ItemRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;

/**
 * Consulta as propostas para relatório de acompanhamento
 * @author WDEV
 */
public interface RelatorioAcompanhamentoDAO {

	/**
	 * Lista as propostas a partir de filtro de pesquisa para gerar o relatório de acompanhamento
	 * @param filtro Filtro de pesquisa de relatório de acompanhamento
	 * @param login do usuário utilizando o sistema
	 * @return Lista de itens de relatório de acompanhamento
	 */
	public List<ItemRelatorioAcompanhamentoVO> listarPropostasPorFiltro(FiltroRelatorioAcompanhamentoVO filtro, LoginVO login);

}