package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import java.io.Serializable;
import java.util.Comparator;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;

/**
 * Comparator da classe CorretorVO
 * 
 * @author WDEV
 */
public class CorretorComparator implements Comparator<CorretorVO>, Serializable {

	private static final long serialVersionUID = 1191825999152219468L;

	/**
	 * Compara os corretores atraves do codigo da sucursal e do cpd, com o objetivo de ordern�-los
	 * @param corretor Um corretor a ser comparado
	 * @param outroCorretor Um outro corretor a ser comparado
	 * 
	 * @return Indicador de comparacao
	 */
	public int compare(CorretorVO corretor, CorretorVO outroCorretor) {
		int comparacaoSucursal = corretor.getSucursalSeguradora().getCodigo().compareTo(outroCorretor.getSucursalSeguradora().getCodigo());
		if (comparacaoSucursal == 0) {
			return corretor.getCpd().compareTo(outroCorretor.getCpd());
		}

		return comparacaoSucursal;
	}
}
