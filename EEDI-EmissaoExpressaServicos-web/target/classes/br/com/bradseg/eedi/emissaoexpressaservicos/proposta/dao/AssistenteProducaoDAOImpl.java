package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;

/**
 * Implementação do acesso a dados do assistente de produção
 * 
 * @author WDEV
 */
@Repository
public class AssistenteProducaoDAOImpl extends JdbcDao implements AssistenteProducaoDAO {

	@Autowired
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public Boolean verificarExistencia(String matricula) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT COUNT(*)                  ");
		sql.append(" FROM                             ");
		sql.append(" ATSAUDAO.SSCSEGURADOS            ");
		sql.append(" WHERE                            ");
		sql.append(" SEG_CIA = 571                    ");
		sql.append(" AND SEG_APOLICE = 6001           ");
		sql.append(" AND SEG_AMD_CANCEL = 0           ");
		sql.append(" AND SEG_CERTIFICADO = :matricula");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("matricula", Long.valueOf(matricula));

		return getJdbcTemplate().queryForInt(sql.toString(), params) > 0;
	} 

}