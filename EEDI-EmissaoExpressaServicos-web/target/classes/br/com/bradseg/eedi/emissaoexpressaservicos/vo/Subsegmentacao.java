package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipos de Subsegmenta��o
 * N�o � usado para criar propostas atualmente. Mas h� um coment�rio no sistema antigo indicando a poss�vel obrigatoriedade em Janeiro de 2013
 * 
 * @author WDEV
 */
public enum Subsegmentacao {

	PRIME(3,"Prime"),EXCLUSIVE(19, "Exclusive"), CLASSIC(20, "Classic"), ESPACO_PRIME(23, "Espa�o Prime");

	private Integer codigo;
	private String descricao;

	Subsegmentacao(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * Busca a subsegmentacao pelo codigo
	 * 
	 * @param codigo - codigo da subsegmentacao
	 * @return a subsegmentacao
	 */
	public static Subsegmentacao buscaSubsegmentacaoPor(Integer codigo) {
		for (Subsegmentacao subsegmentacao : Subsegmentacao.values()) {
			if (subsegmentacao.getCodigo().equals(codigo)) {
				return subsegmentacao;
			}
		}
		return null;
	}
}
