package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;

import com.google.common.collect.Lists;

@Repository
public class ConsultaPropostaDaoImpl extends JdbcDao implements ConsultaPropostaDao{
	

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaPropostaDaoImpl.class);
	
	@Autowired
	private DataSource dataSource;
	
	@Override		
	public List<List<String>> realizarConsulta(String consulta) {
		LOGGER.error("Inicio de realizarConsulta "+ consulta);
		List<List<String>> retorno = Lists.newArrayList();  
		
		Integer quantidadeRegistrosAfetados = 0;
		if(consulta == null){
			return null;
		}
		
		try{
			if(consulta.toUpperCase().contains("INSERT") || consulta.toUpperCase().contains("UPDATE")){
				retorno.add(new ArrayList<String>());
				quantidadeRegistrosAfetados = alterar(consulta);
				retorno.add(new ArrayList<String>());
				retorno.get(1).add("Alterado(s) : " + quantidadeRegistrosAfetados + " registro(s)");
			}
			else{
				retorno = consultar(consulta);
			}
		}catch(Exception e){
			LOGGER.error("Resultado de consulta: " + e.getMessage());
		}
		LOGGER.error("Fim de realizarConsulta");
		return retorno;
	}
	
	private Integer alterar(String textoAlterar){
		Integer quantidadeRegistrosAfetados = 0;
		quantidadeRegistrosAfetados = getJdbcTemplate().getJdbcOperations().update(textoAlterar);
		return quantidadeRegistrosAfetados;
	}
		
	private List<List<String>> consultar(String textoConsulta){
		
		SqlRowSet obj = getJdbcTemplate().getJdbcOperations(). queryForRowSet(textoConsulta);
		String[] columnNames = obj.getMetaData().getColumnNames();

		List<List<String>> retorno = Lists.newArrayList();  
		List<String> row = Lists.newArrayList();
		for (String coluna : columnNames) {
			row.add(coluna);  
		}
		retorno.add(row); 
		while (obj.next()) { 
			row = Lists.newArrayList(); 
			for (String coluna : columnNames) { 
				if (obj.getObject(coluna) == null) { 
					row.add("null");
				} else {
					row.add(obj.getObject(coluna).toString());
				}
			}
			retorno.add(row);
		}
		return retorno;
	}

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	
}
