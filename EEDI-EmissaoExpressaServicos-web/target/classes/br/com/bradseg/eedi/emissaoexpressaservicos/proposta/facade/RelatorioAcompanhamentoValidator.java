package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import org.joda.time.Days;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.Validador;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroRelatorioAcompanhamentoVO;

/**
 * Valida se todos os dados necess�rios para consultar os relat�rios de acompanhamento foram preenchidos
 * 
 * @author WDEV
 */
public class RelatorioAcompanhamentoValidator {

	/**
	 * Valida o filtro de pesquisa de propostas para relat�rio de acompanhamento, segundo as seguintes regras:
	 * 
	 * <ul>
	 * 	<li>
	 * 		Se o c�digo da proposta e o cpf do titular n�o forem preenchidos:
	 * 		<ul>
	 * 			<li>� obrigat�rio informar a data de inicio do per�odo.</li>
	 * 			<li>� obrigat�rio informar a data fim do per�odo.</li>
	 * 		</ul>
	 * 	</li>
	 * 	<li>Se o c�digo da sucursal for preenchido, este deve ser v�lido (ser Emissora ou BVP {@link br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO#isValida()}).
	 * 	<li>Se o CPF do titular for preenchido, este precisa ser v�lido ({@link br.com.bradseg.eedi.emissaoexpressaservicos.util.Validador#cpf(String, String)}.
	 * 	<li>Se o CPF/CNPJ corpo for preenchido, este precisa ser v�lido ({@link br.com.bradseg.eedi.emissaoexpressaservicos.util.Validador#cpfCnpj(String, String)}.
	 * </ul>
	 * 
	 * @param filtroRelatorioAcompanhamento Filtro para pesquisa de propostas para relat�rio de acompanhamento
	 */
	public void validate(FiltroRelatorioAcompanhamentoVO filtroRelatorioAcompanhamento) {
		Validador validador = new Validador();

		if (!filtroRelatorioAcompanhamento.isCodigoPropostaPreenchido() && !filtroRelatorioAcompanhamento.isCpfTitularPreenchido()) {
			// Segundo EN, o preenchimento das duas datas � obrigat�rio. Caso seja pedido para retirar, verificar a consequencia de deixar a query mais lenta
			validador.obrigatorio(filtroRelatorioAcompanhamento.getDataInicio(), "Per�odo inicial");
			validador.obrigatorio(filtroRelatorioAcompanhamento.getDataFim(), "Per�odo final");
		}

		if (filtroRelatorioAcompanhamento.isDataInicioPreenchida() && filtroRelatorioAcompanhamento.isDataFimPreenchida()) {
			Days intervaloData = Days.daysBetween(filtroRelatorioAcompanhamento.getDataInicio(), filtroRelatorioAcompanhamento.getDataFim());
			validador.falso(intervaloData.isGreaterThan(Days.days(30)), "msg.erro.lista.proposta.periodo.maior.que.trinta.dias");
			validador.falso(filtroRelatorioAcompanhamento.getDataFim().isBefore(filtroRelatorioAcompanhamento.getDataInicio()), "msg.erro.lista.proposta.dataFim.menorQue.DataInicio");
		}

		if (filtroRelatorioAcompanhamento.isSucursalSeguradoraPreenchida()) {
			validador.verdadeiro(filtroRelatorioAcompanhamento.getSucursalSeguradora().isValida(), "msg.erro.sucursal.invalida");
		}

		if (filtroRelatorioAcompanhamento.getPessoa().isCpfPreenchido()) {
			validador.cpf(filtroRelatorioAcompanhamento.getPessoa().getCpf(), "CPF do benefici�rio titular");
		}

		if (filtroRelatorioAcompanhamento.isCpfCnpjCorretorPreenchido()) {
			validador.cpfCnpj(filtroRelatorioAcompanhamento.getCorretor().getCpfCnpj(), "CPF/CNPJ Corpo");
		}

		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}
	}
}