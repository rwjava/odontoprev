package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Informações da Unidade Federativa
 * 
 * @author WDEV
 */
public class UnidadeFederativaVO implements Serializable {

	private static final long serialVersionUID = 479979214027560060L;

	private String sigla;
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	@Override
	public String toString() {
		return "UnidadeFederativaVO [sigla=" + sigla + ", nome=" + nome + "]";
	}
	

}