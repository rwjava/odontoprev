package br.com.bradseg.eedi.emissaoexpressaservicos.odontoprev.facade;

import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Map;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONPopulator;
import org.apache.struts2.json.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaCorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.RetornoVO;

/**
 * Classe responsavel por disponibilizar os metodos de neg�cio referentes a odontoprev.
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class OdontoprevServiceFacadeImpl implements OdontoprevServiceFacade {

	@Autowired
	private URL urldpssaude;
	
	/**
	 * Contexto antigo do MIP
	 */
	//private static String METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO_V2 = "/mip/api/v1/cobranca/cancelar/parcela";
	//private static String METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/mip/api/v1/captura/ticket/anular";
	//private static String METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/mip/api/v1/captura/ticket/cobrar";
	//private static String METODO_VALIDAR_DADOS_CARTAO_CREDITO = "/mip/api/v1/captura/ticket/guardar";

	/**
	 * Novo contexto de mapeamento do MIP
	 * Data de altera��o: 17/06/2019
	 */
	private static String METODO_VALIDAR_DADOS_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/guardar";
	private static String METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/cobrar";
	private static String METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO_V2 = "/BSMO-OdontoPrev/v2/mip/1.0/captura/ticket/cancelar/parcela";
	
	/**
	 * Implementado o Endpoint da Braspag para valida��o do cart�o de cr�dito
	 */
	//private static String ENDPOINT_BRASPAG_PAYMENT_TOKEN_DEV = "https://transactionsandbox.pagador.com.br/post/api/public/v1/card";
	//private static String ENDPOINT_BRASPAG_PAYMENT_TOKEN_PRD = "https://pagador.com.br/post/api/public/v1/card";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OdontoprevServiceFacadeImpl.class);

	public void cancelarDebitoPrimeiraParcelaCartaoDeCredito(PropostaVO proposta) {

		try {

			LOGGER.error("INICIANDO METODO DE CANCELAMENTO DE DEBITO 1 PARCELA");

			RetornoVO retorno = (RetornoVO) realizarChamadaOdontoprev(METODO_CANCELAR_PRIMEIRA_PARCELA_CARTAO_CREDITO_V2, RetornoVO.class, "POST", preencherParametros(proposta));

			LOGGER.error("FINALIZANDO METODO DE CANCELAMENTO DE DEBITO 1 PARCELA");

			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("Problema na comunica��o com a Odontoprev:" + retorno.getMensagem());
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DE CANCELAMENTO DE DEBITO 1 PARCELA: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}

	private Object realizarChamadaOdontoprev(String urlServico, Class<?> classe, String tipoEnvio, String parametros) {
		URL url;
		HttpURLConnection conexao;
		try {
			url = new URL(urldpssaude + urlServico);
			if(null == urldpssaude){
				url = new URL("http" + "//dpsusc.bradseg.com.br:8000" + urlServico);
			}
			conexao = (HttpURLConnection) url.openConnection();
			conexao.setRequestMethod(tipoEnvio);
			conexao.setRequestProperty("Accept", "application/json");
			if (null != parametros) {

				LOGGER.error("Parametros : " + parametros);

				conexao.setDoInput(true);
				conexao.setDoOutput(true);
				conexao.setConnectTimeout(60000);
				OutputStream os = conexao.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				writer.write(parametros);

				writer.flush();
				writer.close();
				os.close();
			}
			conexao.connect();

			if (conexao.getResponseCode() != 200) {
				LOGGER.error("ERRO NA CHAMADA: " + conexao.getResponseCode() + " / " + conexao.getResponseMessage());
				throw new BusinessException(conexao.getResponseCode() + " / " + conexao.getResponseMessage());
			}

			return deserializeJSON(inputStreamToString(conexao.getInputStream()), classe);

		} catch (MalformedURLException e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA: " + e.getMessage());
			throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
		}
	}
	
	private String preencherParametros(PropostaVO proposta) {

		StringBuilder parametros = new StringBuilder();
		//parametros.append("sistema=EEDI&").append("proposta=" + proposta.getCodigo());
		parametros.append("sistema=EEDI&").append("proposta=" + proposta.getCodigo() + "&").append("numeroParcela=001");
		return parametros.toString();
	}

	private String inputStreamToString(InputStream is) throws IOException {
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

	private Object deserializeJSON(String json, Class<?> classe) {
		Object objeto = null;
		if (json != null && !"".equals(json.trim())) {
			try {

				LOGGER.error("JSON RETORNADO: " + json);

				objeto = classe.newInstance();
				JSONPopulator jsonPopulator = new JSONPopulator();
				Map<?, ?> map = (Map<?, ?>) JSONUtil.deserialize(json);
				jsonPopulator.populateObject(objeto, map);
			} catch (IllegalAccessException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (InstantiationException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (IllegalArgumentException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (InvocationTargetException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (NoSuchMethodException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (IntrospectionException e) {
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
			} catch (JSONException e) {
				LOGGER.error("Problema na comunica��o com a Odontoprev: " + e.getMessage());
				// throw new BusinessException("Problema na comunica��o com a Odontoprev: " + e.getMessage());
				return null;
			}
		}
		return objeto;
	}

	//@Override
	public void debitarPrimeiraParcelaCartaoDeCredito(PropostaCorretorVO proposta) {
		try {

			LOGGER.error("INICANDO METODO DE DEBITO 1 PARCELA");

			StringBuilder url = new StringBuilder(METODO_DEBITAR_PRIMEIRA_PARCELA_CARTAO_CREDITO);
			
			RetornoVO retorno = (RetornoVO) realizarChamadaOdontoprev(url.toString(), RetornoVO.class, "POST",
					preencherParametros(proposta, true, false));

			LOGGER.error("FINALIZANDO METODO DE DEBITO 1 PARCELA");

			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("Problema na comunica��o com a Odontoprev:" + retorno.getMensagem());
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}
		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DEBITAR 1 PARCELA: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}

	//@Override
	public void validarDadosCartaoCredito(PropostaCorretorVO proposta) {
		try {

			LOGGER.error("INICANDO METODO DE DEBITO DEMAIS PARCELA");

			StringBuilder url = new StringBuilder(METODO_VALIDAR_DADOS_CARTAO_CREDITO);

			RetornoVO retorno = (RetornoVO) realizarChamadaOdontoprev(url.toString(), RetornoVO.class, "POST",
					preencherParametros(proposta, false, true));

			LOGGER.error("FINALIZANDO METODO DE DEBITO DEMAIS PARCELA");

			if (null == retorno || 0 == retorno.getSucesso()) {
				LOGGER.error("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
				throw new BusinessException("Problema na comunica��o com a Odontoprev: " + retorno.getMensagem());
			}

		} catch (Exception e) {
			LOGGER.error("ERRO NA CHAMADA DEBITAR DEMAIS PARCELA: " + e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}


	
	private String preencherParametros(PropostaCorretorVO proposta, boolean indicativoDebitoPrimeiraParcela,
			boolean indicativoValidacaoDados) {

		StringBuilder parametros = new StringBuilder();
		parametros.append("sistema=EEDI&").append("proposta=" + proposta.getCodigo());
		
		if (indicativoDebitoPrimeiraParcela) {
			parametros.append("&").append("paymentToken=" + proposta.getCartaoCreditoVO().getPaymentToken() + "&")
					.append("nome=" + proposta.getCartaoCreditoVO().getNomeCartao() + "&")
					.append("cpf=" + proposta.getCartaoCreditoVO().getCpfCartao() + "&")
					.append("bandeira=" + proposta.getCartaoCreditoVO().getBandeira());

			Double valor = 0.0D;
			
			if(br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca.MENSAL.equals(proposta.getTipoCobranca())){
				valor = proposta.getPlanoVO().getValorPlanoVO().getValorMensalTitular()
					* (proposta.getDependentes().size() + 1);
			}else if (br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca.ANUAL.equals(proposta.getTipoCobranca())){
				valor = proposta.getPlanoVO().getValorPlanoVO().getValorAnualTitular()
						* (proposta.getDependentes().size() + 1);
			}
			
			parametros.append("&valor=" + new DecimalFormat("#0.00").format(valor).replace(",", ""));
		}

		if (indicativoValidacaoDados) {
			parametros.append("&").append("paymentToken=" + proposta.getCartaoCreditoVO().getPaymentToken() + "&")
					.append("nome=" + proposta.getCartaoCreditoVO().getNomeCartao() + "&")
					.append("cpf=" + proposta.getCartaoCreditoVO().getCpfCartao() + "&")
					.append("bandeira=" + proposta.getCartaoCreditoVO().getBandeira());
		}

		return parametros.toString();
	}
	
}
