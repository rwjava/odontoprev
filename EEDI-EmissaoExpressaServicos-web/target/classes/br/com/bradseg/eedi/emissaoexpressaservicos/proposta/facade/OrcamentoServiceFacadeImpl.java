package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.OrcamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

import com.google.common.collect.Lists;

/**
 * Lista os or�amentos de planos de benef�cios de uma proposta
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class OrcamentoServiceFacadeImpl implements OrcamentoServiceFacade {

	@Autowired
	protected PlanoServiceFacade planoServiceFacade;

	public List<OrcamentoVO> listar(PropostaVO proposta) {
		if (proposta.isNova()) {
			// Nova proposta, ou seja, n�o possui situa��o (Rascunho, pendente, etc) e nem c�digo da proposta
			return criarOrcamentos(planoServiceFacade.listarPlanosVigentes(), proposta);
		} else {
			// Proposta j� possui um c�digo gerado, dever� listar os planos dispon�veis para ela na data de sua cria��o
			return criarOrcamentos(planoServiceFacade.listarPlanosVigentesPorData(proposta.getDataCriacao()), proposta);
		}
	}

	/**
	 * 
	 * @param planos Planos
	 * @param proposta proposta
	 * @return
	 */
	private List<OrcamentoVO> criarOrcamentos(List<PlanoVO> planos, PropostaVO proposta) {
		List<OrcamentoVO> orcamentos = Lists.newArrayList();
		for (PlanoVO planoVigente : planos) {
			orcamentos.add(new OrcamentoVO(proposta, planoVigente));
		}
		return orcamentos;
	}
}