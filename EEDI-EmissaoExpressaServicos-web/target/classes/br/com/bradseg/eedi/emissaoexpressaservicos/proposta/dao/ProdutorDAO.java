package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;
import java.util.Set;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.DefinicaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PessoaProdutor;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Descrição do DAO do ProdutorVO
 * 
 * @author WDEV
 */
public interface ProdutorDAO {

	/**
	 * Lista todos os produtores do sequencial da proposta informada
	 * 
	 * @param proposta PropostaVO
	 * @return Lista de produtores da proposta
	 */
	public List<ProdutorVO> listarPorProposta(PropostaVO proposta);

	/**
	 * Salva um produtor
	 * 
	 * @param produtor ProdutorVO de corretagem
	 */
	public void salvar(ProdutorVO produtor);

	/**
	 * Remove os produtores de uma proposta
	 *	
	 * @param definicaoProposta Definicao da Proposta
	 */
	public void removerPorProposta(DefinicaoProposta definicaoProposta);

	/**
	 * Busca o codigo pessoa do produtor, dado o seu numero
	 * 
	 * @param numeroProdutor numero do produtor
	 * @return o codigo pessoa do corretor
	 */
	public Integer buscarCodigoPessoaPorCpd(Integer numeroProdutor);

	/**
	 * Busca as informações do corretor
	 * 
	 * @param codigoPessoa Codigo da pessoa corretor
	 * @return Corretor com as informações
	 */
	public PessoaProdutor buscarInformacoesCorretorPorCodigo(Integer codigoPessoa);

	/**
	 * Busca o cpf da pessoa corretor
	 * 
	 * @param codigoPessoa Codigo da pessoa corretor
	 * @return cpf da pessoa corretor
	 */
	public Long buscarCpfPorCodigoPessoa(Integer codigoPessoa);

	/**
	 * Busca o cnpj da pessoa corretor
	 * 
	 * @param codigoPessoa Codigo da pessoa corretor
	 * @return cnpj da pessoa corretor
	 */
	public Long buscarCnpjPorCodigoPessoa(Integer codigoPessoa);

	/**
	 * Busca o cpd do corretor
	 * 
	 * @param numeroProdutor numero do produtor
	 * @return cpd do corretor
	 */
	public Integer buscarCpdCorretorPorNumeroProdutor(Integer numeroProdutor);

	/**
	 * Consulta os produtores dos sequenciais de proposta informado
	 * 
	 * @param sequenciaisProposta Sequenciais das propostas
	 * @return Produtores das propostas
	 */
	public List<ProdutorVO> listarPorSequenciaisProposta(Set<Long> sequenciaisProposta);

}