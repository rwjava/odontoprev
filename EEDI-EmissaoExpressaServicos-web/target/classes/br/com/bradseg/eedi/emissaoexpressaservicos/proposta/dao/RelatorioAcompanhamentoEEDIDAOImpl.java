package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.SucursalCorretorService;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AcompanhamentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
@Repository
public class RelatorioAcompanhamentoEEDIDAOImpl extends JdbcDao implements RelatorioAcompanhamentoEEDIDAO{

	private static final Logger LOGGER = Logger.getLogger(RelatorioAcompanhamentoEEDIDAOImpl.class);

	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private SucursalCorretorService sucursalService;
	
	
	public List<AcompanhamentoPropostaVO> listarPropostasAcompanhamento(FiltroAcompanhamentoVO filtro,  LoginVO login, boolean isIntranet){
		List<AcompanhamentoPropostaVO> listaAcompanhamento = null;
		
		try{
				
				StringBuilder sql = new StringBuilder(12000)
				.append(" select distinct    ")
				.append("  proposta.cppsta_dntal_indvd  as Codigo_Proposta,                                                                                                                                      ")
				.append("  proposta.CAG_PROTR_PLANO              AS Agencia_produtora,                                                                                                                           ")
				.append("  proposta.CIND_FORMA_PGTO     As Forma_Pagamento,                                                                                                                                      ")
				.append("  proposta.CMATR_ASSTT_PROTR As Assistente_BS,                                                                                                                                          ")
				.append("  proposta.CMATR_GER_PRODT As gerenteProdBVP,                                                                                                                                           ")
				.append("  proposta.CTPO_COBR_PLANO As tipo_cobranca,                                                                                                                                            ")
				.append("  proponente.NCPF_PROPN_DNTAL as CPF_titular_conta,                                                                                                                                     ")
				.append("  corretor.CTPO_PROTR_PPSTA       AS Tipo_Corretor,                                                                                                                                     ")
				.append("  proponente.IPROPN_DNTAL_INDVD as Nome_titular_conta,                                                                                                                                  ")
				.append(" responsavel.NCPF_RESP_LEGAL  as CPF_Responsavel,                                                                                                                                       ")
				.append(" responsavel.IRESP_LEGAL_DNTAL as Nome_responsavel,                                                                                                                                     ")
				.append(" beneficiario.NCPF_BNEFC_DNTAL as CPF_Beneficiario,                                                                                                                                     ")
				.append(" beneficiario.ibnefc_dntal_indvd as Nome_beneficiario,                                                                                                                                  ")
				.append(" case when proposta.CSUCUR_SEGDR_EMISR  is not null                                                                                                                                     ")
				.append(" then (proposta.CSUCUR_SEGDR_EMISR)                                                                                                                                                     ")
				.append(" when proposta.CSUCUR_SEGDR_BVP  is not null                                                                                                                                            ")
				.append(" then  (proposta.CSUCUR_SEGDR_BVP )                                                                                                                                                     ")
				.append(" end as Sucursal,                                                                                                                                                                       ")
				.append(" corretor.CCPD_protr cpd_corretor,                                                                                                                                                      ")
				.append(" corretor.NCPF_CNPJ_PROTR   CPF_CNPJ,                                                                                                                                                   ")
				.append(" corretor.IPROTR_PPSTA_DNTAL   Nome_corretor,                                                                                                                                           ")
				.append(" proposta.CAG_BCRIA Agencia_debito,                                                                                                                                                     ")
				.append(" proposta.NCTA_CORR Conta_debito,                                                                                                                                                       ")
				.append(" proposta.CPROT_PPSTA_CANAL_VDA as protocolo,                                                                                                                                           ")
				.append(" (select count (*) from dbprod.bnefc_dntal_indvd beneficiario where beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL ) as Quantidade_Vidas,                                     ")
				.append(" (select min(movt.dinic_movto_ppsta) from dbprod.movto_ppsta_dntal movt where  proposta.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL ) as Data_Inicio_criacao_codigo,                       ")
				.append(" (SELECT max(movimento.DINIC_MOVTO_PPSTA)  from  DBPROD.MOVTO_PPSTA_DNTAL movimento WHERE proposta.NSEQ_PPSTA_DNTAL = movimento.NSEQ_PPSTA_DNTAL ) as Data_Inicio_Status_Atual,         ")
				.append(" case      movt.CSIT_DNTAL_INDVD                                                                                                                                                        ")
				.append(" when 1 then  'PENDENTE'                                                                                                                                                                ")
				.append(" when 2 then 'RASCUNHO'                                                                                                                                                                 ")
				.append(" when 3 then 'EM_PROCESSAMENTO'                                                                                                                                                         ")
				.append(" when 4 then 'CRITICADA'                                                                                                                                                                ")
				.append(" when 5 then 'CANCELADA'                                                                                                                                                                ")
				.append(" when 6 then 'IMPLANTADA'                                                                                                                                                               ")
				.append(" when 7 then 'PRE_CANCELADA'     end AS situacao,                                                                                                                                       ")
				.append(" movt.CSIT_DNTAL_INDVD as codigo_situacao,                                                                                                                                              ")
				.append(" proposta.CCANAL_VDA_DNTAL_INDVD as CANAL_DE_VENDA,                                                                                                                                     ")
				.append(" proposta.DEMIS_PPSTA_DNTAL_INDVD as Data_Criacao,                                                                                                                                      ")
				.append(" proposta.CPLANO_DNTAL_INDVD as PLANO,                                                                                                                                                  ")
				.append(" (29.90*(select count (*) from dbprod.bnefc_dntal_indvd beneficiario where beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL)+ 5.00 *                                            ")
				.append(" (select count (*) from dbprod.bnefc_dntal_indvd beneficiario where beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL)) as Valor_Total                                           ")
				.append(" from                                                                                                                                                                                   ")
				.append(" dbprod.ppsta_dntal_indvd proposta                                                                                                                                                      ")
				.append(" left outer join  DBPROD.propn_dntal_indvd proponente on proponente.cpropn_dntal_indvd=proposta.cpropn_dntal_indvd                                                                      ")
				.append(" left outer join DBPROD.resp_legal_dntal responsavel  on  responsavel.CRESP_LEGAL_DNTAL=proposta.CRESP_LEGAL_DNTAL                                                                      ")
				.append(" left outer join DBPROD.bnefc_dntal_indvd beneficiario on (beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL and beneficiario.cbnefc_dntal_indvd=1)                              ")
				.append(" left outer join DBPROD.protr_ppsta_dntal corretor on (corretor.NSEQ_PPSTA_DNTAL=proposta.NSEQ_PPSTA_DNTAL and corretor.CTPO_PROTR_PPSTA = 2)                                           ")
				.append(" ,DBPROD.movto_ppsta_dntal movt                                                                                                                                                         ")
				.append(" where                                                                                                                                                                                  ")
				.append(" proposta.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL                                                                                                                                      ")
				.append(" and movt.dinic_movto_ppsta=(                                                                                                                                                           ")
				.append(" SELECT max(movimento.DINIC_MOVTO_PPSTA)                                                                                                                                                ")
				.append(" FROM                                                                                                                                                                                   ")
				.append(" DBPROD.MOVTO_PPSTA_DNTAL movimento                                                                                                                                                     ")
				.append(" WHERE proposta.NSEQ_PPSTA_DNTAL = movimento.NSEQ_PPSTA_DNTAL)                                                                                                                          ");
				
				
				MapSqlParameterSource params = new MapSqlParameterSource();
				
				if (filtro.getFiltroPeriodoVO().getDataInicio() != null) {
					sql.append(" AND movt.dinic_movto_ppsta  >= :dataCriacaoPropostaMenor ");
					Calendar dataInicial = Calendar.getInstance();
					dataInicial.setTime(filtro.getFiltroPeriodoVO().getDataInicio().toDateTimeAtStartOfDay().toDate());
					dataInicial.set(Calendar.HOUR, 00);
					dataInicial.set(Calendar.MINUTE, 00);
					dataInicial.set(Calendar.SECOND, 00);
					params.addValue("dataCriacaoPropostaMenor", new Timestamp(dataInicial.getTime().getTime()));
				}
				
				if (filtro.getFiltroPeriodoVO().getDataFim() != null) {
					Calendar dataFinal = Calendar.getInstance();
					dataFinal.setTime(filtro.getFiltroPeriodoVO().getDataFim().toDateTimeAtStartOfDay().toDate());
					dataFinal.set(Calendar.HOUR, 23);
					dataFinal.set(Calendar.MINUTE, 59);
					dataFinal.set(Calendar.SECOND, 59);
					sql.append(" AND movt.dinic_movto_ppsta  <= :dataCriacaoPropostaMaior ");
					params.addValue("dataCriacaoPropostaMaior", new Timestamp(dataFinal.getTime().getTime()));
				}
				
				if(filtro.getCodigoProposta() != null && StringUtils.isNotBlank(filtro.getCodigoProposta())){
					sql.append(" and proposta.CPPSTA_DNTAL_INDVD LIKE :codigoProposta ");
					params.addValue("codigoProposta", "%" + filtro.getCodigoProposta() + "%");
				}
				if(filtro.getProtocolo() != null && StringUtils.isNotBlank(filtro.getProtocolo())){
					sql.append(" and proposta.CPROT_PPSTA_CANAL_VDA = :protocolo");
					params.addValue("protocolo", filtro.getProtocolo());
				}
				
				if(filtro.getCpfBeneficiarioTitular() != null && StringUtils.isNotBlank(filtro.getCpfBeneficiarioTitular())){
					sql.append(" and beneficiario.NCPF_BNEFC_DNTAL = :cpfBenificiarioTitular ");
					Long cpfBenificiarioTitular = Long.parseLong(filtro.getCpfBeneficiarioTitular());
					params.addValue("cpfBenificiarioTitular", cpfBenificiarioTitular);
				}
				
				if(filtro.getSucursal() != null){
					
					if (sucursalService.validarSucursalRede(filtro.getSucursal().intValue())) {
						sql.append(" and proposta.CSUCUR_SEGDR_BVP = :codSucursal ");
						params.addValue("codSucursal", filtro.getSucursal());
					} else if (sucursalService.validarSucursalMercado(filtro.getSucursal().intValue())) {
						sql.append(" and proposta.CSUCUR_SEGDR_EMISR = :codSucursal ");
						params.addValue("codSucursal", filtro.getSucursal());
					} else {
						sql.append(" and (proposta.CSUCUR_SEGDR_EMISR = :codSucursal or proposta.CSUCUR_SEGDR_BVP = :codSucursal) ");
						params.addValue("codSucursal", filtro.getSucursal());
					}
				}
				
				// CPD Corretor.
				if (filtro.getCpdCorretor() != null) {
					sql.append(" and corretor.CCPD_protr = :cpdCorretor ");
					params.addValue("cpdCorretor", filtro.getCpdCorretor());
				}
				
				// C�digo Agencia de D�bito.
				if (filtro.getCodigoAgenciaDebito() != null && StringUtils.isNotBlank(filtro.getCodigoAgenciaDebito())) {
					sql.append(" and proposta.CAG_BCRIA = :codigoAgenciaDebito ");
					params.addValue("codigoAgenciaDebito", filtro.getCodigoAgenciaDebito());
				}
				
				
				// C�digo Agencia Produtora.
				if (filtro.getCodigoAgenciaProdutora() != null && StringUtils.isNotBlank(filtro.getCodigoAgenciaProdutora())) {
					sql.append(" and proposta.cag_protr_plano = :codigoAgenciaProdutora ");
					params.addValue("codigoAgenciaProdutora", filtro.getCodigoAgenciaProdutora());
				}
		
		// Status Atual.
				if (filtro.getCodigoStatusProposta() != null && filtro.getCodigoStatusProposta().intValue() > 0) {
					sql.append(" and movt.CSIT_DNTAL_INDVD = :codigoStatusAtualProposta ");
					params.addValue("codigoStatusAtualProposta", filtro.getCodigoStatusProposta());
				}
				// Gerente Produto BVP.
				if (filtro.getCodigoGerenteProdutoBVP() != null && filtro.getCodigoGerenteProdutoBVP().intValue() >= 0) {
					sql.append(" and proposta.CMATR_GER_PRODT = :codigoGerenteProdBVP ");
					params.addValue("codigoGerenteProdBVP", filtro.getCodigoGerenteProdutoBVP().toString());
				}
				// C�digo AssistenteBS.
				if (filtro.getCodigoAssistente()!= null && filtro.getCodigoAssistente() >= 0) {
					sql.append(" and proposta.cmatr_asstt_protr = :codigoAssistenteBS ");
					params.addValue("codigoAssistenteBS", String.valueOf(filtro.getCodigoAssistente()));
				}
				if (filtro.getCodigoStatusProposta() != null && !filtro.getCodigoStatusProposta().equals(0)) {
					if (filtro.getCodigoStatusProposta() == 8) {
						sql.append(" AND movt.CSIT_DNTAL_INDVD BETWEEN 1 and 8 ");
					} else {
						sql.append(" AND movt.CSIT_DNTAL_INDVD = :codigoSituacao ");
						params.addValue("codigoSituacao", filtro.getCodigoStatusProposta());
					}
				}

				if (!isIntranet) {
					if (filtro.getCpfCnpjCorretor() != null && !filtro.getCpfCnpjCorretor().equals("")) {
//						sql.append(" AND cast(corretor.NCPF_CNPJ_PROTR as char(14)) LIKE :corretorLogado ");
//						params.addValue("corretorLogado", filtro.getCpfCnpjCorretor() + "%");
						sql.append(" AND cast(corretor.NCPF_CNPJ_PROTR as char(14)) LIKE  :corretorLogado ");
						params.addValue("corretorLogado", Long.valueOf(filtro.getCpfCnpjCorretor()) + "%");
					} else if (filtro.getCpfCnpjCorretor() != null && StringUtils.isNotBlank(filtro.getCpfCnpjCorretor())){
						
						sql.append(" AND corretor.NCPF_CNPJ_PROTR = :corretorLogado ");
						params.addValue("corretorLogado", Long.valueOf(filtro.getCpfCnpjCorretor()));
					}
				}

				sql.append(" AND proposta.CPLANO_DNTAL_INDVD IS NOT NULL ");

				sql.append("order by  proposta.CPPSTA_DNTAL_INDVD ");
				LOGGER.error("QUERY DE CONSULTA DAS PROPOSTAS: " + sql.toString());
				LOGGER.error("PARAMETROS DA QUERY DE CONSULTA DAS PROPOSTAS: " + params.getValues());
				
				listaAcompanhamento = getJdbcTemplate().query(sql.toString(), params, new AcompanhamentoPropostaEEDIRowMapper());

		}catch(Exception e){
			LOGGER.error("Erro no relatorio de acompanhamento: " + e.getMessage());
		}
		
		return listaAcompanhamento;
	}

	@Override
	public DataSource getDataSource() {
		return this.dataSource;
	}
	
	
	
//	private  StringBuilder QUERY_ACOMPANHAMENTO = new StringBuilder()
	
	
	
//			.append(" 	SELECT DISTINCT                                                                                      ")
//			.append("     proposta.cppsta_dntal_indvd     AS Codigo_Proposta,                                                ")
//			.append("     proposta.CAG_PROTR_PLANO        AS Agencia_produtora,                                              ")
//			.append("     proposta.CIND_FORMA_PGTO        AS Forma_Pagamento,                                                ")
//			.append("     proposta.CMATR_ASSTT_PROTR      AS Assistente_BS,                                                  ")
//			.append("     proposta.CMATR_GER_PRODT        AS gerenteProdBVP,                                                 ")
//			.append("     proposta.CTPO_COBR_PLANO        AS tipo_cobranca,                                                  ")
//			.append("     proponente.NCPF_PROPN_DNTAL     AS CPF_titular_conta,                                              ")
//			.append("     corretor.CTPO_PROTR_PPSTA       AS Tipo_Corretor,                                                  ")
//			.append("     proponente.IPROPN_DNTAL_INDVD   AS Nome_titular_conta,                                             ")
//			.append("     responsavel.NCPF_RESP_LEGAL     AS CPF_Responsavel,                                                ")
//			.append("     responsavel.IRESP_LEGAL_DNTAL   AS Nome_responsavel,                                               ")
//			.append("     beneficiario.NCPF_BNEFC_DNTAL   AS CPF_Beneficiario,                                               ")
//			.append("     beneficiario.ibnefc_dntal_indvd AS Nome_beneficiario,                                              ")
//			.append("     CASE                                                                                               ")
//			.append("         WHEN proposta.CSUCUR_SEGDR_EMISR IS NOT NULL                                                   ")
//			.append("         THEN (proposta.CSUCUR_SEGDR_EMISR)                                                             ")
//			.append("         WHEN proposta.CSUCUR_SEGDR_BVP IS NOT NULL                                                     ")
//			.append("         THEN (proposta.CSUCUR_SEGDR_BVP )                                                              ")
//			.append("     END                            AS Sucursal,                                                        ")
//			.append("     corretor.CCPD_protr               cpd_corretor,                                                    ")
//			.append("     corretor.NCPF_CNPJ_PROTR          CPF_CNPJ,                                                        ")
//			.append("     corretor.IPROTR_PPSTA_DNTAL       Nome_corretor,                                                   ")
//			.append("     proposta.CAG_BCRIA                Agencia_debito,                                                  ")
//			.append("     proposta.NCTA_CORR                Conta_debito,                                                    ")
//			.append("     proposta.CPROT_PPSTA_CANAL_VDA AS protocolo,                                                       ")
//			.append("     (                                                                                                  ")
//			.append("         SELECT                                                                                         ")
//			.append("             COUNT (*)                                                                                  ")
//			.append("         FROM                                                                                           ")
//			.append("             dbprod.bnefc_dntal_indvd beneficiario                                                      ")
//			.append("         WHERE                                                                                          ")
//			.append("             beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL ) AS Quantidade_Vidas,            ")
//			.append("     (                                                                                                  ")
//			.append("         SELECT                                                                                         ")
//			.append("             MIN(movt.dinic_movto_ppsta)                                                                ")
//			.append("         FROM                                                                                           ")
//			.append("             dbprod.movto_ppsta_dntal movt                                                              ")
//			.append("         WHERE                                                                                          ")
//			.append("             proposta.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL ) AS Data_Inicio_criacao_codigo,         ")
//			.append("     (                                                                                                  ")
//			.append("         SELECT                                                                                         ")
//			.append("             MAX(movimento.DINIC_MOVTO_PPSTA)                                                           ")
//			.append("         FROM                                                                                           ")
//			.append("             DBPROD.MOVTO_PPSTA_DNTAL movimento                                                         ")
//			.append("         WHERE                                                                                          ")
//			.append("             proposta.NSEQ_PPSTA_DNTAL = movimento.NSEQ_PPSTA_DNTAL ) AS Data_Inicio_Status_Atual,      ")
//			.append("     CASE movt.CSIT_DNTAL_INDVD                                                                         ")
//			.append("         WHEN 1                                                                                         ")
//			.append("         THEN 'PENDENTE'                                                                                ")
//			.append("         WHEN 2                                                                                         ")
//			.append("         THEN 'RASCUNHO'                                                                                ")
//			.append("         WHEN 3                                                                                         ")
//			.append("         THEN 'EM_PROCESSAMENTO'                                                                        ")
//			.append("         WHEN 4                                                                                         ")
//			.append("         THEN 'CRITICADA'                                                                               ")
//			.append("         WHEN 5                                                                                         ")
//			.append("         THEN 'CANCELADA'                                                                               ")
//			.append("         WHEN 6                                                                                         ")
//			.append("         THEN 'IMPLANTADA'                                                                              ")
//			.append("         WHEN 7                                                                                         ")
//			.append("         THEN 'PRE_CANCELADA'                                                                           ")
//			.append("     END                              AS situacao,                                                      ")
//			.append("     movt.CSIT_DNTAL_INDVD            AS codigo_situacao,                                               ")
//			.append("     proposta.CCANAL_VDA_DNTAL_INDVD  AS CANAL_DE_VENDA,                                                ")
//			.append("     proposta.DEMIS_PPSTA_DNTAL_INDVD AS Data_Criacao,                                                  ")
//			.append("     proposta.CPLANO_DNTAL_INDVD      AS PLANO,                                                         ")
//			.append("     (29.90*                                                                                            ")
//			.append("     (                                                                                                  ")
//			.append("         SELECT                                                                                         ")
//			.append("             COUNT (*)                                                                                  ")
//			.append("         FROM                                                                                           ")
//			.append("             dbprod.bnefc_dntal_indvd beneficiario                                                      ")
//			.append("         WHERE                                                                                          ")
//			.append("             beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL)+ 5.00 *                          ")
//			.append("     (                                                                                                  ")
//			.append("         SELECT                                                                                         ")
//			.append("             COUNT (*)                                                                                  ")
//			.append("         FROM                                                                                           ")
//			.append("             dbprod.bnefc_dntal_indvd beneficiario                                                      ")
//			.append("         WHERE                                                                                          ")
//			.append("             beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL)) AS Valor_Total                  ")
//			.append(" FROM                                                                                                   ")
//			.append("     dbprod.ppsta_dntal_indvd proposta                                                                  ")
//			.append(" LEFT OUTER JOIN                                                                                        ")
//			.append("     DBPROD.propn_dntal_indvd proponente                                                                ")
//			.append(" ON                                                                                                     ")
//			.append("     proponente.cpropn_dntal_indvd=proposta.cpropn_dntal_indvd                                          ")
//			.append(" LEFT OUTER JOIN                                                                                        ")
//			.append("     DBPROD.resp_legal_dntal responsavel                                                                ")
//			.append(" ON                                                                                                     ")
//			.append("     responsavel.CRESP_LEGAL_DNTAL=proposta.CRESP_LEGAL_DNTAL                                           ")
//			.append(" LEFT OUTER JOIN                                                                                        ")
//			.append("     DBPROD.bnefc_dntal_indvd beneficiario                                                              ")
//			.append(" ON                                                                                                     ")
//			.append("     (                                                                                                  ")
//			.append("         beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL                                       ")
//			.append("     AND beneficiario.cbnefc_dntal_indvd=1)                                                             ")
//			.append(" LEFT OUTER JOIN                                                                                        ")
//			.append("     DBPROD.protr_ppsta_dntal corretor                                                                  ")
//			.append(" ON                                                                                                     ")
//			.append("     (                                                                                                  ")
//			.append("         corretor.NSEQ_PPSTA_DNTAL=proposta.NSEQ_PPSTA_DNTAL                                            ")
//			.append("     AND corretor.CTPO_PROTR_PPSTA = 2) ,                                                               ")
//			.append("     DBPROD.movto_ppsta_dntal movt                                                                      ")
//			.append(" WHERE                                                                                                  ")
//			.append("     proposta.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL                                                  ")
//			.append(" AND movt.dinic_movto_ppsta=                                                                            ")
//			.append("     (                                                                                                  ")
//			.append("         SELECT                                                                                         ")
//			.append("             MAX(movimento.DINIC_MOVTO_PPSTA)                                                           ")
//			.append("         FROM                                                                                           ")
//			.append("             DBPROD.MOVTO_PPSTA_DNTAL movimento                                                         ")
//			.append("         WHERE                                                                                          ")
//			.append("             proposta.NSEQ_PPSTA_DNTAL = movimento.NSEQ_PPSTA_DNTAL)                                    ");
}
