package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Enum contendos os tipos de registros
 * 
 * @author WDEV
 *
 */
public enum TipoRegistro {
	HEADER, FOOTER, DETALHE;
}
