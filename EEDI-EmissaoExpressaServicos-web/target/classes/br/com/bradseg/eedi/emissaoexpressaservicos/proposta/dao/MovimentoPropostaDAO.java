package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.Date;
import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;

/**
 * Servicos de acesso a dados de movimento da proposta
 * 
 * @author WDEV
 */
public interface MovimentoPropostaDAO {

	/**
	 * Salva um novo movimento da proposta
	 * 
	 * @param movimentoProposta Movimento da proposta
	 * @return Movimento salvo da proposta
	 */
	public MovimentoPropostaVO salvar(MovimentoPropostaVO movimentoProposta);

	/**
	 * Salva um novo movimento da proposta
	 * 
	 * @param movimentoProposta Movimento da proposta
	 * @param responsavelAtualizacao Respons�vel pela atualiza��o
	 * @return Movimento salvo da proposta
	 */
	public MovimentoPropostaVO salvar(MovimentoPropostaVO movimentoProposta, String responsavelAtualizacao);
	
	/**
	 * Consulta o �ltimop movimento da proposta
	 * 
	 * @param numero sequencial 
	 * @return �ltima situa��o da proposta
	 */
	public MovimentoPropostaVO consultarUltimoMovimento(Long numeroSequencial);
	
	public List<MovimentoPropostaVO> obterListaDeMovimentosDeProposta(Long numeroSequencial);
	
	public boolean verificarSePossuiMovimentoDeCancelamento(Long numeroSequencial);

	/**
	 * Obter data de primeiro movimento da proposta
	 * @param numeroSequencial
	 * @return
	 */
	public Date obterDataPrimeiroMovimentoProposta(Long numeroSequencial);
	
	/**
	 * Metodo responsavel por verificar se proposta possui registro com o movimento informado.
	 * 
	 * @param numeroSequencial - sequencial da proposta informada.
	 * @param situacao - situacao da proposta.
	 * @return boolean - true se possui movimento, false se nao possui movimento.
	 */
	public boolean verificarSePossuiMovimentoInformado(Long numeroSequencial, SituacaoProposta situacao);
	
	public Integer obterUltimoCodigoMovimentoPorSequencial(Long sequencialProposta);

	/**
	 * 
	 * @param sequencialProposta
	 * @return
	 */
	public List<MovimentoPropostaVO> listarMovimentoProposta(Long sequencialProposta);

}
