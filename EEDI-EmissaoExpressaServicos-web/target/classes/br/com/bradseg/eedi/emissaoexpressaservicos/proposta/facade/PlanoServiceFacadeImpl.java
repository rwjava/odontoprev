package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.CanalVendaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.PlanoDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.util.ProdutoEnum;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.vo.RetornoObterArquivoVo;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoAnsVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoSegmtVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoSegmento;

import com.google.common.collect.Lists;

/**
 * Lista os planos de benef�cios de uma proposta
 * 
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class PlanoServiceFacadeImpl implements PlanoServiceFacade {

	private static final Logger LOGGER = Logger.getLogger(PlanoServiceFacadeImpl.class);

	@Autowired
	protected PlanoDAO planoDAO;

	@Autowired
	protected CanalVendaDAO canalVendaDAO;	

	@Autowired
	protected transient ApplicationContext applicationContext;
	
	public List<PlanoVO> listarPlanosVigentesPorData(LocalDate dataCriacao) {
		List<PlanoVO> planosVigentes = Lists.newArrayList();

//		for (PlanoVO plano : planoDAO.listar()) {
//			if (plano.isVigente(dataCriacao)) {
//				planosVigentes.add(plano);
//			}
//		}

		return planosVigentes;
	}

	public List<PlanoVO> listarPlanosVigentes() {
		return listarPlanosVigentesPorData(new LocalDate());
	}

	public PlanoVO buscarPlanoPorCodigo(Integer codigoPlano) {
		
		return planoDAO.buscaPlanoPorCodigo(codigoPlano);
		
	}
	
	public PlanoVO obterPlanoAtualPorCodigoCanal(Integer codigo){
		
		return planoDAO.obterPlanoAtualPorCodigoCanal(codigo);
	}
	
	/**
	 * Listar todos os planos vigente associados a um determinado canal.
	 * 
	 * @param codigoCanal - c�digo do canal.
	 * 
	 * @return List<PlanoVO> - lista de planos vigente associados ao canal informado.
	 */
	public List<PlanoVO> listarPlanosVigentePorCanal(Integer codigoCanal){
		List<PlanoVO> listaDePlanos = null;
		
		if(codigoCanal == null || 0 == codigoCanal.intValue()){
			throw new BusinessException("O c�digo do canal � obrigat�rio.");
		}
		
		listaDePlanos = planoDAO.listarPlanosVigentePorCanal(codigoCanal);
		
		if(listaDePlanos == null || listaDePlanos.isEmpty()){
			throw new BusinessException("Nenhum plano vigente foi encontrado para o canal informado.");
		}
		
		return listaDePlanos;
	}
	
	public List<PlanoAnsVO> listarPlanosVigentesPorCanalESegmento(Integer codigoCanal ,Integer codigoSegmento){
		List<PlanoAnsVO> listaDePlanos = null;
		
		List<PlanoAnsVO> listaDeSegmentoECanal = null;
		
		if(codigoCanal == null || 0 == codigoCanal.intValue()){
			throw new BusinessException("O c�digo do canal � obrigat�rio.");
		}
		
		if(codigoSegmento == null || 0 == codigoSegmento.intValue()){
			listaDePlanos = planoDAO.listarPlanosVigentePorCanalPlanoAns(codigoCanal);	
			if(listaDePlanos == null || listaDePlanos.isEmpty()){
				throw new BusinessException("Nenhum plano vigente foi encontrado para o canal informado.");
			}
			return listaDePlanos;
		}
		listaDeSegmentoECanal = planoDAO.listarPlanosVigentesPorCanalESegmento(codigoCanal, codigoSegmento);
		
		if(listaDeSegmentoECanal == null || listaDeSegmentoECanal.isEmpty()){
			
			throw new BusinessException("Nenhum plano vigente foi encontrado para o segmento informado.");
		}
		return listaDeSegmentoECanal;
	}
	
	/**
	 * Listar os planos conforme par�metro de canal e segmento da conta corrente do banco.
	 * 
	 * @param codigoCanal - c�digo do canal.
	 * 
	 * @param codigoCanal - c�digo do segmento.
	 * 
	 * @return List<PlanoSegmtVO> - lista de planos vigente conforme par�metro de canal e segmento da conta corrente do banco.
	 */
	public List<PlanoSegmtVO> listarPlanosVigentesPorCanalESegmentoBanco(Integer codigoCanal, Integer codigoSegmento){
		
		List<PlanoAnsVO> listaDePlanos = null;
		List<PlanoSegmtVO> listaDeSegmentoECanal = null;
		
		
		if((codigoCanal == null || 0 == codigoCanal.intValue()) && (codigoSegmento == null || 0 == codigoSegmento.intValue())){
			throw new BusinessException("O c�digo do canal e c�digo do segmento s�o obrigat�rios.");
		}
		
		if(codigoCanal == null || 0 == codigoCanal.intValue()){
			throw new BusinessException("O c�digo do canal � obrigat�rio.");
		}
		
		if(codigoSegmento == null || 0 == codigoSegmento.intValue()){
			throw new BusinessException("O c�digo do segmento � obrigat�rio.");
		}
		
		TipoSegmento segmento = TipoSegmento.buscaPor(codigoSegmento); 
		if(segmento == null){
			throw new BusinessException("Segmento do banco inexistente.");
		}
		
		CanalVendaVO canalVendaVO = canalVendaDAO.consultarPorCodigo(codigoCanal);
		if(canalVendaVO == null){
			throw new BusinessException("Canal inexistente.");
		}
		
		listaDePlanos = planoDAO.listarPlanosVigentePorCanalPlanoAns(codigoCanal);	
		if(listaDePlanos == null || listaDePlanos.isEmpty()){
			throw new BusinessException("Nenhum plano vigente foi encontrado para o canal informado.");
		}		
		
		listaDeSegmentoECanal = planoDAO.listarPlanosVigentesPorCanalESegmentoBanco(codigoCanal, codigoSegmento);
		
		if(listaDeSegmentoECanal == null || listaDeSegmentoECanal.isEmpty()){
			
			throw new BusinessException("Nenhum plano vigente foi encontrado para o segmento de banco informado.");
		}
		return listaDeSegmentoECanal;
	}
	
	
	/**
	 * Obter as informa��es de um plano atrav�s do c�digo.
	 * 
	 * @param codigoPlano - c�digo do plano a ser obtido.
	 * @return PlanoVO - informa��es do plano obtido.
	 */
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano){
		PlanoVO planoVO = null;
		
		if(codigoPlano == null || 0 == codigoPlano.intValue()){
			throw new BusinessException("O c�digo do plano � obrigat�rio.");
		}
		
		planoVO = planoDAO.obterPlanoPorCodigo(codigoPlano);
		
		if(planoVO == null){
			throw new BusinessException("Nenhum plano foi encontrado com o c�digo informado.");
		}
		
		return planoVO;
	}
	public PlanoAnsVO obterPlanoAnsPorCodigo(Long codigoPlano){
		PlanoAnsVO planoVO = null;
		
		if(codigoPlano == null || 0 == codigoPlano.intValue()){
			throw new BusinessException("O c�digo do plano � obrigat�rio.");
		}
		
		planoVO = planoDAO.obterPlanoAnsPorCodigo(codigoPlano);
		
		if(planoVO == null){
			throw new BusinessException("Nenhum plano foi encontrado com o c�digo informado.");
		}
		
		return planoVO;
	}
	
	public List<PlanoVO> listarPlanoDisponivelParaVenda(Integer codigoSegmento){
		return planoDAO.listarPlanoDisponivelParaVenda(codigoSegmento);
	}
	
	public RetornoObterArquivoVo gerarArquivoProduto(Integer codigoCanal, Integer codigoPlano, Integer codigoDocumento) {
		RetornoObterArquivoVo retorno = new RetornoObterArquivoVo();
		
		try{
			
			if(codigoPlano == 0){
				retorno.setCodigoErro(1);
				retorno.setDescricaoErro("O campo codigoPlano � de preenchimento obrigat�rio.");
				return retorno;
			}
			
			if(codigoCanal == 0){
				retorno.setCodigoErro(2);
				retorno.setDescricaoErro("O campo codigoCanal � de preenchimento obrigat�rio.");
				return retorno;
			}
			
			if(codigoDocumento == 0){
				retorno.setCodigoErro(3);
				retorno.setDescricaoErro("O campo codigoDocumento � de preenchimento obrigat�rio.");
				return retorno;
			}
			
			if(!planoDAO.isPlanoVigente(codigoPlano)){
				retorno.setCodigoErro(4);
				retorno.setDescricaoErro("Plano inexistente.");
				return retorno;
			}
			
			if(canalVendaDAO.consultarPorCodigo(codigoCanal) == null){
				retorno.setCodigoErro(5);
				retorno.setDescricaoErro("Canal inexistente.");
				return retorno;
			}
			
			ProdutoEnum obterArquivoPdf = ProdutoEnum.obterArquivoPdf(codigoPlano, codigoDocumento);
			
			if(obterArquivoPdf != null){
				String path = getResourcePath("WEB-INF/pdf/")+obterArquivoPdf.getNomeArquivo();
				
				retorno.setCodigoErro(0);
				byte[] encodeBase64 = Base64.encodeBase64(IOUtils.toByteArray(new FileInputStream(new File(path))));
				retorno.setBytePdf(Base64.decodeBase64(encodeBase64));
			}else{
				retorno.setCodigoErro(6);
				retorno.setDescricaoErro("Nenhum PDF foi encontrado para o plano/canal informado.");
				return retorno;
			}
			
			
			
		}catch(Exception e){
			LOGGER.error("Erro no processo de busca do documento  - " +e);
			retorno.setCodigoErro(7);
			retorno.setDescricaoErro("Ocorreu um erro inesperado, favor tente novamente mais tarde.");
			return retorno;
		}
			
		return retorno;
	}	

	
	private String getResourcePath(String resourcePath) {
		try {
			Resource resource = applicationContext.getResource(resourcePath);
			if (!resource.exists()) {
				throw new IntegrationException("O path '" + resourcePath + "' n�o foi encontrado");
			}
			return resource.getFile().getPath();
		} catch (IOException e) {
			LOGGER.error("INFO: Erro ao recuperar caminho informado", e);
			throw new IntegrationException(e);
		}
	}
}
