package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;

public interface ConsultaPropostaDao {

	public List<List<String>> realizarConsulta(String consulta);
}
