package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * Dados do movimento da proposta
 * 
 * @author WDEV
 */
public class MovimentoPropostaVO implements Serializable {

	private static final long serialVersionUID = -1983199479296477095L;

	private SituacaoProposta situacao;

	private DateTime dataInicio;

	private PropostaVO proposta;

	private String descricao;

	public SituacaoProposta getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoProposta situacao) {
		this.situacao = situacao;
	}

	@XmlJavaTypeAdapter(type = org.joda.time.DateTime.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DateTimeAdapter.class)
	public DateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(DateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public PropostaVO getProposta() {
		return proposta;
	}

	public void setProposta(PropostaVO proposta) {
		this.proposta = proposta;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public boolean isDescricaoPreenchida() {
		return StringUtils.isNotBlank(descricao);
	}

	@Override
	public String toString() {
		return "MovimentoPropostaVO [situacao=" + situacao + ", dataInicio=" + dataInicio + ", proposta=" + proposta
				+ ", descricao=" + descricao + "]";
	}
	
	

}