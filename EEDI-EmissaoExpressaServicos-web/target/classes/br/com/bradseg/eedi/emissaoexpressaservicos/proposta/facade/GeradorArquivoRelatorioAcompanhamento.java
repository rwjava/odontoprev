package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Arquivo;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AngariadorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorMasterVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ItemRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Gera um arquivo CSV de relat�rio de acompanhamento
 * @author WDEV
 */
public class GeradorArquivoRelatorioAcompanhamento {

	private List<ItemRelatorioAcompanhamentoVO> itensRelatorioAcompanhamento;

	public GeradorArquivoRelatorioAcompanhamento(List<ItemRelatorioAcompanhamentoVO> itensRelatorioAcompanhamento) {
		this.itensRelatorioAcompanhamento = itensRelatorioAcompanhamento;
	}

	public Arquivo gerar(String nomeArquivo) {
		Arquivo arquivo = new Arquivo();
		arquivo.setSeparador(";");
		arquivo.setNome(nomeArquivo);
		new Cabecalho().escrever(arquivo);
		for (ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento : itensRelatorioAcompanhamento) {
			new Detalhe(itemRelatorioAcompanhamento).escrever(arquivo);
		}
		return arquivo;
	}

	/**
	 * Cabe�alho do arquivo de relat�rio de acompanhamento
	 * @author WDEV
	 */
	protected static final class Cabecalho {
		public void escrever(Arquivo arquivo) {
			//@formatter:off
			arquivo
				.texto("C�digo Proposta")
				.texto("CPF Titular da Conta")
				.texto("Nome do Titular da Conta")
				.texto("CPF do Respons�vel")
				.texto("Nome do Respons�vel")
				.texto("CPF Benefici�rio Titular")
				.texto("Nome Benefici�rio Titular")
				.texto("Sucursal")
				.texto("CPD do Corretor")
				.texto("CNPJ/CPF do Corretor Principal")
				.texto("Nome do Corretor")
				.texto("CPD do Angariador")
				.texto("CNPJ/CPF do Angariador")
				.texto("Nome do Angariador")
				.texto("CPD do Corretor Master")
				.texto("CNPJ/CPF do Corretor Master")
				.texto("Nome do Corretor Master")
				.texto("C�digo Ag�ncia de D�bito")
				.texto("Nome Ag�ncia de D�bito")
				.texto("C�digo Ag�ncia Produtora")
				.texto("Nome Ag�ncia Produtora")
				.texto("C�digo Assistente BS")
				.texto("C�digo Gerente Produto BVP")
				.texto("Valor Total da Proposta")
				.texto("Quantidade de Vidas da Proposta")
				.texto("Data da Cria��o do C�digo da Proposta")
				.texto("Data do In�cio do Status Atual da Proposta")
				.texto("Status Atual da Proposta")
				.texto("Motivo Status Proposta")
				.quebraLinha();
			//@formatter:on
		}
	}

	/**
	 * Detalhes de cada linha do arquivo de relat�rio de acompanhamento
	 * @author WDEV
	 */
	protected static final class Detalhe {
		private ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento;

		public Detalhe(ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamento) {
			this.itemRelatorioAcompanhamento = itemRelatorioAcompanhamento;
		}

		public void escrever(Arquivo arquivo) {
			PropostaVO proposta = itemRelatorioAcompanhamento.getProposta();

			//@formatter:off
			arquivo
				.texto(proposta.getCodigo())
				.texto(proposta.getProponente().getCpf())
				.texto(proposta.getProponente().getNome())
				.texto(proposta.getResponsavelLegal().getCpf())
				.texto(proposta.getResponsavelLegal().getNome())
				.texto(proposta.getTitular().getCpf())
				.texto(proposta.getTitular().getNome())
				.numero(proposta.getSucursalSeguradora().getCodigo());
			
			ProdutorVO corretor = proposta.getCorretor();
			if (corretor == null) {
				corretor = new CorretorVO();
			}
			
			arquivo.numero(corretor.getCpd())
				.texto(corretor.getCpfCnpj())
				.texto(corretor.getNome());
				
			ProdutorVO angariador = proposta.getAngariador();
			if (angariador == null) {
				angariador = new AngariadorVO();
			}
			
			arquivo.numero(angariador.getCpd())
				.texto(angariador.getCpfCnpj())
				.texto(angariador.getNome());
			
			ProdutorVO corretorMaster = proposta.getCorretorMaster();
			if (corretorMaster == null) {
				corretorMaster = new CorretorMasterVO();
			}
			
			arquivo.numero(corretorMaster.getCpd())
				.texto(corretorMaster.getCpfCnpj())
				.texto(corretorMaster.getNome());
			
			arquivo
				.numero(proposta.getProponente().getContaCorrente().getAgenciaBancaria().getCodigo())
				.texto(proposta.getProponente().getContaCorrente().getAgenciaBancaria().getNome())
				.numero(proposta.getAgenciaProdutora().getCodigo())
				.texto(proposta.getAgenciaProdutora().getNome())
				.texto(proposta.getCodigoMatriculaAssistente())
				.texto(proposta.getCodigoMatriculaGerente())
				.numero(itemRelatorioAcompanhamento.getOrcamento().getValorTotalComTaxaDeAdesao())
				.numero(itemRelatorioAcompanhamento.getQuantidadeVidas())
				.data(proposta.getDataCriacao(), "yyyy-MM-dd")
				.data(proposta.getMovimento().getDataInicio(), "yyyy-MM-dd")
				.texto(proposta.getMovimento().getSituacao().name())
				.texto(proposta.getMovimento().getDescricao())
				.quebraLinha();
			//@formatter:on
		}
	}

}