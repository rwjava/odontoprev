package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.bradseg.bsad.framework.core.text.formatter.MaskFormatter;

/**
 * Classe utilit�ria para formata��o de dados.
 * 
 * @author EEDI
 */
public class FormatacaoUtil {

	/**
	 * formata data para o padrao (dd/MM/yyyy).
	 * 
	 * @param data the data
	 * @return the string
	 */
	public synchronized static String formataData(Date data) {
		return new SimpleDateFormat("dd/MM/yyyy").format(data);
	}

	/**
	 * Formata data para o padrao (ex: dd/MM/yyyy HH:mm:ss).
	 * 
	 * @param data Data a formatar
	 * @param formato Formato esperado.
	 * @return Data formatada em String.
	 */
	public static String formataData(Date data, String formato) {
		SimpleDateFormat fmt = new SimpleDateFormat(formato);
		return fmt.format(data);
	}

	/**
	 * Formata string.
	 * 
	 * @param valor String a ser formatada
	 * @param tamanhoTotal Tamanho total.
	 * @return String formatada
	 */
	public static String formataString(String valor, int tamanhoTotal) {
		if (valor == null) {
			valor = " ";
		}
		return String.format("%-" + tamanhoTotal + "." + tamanhoTotal + "s", valor);
	}

	/**
	 * Formata string.
	 * 
	 * @param valorNumerico Valor num�rico.
	 * @param tamanhoTotal Tamanho total
	 * @return String formatada
	 */
	public static String formataString(Number valorNumerico, int tamanhoTotal) {
		String valor = "";
		if (valorNumerico != null) {
			valor = valorNumerico.toString();
		}
		return String.format("%-" + tamanhoTotal + "." + valor.length() + "s", valor);
	}

	/**
	 * Formata numerico.
	 * 
	 * @param valor Valor a ser formatado
	 * @param tamanho Tamanho total
	 * @return Valor formatado
	 */
	public static String formataNumerico(String valor, Integer tamanho) {
		String retiraCaracteres = "0" + valor.replaceAll("[^0-9]", "");
		return formataNumerico(Long.parseLong(retiraCaracteres), tamanho);
	}

	/**
	 * Formata numerico.
	 * 
	 * @param valor Valor a ser formatado
	 * @param tamanho Tamanho total
	 * @return String formatada
	 */
	public static String formataNumerico(Number valor, Integer tamanho) {
		return String.format("%0" + tamanho + "d", valor);
	}

	/**
	 * Formata codigo da proposta. no formato <b>'***##############-#'</b>
	 * 
	 * @param codigoProposta the codigo proposta
	 * @return the string
	 */
	public static String formataCodigoDaProposta(String codigoProposta) {
		MaskFormatter mf = new MaskFormatter("***###########-#", String.class);
		mf.setValueContainsLiteralCharacters(false);

		return mf.valueToString(codigoProposta);
	}

	/**
	 * Formata telefone no formato <b>'####-#####'</b>
	 * 
	 * @param numeroTelefone the numero telefone
	 * @return the string
	 */
	public static String formataTelefone(Integer numeroTelefone) {
		MaskFormatter mf = new MaskFormatter("####-#####", Integer.class);
		mf.setValueContainsLiteralCharacters(false);

		return mf.valueToString(numeroTelefone);
	}

	/**
	 * Formata cep no padr�o <b>'#####-###'</b>
	 * 
	 * @param cep the cep
	 * @return the string
	 */
	public static String formataCEP(Integer cep) {
		MaskFormatter mf = new MaskFormatter("#####-###", Integer.class);
		mf.setValueContainsLiteralCharacters(false);

		return mf.valueToString(cep);
	}

	/**
	 * Formata cep relatorio.
	 * 
	 * @param cep the cep
	 * @return the string
	 */
	public static String formataCEPRelatorio(Integer cep) {
		String cepFormatado = formataNumerico(cep, 8);

		MaskFormatter mf = new MaskFormatter("#####-###", String.class);
		mf.setValueContainsLiteralCharacters(false);

		return mf.valueToString(cepFormatado);
	}

	/**
	 * Formata cpf no padrao <b>'###.###.###-##'</b>
	 * 
	 * @param cpf the cpf
	 * @return the string
	 */
	public static String formataCPF(String cpf) {
		if (cpf == null) {
			return null;
		}
		if (cpf.length() < 11) {
			cpf = formataNumerico(cpf, 11);
		}
		MaskFormatter mf = new MaskFormatter("###.###.###-##", String.class);
		mf.setValueContainsLiteralCharacters(false);

		return mf.valueToString(cpf);
	}

	/**
	 * Formata cnpj no padrao <b>'##.###.###/####-##'</b>
	 * 
	 * @param cnpj the cnpj
	 * @return the string
	 */
	public static String formataCNPJ(String cnpj) {
		if (cnpj == null) {
			return null;
		}
		if (cnpj.length() < 14) {
			cnpj = formataNumerico(cnpj, 14);
		}
		MaskFormatter mf = new MaskFormatter("##.###.###/####-##", String.class);
		mf.setValueContainsLiteralCharacters(false);

		return mf.valueToString(cnpj);
	}

	/**
	 * Formata data <b>'dd/MM/yyyy'</b>
	 * 
	 * @param data the data
	 * @return the string
	 */
	public static String formataDataInvertida(String data) {
		if (data == null) {
			return null;
		}
		String ano = data.substring(0, 4);
		String mes = data.substring(5, 7);
		String dia = data.substring(8, 10);
		return dia + "/" + mes + "/" + ano;
	}

	public static String formataValor(Double valor) {
		NumberFormat nf = NumberFormat.getInstance(new Locale("pt", "BR"));
		nf.setMinimumFractionDigits(2);
		return "R$ " + nf.format(valor);
	}
}
