package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.util.TipoComissaoMercadoCorporate;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.util.TipoComissaoRedeCorretorMaster;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.util.TipoComissaoRedeGerenteProdutor;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoSucursalSeguradora;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TipoComissaoServiceFacadeImpl implements TipoComissaoServiceFacade {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TipoComissaoServiceFacadeImpl.class);
	
	public Integer obterTipoComissao(PropostaVO proposta) {
		Integer tipoComissao = null;
		boolean corretorNovaComissao = false;
		
		if(proposta.getCorretor().getCpfCnpj().equals("24878265000155") || proposta.getCorretor().getCpfCnpj().equals("19914513000136")){
			corretorNovaComissao = true;
		}
		
		try {
			if (TipoSucursalSeguradora.EMISSORA.equals(proposta.getSucursalSeguradora().getTipo()) || TipoSucursalSeguradora.CORPORATE.equals(proposta.getSucursalSeguradora().getTipo())) {
				tipoComissao = new TipoComissaoMercadoCorporate().obterTipoComissao(proposta.getTipoCobranca().getCodigo(), corretorNovaComissao);
				
			} else if (TipoSucursalSeguradora.BVP.equals(proposta.getSucursalSeguradora().getTipo())) {
				if (proposta.getCorretorMaster() != null && proposta.getCorretorMaster().getCpd() != null && proposta.getCorretorMaster().getCpd() > 0L) {
					tipoComissao =  new TipoComissaoRedeCorretorMaster().obterTipoComissao(proposta.getTipoCobranca().getCodigo(),corretorNovaComissao);
				} else {
					tipoComissao =  new TipoComissaoRedeGerenteProdutor().obterTipoComissao(proposta.getTipoCobranca().getCodigo());
				}
			}	
		} catch (Exception e) {
			LOGGER.error("Tipo Comiss�o " + e.getMessage());
		}

		return tipoComissao;
	}

}
