package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AcompanhamentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;

public interface RelatorioAcompanhamentoEEDIDAO {

	public  List<AcompanhamentoPropostaVO> listarPropostasAcompanhamento(FiltroAcompanhamentoVO filtro,  LoginVO login, boolean isIntranet);

}
