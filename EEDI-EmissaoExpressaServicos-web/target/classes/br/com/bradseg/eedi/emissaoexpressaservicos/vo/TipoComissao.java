package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Valores permitidos para os tipos de comiss�o 
 * 
 * @author WDEV
 */
public enum TipoComissao {

	COMISSAO_NORMAL(1), 
	COMISSAO_CORRETOR_MASTER_BVP(2), 
	COMISSAO_GERENTE_BVP(3), 
	COMISSAO_TIPO4(4), 
	COMISSAO_CALL_CENTER(8), 
	COMISSAO_FAMILIA_BRADESCO(8), 
	COMISSAO_INTERNET_BANKING(11),
	COMISSAO_SHOPPING_SEGUROS(11); //tipo de comissao 11 para shopping de seguros

	private Integer codigo;

	private TipoComissao(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public static TipoComissao obterPorCodigo(Integer codigo) {
		for (TipoComissao tipo : TipoComissao.values()) {
			if (tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}
		return null;
	}

}