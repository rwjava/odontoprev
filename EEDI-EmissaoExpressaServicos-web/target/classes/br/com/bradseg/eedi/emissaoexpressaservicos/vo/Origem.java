package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

public enum Origem {

	SITE_100_CORRETOR, CALL_CENTER, FAMILIA_BRADESCO, INTERNET_BANKING, WORKSITE_CALL, APP_RODOBENS, APP_BANCO, APP_CORRETOR; 

}