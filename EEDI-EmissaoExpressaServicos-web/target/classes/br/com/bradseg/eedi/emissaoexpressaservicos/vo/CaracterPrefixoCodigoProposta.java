package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Caracteres permitidos na composi��o do prefixo do c�digo da proposta. Exemplo: BDA
 * 
 * @author WDEV
 */
public enum CaracterPrefixoCodigoProposta {

	A(1), B(2), C(3), D(4), L(12), O(15), P(16);

	private Integer codigo;

	private CaracterPrefixoCodigoProposta(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public static CaracterPrefixoCodigoProposta valueOf(char caracter) {
		return valueOf(Character.toString(caracter));
	}

}