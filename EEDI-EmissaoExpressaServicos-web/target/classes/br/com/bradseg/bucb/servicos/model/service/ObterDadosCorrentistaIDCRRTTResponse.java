
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.PessoaContaCorrenteVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="obterDadosCorrentistaIDCRRTTReturn" type="{http://vo.model.servicos.bucb.bradseg.com.br}PessoaContaCorrenteVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "obterDadosCorrentistaIDCRRTTReturn"
})
@XmlRootElement(name = "obterDadosCorrentistaIDCRRTTResponse")
public class ObterDadosCorrentistaIDCRRTTResponse {

    @XmlElement(required = true, nillable = true)
    protected PessoaContaCorrenteVO obterDadosCorrentistaIDCRRTTReturn;

    /**
     * Gets the value of the obterDadosCorrentistaIDCRRTTReturn property.
     * 
     * @return
     *     possible object is
     *     {@link PessoaContaCorrenteVO }
     *     
     */
    public PessoaContaCorrenteVO getObterDadosCorrentistaIDCRRTTReturn() {
        return obterDadosCorrentistaIDCRRTTReturn;
    }

    /**
     * Sets the value of the obterDadosCorrentistaIDCRRTTReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link PessoaContaCorrenteVO }
     *     
     */
    public void setObterDadosCorrentistaIDCRRTTReturn(PessoaContaCorrenteVO value) {
        this.obterDadosCorrentistaIDCRRTTReturn = value;
    }

}
