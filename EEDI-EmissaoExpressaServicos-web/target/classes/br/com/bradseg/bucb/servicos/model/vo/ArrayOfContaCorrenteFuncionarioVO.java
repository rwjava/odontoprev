package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ArrayOfContaCorrenteFuncionarioVO complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfContaCorrenteFuncionarioVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContaCorrenteFuncionarioVO" type="{http://vo.model.servicos.bucb.bradseg.com.br}ContaCorrenteFuncionarioVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfContaCorrenteFuncionarioVO", propOrder = { "contaCorrenteFuncionarioVO" })
public class ArrayOfContaCorrenteFuncionarioVO implements Serializable {

	private static final long serialVersionUID = -6884688812079372659L;
	@XmlElement(name = "ContaCorrenteFuncionarioVO", nillable = true)
	protected List<ContaCorrenteFuncionarioVO> contaCorrenteFuncionarioVO;

	/**
	 * Gets the value of the contaCorrenteFuncionarioVO property.
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
	 * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
	 * the contaCorrenteFuncionarioVO property.
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getContaCorrenteFuncionarioVO().add(newItem);
	 * </pre>
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link ContaCorrenteFuncionarioVO }
	 */
	public List<ContaCorrenteFuncionarioVO> getContaCorrenteFuncionarioVO() {
		if (contaCorrenteFuncionarioVO == null) {
			contaCorrenteFuncionarioVO = new ArrayList<ContaCorrenteFuncionarioVO>();
		}
		return this.contaCorrenteFuncionarioVO;
	}

}
