
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listarCorrentistasPorCpfCnpjReturn" type="{http://service.model.servicos.bucb.bradseg.com.br}ArrayOf_1799424901_nillable_ContaCorrentePorCPFCNPJVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listarCorrentistasPorCpfCnpjReturn"
})
@XmlRootElement(name = "listarCorrentistasPorCpfCnpjResponse")
public class ListarCorrentistasPorCpfCnpjResponse {

    @XmlElement(required = true, nillable = true)
    protected ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO listarCorrentistasPorCpfCnpjReturn;

    /**
     * Gets the value of the listarCorrentistasPorCpfCnpjReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO }
     *     
     */
    public ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO getListarCorrentistasPorCpfCnpjReturn() {
        return listarCorrentistasPorCpfCnpjReturn;
    }

    /**
     * Sets the value of the listarCorrentistasPorCpfCnpjReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO }
     *     
     */
    public void setListarCorrentistasPorCpfCnpjReturn(ArrayOf1799424901NillableContaCorrentePorCPFCNPJVO value) {
        this.listarCorrentistasPorCpfCnpjReturn = value;
    }

}
