
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListarCorrentistasPorAgCcVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListarCorrentistasPorAgCcVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="contaCorrente" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="opcaoTitular" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tpConta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dvConta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListarCorrentistasPorAgCcVO", propOrder = {
    "agencia",
    "contaCorrente",
    "opcaoTitular",
    "tpConta",
    "dvConta"
})
public class ListarCorrentistasPorAgCcVO {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer agencia;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long contaCorrente;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer opcaoTitular;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tpConta;
    @XmlElement(required = true, nillable = true)
    protected String dvConta;

    /**
     * Gets the value of the agencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAgencia() {
        return agencia;
    }

    /**
     * Sets the value of the agencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAgencia(Integer value) {
        this.agencia = value;
    }

    /**
     * Gets the value of the contaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContaCorrente() {
        return contaCorrente;
    }

    /**
     * Sets the value of the contaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContaCorrente(Long value) {
        this.contaCorrente = value;
    }

    /**
     * Gets the value of the opcaoTitular property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOpcaoTitular() {
        return opcaoTitular;
    }

    /**
     * Sets the value of the opcaoTitular property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOpcaoTitular(Integer value) {
        this.opcaoTitular = value;
    }

    /**
     * Gets the value of the tpConta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTpConta() {
        return tpConta;
    }

    /**
     * Sets the value of the tpConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTpConta(Integer value) {
        this.tpConta = value;
    }

    /**
     * Gets the value of the dvConta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvConta() {
        return dvConta;
    }

    /**
     * Sets the value of the dvConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvConta(String value) {
        this.dvConta = value;
    }

}
