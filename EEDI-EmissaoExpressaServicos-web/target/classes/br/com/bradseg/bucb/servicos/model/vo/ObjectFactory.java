
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.bucb.servicos.model.vo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.bucb.servicos.model.vo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListarCorrentistasPorAgCcSaidaVO }
     * 
     */
    public ListarCorrentistasPorAgCcSaidaVO createListarCorrentistasPorAgCcSaidaVO() {
        return new ListarCorrentistasPorAgCcSaidaVO();
    }

    /**
     * Create an instance of {@link ContaCorrenteFuncionarioVO }
     * 
     */
    public ContaCorrenteFuncionarioVO createContaCorrenteFuncionarioVO() {
        return new ContaCorrenteFuncionarioVO();
    }

    /**
     * Create an instance of {@link ListaContaCorrenteFuncionarioWsVO }
     * 
     */
    public ListaContaCorrenteFuncionarioWsVO createListaContaCorrenteFuncionarioWsVO() {
        return new ListaContaCorrenteFuncionarioWsVO();
    }

    /**
     * Create an instance of {@link ValidarDadosContaCorrenteVO }
     * 
     */
    public ValidarDadosContaCorrenteVO createValidarDadosContaCorrenteVO() {
        return new ValidarDadosContaCorrenteVO();
    }

    /**
     * Create an instance of {@link ContaCorrenteVO }
     * 
     */
    public ContaCorrenteVO createContaCorrenteVO() {
        return new ContaCorrenteVO();
    }

    /**
     * Create an instance of {@link ValidarDVFuncionarioSaidaVO }
     * 
     */
    public ValidarDVFuncionarioSaidaVO createValidarDVFuncionarioSaidaVO() {
        return new ValidarDVFuncionarioSaidaVO();
    }

    /**
     * Create an instance of {@link ArrayOfContaCorrenteFuncionarioVO }
     * 
     */
    public ArrayOfContaCorrenteFuncionarioVO createArrayOfContaCorrenteFuncionarioVO() {
        return new ArrayOfContaCorrenteFuncionarioVO();
    }

    /**
     * Create an instance of {@link ArrayOfCorrentistasPorFiltrosVO }
     * 
     */
    public ArrayOfCorrentistasPorFiltrosVO createArrayOfCorrentistasPorFiltrosVO() {
        return new ArrayOfCorrentistasPorFiltrosVO();
    }

    /**
     * Create an instance of {@link ContaCorrentePorCPFCNPJVO }
     * 
     */
    public ContaCorrentePorCPFCNPJVO createContaCorrentePorCPFCNPJVO() {
        return new ContaCorrentePorCPFCNPJVO();
    }

    /**
     * Create an instance of {@link ValidarDadosFuncionarioVO }
     * 
     */
    public ValidarDadosFuncionarioVO createValidarDadosFuncionarioVO() {
        return new ValidarDadosFuncionarioVO();
    }

    /**
     * Create an instance of {@link ValidarDadosFuncionarioSinergiaEntradaVO }
     * 
     */
    public ValidarDadosFuncionarioSinergiaEntradaVO createValidarDadosFuncionarioSinergiaEntradaVO() {
        return new ValidarDadosFuncionarioSinergiaEntradaVO();
    }

    /**
     * Create an instance of {@link ArrayOfCorrentistaVO }
     * 
     */
    public ArrayOfCorrentistaVO createArrayOfCorrentistaVO() {
        return new ArrayOfCorrentistaVO();
    }

    /**
     * Create an instance of {@link ListarSegmentacaoProdutoClienteSaidaVO }
     * 
     */
    public ListarSegmentacaoProdutoClienteSaidaVO createListarSegmentacaoProdutoClienteSaidaVO() {
        return new ListarSegmentacaoProdutoClienteSaidaVO();
    }

    /**
     * Create an instance of {@link ArrayOfContaCorrenteIdBucVO }
     * 
     */
    public ArrayOfContaCorrenteIdBucVO createArrayOfContaCorrenteIdBucVO() {
        return new ArrayOfContaCorrenteIdBucVO();
    }

    /**
     * Create an instance of {@link PessoaContaCorrenteVO }
     * 
     */
    public PessoaContaCorrenteVO createPessoaContaCorrenteVO() {
        return new PessoaContaCorrenteVO();
    }

    /**
     * Create an instance of {@link CorrentistasPorFiltrosVO }
     * 
     */
    public CorrentistasPorFiltrosVO createCorrentistasPorFiltrosVO() {
        return new CorrentistasPorFiltrosVO();
    }

    /**
     * Create an instance of {@link ContaCorrenteCpfVO }
     * 
     */
    public ContaCorrenteCpfVO createContaCorrenteCpfVO() {
        return new ContaCorrenteCpfVO();
    }

    /**
     * Create an instance of {@link ListarCorrentistasPorAgCcVO }
     * 
     */
    public ListarCorrentistasPorAgCcVO createListarCorrentistasPorAgCcVO() {
        return new ListarCorrentistasPorAgCcVO();
    }

    /**
     * Create an instance of {@link IDBucPorContaCorrenteCpfCnpjVO }
     * 
     */
    public IDBucPorContaCorrenteCpfCnpjVO createIDBucPorContaCorrenteCpfCnpjVO() {
        return new IDBucPorContaCorrenteCpfCnpjVO();
    }

    /**
     * Create an instance of {@link ValidarDVFuncionarioEntradaVO }
     * 
     */
    public ValidarDVFuncionarioEntradaVO createValidarDVFuncionarioEntradaVO() {
        return new ValidarDVFuncionarioEntradaVO();
    }

    /**
     * Create an instance of {@link ValidarDadosFuncionarioSinergiaSaidaVO }
     * 
     */
    public ValidarDadosFuncionarioSinergiaSaidaVO createValidarDadosFuncionarioSinergiaSaidaVO() {
        return new ValidarDadosFuncionarioSinergiaSaidaVO();
    }

    /**
     * Create an instance of {@link ContaCorrenteIdBucVO }
     * 
     */
    public ContaCorrenteIdBucVO createContaCorrenteIdBucVO() {
        return new ContaCorrenteIdBucVO();
    }

    /**
     * Create an instance of {@link ContaCorrenteFuncionarioIdBucVO }
     * 
     */
    public ContaCorrenteFuncionarioIdBucVO createContaCorrenteFuncionarioIdBucVO() {
        return new ContaCorrenteFuncionarioIdBucVO();
    }

    /**
     * Create an instance of {@link PessoaContaCorrenteEntradaVO }
     * 
     */
    public PessoaContaCorrenteEntradaVO createPessoaContaCorrenteEntradaVO() {
        return new PessoaContaCorrenteEntradaVO();
    }

    /**
     * Create an instance of {@link CorrentistaVO }
     * 
     */
    public CorrentistaVO createCorrentistaVO() {
        return new CorrentistaVO();
    }

}
