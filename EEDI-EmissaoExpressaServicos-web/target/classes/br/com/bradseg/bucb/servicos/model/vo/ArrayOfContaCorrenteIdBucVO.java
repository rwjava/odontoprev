package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ArrayOfContaCorrenteIdBucVO complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfContaCorrenteIdBucVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContaCorrenteIdBucVO" type="{http://vo.model.servicos.bucb.bradseg.com.br}ContaCorrenteIdBucVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfContaCorrenteIdBucVO", propOrder = { "contaCorrenteIdBucVO" })
public class ArrayOfContaCorrenteIdBucVO implements Serializable {

	private static final long serialVersionUID = -477465828951083012L;
	@XmlElement(name = "ContaCorrenteIdBucVO", nillable = true)
	protected List<ContaCorrenteIdBucVO> contaCorrenteIdBucVO;

	/**
	 * Gets the value of the contaCorrenteIdBucVO property.
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
	 * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
	 * the contaCorrenteIdBucVO property.
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getContaCorrenteIdBucVO().add(newItem);
	 * </pre>
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link ContaCorrenteIdBucVO }
	 */
	public List<ContaCorrenteIdBucVO> getContaCorrenteIdBucVO() {
		if (contaCorrenteIdBucVO == null) {
			contaCorrenteIdBucVO = new ArrayList<ContaCorrenteIdBucVO>();
		}
		return this.contaCorrenteIdBucVO;
	}

}
