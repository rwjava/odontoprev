
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContaCorrenteCpfVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContaCorrenteCpfVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bairro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cep" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cidade" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoAgencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codOcupacao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="compl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dddFone" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dtEmissao" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dtNasc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvAgencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContaCorrente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ender" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estCivil" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fone" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="mae" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nacionalidade" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nrDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroContaCorrente" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="orgaoEmissor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pai" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="razao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="telefone" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tpConta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tpDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idCorrentista" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContaCorrenteCpfVO", propOrder = {
    "bairro",
    "cep",
    "cidade",
    "codigoAgencia",
    "codOcupacao",
    "compl",
    "dddFone",
    "dtEmissao",
    "dtNasc",
    "dvAgencia",
    "dvContaCorrente",
    "ender",
    "estado",
    "estCivil",
    "fone",
    "mae",
    "nacionalidade",
    "nome",
    "nrDoc",
    "numero",
    "numeroContaCorrente",
    "orgaoEmissor",
    "pai",
    "razao",
    "telefone",
    "titular",
    "tpConta",
    "tpDoc",
    "idCorrentista"
})
public class ContaCorrenteCpfVO {

    @XmlElement(required = true, nillable = true)
    protected String bairro;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cep;
    @XmlElement(required = true, nillable = true)
    protected String cidade;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoAgencia;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codOcupacao;
    @XmlElement(required = true, nillable = true)
    protected String compl;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer dddFone;
    @XmlElement(required = true, nillable = true)
    protected String dtEmissao;
    @XmlElement(required = true, nillable = true)
    protected String dtNasc;
    @XmlElement(required = true, nillable = true)
    protected String dvAgencia;
    @XmlElement(required = true, nillable = true)
    protected String dvContaCorrente;
    @XmlElement(required = true, nillable = true)
    protected String ender;
    @XmlElement(required = true, nillable = true)
    protected String estado;
    @XmlElement(required = true, nillable = true)
    protected String estCivil;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long fone;
    @XmlElement(required = true, nillable = true)
    protected String mae;
    @XmlElement(required = true, nillable = true)
    protected String nacionalidade;
    @XmlElement(required = true, nillable = true)
    protected String nome;
    @XmlElement(required = true, nillable = true)
    protected String nrDoc;
    @XmlElement(required = true, nillable = true)
    protected String numero;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long numeroContaCorrente;
    @XmlElement(required = true, nillable = true)
    protected String orgaoEmissor;
    @XmlElement(required = true, nillable = true)
    protected String pai;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer razao;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long telefone;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer titular;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tpConta;
    @XmlElement(required = true, nillable = true)
    protected String tpDoc;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long idCorrentista;

    /**
     * Gets the value of the bairro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Sets the value of the bairro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairro(String value) {
        this.bairro = value;
    }

    /**
     * Gets the value of the cep property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCep() {
        return cep;
    }

    /**
     * Sets the value of the cep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCep(Integer value) {
        this.cep = value;
    }

    /**
     * Gets the value of the cidade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Sets the value of the cidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidade(String value) {
        this.cidade = value;
    }

    /**
     * Gets the value of the codigoAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Sets the value of the codigoAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoAgencia(Integer value) {
        this.codigoAgencia = value;
    }

    /**
     * Gets the value of the codOcupacao property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodOcupacao() {
        return codOcupacao;
    }

    /**
     * Sets the value of the codOcupacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodOcupacao(Integer value) {
        this.codOcupacao = value;
    }

    /**
     * Gets the value of the compl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompl() {
        return compl;
    }

    /**
     * Sets the value of the compl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompl(String value) {
        this.compl = value;
    }

    /**
     * Gets the value of the dddFone property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDddFone() {
        return dddFone;
    }

    /**
     * Sets the value of the dddFone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDddFone(Integer value) {
        this.dddFone = value;
    }

    /**
     * Gets the value of the dtEmissao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtEmissao() {
        return dtEmissao;
    }

    /**
     * Sets the value of the dtEmissao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtEmissao(String value) {
        this.dtEmissao = value;
    }

    /**
     * Gets the value of the dtNasc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDtNasc() {
        return dtNasc;
    }

    /**
     * Sets the value of the dtNasc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtNasc(String value) {
        this.dtNasc = value;
    }

    /**
     * Gets the value of the dvAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvAgencia() {
        return dvAgencia;
    }

    /**
     * Sets the value of the dvAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvAgencia(String value) {
        this.dvAgencia = value;
    }

    /**
     * Gets the value of the dvContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaCorrente() {
        return dvContaCorrente;
    }

    /**
     * Sets the value of the dvContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaCorrente(String value) {
        this.dvContaCorrente = value;
    }

    /**
     * Gets the value of the ender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnder() {
        return ender;
    }

    /**
     * Sets the value of the ender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnder(String value) {
        this.ender = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Gets the value of the estCivil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstCivil() {
        return estCivil;
    }

    /**
     * Sets the value of the estCivil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstCivil(String value) {
        this.estCivil = value;
    }

    /**
     * Gets the value of the fone property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFone() {
        return fone;
    }

    /**
     * Sets the value of the fone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFone(Long value) {
        this.fone = value;
    }

    /**
     * Gets the value of the mae property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMae() {
        return mae;
    }

    /**
     * Sets the value of the mae property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMae(String value) {
        this.mae = value;
    }

    /**
     * Gets the value of the nacionalidade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNacionalidade() {
        return nacionalidade;
    }

    /**
     * Sets the value of the nacionalidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNacionalidade(String value) {
        this.nacionalidade = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the nrDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrDoc() {
        return nrDoc;
    }

    /**
     * Sets the value of the nrDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrDoc(String value) {
        this.nrDoc = value;
    }

    /**
     * Gets the value of the numero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the numeroContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    /**
     * Sets the value of the numeroContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroContaCorrente(Long value) {
        this.numeroContaCorrente = value;
    }

    /**
     * Gets the value of the orgaoEmissor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgaoEmissor() {
        return orgaoEmissor;
    }

    /**
     * Sets the value of the orgaoEmissor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgaoEmissor(String value) {
        this.orgaoEmissor = value;
    }

    /**
     * Gets the value of the pai property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPai() {
        return pai;
    }

    /**
     * Sets the value of the pai property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPai(String value) {
        this.pai = value;
    }

    /**
     * Gets the value of the razao property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRazao() {
        return razao;
    }

    /**
     * Sets the value of the razao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRazao(Integer value) {
        this.razao = value;
    }

    /**
     * Gets the value of the telefone property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTelefone() {
        return telefone;
    }

    /**
     * Sets the value of the telefone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTelefone(Long value) {
        this.telefone = value;
    }

    /**
     * Gets the value of the titular property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTitular() {
        return titular;
    }

    /**
     * Sets the value of the titular property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTitular(Integer value) {
        this.titular = value;
    }

    /**
     * Gets the value of the tpConta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTpConta() {
        return tpConta;
    }

    /**
     * Sets the value of the tpConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTpConta(Integer value) {
        this.tpConta = value;
    }

    /**
     * Gets the value of the tpDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTpDoc() {
        return tpDoc;
    }

    /**
     * Sets the value of the tpDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTpDoc(String value) {
        this.tpDoc = value;
    }

    /**
     * Gets the value of the idCorrentista property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdCorrentista() {
        return idCorrentista;
    }

    /**
     * Sets the value of the idCorrentista property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdCorrentista(Long value) {
        this.idCorrentista = value;
    }

}
