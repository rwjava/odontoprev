
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ValidarDadosFuncionarioSinergiaSaidaVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validarDadosFuncionarioSinergiaReturn" type="{http://vo.model.servicos.bucb.bradseg.com.br}ValidarDadosFuncionarioSinergiaSaidaVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validarDadosFuncionarioSinergiaReturn"
})
@XmlRootElement(name = "validarDadosFuncionarioSinergiaResponse")
public class ValidarDadosFuncionarioSinergiaResponse {

    @XmlElement(required = true, nillable = true)
    protected ValidarDadosFuncionarioSinergiaSaidaVO validarDadosFuncionarioSinergiaReturn;

    /**
     * Gets the value of the validarDadosFuncionarioSinergiaReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ValidarDadosFuncionarioSinergiaSaidaVO }
     *     
     */
    public ValidarDadosFuncionarioSinergiaSaidaVO getValidarDadosFuncionarioSinergiaReturn() {
        return validarDadosFuncionarioSinergiaReturn;
    }

    /**
     * Sets the value of the validarDadosFuncionarioSinergiaReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidarDadosFuncionarioSinergiaSaidaVO }
     *     
     */
    public void setValidarDadosFuncionarioSinergiaReturn(ValidarDadosFuncionarioSinergiaSaidaVO value) {
        this.validarDadosFuncionarioSinergiaReturn = value;
    }

}
