
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="consultarContaCorrentePorCPFCNPJReturn" type="{http://service.model.servicos.bucb.bradseg.com.br}ArrayOf_1799424901_nillable_ContaCorrenteVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarContaCorrentePorCPFCNPJReturn"
})
@XmlRootElement(name = "consultarContaCorrentePorCPFCNPJResponse")
public class ConsultarContaCorrentePorCPFCNPJResponse {

    @XmlElement(required = true, nillable = true)
    protected ArrayOf1799424901NillableContaCorrenteVO consultarContaCorrentePorCPFCNPJReturn;

    /**
     * Gets the value of the consultarContaCorrentePorCPFCNPJReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOf1799424901NillableContaCorrenteVO }
     *     
     */
    public ArrayOf1799424901NillableContaCorrenteVO getConsultarContaCorrentePorCPFCNPJReturn() {
        return consultarContaCorrentePorCPFCNPJReturn;
    }

    /**
     * Sets the value of the consultarContaCorrentePorCPFCNPJReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOf1799424901NillableContaCorrenteVO }
     *     
     */
    public void setConsultarContaCorrentePorCPFCNPJReturn(ArrayOf1799424901NillableContaCorrenteVO value) {
        this.consultarContaCorrentePorCPFCNPJReturn = value;
    }

}
