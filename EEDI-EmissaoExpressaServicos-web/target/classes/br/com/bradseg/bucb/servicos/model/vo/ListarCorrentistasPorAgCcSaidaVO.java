
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListarCorrentistasPorAgCcSaidaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListarCorrentistasPorAgCcSaidaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agenciaDestino" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cdPosto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="contaDestino" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="descPosto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descSegmento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContaDestino" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="indTragueada" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nrOcorrencias" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="razao" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="segmento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tipoConta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="listCorrentistasPorFiltrosVO" type="{http://vo.model.servicos.bucb.bradseg.com.br}ArrayOfCorrentistasPorFiltrosVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListarCorrentistasPorAgCcSaidaVO", propOrder = {
    "agenciaDestino",
    "cdPosto",
    "contaDestino",
    "descPosto",
    "descSegmento",
    "dvContaDestino",
    "indTragueada",
    "nrOcorrencias",
    "razao",
    "segmento",
    "tipoConta",
    "listCorrentistasPorFiltrosVO"
})
public class ListarCorrentistasPorAgCcSaidaVO {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long agenciaDestino;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer cdPosto;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long contaDestino;
    @XmlElement(required = true, nillable = true)
    protected String descPosto;
    @XmlElement(required = true, nillable = true)
    protected String descSegmento;
    @XmlElement(required = true, nillable = true)
    protected String dvContaDestino;
    @XmlElement(required = true, nillable = true)
    protected String indTragueada;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer nrOcorrencias;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer razao;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer segmento;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tipoConta;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfCorrentistasPorFiltrosVO listCorrentistasPorFiltrosVO;

    /**
     * Gets the value of the agenciaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgenciaDestino() {
        return agenciaDestino;
    }

    /**
     * Sets the value of the agenciaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgenciaDestino(Long value) {
        this.agenciaDestino = value;
    }

    /**
     * Gets the value of the cdPosto property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCdPosto() {
        return cdPosto;
    }

    /**
     * Sets the value of the cdPosto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCdPosto(Integer value) {
        this.cdPosto = value;
    }

    /**
     * Gets the value of the contaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContaDestino() {
        return contaDestino;
    }

    /**
     * Sets the value of the contaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContaDestino(Long value) {
        this.contaDestino = value;
    }

    /**
     * Gets the value of the descPosto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescPosto() {
        return descPosto;
    }

    /**
     * Sets the value of the descPosto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescPosto(String value) {
        this.descPosto = value;
    }

    /**
     * Gets the value of the descSegmento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescSegmento() {
        return descSegmento;
    }

    /**
     * Sets the value of the descSegmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescSegmento(String value) {
        this.descSegmento = value;
    }

    /**
     * Gets the value of the dvContaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaDestino() {
        return dvContaDestino;
    }

    /**
     * Sets the value of the dvContaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaDestino(String value) {
        this.dvContaDestino = value;
    }

    /**
     * Gets the value of the indTragueada property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndTragueada() {
        return indTragueada;
    }

    /**
     * Sets the value of the indTragueada property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndTragueada(String value) {
        this.indTragueada = value;
    }

    /**
     * Gets the value of the nrOcorrencias property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNrOcorrencias() {
        return nrOcorrencias;
    }

    /**
     * Sets the value of the nrOcorrencias property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNrOcorrencias(Integer value) {
        this.nrOcorrencias = value;
    }

    /**
     * Gets the value of the razao property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRazao() {
        return razao;
    }

    /**
     * Sets the value of the razao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRazao(Integer value) {
        this.razao = value;
    }

    /**
     * Gets the value of the segmento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSegmento() {
        return segmento;
    }

    /**
     * Sets the value of the segmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSegmento(Integer value) {
        this.segmento = value;
    }

    /**
     * Gets the value of the tipoConta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTipoConta() {
        return tipoConta;
    }

    /**
     * Sets the value of the tipoConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTipoConta(Integer value) {
        this.tipoConta = value;
    }

    /**
     * Gets the value of the listCorrentistasPorFiltrosVO property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCorrentistasPorFiltrosVO }
     *     
     */
    public ArrayOfCorrentistasPorFiltrosVO getListCorrentistasPorFiltrosVO() {
        return listCorrentistasPorFiltrosVO;
    }

    /**
     * Sets the value of the listCorrentistasPorFiltrosVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCorrentistasPorFiltrosVO }
     *     
     */
    public void setListCorrentistasPorFiltrosVO(ArrayOfCorrentistasPorFiltrosVO value) {
        this.listCorrentistasPorFiltrosVO = value;
    }

}
