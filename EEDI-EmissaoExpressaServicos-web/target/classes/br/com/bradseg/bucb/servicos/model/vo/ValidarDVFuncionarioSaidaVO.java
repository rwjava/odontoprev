
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidarDVFuncionarioSaidaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidarDVFuncionarioSaidaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agenciaDest" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoPosto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoSegmento" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ctaDest" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="descricaoPosto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descricaoSegmento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContaDest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvValido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="indicadorTragueamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="razao" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="situacao" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="situacaoDest" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="tipoConta" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="titularidade" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="razaoValida" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidarDVFuncionarioSaidaVO", propOrder = {
    "agenciaDest",
    "codigoPosto",
    "codigoSegmento",
    "ctaDest",
    "descricaoPosto",
    "descricaoSegmento",
    "dvContaDest",
    "dvValido",
    "indicadorTragueamento",
    "razao",
    "situacao",
    "situacaoDest",
    "tipoConta",
    "titularidade",
    "razaoValida"
})
public class ValidarDVFuncionarioSaidaVO {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long agenciaDest;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoPosto;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoSegmento;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long ctaDest;
    @XmlElement(required = true, nillable = true)
    protected String descricaoPosto;
    @XmlElement(required = true, nillable = true)
    protected String descricaoSegmento;
    @XmlElement(required = true, nillable = true)
    protected String dvContaDest;
    @XmlElement(required = true, nillable = true)
    protected String dvValido;
    @XmlElement(required = true, nillable = true)
    protected String indicadorTragueamento;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long razao;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long situacao;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long situacaoDest;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long tipoConta;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long titularidade;
    @XmlElement(required = true, nillable = true)
    protected String razaoValida;

    /**
     * Gets the value of the agenciaDest property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgenciaDest() {
        return agenciaDest;
    }

    /**
     * Sets the value of the agenciaDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgenciaDest(Long value) {
        this.agenciaDest = value;
    }

    /**
     * Gets the value of the codigoPosto property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoPosto() {
        return codigoPosto;
    }

    /**
     * Sets the value of the codigoPosto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoPosto(Long value) {
        this.codigoPosto = value;
    }

    /**
     * Gets the value of the codigoSegmento property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoSegmento() {
        return codigoSegmento;
    }

    /**
     * Sets the value of the codigoSegmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoSegmento(Long value) {
        this.codigoSegmento = value;
    }

    /**
     * Gets the value of the ctaDest property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCtaDest() {
        return ctaDest;
    }

    /**
     * Sets the value of the ctaDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCtaDest(Long value) {
        this.ctaDest = value;
    }

    /**
     * Gets the value of the descricaoPosto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoPosto() {
        return descricaoPosto;
    }

    /**
     * Sets the value of the descricaoPosto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoPosto(String value) {
        this.descricaoPosto = value;
    }

    /**
     * Gets the value of the descricaoSegmento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoSegmento() {
        return descricaoSegmento;
    }

    /**
     * Sets the value of the descricaoSegmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoSegmento(String value) {
        this.descricaoSegmento = value;
    }

    /**
     * Gets the value of the dvContaDest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaDest() {
        return dvContaDest;
    }

    /**
     * Sets the value of the dvContaDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaDest(String value) {
        this.dvContaDest = value;
    }

    /**
     * Gets the value of the dvValido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvValido() {
        return dvValido;
    }

    /**
     * Sets the value of the dvValido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvValido(String value) {
        this.dvValido = value;
    }

    /**
     * Gets the value of the indicadorTragueamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorTragueamento() {
        return indicadorTragueamento;
    }

    /**
     * Sets the value of the indicadorTragueamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorTragueamento(String value) {
        this.indicadorTragueamento = value;
    }

    /**
     * Gets the value of the razao property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRazao() {
        return razao;
    }

    /**
     * Sets the value of the razao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRazao(Long value) {
        this.razao = value;
    }

    /**
     * Gets the value of the situacao property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSituacao() {
        return situacao;
    }

    /**
     * Sets the value of the situacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSituacao(Long value) {
        this.situacao = value;
    }

    /**
     * Gets the value of the situacaoDest property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSituacaoDest() {
        return situacaoDest;
    }

    /**
     * Sets the value of the situacaoDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSituacaoDest(Long value) {
        this.situacaoDest = value;
    }

    /**
     * Gets the value of the tipoConta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTipoConta() {
        return tipoConta;
    }

    /**
     * Sets the value of the tipoConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTipoConta(Long value) {
        this.tipoConta = value;
    }

    /**
     * Gets the value of the titularidade property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTitularidade() {
        return titularidade;
    }

    /**
     * Sets the value of the titularidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTitularidade(Long value) {
        this.titularidade = value;
    }

    /**
     * Gets the value of the razaoValida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRazaoValida() {
        return razaoValida;
    }

    /**
     * Sets the value of the razaoValida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRazaoValida(String value) {
        this.razaoValida = value;
    }

}
