
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ValidarDadosFuncionarioVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validarDadosFuncionarioReturn" type="{http://vo.model.servicos.bucb.bradseg.com.br}ValidarDadosFuncionarioVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validarDadosFuncionarioReturn"
})
@XmlRootElement(name = "validarDadosFuncionarioResponse")
public class ValidarDadosFuncionarioResponse {

    @XmlElement(required = true, nillable = true)
    protected ValidarDadosFuncionarioVO validarDadosFuncionarioReturn;

    /**
     * Gets the value of the validarDadosFuncionarioReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ValidarDadosFuncionarioVO }
     *     
     */
    public ValidarDadosFuncionarioVO getValidarDadosFuncionarioReturn() {
        return validarDadosFuncionarioReturn;
    }

    /**
     * Sets the value of the validarDadosFuncionarioReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidarDadosFuncionarioVO }
     *     
     */
    public void setValidarDadosFuncionarioReturn(ValidarDadosFuncionarioVO value) {
        this.validarDadosFuncionarioReturn = value;
    }

}
