
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IDBucPorContaCorrenteCpfCnpjVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IDBucPorContaCorrenteCpfCnpjVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoAgencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dvContaCorrente" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroContaCorrente" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numeroCpfCnpj" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IDBucPorContaCorrenteCpfCnpjVO", propOrder = {
    "codigoAgencia",
    "dvContaCorrente",
    "numeroContaCorrente",
    "numeroCpfCnpj"
})
public class IDBucPorContaCorrenteCpfCnpjVO {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoAgencia;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer dvContaCorrente;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long numeroContaCorrente;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long numeroCpfCnpj;

    /**
     * Gets the value of the codigoAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Sets the value of the codigoAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoAgencia(Integer value) {
        this.codigoAgencia = value;
    }

    /**
     * Gets the value of the dvContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDvContaCorrente() {
        return dvContaCorrente;
    }

    /**
     * Sets the value of the dvContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDvContaCorrente(Integer value) {
        this.dvContaCorrente = value;
    }

    /**
     * Gets the value of the numeroContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    /**
     * Sets the value of the numeroContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroContaCorrente(Long value) {
        this.numeroContaCorrente = value;
    }

    /**
     * Gets the value of the numeroCpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroCpfCnpj() {
        return numeroCpfCnpj;
    }

    /**
     * Sets the value of the numeroCpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroCpfCnpj(Long value) {
        this.numeroCpfCnpj = value;
    }

}
