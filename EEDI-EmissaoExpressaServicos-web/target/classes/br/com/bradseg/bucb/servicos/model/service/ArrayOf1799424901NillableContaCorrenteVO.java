
package br.com.bradseg.bucb.servicos.model.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ContaCorrenteVO;


/**
 * <p>Java class for ArrayOf_1799424901_nillable_ContaCorrenteVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOf_1799424901_nillable_ContaCorrenteVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContaCorrenteVO" type="{http://vo.model.servicos.bucb.bradseg.com.br}ContaCorrenteVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOf_1799424901_nillable_ContaCorrenteVO", propOrder = {
    "contaCorrenteVO"
})
public class ArrayOf1799424901NillableContaCorrenteVO {

    @XmlElement(name = "ContaCorrenteVO", nillable = true)
    protected List<ContaCorrenteVO> contaCorrenteVO;

    /**
     * Gets the value of the contaCorrenteVO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contaCorrenteVO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContaCorrenteVO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContaCorrenteVO }
     * 
     * 
     */
    public List<ContaCorrenteVO> getContaCorrenteVO() {
        if (contaCorrenteVO == null) {
            contaCorrenteVO = new ArrayList<ContaCorrenteVO>();
        }
        return this.contaCorrenteVO;
    }

}
