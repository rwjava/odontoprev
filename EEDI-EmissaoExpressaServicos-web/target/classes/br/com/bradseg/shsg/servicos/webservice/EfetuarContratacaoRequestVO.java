
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for efetuarContratacaoRequestVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="efetuarContratacaoRequestVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="produto" type="{http://webservice.servicos.shsg.bradseg.com.br/}atualizaEtapaCompraProdutoVO"/>
 *         &lt;element name="numeroProposta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroApolice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "efetuarContratacaoRequestVO", propOrder = {
    "produto",
    "numeroProposta",
    "numeroApolice"
})
public class EfetuarContratacaoRequestVO {

    @XmlElement(required = true)
    protected AtualizaEtapaCompraProdutoVO produto;
    @XmlElement(required = true)
    protected String numeroProposta;
    @XmlElement(required = true)
    protected String numeroApolice;

    /**
     * Gets the value of the produto property.
     * 
     * @return
     *     possible object is
     *     {@link AtualizaEtapaCompraProdutoVO }
     *     
     */
    public AtualizaEtapaCompraProdutoVO getProduto() {
        return produto;
    }

    /**
     * Sets the value of the produto property.
     * 
     * @param value
     *     allowed object is
     *     {@link AtualizaEtapaCompraProdutoVO }
     *     
     */
    public void setProduto(AtualizaEtapaCompraProdutoVO value) {
        this.produto = value;
    }

    /**
     * Gets the value of the numeroProposta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProposta() {
        return numeroProposta;
    }

    /**
     * Sets the value of the numeroProposta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProposta(String value) {
        this.numeroProposta = value;
    }

    /**
     * Gets the value of the numeroApolice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroApolice() {
        return numeroApolice;
    }

    /**
     * Sets the value of the numeroApolice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroApolice(String value) {
        this.numeroApolice = value;
    }

}
