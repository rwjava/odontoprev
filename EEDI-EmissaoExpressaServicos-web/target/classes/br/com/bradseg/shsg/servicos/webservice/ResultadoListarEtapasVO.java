
package br.com.bradseg.shsg.servicos.webservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for resultadoListarEtapasVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="resultadoListarEtapasVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.servicos.shsg.bradseg.com.br/}resultadoVO">
 *       &lt;sequence>
 *         &lt;element name="listaEtapas" type="{http://webservice.servicos.shsg.bradseg.com.br/}etapaVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultadoListarEtapasVO", propOrder = {
    "listaEtapas"
})
public class ResultadoListarEtapasVO
    extends ResultadoVO
{

    @XmlElement(nillable = true)
    protected List<EtapaVO> listaEtapas;

    /**
     * Gets the value of the listaEtapas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaEtapas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaEtapas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EtapaVO }
     * 
     * 
     */
    public List<EtapaVO> getListaEtapas() {
        if (listaEtapas == null) {
            listaEtapas = new ArrayList<EtapaVO>();
        }
        return this.listaEtapas;
    }

}
