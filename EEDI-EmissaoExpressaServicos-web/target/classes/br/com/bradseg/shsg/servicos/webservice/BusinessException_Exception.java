
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.5.2
 * 2016-01-28T17:07:37.155-02:00
 * Generated source version: 2.5.2
 */

@WebFault(name = "BusinessException", targetNamespace = "http" + "://webservice.servicos.shsg.bradseg.com.br/")
public class BusinessException_Exception extends Exception {
    
    private br.com.bradseg.shsg.servicos.webservice.BusinessException businessException;

    public BusinessException_Exception() {
        super();
    }
    
    public BusinessException_Exception(String message) {
        super(message);
    }
    
    public BusinessException_Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException_Exception(String message, br.com.bradseg.shsg.servicos.webservice.BusinessException businessException) {
        super(message);
        this.businessException = businessException;
    }

    public BusinessException_Exception(String message, br.com.bradseg.shsg.servicos.webservice.BusinessException businessException, Throwable cause) {
        super(message, cause);
        this.businessException = businessException;
    }

    public br.com.bradseg.shsg.servicos.webservice.BusinessException getFaultInfo() {
        return this.businessException;
    }
}
