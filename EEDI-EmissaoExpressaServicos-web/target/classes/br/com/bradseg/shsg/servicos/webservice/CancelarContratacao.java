
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cancelarContratacao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cancelarContratacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cancelarContratacao" type="{http://webservice.servicos.shsg.bradseg.com.br/}cancelarContratacaoRequestVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancelarContratacao", propOrder = {
    "cancelarContratacao"
})
public class CancelarContratacao {

    protected CancelarContratacaoRequestVO cancelarContratacao;

    /**
     * Gets the value of the cancelarContratacao property.
     * 
     * @return
     *     possible object is
     *     {@link CancelarContratacaoRequestVO }
     *     
     */
    public CancelarContratacaoRequestVO getCancelarContratacao() {
        return cancelarContratacao;
    }

    /**
     * Sets the value of the cancelarContratacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelarContratacaoRequestVO }
     *     
     */
    public void setCancelarContratacao(CancelarContratacaoRequestVO value) {
        this.cancelarContratacao = value;
    }

}
