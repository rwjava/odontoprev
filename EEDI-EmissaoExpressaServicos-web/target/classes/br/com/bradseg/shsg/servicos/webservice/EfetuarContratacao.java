
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for efetuarContratacao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="efetuarContratacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="efetuarContratacao" type="{http://webservice.servicos.shsg.bradseg.com.br/}efetuarContratacaoRequestVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "efetuarContratacao", propOrder = {
    "efetuarContratacao"
})
public class EfetuarContratacao {

    protected EfetuarContratacaoRequestVO efetuarContratacao;

    /**
     * Gets the value of the efetuarContratacao property.
     * 
     * @return
     *     possible object is
     *     {@link EfetuarContratacaoRequestVO }
     *     
     */
    public EfetuarContratacaoRequestVO getEfetuarContratacao() {
        return efetuarContratacao;
    }

    /**
     * Sets the value of the efetuarContratacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link EfetuarContratacaoRequestVO }
     *     
     */
    public void setEfetuarContratacao(EfetuarContratacaoRequestVO value) {
        this.efetuarContratacao = value;
    }

}
