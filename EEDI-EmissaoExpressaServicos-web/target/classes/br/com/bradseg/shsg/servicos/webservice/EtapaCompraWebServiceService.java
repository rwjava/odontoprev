package br.com.bradseg.shsg.servicos.webservice;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.5.2
 * 2016-11-09T13:52:13.038-02:00
 * Generated source version: 2.5.2
 * 
 */
@WebServiceClient(name = "EtapaCompraWebServiceService", 
                  wsdlLocation = "http"+"://wsphttp.dsv.bradseg.com.br/SHSG-Servicos/service/EtapaCompraWebService?wsdl",
                  targetNamespace = "http"+"://webservice.servicos.shsg.bradseg.com.br/") 
public class EtapaCompraWebServiceService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http"+"://webservice.servicos.shsg.bradseg.com.br/", "EtapaCompraWebServiceService");
    public final static QName EtapaCompraWebServicePort = new QName("http"+"://webservice.servicos.shsg.bradseg.com.br/", "EtapaCompraWebServicePort");
    static {
        URL url = null;
        try {
            url = new URL("http"+"://wsphttp.dsv.bradseg.com.br/SHSG-Servicos/service/EtapaCompraWebService?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(EtapaCompraWebServiceService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http"+"://wsphttp.dsv.bradseg.com.br/SHSG-Servicos/service/EtapaCompraWebService?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public EtapaCompraWebServiceService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public EtapaCompraWebServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public EtapaCompraWebServiceService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public EtapaCompraWebServiceService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public EtapaCompraWebServiceService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public EtapaCompraWebServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName);
    }

    /**
     *
     * @return
     *     returns EtapaCompraWebService
     */
    @WebEndpoint(name = "EtapaCompraWebServicePort")
    public EtapaCompraWebService getEtapaCompraWebServicePort() {
        return super.getPort(EtapaCompraWebServicePort, EtapaCompraWebService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns EtapaCompraWebService
     */
    @WebEndpoint(name = "EtapaCompraWebServicePort")
    public EtapaCompraWebService getEtapaCompraWebServicePort(WebServiceFeature... features) {
        return super.getPort(EtapaCompraWebServicePort, EtapaCompraWebService.class, features);
    }

}
