
package br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.ccrr.cadastrocorretores.model.vo.ws.WSCorretorVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="obterCorretorReturn" type="{http://ws.vo.model.cadastrocorretores.ccrr.bradseg.com.br}WSCorretorVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "obterCorretorReturn"
})
@XmlRootElement(name = "obterCorretorResponse")
public class ObterCorretorResponse {

    @XmlElement(required = true, nillable = true)
    protected WSCorretorVO obterCorretorReturn;

    /**
     * Gets the value of the obterCorretorReturn property.
     * 
     * @return
     *     possible object is
     *     {@link WSCorretorVO }
     *     
     */
    public WSCorretorVO getObterCorretorReturn() {
        return obterCorretorReturn;
    }

    /**
     * Sets the value of the obterCorretorReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCorretorVO }
     *     
     */
    public void setObterCorretorReturn(WSCorretorVO value) {
        this.obterCorretorReturn = value;
    }

}
