
package br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.ccrr.cadastrocorretores.model.corretorsucursal.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObterDadosCorretorPorCpdResponse }
     * 
     */
    public ObterDadosCorretorPorCpdResponse createObterDadosCorretorPorCpdResponse() {
        return new ObterDadosCorretorPorCpdResponse();
    }

    /**
     * Create an instance of {@link ObterMeiosContatoCorretorNaSucursal }
     * 
     */
    public ObterMeiosContatoCorretorNaSucursal createObterMeiosContatoCorretorNaSucursal() {
        return new ObterMeiosContatoCorretorNaSucursal();
    }

    /**
     * Create an instance of {@link ObterMeiosContatoCorretorNaSucursalResponse }
     * 
     */
    public ObterMeiosContatoCorretorNaSucursalResponse createObterMeiosContatoCorretorNaSucursalResponse() {
        return new ObterMeiosContatoCorretorNaSucursalResponse();
    }

    /**
     * Create an instance of {@link ObterCorretor }
     * 
     */
    public ObterCorretor createObterCorretor() {
        return new ObterCorretor();
    }

    /**
     * Create an instance of {@link ObterCorretorResponse }
     * 
     */
    public ObterCorretorResponse createObterCorretorResponse() {
        return new ObterCorretorResponse();
    }

    /**
     * Create an instance of {@link ObterDadosCorretorPorCpd }
     * 
     */
    public ObterDadosCorretorPorCpd createObterDadosCorretorPorCpd() {
        return new ObterDadosCorretorPorCpd();
    }

}
