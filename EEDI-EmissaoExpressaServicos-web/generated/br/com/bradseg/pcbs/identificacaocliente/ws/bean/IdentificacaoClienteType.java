
package br.com.bradseg.pcbs.identificacaocliente.ws.bean;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificacaoClienteType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificacaoClienteType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ativo" type="{http://bean.ws.identificacaocliente.pcbs.bradseg.com.br}statusClienteRestrict"/>
 *         &lt;element name="tipoCliente" type="{http://bean.ws.identificacaocliente.pcbs.bradseg.com.br}tipoClienteRestrict"/>
 *         &lt;element name="empresa" type="{http://bean.ws.identificacaocliente.pcbs.bradseg.com.br}empresaRestrict"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificacaoClienteType", propOrder = {
    "ativo",
    "tipoCliente",
    "empresa"
})
public class IdentificacaoClienteType {

    @XmlElement(required = true, nillable = true)
    protected StatusClienteRestrict ativo;
    @XmlElement(required = true, nillable = true)
    protected TipoClienteRestrict tipoCliente;
    @XmlElement(required = true)
    protected BigInteger empresa;

    /**
     * Gets the value of the ativo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusClienteRestrict }
     *     
     */
    public StatusClienteRestrict getAtivo() {
        return ativo;
    }

    /**
     * Sets the value of the ativo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusClienteRestrict }
     *     
     */
    public void setAtivo(StatusClienteRestrict value) {
        this.ativo = value;
    }

    /**
     * Gets the value of the tipoCliente property.
     * 
     * @return
     *     possible object is
     *     {@link TipoClienteRestrict }
     *     
     */
    public TipoClienteRestrict getTipoCliente() {
        return tipoCliente;
    }

    /**
     * Sets the value of the tipoCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoClienteRestrict }
     *     
     */
    public void setTipoCliente(TipoClienteRestrict value) {
        this.tipoCliente = value;
    }

    /**
     * Gets the value of the empresa property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEmpresa() {
        return empresa;
    }

    /**
     * Sets the value of the empresa property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEmpresa(BigInteger value) {
        this.empresa = value;
    }

}
