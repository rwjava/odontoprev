
package br.com.bradseg.pcbs.identificacaocliente.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import br.com.bradseg.pcbs.identificacaocliente.ws.exception.FaultException;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.pcbs.identificacaocliente.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final String PCBS_IDENTIFICACAO_CLIENTE_QNAME = "http" + "://ws.identificacaocliente.pcbs.bradseg.com.br";
    
	private final static QName _Fault_QNAME = new QName(PCBS_IDENTIFICACAO_CLIENTE_QNAME, "fault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.pcbs.identificacaocliente.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidarClienteType }
     * 
     */
    public ValidarClienteType createValidarClienteType() {
        return new ValidarClienteType();
    }

    /**
     * Create an instance of {@link ValidarClienteResponse }
     * 
     */
    public ValidarClienteResponse createValidarClienteResponse() {
        return new ValidarClienteResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = PCBS_IDENTIFICACAO_CLIENTE_QNAME, name = "fault")
    public JAXBElement<FaultException> createFault(FaultException value) {
        return new JAXBElement<FaultException>(_Fault_QNAME, FaultException.class, null, value);
    }

}
