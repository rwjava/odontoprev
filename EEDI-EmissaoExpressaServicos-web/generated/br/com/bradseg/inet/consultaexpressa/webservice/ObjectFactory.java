
package br.com.bradseg.inet.consultaexpressa.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.inet.consultaexpressa.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final String INET_CONSULTA_EXPRESSA_QNAME = "http" + "://webservice.consultaexpressa.inet.bradseg.com.br/";
    
	private final static QName _IntegrationException_QNAME = new QName(INET_CONSULTA_EXPRESSA_QNAME, "IntegrationException");
    private final static QName _BusinessException_QNAME = new QName(INET_CONSULTA_EXPRESSA_QNAME, "BusinessException");
    private final static QName _ConsultarListaCorretorSucursalResponse_QNAME = new QName(INET_CONSULTA_EXPRESSA_QNAME, "consultarListaCorretorSucursalResponse");
    private final static QName _ConsultarListaCorretorSucursal_QNAME = new QName(INET_CONSULTA_EXPRESSA_QNAME, "consultarListaCorretorSucursal");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.inet.consultaexpressa.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultarListaCorretorSucursal }
     * 
     */
    public ConsultarListaCorretorSucursal createConsultarListaCorretorSucursal() {
        return new ConsultarListaCorretorSucursal();
    }

    /**
     * Create an instance of {@link ConsultarListaCorretorSucursalResponse }
     * 
     */
    public ConsultarListaCorretorSucursalResponse createConsultarListaCorretorSucursalResponse() {
        return new ConsultarListaCorretorSucursalResponse();
    }

    /**
     * Create an instance of {@link IntegrationException }
     * 
     */
    public IntegrationException createIntegrationException() {
        return new IntegrationException();
    }

    /**
     * Create an instance of {@link BusinessException }
     * 
     */
    public BusinessException createBusinessException() {
        return new BusinessException();
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link CorretorSucursal }
     * 
     */
    public CorretorSucursal createCorretorSucursal() {
        return new CorretorSucursal();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntegrationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = INET_CONSULTA_EXPRESSA_QNAME, name = "IntegrationException")
    public JAXBElement<IntegrationException> createIntegrationException(IntegrationException value) {
        return new JAXBElement<IntegrationException>(_IntegrationException_QNAME, IntegrationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = INET_CONSULTA_EXPRESSA_QNAME, name = "BusinessException")
    public JAXBElement<BusinessException> createBusinessException(BusinessException value) {
        return new JAXBElement<BusinessException>(_BusinessException_QNAME, BusinessException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarListaCorretorSucursalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = INET_CONSULTA_EXPRESSA_QNAME, name = "consultarListaCorretorSucursalResponse")
    public JAXBElement<ConsultarListaCorretorSucursalResponse> createConsultarListaCorretorSucursalResponse(ConsultarListaCorretorSucursalResponse value) {
        return new JAXBElement<ConsultarListaCorretorSucursalResponse>(_ConsultarListaCorretorSucursalResponse_QNAME, ConsultarListaCorretorSucursalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarListaCorretorSucursal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = INET_CONSULTA_EXPRESSA_QNAME, name = "consultarListaCorretorSucursal")
    public JAXBElement<ConsultarListaCorretorSucursal> createConsultarListaCorretorSucursal(ConsultarListaCorretorSucursal value) {
        return new JAXBElement<ConsultarListaCorretorSucursal>(_ConsultarListaCorretorSucursal_QNAME, ConsultarListaCorretorSucursal.class, null, value);
    }

}
