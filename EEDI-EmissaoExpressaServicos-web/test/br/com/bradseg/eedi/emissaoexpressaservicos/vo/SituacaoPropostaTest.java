package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

public class SituacaoPropostaTest {

	@Test
	public void deveRetornarDescricaoQuandoInformadoOCodigo() throws Exception {

		//Dado
		PropostaVO proposta = new PropostaVO();
		proposta.setMovimento(new MovimentoPropostaVO());

		//Quando
		proposta.getMovimento().setSituacao(SituacaoProposta.obterPorCodigo(1));

		//Entao
		assertNotNull(proposta.getMovimento().getSituacao());
		assertEquals(SituacaoProposta.PENDENTE, proposta.getMovimento().getSituacao());
		assertEquals("Pendente", proposta.getMovimento().getSituacao().getDescricao());

	}

	@Test
	public void deveRetornarNullQuandoInformadoCodigoInexistente() throws Exception {

		//Dado
		PropostaVO proposta = new PropostaVO();
		proposta.setMovimento(new MovimentoPropostaVO());

		//Quando
		proposta.getMovimento().setSituacao(SituacaoProposta.obterPorCodigo(9));

		//Entao
		assertEquals(null, proposta.getMovimento().getSituacao());

	}

}
