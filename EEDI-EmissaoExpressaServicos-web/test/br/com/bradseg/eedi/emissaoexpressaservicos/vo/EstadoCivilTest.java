package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

public class EstadoCivilTest {

	@Test
	public void deveRetornarDescricaoQuandoInformadoOCodigo() throws Exception {
		//Dado
		BeneficiarioVO beneficiario = new DependenteVO();

		//Quando
		beneficiario.setEstadoCivil(EstadoCivil.buscaPorCodigo(1));

		//Entao		
		assertNotNull(beneficiario.getEstadoCivil());
		assertEquals(EstadoCivil.CASADO, beneficiario.getEstadoCivil());
		assertEquals("Casado", beneficiario.getEstadoCivil().getDescricao());
	}

	@Test
	public void deveRetornarNullQuandoInformadoCodigoInexistente() throws Exception {
		//Dado
		BeneficiarioVO beneficiario = new DependenteVO();

		//Quando
		beneficiario.setEstadoCivil(EstadoCivil.buscaPorCodigo(8));

		//Entao
		assertEquals(null, beneficiario.getEstadoCivil());
	}

}