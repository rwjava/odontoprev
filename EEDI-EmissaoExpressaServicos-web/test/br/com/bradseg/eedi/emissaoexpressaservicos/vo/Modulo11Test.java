package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.containsString;

import org.junit.Test;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Modulo11;

public class Modulo11Test {

	@Test
	public void deveGerarDigitoVerificador() throws Exception {
		//Dado
		String bloco = "6000000038";

		//Quando
		Integer digitoVerificador = Modulo11.gerarDigitoVerificador(bloco);

		//Entao
		assertEquals("1", String.valueOf(digitoVerificador));
	}

	@Test
	public void deveRetornarDigitoVerificadorDiferentDeUm() throws Exception {
		//Dado
		String bloco = "11111";

		//Quando
		Integer digitoVerificador = Modulo11.gerarDigitoVerificador(bloco);

		//Entao
		assertEquals("2", String.valueOf(digitoVerificador));
	}

	@Test
	public void deveRetornarDigitoVerificadorUmInformandoBlocoZero() throws Exception {
		//Dado
		String bloco = "0";

		//Quando
		Integer digitoVerificador = Modulo11.gerarDigitoVerificador(bloco);

		//Entao
		assertEquals("1", String.valueOf(digitoVerificador));
	}

	@Test
	public void deveRetornarDigitoVerificadorUmInformandoBlocoOnze() throws Exception {
		//Dado
		String bloco = "11";

		//Quando
		Integer digitoVerificador = Modulo11.gerarDigitoVerificador(bloco);

		//Entao
		assertEquals("6", String.valueOf(digitoVerificador));
	}

	@Test(expected = IllegalArgumentException.class)
	public void deveOcorrerErroAoInformarBlocoEmBranco() throws Exception {
		try {
			//Dado
			String bloco = "";

			//Quando
			Modulo11.gerarDigitoVerificador(bloco);

			// Entao
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage(), containsString("O valor n�o pode estar vazio"));
			throw e;
		}
	}

}