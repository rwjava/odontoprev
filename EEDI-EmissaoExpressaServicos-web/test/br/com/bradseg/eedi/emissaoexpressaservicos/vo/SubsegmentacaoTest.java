package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

public class SubsegmentacaoTest {

	@Test
	public void deveRetornarDescricaoQuandoInformadoOCodigo() throws Exception {

		//Dado
		PropostaVO proposta = new PropostaVO();

		//Quando
		proposta.setSubsegmentacao(Subsegmentacao.buscaSubsegmentacaoPor(19));

		//Entao
		assertNotNull(proposta.getSubsegmentacao());
		assertEquals(Subsegmentacao.EXCLUSIVE, proposta.getSubsegmentacao());
		assertEquals("Exclusive", proposta.getSubsegmentacao().getDescricao());

	}

}
