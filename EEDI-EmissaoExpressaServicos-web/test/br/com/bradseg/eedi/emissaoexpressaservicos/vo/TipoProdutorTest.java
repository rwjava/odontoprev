package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class TipoProdutorTest {

	@Test
	public void deveRetornarDescricaoQuandoInformadoOCodigo() throws Exception {
		assertEquals(TipoProdutor.ANGARIADOR, TipoProdutor.obterPorCodigo(1));
	}

	@Test
	public void deveRetornarNullQuandoInformadoCodigoInexistente() throws Exception {
		assertNull(TipoProdutor.obterPorCodigo(5));
	}

}