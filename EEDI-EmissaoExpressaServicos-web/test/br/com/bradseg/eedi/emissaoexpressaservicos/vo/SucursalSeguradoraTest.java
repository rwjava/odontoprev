package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import static org.junit.Assert.*;

import org.junit.Test;

public class SucursalSeguradoraTest {

	@Test
	public void deveConsiderarSucursalEmissoraValidaQuandoCodigoEstiverEmFaixaPermitida() throws Exception {
		// Quando
		SucursalSeguradoraVO SucursalSeguradoraVO = new SucursalSeguradoraVO(399);

		// Entao
		assertTrue(SucursalSeguradoraVO.isEmissora());
		assertTrue(SucursalSeguradoraVO.isValida());
	}

	@Test
	public void deveConsiderarSucursalEmissoraValidaQuandoCodigoForIgual540() throws Exception {
		// Quando
		SucursalSeguradoraVO SucursalSeguradoraVO = new SucursalSeguradoraVO(540);

		// Entao
		assertTrue(SucursalSeguradoraVO.isEmissora());
		assertTrue(SucursalSeguradoraVO.isValida());
	}

	@Test
	public void deveConsiderarSucursalBVPValida() throws Exception {
		// Quando
		SucursalSeguradoraVO SucursalSeguradoraVO = new SucursalSeguradoraVO(702);

		// Entao
		assertTrue(SucursalSeguradoraVO.isBVP());
		assertTrue(SucursalSeguradoraVO.isValida());
	}

	@Test
	public void deveConsiderarSucursalInvalidaQuandoNaoForEmissoraNemBVP() throws Exception {
		assertFalse(new SucursalSeguradoraVO(1000).isValida());
		assertFalse(new SucursalSeguradoraVO(1).isValida());
	}

}