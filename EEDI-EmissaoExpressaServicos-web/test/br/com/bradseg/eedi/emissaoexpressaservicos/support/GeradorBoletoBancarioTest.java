package br.com.bradseg.eedi.emissaoexpressaservicos.support;

import static org.junit.Assert.assertNotNull;

import org.joda.time.LocalDate;
import org.junit.Test;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.GeradorBoletoBancario;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BoletoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EstadoCivil;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.Sexo;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UnidadeFederativaVO;

public class GeradorBoletoBancarioTest {

	@Test
	public void deveGerarInformacoesDoBoletoBancarioComSucesso() throws Exception {
		//Dado		
		PropostaVO proposta = new PropostaVO();
		proposta.setCodigo("BDA000000345626");
		proposta.setCodigoFormatado("BDA00000034562-6");
		proposta.setNumeroSequencial(33890L);
		proposta.setCorretor(new CorretorVO());
		proposta.getCorretor().setCpd(743333);
		proposta.setSucursalSeguradora(new SucursalSeguradoraVO(301));
		proposta.setTitular(titular());

		//Quando
		BoletoVO boleto = GeradorBoletoBancario.gerarBoletoProposta(proposta);
		//Entao
		assertNotNull(boleto);
	}

	private TitularVO titular() {
		TitularVO titular = new TitularVO();
		titular.setNome("Jos� Pereira");
		titular.setNomeMae("Maria Jos�");
		titular.setCpf("818.274.521-70");
		titular.setEstadoCivil(EstadoCivil.SOLTEIRO);
		titular.setDataNascimento(new LocalDate(1989, 10, 18));
		titular.setSexo(Sexo.M);
		titular.setEndereco(endereco());
		return titular;
	}

	private EnderecoVO endereco() {
		EnderecoVO endereco = new EnderecoVO();
		endereco.setLogradouro("Av. Prof. Manoel de Abreu");
		endereco.setBairro("Centro");
		endereco.setCep("20900010");
		endereco.setCidade("Rio de Janeiro");
		endereco.setNumero(282L);
		endereco.setUnidadeFederativa(new UnidadeFederativaVO());
		endereco.getUnidadeFederativa().setSigla("RJ");
		return endereco;
	}

}
