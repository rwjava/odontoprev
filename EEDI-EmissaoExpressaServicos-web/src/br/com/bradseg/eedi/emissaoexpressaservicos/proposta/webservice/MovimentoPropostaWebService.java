package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;


@Service
@WebService
public class MovimentoPropostaWebService {
	
	@Autowired
	private PropostaServiceFacade propostaServiceFacade;

	private static final Logger LOGGER = LoggerFactory.getLogger(MovimentoPropostaVO.class);
	
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PROPOSTAS) List<MovimentoPropostaVO> listarMovimentoPropostaPorSequencial(@WebParam(name = Constantes.WEB_SERVICE_PARAM_SEQUENCIAL_PROPOSTA) Long sequencialProposta) throws IntegrationException, BusinessException {
		return propostaServiceFacade.listarMovimentoPropostaPorSequencial(sequencialProposta);
	}
	
}
