package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.eedi.emissaoexpressaservicos.corretorsucursal.facade.CorretorSucursalServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.AgenciaBancariaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.AssistenteProducaoDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.Validador;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorMasterVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Valida se todos os dados necess�rios para a cria��o de uma proposta vazia foram preenchidos e est�o v�lidos de acordo com as regras.
 * 
 * @author WDEV
 */
@Scope("prototype")
@Named("CadastroPropostaVaziaValidator")
public class CadastroPropostaVaziaValidator {

	@Autowired
	protected AssistenteProducaoDAO assistenteProducaoDAO;

	@Autowired
	protected AgenciaBancariaDAO agenciaBancariaDAO;

	@Autowired
	protected CorretorSucursalServiceFacade corretorSucursalServiceFacade;

	/**
	 * Valida se todas as informa��es necess�rias para criar a proposta foram preenchidas e est�o de acordo com as regras
	 * 
	 * @param proposta Dados da proposta
	 */
	public void validate(PropostaVO proposta) {
		Validador validador = new Validador();

		validarSucursalCorretor(validador, proposta);
		validarAssistenteProducao(validador, proposta);

		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}

	}
	
	public void validatePropostaVazia(PropostaVO proposta) {
		Validador validador = new Validador();

		validarSucursalCorretorParaPropostaVazia(validador, proposta);
//		validarAssistenteProducao(validador, proposta);
		validarPostoAtendimento(validador, proposta);

		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}

	}
	

	/**
	 * Valida se a sucursal e o corretor s�o validos e se est�o corretos
	 * 
	 * <ul>
	 *  <li>Verifica se a sucursal � valida, sendo emissora ou BVP </li>
	 *  <li>Se ela for v�lida, valida o corretor</li>
	 * </ul>
	 * 
	 * @param validador validador
	 * @param proposta proposta
	 */
	private void validarSucursalCorretor(Validador validador, PropostaVO proposta) {
		//@formatter:off
		boolean sucursalValida = validador.verdadeiro(proposta.getSucursalSeguradora() != null, "msg.erro.cadastro.proposta.cpd.sucursal.obrigatorio") 
				&& validador.verdadeiro(proposta.getSucursalSeguradora().isValida(), "msg.erro.sucursal.invalida");
		// @formatter:on

		if (sucursalValida) {
			validarCorretor(validador, proposta);
		}
	}
	
	private void validarSucursalCorretorParaPropostaVazia(Validador validador, PropostaVO proposta) {
		validador.verdadeiro(proposta.getSucursalSeguradora() != null, "msg.erro.cadastro.proposta.cpd.sucursal.obrigatorio");
		validador.verdadeiro(proposta.getSucursalSeguradora().isValida(), "msg.erro.sucursal.invalida");
	}
	
	/**
	 * Metodo para valida��o do Posto de Atendimento
	 * @param validador
	 * @param proposta
	 */
	private void validarPostoAtendimento(Validador validador, PropostaVO proposta){
		if(proposta.getSucursalSeguradora().isBVP()){
			validador.verdadeiro(proposta.getCodigoPostoAtendimento() != null, "msg.erro.cadastro.proposta.posto.atendimento.obrigatorio");
			if(proposta.getCodigoPostoAtendimento() != null){	
				validador.verdadeiro(proposta.getCodigoPostoAtendimento() != 0, "msg.erro.cadastro.proposta.posto.atendimento.obrigatorio");
				validador.verdadeiro(!proposta.getCodigoPostoAtendimento().toString().equals(""), "msg.erro.cadastro.proposta.posto.atendimento.obrigatorio");
			}
		}
	}
	/**
	 * Valida as informa��es dos corretores da proposta
	 * <ul>
	 * 	<li>Se a sucursal for emissora, valida o angariador</li>
	 * 	<li>Se a sucursal for BVP, valida o corretor master</li>
	 * </ul>
	 * @param validador validador
	 * @param proposta proposta
	 */
	private void validarCorretor(Validador validador, PropostaVO proposta) {
		if (proposta.getSucursalSeguradora().isEmissora() || proposta.getSucursalSeguradora().isCorporate()) {
			validaAngariadorProposta(validador, proposta);
		} else {
			validarCorretorMasterProposta(validador, proposta);
		}
	}

	/**
	 * Valida o angariador da proposta verificando se ele foi preenchido
	 * 
	 * @param validador validador
	 * @param proposta proposta
	 */
	private void validaAngariadorProposta(Validador validador, PropostaVO proposta) {
		validador.verdadeiro(proposta.getAngariador() != null, "msg.erro.cadastro.proposta.vazia.produtor.angariador.obrigatorio");
	}

	/**
	 * Valida o corretor master da proposta e/ou gerente do produto.
	 * 
	 * <ul>
	 * 	<li>Valida se o corretor master foi preenchido</li>
	 * 	<li>Se ele foi preenchido, verifica se os dados s�o v�lidos</li>
	 * 	<li>Se for preenchido gerente de produto BVP, valida as informa��es</li>
	 * </ul>
	 * 
	 * @param validador validador
	 * @param proposta proposta
	 */
	private void validarCorretorMasterProposta(Validador validador, PropostaVO proposta) {
		CorretorMasterVO corretorMaster = proposta.getCorretorMaster();
		boolean corretorMasterPreenchido = corretorMaster != null && corretorMaster.isCpdPreenchido();

		validador.verdadeiro(corretorMasterPreenchido || proposta.isCodigoMatriculaGerentePreenchido(), "msg.erro.cadastro.proposta.vazia.produtor.bvp.ou.gerente.obrigatorio");

		if (corretorMasterPreenchido) {
			CorretorVO corretor = corretorSucursalServiceFacade.consultarCorretor(proposta.getSucursalSeguradora().getCodigo(), corretorMaster.getCpd());
			validador.verdadeiro(corretor != null, "msg.erro.cadastro.proposta.corretor.inexistente");
		}

		validarGerenteProdutoBVP(validador, proposta);
	}

	/**
	 * Valida o gerente de produto BVP
	 * 
	 * <ul>
	 * 	<li>Verifica se a matricula do gerente e a do assistente est�o preenchidas verificando que essas matriculas n�o podem ser iguais</li>
	 * 	<li>Verifica a exist�ncia do gerente, informando a sua matricula</li>
	 * </ul>
	 * 
	 * @param validador validador
	 * @param proposta proposta
	 */
	private void validarGerenteProdutoBVP(Validador validador, PropostaVO proposta) {
		if (proposta.isCodigoMatriculaGerentePreenchido()) {
			if (proposta.isCodigoMatriculaAssistentePreenchido()) {
				validador.falso(proposta.getCodigoMatriculaAssistente().equals(proposta.getCodigoMatriculaGerente()), "msg.erro.cadastro.proposta.gerente.produto.igual.assistente.producao");
			}
			validador.verdadeiro(assistenteProducaoDAO.verificarExistencia(proposta.getCodigoMatriculaGerente()), "msg.erro.cadastro.proposta.vazia.matricula.gerente.invalido");
		}
	}

	/**
	 * Valida assistente de produ��o
	 * 
	 * <ul>
	 * 	<li>Valida se o c�digo do assistente foi preenchido</li>
	 * 	<li>Se o assistente foi preenchido, verifica a sua exist�ncia atraves do seu c�digo</li>
	 * 	<li>Se existir gerente, verifica a sua exist�ncia atraves do seu c�digo</li>
	 * </ul>
	 * 
	 * @param validador validador
	 * @param proposta proposta
	 */
	private void validarAssistenteProducao(Validador validador, PropostaVO proposta) {
		boolean matriculaAssistentePreenchida = validador.obrigatorio(proposta.getCodigoMatriculaAssistente(), "C�digo do Assistente");
		if (matriculaAssistentePreenchida) {
			validador.verdadeiro(assistenteProducaoDAO.verificarExistencia(proposta.getCodigoMatriculaAssistente()), "msg.erro.cadastro.proposta.vazia.matricula.assistente.invalido");
		}
	}
}
