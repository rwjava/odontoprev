package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.BeneficiarioDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DependenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;

/**
 * Associação de beneficiários nas propostas 
 * 
 * @author WDEV
 */
@Scope("prototype")
@Named("AssociarBeneficiariosNasPropostas")
public class AssociarBeneficiariosNasPropostas extends AssociacaoEntidade<PropostaVO, Long, BeneficiarioVO, PropostaVO> {

	@Autowired
	private BeneficiarioDAO beneficiarioDAO;

	@Override
	public Long extrairIdentificadorEntidadesRelacionadas(PropostaVO entidade) {
		if (entidade == null) {
			return null;
		}
		return entidade.getNumeroSequencial();
	}

	@Override
	public List<BeneficiarioVO> consultarEntidadesRelacionada(Set<Long> idsRelacionados) {
		return beneficiarioDAO.listarPorPropostas(idsRelacionados);
	}

	@Override
	public Long extrairIdentificadorEntidadePai(BeneficiarioVO beneficiario) {
		if (beneficiario == null || beneficiario.getProposta() == null) {
			return null;
		}
		return beneficiario.getProposta().getNumeroSequencial();
	}

	@Override
	public PropostaVO associarEntidade(PropostaVO proposta, BeneficiarioVO beneficiario) {
		if (beneficiario != null) {
			if (beneficiario.isTitular()) {
				proposta.setTitular((TitularVO) beneficiario);
			} else {
				if (proposta.getDependentes() == null) {
					proposta.setDependentes(new ArrayList<DependenteVO>());
				}
				proposta.getDependentes().add((DependenteVO) beneficiario);
			}

		}
		return proposta;
	}

}