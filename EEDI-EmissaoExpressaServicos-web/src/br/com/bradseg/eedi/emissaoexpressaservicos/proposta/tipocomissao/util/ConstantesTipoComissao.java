package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.util;

public class ConstantesTipoComissao {
	
	public static final Integer COMISSAO_MENSAL_MERCADO_CORPORATE = 1;
	public static final Integer COMISSAO_MENSAL_MERCADO_ABREU_LALUPA = 28;
	public static final Integer COMISSAO_ANUAL_MERCADO_CORPORATE = 5;
	public static final Integer COMISSAO_MENSAL_REDE_CORRETOR_MASTER = 2;
	public static final Integer COMISSAO_ANUAL_REDE_CORRETOR_MASTER = 7;
	public static final Integer COMISSAO_MENSAL_REDE_GERENTE_PRODUTO = 3;
	public static final Integer COMISSAO_ANUAL_REDE_GERENTE_PRODUTO = 6;
	//Cria��o da Grade Qualisafe #Melhoria45
	public static final Integer COMISSAO_MENSAL_MERCADO_QUALISAFE = 55;
	
}
