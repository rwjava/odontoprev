package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.cxf.jaxrs.ext.Oneway;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.CanalVendaServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.GerarArquivoRelatorio;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.PlanoServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.RelatorioAcompanhamentoEEDIServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.RelatorioAcompanhamentoServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.RelatorioPropostaServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AcompanhamentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoAnsVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaCorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.RelatorioAcompanhamentoVO;

/**
 * WebService respons�vel por cadastrar, finalizar, cancelar, pesquisar e emitir propostas
 *  
 * @author WDEV
 */
@Service
@WebService
public class Proposta100PorCentoCorretorWebService {

	@Autowired
	private PropostaServiceFacade propostaServiceFacade;

	@Autowired
	private RelatorioAcompanhamentoServiceFacade relatorioAcompanhamentoServiceFacade;

	@Autowired
	private RelatorioPropostaServiceFacade relatorioPropostaServiceFacade;

	@Autowired
	private GerarArquivoRelatorio gerarArquivoRelatorio;

	@Autowired
	private PlanoServiceFacade planoServiceFacade;

	@Autowired
	private CanalVendaServiceFacade canalVendaServiceFacade;
	
	@Autowired
	private RelatorioAcompanhamentoEEDIServiceFacade relatorioAcompanhamentoEEDIServiceFacade;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaWebService.class);

	//@formatter:off
	
	/**
	 * Salva uma proposta como rascunho de forma que possa ser alterada novamente
	 * @param proposta Informa��es da proposta
	 * @return C�digo da proposta gerada
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_SEQUENCIAL_PROPOSTA) Long salvarRascunho(@WebParam(name = Constantes.WEB_SERVICE_PARAM_PROPOSTA) PropostaVO proposta) throws IntegrationException, BusinessException {
		return propostaServiceFacade.salvarRascunho(proposta).getNumeroSequencial();
	}

	/**
	 * Finaliza uma proposta, marcando sua situa��o como PENDENTE de forma que n�o pode mais ser atualizada pelo usu�rio 
	 * @param proposta Informa��es da proposta
	 * @return N�mero sequencial da proposta gerada
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_SEQUENCIAL_PROPOSTA) Long finalizar(@WebParam(name = Constantes.WEB_SERVICE_PARAM_PROPOSTA) PropostaVO proposta) throws IntegrationException, BusinessException {
		LOGGER.error("INFO: FINALIZAR PROPOSTA: "+proposta);
		Long numeroSequencial = propostaServiceFacade.salvarPendente(proposta).getNumeroSequencial();
		LOGGER.error("INFO: SEQUENCIAL RETORNADO: "+ numeroSequencial);
		return numeroSequencial;
	}
	
	/**
	 * Finaliza uma proposta, marcando sua situa��o como PENDENTE de forma que n�o pode mais ser atualizada pelo usu�rio 
	 * @param proposta Informa��es da proposta
	 * @return N�mero sequencial da proposta gerada
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_SEQUENCIAL_PROPOSTA) Long finalizarProposta(@WebParam(name = Constantes.WEB_SERVICE_PARAM_PROPOSTA) PropostaCorretorVO proposta) throws IntegrationException, BusinessException {
		LOGGER.error("INFO: FINALIZAR PROPOSTA 100 CORRETOR: "+proposta);
		Long numeroSequencial = propostaServiceFacade.salvarProposta100CorretorPendente(proposta).getNumeroSequencial();
		LOGGER.error("INFO: SEQUENCIAL RETORNADO: "+ numeroSequencial);
		return numeroSequencial;
	}
	
	/**
	 * Finaliza uma proposta, marcando sua situa��o como RASCUNHO de forma que  pode  ser atualizada pelo usu�rio 
	 * @param proposta Informa��es da proposta
	 * @return N�mero sequencial da proposta gerada
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_SEQUENCIAL_PROPOSTA) Long salvarPropostaVazia(@WebParam(name = Constantes.WEB_SERVICE_PARAM_PROPOSTA) PropostaVO proposta) throws IntegrationException, BusinessException {
		LOGGER.error("INFO: FINALIZAR PROPOSTA VAZIA: "+proposta);
		Long numeroSequencial = propostaServiceFacade.salvarPropostaVazia(proposta).getNumeroSequencial();
		LOGGER.error("INFO: SEQUENCIAL RETORNADO: "+ numeroSequencial);
		return numeroSequencial;
	}
	
	/**
	 * Cancela uma proposta pendente, atualizando sua situa��o para CANCELADA
	 * @param proposta Informa��es da proposta
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public PropostaVO cancelar(@WebParam(name = Constantes.WEB_SERVICE_PARAM_SEQUENCIAL_PROPOSTA) Long sequencialProposta) throws IntegrationException, BusinessException {
		LOGGER.error("INFO: CANCELAR PROPOSTA: "+sequencialProposta);
		return propostaServiceFacade.cancelar(sequencialProposta); 
	}

	/**
	 * Cancela uma proposta pendente, atualizando sua situa��o para  PR�-CANCELADA
	 * @param proposta Informa��es da proposta
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public void cancelarPropostaPorProtocolo(@WebParam(name = Constantes.WEB_SERVICE_PARAM_PROTOCOLO) String protocolo,@WebParam(name = Constantes.WEB_SERVICE_PARAM_CANAL_VENDA) Integer canalVenda) throws IntegrationException, BusinessException {
		propostaServiceFacade.cancelarPropostaPorProtocolo(protocolo, canalVenda);
	}
	
	/**
	 * Lista propostas por cpf, canal de venda e per�odo
	 * @param proposta Informa��es da proposta
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PROPOSTAS) List<PropostaVO> listarPropostasPorProponente(@WebParam(name = Constantes.WEB_SERVICE_PARAM_CPF_PROPONENTE) 
	String cpfProponente,@WebParam(name = Constantes.WEB_SERVICE_PARAM_CANAL_VENDA) Integer canalVenda, @WebParam(name = Constantes.WEB_SERVICE_PARAM_PERIODO_INICIAL) 
	@XmlJavaTypeAdapter(type = org.joda.time.DateTime.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DateTimeAdapter.class)DateTime periodoInicial, 
	@WebParam(name = Constantes.WEB_SERVICE_PARAM_PERIODO_FINAL) @XmlJavaTypeAdapter(type = org.joda.time.DateTime.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.DateTimeAdapter.class) DateTime periodoFinal ) throws IntegrationException, BusinessException {
		return propostaServiceFacade.listarPropostasPorProponente(cpfProponente, canalVenda, periodoInicial, periodoFinal);
	}
	
	
	/**
	 * Lista as proposta a partir do filtro informado
	 * @param filtroProposta Filtro da proposta
	 * @param login Dados do usu�rio logado
	 * @return Lista de propostas encontradas
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PROPOSTAS) List<PropostaVO> listarPorFiltro(@WebParam(name = Constantes.WEB_SERVICE_PARAM_FILTRO) FiltroPropostaVO filtroProposta, @WebParam(name = Constantes.WEB_SERVICE_PARAM_LOGIN) LoginVO login) throws IntegrationException, BusinessException {
		return propostaServiceFacade.listarPorFiltro(filtroProposta, login);
	}
	
	/**
	 * Lista as proposta a partir do filtro informado
	 * @param filtroProposta Filtro da proposta
	 * @param login Dados do usu�rio logado
	 * @return Lista de propostas encontradas
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PROPOSTAS) List<PropostaVO> listarPropostasIntranetPorFiltro(@WebParam(name = Constantes.WEB_SERVICE_PARAM_FILTRO) FiltroPropostaVO filtroProposta, @WebParam(name = Constantes.WEB_SERVICE_PARAM_LOGIN) LoginVO login) throws IntegrationException, BusinessException {
		return propostaServiceFacade.listarPropostasIntranetFiltro(filtroProposta, login);
	}

	/**
	 * Consulta as informa��es de uma proposta a partir do seu c�digo
	 * @param codigoProposta C�digo da proposta 
	 * @return Informa��es da proposta
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PROPOSTA) PropostaVO obterPorCodigo(@WebParam(name = Constantes.WEB_SERVICE_PARAM_CODIGO_PROPOSTA) String codigoProposta) throws IntegrationException, BusinessException {
		return propostaServiceFacade.consultarPorCodigo(codigoProposta);
	}

	/**
	 * Lista as informa��es da proposta para exibir o relat�rio de acompanhamento
	 * @param filtro Filtro de pesquisa
	 * @param login Dados do usu�rio logado
	 * @return Lista de propostas para relat�rio de acompanhamento
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_RELATORIO_ACOMPANHAMENTO) RelatorioAcompanhamentoVO listarParaRelatorioAcompanhamento(@WebParam(name = Constantes.WEB_SERVICE_PARAM_FILTRO) FiltroRelatorioAcompanhamentoVO filtro, @WebParam(name = Constantes.WEB_SERVICE_PARAM_LOGIN) LoginVO login) throws IntegrationException, BusinessException {
		return relatorioAcompanhamentoServiceFacade.listarPropostasPorFiltro(filtro, login);
	}

	/**
	 * Gera o relat�rio de acompanhamento em PDF
	 * @param filtro Filtro de pesquisa para listar os dados do relat�rio de acompanhamento
	 * @param login Dados do usu�rio logado
	 * @return Array de bytes para montar o relat�rio de acompanhamento
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_BYTES_ARQUIVO_RELATORIO) byte[] gerarRelatorioAcompanhamento(@WebParam(name = Constantes.WEB_SERVICE_PARAM_FILTRO) FiltroRelatorioAcompanhamentoVO filtro, @WebParam(name = Constantes.WEB_SERVICE_PARAM_LOGIN) LoginVO login) throws IntegrationException, BusinessException {
		return gerarArquivoRelatorio.gerar(relatorioAcompanhamentoServiceFacade.gerarPDF(filtro, login));
	}

	/**
	 * Emite a ap�lice da proposta em PDF
	 * @param codigoProposta C�digo da proposta
	 * @param login Dados do usu�rio logado
	 * @return Array de bytes para montar a ap�lice da proposta
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = "pdfProposta") byte[] gerarPDFProposta(@WebParam(name = "codigoProposta") Long codigoProposta, @WebParam(name = "gerarViaOperadora") boolean gerarViaOperadora, @WebParam(name = "gerarViaCorretor") boolean gerarViaCorretor, @WebParam(name = "gerarBoleto") boolean gerarBoleto) throws IntegrationException, BusinessException {
		return gerarArquivoRelatorio.gerar(relatorioPropostaServiceFacade.gerarRelatorioPropostaCompleta(codigoProposta, gerarViaOperadora, gerarViaCorretor, gerarBoleto));
	}
	
	/**
	 * Obt�m o canal de venda do call center
	 * @return Canal de venda do Call Center
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_CANAL_VENDA) CanalVendaVO obterCanalVendaCallCenter() throws IntegrationException, BusinessException {
		return canalVendaServiceFacade.obterCanalVendaCallCenter();
	}
	
	/**
	 * Metodo responsavel por listar os planos vigente associados a um determinado canal.
	 * 
	 * @param codigoCanal - codigo do canal.
	 * @return List<PlanoVO> - lista com os planos vigentes.
	 * @throws IntegrationException - Ser� lan�ado um erro para qualquer problema de integra��o.
	 * @throws BusinessException - Ser� lan�ado um erro para qualquer problema de neg�cio.
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PLANOS) List<PlanoVO> listarPlanosVigentePorCanal(@WebParam(name = "codigoCanal") Integer codigoCanal) throws IntegrationException, BusinessException {
		return planoServiceFacade.listarPlanosVigentePorCanal(codigoCanal);
	}
	
	/*
	 * Metodo responsavel por listar os planos vigente associados a um determinado canal e Segmento.
	 * @return List<PlanoVO> - lista com os planos vigentes.
	 * @return Segmento - segmento vigente.
	 * @throws IntegrationException - Ser� lan�ado um erro para qualquer problema de integra��o.
	 * @throws BusinessException - Ser� lan�ado um erro para qualquer problema de neg�cio.
	 * */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PLANOS) List<PlanoAnsVO> listarPlanosVigentesPorCanalESegmento(@WebParam(name = "codigoCanal") Integer codigoCanal, @WebParam(name = "codigoSegmento") Integer codigoSegmento) throws IntegrationException, BusinessException {
		return planoServiceFacade.listarPlanosVigentesPorCanalESegmento(codigoCanal, codigoSegmento);
	}
	
	/**
	 * Metodo responsavel por obter as informa��es do plano atrav�s do c�digo.
	 * 
	 * @param codigoPlano - codigo do plano.
	 * @return PlanoVO - informa��es do plano obtido.
	 * @throws IntegrationException - Ser� lan�ado um erro para qualquer problema de integra��o.
	 * @throws BusinessException - Ser� lan�ado um erro para qualquer problema de neg�cio.
	 */
	@WebMethod
	public @WebResult(name = "plano") PlanoVO obterPlanoPorCodigo(@WebParam(name = "codigoPlano") Long codigoPlano) throws IntegrationException, BusinessException {
		return planoServiceFacade.obterPlanoPorCodigo(codigoPlano);
	}
	
	@WebMethod
	public @WebResult(name = "plano") PlanoAnsVO obterPlanoAnsPorCodigo(@WebParam(name = "codigoPlano") Long codigoPlano) throws IntegrationException, BusinessException {
		return planoServiceFacade.obterPlanoAnsPorCodigo(codigoPlano);
	}
	
	@WebMethod
	public @WebResult(name = "termoDeAdesao") byte[] obterTermoDeAdesao(@WebParam(name = "codigoProposta") Long codigoProposta) throws IntegrationException, BusinessException {
		return gerarArquivoRelatorio.gerar(relatorioPropostaServiceFacade.gerarTermoDeAdesao(codigoProposta));
	}
	
	@WebMethod
	public @WebResult(name = "boleto") byte[] gerarBoleto(@WebParam(name = "codigoProposta") Long codigoProposta) throws IntegrationException, BusinessException {
		return gerarArquivoRelatorio.gerar(relatorioPropostaServiceFacade.gerarBoleto(codigoProposta));
	}
	
	/**
	 * Metodo resposanvel por atualizar o status das propostas na base do shopping de seguros.
	 * 
	 * @throws IntegrationException - Ser� lan�ado um erro para qualquer problema de integra��o.
	 * @throws BusinessException - Ser� lan�ado um erro para qualquer problema de neg�cio.
	 */
	@WebMethod
	public void atualizarStatusDaPropostaNoShoppingDeSeguros() throws IntegrationException, BusinessException {
		propostaServiceFacade.atualizarStatusDaPropostaNoShoppingDeSeguros();
	}
	
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PROPOSTA) PropostaVO obterPropostaPorCodigo(@WebParam(name = Constantes.WEB_SERVICE_PARAM_CODIGO_PROPOSTA) Long codigoProposta) throws IntegrationException, BusinessException {
		return propostaServiceFacade.consultarPorSequencial(codigoProposta);
	}
	
	
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PROPOSTAS) List<PropostaVO> listarPropostasPorBeneficiario(@WebParam(name = "cpfBeneficiario") String cpfBeneficiario, @WebParam(name = "canal") Integer canal) throws IntegrationException, BusinessException {
		return propostaServiceFacade.listarPropostasPorBeneficiario(cpfBeneficiario, canal);
	}
	
	@WebMethod
	public void cancelarPropostaShoppingSeguros(@WebParam(name = "sequencialProposta") Long sequencialProposta, @WebParam(name = "protocolo") String protocolo) throws IntegrationException, BusinessException {
		propostaServiceFacade.cancelarPropostaShoppingSeguros(sequencialProposta, protocolo);
	}
	
	
	
	/**
	 * Lista as informa��es da proposta para exibir o relat�rio de acompanhamento
	 * @param filtro Filtro de pesquisa
	 * @param login Dados do usu�rio logado
	 * @return Lista de propostas para relat�rio de acompanhamento
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_RELATORIO_ACOMPANHAMENTO)  List<AcompanhamentoPropostaVO> listarPropostasAcompanhamento(@WebParam(name = Constantes.WEB_SERVICE_PARAM_FILTRO)FiltroAcompanhamentoVO filtro, @WebParam(name = Constantes.WEB_SERVICE_PARAM_LOGIN) LoginVO login, @WebParam(name = Constantes.WEB_SERVICE_PARAM_IS_INTRANET)  boolean isIntranet)throws IntegrationException, BusinessException  {
		return relatorioAcompanhamentoEEDIServiceFacade.listarPropostasAcompanhamento(filtro, login, isIntranet);
	}
	
	
	/**
	 * Gera o relat�rio de acompanhamento em PDF
	 * @param filtro Filtro de pesquisa para listar os dados do relat�rio de acompanhamento
	 * @param login Dados do usu�rio logado
	 * @return Array de bytes para montar o relat�rio de acompanhamento
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_BYTES_ARQUIVO_RELATORIO) byte[] gerarRelatorioAcompanhamentoEEDI(@WebParam(name = Constantes.WEB_SERVICE_PARAM_FILTRO)FiltroAcompanhamentoVO filtro, @WebParam(name = Constantes.WEB_SERVICE_PARAM_LOGIN) LoginVO login) throws IntegrationException, BusinessException {
		return gerarArquivoRelatorio.gerar(relatorioAcompanhamentoServiceFacade.gerarPDFRelatorioAcompanhamento(filtro, login));
	}
	
	
	/**
	 * Metodos responsavel por listar canais de venda disponiveiz.
	 * @return lista de canais de venda.
	 * @throws IntegrationException
	 * @throws BusinessException
	 */
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_CANAIS_VENDA)  List<CanalVendaVO> listarCanaisVenda() throws IntegrationException, BusinessException {
	
		return canalVendaServiceFacade.listarCanaisVenda();
	}
	//@formatter:on
	
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PROPOSTAS)  List<String> consultarProposta( @WebParam(name = Constantes.WEB_SERVICE_PARAM_FILTRO) String consulta) throws IntegrationException, BusinessException{
		return propostaServiceFacade.consultarProposta(consulta);
	}
	
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_CANAL_VENDA)  CanalVendaVO obterCanalVendaPorCodigo(Integer codigo) throws IntegrationException, BusinessException {
	
		return canalVendaServiceFacade.obterPorCodigo(codigo);
	}
	
	@WebMethod
	public @WebResult(name = Constantes.WEB_SERVICE_PARAM_PLANOS)  List<PlanoVO> listarPlanoDisponivelParaVenda( @WebParam(name = Constantes.WEB_SERVICE_PARAM_SEGMENTO) Integer codigoSegmento) throws IntegrationException, BusinessException{
		return planoServiceFacade.listarPlanoDisponivelParaVenda(codigoSegmento);
	}
	
	@WebMethod
	@Oneway
	public String cancelarPropostasDoSite100CorretorComDataDeValidadeExpirada() throws IntegrationException, BusinessException {
		return propostaServiceFacade.cancelarPropostasDoSite100CorretorComDataDeValidadeExpirada();
	}


}
