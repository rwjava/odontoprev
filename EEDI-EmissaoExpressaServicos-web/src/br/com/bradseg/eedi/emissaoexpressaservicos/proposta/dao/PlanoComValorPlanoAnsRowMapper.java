package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoAnsVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ValorPlanoVO;

/**
 * Row Mapper da classe Plano
 * 
 * @author Bradesco Seguros
 */

public class PlanoComValorPlanoAnsRowMapper implements RowMapper<PlanoAnsVO> {

	public PlanoAnsVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		PlanoAnsVO plano = new PlanoAnsVO();
		plano.setValorPlanoVO(new ValorPlanoVO());
		
		//plano.setPlanoAnsVO(new PlanoAnsVO());
		plano.setCodigo(rs.getLong("CPLANO_DNTAL_INDVD"));
		plano.setNome(rs.getString("RPLANO_DNTAL_INDVD"));
		
		plano.setNomePlanoAns(rs.getString("RPLANO_ANS_DNTAL_INDVD"));
		plano.setidPlano(rs.getLong("CREG_PLANO_DNTAL"));
		plano.setcodigoPlanoAns(rs.getLong("CPLANO_ANS_DNTAL_INDVD"));
		
		plano.getValorPlanoVO().setValorMensalTitular(rs.getDouble("VMESD_PLANO_TTLAR"));
		plano.getValorPlanoVO().setValorMensalDependente(rs.getDouble("VMESD_PLANO_DEPDT"));
		plano.getValorPlanoVO().setValorAnualTitular(rs.getDouble("VANUDD_PLANO_DNTAL_TTLAR"));
		plano.getValorPlanoVO().setValorAnualDependente(rs.getDouble("VANUDD_PLANO_DNTAL_DEPDT"));
		String descricaoCarenciaPeriodoAnualPipe = rs.getString("RPER_CAREN_PLANO_ANO");
		String descricaoCarenciaPeriodoMensalPipe = rs.getString("RPER_CAREN_PLANO_MES");
		if(descricaoCarenciaPeriodoAnualPipe != null){
			descricaoCarenciaPeriodoAnualPipe = descricaoCarenciaPeriodoAnualPipe.replaceAll(", ", "| ");
		}
		if(descricaoCarenciaPeriodoMensalPipe != null){
			descricaoCarenciaPeriodoMensalPipe = descricaoCarenciaPeriodoMensalPipe.replaceAll(", ", "| ");
		}
		plano.setDescricaoCarenciaPeriodoAnual(descricaoCarenciaPeriodoAnualPipe);
		plano.setDescricaoCarenciaPeriodoMensal(descricaoCarenciaPeriodoMensalPipe);
		plano.setDataInicioVigencia(new DateTime(rs.getDate("DINIC_PLANO_INDVD")));

		return plano;
	}

}
