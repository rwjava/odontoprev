package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.util;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca;

public class TipoComissaoMercadoCorporate implements TipoComissao {

	public Integer obterTipoComissao(Integer tipoCobranca, boolean corretorNovaComissao) {

		Integer tipoComissao = null;
		if (TipoCobranca.MENSAL.getCodigo().equals(tipoCobranca)) {
			if(corretorNovaComissao){
				tipoComissao = ConstantesTipoComissao.COMISSAO_MENSAL_MERCADO_ABREU_LALUPA;
			}else{
				tipoComissao = ConstantesTipoComissao.COMISSAO_MENSAL_MERCADO_CORPORATE;	
			}
			
		} else if (TipoCobranca.ANUAL.getCodigo().equals(tipoCobranca)) {
			tipoComissao = ConstantesTipoComissao.COMISSAO_ANUAL_MERCADO_CORPORATE;
		}
		return tipoComissao;
	}
	
	public Integer obterTipoComissaoQualisafe(Integer tipoCobranca, boolean corretorNovaComissao) {

		Integer tipoComissao = null;
		if (TipoCobranca.MENSAL.getCodigo().equals(tipoCobranca)) {
			if(corretorNovaComissao){
				tipoComissao = ConstantesTipoComissao.COMISSAO_MENSAL_MERCADO_QUALISAFE;
			}else{
				tipoComissao = ConstantesTipoComissao.COMISSAO_MENSAL_MERCADO_CORPORATE;	
			}
			
		} else if (TipoCobranca.ANUAL.getCodigo().equals(tipoCobranca)) {
			tipoComissao = ConstantesTipoComissao.COMISSAO_ANUAL_MERCADO_CORPORATE;
		}
		return tipoComissao;
	}

}
