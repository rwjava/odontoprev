package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoAnsVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoSegmtVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Lista os tipos de planos dispon�veis para a proposta
 * 
 * @author WDEV
 */
public interface PlanoDAO {

	/**
	 * Consulta os planos de benef�cios dispon�veis para a proposta
	 * @return Lista de planos dispon�veis
	 */
	public List<PlanoVO> listar();

	/**
	 * Consulta plano por c�digo
	 * @param codigoPlano
	 * @return PlanoVO - objeto contendo informa��es do plano
	 */
	
	public PlanoVO buscaPlanoPorCodigo(Integer codigoPlano);
	
	/**
	 * Obt�m o plano atual 
	 * @param codigo - c�digo do canal de venda
	 * @return planoVO
	 */
	public PlanoVO obterPlanoAtualPorCodigoCanal(Integer codigo);
	
	/**
	 * Listar todos os planos vigente associados a um determinado canal.
	 * 
	 * @param codigoCanal - c�digo do canal.
	 * 
	 * @return List<PlanoVO> - lista de planos vigente associados ao canal informado.
	 */
	public List<PlanoVO> listarPlanosVigentePorCanal(Integer codigoCanal);
	
	public List<PlanoAnsVO> listarPlanosVigentePorCanalPlanoAns(Integer codigoCanal);
	
	public List<PlanoAnsVO> listarPlanosVigentesPorCanalESegmento(Integer codigoCanal, Integer segmento);
	
	public List<PlanoSegmtVO> listarPlanosVigentesPorCanalESegmentoBanco(Integer codigoCanal, Integer segmento);
	
	/**
	 * Obter as informa��es de um plano atrav�s do c�digo.
	 * 
	 * @param codigoPlano - c�digo do plano a ser obtido.
	 * @return PlanoVO - informa��es do plano obtido.
	 */
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano);
	
	public PlanoAnsVO obterPlanoAnsPorCodigo(Long codigoPlano);

	
	public PlanoVO obterPlanoPorDataEmissaoProposta(PropostaVO proposta);

	public List<PlanoVO> listarPlanoDisponivelParaVenda(Integer codigoSegmento);

	public Boolean isPlanoVigente(Integer codigoPlano);

}