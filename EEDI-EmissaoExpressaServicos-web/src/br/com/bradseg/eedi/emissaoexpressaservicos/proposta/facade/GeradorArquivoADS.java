package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Arquivo;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;

/**
 * Gera um arquivo ADS
 * @author WDEV
 */
public class GeradorArquivoADS {

	public Arquivo gerar(Integer sequencial, LocalDate dataInicialPesquisaMovimento, List<PropostaVO> propostas) {

		DateTime dataGeracaoArquivo = new DateTime();
		int totalRegistros = 2;

		Arquivo arquivo = new Arquivo();
		arquivo.setNome(getNomeArquivo(sequencial));

		new Cabecalho().escrever(arquivo, dataGeracaoArquivo, dataInicialPesquisaMovimento);

		for (PropostaVO proposta : propostas) {
			// Escreve os detalhes do titular
			TitularVO titular = proposta.getTitular();
			titular.setProposta(proposta);
			new Detalhe(titular).escrever(arquivo);

			totalRegistros++;

			// Escreve os detalhes para cada dependente
			for (BeneficiarioVO dependente : proposta.getDependentes()) {
				dependente.setProposta(proposta);
				new Detalhe(dependente).escrever(arquivo);

				totalRegistros++;
			}

		}

		new Rodape().escrever(arquivo, totalRegistros, dataGeracaoArquivo);

		return arquivo;
	}

	/**
	 * Monta o nome do arquivo ADS 
	 * 
	 * @param sequencial sequencial da proposta
	 * @return nome do arquivo
	 */
	private String getNomeArquivo(Integer sequencial) {
		StringBuilder nomeArquivo = new StringBuilder();
		nomeArquivo.append(Constantes.PREFIXO_ARQUIVO_ADS).append(StringUtils.leftPad(sequencial.toString(), 8, "0")).append(Constantes.EXTENSAO_ARQUIVO).append(".txt");
		return nomeArquivo.toString();
	}

	/**
	 * Cabe�alho do arquivo ADS
	 * @author WDEV
	 */
	protected static final class Cabecalho {
		public void escrever(Arquivo arquivo, DateTime dataGeracaoArquivo, LocalDate dataMovimento) {
			//@formatter:off
			arquivo
				.texto(Constantes.CODIGO_HEADER)
				.data(dataGeracaoArquivo, Constantes.FORMATO_DT_GERACAO_ARQUIVO)
				.data(dataMovimento, Constantes.FMD_DATA_BD)
				.data(new LocalDate(), Constantes.FMD_DATA_BD)
				.texto("", 8)
				.texto("")
				.quebraLinha();
			//@formatter:on
		}
	}

	/**
	 * Rodap� do arquivo ADS
	 * @author WDEV
	 */
	protected static final class Rodape {
		public void escrever(Arquivo arquivo, int totalRegistros, DateTime dataGeracaoArquivo) {
			//@formatter:off
			arquivo
				.texto(Constantes.CODIGO_TRAILLER)
				.data(dataGeracaoArquivo, Constantes.FORMATO_DT_GERACAO_ARQUIVO)
				.numero(totalRegistros, 9)				
				.quebraLinha();
			//@formatter:on
		}
	}

	/**
	 * Detalhes de cada dependente da proposta do arquivo ADS
	 * @author WDEV
	 */
	protected static final class Detalhe {
		private BeneficiarioVO beneficiario;
		private PropostaVO proposta;

		public Detalhe(BeneficiarioVO beneficiario) {
			this.beneficiario = beneficiario;
			this.proposta = beneficiario.getProposta();
		}

		public void escrever(Arquivo arquivo) {
			//@formatter:off			
			arquivo
				.texto(Constantes.CODIGO_DETALHE)
				.numero(proposta.getSucursalSeguradora().getCodigo(), 3)
				.numero(proposta.getCorretor().getCpd(), 6);
			//@formatter:on

			if (proposta.getAgenciaProdutora().getCodigo() != null) {
				arquivo.numero(proposta.getAgenciaProdutora().getCodigo(), 6);
			} else {
				arquivo.numero(0, 6);
			}
			//@formatter:off
			// Data inicio do movimento
			arquivo
			.data(proposta.getMovimento().getDataInicio(), Constantes.FMD_DATA_BD)
			// Data do cadastro da ap�lice
			.data(proposta.getMovimento().getDataInicio(), Constantes.FMD_DATA_BD)
			.texto(Constantes.SEGMENTO_SAUDE, 5)
			.texto(Constantes.COD_PRODUTO_INDIVIDUAL, 8)
			.texto(Constantes.COD_SUBPRODUTO_INDVD_DNTAL, 8)
			.numero(Constantes.COD_SISTEMA_LEGADO, 3)
			.texto(montarChaveApolice(), 40)
			.texto(beneficiario.getNome(), 40)
			.numero(Constantes.NUMERO_PARCELAS, 2)
			.numero(Constantes.TIPO_MOVIMENTO_VENDA, 2);
			
			//BigInteger valorMensalidadeTitular = proposta.getTitular().getPlano().getValorMensalidadeTitular().unscaledValue();
			BigInteger valorMensalidadeTitular = new BigInteger(String.valueOf(proposta.getPlanoVO().getValorPlanoVO().getValorMensalTitular()));
			arquivo
			.numero(valorMensalidadeTitular, 17)			
			.numero(1, 10) // NUMERO DE PROPOSTAS EFETUADAS
			.numero(proposta.getDependentes().size() + 1, 10)
			.numero(Long.valueOf(proposta.getCodigoMatriculaAssistente()), 9)
			.data(proposta.getMovimento().getDataInicio(), Constantes.FMD_DATA_BD)
			.numero(Long.valueOf(proposta.getTitular().getCpf(), 15))
			.numero(0, 15) // CNPJ SUBESTIPULANTE
			.numero(0, 7) // NUMERO APOLICE CONJUGADA
			.numero(0, 7) // NUMERO APOLICE ANTERIOR
			.numero(0, 4) // SUBFATURA1
			.numero(0, 4) // SUBFATURA2
			.numero(0, 4) // SUBFATURA3
			.texto("0") //IDENTIFICA SE � APOLICE OU SUBFATURA
			.texto(Constantes.IDENTIFICADOR_APOLICE_BDA, 3)
			.numero(Integer.valueOf(recuperarNumeroApolice(proposta.getCodigo()), 12)) 
			.numero(proposta.getNumeroSequencial() + beneficiario.getCodigo(), 20) // TODO NUMERO MATRICULA SEGURADO NAO SABEMOS SE CONCATENA OU SOMA
			.texto(" ", 4)
			.quebraLinha();
		}
		
		private String montarChaveApolice() {
			Arquivo chaveApolice = new Arquivo();
			chaveApolice.setSeparador(Constantes.SEPARADOR_CHAVE_APOLICE_ARQUIVO_ADS);
			//@formatter:off
			chaveApolice
				.numero(Constantes.COD_SISTEMA_LEGADO, 3)
				.texto(obtemNumeroApoliceChaveOrigem(proposta.getCodigo()), 10)
				.numero(1, 5)
				.numero(proposta.getMovimento().getDataInicio().getYear(), 4)
				.numero(proposta.getMovimento().getDataInicio().getMonthOfYear(), 2);

			Long certificado = proposta.getNumeroSequencial() + beneficiario.getCodigo(); // 1 + 1 = 2 /// "1" + "1" = 11 TODO Somar ou concatenar????

			chaveApolice
				.numero(certificado , 5)	
				.texto(Constantes.SEPARADOR_CHAVE_APOLICE_ARQUIVO_ADS);
			
			//@formatter:on

			if (beneficiario instanceof TitularVO) {
				chaveApolice.numero(Integer.valueOf(0), 5);
			} else {
				chaveApolice.numero(beneficiario.getCodigo(), 5);
			}

			return chaveApolice.asString(); // TODO verificar se ficar� com Separador ("/") no final
		}

		private String obtemNumeroApoliceChaveOrigem(String codigoProposta) {
			String codigoChave = "";
			String codigo = recuperarNumeroApolice(codigoProposta);
			codigoChave = codigo.replace(codigo.charAt(0), ' ');
			return codigoChave.trim();
		}

		private String recuperarNumeroApolice(String codigoProposta) {
			return codigoProposta.split("A")[1];
		}
	}
}
