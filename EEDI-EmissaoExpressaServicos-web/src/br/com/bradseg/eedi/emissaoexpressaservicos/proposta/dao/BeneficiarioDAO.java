package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;
import java.util.Set;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DependenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;

/**
 * Dados dos Beneficiarios da proposta
 * 
 * @author WDEV
 */
public interface BeneficiarioDAO {

	/**
	 * Lista todos os beneficiários das propostas
	 * 
	 * @param proposta PropostaVO
	 * @return Lista de beneficiários
	 */
	public List<BeneficiarioVO> listarPorProposta(PropostaVO proposta);

	/**
	 * Lista todos os beneficiários das propostas
	 * 
	 * @param idsPropostas Identificadores das propostas
	 * @return Lista de beneficiários
	 */
	public List<BeneficiarioVO> listarPorPropostas(Set<Long> idsPropostas);

	/**
	 * Salva os titulares da proposta
	 * @param titular Titular da proposta
	 * @param integer 
	 */
	public void salvarTitular(TitularVO titular, Integer integer);
	
	public void salvarTitularServicos(TitularVO titular);

	/**
	 * Salva os dependentes
	 * @param dependentes Dependentes da proposta
	 */
	public void salvarDependentes(List<DependenteVO> dependentes, TitularVO titular);

	/**
	 * Remove o beneficiário
	 * @param proposta Proposta
	 */
	public void remover(PropostaVO proposta);

	/**
	 * Lista a última situação da proposta do beneficiário pelo cpf e canal de venda
	 * 
	 * @param cpf cpf do Beneficiário
	 * @param canalVenda Canal de venda
	 * @return a última situação da proposta do beneficiário informado
	 */
	public SituacaoProposta listarUltimaSituacaoPropostaBeneficiario(String cpf, CanalVendaVO canalVenda);
	
	/**
	 * Lista a última situação da proposta do beneficiário
	 * 
	 * @param cpf cpf do Beneficiário
	 * @return a última situação da proposta do beneficiário informado
	 */
	public SituacaoProposta listarUltimaSituacaoPropostaBeneficiario(String cpf);
	

	/**
	 * Atualiza o numero da carteira nacional do beneficiário
	 * 
	 * @param numeroCarteiraNacional Numero da carteira nacional do beneficiário
	 * @param cpfBeneficiario cpf do beneficiário
	 * @param sequencialProposta sequencial da proposta
	 */
	public void atualizarCarteiraNacional(String numeroCarteiraNacional, String cpfBeneficiario, Long sequencialProposta);
	

	public Boolean verificarSeBeneficiarioPossuiPropostaCancelada(String cpf);
	
	public List<Long> listarPropostasBeneficionario(String cpf); 
	
	public Boolean verificarStatusProposta(Long sequencialProposta, SituacaoProposta situacao);
}