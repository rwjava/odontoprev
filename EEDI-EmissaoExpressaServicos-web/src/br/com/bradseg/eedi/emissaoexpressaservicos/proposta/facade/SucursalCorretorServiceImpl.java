package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;


import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Se c�digo sucursal for 1 ou de 300 at� 399 usa emissora se for de 700 a 799 ou sucursal nova rede usa bvp
 * 
 * @author Bradesco Seguros
 */
@Component
public class SucursalCorretorServiceImpl implements SucursalCorretorService {

	// private static final List<Integer> FAIXA_300_MERCADO = Arrays.asList(300, 304, 307, 308, 309, 310, 312, 314, 315,
	// 316, 317, 318, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337,
	// 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 356, 358, 359, 360,
	// 361, 362, 363, 365, 366, 367, 368, 369, 370, 371, 372, 373, 376, 377, 378, 381, 382, 383, 384, 385, 386,
	// 387, 388, 389, 390, 392, 393, 394, 395, 396, 398, 399);

	private static final List<Integer> FAIXA_600_MERCADO = Arrays.asList(613, 614, 615, 616, 617, 618, 619, 620, 621,
		622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645,
		646, 647, 648, 649, 650, 651, 652, 653, 664, 665, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676,
		677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697,
		698, 699);

	private static final List<Integer> FAIXA_800_MERCADO = Arrays.asList(815, 816, 817, 818, 819, 820, 821, 822, 835,
		836, 837, 838, 839, 840, 841, 842, 843, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877);

	private static final List<Integer> FAIXA_900_MERCADO = Arrays.asList(901, 902, 903, 904, 905, 906, 907, 908, 909,
		911, 913, 914, 916, 917, 919, 921, 922, 923, 924, 925, 926, 928, 929, 930, 932, 933, 934, 936, 937, 938,
		940, 941, 942, 944, 946, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977,
		978, 979, 985, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999);

	private static final List<Integer> FAIXA_600_REDE = Arrays.asList(600, 601, 602, 603, 604, 605, 606, 607, 608, 609,
		610, 611, 612, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 666);

	// private static final List<Integer> FAIXA_700_REDE = Arrays.asList(700, 701, 702, 703, 704, 705, 706, 707, 708, 709,
	// 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730,
	// 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751,
	// 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772,
	// 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793,
	// 794, 795, 796, 797, 798, 799);

	private static final List<Integer> FAIXA_700_REDE = Arrays.asList(794, 798);

	private static final List<Integer> FAIXA_800_REDE = Arrays.asList(844, 845, 846, 847, 848, 849, 850, 851, 852, 855,
		856, 857, 858, 859, 860, 861, 862);

	private static final List<Integer> FAIXA_900_REDE = Arrays.asList(910, 915, 918, 927, 931, 935, 939, 943, 947, 948,
		949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 986);

	private static final List<Integer> FAIXA_600_CORPORATE = Arrays.asList(632, 633, 634);

	private static final List<Integer> FAIXA_800_CORPORATE = Arrays.asList(853, 854, 863, 864);

	private static final List<Integer> FAIXA_900_CORPORATE = Arrays.asList(912, 920, 945, 980, 981, 982, 983, 984);

	/**
	 * M�todo respons�vel por validar a sucursal do corretor.
	 */
	public boolean validarSucursalCorretor(Integer codigo) {
		return validarSucursalMercado(codigo) || validarSucursalRede(codigo) || validarSucursalCorporate(codigo);
	}

	/**
	 * M�todo respons�vel por validar a sucursal mercado.
	 */
	public boolean validarSucursalMercado(Integer codigo) {
		return (FAIXA_600_MERCADO.contains(codigo) || FAIXA_800_MERCADO.contains(codigo) || FAIXA_900_MERCADO.contains(codigo));
	}

	// return (FAIXA_300_EMISSORA.contains(codigo) || FAIXA_600_EMISSORA.contains(codigo)
	// || FAIXA_800_EMISSORA.contains(codigo) || FAIXA_900_EMISSORA.contains(codigo));
	// }

	/**
	 * M�todo respons�vel por validar a sucursal rede.
	 */
	public boolean validarSucursalRede(Integer codigo) {
		return (FAIXA_600_REDE.contains(codigo) || FAIXA_700_REDE.contains(codigo) || FAIXA_800_REDE.contains(codigo) || FAIXA_900_REDE.contains(codigo));
	}

	/**
	 * M�todo respons�vel por validar a sucursal corporate.
	 */
	public boolean validarSucursalCorporate(Integer codigo) {
		return (FAIXA_600_CORPORATE.contains(codigo) || FAIXA_800_CORPORATE.contains(codigo) || FAIXA_900_CORPORATE.contains(codigo));
	}

	// /**
	// * Novas sucursais de rede criadas. No momento elas ser�o tratadas como BVP.
	// */
	// private static final List<Integer> NOVAS_SUCURSAIS_REDE = Arrays.asList(910, 915, 918, 927, 931, 935, 939, 943, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961);
	//
	// /**
	// * Sucursais da faixa 300
	// */
	// private static final Integer SUCURSAL_FAIXA_300_INICIO = 300;
	// private static final Integer SUCURSAL_FAIXA_300_FIM = 399;
	// private static final List<Integer> SUCURSAIS_INVALIDAS = Arrays.asList(301, 302, 303, 305, 306, 311, 313, 319, 355, 357, 364, 374, 375, 379, 380, 391, 397, 900);
	//
	// /**
	// * Sucursais da faixa 600, sendo de 600 at� 612 e de 654 at� 663 tratadas como BVP.
	// */
	// private static final Integer SUCURSAL_FAIXA_600_INICIO = 600;
	// private static final Integer SUCURSAL_FAIXA_600_FIM = 699;
	// private static final Integer SUCURSAL_FAIXA1_600_BVP_INICIO = 600;
	// private static final Integer SUCURSAL_FAIXA1_600_BVP_FIM = 612;
	// // nova faixa BVP: 654 at� 663 - 21/03/2014
	// private static final Integer SUCURSAL_FAIXA2_600_BVP_INICIO = 654;
	// private static final Integer SUCURSAL_FAIXA2_600_BVP_FIM = 663;

	// public boolean validarSucursalEmissora(Integer codigo) {
	// // Inclu�do faixa 900 ~ 999, conforme solicitado � pedido do Maur�cio Quintella
	// return (isFaixa300Valida(codigo)) //
	// || (codigo >= 900 && codigo <= 999 && !NOVAS_SUCURSAIS_REDE.contains(codigo)) //
	// || (isFaixa600(codigo) && !isFaixa600BVP(codigo));
	// }
	//
	// public boolean validarSucursalBVP(Integer codigo) {
	// return (codigo >= 700 && codigo <= 799) //
	// || isFaixa600BVP(codigo) //
	// || NOVAS_SUCURSAIS_REDE.contains(codigo);
	// }
	//
	// private boolean isFaixa300Valida(Integer sucursal) {
	// return sucursal >= SUCURSAL_FAIXA_300_INICIO && sucursal <= SUCURSAL_FAIXA_300_FIM && !SUCURSAIS_FAIXA_300_INVALIDAS.contains(sucursal);
	// }
	//
	// private boolean isFaixa600(Integer sucursal) {
	// return sucursal >= SUCURSAL_FAIXA_600_INICIO && sucursal <= SUCURSAL_FAIXA_600_FIM;
	// }
	//
	// private boolean isFaixa600BVP(Integer sucursal) {
	// return ((sucursal >= SUCURSAL_FAIXA1_600_BVP_INICIO && sucursal <= SUCURSAL_FAIXA1_600_BVP_FIM) || (sucursal >= SUCURSAL_FAIXA2_600_BVP_INICIO && sucursal <= SUCURSAL_FAIXA2_600_BVP_FIM));
	// }
}
