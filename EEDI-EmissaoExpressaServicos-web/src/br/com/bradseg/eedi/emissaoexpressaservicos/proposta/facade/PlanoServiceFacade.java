package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import org.joda.time.LocalDate;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.vo.RetornoObterArquivoVo;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoAnsVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoSegmtVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;

/**
 * Lista os planos de benef�cios de uma proposta
 * 
 * @author WDEV
 *
 */
public interface PlanoServiceFacade {

	/**
	 * Lista os planos vigentes dispon�veis
	 * 
	 * @return Lista de planos vigentes
	 */
	public List<PlanoVO> listarPlanosVigentes();

	/**
	 * Lista os planos vigentes dispon�veis a partir da data
	 * 
	 * @param dataCriacao data criacao
	 * @return Lista de planos vigentes
	 */
	public List<PlanoVO> listarPlanosVigentesPorData(LocalDate dataCriacao);
	
	/**
	 * Busca plano por codigo.
	 * 
	 * @param codigoPlano the codigo plano
	 * @return the plano
	 */
	public PlanoVO buscarPlanoPorCodigo(Integer codigoPlano);

	/**
	 * Obt�m o plano atual 
	 * @param codigo - c�digo do canal de venda
	 * @return planoVO
	 */
	public PlanoVO obterPlanoAtualPorCodigoCanal(Integer codigo);
	
	/**
	 * Listar todos os planos vigente associados a um determinado canal.
	 * 
	 * @param codigoCanal - c�digo do canal.
	 * 
	 * @return List<PlanoVO> - lista de planos vigente associados ao canal informado.
	 */
	public List<PlanoVO> listarPlanosVigentePorCanal(Integer codigoCanal);
	
	public List<PlanoAnsVO> listarPlanosVigentesPorCanalESegmento(Integer codigoCanal ,Integer segmento);
	
	/**
	 * Listar os planos conforme par�metro de canal e segmento da conta corrente do banco.
	 * 
	 * @param codigoCanal - c�digo do canal.
	 * 
	 * @param codigoCanal - c�digo do segmento.
	 * 
	 * @return List<PlanoSegmtVO> - lista de planos vigente conforme par�metro de canal e segmento da conta corrente do banco.
	 */
	public List<PlanoSegmtVO> listarPlanosVigentesPorCanalESegmentoBanco(Integer codigoCanal ,Integer segmento);
	
	/**
	 * Obter as informa��es de um plano atrav�s do c�digo.
	 * 
	 * @param codigoPlano - c�digo do plano a ser obtido.
	 * @return PlanoVO - informa��es do plano obtido.
	 */
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano);

	public PlanoAnsVO obterPlanoAnsPorCodigo(Long codigoPlano);
	
	public List<PlanoVO> listarPlanoDisponivelParaVenda(Integer codigoSegmento);

	public RetornoObterArquivoVo gerarArquivoProduto(Integer codigoCanal, Integer codigoPlano, Integer codigoDocumento);
}
