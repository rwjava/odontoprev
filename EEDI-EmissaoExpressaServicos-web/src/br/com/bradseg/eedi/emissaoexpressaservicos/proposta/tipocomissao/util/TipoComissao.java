package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.util;

public interface TipoComissao {
	public Integer obterTipoComissao(Integer tipoCobranca, boolean corretorNovaComissao);
}
