package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;
import java.util.Set;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TelefoneVO;

/**
 * Dados do telefone do proponente
 * 
 * @author WDEV
 */
public interface TelefoneDAO {

	/**
	 * Lista os telefones do proponente
	 * 
	 * @param proponente ProponenteVO
	 * @return Lista de Telefones do proponente
	 */
	public List<TelefoneVO> listarPorProponente(ProponenteVO proponente);

	/**
	 * Lista os telefones do proponente
	 * 
	 * @param codigoProponente Código do proponente
	 * @return Lista de Telefones do proponente
	 */
	public List<TelefoneVO> listarPorProponentes(Set<Long> codigoProponente);

	/**
	 * Salva um novo telefone utilizado na proposta
	 * 
	 * @param telefone TelefoneVO
	 */
	public void salvar(TelefoneVO telefone);

	/**
	 * Salva o telefone do responsável legal do titular da proposta
	 * 
	 * @param telefone Telefone do responsável legal
	 */
	public void salvarTelefoneResponsavelLegal(TelefoneVO telefone);

	/**
	 * Remove os telefones do proponente 
	 * 
	 * @param proponente Proponente da proposta
	 */
	public void removerPorProponente(ProponenteVO proponente);

	/**
	 * Remove o telefone do responsável legal
	 * 
	 * @param responsavelLegal Responsável legal do titular da proposta
	 */
	public void removerTelefoneResponsavelLegal(ResponsavelLegalVO responsavelLegal);

}