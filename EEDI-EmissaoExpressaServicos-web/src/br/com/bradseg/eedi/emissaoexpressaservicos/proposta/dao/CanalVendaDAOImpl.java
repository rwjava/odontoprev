package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;

/**
 * Consulta as informações dos canais de venda disponíveis
 * 
 * @author WDEV
 */
@Repository
public class CanalVendaDAOImpl extends JdbcDao implements CanalVendaDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CanalVendaDAOImpl.class);

	@Autowired
	private DataSource dataSource;

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	public CanalVendaVO consultarPorCodigo(Integer codigo) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT CCANAL_VDA_DNTAL_INDVD, CSGL_CANAL_VDA_DNTAL_INDVD, RCANAL_VDA_DNTAL_INDVD");
		sql.append(" FROM DBPROD.CANAL_VDA_DNTAL_INDVD");
		sql.append(" WHERE CCANAL_VDA_DNTAL_INDVD = :codigo");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigo", Long.valueOf(codigo));

		try {
			return getJdbcTemplate().queryForObject(sql.toString(), params, new CanalVendaRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Canal de venda nao encontrado com o codigo informado", e);
			return null;
		}
	}
	
	public List<CanalVendaVO> listarCanaisVenda() {
		
		List<CanalVendaVO> listaDeCanais = new ArrayList<CanalVendaVO>();
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT CCANAL_VDA_DNTAL_INDVD, CSGL_CANAL_VDA_DNTAL_INDVD, RCANAL_VDA_DNTAL_INDVD");
		sql.append(" FROM DBPROD.CANAL_VDA_DNTAL_INDVD");

//		MapSqlParameterSource params = new MapSqlParameterSource();

		try {
			listaDeCanais = getJdbcTemplate().query(sql.toString(), new MapSqlParameterSource(), new CanalVendaRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Erro ao obter canais de venda: ", e);
			return null;
		}
		return listaDeCanais;
	}

	/**
	 * Mapeamento dos dados da listagem de planos
	 * 
	 * @author WDEV
	 */
	private static final class CanalVendaRowMapper implements RowMapper<CanalVendaVO> {
		public CanalVendaVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
			CanalVendaVO canalVenda = new CanalVendaVO();
			canalVenda.setCodigo(resultSet.getInteger("CCANAL_VDA_DNTAL_INDVD"));
			canalVenda.setNome(resultSet.getString("RCANAL_VDA_DNTAL_INDVD"));
			canalVenda.setSigla(resultSet.getString("CSGL_CANAL_VDA_DNTAL_INDVD"));
			return canalVenda;
		}
	}

}