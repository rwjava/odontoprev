package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.ConsultaPropostaDao;

@Service
@Transactional(propagation=Propagation.NOT_SUPPORTED)
public class ConsultaPropostaServiceFacadeImpl implements ConsultaPropostaServiceFacade{

	@Autowired
	private ConsultaPropostaDao consultaPropostaDao;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public List<List<String>> realizarConsulta(String consulta) {
		return consultaPropostaDao.realizarConsulta(consulta);
	}

	
}
