package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoAnsVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoSegmtVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ValorPlanoVO;

/**
 * Lista os tipos de planos dispon�veis para a proposta
 * 
 * @author WDEV
 */
@Repository
public class PlanoDAOImpl extends JdbcDao implements PlanoDAO {

	@Autowired
	private DataSource dataSource;

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanoDAOImpl.class);
	
	private static final String OBTER_PLANO_ATUAL_POR_CANAL = new StringBuilder()                                                                                                                                                
    .append("		SELECT                                                       \n")
	.append("	    PLANO.CPLANO_DNTAL_INDVD,                                    \n")
	.append("	    PLANO.RPLANO_DNTAL_INDVD,                                    \n")
	.append("	    PLANO.CREG_PLANO_DNTAL,                                      \n")
	/*.append("       PLANO.CPLANO_ANS_DNTAL_INDVD,                                 \n")*/
	.append("	    PLANO.CRESP_ULT_ATULZ,                                       \n")
	.append("	    PLANO.DINIC_PLANO_INDVD,                                     \n")
	.append("	    PLANO.DFIM_VGCIA,                                            \n")
	.append("	    PLANO.RPER_CAREN_PLANO_ANO,                                  \n")
	.append("	    PLANO.RPER_CAREN_PLANO_MES,                                 \n")
	.append("	    PLANO.DULT_ATULZ_REG                                         \n")
	.append("	FROM                                                             \n")
	.append("	    DBPROD.PLANO_DNTAL_INDVD PLANO,                              \n")
	.append("	    DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_VENDA               \n")
	.append("	WHERE                                                            \n")
	.append("	    CANAL_VENDA.CCANAL_VDA_DNTAL_INDVD = :codigoCanalVenda       \n")
	.append("	AND PLANO.CPLANO_DNTAL_INDVD = CANAL_VENDA.CPLANO_DNTAL_INDVD    \n")
	.append("	AND CANAL_VENDA.DFIM_VGCIA IS NULL                               \n")
	.append("	AND PLANO.DINIC_PLANO_INDVD <= CURRENT_DATE                      \n")
	.append("	AND PLANO.DFIM_VGCIA >= CURRENT_DATE                             \n")
	.append("	ORDER BY                                                         \n")
	.append("	    PLANO.DINIC_PLANO_INDVD DESC                                 \n")
	.append("	FETCH                                                            \n")
	.append("	    FIRST 1 ROWS ONLY                                            \n")
	.toString();
	
	
	private static final String OBTER_VALORES_ATUAL_PLANO = new StringBuilder()	
     .append("   SELECT                                          		                \n") 	
     .append("   NSEQ_VLR_PLANO_DNTAL, CPLANO_DNTAL_INDVD,                              \n")	
     .append("   VANUDD_PLANO_DNTAL_TTLAR, VANUDD_PLANO_DNTAL_DEPDT, VMESD_PLANO_TTLAR, \n")	
     .append("   VMESD_PLANO_DEPDT,  VDESC_PLANO_DNTAL, VTX_PLANO_INDVD, DINIC_VGCIA    \n")	
     .append("   FROM DBPROD.VLR_PLANO_DNTAL_INDVD                               	    \n")
     .append("   WHERE CPLANO_DNTAL_INDVD = :codigoPlano                                \n")
     .append("   AND DINIC_VGCIA <= CURRENT_DATE                                        \n")	                   
     .append("   AND DFIM_VGCIA >= CURRENT_DATE                                         \n")	
     .append("   ORDER BY DINCL_REG_TBELA DESC FETCH FIRST 1 ROWS ONLY	                \n")
	 .toString();
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public List<PlanoVO> listar() {
//		StringBuilder sql = new StringBuilder();
//		sql.append(" SELECT CPLANO_DNTAL_INDVD, RPLANO_DNTAL_INDVD, CREG_PLANO_DNTAL, VMESD_PLANO_TTLAR, VMESD_PLANO_DEPDT");
//		sql.append(" FROM DBPROD.PLANO_DNTAL_INDVD");
//
//		return getJdbcTemplate().query(sql.toString(), new MapSqlParameterSource(), new PlanoRowMapper());
		return null;
	}

	/**
	 * Mapeamento dos dados da listagem de planos
	 * @author WDEV
	 */
//	private static final class PlanoRowMapper implements RowMapper<PlanoVO> {
//		public PlanoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
//			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
//			PlanoVO plano = new PlanoVO();
//			plano.setCodigo(resultSet.getInteger("CPLANO_DNTAL_INDVD"));
//			plano.setDescricao(Strings.maiusculas(resultSet.getString("RPLANO_DNTAL_INDVD")));
//			plano.setRegistro(resultSet.getLong("CREG_PLANO_DNTAL"));
//			plano.setValorMensalidadeTitular(resultSet.getBigDecimal("VMESD_PLANO_TTLAR"));
//			plano.setValorMensalidadeDependente(resultSet.getBigDecimal("VMESD_PLANO_DEPDT"));
//
//			return plano;
//		}
//	}

	public PlanoVO buscaPlanoPorCodigo(Integer codigoPlano) {

//		StringBuilder sql = new StringBuilder();
//		
//		sql.append(" SELECT CPLANO_DNTAL_INDVD, RPLANO_DNTAL_INDVD, CREG_PLANO_DNTAL, VMESD_PLANO_TTLAR, VMESD_PLANO_DEPDT");
//		sql.append(" FROM DBPROD.PLANO_DNTAL_INDVD ");
//		sql.append("WHERE CPLANO_DNTAL_INDVD = :codigoPlano");
//		
//		MapSqlParameterSource param = new MapSqlParameterSource();
//		param.addValue("codigoPlano", codigoPlano);
//		
//	    LOGGER.error("INFO: BUSCAR PLANO: "+sql.toString()+" PAR�METROS: "+param.getValues());
//		
//		return getJdbcTemplate().queryForObject(sql.toString(), param,  new PlanoRowMapper());
		return null;
	}
	
	/**
	 * Obt�m o plano atual 
	 * @param codigo - c�digo do canal de venda
	 * @return planoVO
	 */
	public PlanoVO obterPlanoAtualPorCodigoCanal(Integer codigo){
		
		PlanoVO plano = null;
		
		MapSqlParameterSource param = new MapSqlParameterSource();
		param.addValue("codigoCanalVenda", codigo);
		plano =   getJdbcTemplate().queryForObject(OBTER_PLANO_ATUAL_POR_CANAL,  param, new PlanoRowMapper());
	    plano.setValorPlanoVO(obterValorAtualPlanoPorCodigo(plano.getCodigo()));
	return plano;
	}


	
	private ValorPlanoVO obterValorAtualPlanoPorCodigo(Long codigo){
		
		ValorPlanoVO valorPlanoVO= null;
		try{

			MapSqlParameterSource params = new MapSqlParameterSource();		
			params.addValue("codigoPlano", codigo);
			valorPlanoVO = getJdbcTemplate().queryForObject(OBTER_VALORES_ATUAL_PLANO, params, new ValorPlanoRowMapper());
			
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO OBTER VALORES PLANO");
		}
		
		return valorPlanoVO;
	}
	
	/**
	 * Listar todos os planos vigente associados a um determinado canal.
	 * 
	 * @param codigoCanal - c�digo do canal.
	 * 
	 * @return List<PlanoVO> - lista de planos vigente associados ao canal informado.
	 */
	public List<PlanoVO> listarPlanosVigentePorCanal(Integer codigoCanal){
		
		StringBuffer query = new StringBuffer()
		.append("SELECT                                                                       \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.RPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.RPER_CAREN_PLANO_ANO,                                              \n")
		.append("    PLANO.RPER_CAREN_PLANO_MES,                                              \n")
		.append("    PLANO.DINIC_PLANO_INDVD,                                                 \n")
		.append("    VALOR_PLANO.VMESD_PLANO_TTLAR,                                           \n")
		.append("    VALOR_PLANO.VMESD_PLANO_DEPDT,                                           \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                                    \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                                     \n")
		.append("FROM                                                                         \n")
		.append("    DBPROD.PLANO_DNTAL_INDVD PLANO                                           \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_PLANO                           \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = CANAL_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND CANAL_PLANO.CCANAL_VDA_DNTAL_INDVD = :CODIGO_CANAL                      \n")
		.append(" AND CANAL_PLANO.DFIM_VGCIA IS NULL                                          \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                                 \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND VALOR_PLANO.DINIC_VGCIA <= CURRENT_DATE                                 \n")
		.append(" AND VALOR_PLANO.DFIM_VGCIA >= CURRENT_DATE                                  \n")
		.append("WHERE                                                                        \n")
		.append("	 PLANO.DINIC_PLANO_INDVD <= CURRENT_DATE                                  \n")
		.append("AND PLANO.DFIM_VGCIA >= CURRENT_DATE                                         \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("CODIGO_CANAL", codigoCanal);
	
		return getJdbcTemplate().query(query.toString(), params, new PlanoComValorRowMapper());
	}
        public List<PlanoAnsVO> listarPlanosVigentePorCanalPlanoAns(Integer codigoCanal){
		
		StringBuffer query = new StringBuffer()
		.append("SELECT                                                                       \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.CPLANO_ANS_DNTAL_INDVD,                                            \n")
		.append("    PLANO.RPLANO_ANS_DNTAL_INDVD,                                            \n")
		.append("    PLANO.CREG_PLANO_DNTAL,                                                  \n")
		.append("    PLANO.RPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.RPER_CAREN_PLANO_ANO,                                              \n")
		.append("    PLANO.RPER_CAREN_PLANO_MES,                                              \n")
		.append("    PLANO.DINIC_PLANO_INDVD,                                                 \n")
		.append("    VALOR_PLANO.VMESD_PLANO_TTLAR,                                           \n")
		.append("    VALOR_PLANO.VMESD_PLANO_DEPDT,                                           \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                                    \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                                     \n")
		.append("FROM                                                                         \n")
		.append("    DBPROD.PLANO_DNTAL_INDVD PLANO                                           \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_PLANO                           \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = CANAL_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND CANAL_PLANO.CCANAL_VDA_DNTAL_INDVD = :CODIGO_CANAL                      \n")
		.append(" AND CANAL_PLANO.DFIM_VGCIA IS NULL                                          \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                                 \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND VALOR_PLANO.DINIC_VGCIA <= CURRENT_DATE                                 \n")
		.append(" AND VALOR_PLANO.DFIM_VGCIA >= CURRENT_DATE                                  \n")
		.append("WHERE                                                                        \n")
		.append("	 PLANO.DINIC_PLANO_INDVD <= CURRENT_DATE                                  \n")
		.append("AND PLANO.DFIM_VGCIA >= CURRENT_DATE                                         \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("CODIGO_CANAL", codigoCanal);
	
		return getJdbcTemplate().query(query.toString(), params, new PlanoComValorPlanoAnsRowMapper());
	}
	
	public List<PlanoAnsVO> listarPlanosVigentesPorCanalESegmento(Integer codigoCanal, Integer codigoSegmento){
		
		StringBuffer query = new StringBuffer()
		.append("SELECT                                                                       \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.CPLANO_ANS_DNTAL_INDVD,                                            \n")
		.append("    PLANO.RPLANO_ANS_DNTAL_INDVD,                                            \n")
		.append("    PLANO.CREG_PLANO_DNTAL,                                                  \n")
		.append("    PLANO.RPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.RPER_CAREN_PLANO_ANO,                                              \n")
		.append("    PLANO.RPER_CAREN_PLANO_MES,                                              \n")
		.append("    PLANO.DINIC_PLANO_INDVD,                                                 \n")
		.append("    VALOR_PLANO.VMESD_PLANO_TTLAR,                                           \n")
		.append("    VALOR_PLANO.VMESD_PLANO_DEPDT,                                           \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                                    \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                                     \n")
		.append("FROM                                                                         \n")
		.append("    DBPROD.PLANO_DNTAL_INDVD PLANO                                           \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_PLANO                           \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = CANAL_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND CANAL_PLANO.CCANAL_VDA_DNTAL_INDVD = :CODIGO_CANAL                      \n")
		.append(" AND CANAL_PLANO.DFIM_VGCIA IS NULL                                          \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                                 \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND VALOR_PLANO.DINIC_VGCIA <= CURRENT_DATE                                 \n")
		.append(" AND VALOR_PLANO.DFIM_VGCIA >= CURRENT_DATE                                  \n")
		.append(" INNER JOIN                                                                  \n")                                                                                    
	    .append(" DBPROD.PARMZ_COMCZ_PLANO_DNTAL PARMZ                                        \n")	                                                  
	    .append(" ON                                                                          \n")                                                                                      
		.append(" PLANO.CPLANO_DNTAL_INDVD = PARMZ.CPLANO_DNTAL_INDVD                         \n")	                                  
		.append(" AND                                                                         \n")	                                                                                      
		.append(" PARMZ.CSGMTO_SUCUR = :segmento AND  PARMZ.DFIM_VGCIA_PARMZ_PLANO IS NULL    \n")		       
		.append("WHERE                                                                        \n")
		.append("	 PLANO.DINIC_PLANO_INDVD <= CURRENT_DATE                                  \n")
		.append("AND PLANO.DFIM_VGCIA >= CURRENT_DATE   ORDER BY PLANO.CPLANO_DNTAL_INDVD     \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("CODIGO_CANAL", codigoCanal);
		params.addValue("segmento", codigoSegmento);
	
		return getJdbcTemplate().query(query.toString(), params, new PlanoComValorPlanoAnsRowMapper());
	}
	
	public List<PlanoSegmtVO> listarPlanosVigentesPorCanalESegmentoBanco(Integer codigoCanal, Integer codigoSegmento){
		
		StringBuffer query = new StringBuffer()
		.append("SELECT                                                                       \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.CPLANO_ANS_DNTAL_INDVD,                                            \n")
		.append("    PLANO.RPLANO_ANS_DNTAL_INDVD,                                            \n")
		.append("    PLANO.CREG_PLANO_DNTAL,                                                  \n")
		.append("    PLANO.RPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.RPER_CAREN_PLANO_ANO,                                              \n")
		.append("    PLANO.RPER_CAREN_PLANO_MES,                                              \n")
		.append("    PLANO.DINIC_PLANO_INDVD,                                                 \n")
		.append("    VALOR_PLANO.VMESD_PLANO_TTLAR,                                           \n")
		.append("    VALOR_PLANO.VMESD_PLANO_DEPDT,                                           \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                                    \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                                     \n")
		.append("FROM                                                                         \n")
		.append("    DBPROD.PLANO_DNTAL_INDVD PLANO                                           \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_PLANO                           \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = CANAL_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND CANAL_PLANO.CCANAL_VDA_DNTAL_INDVD = :CODIGO_CANAL                      \n")
		.append(" AND CANAL_PLANO.DFIM_VGCIA IS NULL                                          \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                                 \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND VALOR_PLANO.DINIC_VGCIA <= CURRENT_DATE                                 \n")
		.append(" AND VALOR_PLANO.DFIM_VGCIA >= CURRENT_DATE                                  \n")
		.append("WHERE                                                                        \n")
		.append("	 PLANO.DINIC_PLANO_INDVD <= CURRENT_DATE AND PLANO.CSUB_SEGMT = :segmento \n")
		.append("AND PLANO.DFIM_VGCIA >= CURRENT_DATE   ORDER BY PLANO.CPLANO_DNTAL_INDVD     \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("CODIGO_CANAL", codigoCanal);
		params.addValue("segmento", codigoSegmento);
	
		return getJdbcTemplate().query(query.toString(), params, new PlanoSegmtRowMapper());
	}		
	
	/**
	 * Obter as informa��es de um plano atrav�s do c�digo.
	 * 
	 * @param codigoPlano - c�digo do plano a ser obtido.
	 * @return PlanoVO - informa��es do plano obtido.
	 */
	public PlanoVO obterPlanoPorCodigo(Long codigoPlano){
		
		StringBuffer query = new StringBuffer()
//		.append("SELECT                                                              \n") 
//		.append("    PLANO.CPLANO_DNTAL_INDVD,                                       \n") 
//		.append("    PLANO.RPLANO_DNTAL_INDVD,                                       \n") 
//		.append("    PLANO.RPER_CAREN_PLANO_ANO,                                     \n") 
//		.append("    PLANO.RPER_CAREN_PLANO_MES,                                     \n") 
//		.append("    PLANO.DINIC_PLANO_INDVD,                                        \n") 
//		.append("    VALOR_PLANO.VMESD_PLANO_TTLAR,                                  \n") 
//		.append("    VALOR_PLANO.VMESD_PLANO_DEPDT,                                  \n") 
//		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                           \n") 
//		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                            \n") 
//		.append("FROM                                                                \n") 
//		.append("    DBPROD.PLANO_DNTAL_INDVD PLANO                                  \n") 
//		.append("                                                                    \n")
//		.append("INNER JOIN                                                          \n") 
//		.append("    DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                        \n") 
//		.append(" ON                                                                 \n") 
//		.append("    PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD       \n")   
//		.append("WHERE                                                               \n")
//		.append("    PLANO.CPLANO_DNTAL_INDVD = :CODIGO_PLANO                        \n")
//		.append("ORDER BY                                                            \n")
//		.append("	 PLANO.DFIM_VGCIA DESC                                           \n")
//		.append("FETCH FIRST 1 ROWS ONLY                                             \n");
		
		
		.append("SELECT                                                                       \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.RPLANO_DNTAL_INDVD,                                                \n")
		.append("    PLANO.RPER_CAREN_PLANO_ANO,                                              \n")
		.append("    PLANO.RPER_CAREN_PLANO_MES,                                              \n")
		.append("    PLANO.DINIC_PLANO_INDVD,                                                 \n")
		.append("    VALOR_PLANO.VMESD_PLANO_TTLAR,                                           \n")
		.append("    VALOR_PLANO.VMESD_PLANO_DEPDT,                                           \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                                    \n")
		.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                                     \n")
		.append("FROM                                                                         \n")
		.append("    DBPROD.PLANO_DNTAL_INDVD PLANO                                           \n")
		.append("INNER JOIN                                                                   \n")
		.append("    DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                                 \n")
		.append(" ON                                                                          \n")
		.append("    PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD                \n")
		.append(" AND VALOR_PLANO.DINIC_VGCIA <= CURRENT_DATE                                 \n")
		.append(" AND VALOR_PLANO.DFIM_VGCIA >= CURRENT_DATE                                  \n")
		.append("WHERE                                                                        \n")
		.append("	 PLANO.CPLANO_DNTAL_INDVD = :PLANO                                        \n");		
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("PLANO", codigoPlano);
	
		try{
			return getJdbcTemplate().queryForObject(query.toString(), params, new PlanoComValorRowMapper());
		}catch(EmptyResultDataAccessException exception){
			LOGGER.error("Plano n�o encontrado com o c�digo: " + codigoPlano);
			return null;
		}
	}
	
	public PlanoAnsVO obterPlanoAnsPorCodigo(Long codigoPlano){
		
		StringBuffer query = new StringBuffer()
			.append("SELECT                                                                       \n")
			.append("    PLANO.CPLANO_ANS_DNTAL_INDVD,                                            \n")
			.append("    PLANO.RPLANO_ANS_DNTAL_INDVD,                                            \n")
			.append("    PLANO.CREG_PLANO_DNTAL,                                                  \n")
			.append("    PLANO.CPLANO_DNTAL_INDVD,                                                \n")
			.append("    PLANO.RPLANO_DNTAL_INDVD,                                                \n")
			.append("    PLANO.RPER_CAREN_PLANO_ANO,                                              \n")
			.append("    PLANO.RPER_CAREN_PLANO_MES,                                              \n")
			.append("    PLANO.DINIC_PLANO_INDVD,                                                 \n")
			.append("    VALOR_PLANO.VMESD_PLANO_TTLAR,                                           \n")
			.append("    VALOR_PLANO.VMESD_PLANO_DEPDT,                                           \n")
			.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                                    \n")
			.append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                                     \n")
			.append("FROM                                                                         \n")
			.append("    DBPROD.PLANO_DNTAL_INDVD PLANO                                           \n")
			.append("INNER JOIN                                                                   \n")
			.append("    DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                                 \n")
			.append(" ON                                                                          \n")
			.append("    PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD                \n")
			.append(" AND VALOR_PLANO.DINIC_VGCIA <= CURRENT_DATE                                 \n")
			.append(" AND VALOR_PLANO.DFIM_VGCIA >= CURRENT_DATE                                  \n")
			.append("WHERE                                                                        \n")
			.append("	 PLANO.CPLANO_DNTAL_INDVD = :PLANO                                        \n");		
			
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("PLANO", codigoPlano);
		
			try{
				return getJdbcTemplate().queryForObject(query.toString(), params, new PlanoComValorPlanoAnsRowMapper());
			}catch(EmptyResultDataAccessException exception){
				LOGGER.error("Plano n�o encontrado com o c�digo: " + codigoPlano);
				return null;
			}
	}
	
	public PlanoVO obterPlanoPorDataEmissaoProposta(PropostaVO proposta){
		StringBuffer query = new StringBuffer()
			    .append(" 	SELECT                                                         \n")
				.append("     PLANO.CPLANO_DNTAL_INDVD,                                    \n")
				.append("     PLANO.RPLANO_DNTAL_INDVD,                                    \n")
				.append("     PLANO.RPER_CAREN_PLANO_ANO,                                  \n")
				.append("     PLANO.RPER_CAREN_PLANO_MES,                                  \n")
				.append("     PLANO.DINIC_PLANO_INDVD,                                     \n")
				.append("     VALOR_PLANO.NSEQ_VLR_PLANO_DNTAL,                            \n")
				.append("     VALOR_PLANO.DINIC_VGCIA,                                     \n")
				.append("     VALOR_PLANO.VTX_PLANO_INDVD,                                 \n")
				.append("     VALOR_PLANO.VDESC_PLANO_DNTAL,                               \n")
				.append("     VALOR_PLANO.VMESD_PLANO_TTLAR,                               \n")
				.append("     VALOR_PLANO.VMESD_PLANO_DEPDT,                               \n")
				.append("     VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                        \n")
				.append("     VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                         \n")
				.append(" FROM                                                             \n")
				.append("     DBPROD.PLANO_DNTAL_INDVD PLANO                               \n")
				.append(" INNER JOIN                                                       \n")
				.append("     DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_PLANO               \n")
				.append("  ON                                                              \n")
				.append("     PLANO.CPLANO_DNTAL_INDVD = CANAL_PLANO.CPLANO_DNTAL_INDVD    \n")
				.append("  AND CANAL_PLANO.CCANAL_VDA_DNTAL_INDVD = :codigoCanal           \n")
				.append("  AND PLANO.CPLANO_DNTAL_INDVD = :codigoPlano                     \n")
				.append("  AND CANAL_PLANO.DFIM_VGCIA IS NULL                              \n")
				.append(" INNER JOIN                                                       \n")
				.append("     DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                     \n")
				.append("  ON                                                              \n")
				.append("     PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD    \n")
				.append("  AND VALOR_PLANO.DINIC_VGCIA <= :dataCriacao                     \n")
				.append("  AND VALOR_PLANO.DFIM_VGCIA >= :dataCriacao                      \n")
				.append(" WHERE                                                            \n")
				.append(" 	 PLANO.DINIC_PLANO_INDVD <= :dataCriacao                       \n")
				.append(" AND PLANO.DFIM_VGCIA >= :dataCriacao                             \n")
				.append(" order by NSEQ_VLR_PLANO_DNTAL desc                                \n")
                .append(" fetch first 1 rows only                                           \n");

			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("codigoCanal", proposta.getCanalVenda().getCodigo());
			params.addValue("codigoPlano", proposta.getPlanoVO().getCodigo());

			LocalDate dataCriacao = new LocalDate();

			if (proposta.getDataCriacao() != null) {
				dataCriacao = proposta.getDataCriacao();
			}
			params.addValue("dataCriacao", new java.sql.Date(dataCriacao. toInterval().getStartMillis()));

			return getJdbcTemplate().queryForObject(query.toString(), params, new PlanoComValorRowMapper());
	}
	public List<PlanoVO> listarPlanoDisponivelParaVenda(Integer codigoSegmento){
        List<PlanoVO> planos = null;

        StringBuffer query = new StringBuffer().append("SELECT                                                 ")
                .append("    PLANO.CPLANO_DNTAL_INDVD,                                                         ")
                .append("    PLANO.RPLANO_DNTAL_INDVD,                                                         ")
                .append("    PLANO.RPER_CAREN_PLANO_ANO,                                                       ")
                .append("    PLANO.RPER_CAREN_PLANO_MES,                                                       ")
                .append("    PLANO.DINIC_PLANO_INDVD,                                                          ")
                .append("    VALOR_PLANO.VMESD_PLANO_TTLAR,                                                    ")
                .append("    VALOR_PLANO.VMESD_PLANO_DEPDT,                                                    ")
                .append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_TTLAR,                                             ")
                .append("    VALOR_PLANO.VANUDD_PLANO_DNTAL_DEPDT                                              ")
                .append("FROM                                                                                  ")
                .append("    DBPROD.PLANO_DNTAL_INDVD PLANO                                                    ")
                .append("INNER JOIN                                                                            ")
                .append("    DBPROD.CANAL_VDA_PLANO_DNTAL_INDVD CANAL_PLANO                                    ")
                .append(" ON                                                                                   ")
                .append("    PLANO.CPLANO_DNTAL_INDVD = CANAL_PLANO.CPLANO_DNTAL_INDVD                         ")
                .append(" AND CANAL_PLANO.CCANAL_VDA_DNTAL_INDVD = 1                                           ")
                .append(" AND CANAL_PLANO.DFIM_VGCIA IS NULL                                                   ")
                .append("INNER JOIN                                                                            ")
                .append("    DBPROD.VLR_PLANO_DNTAL_INDVD VALOR_PLANO                                          ")
                .append(" ON                                                                                   ")
                .append("    PLANO.CPLANO_DNTAL_INDVD = VALOR_PLANO.CPLANO_DNTAL_INDVD                         ")
                .append(" AND VALOR_PLANO.DINIC_VGCIA <= CURRENT_DATE                                          ")
                .append(" AND VALOR_PLANO.DFIM_VGCIA >= CURRENT_DATE                                           ")
                .append("INNER JOIN                                                                            ")
                .append("DBPROD.PARMZ_COMCZ_PLANO_DNTAL PARMZ                                                  ")
                .append("ON                                                                                    ")
                .append("    PLANO.CPLANO_DNTAL_INDVD = PARMZ.CPLANO_DNTAL_INDVD                               ")
                .append("AND                                                                                   ")
                .append("     PARMZ.CSGMTO_SUCUR = :segmento AND  PARMZ.DFIM_VGCIA_PARMZ_PLANO IS NULL         ")
                .append("WHERE                                                                                 ")
                .append("        PLANO.DINIC_PLANO_INDVD <= CURRENT_DATE                                       ")
                .append("AND PLANO.DFIM_VGCIA >= CURRENT_DATE ORDER BY PLANO.CPLANO_DNTAL_INDVD                ");

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("segmento", codigoSegmento);

        try {
            planos = getJdbcTemplate().query(query.toString(), params, new PlanoComValorRowMapper());
        } catch (Exception e) {
            LOGGER.error("Erro ao obter planos: " + e.getMessage());
        }

        return planos;
	}
	
	@Override
	public Boolean isPlanoVigente(Integer codigoPlano) {
		try{

			MapSqlParameterSource params = new MapSqlParameterSource();		
			params.addValue("codigoPlano", codigoPlano);
			return getJdbcTemplate().query(OBTER_VALORES_ATUAL_PLANO, params, new ResultSetExtractor<Boolean>() {

				@Override
				public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
					if(rs.next()){
						return true;
					}
					return false;
				}
			});
			
		}catch(Exception e){
			LOGGER.error("INFO: ERRO AO OBTER VALORES PLANO");
			throw new IntegrationException("Erro ao obter os dados do plano - " + e);
		}
	}

}