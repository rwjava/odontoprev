package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.bradseg.bsad.framework.core.text.formatter.MaskFormatter;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Normalizador para as informa��es da proposta. Formatando o c�digo e alterando a descri��o da proposta
 * @author WDEV
 */
@Component
public class NormalizadorProposta {

	public void normalizar(PropostaVO... proposta) {
		if (proposta != null) {
			normalizar(Arrays.asList(proposta));
		}
	}

	public List<PropostaVO> normalizar(List<PropostaVO> propostas) {
		if (propostas != null) {
			for (PropostaVO proposta : propostas) {
				normalizar(proposta);
			}
		}
		return propostas;
	}

	public PropostaVO normalizar(PropostaVO proposta) {
		if (proposta != null) {
			formatarCodigoProposta(proposta);
			alterarDescricaoMovimentoParaPropostaCancelada(proposta);
			alterarDescricaoMovimentoParaPropostaEmProcessamento(proposta);
		}
		return proposta;
	}

	private void formatarCodigoProposta(PropostaVO proposta) {
		MaskFormatter mf = new MaskFormatter("***###########-#");
		mf.setValueContainsLiteralCharacters(false);
		proposta.setCodigoFormatado(mf.valueToString(proposta.getCodigo()));
	}

	private void alterarDescricaoMovimentoParaPropostaEmProcessamento(PropostaVO proposta) {
		if (proposta.isEmProcessamento() && !proposta.getMovimento().isDescricaoPreenchida()) {
			proposta.getMovimento().setDescricao("Envio");
		}
	}

	/**
	 * Altera a descri��o do movimento da proposta se ela estiver na situa��o cancelada.
	 * 
	 * <ul>
	 * 
	 * 	<li>Se o movimento da proposta possuir descri��o, altera a descri��o para Cancelada OdontoPrev</li>
	 * 	<li>Se o movimento da proposta n�o possuir descri��o, altera a descri��o para Cancelada online</li>
	 * 
	 * </ul>
	 * 
	 * @param proposta Proposta
	 */
	private void alterarDescricaoMovimentoParaPropostaCancelada(PropostaVO proposta) {
		if (proposta.isCancelada()) {
			MovimentoPropostaVO movimentoProposta = proposta.getMovimento();
			if (movimentoProposta.isDescricaoPreenchida()) {
				movimentoProposta.setDescricao("Cancelada Odontroprev");
			} else {
				movimentoProposta.setDescricao("Cancelada online");
			}
		}
	}

}
