package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AgenciaBancariaVO;

/**
 * Servi�os de acesso a dados de Ag�ncia Banc�ria
 * 
 * @author WDEV
 */
public interface AgenciaBancariaDAO {

	/**
	 * Consulta as informa��es da ag�ncia banc�ria a partir do id 
	 * 
	 * @param id Identificador da ag�ncia banc�ria
	 * @return Ag�ncia banc�ria encontrada ou nulo
	 */
	public AgenciaBancariaVO consultarPorId(Integer id);
	
	/**
	 * Validar se a ag�ncia pertence ao banco informado.
	 * 
	 * @param codigoBanco - codigo do banco.
	 * @param codigoAgencia - codigo da agencia.
	 * 
	 * @return boolean - retorna se a agencia pertence ao banco informado.
	 */
	public boolean validarAgenciaBancariaComBanco(Integer codigoBanco, Integer codigoAgencia);

}