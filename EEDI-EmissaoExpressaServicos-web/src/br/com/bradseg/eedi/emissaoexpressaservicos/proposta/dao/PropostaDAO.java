package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.DefinicaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DadosProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaExpiradaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.StatusPropostaVO;

/**
 * Descri��o do DAO da proposta
 * 
 * @author WDEV
 */
public interface PropostaDAO {

	/**
	 * Lista todos as propostas encontradas a partir de filtro de pesquisa
	 * 
	 * @param filtroProposta Filtro para consulta de proposta
	 * @param login Informa��es do usu�rio logado
	 * @return Lista de propostas
	 */
	public List<PropostaVO> listarPorFiltro(FiltroPropostaVO filtroProposta, LoginVO login);
	
	public List<PropostaVO> listarPropostasIntranetFiltro(FiltroPropostaVO filtroProposta, LoginVO login);
	
	/**
	 * Lista propostas por cpf o proponente, canal de venda e per�odo
	 * @param cpfProponente
	 * @param canalVenda
	 * @param periodoInicial
	 * @param periodoFinal
	 * @return
	 */
	public List<PropostaVO> listarPropostasPorProponente(String cpfProponente, Integer canalVenda,
			DateTime periodoInicial, DateTime periodoFinal);

	/**
	 * Consulta a proposta a partir do c�digo da proposta informado
	 * 
	 * @param codigoProposta C�digo da proposta
	 * @return Proposta encontrada
	 */
	public PropostaVO consultarPorCodigo(String codigoProposta);

	/**
	 * Consulta a proposta a partir do sequencial da proposta informado
	 * 
	 * @param sequencialProposta Sequencial da proposta
	 * @return Proposta encontrada
	 */
	public PropostaVO consultarPorSequencial(Long sequencialProposta);

	/**
	 * Cria uma nova proposta no sistema
	 * 
	 * @param proposta PropostaVO a ser criada
	 * @param definicaoProposta definicao proposta
	 */
	public void salvar(PropostaVO proposta, DefinicaoProposta definicaoProposta);

	/**
	 * Obt�m um n�mero sequencial para a nova proposta
	 * 
	 * @return n�mero sequencial da proposta
	 */
	public Long obterSequencialProposta();

	/**
	 * Consulta a quantidade de propostas com situa��o Rascunho ou Pendente existentes na data atual para o CPF do benefici�rio informado 
	 *  
	 * @param cpf CPF do benefici�rio
	 * @return Quantidade de propostas
	 */
	public Integer consultarQuantidadePropostasPorDataAtualParaBeneficiario(String cpf);

	/**
	 * Atualiza as informa��es da proposta
	 * 
	 * @param proposta Proposta a ser atualizada
	 */
	public void atualizar(PropostaVO proposta);

	/**
	 * Busca as propostas para a gera��o de arquivo
	 * 
	 * @param dataInicialPesquisaMovimento Data Inicial da pesquisa do movimento
	 * @param situacaoProposta Situacao da proposta
	 * @return lista de propostas
	 */
	public List<PropostaVO> buscarPropostaParaArquivo(LocalDate dataInicialPesquisaMovimento, SituacaoProposta situacaoProposta);

	/**
	 * Atualiza a proposta removendo o respons�vel legal
	 * @param proposta Proposta
	 */
	public void removerResponsavelLegal(PropostaVO proposta);

	/**
	 * Obter o n�mero sequencial da proposta 
	 * @param protocolo protocolo da proposta
	 * @param canalVenda canal de venda da proposta
	 * @return o n�mero sequencial da proposta
	 */
	
	public Long obterSequencialPorProtocolo(String protocolo, Integer canalVenda);
	
	/**
	 * 
	 * Atualizar o respons�vel por �ltima atualiza��o
	 * @param N�mero sequencial de proposta a ser atualizada
	 * @param Respons�vel por atualiza��o
	 */
	  
	public void atualizarResponsavelPorAtualizacao(Long numeroSequencial, String responsavelAtualizacao);
	
	
	/**
	 * Verificar se proposta possui status de cancelada
	 * @param numeroSequencial 
	 * @return boolean true se possui status cancelada false se n�o possui status cancelada
	 */
	public boolean possuiStatusCancelada(Long numeroSequencial);
	
	/**
	 * Verificar se proposta possui status de implantada
	 * @param numeroSequencial 
	 * @return boolean true se possui status cancelada false se n�o possui status cancelada
	 */
	public boolean possuiStatusImplantada(Long numeroSequencial);
	
	public boolean possuiStatusImplantadaProponente(Long numeroSequencial);
	
	public Integer consultarParentescoAtivo(Long numeroSequencial);
	
	public Integer consultaMovimentacao(Long numerosequencial);
	
	public Integer consultarTipoParentesco(Long numeroSequencial);
	
	public Integer consultarTipoParentescoDepen(Long numeroSequencial);
	/**
	 * Metodo responsavel por listar as propostas do shopping de seguros que foram atualizadas.
	 * 
	 * @return List<StatusPropostaVO> - lista das propostas com os respectivos status.
	 */
	public List<StatusPropostaVO> listarPropostasAtualizadasDoShoppingDeSeguros();

	public List<PropostaVO> listarPropostasPorBeneficiario(String cpfBeneficiario, Integer canal);

	public void incluirProtocoloDeCancelamentoNaProposta(Long sequencialProposta, String protocolo);
	
	public String obterCodigoPropostaPorSequencial(Long sequencialProposta);
	
	public Long obterSequencialPorCodigoProposta(String codigoProposta);
	
	public List<DadosProdutorVO> verificaProdutoresDeCadaProposta(String codigoProposta);
	
	public String descricaoRetornoMovimentoProposta(Long sequencialProposta, Integer movimento);

	public List<String> consultarProposta(String consulta);
	
	public List<PropostaExpiradaVO> listarPropostasDoSite100CorretorComDataDeValidadeExpirada();

}
