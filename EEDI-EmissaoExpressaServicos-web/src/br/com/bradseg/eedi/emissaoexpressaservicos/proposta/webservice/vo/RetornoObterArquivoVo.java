package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.vo;

import java.io.Serializable;

public class RetornoObterArquivoVo implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6552454640824765201L;
	
	private Integer codigoErro;
	private String descricaoErro;
	private byte[] bytePdf;

	public Integer getCodigoErro() {
		return codigoErro;
	}

	public void setCodigoErro(Integer codigoErro) {
		this.codigoErro = codigoErro;
	}

	public String getDescricaoErro() {
		return descricaoErro;
	}

	public void setDescricaoErro(String descricaoErro) {
		this.descricaoErro = descricaoErro;
	}

	public byte[] getBytePdf() {
		return bytePdf;
	}

	public void setBytePdf(byte[] bytePdf) {
		this.bytePdf = bytePdf;
	}

}
