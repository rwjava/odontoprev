package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AgenciaBancariaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BancoVO;

/**
 * Implementa��o dos servi�os de acesso a dados de Ag�ncia Banc�ria
 * 
 * @author WDEV
 */
@Repository
public class AgenciaBancariaDAOImpl extends JdbcDao implements AgenciaBancariaDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgenciaBancariaDAOImpl.class);

	@Autowired
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public AgenciaBancariaVO consultarPorId(Integer id) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT AG_BCRIA.CBCO, AG_BCRIA.CAG_BCRIA, AG_BCRIA.CDIG_AG_BCRIA, PSSOA_FIS_JURID.IPSSOA FROM DBPROD.AG_BCRIA AG_BCRIA, DBPROD.PSSOA_FIS_JURID PSSOA_FIS_JURID WHERE AG_BCRIA.CPSSOA = PSSOA_FIS_JURID.CPSSOA AND AG_BCRIA.CAG_BCRIA = :id AND AG_BCRIA.CBCO = :banco");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("banco", 237);
		params.addValue("id", id);
		
		try {
			return getJdbcTemplate().queryForObject(sql.toString(), params, new AgenciaBancariaRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhuma ag�ncia banc�ria encontrada para o c�digo informado", e);
			return null;
		}
	}
	
	/**
	 * Validar se a ag�ncia pertence ao banco informado.
	 * 
	 * @param codigoBanco - codigo do banco.
	 * @param codigoAgencia - codigo da agencia.
	 * 
	 * @return boolean - retorna se a agencia pertence ao banco informado.
	 */
	public boolean validarAgenciaBancariaComBanco(Integer codigoBanco, Integer codigoAgencia) {

		StringBuilder sql = new StringBuilder()
		.append("SELECT                                    \n")
		.append("	COUNT(*)                               \n")
		.append("FROM                                      \n")
		.append("	DBPROD.AG_BCRIA                        \n")
		.append("WHERE                                     \n")
		.append("	CBCO = :CODIGO_BANCO                   \n")
		.append(" AND                                      \n")
		.append("	CAG_BCRIA = :CODIGO_AGENCIA            \n");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("CODIGO_BANCO", codigoBanco);
		params.addValue("CODIGO_AGENCIA", codigoAgencia);
		
		return (getJdbcTemplate().queryForInt(sql.toString(), params) > 0);
	}

	/**
	 * Mapemanto de dados de ag�ncia banc�ria
	 * 
	 * @author WDEV
	 */
	private static final class AgenciaBancariaRowMapper implements RowMapper<AgenciaBancariaVO> {
		public AgenciaBancariaVO mapRow(ResultSet es, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(es);
			AgenciaBancariaVO agenciaBancaria = new AgenciaBancariaVO();
			agenciaBancaria.setCodigo(resultSet.getInteger("CAG_BCRIA"));
			agenciaBancaria.setDigito(resultSet.getString("CDIG_AG_BCRIA"));
			agenciaBancaria.setBanco(new BancoVO(resultSet.getInteger("CBCO")));
			agenciaBancaria.setNome(Strings.maiusculas(resultSet.getString("IPSSOA")));
			return agenciaBancaria;
		}
	}

}