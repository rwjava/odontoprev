package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

/**
 * Dados dos assistente de produ��o da proposta
 * 
 * @author WDEV
 */
public interface AssistenteProducaoDAO {

	/**
	 * Verifica se o assistente de produ��o informado existe
	 * 
	 * @param matricula Matricula do assistente de produ��o
	 * @return verdadeiro ou falso se existe o assistente de produ��o informado
	 */
	public Boolean verificarExistencia(String matricula);

}
