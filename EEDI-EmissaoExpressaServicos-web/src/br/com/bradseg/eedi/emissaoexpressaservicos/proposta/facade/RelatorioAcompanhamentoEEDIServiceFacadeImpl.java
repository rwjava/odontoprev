package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.math.BigDecimal;
import java.util.List;

import org.apache.wicket.util.string.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.PlanoDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.PropostaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.RelatorioAcompanhamentoEEDIDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AcompanhamentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DadosProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class RelatorioAcompanhamentoEEDIServiceFacadeImpl implements RelatorioAcompanhamentoEEDIServiceFacade{

	
	@Autowired
	private RelatorioAcompanhamentoEEDIDAO relatorioAcompanhamentoEEDIDAO;
	
	@Autowired
	private PropostaDAO propostaDAO;
	
	@Autowired
	private PlanoDAO planoDAO;
	
	public List<AcompanhamentoPropostaVO> listarPropostasAcompanhamento(FiltroAcompanhamentoVO filtro,  LoginVO login, boolean isIntranet){
		
		List<AcompanhamentoPropostaVO> listaAcompanhamentoProposta =  this.relatorioAcompanhamentoEEDIDAO.listarPropostasAcompanhamento(filtro, login, isIntranet);
			if (!listaAcompanhamentoProposta.isEmpty()) {
				for (AcompanhamentoPropostaVO acompanhamento : listaAcompanhamentoProposta) {
	
					if (verificaSituacaoCancelada(acompanhamento) == true) {
						acompanhamento.setSituacaoProposta(5);
						acompanhamento.setStatusAtualProposta("CANCELADA");
					} else if (verificaSituacaoImplantada(acompanhamento)) {
						acompanhamento.setSituacaoProposta(6);
						acompanhamento.setStatusAtualProposta("IMPLANTADA");
	
					}
	
					PropostaVO proposta = propostaDAO.consultarPorCodigo(acompanhamento.getCodProposta());

					
					proposta.setPlanoVO(new PlanoVO());
					proposta.getPlanoVO().setCodigo(acompanhamento.getCodigoPlano());
					proposta.setCanalVenda(new CanalVendaVO());
					proposta.getCanalVenda().setCodigo(acompanhamento.getCodigoCanalVenda().intValue());
//					proposta.setDataEmissaoProposta (acompanhamento.getDataCriacaoProposta());
					PlanoVO plano = obterPlanoPorDataEmissaoProposta(proposta);
					if (TipoCobranca.MENSAL.equals(acompanhamento.getTipoCobranca())) {
						BigDecimal valorTotal = new BigDecimal(new Integer(acompanhamento.getQuantidadeVidasProposta()) * plano.getValorPlanoVO().getValorMensalTitular());
						acompanhamento.setValorTotalProposta(valorTotal);
					} else if (TipoCobranca.ANUAL.equals(acompanhamento.getTipoCobranca())) {
						BigDecimal valorTotal = new BigDecimal(new Integer(acompanhamento.getQuantidadeVidasProposta()) * plano.getValorPlanoVO().getValorAnualTitular());
						acompanhamento.setValorTotalProposta(valorTotal);
					}
	
					String descricaoRetornoCancelada = null;
					String descricaoRetornoImplantada = null;
					String descricaoRetorno = null;
					descricaoRetornoCancelada = descricaoRetornoMovimentoCancelada(acompanhamento.getCodProposta());
					acompanhamento.setMotivoStatusProposta(descricaoRetornoCancelada);
					if (Strings.isEmpty(descricaoRetornoCancelada)) {
						descricaoRetornoImplantada = descricaoRetornoMovimentoImpantada(acompanhamento.getCodProposta());
						acompanhamento.setMotivoStatusProposta(descricaoRetornoImplantada);
					}
					if (Strings.isEmpty(descricaoRetornoImplantada)) {
						descricaoRetorno = descricaoRetornoMovimento(acompanhamento.getCodProposta());
						acompanhamento.setMotivoStatusProposta(descricaoRetorno);
					}
	
					if (acompanhamento.getCodProposta() != null) {
						List<DadosProdutorVO> dados = propostaDAO.verificaProdutoresDeCadaProposta(acompanhamento.getCodProposta());
						if (!dados.isEmpty()) {
							for (DadosProdutorVO dadosProdutor : dados) {
								if (dadosProdutor.getTipoProdutor() != null && dadosProdutor.getTipoProdutor().equals("1")) {
									if (dadosProdutor.getNomeProdutor() != null) {
										acompanhamento.setNomeAngariador(dadosProdutor.getNomeProdutor());
									}
									if (dadosProdutor.getCpdProdutor() != null) {
										acompanhamento.setCpdAngariador(dadosProdutor.getCpdProdutor());
									}
									if (dadosProdutor.getCpfCnpjProdutor() != null) {
										acompanhamento.setCpfCnpjAngariador(dadosProdutor.getCpfCnpjProdutor());
									}
								} else if (dadosProdutor.getTipoProdutor() != null && dadosProdutor.getTipoProdutor().equals("2")) {
									if (dadosProdutor.getNomeProdutor() != null) {
										acompanhamento.setNomeCorretor(dadosProdutor.getNomeProdutor());
									}
									if (dadosProdutor.getCpdProdutor() != null) {
										acompanhamento.setCpdCorretor(dadosProdutor.getCpdProdutor());
									}
									if (dadosProdutor.getCpfCnpjProdutor() != null) {
										acompanhamento.setCpfCnpjCorretor(dadosProdutor.getCpfCnpjProdutor());
									}
								} else if (dadosProdutor.getTipoProdutor() != null && dadosProdutor.getTipoProdutor().equals("3")) {
									if (dadosProdutor.getNomeProdutor() != null) {
										acompanhamento.setNomeCorretorMaster(dadosProdutor.getNomeProdutor());
									}
									if (dadosProdutor.getCpdProdutor() != null) {
										acompanhamento.setCpdCorretorMaster(dadosProdutor.getCpdProdutor());
									}
									if (dadosProdutor.getCpfCnpjProdutor() != null) {
										acompanhamento.setCpfCnpjCorretorMaster(dadosProdutor.getCpfCnpjProdutor());
									}
								}
							}
						}
					}
				}
			}
			return listaAcompanhamentoProposta;
	}
	
	private boolean verificaSituacaoCancelada(AcompanhamentoPropostaVO acompanhamentoPropostaVO) {

		PropostaVO proposta = propostaDAO.consultarPorCodigo(acompanhamentoPropostaVO.getCodProposta());
		
		if(proposta != null){
			boolean temSituacaoCancelada = propostaDAO.possuiStatusCancelada(proposta.getNumeroSequencial());
			if (temSituacaoCancelada == true) {
				return true;
			} else {
				return false;
			}
		}
		return false;
		
	}

	private boolean verificaSituacaoImplantada(AcompanhamentoPropostaVO acompanhamentoPropostaVO) {

		PropostaVO proposta = propostaDAO.consultarPorCodigo(acompanhamentoPropostaVO.getCodProposta());
		
		if(proposta != null){
			boolean temSituacaoImplantada = propostaDAO.possuiStatusImplantada(proposta.getNumeroSequencial());
			if (temSituacaoImplantada == true) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	private String descricaoRetornoMovimentoCancelada(String codigoProposta) {

		PropostaVO proposta = propostaDAO.consultarPorCodigo(codigoProposta);
		if(proposta == null){
			return null;
		}
		return propostaDAO.descricaoRetornoMovimentoProposta(proposta.getNumeroSequencial(), 5); 
	}
	
	private String descricaoRetornoMovimentoImpantada(String codigoProposta){
		
		PropostaVO proposta = propostaDAO.consultarPorCodigo(codigoProposta);
		if(proposta == null){
			return null;
		}
		return propostaDAO.descricaoRetornoMovimentoProposta(proposta.getNumeroSequencial(), 6); 
	}
	
	private String descricaoRetornoMovimento(String codigoProposta){
		
		PropostaVO proposta = propostaDAO.consultarPorCodigo(codigoProposta);
		if(proposta == null){
			return null;
		}
		return propostaDAO.descricaoRetornoMovimentoProposta(proposta.getNumeroSequencial(), 6); 
	}
	
	
	
	/**
	 * Metodo responsavel por obter o valor do plano por data de emiss�o de proposta
	 * 
	 * @param proposta - a proposta.
	 * @return ValorPlanoVO - valor do plano.
	 */
	public PlanoVO obterPlanoPorDataEmissaoProposta(PropostaVO proposta) {
		return planoDAO.obterPlanoPorDataEmissaoProposta(proposta);
	}
}
