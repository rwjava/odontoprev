package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AgenciaBancariaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FormaPagamento;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ItemRelatorioAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;

/**
 * Permite listar as propostas para o relatório de acompanhamento
 * 
 * @author WDEV
 */
@Repository
public class RelatorioAcompanhamentoDAOImpl extends JdbcDao implements RelatorioAcompanhamentoDAO {

	@Autowired
	private DataSource dataSource;

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	public List<ItemRelatorioAcompanhamentoVO> listarPropostasPorFiltro(FiltroRelatorioAcompanhamentoVO filtro, LoginVO login) {
		StringBuilder sql = new StringBuilder()
			.append(" 	SELECT DISTINCT                                                                                    ")		
			.append("      proposta.cppsta_dntal_indvd,                                                                    ")
			.append("      proposta.cplano_dntal_indvd,                                                                    ")
			.append("      proposta.ccanal_vda_dntal_indvd,                                                                ")
			.append("      proposta.NSEQ_PPSTA_DNTAL,                                                                      ")
			.append("      proposta.CAG_PROTR_PLANO,                                                                       ")
			.append("      proposta.CIND_FORMA_PGTO,                                                                       ")
			.append("      proposta.CMATR_ASSTT_PROTR,                                                                     ")
			.append("      proposta.CMATR_GER_PRODT,                                                                       ")
			.append("      proposta.CAG_BCRIA,                                                                             ")
			.append("      proposta.NCTA_CORR,                                                                             ")
			.append("      retorno.RRETOR_MOVTO_PPSTA,                                                                     ")
			.append("      proponente.NCPF_PROPN_DNTAL,                                                                    ")
			.append("      proponente.IPROPN_DNTAL_INDVD,                                                                  ")
			.append("      corretor.CTPO_PROTR_PPSTA,                                                                      ")
			.append("      corretor.CCPD_protr,                                                                            ")
			.append("      corretor.NCPF_CNPJ_PROTR,                                                                       ")
			.append("      corretor.IPROTR_PPSTA_DNTAL,                                                                    ")
			.append("      responsavel.NCPF_RESP_LEGAL,                                                                    ")
			.append("      responsavel.IRESP_LEGAL_DNTAL,                                                                  ")
			.append("      beneficiario.NCPF_BNEFC_DNTAL,                                                                  ")
			.append("      beneficiario.ibnefc_dntal_indvd,                                                                ")
			.append("      agenciaDebito.CAG_BCRIA,                                                                        ")
			.append("      agenciaDebito.CDIG_AG_BCRIA,                                                                    ")
			.append("      nomeAgenciaDebito.IPSSOA    AS nomeAgenciaDebito,                                               ")
			.append("      nomeAgenciaProdutora.IPSSOA AS nomeAgenciaProdutora,                                            ")
			.append("      CASE                                                                                            ")
			.append("          WHEN proposta.CSUCUR_SEGDR_EMISR IS NOT NULL                                                ")
			.append("          THEN proposta.CSUCUR_SEGDR_EMISR                                                            ")
			.append("          WHEN proposta.CSUCUR_SEGDR_BVP IS NOT NULL                                                  ")
			.append("          THEN proposta.CSUCUR_SEGDR_BVP                                                              ")
			.append("      END AS SUCURSAL,                                                                                ")
			.append("      (                                                                                               ")
			.append("          SELECT                                                                                      ")
			.append("              COUNT(*)                                                                                ")
			.append("          FROM                                                                                        ")
			.append("              dbprod.bnefc_dntal_indvd beneficiario                                                   ")
			.append("          WHERE                                                                                       ")
			.append("              beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL) AS QTDE_VIDAS,                ")
			.append("      (                                                                                               ")
			.append("          SELECT                                                                                      ")
			.append("              MIN(movt.dinic_movto_ppsta)                                                             ")
			.append("          FROM                                                                                        ")
			.append("              dbprod.movto_ppsta_dntal movt                                                           ")
			.append("          WHERE                                                                                       ")
			.append("              proposta.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL) AS DT_CRIACAO_PROPOSTA,              ")
			.append("      (                                                                                               ")
			.append("          SELECT                                                                                      ")
			.append("              MAX(movimento.DINIC_MOVTO_PPSTA)                                                        ")
			.append("          FROM                                                                                        ")
			.append("              DBPROD.MOVTO_PPSTA_DNTAL movimento                                                      ")
			.append("          WHERE                                                                                       ")
			.append("              proposta.NSEQ_PPSTA_DNTAL = movimento.NSEQ_PPSTA_DNTAL) AS DT_STATUS_PROPOSTA,          ")
			.append("      movt.CSIT_DNTAL_INDVD                                                                           ")
			.append("  FROM                                                                                                ")
			.append("      dbprod.ppsta_dntal_indvd proposta                                                               ")
			.append("  INNER JOIN                                                                                          ")
			.append("      dbprod.movto_ppsta_dntal movt                                                                   ")
			.append("  ON                                                                                                  ")
			.append("      proposta.NSEQ_PPSTA_DNTAL = movt.NSEQ_PPSTA_DNTAL                                               ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      dbprod.propn_dntal_indvd proponente                                                             ")
			.append("  ON                                                                                                  ")
			.append("      proponente.cpropn_dntal_indvd=proposta.cpropn_dntal_indvd                                       ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      dbprod.resp_legal_dntal responsavel                                                             ")
			.append("  ON                                                                                                  ")
			.append("      responsavel.CRESP_LEGAL_DNTAL=proposta.CRESP_LEGAL_DNTAL                                        ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      dbprod.bnefc_dntal_indvd beneficiario                                                           ")
			.append("  ON                                                                                                  ")
			.append("      (                                                                                               ")
			.append("          beneficiario.NSEQ_PPSTA_DNTAL= proposta.NSEQ_PPSTA_DNTAL                                    ")
			.append("      AND beneficiario.cbnefc_dntal_indvd = 1)                                                        ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      dbprod.protr_ppsta_dntal corretor                                                               ")
			.append("  ON                                                                                                  ")
			.append("      (                                                                                               ")
			.append("          corretor.NSEQ_PPSTA_DNTAL = proposta.NSEQ_PPSTA_DNTAL                                       ")
			.append("      AND corretor.CTPO_PROTR_PPSTA = 2)                                                              ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      dbprod.RETOR_MOVTO_PPSTA retorno                                                                ")
			.append("  ON                                                                                                  ")
			.append("      (                                                                                               ")
			.append("          proposta.NSEQ_PPSTA_DNTAL = retorno.NSEQ_PPSTA_DNTAL                                        ")
			.append("      AND movt.dinic_movto_ppsta = retorno.DINIC_MOVTO_PPSTA                                          ")
			.append("      AND movt.CSIT_DNTAL_INDVD = retorno.CSIT_DNTAL_INDVD)                                           ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      DBPROD.AG_BCRIA agenciaDebito                                                                   ")
			.append("  ON                                                                                                  ")
			.append("      (                                                                                               ")
			.append("          agenciaDebito.CAG_BCRIA = proposta.CAG_BCRIA                                                ")
			.append("      AND agenciaDebito.cbco = proposta.cbco)                                                         ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      DBPROD.PSSOA_FIS_JURID nomeAgenciaDebito                                                        ")
			.append("  ON                                                                                                  ")
			.append("      (                                                                                               ")
			.append("          nomeAgenciaDebito.CPSSOA = agenciaDebito.CPSSOA)                                            ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      DBPROD.AG_BCRIA agenciaProdutora                                                                ")
			.append("  ON                                                                                                  ")
			.append("      (                                                                                               ")
			.append("          agenciaProdutora.CAG_BCRIA = proposta.CAG_PROTR_PLANO                                       ")
			.append("      AND agenciaProdutora.cbco = proposta.cbco)                                                      ")
			.append("  LEFT OUTER JOIN                                                                                     ")
			.append("      DBPROD.PSSOA_FIS_JURID nomeAgenciaProdutora                                                     ")
			.append("  ON                                                                                                  ")
			.append("      (                                                                                               ")
			.append("          nomeAgenciaProdutora.CPSSOA = agenciaProdutora.CPSSOA)                                      ")
			.append("  WHERE                                                                                               ")
			.append("      corretor.NCPF_CNPJ_PROTR = :cpfCnpjCorretor                                                     ")
			.append("  AND movt.dinic_movto_ppsta =                                                                        ")
			.append("      (                                                                                               ")
			.append("          SELECT                                                                                      ")
			.append("              MAX(movimento.DINIC_MOVTO_PPSTA)                                                        ")
			.append("          FROM                                                                                        ")
			.append("              DBPROD.MOVTO_PPSTA_DNTAL movimento                                                      ")
			.append("          WHERE                                                                                       ")
			.append("              proposta.NSEQ_PPSTA_DNTAL = movimento.NSEQ_PPSTA_DNTAL)                                 ");
	            
		
		
		// Filtros
		if (filtro.isDataInicioPreenchida()) {
			sql.append(" AND movt.dinic_movto_ppsta  >= :dataCriacaoPropostaMenor");
		}

		if (filtro.isDataFimPreenchida()) {
			sql.append(" AND movt.dinic_movto_ppsta  <= :dataCriacaoPropostaMaior");
		}

		if (filtro.isCodigoPropostaPreenchido()) {
			sql.append(" AND proposta.CPPSTA_DNTAL_INDVD LIKE :codigoProposta");
		}

		if (filtro.isCpfTitularPreenchido()) {
			sql.append(" AND beneficiario.NCPF_BNEFC_DNTAL = :cpfBenificiarioTitular");
		}

		if (filtro.isSucursalSeguradoraPreenchida()) {
			if (filtro.getSucursalSeguradora().isEmissora()) {
				sql.append(" AND proposta.CSUCUR_SEGDR_EMISR = :codSucursal");
				sql.append(" AND proposta.CSUCUR_SEGDR_BVP IS NULL");
			} else if (filtro.getSucursalSeguradora().isBVP()) {
				sql.append(" AND proposta.CSUCUR_SEGDR_BVP = :codSucursal");
				sql.append(" AND proposta.CSUCUR_SEGDR_EMISR IS NULL");
			}
		}

		if (filtro.isCpdCorretorPreenchido()) {
			sql.append(" AND corretor.CCPD_protr = :cpdCorretor");
		}

		if (filtro.isAgenciaDebitoPreenchida()) {
			sql.append(" AND proposta.CAG_BCRIA = :codigoAgenciaDebito");
		}

		if (filtro.isAgenciaProdutoraPreenchida()) {
			sql.append(" AND proposta.cag_protr_plano = :codigoAgenciaProdutora");
		}

		if (filtro.isMatriculaGerentePreenchida()) {
			sql.append(" AND proposta.CMATR_GER_PRODT = :matriculaGerente");
		}

		if (filtro.isMatriculaAssistentePreenchida()) {
			sql.append(" AND proposta.cmatr_asstt_protr = :codigoAssistenteBS");
		}

		if (filtro.isSituacaoPreenchida() && !SituacaoProposta.TODAS.equals(filtro.getSituacaoProposta())) {
			sql.append(" AND movt.CSIT_DNTAL_INDVD = :codigoSituacao ");
		}

		sql.append(" ORDER BY proposta.CPPSTA_DNTAL_INDVD ");

		MapSqlParameterSource params = new MapSqlParameterSource();

		if (filtro.isCpfCnpjCorretorPreenchido()) {
			params.addValue("cpfCnpjCorretor", Long.valueOf(filtro.getCorretor().getCpfCnpj()));
		} else {
			params.addValue("cpfCnpjCorretor", Long.valueOf(login.getId()));
		}

		if (filtro.isDataInicioPreenchida()) {
			params.addValue("dataCriacaoPropostaMenor", filtro.getDataInicio().toDateTimeAtStartOfDay().toDate());
		}

		if (filtro.isDataFimPreenchida()) {
			params.addValue("dataCriacaoPropostaMaior", filtro.getDataFim().toDateTime(new LocalTime(23, 59, 59)).toDate());
		}

		params.addValue("codigoProposta", "%" + filtro.getCodigoProposta() + "%");
		params.addValue("cpfBenificiarioTitular", Strings.desnormalizarCPF(filtro.getPessoa().getCpf()));

		if (filtro.isSucursalSeguradoraPreenchida()) {
			params.addValue("codSucursal", filtro.getSucursalSeguradora().getCodigo());
		}

		if (filtro.isCpdCorretorPreenchido()) {
			params.addValue("cpdCorretor", filtro.getCorretor().getCpd());
		}

		params.addValue("codigoAgenciaDebito", filtro.getCodigoAgenciaDebito());
		params.addValue("codigoAgenciaProdutora", filtro.getCodigoAgenciaProdutora());
		params.addValue("matriculaGerente", filtro.getMatriculaGerente());
		params.addValue("codigoAssistenteBS", filtro.getMatriculaAssistenteBS());

		if (filtro.isSituacaoPreenchida()) {
			params.addValue("codigoSituacao", filtro.getSituacaoProposta().getCodigo());
		}

		return getJdbcTemplate().query(sql.toString(), params, new PropostaRelatorioAcompanhamentoRowMapper());
	}

	/**
	 * Mapeamento de dados da listagem de proposta
	 * @author WDEV
	 */
	private static final class PropostaRelatorioAcompanhamentoRowMapper implements RowMapper<ItemRelatorioAcompanhamentoVO> {
		public ItemRelatorioAcompanhamentoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);

			PropostaVO proposta = new PropostaVO();
			proposta.setCodigo(Strings.maiusculas(resultSet.getString("cppsta_dntal_indvd")));
			proposta.setPlanoVO(new PlanoVO());
			proposta.getPlanoVO().setCodigo(rs.getLong("cplano_dntal_indvd"));
			
			proposta.setCanalVenda(new CanalVendaVO());
			proposta.getCanalVenda().setCodigo(rs.getInt("ccanal_vda_dntal_indvd"));
			
			proposta.setNumeroSequencial(resultSet.getLong("NSEQ_PPSTA_DNTAL"));

			proposta.setAgenciaProdutora(new AgenciaBancariaVO());
			proposta.getAgenciaProdutora().setCodigo(resultSet.getInteger("CAG_PROTR_PLANO"));
			proposta.getAgenciaProdutora().setNome(resultSet.getString("nomeAgenciaProdutora"));

			if (resultSet.getInteger("CIND_FORMA_PGTO") != null) {
				proposta.setFormaPagamento(FormaPagamento.obterPorCodigo(resultSet.getInteger("CIND_FORMA_PGTO")));
			}

			proposta.setCodigoMatriculaAssistente(resultSet.getString("CMATR_ASSTT_PROTR"));
			proposta.setCodigoMatriculaGerente(resultSet.getString("CMATR_GER_PRODT"));

			proposta.setProponente(new ProponenteVO());
			proposta.getProponente().setCpf(Strings.normalizarCPFMaiorQueZero(resultSet.getString("NCPF_PROPN_DNTAL")));
			proposta.getProponente().setNome(Strings.maiusculas(resultSet.getString("IPROPN_DNTAL_INDVD")));
			proposta.getProponente().setContaCorrente(new ContaCorrenteVO(resultSet.getString("NCTA_CORR")));

			proposta.getProponente().getContaCorrente().setAgenciaBancaria(new AgenciaBancariaVO());
			proposta.getProponente().getContaCorrente().getAgenciaBancaria().setCodigo(resultSet.getInteger("CAG_BCRIA"));
			proposta.getProponente().getContaCorrente().getAgenciaBancaria().setDigito(resultSet.getString("CDIG_AG_BCRIA"));
			proposta.getProponente().getContaCorrente().getAgenciaBancaria().setNome(resultSet.getString("nomeAgenciaDebito"));

			proposta.setMovimento(new MovimentoPropostaVO());
			proposta.getMovimento().setDescricao(Strings.maiusculas(resultSet.getString("RRETOR_MOVTO_PPSTA")));
			proposta.getMovimento().setSituacao(SituacaoProposta.obterPorCodigo(resultSet.getInteger("CSIT_DNTAL_INDVD")));
			proposta.getMovimento().setDataInicio(resultSet.getDateTime("DT_STATUS_PROPOSTA"));

			CorretorVO corretor = new CorretorVO();
			proposta.setCorretor(corretor);
			proposta.getCorretor().setCpd(resultSet.getInteger("CCPD_protr"));
			proposta.getCorretor().setCpfCnpj(Strings.normalizarCpfCnpj(resultSet.getString("NCPF_CNPJ_PROTR")));
			proposta.getCorretor().setNome(Strings.maiusculas(resultSet.getString("IPROTR_PPSTA_DNTAL")));

			proposta.setResponsavelLegal(new ResponsavelLegalVO());
			proposta.getResponsavelLegal().setCpf(Strings.normalizarCPFMaiorQueZero(resultSet.getString("NCPF_RESP_LEGAL")));
			proposta.getResponsavelLegal().setNome(Strings.maiusculas(resultSet.getString("IRESP_LEGAL_DNTAL")));

			proposta.setTitular(new TitularVO());
			proposta.getTitular().setNome(Strings.maiusculas(resultSet.getString("ibnefc_dntal_indvd")));
			proposta.getTitular().setCpf(Strings.normalizarCPFMaiorQueZero(resultSet.getString("NCPF_BNEFC_DNTAL")));

			proposta.setSucursalSeguradora(new SucursalSeguradoraVO(resultSet.getInteger("SUCURSAL")));
			proposta.setDataCriacao(resultSet.getLocalDate("DT_CRIACAO_PROPOSTA"));

			ItemRelatorioAcompanhamentoVO itemRelatorioAcompanhamentoVO = new ItemRelatorioAcompanhamentoVO();
			itemRelatorioAcompanhamentoVO.setProposta(proposta);
			itemRelatorioAcompanhamentoVO.setQuantidadeVidas(resultSet.getInteger("QTDE_VIDAS"));

			return itemRelatorioAcompanhamentoVO;
		}
	}

}