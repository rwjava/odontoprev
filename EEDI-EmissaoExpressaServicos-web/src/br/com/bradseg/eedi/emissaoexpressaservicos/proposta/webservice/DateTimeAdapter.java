package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;

/**
 * Adaptador de String para DateTime e DateTime para String para ser usado pelos webservices expostos
 * 
 * @author WDEV
 */
public class DateTimeAdapter extends XmlAdapter<String, DateTime> {

	@Override
	public DateTime unmarshal(String dataHora) {
		return DateTimeFormat.forPattern(Constantes.FORMATO_DATE_TIME_WEB_SERVICE).parseDateTime(dataHora);
	}

	@Override
	public String marshal(DateTime dataHora) {
		return dataHora.toString(Constantes.FORMATO_DATE_TIME_WEB_SERVICE);
	}

}