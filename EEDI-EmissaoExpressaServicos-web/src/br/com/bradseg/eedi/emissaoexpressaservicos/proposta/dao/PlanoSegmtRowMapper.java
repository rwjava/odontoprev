package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoSegmtVO;

public class PlanoSegmtRowMapper implements RowMapper<PlanoSegmtVO> {
	
	public PlanoSegmtVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		PlanoSegmtVO plano = new PlanoSegmtVO();
		
		plano.setCodigoPlano(rs.getLong("CPLANO_DNTAL_INDVD"));
		String periodoCarenciaAnualPipe = rs.getString("RPER_CAREN_PLANO_ANO");
		String periodoCarenciaMensalPipe = rs.getString("RPER_CAREN_PLANO_MES");
		if(periodoCarenciaAnualPipe != null){
			periodoCarenciaAnualPipe = periodoCarenciaAnualPipe.replaceAll(", ", "| ");
		}
		if(periodoCarenciaMensalPipe != null){
			periodoCarenciaMensalPipe = periodoCarenciaMensalPipe.replaceAll(", ", "| ");
		}
		plano.setPeriodoCarenciaAnual(periodoCarenciaAnualPipe);
		plano.setPeriodoCarenciaMensal(periodoCarenciaMensalPipe);
		plano.setNomePlano(rs.getString("RPLANO_DNTAL_INDVD"));
		plano.setNomePlanoANS(rs.getString("RPLANO_ANS_DNTAL_INDVD"));
		plano.setValorAnualDependente(rs.getDouble("VANUDD_PLANO_DNTAL_DEPDT"));
		plano.setValorAnualTitular(rs.getDouble("VANUDD_PLANO_DNTAL_TTLAR"));
		plano.setValorMensalDependente(rs.getDouble("VMESD_PLANO_DEPDT"));
		plano.setValorMensalTitular(rs.getDouble("VMESD_PLANO_TTLAR"));
		plano.setCodigoPlanoANS(rs.getLong("CPLANO_ANS_DNTAL_INDVD"));

		return plano;
	}	

}
