package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Arquivo;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AgenciaBancariaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DependenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TelefoneVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoBeneficiario;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoEndereco;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;

import com.google.common.collect.Lists;

/**
 * Gera o arquivo de benefici�rios associados a proposta
 * @author WDEV
 */
public class GeradorArquivoAssociados {

	public Arquivo gerar(Integer sequencial, LocalDate dataInicialPesquisaMovimento, List<PropostaVO> propostas) {
		DateTime dataGeracaoArquivo = new DateTime();
		int totalRegistros = 2;

		Arquivo arquivo = new Arquivo();
		arquivo.setNome(getNomeArquivo(sequencial));

		new Cabecalho().escrever(arquivo, dataGeracaoArquivo, sequencial);

		for (PropostaVO proposta : propostas) {
			// Escreve os detalhes do titular
			TitularVO titular = proposta.getTitular();
			titular.setProposta(proposta);
			new DetalheTitular(titular).escrever(arquivo);

			totalRegistros++;

			if (proposta.getDependentes() != null) {

				// Escreve os detalhes para cada dependente
				for (DependenteVO dependente : proposta.getDependentes()) {
					dependente.setProposta(proposta);
					new DetalheDependente(dependente).escrever(arquivo);

					totalRegistros++;
				}
			}
		}

		new Rodape().escrever(arquivo, totalRegistros, dataGeracaoArquivo);

		return arquivo;
	}

	/**
	 * Monta o nome do arquivo de associados 
	 * 
	 * @param sequencial sequencial da proposta
	 * @return nome do arquivo
	 */
	private String getNomeArquivo(Integer sequencial) {
		StringBuilder nomeArquivo = new StringBuilder();
		nomeArquivo.append(Constantes.PREFIXO_ARQUIVO_ASSOCIADOS).append(StringUtils.leftPad(sequencial.toString(), 8, "0")).append(Constantes.EXTENSAO_ARQUIVO).append(".txt");
		return nomeArquivo.toString();
	}

	/**
	 * Cabe�alho do arquivo de associados
	 * @author WDEV
	 */
	protected static final class Cabecalho {
		public void escrever(Arquivo arquivo, DateTime dataGeracaoArquivo, Integer sequencial) {
			//@formatter:off
			arquivo
				.texto(Constantes.CODIGO_HEADER)
				.texto(Constantes.BSP, 50)
				.texto(Constantes.ODONTOPREV, 50)
				.data(dataGeracaoArquivo, Constantes.FORMATO_DATA_PT_BR)
				.numero(sequencial , 8)
				.texto("" , 1369)
				.quebraLinha();				
			//@formatter:on
		}
	}

	/**
	 * Detalhe do Dependente
	 * 
	 * @author WDEV
	 *
	 */
	private class DetalheDependente extends DetalheBeneficiario {

		private DependenteVO dependente;

		public DetalheDependente(DependenteVO dependente) {
			super(dependente);
			this.dependente = dependente;
		}

		protected Integer getCodigoTipoBeneficiario() {
			return TipoBeneficiario.DEPENDENTE.getCodigo();
		}

		protected Integer getGrauParentesco() {
			return dependente.getGrauParentesco().getCodigo();
		}

	}

	/**
	 * Detalhe do Titular
	 * 
	 * @author WDEV
	 *
	 */
	private class DetalheTitular extends DetalheBeneficiario {

		public DetalheTitular(TitularVO titular) {
			super(titular);
		}

		protected Integer getCodigoTipoBeneficiario() {
			return TipoBeneficiario.TITULAR.getCodigo();
		}

		protected Integer getGrauParentesco() {
			return null;
		}

	}

	/**
	 * Detalhe do Benefici�rio
	 * 
	 * @author WDEV
	 *
	 */
	private abstract class DetalheBeneficiario {

		private PropostaVO proposta;

		private BeneficiarioVO beneficiario;

		protected abstract Integer getCodigoTipoBeneficiario();

		protected abstract Integer getGrauParentesco();

		protected DetalheBeneficiario(BeneficiarioVO beneficiario) {
			this.beneficiario = beneficiario;
			this.proposta = beneficiario.getProposta();
		}

		public void escrever(Arquivo arquivo) {
			//@formatter:off
			arquivo
				.texto(Constantes.CODIGO_CONTRATO, 10)
				.texto(Constantes.CODIGO_STATUS_INCLUSAO)
				.numero(getCodigoTipoBeneficiario(), 1)
				.texto(proposta.getCodigo(), 15)
				//TODO: REVER MATRICULA SEGURADO (Somar toString() ou Somar os valores num�ricos)
				.texto(proposta.getNumeroSequencial().toString() + beneficiario.getCodigo().toString(), 20);
				
				//TODO: //Grau de parentesco - Se for titular preencher com espa�os
				if(TipoBeneficiario.TITULAR.equals(beneficiario.getTipoBeneficiario())){
					arquivo.branco(2);
				}else{
					arquivo.numero(getGrauParentesco(), 2);					
				}				
			
			arquivo
				.texto(beneficiario.getNome(), 70)
				.data(beneficiario.getDataNascimento(), Constantes.FORMATO_DATA_PT_BR)
				.texto(beneficiario.getSexo().name())
				.texto(Strings.desnormalizarCPF(beneficiario.getCpf()), 11)
				.branco(11) //PIS/PASEP/NIT
				.texto(beneficiario.getNumeroCartaoNacionalSaude(), 15)
				.texto(beneficiario.getNomeMae(), 70)
				.numero(beneficiario.getNumeroDeclaracaoNascidoVivo(), 11)
				.numero(beneficiario.getEstadoCivil().getCodigo(), 2);
			
			escreverEnderecoTitular(arquivo, proposta.getTitular().getEndereco());
			escreverTelefone(arquivo);
			
			arquivo
				.numero(proposta.getPlanoVO().getCodigo(), 9)
				.data(proposta.getMovimento().getDataInicio(), Constantes.FORMATO_DATA_PT_BR)
				.branco(10) // DT_CANCELAMENTO
				.texto("00", 2); // CODIGO_MOTIVO_CANCELAMENTO

			EnderecoVO enderecoResponsavelLegal = escreverDadosCobranca(arquivo);	
			escreverTelefonesProponente(proposta.getProponente().getTelefones(), arquivo);
			
			arquivo
				.texto(proposta.getProponente().getEmail(), 50)
				.texto("00", 2) // Dia do vencimento - ENVIAR VAZIO segundo a defini��o de layout v10
				.data(new LocalDate(), Constantes.FORMATO_DATA_PT_BR);
			
			escreverInformacoesBancarias(arquivo, proposta.getProponente());

			arquivo.numero(proposta.getCodigoMatriculaAssistente(), 9);

			escreverSucursalSeguradora(arquivo, proposta.getSucursalSeguradora());

			escreverInformacoesCorretores(arquivo);
									
			arquivo.numero(proposta.getCodigoMatriculaGerente(), 9);
									
			if(proposta.getFormaPagamento() == null){
				arquivo.texto("1");
			} else {
				arquivo.numero(proposta.getFormaPagamento().getCodigo());
			}
			
			arquivo
				.numero(1, 1) // TIPO_NEGOCIA��O conforme acordado � sempre mensal.
				.numero(proposta.getTipoComissao().getCodigo(), 2);
			
			escreverInformacoesEnderecoIBGE(arquivo, proposta.getTitular().getEndereco(), enderecoResponsavelLegal);
			
			if(possuiCorretorMaster()){
				arquivo
					.texto(proposta.getCorretorMaster().getNome(), 70)
					.numero(proposta.getCorretorMaster().getCpfCnpj(), 14);
			} else {
				arquivo
					.branco(70)
					.zeros(14);
			}
			
			arquivo
				.texto(proposta.getProponente().getNome(), 70)
				.numero(proposta.getProponente().getCpf(), 11)
				.texto("00", 2)
				.branco(16);
				

			if(proposta.getSubsegmentacao() != null){
				arquivo.numero(proposta.getSubsegmentacao().getCodigo(), 3);
			} else {
				arquivo.zeros(3);
			}
			
			arquivo
				.zeros(5) //Numero da ag�ncia de produ��o
				.quebraLinha();
			
			
			//@formatter:on
		}

		private void escreverSucursalSeguradora(Arquivo arquivo, SucursalSeguradoraVO sucursalSeguradora) {
			if (sucursalSeguradora.isEmissora()) {
				arquivo.numero(sucursalSeguradora.getCodigo(), 3).zeros(3);
			} else if (sucursalSeguradora.isBVP()) {
				arquivo.zeros(3).numero(sucursalSeguradora.getCodigo(), 3);
			}
		}

		private void escreverInformacoesEnderecoIBGE(Arquivo arquivo, EnderecoVO enderecoTitular, EnderecoVO enderecoResponsavelLegal) {
			//@formatter:off
			arquivo.
				numero(enderecoTitular.getCodigoUfIbge(), 2)
				.numero(enderecoTitular.getSiglaIbgeMunicipio(), 5)
				.numero(enderecoResponsavelLegal.getCodigoUfIbge(), 2)
				.numero(enderecoResponsavelLegal.getSiglaIbgeMunicipio(), 5);			
			//@formatter:on	
		}

		private void escreverInformacoesCorretores(Arquivo arquivo) {
			//@formatter:off
			// Dados do produtor Angariador
			escreverInformacoesProdutor(arquivo, proposta.getAngariador());
			// Dados do produtor Corretor
			escreverInformacoesProdutor(arquivo, proposta.getCorretor());
			
			// TODO N�o est� escrevendo todos os dados do corretor master, segundo c�digo antigo est� esperando aceite da bradesco
			// Dados do produtor Corretor Master
			//escreverInformacoesProdutor(arquivo, proposta.getCorretorMaster());
			if (possuiCorretorMaster()) {
				arquivo.numero(proposta.getCorretorMaster().getCpd(), 9);				
			} else {
				arquivo.zeros(9);
			}			
			//@formatter:on
		}

		private void escreverInformacoesProdutor(Arquivo arquivo, ProdutorVO produtor) {
			//@formatter:off
			if (produtor == null) {
				arquivo
					.zeros(9) //CPD do angariador
					.branco(70) //Nome do angariador
					.zeros(14); //CPF/CNPJ do angariador
			} else {
				arquivo
					.numero(produtor.getCpd(), 9)
					.texto(produtor.getNome(), 70)
					.numero(produtor.getCpfCnpj(), 14);
			}
			//@formatter:on
		}

		private boolean possuiCorretorMaster() {
			return proposta.getCorretorMaster() != null;
		}

		private void escreverInformacoesBancarias(Arquivo arquivo, ProponenteVO proponente) {
			//@formatter:off
			ContaCorrenteVO contaCorrente = proponente.getContaCorrente();
			AgenciaBancariaVO agenciaBancaria = contaCorrente.getAgenciaBancaria();
			arquivo
				.numero(agenciaBancaria.getCodigo(), 4)
				.numero(agenciaBancaria.getDigito(), 1)
				.texto(agenciaBancaria.getNome(), 20)//Nome da agencia, sempre vir� nulo, pq n�o � gravado no banco de dados
				.numero(contaCorrente.getNumero(), 7)
				.texto(contaCorrente.getDigitoVerificador(), 1);
		}

		/**
		 * Escreve os dados de cobran�a da proposta. Utiliza os dados do titular ou do respons�vel legal do benefici�rio, caso o titular seja menor de idade. 
		 * @param arquivo Arquivo sendo escrito
		 */
		private EnderecoVO escreverDadosCobranca(Arquivo arquivo) {
			//@formatter:off
			
			// Por padr�o, utilizar� o endere�o do titular da proposta
			EnderecoVO endereco = proposta.getTitular().getEndereco();
			
			if (beneficiario.isMenorDeIdade() && proposta.isPossuiResponsavelLegal()) {
				// Escreve os dados do respons�vel legal
				ResponsavelLegalVO responsavelLegal = proposta.getResponsavelLegal();
				arquivo
					.texto(responsavelLegal.getNome(), 70)
					.data(responsavelLegal.getDataNascimento(), Constantes.FORMATO_DATA_PT_BR)
					.texto(responsavelLegal.getCpf(), 11);
			
				// Se o respons�vel legal possuir endere�o, usar� o dele
				if (responsavelLegal.isPossuiEndereco()) {
					endereco = responsavelLegal.getEndereco();
				}
			} else {
				// Escreve os dados do pr�prio benefici�rio
				arquivo
					.texto(beneficiario.getNome(), 70)
					.data(beneficiario.getDataNascimento(), Constantes.FORMATO_DATA_PT_BR)
					.texto(Strings.desnormalizarCPF(beneficiario.getCpf()), 11);
			}
			
			// Escreve os dados do endere�o
			escreverEnderecoCobranca(arquivo, endereco);
			return endereco;
			//@formatter:on
		}

		/**
		 * Escreve os telefones do proponente, caso ele possua menos de 3 telefones, 
		 * completa com zero e vazio at� completar os 3 telefones
		 * 
		 * @param telefones Telefones do proponente
		 * @param arquivo Arquivo sendo escrito
		 */
		private void escreverTelefonesProponente(List<TelefoneVO> telefones, Arquivo arquivo) {
			/*
			 * TODO No EEDI Antigo n�o � preenchido nenhum telefone cadastrado na proposta, apesar de possuirmos as informa��es. 
			 * Zeramos a lista para que o comportamento fique igual ao antigo
			 */
			telefones = Lists.newArrayList();

			for (TelefoneVO telefone : telefones) {
				//@formatter:off
				arquivo
					.numero(telefone.getDdd(), 3)
					.texto(telefone.getNumero(), 15)
					.texto(telefone.getRamal(), 20);
				//@formatter:on
			}

			preencherTelefonesProponente(arquivo, telefones.size());
		}

		/**
		 * Preenche o telefone do proponente com zero e branco at� atingir a quantidade de 3 telefones
		 * 
		 * @param arquivo Arquivo sendo escrito
		 * @param quantidadeTelefones Quantidade de telefones do proponentes
		 */
		private void preencherTelefonesProponente(Arquivo arquivo, int quantidadeTelefones) {
			for (int i = quantidadeTelefones; i < 3; i++) {
				//@formatter:off
				arquivo
					.zeros(3)
					.branco(15)
					.branco(20);
				//@formatter:on
			}
		}

		private void escreverTelefone(Arquivo arquivo) {
			//@formatter:off
			arquivo
				.branco(30) //REFERENCIA ENDERE�O
				.zeros(3) //TELEFONES DO BENEFICIARIO
				.branco(15)
				.branco(20)
				.zeros(3)
				.branco(15)
				.branco(20)
				.zeros(3)
				.branco(15)
				.branco(20)
				.branco(50);
		}
		
		private void escreverEnderecoTitular(Arquivo arquivo, EnderecoVO endereco) {
			escreverEndereco(arquivo, endereco, TipoEndereco.RESIDENCIAL);
		}
		
		private void escreverEnderecoCobranca(Arquivo arquivo, EnderecoVO endereco) {
			escreverEndereco(arquivo, endereco, TipoEndereco.COBRANCA);
		}
		
		private void escreverEndereco(Arquivo arquivo, EnderecoVO endereco, TipoEndereco tipoEndereco) {
			//@formatter:off
			arquivo.texto(endereco.getLogradouro(), 50);
				
			// TODO Deve ser 10 ou 5? (Atualmente para o titular � usado 10 e para o respons�vel legal 5). A coluna no banco de dados � tamanho 6
			if(TipoEndereco.COBRANCA.equals(tipoEndereco)) {
				arquivo.texto(endereco.getNumero(), 5);
			} else {
				arquivo.texto(endereco.getNumero(), 10);
			}
			
			arquivo
				.texto(endereco.getComplemento(), 15)
				.texto(endereco.getBairro(), 30)
				.texto(endereco.getCidade(), 30)
				.texto(endereco.getUnidadeFederativa().getSigla(), 2)
				.texto(endereco.getCep(), 8);
			//@formatter:on
		}
	}

	/**
	 * Rodap� do arquivo de associados
	 * @author WDEV
	 */
	protected static final class Rodape {
		public void escrever(Arquivo arquivo, int totalRegistros, DateTime dataGeracaoArquivo) {
			//@formatter:off
			arquivo
				.texto(Constantes.CODIGO_TRAILLER)
				.numero(totalRegistros, 10)				
				.branco(1477)				
				.quebraLinha();
			//@formatter:on
		}
	}

}
