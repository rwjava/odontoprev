package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.FormatacaoUtil;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AcompanhamentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoProdutor;

public class AcompanhamentoPropostaEEDIRowMapper implements RowMapper<AcompanhamentoPropostaVO>{

	public AcompanhamentoPropostaVO mapRow(ResultSet rs, int arg1) throws SQLException {

		AcompanhamentoPropostaVO acompanhamentoPropostaVO = new AcompanhamentoPropostaVO();
		
		acompanhamentoPropostaVO.setNomeTitularConta(rs.getString("Nome_titular_conta"));
		acompanhamentoPropostaVO.setCodigoAgenciaProdutora(rs.getLong("Agencia_produtora"));
		acompanhamentoPropostaVO.setCodProposta(rs.getString("Codigo_Proposta"));

		if (rs.getString("protocolo") != null && !rs.getString("protocolo").equals("")) {
			acompanhamentoPropostaVO.setProtocolo(rs.getString("protocolo").trim());
		}
		if (rs.getString("CPF_titular_conta") != null && !rs.getString("CPF_titular_conta").equals("0")) {
			acompanhamentoPropostaVO.setCpfTitularConta(FormatacaoUtil.formataCPF(rs.getString("CPF_titular_conta")));
		}
		if (rs.getString("CPF_Responsavel") != null && !rs.getString("CPF_Responsavel").equals("0")) {
			acompanhamentoPropostaVO.setCpfResponsavel(FormatacaoUtil.formataCPF(rs.getString("CPF_Responsavel")));
		}
		acompanhamentoPropostaVO.setNomeResponsavel(rs.getString("Nome_responsavel"));
		acompanhamentoPropostaVO.setCodigoAssistenteBS(rs.getLong("Assistente_BS"));
		if (rs.getString("CPF_Beneficiario") != null && !rs.getString("CPF_Beneficiario").equals("0")) {
			acompanhamentoPropostaVO.setCpfBenificiarioTitular(FormatacaoUtil.formataCPF(rs.getString("CPF_Beneficiario")));
		}
		acompanhamentoPropostaVO.setNomeBenificiarioTitular(rs.getString("Nome_beneficiario"));
		acompanhamentoPropostaVO.setSucursal(rs.getLong("Sucursal"));
		// TODO n�o entendi esse forma_pagamento em situa��o proposta
		acompanhamentoPropostaVO.setSituacaoProposta(rs.getInt("Forma_Pagamento"));
		acompanhamentoPropostaVO.setTipoProdutor(TipoProdutor.obterPorCodigo(rs.getInt("Tipo_Corretor")));
		// regra informada pela L�vea.
		if (acompanhamentoPropostaVO.getTipoProdutor() != null) {
			// Angariador
			if (acompanhamentoPropostaVO.getTipoProdutor().getCodigo() == 1) {
				acompanhamentoPropostaVO.setCpdAngariador(rs.getString("cpd_corretor"));
				String cpfCnpj = rs.getString("CPF_CNPJ");
				if (cpfCnpj != null) {
					if (cpfCnpj.length() == 11) {
						acompanhamentoPropostaVO.setCpfCnpjAngariador(FormatacaoUtil.formataCPF(rs.getString("CPF_CNPJ")));
					} else {
						acompanhamentoPropostaVO.setCpfCnpjAngariador(FormatacaoUtil.formataCNPJ(rs.getString("CPF_CNPJ")));
					}
				}
				acompanhamentoPropostaVO.setNomeAngariador(rs.getString("Nome_corretor"));
			} else if (acompanhamentoPropostaVO.getTipoProdutor().getCodigo() == 2) { // Corretor
				acompanhamentoPropostaVO.setCpdCorretor(rs.getString("cpd_corretor"));
				String cpfCnpj = rs.getString("CPF_CNPJ");
				if (cpfCnpj != null) {
					if (cpfCnpj.length() == 11) {
						acompanhamentoPropostaVO.setCpfCnpjCorretor(FormatacaoUtil.formataCPF(rs.getString("CPF_CNPJ")));
					} else {
						acompanhamentoPropostaVO.setCpfCnpjCorretor(FormatacaoUtil.formataCNPJ(rs.getString("CPF_CNPJ")));
					}
				}
				acompanhamentoPropostaVO.setNomeCorretor(rs.getString("Nome_corretor"));
			} else if (acompanhamentoPropostaVO.getTipoProdutor().getCodigo() == 3) { // Corretor Master
				acompanhamentoPropostaVO.setCpdCorretorMaster(rs.getString("cpd_corretor"));
				String cpfCnpj = rs.getString("CPF_CNPJ");
				if (cpfCnpj != null) {
					if (rs.getString("CPF_CNPJ").length() == 11) {
						acompanhamentoPropostaVO.setCpfCnpjCorretorMaster(FormatacaoUtil.formataCPF(rs.getString("CPF_CNPJ")));
					} else {
						acompanhamentoPropostaVO.setCpfCnpjCorretorMaster(FormatacaoUtil.formataCNPJ(rs.getString("CPF_CNPJ")));
					}
				}
				acompanhamentoPropostaVO.setNomeCorretorMaster(rs.getString("Nome_corretor"));
			}
		}
		acompanhamentoPropostaVO.setAgenciaDebito(rs.getString("Agencia_debito"));
		acompanhamentoPropostaVO.setQuantidadeVidasProposta(rs.getString("Quantidade_Vidas"));
		acompanhamentoPropostaVO.setDataInicioCriacaoCodigo(rs.getString("Data_Inicio_criacao_codigo"));
		acompanhamentoPropostaVO.setDataCriacaoProposta(new DateTime(rs.getDate("Data_Criacao")));
		acompanhamentoPropostaVO.setDataInicioStatusAtualProposta(rs.getString("Data_Inicio_Status_Atual"));
		if (rs.getString("situacao").equals("EM_PROCESSAMENTO")) {
			acompanhamentoPropostaVO.setStatusAtualProposta("EM PROCESSAMENTO");
		} else if (rs.getString("situacao").equals("PRE_CANCELADA")) {
			acompanhamentoPropostaVO.setStatusAtualProposta("PR�-CANCELADA");
		} else {
			acompanhamentoPropostaVO.setStatusAtualProposta(rs.getString("situacao"));
		}
		acompanhamentoPropostaVO.setValorTotalProposta(rs.getBigDecimal("Valor_Total"));

		// acompanhamentoPropostaVO.setNomeAgenciaDebito("Teste: Ag�ncia de D�bito");
		// acompanhamentoPropostaVO.setNomeAgenciaProdutora("Teste: Ag�ncia Produtora");

		if (rs.getString("gerenteProdBVP") != null && !rs.getString("gerenteProdBVP").equals("")) {
			acompanhamentoPropostaVO.setCodigoGerenteProdutoBVP(Long.parseLong(rs.getString("gerenteProdBVP")));
		}
		acompanhamentoPropostaVO.setCodigoCanalVenda(rs.getLong("CANAL_DE_VENDA"));
		acompanhamentoPropostaVO.setTipoCobranca(TipoCobranca.buscaPor(rs.getInt("tipo_cobranca")));
		acompanhamentoPropostaVO.setCodigoPlano(rs.getLong("PLANO"));
		acompanhamentoPropostaVO.setCodigoPostoAtendimento(rs.getLong("posto_atendimento"));
		
		return acompanhamentoPropostaVO;
	}

}
