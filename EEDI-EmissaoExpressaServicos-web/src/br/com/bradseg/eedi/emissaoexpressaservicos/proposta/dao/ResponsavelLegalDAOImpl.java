package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.endereco.dao.EnderecoRowMapper;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TelefoneVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoTelefone;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UsuarioUltimaAtualizacao;

/**
 * Implementação do acesso a dados do responsavel legal
 * 
 * @author WDEV
 */
@Repository
public class ResponsavelLegalDAOImpl extends JdbcDao implements ResponsavelLegalDAO {

	@Autowired
	private DataSource dataSource;

	private static final Logger LOGGER = LoggerFactory.getLogger(ResponsavelLegalDAOImpl.class);

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	public ResponsavelLegalVO listarPorProposta(PropostaVO proposta) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" RESP_LEGAL_DNTAL.CRESP_LEGAL_DNTAL,");
		sql.append(" RESP_LEGAL_DNTAL.IRESP_LEGAL_DNTAL,");
		sql.append(" RESP_LEGAL_DNTAL.NCPF_RESP_LEGAL,");
		sql.append(" RESP_LEGAL_DNTAL.DNASC_RESP_LEGAL,");
		sql.append(" RESP_LEGAL_DNTAL.REMAIL_RESP_LEGAL,");
		sql.append(" ENDER_DNTAL_INDVD.CENDER_DNTAL_INDVD,");
		sql.append(" ENDER_DNTAL_INDVD.ILOGDR_ENDER_COTAC,");
		sql.append(" ENDER_DNTAL_INDVD.NLOGDR_ENDER_COTAC,");
		sql.append(" ENDER_DNTAL_INDVD.RCOMPL_ENDER_COTAC,");
		sql.append(" ENDER_DNTAL_INDVD.IBAIRO,");
		sql.append(" ENDER_DNTAL_INDVD.ICIDDE,");
		sql.append(" ENDER_DNTAL_INDVD.CSGL_UF,");
		sql.append(" ENDER_DNTAL_INDVD.CNRO_CEP,");
		sql.append(" FONE_RESP_LEGAL.CFONE_RESP_LEGAL,");
		sql.append(" FONE_RESP_LEGAL.NDDD_FONE,");
		sql.append(" FONE_RESP_LEGAL.NFONE,");
		sql.append(" FONE_RESP_LEGAL.NRMAL_FONE,");
		sql.append(" FONE_RESP_LEGAL.CTPO_FONE");
		sql.append(" FROM DBPROD.RESP_LEGAL_DNTAL RESP_LEGAL_DNTAL");
		sql.append(" INNER JOIN DBPROD.PPSTA_DNTAL_INDVD PPSTA_DNTAL_INDVD ON (PPSTA_DNTAL_INDVD.CRESP_LEGAL_DNTAL = RESP_LEGAL_DNTAL.CRESP_LEGAL_DNTAL)");
		sql.append(" INNER JOIN DBPROD.FONE_RESP_LEGAL FONE_RESP_LEGAL ON (FONE_RESP_LEGAL.CRESP_LEGAL_DNTAL = PPSTA_DNTAL_INDVD.CRESP_LEGAL_DNTAL)");
		sql.append(" INNER JOIN DBPROD.ENDER_RESP_PPSTA ENDER_RESP_PPSTA ON (ENDER_RESP_PPSTA.CRESP_LEGAL_DNTAL = RESP_LEGAL_DNTAL.CRESP_LEGAL_DNTAL)");
		sql.append(" INNER JOIN DBPROD.ENDER_DNTAL_INDVD ENDER_DNTAL_INDVD ON (ENDER_RESP_PPSTA.CENDER_DNTAL_INDVD = ENDER_DNTAL_INDVD.CENDER_DNTAL_INDVD)");
		sql.append(" WHERE PPSTA_DNTAL_INDVD.NSEQ_PPSTA_DNTAL = :idSequencial");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("idSequencial", proposta.getNumeroSequencial());
        LOGGER.error("INFO: LISTAR RESPONSÁVEL TITULAR: "+sql.toString()+" PARÂMETROS: "+params.getValues());
		try {
			return getJdbcTemplate().queryForObject(sql.toString(), params, new ResponsavelLegalRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error("INFO: Nenhum responsável legal encontrado", e);
			return null;
		}
	}

	/**
	 * Mapeamento de dados do responsavel legal
	 * @author WDEV
	 */
	private static final class ResponsavelLegalRowMapper implements RowMapper<ResponsavelLegalVO> {
		public ResponsavelLegalVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
			ResponsavelLegalVO responsavelLegal = new ResponsavelLegalVO();
			responsavelLegal.setCodigo(resultSet.getLong("CRESP_LEGAL_DNTAL"));
			responsavelLegal.setNome(Strings.maiusculas(resultSet.getString("IRESP_LEGAL_DNTAL")));
			responsavelLegal.setCpf(Strings.normalizarCpfCnpj(resultSet.getString("NCPF_RESP_LEGAL")));
			responsavelLegal.setDataNascimento(resultSet.getLocalDate("DNASC_RESP_LEGAL"));
			responsavelLegal.setEmail(Strings.maiusculas(resultSet.getString("REMAIL_RESP_LEGAL")));

			if (resultSet.getLong("CENDER_DNTAL_INDVD") != null) {
				responsavelLegal.setEndereco(new EnderecoRowMapper().mapRow(rs, rowNum));
			}

			if (resultSet.getLong("NFONE") != null) {
				TelefoneVO telefone = new TelefoneVO();
				telefone.setCodigo(resultSet.getInteger("CFONE_RESP_LEGAL"));
				telefone.setDdd(resultSet.getString("NDDD_FONE"));
				telefone.setNumero(resultSet.getString("NFONE"));
				telefone.setRamal(resultSet.getInteger("NRMAL_FONE"));
				telefone.setTipo(TipoTelefone.buscaPorCodigo(resultSet.getInteger("CTPO_FONE")));
				responsavelLegal.setTelefone(telefone);
			}
			return responsavelLegal;
		}
	}

	public ResponsavelLegalVO salvar(ResponsavelLegalVO responsavelLegal) {
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO DBPROD.RESP_LEGAL_DNTAL");
		sql.append(" (CRESP_LEGAL_DNTAL, IRESP_LEGAL_DNTAL, NCPF_RESP_LEGAL, DNASC_RESP_LEGAL, REMAIL_RESP_LEGAL, DULT_ATULZ_REG, CRESP_ULT_ATULZ)");
		sql.append(" VALUES");
		sql.append(" (:codigo, :nome, :cpf, :dataNascimento, :email, CURRENT_TIMESTAMP, :usuarioUltimaAtualizacao)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigo", responsavelLegal.getCodigo());
		params.addValue("nome", Strings.maiusculas(responsavelLegal.getNome()));
		params.addValue("cpf", Strings.desnormalizarCPF(responsavelLegal.getCpf()));
		params.addValue("dataNascimento", responsavelLegal.getDataNascimento().toDateTimeAtCurrentTime().toDate());
		params.addValue("email", Strings.maiusculas(responsavelLegal.getEmail()));
		params.addValue("usuarioUltimaAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());
		
		LOGGER.error("INFO: INSERT RESPONSÁVEL TITULAR "+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);

		return responsavelLegal;
	}

	public void remover(ResponsavelLegalVO responsavelLegal) {
		StringBuilder sql = new StringBuilder();
		sql.append(" DELETE FROM DBPROD.RESP_LEGAL_DNTAL WHERE CRESP_LEGAL_DNTAL = :responsavelLegal");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("responsavelLegal", responsavelLegal.getCodigo());

		getJdbcTemplate().update(sql.toString(), params);
	}

	public void associarEndereco(ResponsavelLegalVO responsavelLegal, EnderecoVO endereco) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO DBPROD.ENDER_RESP_PPSTA (CRESP_LEGAL_DNTAL,	CENDER_DNTAL_INDVD) VALUES (:responsavelLegal, :endereco)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("responsavelLegal", responsavelLegal.getCodigo());
		params.addValue("endereco", endereco.getCodigo());
		LOGGER.error("INFO: INSERT ENDEREÇO RESPONSÁVEL TITULAR "+sql.toString()+" PARÂMETROS: "+params.getValues());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public void desassociarEndereco(ResponsavelLegalVO responsavelLegal, EnderecoVO endereco) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM DBPROD.ENDER_RESP_PPSTA WHERE CRESP_LEGAL_DNTAL = :responsavelLegal AND CENDER_DNTAL_INDVD = :endereco");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("responsavelLegal", responsavelLegal.getCodigo());
		params.addValue("endereco", endereco.getCodigo());
		LOGGER.error("INFO: "+sql.toString());
		getJdbcTemplate().update(sql.toString(), params);
	}

	public Long obterCodigoSequencial() {
		return getJdbcTemplate().queryForLong("SELECT NEXT VALUE FOR DBPROD.SQ01_RESP_LEGAL_DNTAL FROM SYSIBM.SYSDUMMY1", new MapSqlParameterSource());
		//TODO postgres
		//return getJdbcTemplate().queryForLong("SELECT NEXT VALUE FOR SELECT COALESCE(MAX(cresp_legal_dntal),0)+1 FROM DBPROD.RESP_LEGAL_DNTAL", new MapSqlParameterSource());
		
	}

}