package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.Origem;

/**
 * Servicos para obter o canal de venda
 * 
 * @author WDEV
 */
public interface CanalVendaServiceFacade {

	/**
	 * Obt�m o canal de venda do call center
	 * 
	 * @return Canal de venda do Call Center
	 */
	CanalVendaVO obterCanalVendaCallCenter();

	/**
	 * Obt�m o canal de venda a partir do c�digo
	 * 
	 * @param codigo C�digo do canal de venda
	 * @return Canal de Venda
	 */
	CanalVendaVO obterPorCodigo(Integer codigo);

	/**
	 * Define o canal de venda a partir da origem enviada
	 * @param origem Origem da venda
	 * @return Canal de venda
	 */
	CanalVendaVO definirCanalVenda(Origem origem);
	
	public List<CanalVendaVO> listarCanaisVenda();

}