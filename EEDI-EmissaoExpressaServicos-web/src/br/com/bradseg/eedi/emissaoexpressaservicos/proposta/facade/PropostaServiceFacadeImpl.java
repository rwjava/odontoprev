package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.eedi.emissaoexpressaservicos.corretorsucursal.facade.CorretorSucursalServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.endereco.dao.EnderecoDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.endereco.facade.EnderecoServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.odontoprev.facade.OdontoprevServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.AgenciaBancariaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.ArquivoPropostaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.BeneficiarioDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.MovimentoBeneficiarioDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.MovimentoPropostaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.ProdutorDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.ProponenteDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.PropostaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.ResponsavelLegalDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.TelefoneDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.DefinicaoProposta.DefinicaoPropostaFactory;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.Validador;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.DependenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FormaPagamento;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.GrauParentesco;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoBeneficiarioVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProdutorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaCorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaExpiradaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ResponsavelLegalVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SituacaoProposta;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.StatusPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.SucursalSeguradoraVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TelefoneVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TitularVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UnidadeFederativaVO;
import br.com.bradseg.shsg.servicos.webservice.AtualizaEtapaCompraProdutoVO;
import br.com.bradseg.shsg.servicos.webservice.CancelarContratacaoRequestVO;
import br.com.bradseg.shsg.servicos.webservice.EfetuarContratacaoRequestVO;
import br.com.bradseg.shsg.servicos.webservice.EtapaCompraWebService;
import br.com.bradseg.shsg.servicos.webservice.ResultadoVO;

/**
 * Servi�os respons�veis para consulta e cadastro deproposta
 * 
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class PropostaServiceFacadeImpl implements PropostaServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(PropostaServiceFacadeImpl.class);
	
	// O c�digo do titular � sempre 1, pois � o primeiro benefici�rio da proposta 
	private static final Integer CODIGO_BENEFICIARIO_TITULAR = 1;
	// O c�digo do dependente inicia em 2, pois vem a partir do benefici�rio titular da proposta
	private static final Integer CODIGO_INICIAL_BENEFICIARIO_DEPENDENTE = 2;
	
	private static final Integer CODIGO_CANAL_VENDA_APP_CORRETOR = 11;
	
	private static final Integer CODIGO_PLANO_DENTAL_EXCLUSIVE_PLUS = 6;
	
	private static final Integer CODIGO_PLANO_DENTAL_PRIME_PLUS = 8;

	@Autowired
	protected PropostaDAO propostaDAO;

	@Autowired
	private ProdutorDAO produtorDAO;

	@Autowired
	private TelefoneDAO telefoneDAO;

	@Autowired
	private BeneficiarioDAO beneficiarioDAO;

	@Autowired
	private ResponsavelLegalDAO responsavelLegalDAO;

	@Autowired
	private MovimentoBeneficiarioDAO movimentoBeneficiarioDAO;

	@Autowired
	private ProponenteDAO proponenteDAO;

	@Autowired
	private MovimentoPropostaDAO movimentoPropostaDAO;

	@Autowired
	private EnderecoDAO enderecoDAO;

	@Autowired
	private CorretorSucursalServiceFacade corretorSucursalServiceFacade;

	@Autowired
	private CadastroPropostaValidator cadastroPropostaValidator;

	@Autowired
	protected CadastroPropostaVaziaValidator cadastroPropostaVaziaValidator;

	@Autowired
	private DefinicaoPropostaFactory definicaoPropostaFactory;

	@Autowired
	private NormalizadorProposta normalizadorProposta;

	@Autowired
	protected ArquivoPropostaDAO arquivoPropostaDAO;

	@Autowired
	protected AssociarBeneficiariosNasPropostas associarBeneficiariosNasPropostas;

	@Autowired
	protected AssociarProdutoresNasPropostas associarProdutoresNasPropostas;

	@Autowired
	protected AssociarTelefonesProponenteNasPropostas associarTelefonesProponenteNasPropostas;

	@Autowired
	private AgenciaBancariaDAO agenciaBancariaDAO;

	@Autowired
	private PlanoServiceFacade planoServiceFacade;

	@Autowired
	private CanalVendaServiceFacade canalVendaServiceFacade;
	
	@Autowired
	private EnderecoServiceFacade enderecoServiceFacade;
	
	@Autowired
	private EtapaCompraWebService etapaCompraWebService;
	
	@Autowired
	private OdontoprevServiceFacade odontoprevServiceFacade;
	
	@Autowired
	private transient br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.facade.TipoComissaoServiceFacade tipoComissaoServiceFacade;
	

	public List<PropostaVO> listarPorFiltro(FiltroPropostaVO filtroProposta, LoginVO login) {

		filtroProposta.setTipoProdutor(Constantes.FILTRO_TIPO_PRODUTOR_LISTAGEM_PROPOSTAS);
		new ListarPropostasValidator().validate(filtroProposta);

		List<PropostaVO> propostas = propostaDAO.listarPorFiltro(filtroProposta, login);
		if (propostas.isEmpty()) {
			throw new BusinessException(new Message("msg.erro.nenhuma.proposta.encontrada.para.filtro.informado", Message.ERROR_TYPE));
		}
		for(PropostaVO proposta : propostas){
			proposta.setMovimento(obterSituacaoDeProposta(proposta.getNumeroSequencial()));
		}
		
		return normalizadorProposta.normalizar(propostas);
		
	}
	
	public List<PropostaVO> listarPropostasIntranetFiltro(FiltroPropostaVO filtroProposta, LoginVO login) {
		filtroProposta.setTipoProdutor(Constantes.FILTRO_TIPO_PRODUTOR_LISTAGEM_PROPOSTAS);
		new ListarPropostasValidator().validate(filtroProposta);

		List<PropostaVO> propostas = propostaDAO.listarPropostasIntranetFiltro(filtroProposta, login);
		if (propostas.isEmpty()) {
			throw new BusinessException(new Message("msg.erro.nenhuma.proposta.encontrada.para.filtro.informado", Message.ERROR_TYPE));
		}
		for(PropostaVO proposta : propostas){
			proposta.setMovimento(obterSituacaoDeProposta(proposta.getNumeroSequencial()));
		}

		return normalizadorProposta.normalizar(propostas);
	}

	public PropostaVO consultarPorCodigo(String codigoProposta) {
		PropostaVO proposta = propostaDAO.consultarPorCodigo(codigoProposta);
		if (proposta == null) {
			throw new BusinessException(new Message("msg.erro.proposta.nao.encontrada", Message.ERROR_TYPE));
		}

		proposta.setProdutores(produtorDAO.listarPorProposta(proposta));
		
		proposta.setMovimento(obterSituacaoDeProposta(proposta.getNumeroSequencial()));
		proposta.setBeneficiarios(beneficiarioDAO.listarPorProposta(proposta));

		if (proposta.getTitular() != null) {
			proposta.getProponente().setTelefones(telefoneDAO.listarPorProponente(proposta.getProponente()));
			if (proposta.getTitular().isMenorDeIdade()) {
				proposta.setResponsavelLegal(responsavelLegalDAO.listarPorProposta(proposta));
			}
		}

		if (proposta.isPagamentoDebitoAutomatico()) {
			detalharAgenciaBancaria(proposta);
		}
		
//		Date dataCriacaoProposta = movimentoPropostaDAO.obterDataPrimeiroMovimentoProposta(proposta.getNumeroSequencial());
//		if(dataCriacaoProposta != null){
//			
//			try{
//				
//				SimpleDateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy");
//				String dataMovimento = dataFormatada.format(dataCriacaoProposta);
//				br.com.bradseg.eedi.administrativo.plano.webservice.PlanoVO plano = 
//						planoWebService.buscarPlanoPorCodigoCanalEDataMovimento(Long.valueOf(proposta.getCanalVenda().getCodigo()), dataMovimento);
//				
//				PlanoVO planoVO = new PlanoVO();
//				planoVO.setCodigo(plano.getCodigo());
//				planoVO.setCodigoRegistro(plano.getCodigoRegistro());
//				planoVO.setNome(plano.getNome());
//				ValorPlanoVO valorPlanoVO = new ValorPlanoVO();
//				valorPlanoVO.setValorAnualDependente(plano.getValorPlanoVO().getValorAnualDependente());
//				valorPlanoVO.setValorAnualTitular(plano.getValorPlanoVO().getValorAnualTitular());
//				valorPlanoVO.setValorMensalDependente(plano.getValorPlanoVO().getValorMensalDependente());
//				valorPlanoVO.setValorMensalTitular(plano.getValorPlanoVO().getValorMensalTitular());
//				valorPlanoVO.setValorDesconto(plano.getValorPlanoVO().getValorDesconto());
//				valorPlanoVO.setValorTaxa(plano.getValorPlanoVO().getValorTaxa());
//				planoVO.setValorPlanoVO(valorPlanoVO);
//		
//				proposta.setPlanoVO(planoVO);
//				LOGGER.error("PLANO OBTIDO: "+planoVO);
//			}catch(Exception e){
//				LOGGER.error("INFO: ERRO AO OBTER PLANO");
//			}
//					
//			
//		}

		return normalizadorProposta.normalizar(proposta);
	}
	
	public MovimentoPropostaVO obterSituacaoDeProposta(Long sequencialProposta) {
		MovimentoPropostaVO movimentoProposta = null;
		List<MovimentoPropostaVO> listaDeMovimentos = movimentoPropostaDAO.obterListaDeMovimentosDeProposta(sequencialProposta);
		for(MovimentoPropostaVO movimento : listaDeMovimentos){
			if(SituacaoProposta.CANCELADA.equals(movimento.getSituacao())){
				movimentoProposta = movimento;
				break;
			}
			else if(SituacaoProposta.IMPLANTADA.equals(movimento.getSituacao())){
				movimentoProposta = movimento;
				break;
			}	
		}
		if(movimentoProposta == null){
			movimentoProposta = movimentoPropostaDAO.consultarUltimoMovimento(sequencialProposta);
		}
		return movimentoProposta;
	}

	/**
	 * Detalha a ag�ncia banc�ria a partir do seu c�digo
	 * 
	 * @param proposta proposta
	 */
	private void detalharAgenciaBancaria(PropostaVO proposta) {
		if (proposta.getProponente() != null && proposta.getProponente().getContaCorrente() != null && proposta.getProponente().getContaCorrente().getAgenciaBancaria() != null) {
			Integer codigoAgencia = proposta.getProponente().getContaCorrente().getAgenciaBancaria().getCodigo();
			proposta.getProponente().getContaCorrente().setAgenciaBancaria(agenciaBancariaDAO.consultarPorId(codigoAgencia));
		}
	}

	private PropostaVO atualizarRascunho(PropostaVO proposta) {
		return atualizar(proposta, SituacaoProposta.RASCUNHO);
	}

	private PropostaVO atualizarPendente(PropostaVO proposta) {
		return atualizar(proposta, SituacaoProposta.PENDENTE);
	}

	/**
	 * Atualiza a proposta, realizando os seguintes passos:
	 * <ul>
	 * <li>Obtem e preenche as informa��es dos produtores</li>
	 * <li>Se o proponente possuir c�digo, atualiza o proponente, caso contr�rio salva</li>
	 * <li></li>
	 * </ul>
	 * 
	 * @param proposta proposta
	 * @param situacao situacao da proposta
	 * @return
	 */
	private PropostaVO atualizar(PropostaVO proposta, SituacaoProposta situacao) {
		preencherProdutores(proposta);
		cadastroPropostaValidator.validate(proposta);
		if(proposta.isCodigoPreenchido() && proposta.getNumeroSequencial() == null){
			proposta.setNumeroSequencial(propostaDAO.obterSequencialPorCodigoProposta(proposta.getCodigo()));
		}
		
		DefinicaoProposta definicaoProposta = definicaoPropostaFactory.definicaoProposta(proposta);

		ProponenteVO proponente = proposta.getProponente();
		if (proponente.isCodigoPreenchido()) {
			proponenteDAO.atualizar(proponente);
		} else {
			proponenteDAO.salvar(proponente);
		}

		if (definicaoProposta.isTitularMenorIdade()) {
			salvarResponsavelLegalTitular(proposta, proposta.getResponsavelLegal());
		}

		atualizarProposta(proposta, definicaoProposta);
		atualizarMovimentoProposta(definicaoProposta.getSequencial(), situacao);
		atualizarTelefones(proposta);
		atualizarBeneficiarios(proposta, definicaoProposta, situacao);
		atualizarProdutores(proposta, definicaoProposta, definicaoProposta.getSucursalSeguradora());

		return consultarPorCodigo(proposta.getCodigo());
	}

	private void atualizarMovimentoProposta(Long sequencialProposta, SituacaoProposta situacao) {
		if (!SituacaoProposta.RASCUNHO.equals(situacao)) {
			// So cria um novo movimento caso a situa��o n�o seja rascunho
			salvarMovimentoProposta(sequencialProposta, situacao);
		}
	}

	private void atualizarProdutores(PropostaVO proposta, DefinicaoProposta definicaoProposta, SucursalSeguradoraVO sucursalSeguradora) {
		preencherProdutores(proposta);
		produtorDAO.removerPorProposta(definicaoProposta);
		salvarProdutores(sucursalSeguradora, proposta, definicaoProposta);
	}

	private void atualizarBeneficiarios(PropostaVO proposta, DefinicaoProposta definicaoProposta, SituacaoProposta situacao) {
		movimentoBeneficiarioDAO.removerPorProposta(proposta);
		beneficiarioDAO.remover(proposta);
		enderecoDAO.remover(proposta.getTitular().getEndereco());

		salvarBeneficiarios(proposta, definicaoProposta, situacao);
	}

	private void atualizarTelefones(PropostaVO proposta) {
		telefoneDAO.removerPorProponente(proposta.getProponente());
		salvarTelefones(proposta.getProponente().getTelefones(), proposta.getProponente());
	}

	private void atualizarProposta(PropostaVO proposta, DefinicaoProposta definicaoProposta) {
		propostaDAO.atualizar(proposta);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public PropostaVO salvarRascunho(PropostaVO proposta) {
		 if(proposta.getDiaVencimento() != null){
             if(proposta.getDiaVencimento() < 1 || proposta.getDiaVencimento() > 31){
                     throw new BusinessException(new Message("msg.erro.proposta.dia.vencimento.invalido", Message.ERROR_TYPE));
             }
		 }
		 
		 PlanoVO planoVO = null;
		 try {
			if(proposta.getPlanoVO() == null || proposta.getPlanoVO().getCodigo() == null || proposta.getPlanoVO().getCodigo().longValue() == 0){
				planoVO = planoServiceFacade.obterPlanoAtualPorCodigoCanal(proposta.getCanalVenda().getCodigo());
			} else{
				planoVO = planoServiceFacade.obterPlanoPorCodigo(proposta.getPlanoVO().getCodigo());
			}
		    
		} catch (Exception e){
			LOGGER.error("INFO: ERRO AO OBTER PLANO."+e.getMessage());
		}		 
			
		if(planoVO == null){ 	
			throw new BusinessException(new Message("msg.erro.proposta.plano", Message.ERROR_TYPE));
		}
		
	/*	if(proposta.getCanalVenda() != null){
			if(proposta.getCanalVenda().getCodigo() == 1 || proposta.getCanalVenda().getCodigo() == 8){
				throw new BusinessException(new Message("msg.erro.proposta.parentesco.obrigatorio", Message.ERROR_TYPE));
			}
		}*/
		proposta.setPlanoVO(planoVO);
				
		if (proposta.isCodigoPreenchido()) {
			return atualizarRascunho(proposta);
		}
		
		return salvar(proposta, SituacaoProposta.RASCUNHO);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public PropostaVO salvarPendente(PropostaVO proposta) {
		
		 if(proposta.getDiaVencimento() != null){
             if(proposta.getDiaVencimento() < 1 || proposta.getDiaVencimento() > 31){
            	 LOGGER.error("INFO: ERRO NO VALOR DO DIA DE VENCIMENTO. DIA RECEBIDO: "+proposta.getDiaVencimento());
                     throw new BusinessException(new Message("msg.erro.proposta.dia.vencimento.invalido", Message.ERROR_TYPE));
             }
		 }
		
		if(proposta.getProtocolo() != null && proposta.getProtocolo().length() > 30){
			 throw new BusinessException("Protocolo com mais de 30 caracteres.");
		}
		
		 PlanoVO planoVO = null;
		 try {
			if(proposta.getPlanoVO() == null || proposta.getPlanoVO().getCodigo() == null || proposta.getPlanoVO().getCodigo().longValue() == 0){
				planoVO = planoServiceFacade.obterPlanoAtualPorCodigoCanal(proposta.getCanalVenda().getCodigo());
			} else{
				planoVO = planoServiceFacade.obterPlanoPorCodigo(proposta.getPlanoVO().getCodigo());
			}
		    
		} catch (Exception e){
			LOGGER.error("INFO: ERRO AO OBTER PLANO."+e.getMessage());
		}

			
		if(planoVO == null){ 	
			throw new BusinessException(new Message("msg.erro.proposta.plano", Message.ERROR_TYPE));
		}
		proposta.setPlanoVO(planoVO);
		 
		if (proposta.isCodigoPreenchido()) {
			return atualizarPendente(proposta);
		}
		
	    //TODO setar aqui o plano da proposta pegando o plano do service
		//TODO caso traga vazio retorna excess�o 
		//TODO caso retorne correto savar proposta
		
		//validar dependentes dos novos planos para testes.
		if(planoVO.getCodigo() == 15 || planoVO.getCodigo() == 16 || planoVO.getCodigo() == 17 || planoVO.getCodigo() == 18 || planoVO.getCodigo() == 19){
		    if(Validador.validarDataMaximaDenteLeite(proposta.getTitular().getDataNascimento()) == false){
		        throw new BusinessException("Os planos Dente de Leite s� podem ser comercializados para benefici�rios com idade de 0 a 7 anos, 11 meses e 30 dias.");
		    };
		    
		    if(proposta.getDependentes() != null){
		        throw new BusinessException("Dependentes n�o podem ser cadastrados para este c�digo.");
		    }
		}
		
		if(proposta.getCanalVenda() != null){
			if(proposta.getCanalVenda().getCodigo() == 1 || proposta.getCanalVenda().getCodigo() == 8){
				
				if(proposta.getTitular().getGrauParentescoTitulaProponente() == null){
					throw new BusinessException(new Message("msg.erro.proposta.parentesco.obrigatorio", Message.ERROR_TYPE));
				}
				
			}
			
			/**
			 * Melhoria 52
			 * Criar uma regra no EEDI-Servi�os para impossibilitar venda no Canal APP Corretor
			 */
			if(proposta.getCanalVenda().getCodigo().intValue() == CODIGO_CANAL_VENDA_APP_CORRETOR && proposta.getSucursalSeguradora() != null){
				if(proposta.getSucursalSeguradora().isBVP()){
					if(proposta.getPlanoVO().getCodigo().intValue() == CODIGO_PLANO_DENTAL_EXCLUSIVE_PLUS || proposta.getPlanoVO().getCodigo().intValue() == CODIGO_PLANO_DENTAL_PRIME_PLUS){				
						throw new BusinessException(new Message("msg.erro.proposta.plano.vigente", Message.ERROR_TYPE));
					}
				}
			}
		}

		if(planoVO.getCodigo() == 20 || planoVO.getCodigo() == 21 || planoVO.getCodigo() == 22 || planoVO.getCodigo() == 24 || planoVO.getCodigo() == 23){
		    if(Validador.validarDataMaximaJunior(proposta.getTitular().getDataNascimento()) == false){
		        throw new BusinessException("Os planos Dental Junior s� podem ser comercializados para benefici�rios com idade entre 8 at� 18 anos.");
		    };
		    
		    if(proposta.getDependentes() != null){
		        throw new BusinessException("Dependentes n�o podem ser cadastrados para este c�digo.");
		    }
		}
	
		return salvar(proposta, SituacaoProposta.PENDENTE);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public PropostaVO salvarProposta100CorretorPendente(PropostaCorretorVO proposta) {
		 if(proposta.getDiaVencimento() != null){
             if(proposta.getDiaVencimento() < 1 || proposta.getDiaVencimento() > 31){
            	 LOGGER.error("INFO: ERRO NO VALOR DO DIA DE VENCIMENTO. DIA RECEBIDO: "+proposta.getDiaVencimento());
                     throw new BusinessException(new Message("msg.erro.proposta.dia.vencimento.invalido", Message.ERROR_TYPE));
             }
		 }
		
		if(proposta.getProtocolo() != null && proposta.getProtocolo().length() > 30){
			 throw new BusinessException("Protocolo com mais de 30 caracteres.");
		} 
		 
		if (proposta.isCodigoPreenchido()) {
			return atualizarPendente(proposta);
		}
		
	    //TODO setar aqui o plano da proposta pegando o plano do service
		//TODO caso traga vazio retorna excess�o 
		//TODO caso retorne correto savar proposta
		
		 PlanoVO planoVO = null;
		 try {
			if(proposta.getPlanoVO() == null || proposta.getPlanoVO().getCodigo() == null || proposta.getPlanoVO().getCodigo().longValue() == 0){
				planoVO = planoServiceFacade.obterPlanoAtualPorCodigoCanal(proposta.getCanalVenda().getCodigo());
			} else{
				planoVO = planoServiceFacade.obterPlanoPorCodigo(proposta.getPlanoVO().getCodigo());
			}
		    
		} catch (Exception e){
			LOGGER.error("INFO: ERRO AO OBTER PLANO."+e.getMessage());
		}

		/*if(proposta.getCanalVenda() != null){
			if(proposta.getCanalVenda().getCodigo() == 1 || proposta.getCanalVenda().getCodigo() == 8){
				throw new BusinessException(new Message("msg.erro.proposta.parentesco.obrigatorio", Message.ERROR_TYPE));
			}
		}*/
			
		if(planoVO == null){ 	
			throw new BusinessException(new Message("msg.erro.proposta.plano", Message.ERROR_TYPE));
		}
		proposta.setPlanoVO(planoVO);
		
		/**
		 * Melhoria 52
		 * Criar uma regra no EEDI-Servi�os para impossibilitar venda no Canal APP Corretor, seguimento REDE\BVP
		 */
		if(proposta.getCanalVenda()!= null){
			if(proposta.getCanalVenda().getCodigo().intValue() == CODIGO_CANAL_VENDA_APP_CORRETOR && proposta.getSucursalSeguradora() != null){
				if(proposta.getSucursalSeguradora().isBVP()){
					if(proposta.getPlanoVO().getCodigo().intValue() == CODIGO_PLANO_DENTAL_EXCLUSIVE_PLUS || proposta.getPlanoVO().getCodigo().intValue() == CODIGO_PLANO_DENTAL_PRIME_PLUS){				
						throw new BusinessException(new Message("msg.erro.proposta.plano.vigente", Message.ERROR_TYPE));
					}
				}
			}
		}
		
		salvar(proposta, SituacaoProposta.PENDENTE);
		
		if(proposta.getCanalVenda().getCodigo() == null || proposta.getCanalVenda().getCodigo().equals(1)){
			LOGGER.error("Inicio Processamento cartao credito");
			processamentoCartaoCredito(proposta);
			LOGGER.error("Fim Processamento cartao credito");
		}
		return consultarPorSequencial(proposta.getNumeroSequencial());
	}
	
	
	
	
	private void processamentoCartaoCredito(PropostaCorretorVO proposta) {

		proposta.getCartaoCreditoVO().setMensagemRetorno("");

		try {
			if (FormaPagamento.CARTAO_CREDITO.equals(proposta.getFormaPagamento())) {
				LOGGER.error("PAGAMENTO CARTAO CREDITO");
				proposta.getCartaoCreditoVO().setCpfCartao(proposta.getTitular().getCpf());
				odontoprevServiceFacade.debitarPrimeiraParcelaCartaoDeCredito(proposta);
			} else if (FormaPagamento.BOLETO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento()) || FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento())) {
				if (FormaPagamento.BOLETO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento())) {
					LOGGER.error("PAGAMENTO BOLETO E CARTAO CREDITO");
					proposta.getCartaoCreditoVO().setCpfCartao(proposta.getTitular().getCpf());
				} else if (FormaPagamento.DEBITO_AUTOMATICO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento())) {
					LOGGER.error("PAGAMENTO DEBITO AUTOMATICO E CARTAO CREDITO");
					proposta.getCartaoCreditoVO().setCpfCartao(proposta.getProponente().getCpf());
				}
				odontoprevServiceFacade.validarDadosCartaoCredito(proposta);
			}
		} catch (BusinessException be) {
//			throw new  proposta.getCartaoCreditoVO().setMensagemRetorno(be.getMessage());
			LOGGER.error("ERRO NA VALIDACAO CARTAO CREDITO: " + be.getMessage());
			throw new BusinessException(be.getMessage());
//			proposta.getCartaoCreditoVO().setAccessToken(odontoprevServiceFacade.obterAccessToken());

		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public PropostaVO salvarPropostaVazia(PropostaVO proposta) {		
		preencherProdutores(proposta);
		cadastroPropostaVaziaValidator.validatePropostaVazia(proposta);
		proposta.setPlanoVO(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlanoVO().getCodigo()));
		DefinicaoProposta definicaoProposta = definicaoPropostaFactory.definicaoProposta(proposta);
		salvarProposta(proposta, definicaoProposta);
		salvarProdutores(definicaoProposta.getSucursalSeguradora(), proposta, definicaoProposta);
		salvarMovimentoProposta(definicaoProposta.getSequencial(), SituacaoProposta.RASCUNHO);

		return consultarPorCodigo(definicaoProposta.getCodigo());
	}

	private PropostaVO salvar(PropostaVO proposta, SituacaoProposta situacao) {
		preencherProdutores(proposta);
		
		LOGGER.error("INFO: IN�CIO DE VALIDA��O DE PROPOSTA");
		
		cadastroPropostaValidator.validate(proposta);
		
		LOGGER.error("INFO: FIM DE VALIDA��O DE PROPOSTA");
		
		//TODO setar o canal de venda aqui?? Ele j� vem preenchido
		//s� buscar assim se estivar vazio
		if(proposta.getCanalVenda() == null){
			proposta.setCanalVenda(canalVendaServiceFacade.definirCanalVenda(proposta.getOrigem()));
		}
		
		EnderecoVO enderecoVO = enderecoServiceFacade.obterEnderecoPorCep(proposta.getTitular().getEndereco().getCep());
		
		if(enderecoVO.getBairro() != null && StringUtils.isNotBlank(enderecoVO.getBairro())){
			proposta.getTitular().getEndereco().setBairro(enderecoVO.getBairro());
		}
		if(enderecoVO.getCep() != null && StringUtils.isNotBlank(enderecoVO.getCep())){
			proposta.getTitular().getEndereco().setCep(enderecoVO.getCep());
		}
		if(enderecoVO.getCidade() != null && StringUtils.isNotBlank(enderecoVO.getCidade())){
			proposta.getTitular().getEndereco().setCidade(enderecoVO.getCidade());
		}
		if(enderecoVO.getComplemento() != null && StringUtils.isNotBlank(enderecoVO.getComplemento())){
			proposta.getTitular().getEndereco().setComplemento(enderecoVO.getComplemento());
		}
		if(enderecoVO.getLogradouro() != null && StringUtils.isNotBlank(enderecoVO.getLogradouro())){
			proposta.getTitular().getEndereco().setLogradouro(enderecoVO.getLogradouro());
		}
		if(enderecoVO.getUnidadeFederativa().getSigla() != null && StringUtils.isNotBlank(enderecoVO.getUnidadeFederativa().getSigla())){
			proposta.getTitular().getEndereco().setUnidadeFederativa(new UnidadeFederativaVO());
			proposta.getTitular().getEndereco().getUnidadeFederativa().setSigla(enderecoVO.getUnidadeFederativa().getSigla());
		}
		if(enderecoVO.getCodigoUfIbge() != null && StringUtils.isNotBlank(enderecoVO.getCodigoUfIbge())){
			proposta.getTitular().getEndereco().setCodigoUfIbge(enderecoVO.getCodigoUfIbge());
		}
		if(enderecoVO.getSiglaIbgeMunicipio() != null && StringUtils.isNotBlank(enderecoVO.getSiglaIbgeMunicipio())){
			proposta.getTitular().getEndereco().setSiglaIbgeMunicipio(enderecoVO.getSiglaIbgeMunicipio());
		}
			
		validaEnderecoServico(proposta);

		if(Constantes.CODIGO_CANAL_VENDA_INTERNET_BANKING.equals(proposta.getCanalVenda().getCodigo())){
			proposta.setFormaPagamento(FormaPagamento.DEBITO_AUTOMATICO);
		}
		
		if(proposta.getCanalVenda() != null &&  Constantes.CODIGO_CANAL_VENDA_INTERNET_BANKING.equals(proposta.getCanalVenda().getCodigo()) 
				&& !FormaPagamento.BOLETO_BANCARIO.equals(proposta.getFormaPagamento())){
			proposta.getAgenciaProdutora().setCodigo(proposta.getProponente().getContaCorrente().getAgenciaBancaria().getCodigo());
		}

//		if (proposta.getPlano() == null || proposta.getPlano().getCodigo() == null) {
//			// Caso nao seja informado nenhum plano, consulta os planos vigentes e preenche na proposta
//			preencherPlanoVigente(proposta);
//		}

		DefinicaoProposta definicaoProposta = definicaoPropostaFactory.definicaoProposta(proposta);
		LOGGER.error("INFO: SALVAR PROPONENTE : "+proposta.getProponente());
		ProponenteVO proponente = salvarProponente(proposta.getProponente());
		LOGGER.error("INFO: PROPONENTE SALVO");
		proposta.setProponente(proponente);

		if (definicaoProposta.isTitularMenorIdade()) {
			LOGGER.error("INFO: SALVAR RESPONS�VEL LEGAL :"+proposta.getResponsavelLegal());
			salvarResponsavelLegalTitular(proposta, proposta.getResponsavelLegal());
			LOGGER.error("INFO: RESPONS�VEL LEGAL SALVO");
		}

		salvarProposta(proposta, definicaoProposta);
		salvarBeneficiarios(proposta, definicaoProposta, situacao);
		salvarProdutores(definicaoProposta.getSucursalSeguradora(), proposta, definicaoProposta);

		if (proposta.getMovimento() != null && proposta.getMovimento().getDataInicio() != null) {
			salvarMovimentoProposta(definicaoProposta.getSequencial(), proposta.getMovimento().getDataInicio(), situacao);
		} else {
			salvarMovimentoProposta(definicaoProposta.getSequencial(), situacao);
		}
		salvarTelefones(proposta.getProponente().getTelefones(), proposta.getProponente());

		return consultarPorCodigo(definicaoProposta.getCodigo());
	}

	/**
	 * 
	 * @param proposta
	 * @param enderecoVO
	 */
	private void validaEnderecoServico(PropostaVO proposta) {
		if(proposta.getTitular().getEndereco().getBairro() != null){
			if (proposta.getTitular().getEndereco().getBairro().length() >= 60 ) {
				proposta.getTitular().getEndereco().setBairro(proposta.getTitular().getEndereco().getBairro().substring(0, 60));
			}
		}
		
		if(proposta.getTitular().getEndereco().getCidade()!= null){
			if (proposta.getTitular().getEndereco().getCidade().length() >= 72) {
				proposta.getTitular().getEndereco().setCidade(proposta.getTitular().getEndereco().getCidade().substring(0, 72));
			}
		}
		
		if(proposta.getTitular().getEndereco().getLogradouro()!= null){
			if (proposta.getTitular().getEndereco().getLogradouro().length() >= 50) {
				proposta.getTitular().getEndereco().setLogradouro(proposta.getTitular().getEndereco().getLogradouro().substring(0, 80));
			}
		}
		
		if(proposta.getTitular().getEndereco().getComplemento()!= null){
			if (proposta.getTitular().getEndereco().getComplemento().length() >= 25) {
				proposta.getTitular().getEndereco().setComplemento(proposta.getTitular().getEndereco().getComplemento().substring(0, 50));
			}
		}
	}

//	private void preencherPlanoVigente(PropostaVO proposta) {
//		PlanoVO plano = planoServiceFacade.listarPlanosVigentes().get(0);
//		proposta.getTitular().setPlano(plano);
//		if (proposta.getDependentes() != null) {
//			for (DependenteVO dependente : proposta.getDependentes()) {
//				dependente.setPlano(plano);
//			}
//		}
//		
//		if(proposta.getCanalVenda().getCodigo().equals(Constantes.CODIGO_CANAL_VENDA_FAMILIA_BRADESCO)){
//			proposta.getTitular().setPlano(planoServiceFacade.buscarPlanoPorCodigo(Constantes.CODIGO_PLANO_QUATRO));
//		}
//	}

	private void preencherProdutores(PropostaVO proposta) {
		SucursalSeguradoraVO sucursalSeguradora = proposta.getSucursalSeguradora();
		obterInformacoesProdutor(proposta.getCorretor(), sucursalSeguradora);
		obterInformacoesProdutor(proposta.getCorretorMaster(), sucursalSeguradora);
		obterInformacoesProdutor(proposta.getAngariador(), sucursalSeguradora);
	}

	private void obterInformacoesProdutor(ProdutorVO produtor, SucursalSeguradoraVO sucursalSeguradora) {
		try{
			if (produtor != null && produtor.isCpdPreenchido()) {
				LOGGER.error("INFO: CONSULTAR CORRETOR. SUCURSAL: " + sucursalSeguradora.getCodigo() + "; CPD: " + produtor.getCpd());
				CorretorVO corretor = corretorSucursalServiceFacade.consultarCorretor(sucursalSeguradora.getCodigo(), produtor.getCpd());
				LOGGER.error("INFO: CONSULTA REALIZADA");
				if (corretor != null) {
					validarCPFCNPJRetornadoPeloCCRR(corretor.getCpfCnpj());
					if(produtor.getTipo().getCodigo() == 1){
						produtor.setCpfCnpj(corretor.getCpfCnpj());
					}
					produtor.setNome(corretor.getNome());
					produtor.setCpfCnpj(corretor.getCpfCnpj());
				}
			}
		}catch(IntegrationException ie){
			LOGGER.error(ie.getMessage());
			throw new IntegrationException("O Servi�o que obt�m os dados do Corretor encontra-se indispon�vel. Favor tente novamente mais tarde.");
		}
	}
	
/*	private void obterInfoProdutor(ProdutorVO produtor, SucursalSeguradoraVO sucursalSeguradora) {
		try{
			if (produtor != null && produtor.isCpdPreenchido()) {
				LOGGER.error("INFO: CONSULTAR CORRETOR. SUCURSAL: " + sucursalSeguradora.getCodigo() + "; CPD: " + produtor.getCpd());
				CorretorVO corretor = corretorSucursalServiceFacade.consultarCorretor(sucursalSeguradora.getCodigo(), produtor.getCpd());
				LOGGER.error("INFO: CONSULTA REALIZADA");
				if (corretor != null) {
					validarCPFCNPJRetornadoPeloCCRR(corretor.getCpfCnpj());
					
					produtor.setCpfCnpj(produtor.getCpfCnpj());
					produtor.setNome(corretor.getNome());
				}
			}
		}catch(IntegrationException ie){
			LOGGER.error(ie.getMessage());
			throw new IntegrationException("O Servi�o que obt�m os dados do Corretor encontra-se indispon�vel. Favor tente novamente mais tarde.");
		}
	}*/
	private void validarCPFCNPJRetornadoPeloCCRR(String cpfCnpj) {
		// TODO Auto-generated method stub
		boolean cpfCnpjValido = true;
		
		if(cpfCnpj.length() == 11){
		if(("00000000000").equals(cpfCnpj) 
				|| ("11111111111").equals(cpfCnpj)
				|| ("22222222222").equals(cpfCnpj)
				|| ("33333333333").equals(cpfCnpj) 
				|| ("44444444444").equals(cpfCnpj) 
				|| ("55555555555").equals(cpfCnpj)
				|| ("66666666666").equals(cpfCnpj) 
				|| ("77777777777").equals(cpfCnpj) 
				|| ("88888888888").equals(cpfCnpj) 
				|| ("99999999999").equals(cpfCnpj) 
				|| ("404040404040404").equals(cpfCnpj)){
			
						cpfCnpjValido = false;
		}
		
		}else{
			
			String CNPJ = StringUtils.leftPad(cpfCnpj, 14, '0');
			if (("00000000000000").equals(CNPJ) 
					|| ("11111111111111").equals(CNPJ) 
					|| ("22222222222222").equals(CNPJ) 
					|| ("33333333333333").equals(CNPJ) 
					|| ("44444444444444").equals(CNPJ) 
					|| ("55555555555555").equals(CNPJ)
					|| ("66666666666666").equals(CNPJ)
					|| ("77777777777777").equals(CNPJ)
					|| ("88888888888888").equals(CNPJ)
					|| ("99999999999999").equals(CNPJ) 
					|| ("404040404040404").equals(CNPJ)
					) {
			
						cpfCnpjValido = false;
			}
			
		}
		
		if(cpfCnpjValido == false){
			LOGGER.error(String.format("CPF/CNPJ retornado pelo CCRR invalido: [%s]", cpfCnpj));
			throw new IntegrationException("O servi�o que obt�m as informa��es do corretor encontra-se indispon�vel.");
		}
		
	}

	private void salvarProposta(PropostaVO proposta, DefinicaoProposta definicaoProposta) {
		proposta.setBanco(definicaoProposta.getBanco());
		
		if(proposta.getCanalVenda().getCodigo() == 11){
			proposta.setTipoCobrancaCanalNovo(tipoComissaoServiceFacade.obterTipoComissao(proposta));
		}
		proposta.setTipoCobranca(definicaoProposta.getTipoCobranca());

		propostaDAO.salvar(proposta, definicaoProposta);
	}

	private void salvarTelefones(List<TelefoneVO> telefones, ProponenteVO proponente) {
		for (TelefoneVO telefone : telefones) {
			telefone.setProponente(proponente);
			telefoneDAO.salvar(telefone);
		}
	}

	private void salvarMovimentoProposta(Long sequencialProposta, DateTime dataInicioVigencia, SituacaoProposta situacao) {
		MovimentoPropostaVO movimentoProposta = new MovimentoPropostaVO();
		movimentoProposta.setDataInicio(dataInicioVigencia);
		movimentoProposta.setProposta(new PropostaVO());
		movimentoProposta.getProposta().setNumeroSequencial(sequencialProposta);
		movimentoProposta.setSituacao(situacao);
		movimentoPropostaDAO.salvar(movimentoProposta);
	}
	
	private void salvarMovimentoProposta(Long sequencialProposta, DateTime dataInicioVigencia, SituacaoProposta situacao, String responsavelAtualizacao) {
		MovimentoPropostaVO movimentoProposta = new MovimentoPropostaVO();
		movimentoProposta.setDataInicio(dataInicioVigencia);
		movimentoProposta.setProposta(new PropostaVO());
		movimentoProposta.getProposta().setNumeroSequencial(sequencialProposta);
		movimentoProposta.setSituacao(situacao);
		movimentoPropostaDAO.salvar(movimentoProposta, responsavelAtualizacao);
	}
	private void salvarMovimentoProposta(Long sequencialProposta, SituacaoProposta situacao) {
		   
			if(movimentoPropostaDAO.verificarSePossuiMovimentoDeCancelamento(sequencialProposta) == false){
		    	salvarMovimentoProposta(sequencialProposta, new DateTime(), situacao);
		    	LOGGER.error("INFO: PROPOSTA n�: "+sequencialProposta+" CANCELADA COM SUCESSO.");
		    }else{
		    	LOGGER.error("INFO: PROPOSTA JA ESTAVA CANCELADA: "+sequencialProposta);
		    	throw new BusinessException(new Message("msg.erro.cancelar.proposta.ja.cancelada",Message.ERROR_TYPE));
		    	
		    }
		
	}

	private void salvarMovimentoProposta(Long sequencialProposta, SituacaoProposta situacao,String responsavelAtualizacao) {
		
		if(movimentoPropostaDAO.verificarSePossuiMovimentoDeCancelamento(sequencialProposta) == false){
			salvarMovimentoProposta(sequencialProposta, new DateTime(), situacao, responsavelAtualizacao);
		}else{
	    	LOGGER.error("INFO: PROPOSTA JA ESTAVA CANCELADA: "+sequencialProposta);
	    	throw new BusinessException(new Message("msg.erro.cancelar.proposta.ja.cancelada", Message.ERROR_TYPE));
	    }
	}
	
	private ProponenteVO salvarProponente(ProponenteVO proponente) {
		return proponenteDAO.salvar(proponente);
	}

	private void salvarBeneficiarios(PropostaVO proposta, DefinicaoProposta definicaoProposta, SituacaoProposta situacaoProposta) {
		salvarTitularComMovimentos(proposta, definicaoProposta, situacaoProposta);
		salvarDependentesComMovimentos(proposta, definicaoProposta, situacaoProposta);
	}

	private void salvarTitularComMovimentos(PropostaVO proposta, DefinicaoProposta definicaoProposta, SituacaoProposta situacaoProposta) {
		TitularVO titular = prepararTitular(proposta, definicaoProposta);
		titular.setEndereco(salvarEndereco(titular.getEndereco()));
		GrauParentesco grauParentescoTitulaProponente = titular.getGrauParentescoTitulaProponente();
		
		if(grauParentescoTitulaProponente != null){
			beneficiarioDAO.salvarTitular(titular, grauParentescoTitulaProponente.getCodigo());
		}else{
			beneficiarioDAO.salvarTitularServicos(titular);
		}
		
		salvarMovimentoBeneficiario(titular, situacaoProposta);
	}

	private void salvarDependentesComMovimentos(PropostaVO proposta, DefinicaoProposta definicaoProposta, SituacaoProposta situacaoProposta) {
		List<DependenteVO> dependentes = proposta.getDependentes();
		if (dependentes != null) {
			prepararDependentes(proposta, definicaoProposta);
			beneficiarioDAO.salvarDependentes(dependentes,proposta.getTitular());
			for (DependenteVO dependenteVO : dependentes) {
				salvarMovimentoBeneficiario(dependenteVO, situacaoProposta);
			}
		}
	}

	private void prepararDependentes(PropostaVO proposta, DefinicaoProposta definicaoProposta) {
		Integer codigo = CODIGO_INICIAL_BENEFICIARIO_DEPENDENTE;
		for (DependenteVO dependente : proposta.getDependentes()) {
			// Codigo gerado para o dependente
			dependente.setCodigo(codigo++);
			// Relacao do dependente na proposta
			dependente.setProposta(new PropostaVO());
			dependente.getProposta().setNumeroSequencial(definicaoProposta.getSequencial());
			// Colocando o mesmo plano do titular para os dependentes
			//dependente.setPlano(proposta.getTitular().getPlano());
		}
	}

	private TitularVO prepararTitular(PropostaVO proposta, DefinicaoProposta definicaoProposta) {
		TitularVO titular = proposta.getTitular();
		titular.setCodigo(CODIGO_BENEFICIARIO_TITULAR);
		titular.setProposta(new PropostaVO());
		titular.getProposta().setNumeroSequencial(definicaoProposta.getSequencial());
		return titular;
	}

	private EnderecoVO salvarEndereco(EnderecoVO endereco) {
		return enderecoDAO.salvar(endereco);
	}

	private void salvarResponsavelLegalTitular(PropostaVO proposta, ResponsavelLegalVO responsavelLegal) {
		if (responsavelLegal.getCodigo() == null) {
			responsavelLegal.setCodigo(responsavelLegalDAO.obterCodigoSequencial());
		} else {
			// Remove o respons�vel legal da proposta
			propostaDAO.removerResponsavelLegal(proposta);

			// Remove os dados do respons�vel legal
			removerResponsavelLegalTitular(responsavelLegal);
		}

		// salvar respons�vel
		responsavelLegal = responsavelLegalDAO.salvar(responsavelLegal);

		// salvar endere�o
		EnderecoVO endereco = salvarEndereco(responsavelLegal.getEndereco());

		// salvar telefone
		salvarTelefoneResponsavelLegal(responsavelLegal.getTelefone(), responsavelLegal);

		// associar endere�o ao respons�vel
		responsavelLegalDAO.associarEndereco(responsavelLegal, endereco);
	}

	private void removerResponsavelLegalTitular(ResponsavelLegalVO responsavelLegal) {
		EnderecoVO endereco = responsavelLegal.getEndereco();

		// Desassocia o endere�o
		responsavelLegalDAO.desassociarEndereco(responsavelLegal, endereco);

		// Remove o telefone do respons�vel legal
		telefoneDAO.removerTelefoneResponsavelLegal(responsavelLegal);

		// Remove o endere�o do respons�vel legal
		enderecoDAO.remover(endereco);

		// Remove o respons�vel legal
		responsavelLegalDAO.remover(responsavelLegal);
	}

	private void salvarTelefoneResponsavelLegal(TelefoneVO telefone, ResponsavelLegalVO responsavelLegal) {
		telefone.setResponsavelLegal(responsavelLegal);
		telefoneDAO.salvarTelefoneResponsavelLegal(telefone);
	}

	private void salvarProdutores(SucursalSeguradoraVO sucursalSeguradora, PropostaVO proposta, DefinicaoProposta definicaoProposta) {
		salvarProdutor(sucursalSeguradora, proposta.getCorretor(), definicaoProposta);
		salvarProdutor(sucursalSeguradora, proposta.getAngariador(), definicaoProposta);
		/**
		 * valida��o caso o cnpj do corretor master n�o for preenchido
		 */
		if(proposta.getCorretorMaster() != null){
			if(proposta.getCorretorMaster().getCpfCnpj() == null){
				proposta.getCorretorMaster().setCpfCnpj(proposta.getCorretor().getCpfCnpj());
			}
		salvarProdutor(sucursalSeguradora, proposta.getCorretorMaster(), definicaoProposta);
		}

	}

	/**
	 * Metodo alterado para corrigir erro de caracter especial ao inserir no banco de dados
	 * Data: 13/06/2019
	 * autor: Roberto William e Leonardo
	 * @param sucursalSeguradora
	 * @param produtor
	 * @param definicaoProposta
	 */
	private void salvarProdutor(SucursalSeguradoraVO sucursalSeguradora, ProdutorVO produtor, DefinicaoProposta definicaoProposta) {
		
		if (produtor != null && produtor.getCpd() != null && produtor.getCpfCnpj() != null) {
			String cpfCnpj = produtor.getCpfCnpj().replaceAll("[.,;\\-\\s]", "");		
			produtor.setCpfCnpj(cpfCnpj.replace("/", ""));
			produtor.setNome(produtor.getNome());
			produtor.setProposta(new PropostaVO());
			produtor.getProposta().setNumeroSequencial(definicaoProposta.getSequencial());
			produtorDAO.salvar(produtor);
		}
		
	}

	private void salvarMovimentoBeneficiario(BeneficiarioVO beneficiario, SituacaoProposta situacaoProposta) {
		MovimentoBeneficiarioVO movimentoBeneficiario = new MovimentoBeneficiarioVO();
		movimentoBeneficiario.setBeneficiario(beneficiario);
		movimentoBeneficiario.setProposta(beneficiario.getProposta());
		movimentoBeneficiario.setSituacaoProposta(situacaoProposta);

		movimentoBeneficiarioDAO.salvar(movimentoBeneficiario);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public PropostaVO cancelar(Long numeroSequencial) {
		
		PropostaVO propostaVO = consultarPorSequencial(numeroSequencial);
	
		if(propostaVO == null){
			throw new BusinessException(new Message("msg.erro.cancelar.proposta.nao.encontrada",Message.ERROR_TYPE));

		}
		else{
			/*PR�-CANCELADA PROPOSTA IB*/		
			if(propostaVO.getCanalVenda() != null && (Constantes.CODIGO_CANAL_VENDA_INTERNET_BANKING.equals(propostaVO.getCanalVenda().getCodigo()) 
					|| Constantes.CODIGO_CANAL_VENDA_SHOPPING_SEGUROS.equals(propostaVO.getCanalVenda().getCodigo()) || Constantes.CODIGO_CANAL_VENDA_APP_BANCO.equals(propostaVO.getCanalVenda().getCodigo())
					|| Constantes.CODIGO_CANAL_VENDA_RODOBENS.equals(propostaVO.getCanalVenda().getCodigo()) || Constantes.CODIGO_CANAL_VENDA_CALL_CENTER.equals(propostaVO.getCanalVenda().getCodigo()))){
				if(SituacaoProposta.RASCUNHO.equals(propostaVO.getMovimento().getSituacao()) || SituacaoProposta.PENDENTE.equals(propostaVO.getMovimento().getSituacao())){
					salvarMovimentoProposta(numeroSequencial, SituacaoProposta.CANCELADA);
					
					if( Constantes.CODIGO_CANAL_VENDA_SHOPPING_SEGUROS.equals(propostaVO.getCanalVenda().getCodigo())){							
						  enviarCancelamentoShopping(numeroSequencial);
					}
					
				}else{
					salvarMovimentoProposta(numeroSequencial, SituacaoProposta.PRE_CANCELADA);
//					Nao chamar Shopping para proposte pre cancelada
//					if( Constantes.CODIGO_CANAL_VENDA_SHOPPING_SEGUROS.equals(propostaVO.getCanalVenda().getCodigo())){							
//						  enviarCancelamentoShopping(numeroSequencial);
//					}
				}
			}else{
				salvarMovimentoProposta(numeroSequencial, SituacaoProposta.CANCELADA);
			}
		}
		
		//chamar o m�todo do MIP(odontoprev) para realizar o estorno da 1� parcela creditada no cart�o
		if(FormaPagamento.CARTAO_CREDITO.equals(propostaVO.getFormaPagamento()) && SituacaoProposta.PENDENTE.equals(propostaVO.getMovimento().getSituacao())){
			odontoprevServiceFacade.cancelarDebitoPrimeiraParcelaCartaoDeCredito(propostaVO);
		}
		
		//salvarMovimentoProposta(numeroSequencial, SituacaoProposta.CANCELADA);
		return consultarPorSequencial(numeroSequencial);
	
	}
	
	private void enviarCancelamentoShopping(Long numeroSequencial){
		 String codigoProposta = propostaDAO.obterCodigoPropostaPorSequencial(numeroSequencial);
		   if(codigoProposta != null){
				CancelarContratacaoRequestVO cancelarContratacao = new CancelarContratacaoRequestVO();
				cancelarContratacao.setChaveOrigem(codigoProposta.trim());
				cancelarContratacao.setProduto(new AtualizaEtapaCompraProdutoVO());
				cancelarContratacao.getProduto().setCodigoProduto(Constantes.CODIGO_PRODUTO_SHOPPING_SEGUROS);
				ResultadoVO resultado = etapaCompraWebService.cancelarContratacao(cancelarContratacao);
				LOGGER.error("Retorno do servi�o de cancelamento do etapaCompraWebService: "+ resultado);
		   }else{
			   LOGGER.error("Codigo de proposta nao encontrado.");
		   }
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public PropostaVO cancelarPropostaPorProtocolo(String protocolo, Integer canalVenda) {
		Long numeroSequencial;
		String responsavalAtualizacao = "WEBSERVICE-"+canalVenda.toString();
		numeroSequencial = propostaDAO.obterSequencialPorProtocolo(protocolo, canalVenda);
		
		if(numeroSequencial == null){
			throw new BusinessException(new Message("msg.erro.proposta.nao.encontrada.protocolo", Message.ERROR_TYPE));
		}
		propostaDAO.atualizarResponsavelPorAtualizacao(numeroSequencial, responsavalAtualizacao);
		if(propostaPossuiStatusCancelada(numeroSequencial) == false){
			salvarMovimentoProposta(numeroSequencial, SituacaoProposta.CANCELADA, responsavalAtualizacao);
		}
		return consultarPorSequencial(numeroSequencial);
	}
	

	
	
	public boolean propostaPossuiStatusCancelada(Long numeroSequencial){
		
		return propostaDAO.possuiStatusCancelada(numeroSequencial);
	}
	
	public boolean possuiStatusImplantada(Long numeroSequencial){
		return propostaDAO.possuiStatusImplantada(numeroSequencial);
	}


	public boolean possuiStatusImplantadaProponente(Long numeroSequencial){
		return propostaDAO.possuiStatusImplantadaProponente(numeroSequencial);
	}
	
	public Integer consultarParentescoAtivo(Long numeroSequencial){
		return propostaDAO.consultarParentescoAtivo(numeroSequencial);
	}
	
	public Integer consultaMovimentacao(Long numeroSequencial){
		return propostaDAO.consultaMovimentacao(numeroSequencial);
	}
	
	public Integer consultarTipoParentesco(Long numeroSequencial){
		return propostaDAO.consultarTipoParentesco(numeroSequencial);
	}
	
	public Integer consultarTipoParentescoDepen(Long numeroSequencial){
		return propostaDAO.consultarTipoParentescoDepen(numeroSequencial);
	}
	
	public PropostaVO consultarPorSequencial(Long sequencialProposta) {
		PropostaVO proposta = propostaDAO.consultarPorSequencial(sequencialProposta);
		if (proposta == null) {
			throw new BusinessException(new Message("msg.erro.proposta.nao.encontrada", Message.ERROR_TYPE));
		}
		
		proposta.setProdutores(produtorDAO.listarPorProposta(proposta));
		proposta.setMovimento(obterSituacaoDeProposta(proposta.getNumeroSequencial()));
		proposta.setBeneficiarios(beneficiarioDAO.listarPorProposta(proposta));	
		proposta.setPlanoVO(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlanoVO().getCodigo()));
		
		if (proposta.getTitular() != null) {
			proposta.getProponente().setTelefones(telefoneDAO.listarPorProponente(proposta.getProponente()));
			if (proposta.getTitular().isMenorDeIdade()) {
				proposta.setResponsavelLegal(responsavelLegalDAO.listarPorProposta(proposta));
			}
		}

		if (proposta.isPagamentoDebitoAutomatico()) {
			detalharAgenciaBancaria(proposta);
		}

		return normalizadorProposta.normalizar(proposta);
	}

	public Integer consultarQuantidadePropostasPorDataAtualParaBeneficiario(String cpf) {
		return propostaDAO.consultarQuantidadePropostasPorDataAtualParaBeneficiario(cpf);
	}

	public SituacaoProposta consultarUltimaSituacaoProposta(PropostaVO proposta) {
		MovimentoPropostaVO movimentoProposta = movimentoPropostaDAO.consultarUltimoMovimento(proposta.getNumeroSequencial());
		if (movimentoProposta == null) {
			return null;
		}
		return movimentoProposta.getSituacao();
	}
	public SituacaoProposta consultarUltimaSituacaoProposta(Long numeroSequencial) {
		MovimentoPropostaVO movimentoProposta = movimentoPropostaDAO.consultarUltimoMovimento(numeroSequencial);
		if (movimentoProposta == null) {
			return null;
		}
		return movimentoProposta.getSituacao();
	}

	public void adicionarDependente(PropostaVO proposta) {
		if (proposta.getDependentes() == null) {
			proposta.setDependentes(new ArrayList<DependenteVO>());
		}

		Validador validador = new Validador();
		// Uma proposta s� pode possuir no m�ximo 8 dependentes
		validador.verdadeiro(proposta.getDependentes().size() < 8, "msg.erro.cadastro.proposta.dependentes.maior.que.permitido");
		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}

		proposta.getDependentes().add(new DependenteVO());
	}

	public void adicionarTelefone(PropostaVO proposta) {
		if (proposta.getProponente().getTelefones() == null) {
			proposta.getProponente().setTelefones(new ArrayList<TelefoneVO>());
		}

		Validador validador = new Validador();
		validador.verdadeiro(proposta.getProponente().getTelefones().size() < 3, "msg.erro.cadastro.proposta.telefone.maior.que.permitido");
		if (validador.hasError()) {
			throw new BusinessException(validador.getMessages());
		}

		proposta.getProponente().getTelefones().add(new TelefoneVO());
	}

	public List<PropostaVO> listarPropostasPorProponente(String cpfProponente, Integer canalVenda,
			DateTime periodoInicial, DateTime periodoFinal) {
		 Validador validador = new Validador();
		 validador.obrigatorio(cpfProponente, "CPF do proponente");
		 validador.obrigatorio(canalVenda, "canal de venda");
		 validador.obrigatorio(periodoInicial,"per�odo inicial");
		 validador.obrigatorio(periodoFinal, "per�odo final");
		 if(periodoInicial != null && periodoFinal != null){
			validador.falso(periodoInicial.isAfter(periodoFinal),"msg.erro.lista.datainicio.datafim");
		 }
		 if(validador.hasError()){
			 throw new BusinessException(validador.getMessages());
		 }
		 List<PropostaVO> listaDePropostas =  propostaDAO.listarPropostasPorProponente(cpfProponente, canalVenda, periodoInicial, periodoFinal);
		 
		 validador.verdadeiro((listaDePropostas != null && !listaDePropostas.isEmpty()),"msg.erro.lista.propostas.vigentes");
		 if(validador.hasError()){
			 throw new BusinessException(validador.getMessages());
		 }
		 
		 for(Iterator<PropostaVO> listaComItemARemover = listaDePropostas.iterator(); listaComItemARemover.hasNext();){
				PropostaVO p = listaComItemARemover.next();
				if(propostaPossuiStatusCancelada(p.getNumeroSequencial())){
					listaComItemARemover.remove();
				}
				else{
					MovimentoPropostaVO movimentoPropostaVO =  movimentoPropostaDAO.consultarUltimoMovimento(p.getNumeroSequencial());
					//TODO rascunho n�o � considerado vigente??
					if(SituacaoProposta.RASCUNHO.getCodigo().equals(movimentoPropostaVO.getSituacao().getCodigo())){
						listaComItemARemover.remove();
					}
					
					
						
				}
		}
		if(listaDePropostas == null || listaDePropostas.isEmpty()){
			validador.adicionarErro("msg.erro.lista.propostas.vigentes");
			throw new BusinessException(validador.getMessages());
		}
		 
		return listaDePropostas;
	}

	/**
	 * Metodo resposanvel por atualizar o status das propostas na base do shopping de seguros.
	 */
	public void atualizarStatusDaPropostaNoShoppingDeSeguros(){
		//obter todas as propostas atualizadas no dia referentes ao shopping de seguros
		LOGGER.error("Inicio do metodo para atualizar propostas no shopping de seguros");
		List<StatusPropostaVO> listaProposta = propostaDAO.listarPropostasAtualizadasDoShoppingDeSeguros();
		
		//para cada proposta obtida, verificar se a proposta deve ter o seu status atualizado na base do shooping de seguros
		//a proposta deve ser atualizada quando seu status for: Implantada, Cancelada
		if(listaProposta != null && !listaProposta.isEmpty()){
			EfetuarContratacaoRequestVO contratacao = null;
			CancelarContratacaoRequestVO cancelarContratacao = null;
			ResultadoVO resultado = null;
			for (StatusPropostaVO statusPropostaVO : listaProposta) {
				try {
					
					LOGGER.error(String.format("Proposta a ser atualizada: [%s]", statusPropostaVO.getCodigoProposta()));
					
					if(SituacaoProposta.IMPLANTADA.equals(statusPropostaVO.getSituacaoProposta())){
						contratacao = new EfetuarContratacaoRequestVO();
						contratacao.setNumeroProposta(statusPropostaVO.getCodigoProposta().toString());
						contratacao.setNumeroApolice(statusPropostaVO.getCodigoProposta().toString());
						contratacao.setProduto(new AtualizaEtapaCompraProdutoVO());
						contratacao.getProduto().setCodigoProduto(Constantes.CODIGO_PRODUTO_SHOPPING_SEGUROS);
						resultado = etapaCompraWebService.efetuarContratacao(contratacao);
					}
					else if(SituacaoProposta.CANCELADA.equals(statusPropostaVO.getSituacaoProposta())){
						cancelarContratacao = new CancelarContratacaoRequestVO();
						cancelarContratacao.setChaveOrigem(statusPropostaVO.getCodigoProposta().toString());
						cancelarContratacao.setProduto(new AtualizaEtapaCompraProdutoVO());
						cancelarContratacao.getProduto().setCodigoProduto(Constantes.CODIGO_PRODUTO_SHOPPING_SEGUROS);
						resultado = etapaCompraWebService.cancelarContratacao(cancelarContratacao);
					}
					LOGGER.error(String.format(" Resultado para proposta [%s] igual a [%s] - [%s] ", statusPropostaVO.getCodigoProposta(), resultado.getCodigoRetorno(), resultado.getMensagemRetorno()));
				} catch (IntegrationException e) {
					LOGGER.error("Erro ao atualizar o status da proposta no shopping de seguros. Proposta: " + statusPropostaVO.getCodigoProposta() + " / Status: " + statusPropostaVO.getSituacaoProposta().getDescricao());
					throw new IntegrationException(e);
				} catch (BusinessException e) {
					LOGGER.error("Erro ao atualizar o status da proposta no shopping de seguros. Proposta: " + statusPropostaVO.getCodigoProposta() + " / Status: " + statusPropostaVO.getSituacaoProposta().getDescricao());
					throw new BusinessException(e.getMessage());
				}
			}
		}
		LOGGER.error("Fim do metodo para atualizar propostas no shopping de seguros");
	}

	public List<PropostaVO> listarPropostasPorBeneficiario(String cpfBeneficiario, Integer canal) {
		
		List<PropostaVO> propostas = new ArrayList<PropostaVO>();
		
		Validador validador = new Validador();
		validador.cpf(cpfBeneficiario, "CPF do Benefici�rio");
		
		if(validador.hasError()){
			throw new BusinessException(validador.getMessages());
		}
		propostas =  propostaDAO.listarPropostasPorBeneficiario(cpfBeneficiario, canal);
		validador.verdadeiro((propostas != null && !propostas.isEmpty()),"msg.erro.nenhuma.proposta.encontrada.para.filtro.informado");
		if(validador.hasError()){
			throw new BusinessException(validador.getMessages());
		}
		 
		return propostas;
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public void cancelarPropostaShoppingSeguros(Long sequencialProposta, String protocolo){
		PropostaVO propostaVO = consultarPorSequencial(sequencialProposta);

		if(propostaVO == null){
			throw new BusinessException(new Message("msg.erro.cancelar.proposta.nao.encontrada",Message.ERROR_TYPE));
		}
		else if(protocolo == null || "".equals(protocolo)){
			throw new BusinessException("O Protocolo � obrig�torio.");
		}
		else if(protocolo.length() > 30){
			throw new BusinessException("Protocolo com mais de 30 caracteres.");
		}
		else if(SituacaoProposta.CANCELADA.equals(propostaVO.getMovimento().getSituacao()) || SituacaoProposta.PRE_CANCELADA.equals(propostaVO.getMovimento().getSituacao())){
			throw new BusinessException("Proposta j� se encontra cancelada.");
		}
		else{
			if(SituacaoProposta.RASCUNHO.equals(propostaVO.getMovimento().getSituacao()) || SituacaoProposta.PENDENTE.equals(propostaVO.getMovimento().getSituacao())){
				
				salvarMovimentoProposta(sequencialProposta, SituacaoProposta.CANCELADA);
				
				if( Constantes.CODIGO_CANAL_VENDA_SHOPPING_SEGUROS.equals(propostaVO.getCanalVenda().getCodigo())){							
					  enviarCancelamentoShopping(sequencialProposta);
				}
			}else{
				salvarMovimentoProposta(sequencialProposta, SituacaoProposta.PRE_CANCELADA);
			}
			propostaDAO.incluirProtocoloDeCancelamentoNaProposta(sequencialProposta, protocolo);
			
			//chamar o m�todo do MIP(odontoprev) para realizar o estorno da 1� parcela creditada no cart�o
			if(FormaPagamento.CARTAO_CREDITO.equals(propostaVO.getFormaPagamento()) && SituacaoProposta.PENDENTE.equals(propostaVO.getMovimento().getSituacao())){
				odontoprevServiceFacade.cancelarDebitoPrimeiraParcelaCartaoDeCredito(propostaVO);
			}
		}
	}
	
	public String obterCodigoPropostaPorSequencial(Long sequencialProposta){
		return propostaDAO.obterCodigoPropostaPorSequencial(sequencialProposta);
	}
		
	public Long obterSequencialPorCodigoProposta(String codigoProposta){
		return propostaDAO.obterSequencialPorCodigoProposta(codigoProposta);
	}
	
	/**
	 * <ul>Metodo responsavel por obter a situacao de proposta.</ul>
	 * <li>Caso possua status de cancelamento, o status e de cancelamento.</li>
	 * <li>Caso nao possua status de cancelamento e possua status de implantada, o status e de implantada.</li>
	 * <li>Caso nao possua status de cancelamento nem de implantacao, o status e o mais recente</li>
	 * 
	 * @param codigoProposta - codigo de proposta a ser buscado.
	 * @return Long - situacao da proposta.
	 */
	public Integer obterSituacaoProposta(String codigoProposta, Integer codigoCanal){
		
		Integer codigoMovimento = null;
		Long sequencialProposta = obterSequencialPorCodigoProposta(codigoProposta);
				
		if(movimentoPropostaDAO.verificarSePossuiMovimentoInformado(sequencialProposta, SituacaoProposta.CANCELADA)){
			codigoMovimento = SituacaoProposta.CANCELADA.getCodigo();
		}else if(movimentoPropostaDAO.verificarSePossuiMovimentoInformado(sequencialProposta, SituacaoProposta.IMPLANTADA)){
			codigoMovimento = SituacaoProposta.IMPLANTADA.getCodigo();
		}else{
			codigoMovimento = movimentoPropostaDAO.obterUltimoCodigoMovimentoPorSequencial(sequencialProposta);
		}
		
		return codigoMovimento;
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	public List<String> consultarProposta(String consulta){
		return propostaDAO.consultarProposta(consulta);
	}
	
	/**
	 * M�todo respons�vel por cancelar as propostas do site 100% corretor com data de validade expirada.
	 * 
	 * @return String - Quantidade de propostas canceladas.
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public String cancelarPropostasDoSite100CorretorComDataDeValidadeExpirada() {

		StringBuilder mensagemDeRetorno = new StringBuilder(50);
		int contadorPropostaCancelada = 0;

		// listar as propostas com data de validade expirada
		List<PropostaExpiradaVO> listaPropostasExpiradas = propostaDAO.listarPropostasDoSite100CorretorComDataDeValidadeExpirada();

		// Cancelar todas as propostas com data de validade expirada.
		// Caso a data de validade da proposta seja no fim de semana, a proposta dever� ganhar mais um dia �til.
		// Para cancelar uma proposta, � necess�rio incluir um movimento de cancelamento na tabela de movimento.
		if (listaPropostasExpiradas != null) {
			DateTime dataAtual = new DateTime();
			for (PropostaExpiradaVO propostaExpiradaVO : listaPropostasExpiradas) {
				
				if (dataAtual.isAfter(propostaExpiradaVO.getDataValidadeProposta())) {
					salvarMovimento(propostaExpiradaVO.getSequencialProposta(), SituacaoProposta.CANCELADA, "CANC. EXPIRADA");
					++contadorPropostaCancelada;
				}
				
			}
		}

		// Retornar a quantidade de propostas canceladas por data de validade expirada.
		if (contadorPropostaCancelada == 0) {
			mensagemDeRetorno.append("Nenhuma proposta cancelada.");
		} else if (contadorPropostaCancelada == 1) {
			mensagemDeRetorno.append(contadorPropostaCancelada).append(" proposta cancelada.");
		} else {
			mensagemDeRetorno.append(contadorPropostaCancelada).append(" propostas canceladas.");
		}

		return mensagemDeRetorno.toString();
	}


	
	public void salvarMovimento(Long sequencialProposta,  SituacaoProposta situacao, String responsavelAtualizacao) {
		MovimentoPropostaVO movimentoProposta = new MovimentoPropostaVO();
		movimentoProposta.setDataInicio(new DateTime());
		movimentoProposta.setProposta(new PropostaVO());
		movimentoProposta.getProposta().setNumeroSequencial(sequencialProposta);
		movimentoProposta.setSituacao(situacao);
		movimentoPropostaDAO.salvar(movimentoProposta, responsavelAtualizacao);
	}
	
	
	public List<MovimentoPropostaVO> listarMovimentoPropostaPorSequencial(Long sequencialProposta) {
		
		List<MovimentoPropostaVO> movimentoProposta = movimentoPropostaDAO.listarMovimentoProposta(sequencialProposta);
		
		return movimentoProposta;
	}
	

}
