package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaExpiradaVO;

/**
 * Classe para implementar o RowMapper da proposta expirada.
 * 
 */
public class PropostaExpiradaRowMapper implements RowMapper<PropostaExpiradaVO> {

	/**
	 * Mapeia uma row a um objeto
	 * 
	 * @param rs - ResultSet
	 * @param row - indice da linha
	 * 
	 * @return PropostaExpiradaVO - objeto mapeado
	 */
	public PropostaExpiradaVO mapRow(ResultSet rs, int row) throws SQLException {
		PropostaExpiradaVO propostaExpirada = new PropostaExpiradaVO();

		propostaExpirada.setSequencialProposta(rs.getLong("NSEQ_PPSTA_DNTAL"));
		// Data Emiss�o Proposta
		Date dataEmissaoProposta = rs.getDate("DEMIS_PPSTA_DNTAL_INDVD");
		if (dataEmissaoProposta != null) {
			propostaExpirada.setDataEmissaoProposta(new DateTime(dataEmissaoProposta.getTime()));
		}

		return propostaExpirada;
	}
}
