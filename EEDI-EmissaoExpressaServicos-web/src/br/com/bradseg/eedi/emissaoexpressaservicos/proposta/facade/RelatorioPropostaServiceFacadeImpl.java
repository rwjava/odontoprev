package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.emissaoexpressaservicos.support.Report;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ReportFile;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.FormatacaoUtil;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.GeradorBoletoBancario;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.GeradorBoletoCodigo;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.Banco;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BoletoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FormaPagamento;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PlanoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;

/**
 * Servi�o para gerar o relat�rio da proposta
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class RelatorioPropostaServiceFacadeImpl implements RelatorioPropostaServiceFacade {

	//private static final Logger LOGGER = LoggerFactory.getLogger(RelatorioPropostaServiceFacadeImpl.class);
	
	@Autowired
	private PropostaServiceFacade propostaServiceFacade;

	@Autowired
	private PlanoServiceFacade planoServiceFacade;

	/**
	 * Consulta as informa��es detalhadas da proposta e gera o arquivo de relat�rio.
	 * @return Arquivo de relat�rio
	 */
	public ReportFile gerarRelatorioPropostaCompleta(Long codigoProposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto) {
		return gerarRelatorio(propostaServiceFacade.consultarPorSequencial(codigoProposta), gerarViaOperadora, gerarViaCorretor, gerarBoleto);
	}


	/**
	 * Gera um arquivo de relat�rio para proposta. O arquivo de relat�rio cont�m 2 relat�rios:
	 * 	<ul>
	 * 		<li>3 vias da proposta para ser preenchida manualmente</li>
	 * 		<li>Boleto banc�rio para ser preenchido manualmente</li>
	 * 	</ul>
	 * 
	 * O nome do arquivo de relat�rio gerado ser� composto por:
	 * 	<ul>
	 * 		<li>BS-PROPOSTA- + c�digo da proposta formatado (Ex.: BS-PROPOSTA-BDA00000032932-3)</li>
	 * 	</ul>
	 * 
	 * Existem 2 layouts de relat�rio:
	 * 	<ul>
	 * 		<li>imprime_proposta.jrxml, para propostas que foram criadas com o plano antigo (29,90 + taxa de ades�o de 5,00)</li>
	 * 		<li>imprime_proposta_novo_termo.jrxml, para propostas que foram criadas com o plano novo (35,90 sem taxa de ades�o</li>
	 * 	</ul>
	 * 
	 * @param proposta Dados da proposta para gera��o
	 * @return Arquivo de relat�rio
	 */
	private ReportFile gerarRelatorio(PropostaVO proposta, boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto) {
		
		proposta.setPlanoVO(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlanoVO().getCodigo()));
		
		BoletoVO boleto = GeradorBoletoBancario.gerarBoletoProposta(proposta);
		GeradorBoletoCodigo gerarBoletoCodigo = new GeradorBoletoCodigo(boleto);

		ReportFile reportFile = new ReportFile();
		reportFile.setFileName("BS-PROPOSTA-" + proposta.getCodigoFormatado());
		
		// Cria os par�metros da proposta
		Map<Object, Object> parametros = preencherParametros(proposta, boleto ,gerarBoletoCodigo);
		Map<Object, Object> paramsViaOperadora = new HashMap<Object, Object>();
		Map<Object, Object> paramsViaContratante = new HashMap<Object, Object>();
		Map<Object, Object> paramsViaCorretor = new HashMap<Object, Object>();

		parametros.put("TOTAL_PAGINAS", calcularTotalDePaginasDoPDF(gerarViaOperadora, gerarViaCorretor, gerarBoleto));
		
		paramsViaContratante.putAll(parametros);
		paramsViaContratante.put("via", "VIA: CONTRATANTE");
		//paramsViaContratante.put("viaBoleto", "VIA: CONTRATANTE");
		paramsViaContratante.put("exibirPontilhado", true);
		paramsViaContratante.put("contadorVia", 0);
		paramsViaContratante.put("contadorBoleto", 3);
		
		if(proposta.getCanalVenda().getCodigo() == 7){
			reportFile.adicionarRelatorio(new Report("imprime_nova_proposta.shopping.jrxml", proposta, paramsViaContratante));
		}else{
			reportFile.adicionarRelatorio(new Report("imprime_nova_proposta.jrxml", proposta, paramsViaContratante));
			
		}
		
		
		if(FormaPagamento.BOLETO_DEMAIS_DEBITO.equals(proposta.getFormaPagamento()) || FormaPagamento.BOLETO_E_CARTAO_CREDITO.equals(proposta.getFormaPagamento()) || FormaPagamento.BOLETO_BANCARIO.equals(proposta.getFormaPagamento())){
			reportFile.adicionarRelatorio(new Report("boleto_proposta.jrxml", proposta, paramsViaContratante));
		}
		if(gerarBoleto){
			reportFile.adicionarRelatorio(new Report("boleto_proposta.jrxml", proposta, paramsViaContratante));
		}
		
		if(gerarViaOperadora){
			paramsViaOperadora.putAll(parametros);
			paramsViaOperadora.put("via", "VIA: OPERADORA - CONTRATADA");
			//paramsViaOperadora.put("viaBoleto", "VIA: OPERADORA - CONTRATADA");
			paramsViaOperadora.put("exibirPontilhado", true);
			if(gerarBoleto){
				paramsViaOperadora.put("contadorVia", 3);
				paramsViaOperadora.put("contadorBoleto", 6);
			}else{
				paramsViaOperadora.put("contadorVia", 2);
				paramsViaOperadora.put("contadorBoleto", 5);
			}
			
			reportFile.adicionarRelatorio(new Report("imprime_nova_proposta.jrxml", proposta, paramsViaOperadora));
			if(gerarBoleto){
				reportFile.adicionarRelatorio(new Report("boleto_proposta.jrxml", proposta, paramsViaOperadora));
			}
		}
		
		if(gerarViaCorretor){
			paramsViaCorretor.putAll(parametros);
			paramsViaCorretor.put("via", "VIA: CORRETOR");
			//paramsViaCorretor.put("viaBoleto", "VIA: BANCO");
			paramsViaCorretor.put("exibirPontilhado", true);
			
			if(gerarBoleto){
				paramsViaCorretor.put("contadorVia", 6);
				paramsViaCorretor.put("contadorBoleto", 9);
			}else{
				paramsViaCorretor.put("contadorVia", 5);
				paramsViaCorretor.put("contadorBoleto", 8);
			}
			
			reportFile.adicionarRelatorio(new Report("imprime_nova_proposta.jrxml", proposta, paramsViaCorretor));
			if(gerarBoleto){
				reportFile.adicionarRelatorio(new Report("boleto_proposta.jrxml", proposta, paramsViaCorretor));
			}
		}

		return reportFile;
	}


	private Integer calcularTotalDePaginasDoPDF(boolean gerarViaOperadora, boolean gerarViaCorretor, boolean gerarBoleto) {
		
		Integer totalDePaginas = 2;
		
		if(gerarBoleto){
			totalDePaginas = totalDePaginas + 1;
		}
		
		if(gerarViaOperadora){
			totalDePaginas = totalDePaginas + 2;
			if(gerarBoleto){
				totalDePaginas = totalDePaginas + 1;
			}
		}
		if(gerarViaCorretor){
			totalDePaginas = totalDePaginas + 2;
			if(gerarBoleto){
				totalDePaginas = totalDePaginas + 1;
			}
		}
		
		return totalDePaginas;
	}

	/**
	 * Preenche os par�metros que ser�o usados no relat�rio da proposta:
	 * 	- Nome do corretor logado no sistema (Esse nome � usado no corretor da proposta, dever�amos pegar do banco e n�o do usu�rio logado)
	 * 	- Valor da taxa de ades�o, fixo em R$ 5,00 para propostas de antes 01/03/2013
	 * @param proposta Proposta
	 * @return Mapa de par�metros
	 */
	private Map<Object, Object> preencherParametros(PropostaVO proposta, BoletoVO boleto, GeradorBoletoCodigo gerarBoleto) {
		Map<Object, Object> parametros = Maps.newHashMap();
		
		parametros.put("CODIGO_PROPOSTA", proposta.getCodigoFormatado());
		parametros.put("IMG_BARCODE", proposta.getCodigoFormatado());
		parametros.put("IS_BOLETO", FormaPagamento.BOLETO_BANCARIO.equals(proposta.getFormaPagamento()));
		
		parametros.put("IS_DEBITO_AUTOMATICO", !FormaPagamento.BOLETO_BANCARIO.equals(proposta.getFormaPagamento()));
		parametros.put("FORMA_PAGAMENTO", FormaPagamento.BOLETO_DEMAIS_DEBITO.getDescricao());
		/**
		 * EXIBIR FORMA DE PAGAMENTO
		 */
		if(proposta.getFormaPagamento() != null){
			parametros.put("FORMA_PAGAMENTO_SELECIONADA", proposta.getFormaPagamento().getCodigo().toString());
		} else{
			parametros.put("FORMA_PAGAMENTO_SELECIONADA", null);
		}
		
		if(!FormaPagamento.BOLETO_BANCARIO.equals(proposta.getFormaPagamento())){
			if(proposta.getFormaPagamento() != null){
				parametros.put("FORMA_PAGAMENTO", proposta.getFormaPagamento().getDescricao());
			}else{
				parametros.put("FORMA_PAGAMENTO", null);
			}
		}		
		//GeradorBoletoCodigo gerarBoleto = new GeradorBoletoCodigo();
		
		parametros.put("CANAL_DE_VENDA", proposta.getCanalVenda().getCodigo());
		
		parametros.put("BOLETO_IMG_BARCODE", gerarBoleto.calcularCodigoBarra());
		parametros.put("BOLETO_VENCIMENTO", boleto.getDataDeVencimento().toString("dd/MM/yyyy"));
		parametros.put("BOLETO_LINHA_DIGITAVEL", gerarBoleto.calcularLinhaDigitavel());
		parametros.put("BOLETO_SACADO", boleto.getSacado());
		parametros.put("BOLETO_NOSSO_NUMERO", boleto.getNossoNumero());
		parametros.put("BOLETO_CODIGO_SUCURSAL", boleto.getCodigoSucursal().toString());
		parametros.put("BOLETO_CODIGO_CORRETOR", boleto.getCodigoCorretor().toString());
		parametros.put("BOLETO_VALOR_DOCUMENTO", boleto.getValorBoleto());
		
		//PROPONENTE
		if (proposta.getProponente() != null && proposta.getProponente().getCodigo() != null) {
			parametros.put("nome_proponente", proposta.getProponente().getNome());
			parametros.put("cpf_proponente", proposta.getProponente().getCpf());
			parametros.put("email_proponente", proposta.getProponente().getEmail());
			parametros.put("data_nascimento_proponente", proposta.getProponente().getDataNascimentoFormatada());
			if(proposta.getBanco() != null && proposta.getBanco().getCodigo() != null && 0 != proposta.getBanco().getCodigo().intValue()){
				parametros.put("numero_banco", proposta.getBanco().getCodigo().toString());
				parametros.put("nome_banco", Banco.buscaBancoPor(proposta.getBanco().getCodigo()).getNome());
			}
			if(null != proposta.getProponente().getContaCorrente().getNumero()){
				parametros.put("numero_conta_corrente_com_digito", proposta.getProponente().getContaCorrente().getNumero().toString() + "-" + proposta.getProponente().getContaCorrente().getDigitoVerificador());
			}
			if(null != proposta.getProponente().getContaCorrente().getAgenciaBancaria()){
				parametros.put("nome_agencia", proposta.getProponente().getContaCorrente().getAgenciaBancaria().getNome());
			}
		}
		
		//TITULAR
		if(proposta.getTitular() != null && proposta.getTitular().getCodigo() != null){
			parametros.put("titular_nome", proposta.getTitular().getNome());
			parametros.put("titular_cpf", proposta.getTitular().getCpf());
			parametros.put("titular_estado_civil", proposta.getTitular().getEstadoCivil().getCodigo().toString());
			parametros.put("titular_data_nascimento", proposta.getTitular().getDataNascimentoFormatada());
			parametros.put("titular_sexo", proposta.getTitular().getSexo().getDescricao());
			parametros.put("titular_nome_mae", proposta.getTitular().getNomeMae());
			parametros.put("titular_cns", proposta.getTitular().getNumeroCartaoNacionalSaude());
			parametros.put("titular_dnv", proposta.getTitular().getNumeroDeclaracaoNascidoVivo());
			parametros.put("emailBeneficiario", proposta.getTitular().getEmail());
			if(proposta.getTitular().getGrauParentesco() != null){
				parametros.put("titular_parentesco_proponente", proposta.getTitular().getGrauParentesco().getCodigo());
			}
			
			parametros.put("titular_parentesco_proponente", null);
		/*	for(TelefoneVO telefoneBeneficiario : proposta.getProponente().getTelefones()){
				if(telefoneBeneficiario.getTipo().getCodigo().equals(3)){
					parametros.put("telefoneCelularBeneficiario", "(" + telefoneBeneficiario.getDdd() + ") " + telefoneBeneficiario.getNumero());
				}else{
					parametros.put("telefoneResidencialBeneficiario", "(" + telefoneBeneficiario.getDdd() + ") " + telefoneBeneficiario.getNumero());
				}
			}*/
			parametros.put("titular_endereco", proposta.getTitular().getEndereco().getLogradouro());
			parametros.put("titular_numero", proposta.getTitular().getEndereco().getNumero().toString());
			parametros.put("titular_compl", proposta.getTitular().getEndereco().getComplemento());
			parametros.put("titular_bairro", proposta.getTitular().getEndereco().getBairro());
			parametros.put("titular_cidade", proposta.getTitular().getEndereco().getCidade());
			parametros.put("titular_uf", proposta.getTitular().getEndereco().getUnidadeFederativa().getSigla());
			parametros.put("titular_cep", proposta.getTitular().getEndereco().getCep());
		}
		
		//RESPONSAVEL LEGAL
		if(proposta.getCanalVenda().getCodigo() != 7){
			if(proposta.getResponsavelLegal() != null && proposta.getResponsavelLegal().getCodigo() != null){
				parametros.put("responsavel_nome", proposta.getResponsavelLegal().getNome());
				parametros.put("responsavel_cpf", proposta.getResponsavelLegal().getCpf());
				parametros.put("responsavel_email", proposta.getResponsavelLegal().getEmail());
				if(proposta.getResponsavelLegal().getTelefone() != null){
					parametros.put("responsavel_telefone", "(" + proposta.getResponsavelLegal().getTelefone().getDdd() + ") " + proposta.getResponsavelLegal().getTelefone().getNumero());
				}
				parametros.put("responsavel_nasc", proposta.getResponsavelLegal().getDataNascimentoFormatada());
				parametros.put("responsavel_logradouro", proposta.getResponsavelLegal().getEndereco().getLogradouro());
				parametros.put("responsavel_numero", proposta.getResponsavelLegal().getEndereco().getNumero());
				parametros.put("responsavel_compl", proposta.getResponsavelLegal().getEndereco().getComplemento());
				parametros.put("responsavel_bairro", proposta.getResponsavelLegal().getEndereco().getBairro());
				parametros.put("responsavel_municipio", proposta.getResponsavelLegal().getEndereco().getCidade());
				parametros.put("responsavel_uf", proposta.getResponsavelLegal().getEndereco().getUnidadeFederativa().getSigla());
				parametros.put("responsavel_cep", proposta.getResponsavelLegal().getEndereco().getCep());
			}
		}
		
		//PLANO
		if(proposta.getPlanoVO() != null && proposta.getPlanoVO().getCodigo() != null){
			parametros.put("nome_plano", proposta.getPlanoVO().getNome() + Constantes.IFLE);
			parametros.put("periodo_carencia_mensal", proposta.getPlanoVO().getDescricaoCarenciaPeriodoMensal());
			parametros.put("periodo_carencia_anual", proposta.getPlanoVO().getDescricaoCarenciaPeriodoAnual());
			//valores mensal
			if(0.0 != proposta.getPlanoVO().getValorPlanoVO().getValorMensalTitular()){
				parametros.put("valor_plano_tit_mensal", FormatacaoUtil.formataValor(proposta.getPlanoVO().getValorPlanoVO().getValorMensalTitular()));
				parametros.put("valor_plano_dep_mensal", FormatacaoUtil.formataValor(proposta.getPlanoVO().getValorPlanoVO().getValorMensalDependente()));
			}
			//valores anual
			if(0.0 != proposta.getPlanoVO().getValorPlanoVO().getValorAnualTitular()){
				parametros.put("valor_plano_tit_anual", FormatacaoUtil.formataValor(proposta.getPlanoVO().getValorPlanoVO().getValorAnualTitular()));
				parametros.put("valor_plano_dep_anual", FormatacaoUtil.formataValor(proposta.getPlanoVO().getValorPlanoVO().getValorAnualDependente()));
			}
			//calculando o valor total de acordo com o tipo de cobran�a (mensal/anual)
			if(TipoCobranca.MENSAL.equals(proposta.getTipoCobranca())){
				parametros.put("valor_total_plano_mensal", FormatacaoUtil.formataValor(proposta.getPlanoVO().getValorPlanoVO().getValorMensalTitular() * proposta.getQuantidadeBeneficiarios().doubleValue()));
			}else if(TipoCobranca.ANUAL.equals(proposta.getTipoCobranca())){
				parametros.put("valor_plano_total_anual", FormatacaoUtil.formataValor(proposta.getPlanoVO().getValorPlanoVO().getValorAnualTitular() * proposta.getQuantidadeBeneficiarios().doubleValue()));
			}
			//variaveis que mudam de acordo com o plano selecionado
			if (Constantes.CODIGO_PLANO_EXCLUSIVE.equals(proposta.getPlanoVO().getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_EXCLUSIVE);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_EXCLUSIVE);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_EXCLUSIVE);
			} else if (Constantes.CODIGO_PLANO_EXCLUSIVE_PLUS.equals(proposta.getPlanoVO().getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_EXCLUSIVE_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_EXCLUSIVE_PLUS);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_EXCLUSIVE_PLUS);
			} else if (Constantes.CODIGO_PLANO_PRIME.equals(proposta.getPlanoVO().getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_PRIME);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_PRIME);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_PRIME);
			} else if (Constantes.CODIGO_PLANO_PRIME_PLUS.equals(proposta.getPlanoVO().getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_PRIME_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_PRIME_PLUS);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_PRIME_PLUS);
			}else if(Constantes.CODIGO_PLANO_MAX.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_MAX);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_MAX);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_MAX);
			
			}else if(Constantes.CODIGO_PLANO_MAX_PLUS.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_MAX_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_MAX_PLUS);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_MAX_PLUS);
			
			} else if(Constantes.CODIGO_PLANO_DENTE_LEITE.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTE_LEITE);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_DENTE_LEITE);
			
			}else if(Constantes.CODIGO_PLANO_DENTE_LEITE_MAX.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_MAX);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTE_LEITE_MAX);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX);
			
			}else if(Constantes.CODIGO_PLANO_DENTE_LEITE_EXECLUSIVE.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_EXCLUSIVE);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTE_LEITE_EXECUTIVE);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_DENTE_LEITE_EXCLUSIVE);
			
			}else if(Constantes.CODIGO_PLANO_DENTE_LEITE_MAX_PLUS.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_MAX);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTE_LEITE_MAX);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX);
			
			}else if(Constantes.CODIGO_PLANO_DENTE_LEITE_PRIME.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_MAX);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTE_LEITE_MAX);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX);
			
			}else if(Constantes.CODIGO_PLANO_DENTAL_JUNIOR.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_DENTAL_JUNIOR);
			
			}else if(Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_MAX);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR_MAX);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_JUNIOR_MAX);
			
			}else if(Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX_PLUS.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_MAX_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR_MAX_PLUS);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_JUNIOR_MAX_PLUS);
			
			}else if(Constantes.CODIGO_PLANO_DENTAL_JUNIOR_PRIME.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_PRIME);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR_PRIME);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_DENTAL_JUNIOR_PRIME);
			
			}else if(Constantes.CODIGO_PLANO_DENTAL_JUNIOR_EXCLUSIVE.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_EXCLUSIVE);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_JUNIOR_EXCLUSIVE);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_JUNIOR_EXCLUSIVE);
			
			}else if(Constantes.CODIGO_PLANO_MULTI.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_MULTI);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_MULTI);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_MULTI);
			
			}else if(Constantes.CODIGO_PLANO_MULTI_PLUS.equals(proposta.getPlanoVO().getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_MULTI_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_DENTAL_MULTI_PLUS);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_MULTI_PLUS);	
				
			}else {
				parametros.put("tipo_plano", Constantes.STRING_VAZIA);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("registro_ans", Constantes.NUMERO_ANS_PLANO_4);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_DENTAL);
			}
		}
		
		//CORRETOR
		if(proposta.getCorretor() != null && !Strings.isNullOrEmpty(proposta.getCorretor().getCpfCnpj())){
			if(null != proposta.getAgenciaProdutora() && null != proposta.getAgenciaProdutora().getCodigo()){
				parametros.put("codigo_agencia_produtora", proposta.getAgenciaProdutora().getCodigo().longValue());	
			}
			parametros.put("sucursal_cpd", proposta.getSucursalSeguradora().getCodigo().toString());
			if(proposta.getAngariador() != null){
				parametros.put("cpd_angariador", proposta.getAngariador().getCpd());
				parametros.put("nome_angariador", proposta.getAngariador().getNome());
			}
			parametros.put("cpd_corretor", proposta.getCorretor().getCpd().toString());
			parametros.put("nome_corretor", proposta.getCorretor().getNome());
			if(proposta.getCorretorMaster() != null){
				parametros.put("cpd_corretor_master", proposta.getCorretorMaster().getCpd());
				parametros.put("nome_corretor_master", proposta.getCorretorMaster().getNome());
			}
			parametros.put("gerente_produto_bvp", proposta.getCodigoMatriculaGerente());
			parametros.put("codigoMatriculaAssistente", proposta.getCodigoMatriculaAssistente());
			
			//solu��o para proposta antigas em que o posto de atendimento � igual a null na base de dados 
			if(!(proposta.getSucursalSeguradora().getCodigo() == 2 && proposta.getCodigoPostoAtendimento() == null)){
					proposta.setCodigoPostoAtendimento(Long.parseLong("0"));
			}	
			parametros.put("codigo_posto_atendimento", proposta.getCodigoPostoAtendimento().toString());
		}
		
	    parametros.put("data_emissao_proposta", proposta.getDataEmissao().toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
	    parametros.put("data_validade_proposta", getDataValidadeProposta(proposta).toString(Constantes.DATA_FORMATO_DD_MM_YYYY));
		
		return parametros;
	}
	
	/**
	 * 
	 * @param proposta
	 * @return
	 */
	public LocalDate getDataValidadeProposta(PropostaVO proposta) {
		LocalDate dataValidadeProposta = null;
		if (proposta.getDataEmissao() != null) {
			 dataValidadeProposta = proposta.getDataEmissao().plusDays(90);
			if (dataValidadeProposta.getDayOfWeek() == 6) {
				dataValidadeProposta = dataValidadeProposta.plusDays(2);
			} else if (dataValidadeProposta.getDayOfWeek() == 7) {
				dataValidadeProposta = dataValidadeProposta.plusDays(1);
			}
		}

		return dataValidadeProposta;
	}


	/**
	 * Gerar o pdf com o termo de ades�o.
	 * 
	 * @param codigoPlano - c�digo de plano.
	 * 
	 * @return ReportFile - Termo de Ades�o em PDF.
	 */
	public ReportFile gerarTermoDeAdesao(Long codigoProposta) {
		
		ReportFile reportFile = new ReportFile();
		
		PropostaVO proposta = propostaServiceFacade.consultarPorSequencial(codigoProposta);
		
		PlanoVO planoVO = planoServiceFacade.obterPlanoPorCodigo(proposta.getPlanoVO().getCodigo());
		
		if(planoVO != null){
			reportFile.setFileName("TERMO DE ADES�O");
			
			Map<Object, Object> parametros = Maps.newHashMap();
			
			//variaveis que mudam de acordo com o plano selecionado
			if (Constantes.CODIGO_PLANO_EXCLUSIVE.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_EXCLUSIVE);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_EXCLUSIVE);
			} else if (Constantes.CODIGO_PLANO_EXCLUSIVE_PLUS.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_EXCLUSIVE_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_EXCLUSIVE_PLUS);
			} else if (Constantes.CODIGO_PLANO_PRIME.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_PRIME);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_PRIME);
			} else if (Constantes.CODIGO_PLANO_PRIME_PLUS.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_PRIME_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_PRIME_PLUS);
			}else if (Constantes.CODIGO_PLANO_MAX.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_PRIME_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_PRIME_PLUS);
			}else if (Constantes.CODIGO_PLANO_MAX_PLUS.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_PRIME_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_PRIME_PLUS);
			}
			else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR);
				parametros.put("tipo_plano_logo",Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_DENTAL_JUNIOR);
			}
			else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_EXCLUSIVE.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_EXCLUSIVE);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_JUNIOR_EXCLUSIVE);
			}
			else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_MAX);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_JUNIOR_MAX);
			}
			else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX_PLUS.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_MAX_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_JUNIOR_MAX_PLUS);
			}
			else if (Constantes.CODIGO_PLANO_DENTE_LEITE.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_DENTE_LEITE);
			}
			else if (Constantes.CODIGO_PLANO_DENTE_LEITE_MAX.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_MAX);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX);
			}
			else if (Constantes.CODIGO_PLANO_DENTE_LEITE_MAX_PLUS.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_MAX_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_DENTE_LEITE_MAX_PLUS);
			}
			else if (Constantes.CODIGO_PLANO_DENTE_LEITE_PRIME.equals(planoVO.getCodigo())) {
				parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_PRIME);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_DENTE_LEITE_PRIME);
			
			}else if(Constantes.CODIGO_PLANO_MULTI.equals(planoVO.getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_MULTI);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_MULTI);
			
			}else if(Constantes.CODIGO_PLANO_MULTI_PLUS.equals(planoVO.getCodigo())){
				parametros.put("tipo_plano", Constantes.PLANO_MULTI_PLUS);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato",Constantes.TERMO_CONTRATO_MULTI_PLUS);
			
			}else {
				parametros.put("tipo_plano", Constantes.STRING_VAZIA);
				parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
				parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_DENTAL);
			}
			
			reportFile.adicionarRelatorio(new Report("termo_adesao.jrxml", null, parametros));
		}
		
		
		return reportFile;
	}
	
	/**
	 * Metodo responsavel por gerar boleto referente a uma proposta.
	 * 
	 * @param codigoProposta - codigo da proposta.
	 * 
	 * @return ReportFile - PDF com o boleto.
	 */
	public ReportFile gerarBoleto(Long codigoProposta){
		
		PropostaVO proposta = propostaServiceFacade.consultarPorSequencial(codigoProposta);
		
		ReportFile reportFile = new ReportFile();
		reportFile.setFileName("BOLETO-PROPOSTA-" + proposta.getCodigoFormatado());
		
		proposta.setPlanoVO(planoServiceFacade.obterPlanoPorCodigo(proposta.getPlanoVO().getCodigo()));
		
		BoletoVO boleto = GeradorBoletoBancario.gerarBoletoProposta(proposta);
		GeradorBoletoCodigo gerarBoleto = new GeradorBoletoCodigo(boleto);
		
		Map<Object, Object> parametros = Maps.newHashMap();
		
		parametros.put("CODIGO_PROPOSTA", proposta.getCodigoFormatado());
		parametros.put("BOLETO_CODIGO_PROPOSTA", proposta.getCodigoFormatado());
		parametros.put("BOLETO_IMG_BARCODE", gerarBoleto.calcularCodigoBarra());
		parametros.put("BOLETO_VENCIMENTO", boleto.getDataDeVencimento().toString("dd/MM/yyyy"));
		parametros.put("BOLETO_LINHA_DIGITAVEL", gerarBoleto.calcularLinhaDigitavel());
		parametros.put("BOLETO_SACADO", boleto.getSacado());
		parametros.put("BOLETO_NOSSO_NUMERO", boleto.getNossoNumero());
		parametros.put("BOLETO_CODIGO_SUCURSAL", boleto.getCodigoSucursal().toString());
		parametros.put("BOLETO_CODIGO_CORRETOR", boleto.getCodigoCorretor().toString());
		parametros.put("BOLETO_VALOR_DOCUMENTO", boleto.getValorBoleto());
		
		//variaveis que mudam de acordo com o plano selecionado
		if (Constantes.CODIGO_PLANO_EXCLUSIVE.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_EXCLUSIVE);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_EXCLUSIVE_PLUS.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_EXCLUSIVE_PLUS);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_PRIME.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_PRIME);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_PRIME_PLUS.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_PRIME_PLUS);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_MAX.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_MAX);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_MAX_PLUS.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_MAX_PLUS);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_JUNIOR);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
			parametros.put("termo_contrato", Constantes.TERMO_CONTRATO_DENTAL_JUNIOR);
		} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_EXCLUSIVE.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_EXCLUSIVE);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_MAX);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_DENTAL_JUNIOR_MAX_PLUS.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_JUNIOR_MAX_PLUS);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_DENTE_LEITE.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE);
			parametros.put("tipo_plano_logo",Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_DENTE_LEITE_MAX.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_MAX);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_DENTE_LEITE_MAX_PLUS.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_MAX_PLUS);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_DENTE_LEITE_PRIME.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_DENTE_LEITE_PRIME);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_MULTI.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_MULTI);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else if (Constantes.CODIGO_PLANO_MULTI_PLUS.equals(proposta.getPlanoVO().getCodigo())) {
			parametros.put("tipo_plano", Constantes.PLANO_MULTI_PLUS);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		} else {
			parametros.put("tipo_plano", Constantes.STRING_VAZIA);
			parametros.put("tipo_plano_logo", Constantes.TIPO_PLANO_LOGO_NORMAL);
		}
		
		
		reportFile.adicionarRelatorio(new Report("boleto.jrxml", proposta, parametros));
		
		return reportFile;
	}

}