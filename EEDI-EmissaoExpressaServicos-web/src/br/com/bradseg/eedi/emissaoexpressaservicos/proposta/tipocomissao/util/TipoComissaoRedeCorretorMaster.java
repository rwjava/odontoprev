package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.tipocomissao.util;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.TipoCobranca;

public class TipoComissaoRedeCorretorMaster implements TipoComissao {
	
		
		//@Override
		public Integer obterTipoComissao(Integer tipoCobranca, boolean corretorNovaComissao) {
			Integer tipoComissao = null;

			if (TipoCobranca.MENSAL.getCodigo().equals(tipoCobranca)) {
				tipoComissao = ConstantesTipoComissao.COMISSAO_MENSAL_REDE_CORRETOR_MASTER;
			} else if (TipoCobranca.ANUAL.getCodigo().equals(tipoCobranca)) {
				tipoComissao = ConstantesTipoComissao.COMISSAO_ANUAL_REDE_CORRETOR_MASTER;
			}
			return tipoComissao;
		}

}
