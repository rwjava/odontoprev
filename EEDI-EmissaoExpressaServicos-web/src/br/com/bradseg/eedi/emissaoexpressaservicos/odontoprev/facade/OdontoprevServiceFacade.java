package br.com.bradseg.eedi.emissaoexpressaservicos.odontoprev.facade;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaCorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.PropostaVO;

/**
 * Interface responsavel por disponibilizar os metodos de neg�cio referentes a odontoprev.
 */
public interface OdontoprevServiceFacade {


	public void cancelarDebitoPrimeiraParcelaCartaoDeCredito(PropostaVO proposta);
	

	public void debitarPrimeiraParcelaCartaoDeCredito(PropostaCorretorVO proposta);
	public void validarDadosCartaoCredito(PropostaCorretorVO proposta);
	

}
