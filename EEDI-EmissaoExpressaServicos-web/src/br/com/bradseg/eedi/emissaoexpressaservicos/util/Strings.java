package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import br.com.bradseg.bsad.framework.core.text.formatter.MaskFormatter;

/**
 * M�todos est�ticos utilit�rios para trabalhar com Strings ou cole��es de Strings
 * @author WDEV
 */
public class Strings {

	/**
	 * Normaliza o CEP colocando tamanho 8 e preenchendo com zeros a esquerda se necess�rio
	 * @param cep CEP original
	 * @return CEP normalizado
	 */
	public static String normalizarCep(String cep) {
		if (StringUtils.isBlank(cep)) {
			return null;
		}
		return StringUtils.leftPad(removerCaracteresNaoNumericos(cep), 8, '0');
	}

	/**
	 * Normaliza o CPF mantendo-o com tamanho 11, removendo a formata��o e com zeros a esquerda se necess�rio
	 * 
	 * @param cpf CPF original
	 * @return CPF normalizado
	 */
	public static String normalizarCPF(String cpf) {
		if (StringUtils.isNotBlank(cpf)) {
			MaskFormatter formatter = new MaskFormatter("###.###.###-##");
			formatter.setValueContainsLiteralCharacters(false);
			return formatter.valueToString(cpfComDigitosCompletos(cpf));
		}
		return null;
	}

	/**
	 * Desnormaliza o CPF removendo toda a formata��o, mantendo apenas os n�meros
	 * 
	 * @param cpf CPF original
	 * @return CPF normalizado
	 */
	public static String desnormalizarCPF(String cpf) {
		if (StringUtils.isNotBlank(cpf)) {
			return cpfComDigitosCompletos(cpf);
		}
		return null;
	}

	/**
	 * Desnormaliza o CPF removendo toda a formata��o, mantendo apenas os n�meros
	 * 
	 * @param cpf CPF original
	 * @return CPF normalizado
	 */
	public static Long desnormalizarCPFParaNumero(String cpf) {
		if (StringUtils.isNotBlank(cpf)) {
			return Long.valueOf(removerCaracteresNaoNumericos(cpf));
		}
		return null;
	}

	/**
	 * Remove os caracteres n�o num�ricos do cpf e preenche com zeros a esquerda se necess�rio
	 * @param cpf CPF original
	 * @return CPF apenas com caracteres num�ricos e com zeros a esquerda se necess�rio
	 */
	protected static String cpfComDigitosCompletos(String cpf) {
		return StringUtils.leftPad(removerCaracteresNaoNumericos(cpf), 11, '0');
	}

	/**
	 * Normaliza o CPF mantendo-o com tamanho 11, removendo a formata��o, colocando zeros a esquerda se necess�rio e colocando null se for menor ou igual a zero
	 * 
	 * @param cpf CPF original
	 * @return CPF normalizado
	 */
	public static String normalizarCPFMaiorQueZero(String cpf) {
		if (StringUtils.isNotBlank(cpf) && isMaiorQueZero(Long.valueOf(cpf))) {
			return normalizarCPF(cpf);
		}
		return null;
	}

	/**
	 * Verifica se o cpf informado � maior que zero
	 * 
	 * @param cpf CPF a ser verificado
	 * @return true ou false
	 */
	private static boolean isMaiorQueZero(Long cpf) {
		return Long.valueOf(0).compareTo(cpf) < 0;
	}

	/**
	 * Normaliza o CNPJ mantendo-o com tamanho 14 e com zeros a esquerda se necess�rio
	 * 
	 * @param cnpj cnpj original
	 * @return cnpj normalizado
	 */
	public static String normalizarCNPJ(String cnpj) {
		if (cnpj == null) {
			return null;
		}
		return StringUtils.leftPad(removerCaracteresNaoNumericos(cnpj), 14, '0');
	}

	public static String removerCaracteresNaoNumericos(String texto) {
		return texto.replaceAll("[^\\d]", "");
	}

	/**
	 * Verifica se o campo que foi informado � um cpf ou cnpj, retornando ele normalizado
	 * 
	 * @param cpfCnpj cpfCnpj original
	 * @return cpfCnpj normalizado
	 */
	public static String normalizarCpfCnpj(String cpfCnpj) {
		if (StringUtils.isBlank(cpfCnpj)) {
			return null;
		}

		if (cpfCnpj.length() <= 11) {
			return normalizarCPF(cpfCnpj);
		} else {
			return normalizarCNPJ(cpfCnpj);
		}
	}

	/**
	 * A partir de uma lista de Strings retorna um Set com os valores duplicados dessa lista.
	 * 
	 * @param lista Lista completa com as Strings
	 * @return Set com as strings duplicadas
	 */
	public static Set<String> encontrarDuplicados(List<String> lista) {
		List<String> copia = new ArrayList<String>(lista);
		for (String value : new HashSet<String>(lista)) {
			copia.remove(value);
		}
		return new HashSet<String>(copia);
	}

	public static String normalizarTelefone(String telefone) {
		if (StringUtils.isBlank(telefone)) {
			return null;
		}
		return removerCaracteresNaoNumericos(telefone);
	}

	public static String maiusculas(String texto) {
		if (StringUtils.isBlank(texto)) {
			return null;
		}
		return texto.toUpperCase();
	}

}