package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;

/**
 * Utilizada para gerar digito verificador para o codigo de barras do boleto banc�rio 
 * 
 * @author WDEV
 */
public class Modulo11 {

	/**
	 * Gera o d�gito verificador de um valor n�merico.
	 * 
	 * @param valorSemDigitoVerificador Valor em string contendo apenas n�meros. N�o pode ser nulo.
	 * @return D�gito verificador 
	 */
	public static Integer gerarDigitoVerificador(String valorSemDigitoVerificador) {
		Preconditions.checkNotNull(valorSemDigitoVerificador);
		if (StringUtils.isEmpty(valorSemDigitoVerificador)) {
			throw new IllegalArgumentException("O valor n�o pode estar vazio");
		}
		int i = 2;
		int soma = 0;
		for (char c : StringUtils.reverse(valorSemDigitoVerificador).toCharArray()) {
			soma += Character.digit(c, 10) * i;
			i++;
			if (i == 10) {
				i = 2;
			}
		}

		int digito = 11 - (soma % 11);
		if (digito > 9) {
			digito = 1;
		}
		return digito;
	}
}