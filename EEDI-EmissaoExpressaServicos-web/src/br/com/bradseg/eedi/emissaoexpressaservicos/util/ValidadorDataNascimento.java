package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import org.joda.time.LocalDate;

/**
 * Verifica se a data de nascimento � v�lida de acordo com as regras definidas
 * @author WDEV
 */
public class ValidadorDataNascimento {

	/**
	 * Verifica se a data de nascimento � v�lida. Para ser v�lido, a data de nascimento precisa seguir as seguintes regras:
	 * 	- Ser no m�ximo no dia atual;
	 * 	- Possuir no m�nimo 150 anos a partir de hoje;
	 * @param dataNascimento Data de nascimento
	 * @return verdadeiro se a data for v�lida
	 */
	public boolean validar(LocalDate dataNascimento) {
		if (dataNascimento == null) {
			return false;
		}

		LocalDate dataAtual = new LocalDate();
		return dataNascimento.isBefore(dataAtual.plusDays(1)) && dataNascimento.isAfter(dataAtual.minusYears(Constantes.ANO_MINIMO_PERMITIDO_DATA_NASCIMENTO_A_PARTIR_DATA_ATUAL));
	}
}