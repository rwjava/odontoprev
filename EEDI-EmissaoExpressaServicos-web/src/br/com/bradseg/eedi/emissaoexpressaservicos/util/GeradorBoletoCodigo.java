package br.com.bradseg.eedi.emissaoexpressaservicos.util;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.BoletoVO;



public class GeradorBoletoCodigo  {

	private String numeroBanco = "000";
	private String moeda = "9";
	private String campoLivre = "0";
	//private BigDecimal valorBoleto = BigDecimal.ZERO;
	
	protected String numeroAgenciaOdonto = "2372";
	protected String codigoCarteira = "22";
	protected String codigoCarteiraFirst = "2";
	protected String ccOdontoPrev = "0008162";
	protected String dataVencimento = "0000";
	
	//@Autowired
	private BoletoVO boleto;
	
	public GeradorBoletoCodigo(BoletoVO boleto){
		this.boleto = boleto;
	}
	
	public String calcularLinhaDigitavel() {
		return new CalculadorLinhaDigitavel().calcularLinhaDigitalComMascara();
	}

	public String calcularCodigoBarra() {
		return new CalculadorCodigoBarra().calcularCodigoBarra();
	}

	public String getCalcularFator() {
		return new CalculadorLinhaDigitavel().calcularFatorLinhaDigital();
	}


	protected class CalculadorLinhaDigitavel {
		
		private final Pattern PADRAO = Pattern.compile("(\\d{5})(\\d{5})(\\d{5})(\\d{6})(\\d{5})(\\d{6})(\\d{1})(\\d{14})");

		public String calcularLinhaDigitalComMascara() {
			String linhaDigitavelSemMascara = calcularLinhaDigitavelComFator();
			Matcher matcher = PADRAO.matcher(linhaDigitavelSemMascara);
			if (matcher.find()) {
				return matcher.group(1) + "." + matcher.group(2) + " " + matcher.group(3) + "." + matcher.group(4) + " " + matcher.group(5) + "." + matcher.group(6) + " " + matcher.group(7) + " " + matcher.group(8);
			}
			return null;
		}

		public String calcularFatorLinhaDigital() {
			
			if (boleto.getValorBoleto().compareTo(BigDecimal.ZERO) == 0) {
				return dataVencimento;
			}
			Calendar vencimento = Calendar.getInstance();
			vencimento.set(Calendar.DATE, vencimento.get(Calendar.DATE) + 1);
			Calendar dataBase = Calendar.getInstance();
			dataBase.set(1997, 9, 7);
			long qtdmillis = vencimento.getTimeInMillis() - dataBase.getTimeInMillis();
			long qtddias = qtdmillis / (24 * 60 * 60 * 1000);
			return String.valueOf(Math.abs(qtddias));
		}

		public String calcularLinhaDigitavel() {
			StringBuilder sb = new StringBuilder();
			String primeiraParte = calcularPrimeiraParteLinhaDigitavel();
			String segundaParte = calcularSegundaParteLinhaDigitavel();
			String digitoVerificadorCodigoBarra = new CalculadorCodigoBarra().calcularDigitoVerificadorCodigoBarra();
			sb.append(primeiraParte);
			sb.append(digitoVerificadorCodigoBarra);
			sb.append(segundaParte);
			return sb.toString();
		}

		public String calcularLinhaDigitavelComFator() {
			StringBuilder sb = new StringBuilder();
			String primeiraParte = calcularPrimeiraParteLinhaDigitavel();
			String segundaParte = calcularSegundaParteLinhaDigitavelComFatorDeVencimento();
			String digitoVerificadorCodigoBarra = new CalculadorCodigoBarra().calcularDigitoVerificadorCodigoBarra();
			sb.append(primeiraParte);
			sb.append(digitoVerificadorCodigoBarra);
			sb.append(segundaParte);
			return sb.toString();
		}

		protected String calcularPrimeiraParteLinhaDigitavel() {
			StringBuilder sb = new StringBuilder();
			sb.append(calcularPrimeiroBlocoLinhaDigitavel());
			sb.append(calcularSegundoBlocoLinhaDigitavel());
			sb.append(calcularTerceiroBlocoLinhaDigitavel());
			return sb.toString();
		}

		protected String calcularSegundaParteLinhaDigitavel() {
			return new StringBuilder().append(dataVencimento).append(boleto.getValorBoletoAsString()).toString();
		}

		protected String calcularSegundaParteLinhaDigitavelComFatorDeVencimento() {
			return new StringBuilder().append(getCalcularFator()).append(boleto.getValorBoletoAsString()).toString();
		}

		protected String calcularPrimeiroBlocoLinhaDigitavel() {
			String bloco = new StringBuilder().append(numeroBanco).append(moeda).append(numeroAgenciaOdonto)
					.append(codigoCarteiraFirst).toString();
			return bloco + Modulo10.gerarDigitoVerificador(bloco);
		}

		protected String calcularSegundoBlocoLinhaDigitavel() {
			String bloco = new StringBuilder().append(codigoCarteiraFirst)
					.append(boleto.getNumeroPropostaAsString().substring(0, 9)).toString();
			return bloco + Modulo10.gerarDigitoVerificador(bloco);
		}

		protected String calcularTerceiroBlocoLinhaDigitavel() {
			String bloco = new StringBuilder().append(boleto.getNumeroPropostaAsString().substring(9, 11)).append(ccOdontoPrev).append(campoLivre).toString();
			return bloco + Modulo10.gerarDigitoVerificador(bloco);
		}
	}// Fim da Classe CalculadorLinhaDigitavel.

	protected class CalculadorCodigoBarra {// Inicio da classe CalculadorCodigoBarra
		
		public String calcularCodigoBarra() {
			String primeiroBloco = calcularPrimeiroBlocoCodigoBarra();
			String segundoBloco = calcularSegundoBlocoCodigoBarra();
			String terceiroBloco = calcularTerceiroBlocoCodigoBarra();
			String digitoVerificador = calcularDigitoVerificadorCodigoBarra();
			return primeiroBloco + digitoVerificador + segundoBloco + terceiroBloco;
		}

		public String calcularDigitoVerificadorCodigoBarra() {
			String primeiroBloco = calcularPrimeiroBlocoCodigoBarra();
			String segundoBloco = calcularSegundoBlocoCodigoBarra();
			String terceiroBloco = calcularTerceiroBlocoCodigoBarra();
			Integer digitoVerificador = Modulo11BoletoBradesco.gerarDigitoVerificador(primeiroBloco + segundoBloco
					+ terceiroBloco);
			return digitoVerificador.toString();
		}

		public String calcularPrimeiroBlocoCodigoBarra() {
			StringBuilder sb = new StringBuilder();
			sb.append(numeroBanco).append(moeda);
			return sb.toString();
		}

		public String calcularSegundoBlocoCodigoBarra() {
			StringBuilder sb = new StringBuilder();
			sb.append(getCalcularFator()).append(boleto.getValorBoletoAsString());
			return sb.toString();
		}

		public String calcularTerceiroBlocoCodigoBarra() {
			StringBuilder sb = new StringBuilder();
			sb.append(numeroAgenciaOdonto).append(getCodigoCarteiraCom2Posicoes()).append(boleto.getNumeroPropostaAsString())
					.append(ccOdontoPrev).append("0");
			return sb.toString();
		}

		private String getCodigoCarteiraCom2Posicoes() {
			return StringUtils.leftPad(codigoCarteira, 2, "0");
		}
	}

}
