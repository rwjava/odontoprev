package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

/**
 * Filtro para consulta de proposta
 * 
 * @author WDEV
 */
public class FiltroPropostaVO implements Serializable {

	private static final long serialVersionUID = -5815967881317315373L;

	private String codigoProposta;
	private SituacaoProposta situacaoProposta;
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private String nomeProponente;
	private TipoProdutor tipoProdutor;
	private CorretorVO corretor;
	private String cpfProponente;
	private CanalVendaVO canalVenda;

	public SituacaoProposta getSituacaoProposta() {
		return situacaoProposta;
	}

	public void setSituacaoProposta(SituacaoProposta situacaoProposta) {
		this.situacaoProposta = situacaoProposta;
	}

	@XmlJavaTypeAdapter(type = org.joda.time.LocalDate.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDateAdapter.class)
	public LocalDate getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	@XmlJavaTypeAdapter(type = org.joda.time.LocalDate.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDateAdapter.class)
	public LocalDate getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}

	public String getNomeProponente() {
		return nomeProponente;
	}

	public void setNomeProponente(String nomeProponente) {
		this.nomeProponente = nomeProponente;
	}

	public TipoProdutor getTipoProdutor() {
		return tipoProdutor;
	}

	public void setTipoProdutor(TipoProdutor tipoProdutor) {
		this.tipoProdutor = tipoProdutor;
	}

	public String getCodigoProposta() {
		return codigoProposta;
	}

	public void setCodigoProposta(String codigoProposta) {
		this.codigoProposta = codigoProposta;
	}

	public CorretorVO getCorretor() {
		return corretor;
	}

	public void setCorretor(CorretorVO corretor) {
		this.corretor = corretor;
	}

	public String getCpfProponente() {
		return cpfProponente;
	}

	public void setCpfProponente(String cpfProponente) {
		this.cpfProponente = cpfProponente;
	}

	public CanalVendaVO getCanalVenda() {
		return canalVenda;
	}

	public void setCanalVenda(CanalVendaVO canalVenda) {
		this.canalVenda = canalVenda;
	}

	public boolean isNomeProponentePreenchido() {
		return StringUtils.isNotBlank(getNomeProponente());
	}

	public boolean isCodigoPropostaPreenchido() {
		return StringUtils.isNotBlank(getCodigoProposta());
	}

	public boolean isPeriodoPreenchido() {
		return isDataInicioPreenchida() && isDataFimPreenchida();
	}

	public boolean isDataFimPreenchida() {
		return dataFim != null;
	}

	public boolean isDataInicioPreenchida() {
		return dataInicio != null;
	}

	public boolean isSituacaoPreenchida() {
		return situacaoProposta != null;
	}

	public boolean isSucursalCorretorPreenchida() {
		return isCorretorPreenchido() && getCorretor().getSucursalSeguradora() != null && getCorretor().getSucursalSeguradora().isCodigoPreenchido();
	}

	public boolean isCpdCorretorPreenchido() {
		return isCorretorPreenchido() && corretor.getCpd() != null;
	}

	public boolean isCpfCnpjCorretorPreenchido() {
		return isCorretorPreenchido() && StringUtils.isNotBlank(corretor.getCpfCnpj());
	}

	public boolean isCorretorPreenchido() {
		return getCorretor() != null;
	}

	public boolean isCpfProponentePreenchido() {
		return StringUtils.isNotBlank(cpfProponente);
	}

}