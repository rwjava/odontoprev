package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;

import com.google.common.base.Objects;

/**
 * Dados da sucursal seguradora
 * 
 * @author WDEV
 */
public class SucursalSeguradoraVO implements Serializable {

	private static final long serialVersionUID = -5141630086465256256L;

	private Integer codigo;

	private String descricao;

	private TipoSucursalSeguradora tipo;

	public SucursalSeguradoraVO() {
		// Construtor Default
		super();
	}

	public SucursalSeguradoraVO(Integer codigo) {
		this.codigo = codigo;
		tipo = new DefinicaoSucursal(codigo).getTipo();
	}

	public boolean isEmissora() {
		return TipoSucursalSeguradora.EMISSORA.equals(tipo);
	}

	public boolean isBVP() {
		return TipoSucursalSeguradora.BVP.equals(tipo);
	}
	
	public boolean isCorporate() {
		return TipoSucursalSeguradora.CORPORATE.equals(tipo);
	}

	public boolean isValida() {
		return isBVP() || isEmissora() || isCorporate();
	}

	public TipoSucursalSeguradora getTipo() {
		return tipo;
	}

	public void setTipo(TipoSucursalSeguradora tipo) {
		this.tipo = tipo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int hashCode() {
		return Objects.hashCode(codigo);
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SucursalSeguradoraVO other = (SucursalSeguradoraVO) obj;
		return Objects.equal(codigo, other.codigo);
	}

	/**
	 * Defini��o da sucursal, como tipo (bvp ou emissora)
	 * @author WDEV
	 */
	private final static class DefinicaoSucursal {
		private TipoSucursalSeguradora tipo;

		public DefinicaoSucursal(Integer codigo) {
			if (codigo != null) {
				if ((Constantes.CODIGO_INICIAL_SUCURSAL_EMISSORA.compareTo(codigo) <= 0 && Constantes.CODIGO_FINAL_SUCURSAL_EMISSORA.compareTo(codigo) >= 0) || (Constantes.SUCURSAL_EMISSORA_CAMPINAS_SAUDE.equals(codigo))) {
					tipo = TipoSucursalSeguradora.EMISSORA;
				} else if (Constantes.CODIGO_INICIAL_SUCURSAL_BVP.compareTo(codigo) <= 0 && Constantes.CODIGO_FINAL_SUCURSAL_BVP.compareTo(codigo) >= 0) {
					tipo = TipoSucursalSeguradora.BVP;
				}
			}
		}

		public TipoSucursalSeguradora getTipo() {
			return tipo;
		}
	}

	public boolean isCodigoPreenchido() {
		return codigo != null;
	}

	@Override
	public String toString() {
		return "SucursalSeguradoraVO [codigo=" + codigo + ", descricao=" + descricao + ", tipo=" + tipo + "]";
	}

	
	
}