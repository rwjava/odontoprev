package br.com.bradseg.eedi.emissaoexpressaservicos.vo;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.joda.time.DateTime;

public class AcompanhamentoPropostaVO implements Serializable {

	private static final long serialVersionUID = -1028444040988287475L;

	private String inicioPeriodo;
	private String fimPeriodo;
	private String codProposta;
	private String cpfTitularConta;
	private String nomeTitularConta;
	private String cpfResponsavel;
	private String nomeResponsavel;
	private String cpfBenificiarioTitular;
	private String nomeBenificiarioTitular;
	private Long sucursal;
	private String cpdCorretor;
	private Long codigoAgenciaDebito;
	private Long codigoAgenciaProdutora;
	private String nomeAgenciaProdutora;
	private Long codigoAssistenteBS;
	private String motivoStatusProposta;
	private String agenciaDebito;
	private String nomeAgenciaDebito;
	private String nomeCorretor;
	private String cpfCnpjCorretor;
	private String cpdAngariador;
	private String cpfCnpjAngariador;
	private String nomeAngariador;
	private String cpdCorretorMaster;
	private String cpfCnpjCorretorMaster;
	private String nomeCorretorMaster;
	private Long codigoGerenteProdutoBVP;
	private BigDecimal valorTotalProposta;
	private String quantidadeVidasProposta;
	private Date dataCriacaoPropostaMenor;
	private Date dataCriacaoPropostaMaior;
	private String dataInicioStatusAtualProposta;
	private String dataInicioCriacaoCodigo;
	private String statusAtualProposta;
	private Integer codigoStatusAtualProposta;
	private Integer situacaoProposta;
	private TipoProdutor tipoProdutor;
	private Long codigoCanalVenda;
	private String protocolo;
	private Long codigoPlano;
	private DateTime dataCriacaoProposta;
	private Long codigoPostoAtendimento;

	private TipoCobranca tipoCobranca;

	public void setDataInicioCriacaoCodigo(String dataInicioCriacaoCodigo) {
		this.dataInicioCriacaoCodigo = dataInicioCriacaoCodigo;
	}

	public String getDataInicioCriacaoCodigo() {
		return dataInicioCriacaoCodigo;
	}

	public void setTipoProdutor(TipoProdutor tipoProdutor) {
		this.tipoProdutor = tipoProdutor;
	}

	public TipoProdutor getTipoProdutor() {
		return tipoProdutor;
	}

	public Integer getSituacaoProposta() {
		return situacaoProposta;
	}

	public void setSituacaoProposta(Integer situacaoProposta) {
		this.situacaoProposta = situacaoProposta;
	}

	public String getNomeAgenciaProdutora() {
		return nomeAgenciaProdutora;
	}

	public void setNomeAgenciaProdutora(String nomeAgenciaProdutora) {
		this.nomeAgenciaProdutora = nomeAgenciaProdutora;
	}

	public String getCpfTitularConta() {
		return cpfTitularConta;
	}

	public String getNomeTitularConta() {
		return nomeTitularConta;
	}

	public String getCpfResponsavel() {
		return cpfResponsavel;
	}

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public String getCpfBenificiarioTitular() {
		return cpfBenificiarioTitular;
	}

	public String getNomeBenificiarioTitular() {
		return nomeBenificiarioTitular;
	}

	public Long getSucursal() {
		return sucursal;
	}

	public void setCpfTitularConta(String cpfTitularConta) {
		this.cpfTitularConta = cpfTitularConta;
	}

	public void setNomeTitularConta(String nomeTitularConta) {
		this.nomeTitularConta = nomeTitularConta;
	}

	public void setCpfResponsavel(String cpfResponsavel) {
		this.cpfResponsavel = cpfResponsavel;
	}

	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public void setCpfBenificiarioTitular(String cpfBenificiarioTitular) {
		this.cpfBenificiarioTitular = cpfBenificiarioTitular;
	}

	public void setNomeBenificiarioTitular(String nomeBenificiarioTitular) {
		this.nomeBenificiarioTitular = nomeBenificiarioTitular;
	}

	public void setSucursal(Long sucursal) {
		this.sucursal = sucursal;
	}

	public String getInicioPeriodo() {
		return inicioPeriodo;
	}

	public String getFimPeriodo() {
		return fimPeriodo;
	}

	public String getCpdCorretor() {
		return cpdCorretor;
	}

	public Long getCodigoAgenciaDebito() {
		return codigoAgenciaDebito;
	}

	public Long getCodigoAgenciaProdutora() {
		return codigoAgenciaProdutora;
	}

	public Long getCodigoAssistenteBS() {
		return codigoAssistenteBS;
	}

	public String getMotivoStatusProposta() {
		return motivoStatusProposta;
	}

	public void setInicioPeriodo(String inicioPeriodo) {
		this.inicioPeriodo = inicioPeriodo;
	}

	public void setFimPeriodo(String fimPeriodo) {
		this.fimPeriodo = fimPeriodo;
	}

	public void setCpdCorretor(String cpdCorretor) {
		this.cpdCorretor = cpdCorretor;
	}

	public void setCodigoAgenciaDebito(Long codigoAgenciaDebito) {
		this.codigoAgenciaDebito = codigoAgenciaDebito;
	}

	public void setCodigoAgenciaProdutora(Long codigoAgenciaProdutora) {
		this.codigoAgenciaProdutora = codigoAgenciaProdutora;
	}

	public void setCodigoAssistenteBS(Long codigoAssistenteBS) {
		this.codigoAssistenteBS = codigoAssistenteBS;
	}

	public void setMotivoStatusProposta(String motivoStatusProposta) {
		this.motivoStatusProposta = motivoStatusProposta;
	}

	public String getCodProposta() {
		return codProposta;
	}

	public void setCodProposta(String codProposta) {
		this.codProposta = codProposta;
	}

	public String getAgenciaDebito() {
		return agenciaDebito;
	}

	public void setAgenciaDebito(String agenciaDebito) {
		this.agenciaDebito = agenciaDebito;
	}

	public String getNomeAgenciaDebito() {
		return nomeAgenciaDebito;
	}

	public void setNomeAgenciaDebito(String nomeAgenciaDebito) {
		this.nomeAgenciaDebito = nomeAgenciaDebito;
	}

	public String getNomeCorretor() {
		return nomeCorretor;
	}

	public void setNomeCorretor(String nomeCorretor) {
		this.nomeCorretor = nomeCorretor;
	}

	public String getCpdAngariador() {
		return cpdAngariador;
	}

	public void setCpdAngariador(String cpdAngariador) {
		this.cpdAngariador = cpdAngariador;
	}

	public String getCpfCnpjAngariador() {
		return cpfCnpjAngariador;
	}

	public void setCpfCnpjAngariador(String cpfCnpjAngariador) {
		this.cpfCnpjAngariador = cpfCnpjAngariador;
	}

	public String getNomeAngariador() {
		return nomeAngariador;
	}

	public void setNomeAngariador(String nomeAngariador) {
		this.nomeAngariador = nomeAngariador;
	}

	public String getCpdCorretorMaster() {
		return cpdCorretorMaster;
	}

	public void setCpdCorretorMaster(String cpdCorretorMaster) {
		this.cpdCorretorMaster = cpdCorretorMaster;
	}

	public String getCpfCnpjCorretorMaster() {
		return cpfCnpjCorretorMaster;
	}

	public void setCpfCnpjCorretorMaster(String cpfCnpjCorretorMaster) {
		this.cpfCnpjCorretorMaster = cpfCnpjCorretorMaster;
	}

	public String getNomeCorretorMaster() {
		return nomeCorretorMaster;
	}

	public void setNomeCorretorMaster(String nomeCorretorMaster) {
		this.nomeCorretorMaster = nomeCorretorMaster;
	}

	public BigDecimal getValorTotalProposta() {
		return valorTotalProposta;
	}

	public void setValorTotalProposta(BigDecimal valorTotalProposta) {
		this.valorTotalProposta = valorTotalProposta;
	}

	public String getQuantidadeVidasProposta() {
		return quantidadeVidasProposta;
	}

	public void setQuantidadeVidasProposta(String quantidadeVidasProposta) {
		this.quantidadeVidasProposta = quantidadeVidasProposta;
	}

	public String getDataInicioStatusAtualProposta() {
		return dataInicioStatusAtualProposta;
	}

	public void setDataInicioStatusAtualProposta(String dataInicioStatusAtualProposta) {
		this.dataInicioStatusAtualProposta = dataInicioStatusAtualProposta;
	}

	public String getStatusAtualProposta() {
		return statusAtualProposta;
	}

	public void setStatusAtualProposta(String statusAtualProposta) {
		this.statusAtualProposta = statusAtualProposta;
	}

	public Long getCodigoGerenteProdutoBVP() {
		return codigoGerenteProdutoBVP;
	}

	public void setCodigoGerenteProdutoBVP(Long codigoGerenteProdutoBVP) {
		this.codigoGerenteProdutoBVP = codigoGerenteProdutoBVP;
	}

	public Date getDataCriacaoPropostaMenor() {
		return dataCriacaoPropostaMenor;
	}

	public void setDataCriacaoPropostaMenor(Date dataCriacaoPropostaMenor) {
		this.dataCriacaoPropostaMenor = dataCriacaoPropostaMenor;
	}

	public Date getDataCriacaoPropostaMaior() {
		return dataCriacaoPropostaMaior;
	}

	public void setDataCriacaoPropostaMaior(Date dataCriacaoPropostaMaior) {
		this.dataCriacaoPropostaMaior = dataCriacaoPropostaMaior;
	}

	public Integer getCodigoStatusAtualProposta() {
		return codigoStatusAtualProposta;
	}

	public void setCodigoStatusAtualProposta(Integer codigoStatusAtualProposta) {
		this.codigoStatusAtualProposta = codigoStatusAtualProposta;
	}

	public String getCpfCnpjCorretor() {
		return cpfCnpjCorretor;
	}

	public void setCpfCnpjCorretor(String cpfCnpjCorretor) {
		this.cpfCnpjCorretor = cpfCnpjCorretor;
	}

	public TipoCobranca getTipoCobranca() {
		return tipoCobranca;
	}

	public void setTipoCobranca(TipoCobranca tipoCobranca) {
		this.tipoCobranca = tipoCobranca;
	}

	public Long getCodigoCanalVenda() {
		return codigoCanalVenda;
	}

	public void setCodigoCanalVenda(Long codigoCanalVenda) {
		this.codigoCanalVenda = codigoCanalVenda;
	}

	/**
	 * Retorna codigoPlano.
	 *
	 * @return codigoPlano - codigoPlano.
	 */
	public Long getCodigoPlano() {
		return codigoPlano;
	}

	/**
	 * Especifica codigoPlano.
	 *
	 * @param codigoPlano - codigoPlano.
	 */
	public void setCodigoPlano(Long codigoPlano) {
		this.codigoPlano = codigoPlano;
	}

	/**
	 * Retorna dataCriacaoProposta.
	 *
	 * @return dataCriacaoProposta - dataCriacaoProposta.
	 */
	public DateTime getDataCriacaoProposta() {
		return dataCriacaoProposta;
	}

	/**
	 * Especifica dataCriacaoProposta.
	 *
	 * @param dataCriacaoProposta - dataCriacaoProposta.
	 */
	public void setDataCriacaoProposta(DateTime dataCriacaoProposta) {
		this.dataCriacaoProposta = dataCriacaoProposta;
	}

	/**
	 * Retorna protocolo.
	 *
	 * @return protocolo - protocolo
	 */
	public String getProtocolo() {
		return protocolo;
	}

	/**
	 * Especifica protocolo.
	 *
	 * @param protocolo - protocolo
	 */
	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

	public Long getCodigoPostoAtendimento() {
		return codigoPostoAtendimento;
	}

	public void setCodigoPostoAtendimento(Long codigoPostoAtendimento) {
		this.codigoPostoAtendimento = codigoPostoAtendimento;
	}
	
	

}
