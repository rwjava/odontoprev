package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;

public class BoletoVO implements Serializable {

	private static final long serialVersionUID = -6273804794684569107L;

	private Long numeroProposta;
	private Integer codigoSucursal;
	private String codigoProposta;
	private Integer codigoCorretor;
	//private String numeroBanco = "000";
	//private String moeda = "9";
	//private String campoLivre = "0";
	private BigDecimal valorBoleto = BigDecimal.ZERO;
	private String sacado;
	
	protected String numeroAgenciaOdonto = "2372";
	protected String codigoCarteira = "22";
	protected String codigoCarteiraFirst = "2";
	protected String ccOdontoPrev = "0008162";
	protected String dataVencimento = "0000";

	private String codigoBarraCompleto;
	/**
	 * @return the codigoBarraCompleto
	 */
	public String getCodigoBarraCompleto() {
		return codigoBarraCompleto;
	}

	/**
	 * @param codigoBarraCompleto the codigoBarraCompleto to set
	 */
	public void setCodigoBarraCompleto(String codigoBarraCompleto) {
		this.codigoBarraCompleto = codigoBarraCompleto;
	}

	/**
	 * Retorna dataDeVencimento.
	 *
	 * @return dataDeVencimento - dataDeVencimento.
	 */
	public LocalDate getDataDeVencimento() {
		return new LocalDate().plusDays(1);
	}

	public String getSacado() {
		return sacado;
	}

	public void setSacado(String sacado) {
		this.sacado = sacado;
	}

	public void setNumeroProposta(Long numeroProposta) {
		this.numeroProposta = numeroProposta;
	}

	public Long getNumeroProposta() {
		return numeroProposta;
	}

	public BigDecimal getValorBoleto() {
		return valorBoleto;
	}

	public String getNossoNumero() {
		return getNumeroPropostaAsString();
	}

	public Integer getCodigoCorretor() {
		return codigoCorretor;
	}

	public Integer getCodigoSucursal() {
		return codigoSucursal;
	}

	public String getCodigoProposta() {
		return codigoProposta;
	}

	public void setCodigoCorretor(Integer codigoCorretor) {
		this.codigoCorretor = codigoCorretor;
	}

	public void setCodigoProposta(String codigoProposta) {
		this.codigoProposta = codigoProposta;
	}

	public void setCodigoSucursal(Integer codigoSucursal) {
		this.codigoSucursal = codigoSucursal;
	}

	public String getNumeroPropostaAsString() {
		return StringUtils.leftPad(numeroProposta.toString(), 11, "0");
	}

	public String getValorBoletoAsString() {

		return StringUtils.leftPad(String.format("%.2f", valorBoleto).replaceAll("[^\\d]", ""), 10, "0");
	}

	public void setValorBoleto(BigDecimal valorBoleto) {
		this.valorBoleto = valorBoleto;
	}

}