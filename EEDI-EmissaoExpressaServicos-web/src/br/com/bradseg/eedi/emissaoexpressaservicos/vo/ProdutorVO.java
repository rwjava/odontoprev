package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * Produtor da proposta
 * 
 * @author WDEV
 */
public abstract class ProdutorVO implements Serializable {

	private static final long serialVersionUID = 977750402661103741L;

	private PropostaVO proposta;

	private Integer cpd;

	private TipoProdutor tipo;

	private String nome;

	private String cpfCnpj;

	private SucursalSeguradoraVO sucursalSeguradora;

	public ProdutorVO() {
		// Construtor Default
		super();
	}

	public ProdutorVO(TipoProdutor tipo) {
		this.tipo = tipo;
	}

	public abstract TipoComissao getTipoComissao();

	public PropostaVO getProposta() {
		return proposta;
	}

	public void setProposta(PropostaVO proposta) {
		this.proposta = proposta;
	}

	public Integer getCpd() {
		return cpd;
	}

	public void setCpd(Integer cpd) {
		this.cpd = cpd;
	}

	public TipoProdutor getTipo() {
		return tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public SucursalSeguradoraVO getSucursalSeguradora() {
		return sucursalSeguradora;
	}

	public void setSucursalSeguradora(SucursalSeguradoraVO sucursalSeguradora) {
		this.sucursalSeguradora = sucursalSeguradora;
	}

	public boolean isCpdPreenchido() {
		return getCpd() != null;
	}

	public boolean isSucursalSeguradoraPreenchida() {
		return getSucursalSeguradora() != null;
	}

	public boolean isNomePreenchido() {
		return StringUtils.isNotBlank(getNome());
	}

	@Override
	public String toString() {
		return "ProdutorVO [proposta=" + proposta + ", cpd=" + cpd + ", tipo=" + tipo + ", nome=" + nome + ", cpfCnpj="
				+ cpfCnpj + ", sucursalSeguradora=" + sucursalSeguradora +"]";
	}
	

}