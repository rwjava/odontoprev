package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Representa os movimentos do beneficiário de acordo com a evolução do processo.
 * 
 * @author WDEV
 */
public class MovimentoBeneficiarioVO implements Serializable {

	private static final long serialVersionUID = 237669918410902155L;

	private BeneficiarioVO beneficiario;
	private PropostaVO proposta;
	private SituacaoProposta situacaoProposta;

	public BeneficiarioVO getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(BeneficiarioVO beneficiario) {
		this.beneficiario = beneficiario;
	}

	public PropostaVO getProposta() {
		return proposta;
	}

	public void setProposta(PropostaVO proposta) {
		this.proposta = proposta;
	}

	public SituacaoProposta getSituacaoProposta() {
		return situacaoProposta;
	}

	public void setSituacaoProposta(SituacaoProposta situacaoProposta) {
		this.situacaoProposta = situacaoProposta;
	}

}