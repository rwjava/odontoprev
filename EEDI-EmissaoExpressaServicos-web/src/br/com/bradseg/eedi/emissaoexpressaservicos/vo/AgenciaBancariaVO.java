package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Dados da ag�ncia Banc�ria
 * 
 * @author WDEV
 */
public class AgenciaBancariaVO implements Serializable {

	private static final long serialVersionUID = -7280938146732671436L;

	private Integer codigo;

	private String nome;

	private String digito;

	private BancoVO banco;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public BancoVO getBanco() {
		return banco;
	}

	public void setBanco(BancoVO banco) {
		this.banco = banco;
	}

	public String getDigito() {
		return digito;
	}

	public void setDigito(String digito) {
		this.digito = digito;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isCodigoPreenchido() {
		return codigo != null;
	}

	@Override
	public String toString() {
		return "AgenciaBancariaVO [codigo=" + codigo + ", nome=" + nome + ", digito=" + digito + ", banco=" + banco
				+ "]";
	}
	
	
}