package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;

/**
 * Produtor angariador da proposta
 * 
 * @author WDEV
 */
public class AngariadorVO extends ProdutorVO implements Serializable {

	private static final long serialVersionUID = -5322479176575745775L;

	public AngariadorVO() {
		super(TipoProdutor.ANGARIADOR);
	}

	public TipoComissao getTipoComissao() {
		if (getCpd() != null && Constantes.CPD_CORRETOR_COMISSAO_TIPO_4.equals(getCpd())) {
			return TipoComissao.COMISSAO_TIPO4;
		}
		return TipoComissao.COMISSAO_NORMAL;
	}

	@Override
	public String toString() {
		return "AngariadorVO [getTipoComissao()=" + getTipoComissao() + "]";
	}

	
}