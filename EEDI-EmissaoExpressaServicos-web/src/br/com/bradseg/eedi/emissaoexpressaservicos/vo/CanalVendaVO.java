package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Informa��es do canal de venda da emiss�o da proposta
 * @author WDEV
 */
public class CanalVendaVO implements Serializable {

	private static final long serialVersionUID = -2261446276137904451L;

	private Integer codigo;
	private String sigla;
	private String nome;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "CanalVendaVO [codigo=" + codigo + ", sigla=" + sigla + ", nome=" + nome + "]";
	}

	
}