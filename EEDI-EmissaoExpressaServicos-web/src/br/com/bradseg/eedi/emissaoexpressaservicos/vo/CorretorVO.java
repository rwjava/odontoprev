package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import com.google.common.base.Objects;

/**
 * Produtor corretor da proposta
 * 
 * @author WDEV
 */
public class CorretorVO extends ProdutorVO implements Serializable {

	private static final long serialVersionUID = -6079461859203547294L;
	
	public CorretorVO() {
		super(TipoProdutor.CORRETOR);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getCpd(), getSucursalSeguradora());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		CorretorVO other = (CorretorVO) obj;
		return Objects.equal(getCpd(), other.getCpd()) && Objects.equal(getCpd(), other.getCpd());
	}

	public TipoComissao getTipoComissao() {
		return TipoComissao.COMISSAO_NORMAL;
	}

	
	

}