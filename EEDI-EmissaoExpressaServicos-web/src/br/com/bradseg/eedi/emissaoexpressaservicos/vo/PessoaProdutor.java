package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Pessoa Produtor da proposta
 * @author WDEV
 */
public class PessoaProdutor implements Serializable {

	private static final long serialVersionUID = 2329231769030810368L;

	private TipoPessoaProdutor tipoPessoaProdutor;

	public TipoPessoaProdutor getTipoPessoaProdutor() {
		return tipoPessoaProdutor;
	}

	public void setTipoPessoaProdutor(TipoPessoaProdutor tipoPessoaProdutor) {
		this.tipoPessoaProdutor = tipoPessoaProdutor;
	}

}
