package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipo de situa��es de uma proposta
 * 
 * @author WDEV
 */
public enum SituacaoPropostaShopping {

	//@formatter:off
	CONTRATACAO_EFETUADA(3, "Contrata��o Efetuada"), 
	SOLICITACACAO_DE_CANCELAMENTO(5, "Solicita��o de Cancelamento"),  
	CANCELADA(6, "Cancelada");
	//@formatter:on

	private Integer codigo;
	private String descricao;

	private SituacaoPropostaShopping(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * A partir do c�digo da situa��o da proposta obt�m a situa��o completa com descri��o
	 * @param codigo C�digo da situa��o da proposta
	 * @return SituacaoProposta
	 */
	public static SituacaoPropostaShopping obterPorCodigo(Integer codigo) {
		for (SituacaoPropostaShopping situacaoPropostaShopping : SituacaoPropostaShopping.values()) {
			if (situacaoPropostaShopping.getCodigo().equals(codigo)) {
				return situacaoPropostaShopping;
			}
		}
		return null;
	}
}
