package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * Filtro para consulta de relatório de acompanhamento
 * 
 * @author WDEV
 */
public class FiltroRelatorioAcompanhamentoVO extends FiltroPropostaVO implements Serializable {

	private static final long serialVersionUID = -1810899337037860221L;

	private PessoaVO pessoa = new PessoaVO();
	private Integer codigoAgenciaDebito;
	private Integer codigoAgenciaProdutora;
	private String matriculaAssistenteBS;
	private String matriculaGerente;
	private Integer codigoSucursalSeguradora;

	public SucursalSeguradoraVO getSucursalSeguradora() {
		return new SucursalSeguradoraVO(codigoSucursalSeguradora);
	}

	public void setCodigoSucursalSeguradora(Integer codigoSucursalSeguradora) {
		this.codigoSucursalSeguradora = codigoSucursalSeguradora;
	}

	public Integer getCodigoSucursalSeguradora() {
		return codigoSucursalSeguradora;
	}

	public Integer getCodigoAgenciaDebito() {
		return codigoAgenciaDebito;
	}

	public void setCodigoAgenciaDebito(Integer codigoAgenciaDebito) {
		this.codigoAgenciaDebito = codigoAgenciaDebito;
	}

	public boolean isCpfTitularPreenchido() {
		return getPessoa().isCpfPreenchido();
	}

	public Integer getCodigoAgenciaProdutora() {
		return codigoAgenciaProdutora;
	}

	public void setCodigoAgenciaProdutora(Integer codigoAgenciaProdutora) {
		this.codigoAgenciaProdutora = codigoAgenciaProdutora;
	}

	public String getMatriculaAssistenteBS() {
		return matriculaAssistenteBS;
	}

	public void setMatriculaAssistenteBS(String matriculaAssistenteBS) {
		this.matriculaAssistenteBS = matriculaAssistenteBS;
	}

	public String getMatriculaGerente() {
		return matriculaGerente;
	}

	public void setMatriculaGerente(String matriculaGerente) {
		this.matriculaGerente = matriculaGerente;
	}

	public boolean isAgenciaDebitoPreenchida() {
		return getCodigoAgenciaDebito() != null;
	}

	public boolean isAgenciaProdutoraPreenchida() {
		return getCodigoAgenciaProdutora() != null;
	}

	public boolean isMatriculaAssistentePreenchida() {
		return StringUtils.isNotBlank(getMatriculaAssistenteBS());
	}

	public boolean isMatriculaGerentePreenchida() {
		return StringUtils.isNotBlank(getMatriculaGerente());
	}

	public boolean isSucursalSeguradoraPreenchida() {
		return getSucursalSeguradora() != null && getSucursalSeguradora().isCodigoPreenchido();
	}

	public PessoaVO getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaVO pessoa) {
		this.pessoa = pessoa;
	}

}