package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Situa��es do retorno do movimento da proposta
 * @author WDEV
 */
public enum SituacaoRetornoMovimento {

	STATUS_ALTERACAO("A"), STATUS_INCLUSAO("I"), SITUACAO_CANCELADA("8"), SITUACAO_PROCESSADA("P"), SITUACAO_1("1");

	private String codigo;

	private SituacaoRetornoMovimento(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoRetornoMovimento obterPorCodigo(String codigo) {
		for (SituacaoRetornoMovimento situacao : SituacaoRetornoMovimento.values()) {
			if (situacao.getCodigo().equals(codigo)) {
				return situacao;
			}
		}
		return null;
	}

}