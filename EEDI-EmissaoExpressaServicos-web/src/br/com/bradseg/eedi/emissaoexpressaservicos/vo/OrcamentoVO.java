package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;

/**
 * Representa o or�amento total do plano de benef�cios de uma proposta para quantidade informada de benefici�rios
 * @author WDEV
 */
public class OrcamentoVO implements Serializable {

	private static final long serialVersionUID = 1730687552418723058L;

	private PropostaVO proposta;

	private PlanoVO plano;

	public OrcamentoVO(PropostaVO proposta, PlanoVO plano) {
		this.proposta = proposta;
		this.plano = plano;
	}

	public BigDecimal getValorTotalSemTaxaDeAdesao() {
		
		BigDecimal valorMensalTitular = new BigDecimal(plano.getValorPlanoVO().getValorMensalTitular());
		BigDecimal valorMensalDependente = new BigDecimal(plano.getValorPlanoVO().getValorMensalDependente());
		
		BigDecimal valorTotal = valorMensalTitular.add(valorMensalDependente.multiply(proposta.getQuantidadeDependentes()));
//		valorMensalTitular.add(new BigDecimal(plano.getValorPlanoVO().getValorMensalDependente())
				//BigDecimal valorTotal = plano.getValorMensalidadeTitular().add(plano.getValorMensalidadeDependente().multiply(proposta.getQuantidadeDependentes()));

		if (BigDecimal.ZERO.compareTo(valorTotal) == 0) {
			return BigDecimal.ZERO;
		}
		return valorTotal;
	}

	public BigDecimal getValorTotalComTaxaDeAdesao() {
		BigDecimal valorTotal = getValorTotalSemTaxaDeAdesao();

		if (BigDecimal.ZERO.compareTo(valorTotal) == 0) {
			return BigDecimal.ZERO;
		}

	   BigDecimal valorTaxa = new BigDecimal(plano.getValorPlanoVO().getValorTaxa());
	   valorTotal = valorTotal.add(valorTaxa);
	   
		
		// Adiciona a taxa de ades�o, caso o plano possua
//		if (plano.isPossuiTaxaAdesao(proposta.getDataCriacao())) {
//			BigDecimal quantidadeBeneficiarios = proposta.getQuantidadeDependentes().add(BigDecimal.ONE);
//			valorTotal = valorTotal.add(Constantes.TAXA_ADESAO_POR_DEPENDENTE.multiply(quantidadeBeneficiarios));
//		}

		return valorTotal;
	}

	public PlanoVO getPlano() {
		return plano;
	}

//	public boolean isPlanoPreenchido() {
//		return plano != null && plano.isCodigoPreenchido();
//	}
}