package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe enumeration de Banco: Bradesco, Santander, Ita� e Banco do Brasil.
 */
public enum Banco {

	SANTANDER(33, "033 - Santander", "Santander"), 
	BRADESCO(237, "237 - Bradesco", "Bradesco"), 
	ITAU(341, "341 - Ita�", "Ita�");
	//HSBC(399, "399 - HSBC", "HSBC");

	// BRADESCO(237, "237 - Bradesco", "Bradesco");

	private Integer codigo;
	private String descricao;
	private String nome;

	private Banco(Integer codigo, String descricao, String nome) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.nome = nome;
	}

	/**
	 * @return the codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Busca o banco pelo codigo
	 * 
	 * @param codigo - codigo do banco
	 * @return o banco
	 */
	public static Banco buscaBancoPor(Integer codigo) {
		for (Banco banco : Banco.values()) {
			if (banco.getCodigo().equals(codigo)) {
				return banco;
			}
		}
		return null;
	}

	/**
	 * Metodo responsavel por obter a lista de bancos de acordo com o segmento da sucursal.
	 * 
	 * @param segmento - segmento da sucursal.
	 * @return List<Banco> - lista de bancos.
	 */
	public static List<Banco> obterListaDeBancoPorSegmento(String segmento) {

		List<Banco> listaDeBancos = new ArrayList<Banco>();

		if ("REDE".equalsIgnoreCase(segmento)) {
			listaDeBancos.add(Banco.BRADESCO);
			//listaDeBancos.add(Banco.HSBC);
		} else if ("MERCADO".equalsIgnoreCase(segmento) || "CORPORATE".equalsIgnoreCase(segmento)) {
			listaDeBancos.add(Banco.BRADESCO);
			listaDeBancos.add(Banco.SANTANDER);
			listaDeBancos.add(Banco.ITAU);
		}

		return listaDeBancos;
	}

	/**
	 * Metodo responsavel por obter a legenda dos bancos de acordo com o segmento da sucursal.
	 * 
	 * @param segmento - segmento da sucursal.
	 * @return List<Banco> - lista de bancos.
	 */
	public static String obterLegendaDeBancoPorSegmento(String segmento) {

		StringBuilder legenda = new StringBuilder();

		if ("REDE".equalsIgnoreCase(segmento)) {
			legenda.append(BRADESCO.descricao)
				.append("   ");
				//.append(HSBC.descricao);
		} else if ("MERCADO".equalsIgnoreCase(segmento) || "CORPORATE".equalsIgnoreCase(segmento)) {
			legenda.append(SANTANDER.descricao)
				.append("   ")
				.append(BRADESCO.descricao)
				.append("   ")
				.append(ITAU.descricao);
		}

		return legenda.toString();
	}
}
