package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipos de Estado Civil
 * 
 * @author WDEV
 *
 */
public enum EstadoCivil {

	CASADO(1, "Casado"), DIVORCIADO(3, "Divorciado"), SEPARADO(5, "Separado"), SOLTEIRO(6, "Solteiro"), VIUVO(7, "Vi�vo");

	private Integer codigo;
	private String descricao;

	EstadoCivil(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * Busca o estado civil pelo codigo
	 * 
	 * @param codigo - codigo do estado civil
	 * @return o estado civil
	 */
	public static EstadoCivil buscaPorCodigo(Integer codigo) {
		for (EstadoCivil estadoCivil : EstadoCivil.values()) {
			if (estadoCivil.getCodigo().equals(codigo)) {
				return estadoCivil;
			}
		}
		return null;
	}

}
