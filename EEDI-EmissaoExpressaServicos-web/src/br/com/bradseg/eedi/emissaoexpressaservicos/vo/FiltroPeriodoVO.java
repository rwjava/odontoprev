package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.LocalDate;

public class FiltroPeriodoVO implements Serializable {
	
	private static final long serialVersionUID = 6038804016825604090L;
	
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private Integer status;

	/**
	 * Retorna dataInicio.
	 *
	 * @return dataInicio - dataInicio.
	 */
	@XmlJavaTypeAdapter(type = org.joda.time.LocalDate.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDateAdapter.class)
	public LocalDate getDataInicio() {
		return dataInicio;
	}

	/**
	 * Especifica dataInicio.
	 *
	 * @param dataInicio - dataInicio.
	 */
	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * Retorna dataFim.
	 *
	 * @return dataFim - dataFim.
	 */
	@XmlJavaTypeAdapter(type = org.joda.time.LocalDate.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDateAdapter.class)
	public LocalDate getDataFim() {
		return dataFim;
	}

	/**
	 * Especifica dataFim.
	 *
	 * @param dataFim - dataFim.
	 */
	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}

	/**
	 * Retorna status.
	 *
	 * @return status - status.
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Especifica status.
	 *
	 * @param status - status.
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

}
