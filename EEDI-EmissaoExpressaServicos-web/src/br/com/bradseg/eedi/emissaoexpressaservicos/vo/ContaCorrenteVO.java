package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * Dados da conta corrente
 * 
 * @author WDEV
 */
public class ContaCorrenteVO implements Serializable {

	private static final long serialVersionUID = 302102919277098883L;

	private Long numero;
	private String digitoVerificador;

	private AgenciaBancariaVO agenciaBancaria;

//	private Integer codigoPostoAtendimento;
	private String siglaPostoAtendimento;
	
	
	public ContaCorrenteVO() {
		// Construtor default
	}

	public ContaCorrenteVO(String numeroContaCompleto) {
		if (StringUtils.isNotBlank(numeroContaCompleto)) {
			numeroContaCompleto = numeroContaCompleto.trim();
			String conta = numeroContaCompleto.substring(0, numeroContaCompleto.length() - 1);
			String digitoVerificador = numeroContaCompleto.substring(numeroContaCompleto.length() - 1);

			this.numero = Long.valueOf(conta);
			this.digitoVerificador = digitoVerificador;
		}
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(String digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}

	public String getNumeroComDigitoVerificador() {
		if (getNumero() == null || StringUtils.isBlank(getDigitoVerificador())) {
			return null;
		}
		return new StringBuilder().append(numero).append(digitoVerificador).toString();
	}

	public AgenciaBancariaVO getAgenciaBancaria() {
		return agenciaBancaria;
	}

	public void setAgenciaBancaria(AgenciaBancariaVO agenciaBancaria) {
		this.agenciaBancaria = agenciaBancaria;
	}

	@Override
	public String toString() {
		return "ContaCorrenteVO [numero=" + numero + ", digitoVerificador=" + digitoVerificador + ", agenciaBancaria="
				+ agenciaBancaria + "]";
	}
//
//	/**
//	 * @return the codigoPostoAtendimento
//	 */
//	public Integer getCodigoPostoAtendimento() {
//		return codigoPostoAtendimento;
//	}
//
//	/**
//	 * @param codigoPostoAtendimento the codigoPostoAtendimento to set
//	 */
//	public void setCodigoPostoAtendimento(Integer codigoPostoAtendimento) {
//		this.codigoPostoAtendimento = codigoPostoAtendimento;
//	}

	/**
	 * @return the siglaPostoAtendimento
	 */
	public String getSiglaPostoAtendimento() {
		return siglaPostoAtendimento;
	}

	/**
	 * @param siglaPostoAtendimento the siglaPostoAtendimento to set
	 */
	public void setSiglaPostoAtendimento(String siglaPostoAtendimento) {
		this.siglaPostoAtendimento = siglaPostoAtendimento;
	}
}