package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.json.annotations.JSON;
import org.joda.time.LocalDate;
import org.joda.time.Period;

import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;

/**
 * Dados da PessoaVO
 * 
 * @author WDEV
 */
public class PessoaVO implements Serializable {

	private static final long serialVersionUID = 9064739460905338988L;

	private String nome;

	private String cpf;

	private LocalDate dataNascimento;

	private String email;

	private Integer idade;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@XmlJavaTypeAdapter(type = org.joda.time.LocalDate.class, value = br.com.bradseg.eedi.emissaoexpressaservicos.proposta.webservice.LocalDateAdapter.class)
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public String getDataNascimentoFormatada() {
		if (dataNascimento == null) {
			return null;
		}
		return dataNascimento.toString(Constantes.FORMATO_DATA_PT_BR);
	}

	@JSON(format = Constantes.FORMATO_DATA_PT_BR)
	public Date getDataNascimentoJson() {
		return dataNascimento.toDateMidnight().toDate();
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public boolean isCpfPreenchido() {
		return StringUtils.isNotBlank(getCpf());
	}

	public boolean isNomePreenchido() {
		return StringUtils.isNotBlank(getNome());
	}

	/**
	 * Verifica se a pessoa � menor de idade
	 * 
	 * @return true se � menor de idade
	 */
	public boolean isMenorDeIdade() {
		if (getDataNascimento() != null) {
//			Period periodo = new Period(getDataNascimento(), new LocalDate());
//			int idade = periodo.getYears();
			return (new Period(getDataNascimento(), new LocalDate()).getYears() < 18);
		}
		return false;
	}

	@Override
	public String toString() {
		return "PessoaVO [nome=" + nome + ", cpf=" + cpf + ", dataNascimento=" + dataNascimento + ", email=" + email
				+ ", idade=" + idade + "]";
	}



	
}