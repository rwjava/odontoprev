package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Dados contidos na leitura do arquivo de carga 
 * 
 * @author WDEV
 */
public class RegistroRetornoMovimentoPropostaVO implements Serializable {

	private static final long serialVersionUID = -6671809127837575808L;

	private Integer codigoErro;

	private String codigoProposta;

	private SituacaoRetornoMovimento situacaoProcessada;

	private String numeroCarteiraNacional;

	private String cpfBeneficiario;

	public Integer getCodigoErro() {
		return codigoErro;
	}

	public void setCodigoErro(Integer codigoErro) {
		this.codigoErro = codigoErro;
	}

	public String getCodigoProposta() {
		return codigoProposta;
	}

	public void setCodigoProposta(String codigoProposta) {
		this.codigoProposta = codigoProposta;
	}

	public SituacaoRetornoMovimento getSituacaoProcessada() {
		return situacaoProcessada;
	}

	public void setSituacaoProcessada(SituacaoRetornoMovimento situacaoProcessada) {
		this.situacaoProcessada = situacaoProcessada;
	}

	public String getNumeroCarteiraNacional() {
		return numeroCarteiraNacional;
	}

	public void setNumeroCarteiraNacional(String numeroCarteiraNacional) {
		this.numeroCarteiraNacional = numeroCarteiraNacional;
	}

	public String getCpfBeneficiario() {
		return cpfBeneficiario;
	}

	public void setCpfBeneficiario(String cpfBeneficiario) {
		this.cpfBeneficiario = cpfBeneficiario;
	}

	public boolean isInclusao() {
		return SituacaoRetornoMovimento.STATUS_INCLUSAO.equals(getSituacaoProcessada());
	}

	public boolean isAlteracao() {
		return SituacaoRetornoMovimento.STATUS_ALTERACAO.equals(getSituacaoProcessada());
	}

	public boolean isCancelada() {
		return SituacaoRetornoMovimento.SITUACAO_CANCELADA.equals(getSituacaoProcessada());
	}

	public boolean isImplantada() {
		return SituacaoRetornoMovimento.SITUACAO_PROCESSADA.equals(getSituacaoProcessada()) || SituacaoRetornoMovimento.SITUACAO_1.equals(getSituacaoProcessada());
	}

	public boolean isRegistroComErro() {
		return codigoErro.longValue() != 0;
	}

}
