package br.com.bradseg.eedi.emissaoexpressaservicos.vo;


import java.io.Serializable;

import org.joda.time.DateTime;

/**
 * Classe responsável pelo transporte das informações das propostas expiradas.
 */
public class PropostaExpiradaVO implements Serializable {

	private static final long serialVersionUID = -3536236303926171864L;

	private Long sequencialProposta;
	private DateTime dataValidadeProposta;
	private DateTime dataEmissaoProposta;

	/**
	 * @return the sequencialProposta
	 */
	public Long getSequencialProposta() {
		return sequencialProposta;
	}

	/**
	 * @param sequencialProposta the sequencialProposta to set
	 */
	public void setSequencialProposta(Long sequencialProposta) {
		this.sequencialProposta = sequencialProposta;
	}

	/**
	 * @return the dataEmissaoProposta
	 */
	public DateTime getDataEmissaoProposta() {
		return dataEmissaoProposta;
	}

	/**
	 * @param dataEmissaoProposta the dataEmissaoProposta to set
	 */
	public void setDataEmissaoProposta(DateTime dataEmissaoProposta) {
		this.dataEmissaoProposta = dataEmissaoProposta;
	}

	/**
	 * @return the dataValidadeProposta
	 */
	public DateTime getDataValidadeProposta() {
		if (this.getDataEmissaoProposta() != null) {
			dataValidadeProposta = this.getDataEmissaoProposta().plusDays(90);
			if (dataValidadeProposta.getDayOfWeek() == 6) {
				dataValidadeProposta = dataValidadeProposta.plusDays(2);
			} else if (dataValidadeProposta.getDayOfWeek() == 7) {
				dataValidadeProposta = dataValidadeProposta.plusDays(1);
			}
		}

		return dataValidadeProposta;
	}

	/**
	 * @param dataValidadeProposta the dataValidadeProposta to set
	 */
	public void setDataValidadeProposta(DateTime dataValidadeProposta) {
		this.dataValidadeProposta = dataValidadeProposta;
	}
}
