package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipos de telefone
 * 
 * @author WDEV
 */
public enum TipoTelefone {

	RESIDENCIAL(1, "Residencial"), COMERCIAL(2, "Comercial"), CELULAR(3, "Celular");

	private Integer codigo;
	private String descricao;

	private TipoTelefone(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoTelefone buscaPorCodigo(Integer codigo) {
		for (TipoTelefone tipo : TipoTelefone.values()) {
			if (tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}

		return null;
	}

}