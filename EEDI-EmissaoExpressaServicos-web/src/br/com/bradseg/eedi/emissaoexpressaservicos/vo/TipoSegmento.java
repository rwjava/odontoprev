package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.util.ArrayList;
import java.util.List;

public enum TipoSegmento {

	NAO_INFORMADO(0, "Tipo segmento banco n�o informado"), CLASSIC(1, "Classic"), EXCLUSIVE(2, "Exclusive"), PRIME(3,"Prime");

	private Integer codigo;
	private String descricao;

	private TipoSegmento(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String toString() {
		return descricao;
	}

	/**
	 * Busca por codigo.
	 * 
	 * @param codigo
	 *            - codigo do tipo de segmento
	 * @return o tipo segmento
	 */
	public static TipoSegmento buscaPor(Integer codigo) {
		for (TipoSegmento tipo : TipoSegmento.values()) {
			if (tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}
		return null;
	}

	
	/**
	 * Retorna uma lista com todos os segmentos.
	 * 
	 * @return List<LabelValueVO> - lista de segmento.
	 */
	public static List<LabelValueVO> obterLista(){
		List<LabelValueVO> lista = new ArrayList<LabelValueVO>();
		for (TipoSegmento tipo : TipoSegmento.values()) {
			lista.add(new LabelValueVO(String.valueOf(tipo.getCodigo()), tipo.getDescricao()));
		}
		return lista;
	}

}
