package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipo de situa��es de uma proposta
 * 
 * @author WDEV
 */
public enum SituacaoProposta {

	//@formatter:off
	PENDENTE(1, "Pendente"), 
	RASCUNHO(2, "Rascunho"), 
	EM_PROCESSAMENTO(3, "Em Processamento"), 
	CRITICADA(4, "Criticada"), 
	CANCELADA(5, "Cancelada"), 
	IMPLANTADA(6, "Implantada"), 
	PRE_CANCELADA(7,"Pr�-cancelada"),
	TODAS(8, "Todas");
	//@formatter:on

	private Integer codigo;
	private String descricao;

	private SituacaoProposta(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * A partir do c�digo da situa��o da proposta obt�m a situa��o completa com descri��o
	 * @param codigo C�digo da situa��o da proposta
	 * @return SituacaoProposta
	 */
	public static SituacaoProposta obterPorCodigo(Integer codigo) {
		for (SituacaoProposta situacaoProposta : SituacaoProposta.values()) {
			if (situacaoProposta.getCodigo().equals(codigo)) {
				return situacaoProposta;
			}
		}
		return null;
	}
}
