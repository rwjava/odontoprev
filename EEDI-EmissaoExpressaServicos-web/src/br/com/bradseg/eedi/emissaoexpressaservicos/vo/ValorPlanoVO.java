package br.com.bradseg.eedi.emissaoexpressaservicos.vo;



import java.io.Serializable;

import org.joda.time.DateTime;

public class ValorPlanoVO implements Serializable {

	private static final long serialVersionUID = -3461919268576237696L;

	private Long sequencial;

	private Double valorAnualTitular;
	private Double valorAnualDependente;
	private Double valorMensalTitular;
	private Double valorMensalDependente;
	private Double valorTaxa;
	private Double valorDesconto;
	private DateTime dataInicio;
	private DateTime dataFim;
	
	public Long getSequencial() {
		return sequencial;
	}
	public void setSequencial(Long sequencial) {
		this.sequencial = sequencial;
	}
	public Double getValorAnualTitular() {
		return valorAnualTitular;
	}
	public void setValorAnualTitular(Double valorAnualTitular) {
		this.valorAnualTitular = valorAnualTitular;
	}
	public Double getValorAnualDependente() {
		return valorAnualDependente;
	}
	public void setValorAnualDependente(Double valorAnualDependente) {
		this.valorAnualDependente = valorAnualDependente;
	}
	public Double getValorMensalTitular() {
		return valorMensalTitular;
	}
	public void setValorMensalTitular(Double valorMensalTitular) {
		this.valorMensalTitular = valorMensalTitular;
	}
	public Double getValorMensalDependente() {
		return valorMensalDependente;
	}
	public void setValorMensalDependente(Double valorMensalDependente) {
		this.valorMensalDependente = valorMensalDependente;
	}
	public DateTime getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(DateTime dataInicio) {
		this.dataInicio = dataInicio;
	}
	public DateTime getDataFim() {
		return dataFim;
	}
	public void setDataFim(DateTime dataFim) {
		this.dataFim = dataFim;
	}
	/**
	 * @return the valorTaxa
	 */
	public Double getValorTaxa() {
		return valorTaxa;
	}
	/**
	 * @param valorTaxa the valorTaxa to set
	 */
	public void setValorTaxa(Double valorTaxa) {
		this.valorTaxa = valorTaxa;
	}
	/**
	 * @return the valorDesconto
	 */
	public Double getValorDesconto() {
		return valorDesconto;
	}
	/**
	 * @param valorDesconto the valorDesconto to set
	 */
	public void setValorDesconto(Double valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	@Override
	public String toString() {
		return "ValorPlanoVO [sequencial=" + sequencial + ", valorAnualTitular=" + valorAnualTitular
				+ ", valorAnualDependente=" + valorAnualDependente + ", valorMensalTitular=" + valorMensalTitular
				+ ", valorMensalDependente=" + valorMensalDependente + ", valorTaxa=" + valorTaxa + ", valorDesconto="
				+ valorDesconto + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + "]";
	}
	
	
	
	

}
