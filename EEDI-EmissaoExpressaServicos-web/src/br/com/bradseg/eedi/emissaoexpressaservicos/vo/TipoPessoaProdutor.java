package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Enum com os tipos de Pessoao
 * 
 * @author WDEV
 */
public enum TipoPessoaProdutor {

	PESSOA_FISICA("F"), PESSOA_JURIDICA("J");

	private String tipo;

	private TipoPessoaProdutor(String tipo) {
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public String toString() {
		return tipo;
	}

	/**
	 * Busca por Tipo PessoaProdutor.
	 * 
	 * @param codigo codigo
	 * @return tipo de pessoa Produtor
	 */
	public static TipoPessoaProdutor buscaPor(String codigo) {
		for (TipoPessoaProdutor tipo : TipoPessoaProdutor.values()) {
			if (tipo.getTipo().equals(codigo)) {
				return tipo;
			}
		}
		return null;
	}

}
