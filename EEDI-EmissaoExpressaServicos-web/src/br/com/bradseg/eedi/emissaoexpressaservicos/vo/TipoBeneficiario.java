package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipos de benefici�rios
 * 
 * @author WDEV
 */
public enum TipoBeneficiario {

	TITULAR(1, "Titular"), DEPENDENTE(2, "Dependente");

	private Integer codigo;
	private String descricao;

	private TipoBeneficiario(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	/**
	 * A partir do c�digo do tipo de benefici�rio da proposta obt�m o TipoBeneficiario
	 * @param codigo C�digo do tipo de benefici�rio
	 * @return Tipo de benefici�rio
	 */
	public static TipoBeneficiario obterTipoBeneficiarioPorCodigo(Integer codigo) {
		for (TipoBeneficiario tipoBeneficiario : TipoBeneficiario.values()) {
			if (tipoBeneficiario.getCodigo().equals(codigo)) {
				return tipoBeneficiario;
			}
		}
		return null;
	}

}