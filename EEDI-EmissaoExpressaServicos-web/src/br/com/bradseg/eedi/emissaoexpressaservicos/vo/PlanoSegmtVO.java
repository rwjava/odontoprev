package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

public class PlanoSegmtVO implements Serializable {

	private static final long serialVersionUID = 2614580778703859865L;
	
	private Long codigoPlano;
	private String periodoCarenciaAnual;
	private String periodoCarenciaMensal;
	private String nomePlano;
	private String nomePlanoANS;
	private Double valorAnualDependente;
	private Double valorAnualTitular;
	private Double valorMensalDependente;
	private Double valorMensalTitular;
	private Long codigoPlanoANS;
	
	
	/**
	 * Instantiates a new plano segmt VO.
	 */
	public PlanoSegmtVO() {

	}


	/**
	 * Gets the codigo plano.
	 *
	 * @return the codigo plano
	 */
	public Long getCodigoPlano() {
		return codigoPlano;
	}


	/**
	 * Sets the codigo plano.
	 *
	 * @param codigoPlano the new codigo plano
	 */
	public void setCodigoPlano(Long codigoPlano) {
		this.codigoPlano = codigoPlano;
	}


	/**
	 * Gets the periodo carencia anual.
	 *
	 * @return the periodo carencia anual
	 */
	public String getPeriodoCarenciaAnual() {
		return periodoCarenciaAnual;
	}


	/**
	 * Sets the periodo carencia anual.
	 *
	 * @param periodoCarenciaAnual the new periodo carencia anual
	 */
	public void setPeriodoCarenciaAnual(String periodoCarenciaAnual) {
		this.periodoCarenciaAnual = periodoCarenciaAnual;
	}


	/**
	 * Gets the periodo carencia mensal.
	 *
	 * @return the periodo carencia mensal
	 */
	public String getPeriodoCarenciaMensal() {
		return periodoCarenciaMensal;
	}


	/**
	 * Sets the periodo carencia mensal.
	 *
	 * @param periodoCarenciaMensal the new periodo carencia mensal
	 */
	public void setPeriodoCarenciaMensal(String periodoCarenciaMensal) {
		this.periodoCarenciaMensal = periodoCarenciaMensal;
	}


	/**
	 * Gets the nome plano.
	 *
	 * @return the nome plano
	 */
	public String getNomePlano() {
		return nomePlano;
	}


	/**
	 * Sets the nome plano.
	 *
	 * @param nomePlano the new nome plano
	 */
	public void setNomePlano(String nomePlano) {
		this.nomePlano = nomePlano;
	}


	/**
	 * Gets the nome plano ANS.
	 *
	 * @return the nome plano ANS
	 */
	public String getNomePlanoANS() {
		return nomePlanoANS;
	}


	/**
	 * Sets the nome plano ANS.
	 *
	 * @param nomePlanoANS the new nome plano ANS
	 */
	public void setNomePlanoANS(String nomePlanoANS) {
		this.nomePlanoANS = nomePlanoANS;
	}


	/**
	 * Gets the valor anual dependente.
	 *
	 * @return the valor anual dependente
	 */
	public Double getValorAnualDependente() {
		return valorAnualDependente;
	}


	/**
	 * Sets the valor anual dependente.
	 *
	 * @param valorAnualDependente the new valor anual dependente
	 */
	public void setValorAnualDependente(Double valorAnualDependente) {
		this.valorAnualDependente = valorAnualDependente;
	}


	/**
	 * Gets the valor anual titular.
	 *
	 * @return the valor anual titular
	 */
	public Double getValorAnualTitular() {
		return valorAnualTitular;
	}


	/**
	 * Sets the valor anual titular.
	 *
	 * @param valorAnualTitular the new valor anual titular
	 */
	public void setValorAnualTitular(Double valorAnualTitular) {
		this.valorAnualTitular = valorAnualTitular;
	}


	/**
	 * Gets the valor mensal dependente.
	 *
	 * @return the valor mensal dependente
	 */
	public Double getValorMensalDependente() {
		return valorMensalDependente;
	}


	/**
	 * Sets the valor mensal dependente.
	 *
	 * @param valorMensalDependente the new valor mensal dependente
	 */
	public void setValorMensalDependente(Double valorMensalDependente) {
		this.valorMensalDependente = valorMensalDependente;
	}


	/**
	 * Gets the valor mensal titular.
	 *
	 * @return the valor mensal titular
	 */
	public Double getValorMensalTitular() {
		return valorMensalTitular;
	}


	/**
	 * Sets the valor mensal titular.
	 *
	 * @param valorMensalTitular the new valor mensal titular
	 */
	public void setValorMensalTitular(Double valorMensalTitular) {
		this.valorMensalTitular = valorMensalTitular;
	}


	/**
	 * Gets the codigo plano ANS.
	 *
	 * @return the codigo plano ANS
	 */
	public Long getCodigoPlanoANS() {
		return codigoPlanoANS;
	}


	/**
	 * Sets the codigo plano ANS.
	 *
	 * @param codigoPlanoANS the new codigo plano ANS
	 */
	public void setCodigoPlanoANS(Long codigoPlanoANS) {
		this.codigoPlanoANS = codigoPlanoANS;
	}

}
