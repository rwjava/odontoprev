package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipos de produtores
 * 
 * @author WDEV
 */
public enum TipoProdutor {

	ANGARIADOR(1), CORRETOR(2), CORRETOR_MASTER(3), GERENTE(4);

	private Integer codigo;

	private TipoProdutor(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	/**
	 * Busca por codigo.
	 * 
	 * @param codigo - codigo do tipo do produtor
	 * @return o tipo ProdutorVO
	 */
	public static TipoProdutor obterPorCodigo(Integer codigo) {
		for (TipoProdutor tipo : TipoProdutor.values()) {
			if (tipo.getCodigo().equals(codigo)) {
				return tipo;
			}
		}
		return null;
	}

}