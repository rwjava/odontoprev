package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Item de relatório de acompanhamento
 * @author WDEV
 */
public class ItemRelatorioAcompanhamentoVO implements Serializable {

	private static final long serialVersionUID = 8602053202019283265L;

	private PropostaVO proposta;

	private Integer quantidadeVidas;

	public PropostaVO getProposta() {
		return proposta;
	}

	public void setProposta(PropostaVO proposta) {
		this.proposta = proposta;
	}

	public Integer getQuantidadeVidas() {
		return quantidadeVidas;
	}

	public void setQuantidadeVidas(Integer quantidadeVidas) {
		this.quantidadeVidas = quantidadeVidas;
	}

	public OrcamentoVO getOrcamento() {
		if (Integer.valueOf(1).compareTo(quantidadeVidas) < 0) {
			proposta.setDependentes(new ArrayList<DependenteVO>());

			// Adiciona na proposta a quantidade de dependentes (quantidadeVidas - titular)
			for (int i = 0; i < (quantidadeVidas - 1); i++) {
				proposta.getDependentes().add(new DependenteVO());
			}
		}
		return new OrcamentoVO(proposta, proposta.getPlanoVO());
	}

}