package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

import java.io.Serializable;

/**
 * Dados do responsavel legal
 * 
 * @author WDEV
 */
public class ResponsavelLegalVO extends PessoaVO implements Serializable {

	private static final long serialVersionUID = -2778125596400565192L;

	private Long codigo;

	private EnderecoVO endereco;

	private TelefoneVO telefone;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public EnderecoVO getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoVO endereco) {
		this.endereco = endereco;
	}

	public TelefoneVO getTelefone() {
		return telefone;
	}

	public void setTelefone(TelefoneVO telefone) {
		this.telefone = telefone;
	}

	public boolean isPossuiEndereco() {
		return endereco != null && endereco.getCodigo() != null;
	}

}