package br.com.bradseg.eedi.emissaoexpressaservicos.vo;

/**
 * Tipo de endere�o utilizado na proposta.
 * 	- Pode ser RESIDENCIAL, indicando o endere�o do titular da proposta;
 *  - Pode ser COBRANCA, indicando o endere�o de cobran�a da proposta (este podendo ser o pr�prio endere�o do titular ou do respons�vel legal)
 * 
 * @author WDEV
 */
public enum TipoEndereco {

	COBRANCA, RESIDENCIAL;

}