package br.com.bradseg.eedi.emissaoexpressaservicos.servico.action;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.SortedSet;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletOutputStream;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.bradseg.eedi.emissaoexpressaservicos.contacorrente.facade.ContaCorrenteServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.corretorsucursal.facade.CorretorSucursalServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.ConsultaPropostaServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.GerarArquivoRelatorio;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.PropostaServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.RelatorioAcompanhamentoEEDIServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.RelatorioAcompanhamentoServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade.RelatorioPropostaServiceFacade;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.action.BaseAction;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AcompanhamentoPropostaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.FiltroAcompanhamentoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.LoginVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.MovimentoPropostaVO;

/**
 * Servicos disponiveis
 * 
 * @author WDEV
 */
@Controller
@Scope(value = "request")
public class ServicosAction extends BaseAction {

	private static final long serialVersionUID = 7604396081048785340L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ServicosAction.class);
	
	@Autowired
	private transient ContaCorrenteServiceFacade contaCorrenteServiceFacade;

	@Autowired
	private transient CorretorSucursalServiceFacade corretorSucursalServiceFacade;

	@Autowired
	private RelatorioPropostaServiceFacade relatorioPropostaServiceFacade;

	@Autowired
	private GerarArquivoRelatorio gerarArquivoRelatorio;
	
	
	private ServicosForm form = new ServicosForm();

	private Boolean clienteEmListaRestricao;

	private CorretorVO corretor;

	private SortedSet<CorretorVO> corretores;
	
	@Autowired
	private RelatorioAcompanhamentoEEDIServiceFacade relatorioAcompanhamentoEEDIServiceFacade;
	
	@Autowired
	private RelatorioAcompanhamentoServiceFacade relatorioAcompanhamentoServiceFacade;
	
	@Autowired
	private ConsultaPropostaServiceFacade consultaPropostaServiceFacade;
	
	private String textoConsulta;
	
	private List<List<String>> detalhes;
	
	@Autowired
	private PropostaServiceFacade propostaServiceFacade;
	
	private List<MovimentoPropostaVO> movimentosProposta;

	public String gerarPDF(){
		
		byte[] bytes = gerarArquivoRelatorio.gerar(relatorioPropostaServiceFacade.gerarRelatorioPropostaCompleta(50363L, false, false, false));
		
		try {
			ServletOutputStream servletOutputStream = ServletActionContext.getResponse().getOutputStream();
			ServletActionContext.getResponse().setContentType("application/pdf");
			ServletActionContext.getResponse().setContentLength(bytes.length);
	        servletOutputStream.write(bytes, 0, bytes.length);
	        servletOutputStream.flush();
	        servletOutputStream.close();
		} catch (IOException e) {
			LOGGER.error("Erro ao gerar o PDF da proposta.");
		}
		
		
		return SUCCESS;
	}
	
	public String gerarPDFVazia(){
		
		byte[] bytes = gerarArquivoRelatorio.gerar(relatorioPropostaServiceFacade.gerarRelatorioPropostaCompleta(51527L, true, true, true));
		
		try {
			ServletOutputStream servletOutputStream = ServletActionContext.getResponse().getOutputStream();
			ServletActionContext.getResponse().setContentType("application/pdf");
			ServletActionContext.getResponse().setContentLength(bytes.length);
	        servletOutputStream.write(bytes, 0, bytes.length);
	        servletOutputStream.flush();
	        servletOutputStream.close();
		} catch (IOException e) {
			LOGGER.error("Erro ao gerar o PDF da proposta vazia.");
		}
		
		
		return SUCCESS;
	}
	
	public String gerarBoleto(){
		
		byte[] bytes = gerarArquivoRelatorio.gerar(relatorioPropostaServiceFacade.gerarBoleto(49609L));
		
		try {
			ServletOutputStream servletOutputStream = ServletActionContext.getResponse().getOutputStream();
			ServletActionContext.getResponse().setContentType("application/pdf");
			ServletActionContext.getResponse().setContentLength(bytes.length);
	        servletOutputStream.write(bytes, 0, bytes.length);
	        servletOutputStream.flush();
	        servletOutputStream.close();
		} catch (IOException e) {
			LOGGER.error("Erro ao gerar o boleto da proposta.");
		}
		
		
		return SUCCESS;

	}
	
	
     public String gerarPDFRelatorioAcompanhamento(){
		
    	FiltroAcompanhamentoVO filtro = new FiltroAcompanhamentoVO();
    	filtro.setCodigoProposta("BDA000000512431");
    	LoginVO login = new LoginVO();
    	login.setId("02176000854");
    	login.setNome("");
 		List<AcompanhamentoPropostaVO>  listaDePropostas = relatorioAcompanhamentoEEDIServiceFacade.listarPropostasAcompanhamento(filtro, login, false);

    	 
		byte[] bytes =  gerarArquivoRelatorio.gerar(relatorioAcompanhamentoServiceFacade.gerarPDFRelatorioAcompanhamento(filtro,  login));
		
		try {
			ServletOutputStream servletOutputStream = ServletActionContext.getResponse().getOutputStream();
			ServletActionContext.getResponse().setContentType("application/pdf");
			ServletActionContext.getResponse().setContentLength(bytes.length);
	        servletOutputStream.write(bytes, 0, bytes.length);
	        servletOutputStream.flush();
	        servletOutputStream.close();
		} catch (IOException e) {
			LOGGER.error("Erro ao gerar o boleto da proposta.");
		}
		
		
		return SUCCESS;

	}
     
    public String iniciarConsulta(){
    	return SUCCESS;
    }
    
    public String consultar(){
    	
    	this.detalhes = consultaPropostaServiceFacade.realizarConsulta(textoConsulta);
    	return SUCCESS;
    }
	
	public String inicio() {
		return SUCCESS;
	}

	public String validarCliente() {
		clienteEmListaRestricao = contaCorrenteServiceFacade.isClienteEmListaRestricao(form.getCpf(), form.getCorretor());
		return SUCCESS;
	}

	public String consultarCorretor() {
		corretor = corretorSucursalServiceFacade.consultarCorretor(form.getCorretor().getSucursalSeguradora().getCodigo(), form.getCorretor().getCpd());
		return SUCCESS;
	}

	public String listarCorretor() {
		corretores = corretorSucursalServiceFacade.listarPorCpfCnpjUsuario(form.getCorretor().getCpfCnpj());
		return SUCCESS;
	}
	
	
	public String consultaServicos() {
		
		return SUCCESS;
	}
	
	public String consultaMovimento() {
		try {
			movimentosProposta = propostaServiceFacade.listarMovimentoPropostaPorSequencial(form.getSequencial());
		} catch (Exception e) {
			LOGGER.error("N�o foi poss�vel listar movimento da proposta: " + e.getMessage());
		}
		
		return SUCCESS;
	}
	
	public String statusServicos() {	
		return SUCCESS;
	}
	
	/****************************************************************
	 * TESTA SERVICOS                                               *
	 * @return                                                      *
	 ****************************************************************/
	public Boolean obterStatusListarMovimentoPropostaPorSequencial() {
			String listarMovimentoPropostaPorSequencial = "http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService/listarMovimentoPropostaPorSequencial?sequencialProposta=53673";
		    URL url = null;
		    URLConnection urlConnection = null;
		    try {
		      url = new URL(listarMovimentoPropostaPorSequencial);
		      urlConnection = url.openConnection();
		      if(urlConnection.getContent() != null) {
		          LOGGER.error("URL ONLINE");
		      } else {
		    	  LOGGER.error("URL OFFLINE");
		      }
		    } catch (MalformedURLException ex) {
		    	LOGGER.error("URL OFFLINE");
		    } catch (IOException ex) {
		    	LOGGER.error("Falha para abrir conex�o.");
		    } 
		return true;
	}

	/**
	 * Formulario
	 * @author WDEV
	 */
	public static class ServicosForm implements Serializable {

		private static final long serialVersionUID = 2242215181528467015L;

		private String cpf;
		private CorretorVO corretor = new CorretorVO();
		private Long sequencial; 

		public String getCpf() {
			return cpf;
		}

		public void setCpf(String cpf) {
			this.cpf = cpf;
		}

		public CorretorVO getCorretor() {
			return corretor;
		}

		public void setCorretor(CorretorVO corretor) {
			this.corretor = corretor;
		}

		public Long getSequencial() {
			return sequencial;
		}

		public void setSequencial(Long sequencial) {
			this.sequencial = sequencial;
		}

	}

	public ServicosForm getForm() {
		return form;
	}

	public void setForm(ServicosForm form) {
		this.form = form;
	}

	public Boolean getClienteEmListaRestricao() {
		return clienteEmListaRestricao;
	}

	public CorretorVO getCorretor() {
		return corretor;
	}

	public SortedSet<CorretorVO> getCorretores() {
		return corretores;
	}

	public String getTextoConsulta() {
		return textoConsulta;
	}

	public void setTextoConsulta(String textoConsulta) {
		this.textoConsulta = textoConsulta;
	}

	public List<List<String>> getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(List<List<String>> detalhes) {
		this.detalhes = detalhes;
	}

	/**
	 * @return the movimentosPropostaVO
	 */
	public List<MovimentoPropostaVO> getMovimentosProposta() {
		return movimentosProposta;
	}

	/**
	 * @param movimentosPropostaVO the movimentosPropostaVO to set
	 */
	public void setMovimentosProposta(List<MovimentoPropostaVO> movimentosProposta) {
		this.movimentosProposta = movimentosProposta;
	}

	
}