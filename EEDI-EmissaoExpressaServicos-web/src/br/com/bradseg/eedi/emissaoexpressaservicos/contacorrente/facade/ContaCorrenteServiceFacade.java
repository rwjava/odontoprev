package br.com.bradseg.eedi.emissaoexpressaservicos.contacorrente.facade;

import java.util.List;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;

/**
 * Servi�o respons�vel por consultar as informa��es da conta corrente
 * 
 * @author WDEV
 */
public interface ContaCorrenteServiceFacade {

	/**
	 * Obt�m a lista de contas correntes para o cpf informado
	 * @param cpf Cpf do titular da conta
	 * @return Lista com as contas do proponente
	 */
	public List<ContaCorrenteVO> listarContaCorrentePorCPF(String cpf);

	/**
	 * Obt�m os dados do proponente da conta a partir do cpf informado
	 * @param cpf Cpf do titular da conta
	 * 
	 * @return Proponente da conta corrente
	 */
	public ProponenteVO obterTitularContaPorCPF(String cpf);

	/**
	 * Verifica se a conta corrente do cliente possui alguma restri��o
	 * @param cpf CPF do titular da conta
	 * @param corretor Corretor logado no sistema
	 * @return verdade se cliente estiver em lista de restri��o
	 */
	public boolean isClienteEmListaRestricao(String cpf, CorretorVO corretor);

}