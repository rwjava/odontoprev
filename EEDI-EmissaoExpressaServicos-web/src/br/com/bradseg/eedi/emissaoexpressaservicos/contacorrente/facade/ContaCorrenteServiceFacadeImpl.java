package br.com.bradseg.eedi.emissaoexpressaservicos.contacorrente.facade;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bucb.servicos.model.service.ContaCorrenteSOAPImpl;
import br.com.bradseg.bucb.servicos.model.service.WebServiceBusinessException;
import br.com.bradseg.bucb.servicos.model.service.WebServiceIntegrationException;
import br.com.bradseg.bucb.servicos.model.vo.ContaCorrenteCpfVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.Validador;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.AgenciaBancariaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ContaCorrenteVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.ProponenteVO;
import br.com.bradseg.pcbs.identificacaocliente.ws.FaultException;
import br.com.bradseg.pcbs.identificacaocliente.ws.ValidarCliente;
import br.com.bradseg.pcbs.identificacaocliente.ws.bean.IdentificacaoClienteType;
import br.com.bradseg.pcbs.identificacaocliente.ws.bean.StatusClienteRestrict;

import com.google.common.collect.Lists;

/**
 * Servi�o respons�vel por consultar as informa��es da conta corrente
 * 
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class ContaCorrenteServiceFacadeImpl implements ContaCorrenteServiceFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContaCorrenteServiceFacadeImpl.class);

	private static final String CODIGO_ERRO_TITULAR_NAO_ENCONTRADO = "3";


	@Autowired
	private ContaCorrenteSOAPImpl contaCorrenteSOAPImpl;

	@Autowired
	protected ValidarCliente validarCliente;

	/**
	 * Lista as contas-correntes do titular a partir do cpf informado.
	 * @param cpf CPF do titular da conta
	 * @return Lista de contas-correntes
	 */
	public List<ContaCorrenteVO> listarContaCorrentePorCPF(String cpf) {
		List<ContaCorrenteVO> contas = Lists.newArrayList();
		for (ContaCorrenteCpfVO contaCorrenteCpf : listarContaCorrente(cpf)) {
			ContaCorrenteVO contaCorrente = new ContaCorrenteVO();
			contaCorrente.setNumero(contaCorrenteCpf.getNumeroContaCorrente());
			contaCorrente.setDigitoVerificador(contaCorrenteCpf.getDvContaCorrente());

			contaCorrente.setAgenciaBancaria(new AgenciaBancariaVO());
			contaCorrente.getAgenciaBancaria().setCodigo(contaCorrenteCpf.getCodigoAgencia());
			contaCorrente.getAgenciaBancaria().setDigito(contaCorrenteCpf.getDvAgencia());
			contas.add(contaCorrente);
		}
		return contas;
	}

	/**
	 * Obt�m as informa��es do titular da conta, como: nome, cpf e data de nascimento ou null caso n�o encontre os dados para aquele cpf
	 * @param cpf CPF do titular
	 * @return Informa��es do titular da conta
	 */
	public ProponenteVO obterTitularContaPorCPF(String cpf) {
		for (ContaCorrenteCpfVO contaCorrenteCpf : listarContaCorrente(cpf)) {
			// Retorna o primeiro encontrado, pois so ira usar informacoes do cpf, nome, data de nascimento e email 
			ProponenteVO proponente = new ProponenteVO();
			proponente.setNome(contaCorrenteCpf.getNome());
			proponente.setCpf(String.valueOf(cpf));
			proponente.setDataNascimento(obterDataNascimento(contaCorrenteCpf));
			return proponente;
		}
		return null;
	}

	/**
	 * Acessa o servi�o externo que consulta a lista de conta-corrente do titular a partir do cpf informado.
	 * Recebe um cpf normalizado (xxx.xxx.xxx-xx) e o desnormaliza (xxxxxxxxxxx). Caso o cpf n�o seja informado, ser� lan�ado um erro informando a obrigatoriedade.
	 * @param cpf CPF do titular
	 * @return Lista de contas-correntes
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	private List<ContaCorrenteCpfVO> listarContaCorrente(String cpf) {
		try {
			cpf = Strings.desnormalizarCPF(cpf);
			validarCPF(cpf);
			LOGGER.error("INFO: CONSULTAR CONTA CORRENTE: " + cpf);
			return contaCorrenteSOAPImpl.consultarContaCorrentePorCPF(null,null, Long.valueOf(cpf)).getContaCorrenteCpfVO();
		} catch (WebServiceBusinessException e) {
			LOGGER.error("--- WebServiceBusinessException ---");
			LOGGER.error(e.getMessage(), e);
			String numero = e.getMessage();
			//if ("3".equals(numero)) {
			throw new BusinessException("INFO: N�o existe nenhum correntista com o CPF informado.");
			//} else {
			//	throw new BusinessException(e.getMessage() + " (" + numero + ")");
			//}
		} catch (WebServiceIntegrationException e) {
			LOGGER.error("--- WebServiceIntegrationException ---");
			LOGGER.error(e.getMessage(), e);
			LOGGER.error("INFO: ERRO AO CONSULTAR CONTA CORRENTE. CONSULTANDO NOVAMENTE PARA CPF: "+cpf);
			cpf = Strings.desnormalizarCPF(cpf);
			validarCPF(cpf);
			try {
				return contaCorrenteSOAPImpl.consultarContaCorrentePorCPF(null,null, Long.valueOf(cpf)).getContaCorrenteCpfVO();
			}  catch (WebServiceIntegrationException e1) {
				
				LOGGER.error("--- WebServiceIntegrationException ---");
				LOGGER.error(e.getMessage(), e);
				throw new BusinessException("INFO: Ocorreu um erro ao tentar consultar o CPF no servi�o remoto: " + e.getMessage());
				
			} catch (WebServiceBusinessException e1) {
				  
				LOGGER.error("--- WebServiceBusinessException ---");
				LOGGER.error(e.getMessage(), e);
				throw new BusinessException("INFO: N�o existe nenhum correntista com o CPF informado.");
			}
			
			
		} catch (Exception e) {
			LOGGER.error("--- Exception ---");
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException("INFO: Ocorreu um erro ao tentar consultar o CPF no servi�o remoto: " + e.getMessage());
		}
	}

	/**
	 * Valida se o cpf est� preenchido
	 * @param cpf CPF do titular
	 */
	private void validarCPF(String cpf) {
		Validador validador = new Validador();
		validador.cpf(cpf, "CPF do titular");
		if (validador.hasError()) {
			LOGGER.error("INFO: CPF INV�LIDO: "+cpf);
			throw new BusinessException(validador.getMessages());
		}
	}

	/**
	 * Obt�m um {@link LocalDate} a partir da data de nascimento retornada do servi�o de consulta de titular.
	 * A data no servi�o dever� vir no padr�o dd.mm.yyyy, caso contr�rio ocorrer� erro. 
	 * 
	 * @param contaCorrenteCpfVO Retorno do servi�o de consulta de titular de conta
	 * @return Data de nascimento em {@link LocalDate}
	 */
	private LocalDate obterDataNascimento(ContaCorrenteCpfVO contaCorrenteCpfVO) {
		String dataNascimento = contaCorrenteCpfVO.getDtNasc();
		try {			
			Date dataNascimentoDate = new SimpleDateFormat("dd.mm.yyyy").parse(dataNascimento);
			return new LocalDate(dataNascimentoDate);
		} catch (ParseException e) {
			LOGGER.error("INFO: Erro ao realizar parse da data de nascimento do correntista", e);
			return null;
			//throw new BusinessException("INFO: Formato de data de nascimento inv�lido retornado do servi�o remoto de consulta de conta corrente por cpf. ", e);
		}
	}

	/**
	 * Verifica se o titular est� em algum tipo de lista de restri��o para o corretor criando a proposta.
	 * Consulta um servi�o externo informando o cpf do titular, cnpj e sucursal do corretor que est� criando a proposta. O servi�o retornar� se o cliente est� ou n�o na lista de restri��o.
	 * @param cpf CPF do titular
	 * @param corretor Corretor que est� criando a proposta
	 * @return true se cliente est� em lista de restri��o
	 */
	public boolean isClienteEmListaRestricao(String cpf, CorretorVO corretor) {
		try {
			cpf = Strings.removerCaracteresNaoNumericos(cpf);
			String cpfCnpjCorretor = Strings.removerCaracteresNaoNumericos(corretor.getCpfCnpj());
			List<IdentificacaoClienteType> listaIdentificacaoCliente = validarCliente.validarCliente(cpf, cpfCnpjCorretor, String.valueOf(corretor.getSucursalSeguradora().getCodigo()));
			for (IdentificacaoClienteType identificacaoClienteType : listaIdentificacaoCliente) {
				if (isClienteAtivo(identificacaoClienteType)) {
					return true;
				}
			}
			return false;
		} catch (FaultException e) {
			LOGGER.error("INFO: Erro ao validar cliente em lista de restri��o com os parametros: " + Lists.newArrayList(cpf, corretor.getCpfCnpj(), String.valueOf(corretor.getSucursalSeguradora().getCodigo())), e);
			throw new BusinessException("Erro ao tentar validar cliente em lista de restri��o", e);
		}
	}

	/**
	 * Verifica se o titular est� ativo e se o status est� como restrito
	 * @param identificacaoClienteType Identifica��o do cliente
	 * @return true se cliente est� ativo em lista de restri��o
	 */
	private boolean isClienteAtivo(IdentificacaoClienteType identificacaoClienteType) {
		return identificacaoClienteType.getAtivo() != null && StatusClienteRestrict.S.equals(identificacaoClienteType.getAtivo());
	}
}
