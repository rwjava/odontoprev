package br.com.bradseg.eedi.emissaoexpressaservicos.endereco.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.bradseg.bsad.framework.core.jdbc.JdbcDao;
import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UnidadeFederativaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UsuarioUltimaAtualizacao;

/**
 * Servi�os de acesso a dados de Endere�o
 * 
 * @author WDEV
 */
@Repository
public class EnderecoDAOImpl extends JdbcDao implements EnderecoDAO {

	@Autowired
	private DataSource dataSource;

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

	public EnderecoVO salvar(EnderecoVO endereco) {
		Long codigoEndereco = getJdbcTemplate().queryForLong("SELECT NEXT VALUE FOR DBPROD.SQ01_ENDER_DNTAL_INDVD FROM SYSIBM.SYSDUMMY1", new MapSqlParameterSource());
		//TODO postgres
		//Long codigoEndereco = getJdbcTemplate().queryForLong("SELECT COALESCE(MAX(cender_dntal_indvd),0)+1 FROM dbprod.ENDER_DNTAL_INDVD ", new MapSqlParameterSource());
		

		endereco.setCodigo(codigoEndereco);

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO DBPROD.ENDER_DNTAL_INDVD");
		sql.append(" (CENDER_DNTAL_INDVD, ILOGDR_ENDER_COTAC, NLOGDR_ENDER_COTAC, RCOMPL_ENDER_COTAC, IBAIRO, ICIDDE, CSGL_UF, CNRO_CEP, DULT_ATULZ_REG, CRESP_ULT_ATULZ )");
		sql.append(" VALUES");
		sql.append(" (:codigo, :logradouro, :numero, :complemento, :bairro, :cidade, :uf, :cep, CURRENT_TIMESTAMP, :usuarioUltimaAtualizacao)");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigo", endereco.getCodigo());
		params.addValue("logradouro", endereco.getLogradouro());
		params.addValue("numero", endereco.getNumero());
		params.addValue("complemento", endereco.getComplemento());
		params.addValue("bairro", endereco.getBairro());
		params.addValue("cidade", endereco.getCidade());
		params.addValue("uf", endereco.getUnidadeFederativa().getSigla());
		params.addValue("cep", endereco.getCep());
		params.addValue("usuarioUltimaAtualizacao", new UsuarioUltimaAtualizacao().getCodigo());

		getJdbcTemplate().update(sql.toString(), params);

		return endereco;
	}

	public void remover(EnderecoVO endereco) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM DBPROD.ENDER_DNTAL_INDVD WHERE CENDER_DNTAL_INDVD = :codigo");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("codigo", endereco.getCodigo());

		getJdbcTemplate().update(sql.toString(), params);
	}

	public List<UnidadeFederativaVO> listarUnidadeFederativa() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT CSGL_UF, IUF FROM DBPROD.UF WHERE CIND_REG_EXCL = 'N'");

		return getJdbcTemplate().query(sql.toString(), new MapSqlParameterSource(), new UnidadeFederativaRowMapper());
	}

	/**
	 * Mapeamento de dados da listagem de unidade federativa
	 * @author WDEV
	 */
	private static final class UnidadeFederativaRowMapper implements RowMapper<UnidadeFederativaVO> {
		public UnidadeFederativaVO mapRow(ResultSet rs, int rowNum) throws SQLException {
			ResultSetAdapter resultSet = new ResultSetAdapter(rs);
			UnidadeFederativaVO uf = new UnidadeFederativaVO();
			uf.setSigla(resultSet.getString("CSGL_UF"));
			uf.setNome(resultSet.getString("IUF"));
			return uf;
		}
	}

}