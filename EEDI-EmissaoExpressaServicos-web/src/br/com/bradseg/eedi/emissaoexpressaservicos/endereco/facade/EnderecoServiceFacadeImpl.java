package br.com.bradseg.eedi.emissaoexpressaservicos.endereco.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.message.Message;
import br.com.bradseg.eedi.emissaoexpressaservicos.contacorrente.facade.ContaCorrenteServiceFacadeImpl;
import br.com.bradseg.eedi.emissaoexpressaservicos.endereco.dao.EnderecoDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UnidadeFederativaVO;
import br.com.bradseg.repf.digitacaoproposta.cep.webservice.BusinessException_Exception;
import br.com.bradseg.repf.digitacaoproposta.cep.webservice.CepWebService;
import br.com.bradseg.repf.digitacaoproposta.cep.webservice.EnderecoConsultaCepVO;
import br.com.bradseg.repf.digitacaoproposta.cep.webservice.IntegrationException_Exception;
//import br.com.bradseg.ccep.consultacep.model.cep.ejb.CepSessionFacade;

/**
 * Servi�o respons�vel por consultar o endere�o
 * 
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class EnderecoServiceFacadeImpl implements EnderecoServiceFacade {


	@Autowired
	private EnderecoDAO enderecoDAO;
	
	@Autowired
	private CepWebService cepWebService;


	private static final Logger LOGGER = LoggerFactory.getLogger(ContaCorrenteServiceFacadeImpl.class);

	
	public EnderecoVO obterEnderecoPorCep(String enderecoCep){
		EnderecoVO endereco = new EnderecoVO();
		
		enderecoCep = garanteZeros(enderecoCep, 8);
		
		
		try {
			EnderecoConsultaCepVO enderecoConsultaCepVO =  cepWebService.obterEnderecoPorCep(enderecoCep);
			if(enderecoConsultaCepVO != null){
				endereco.setBairro(enderecoConsultaCepVO.getBairroLogradouro());
				endereco.setCidade(enderecoConsultaCepVO.getCidadeLogradouro());
				endereco.setLogradouro(enderecoConsultaCepVO.getLogradouro());
				endereco.setCep(enderecoConsultaCepVO.getCepLogradouro());
				if(enderecoConsultaCepVO.getUfLogradouro() != null){
						UnidadeFederativaVO unidadeFederativaVO = new UnidadeFederativaVO();
						unidadeFederativaVO.setSigla(enderecoConsultaCepVO.getUfLogradouro());
						endereco.setUnidadeFederativa(unidadeFederativaVO);
				}
				if(enderecoConsultaCepVO.getNumeroLogradouro() != null){
					endereco.setNumero(Long.valueOf(enderecoConsultaCepVO.getNumeroLogradouro()));
				}
				
			}
			LOGGER.error("INFO: ENDERE�O RETORNADO: "+endereco);
			return endereco;
			
		} catch (IntegrationException_Exception e) {
			LOGGER.error("INFO: CEP Inv�lido!");
			LOGGER.error("INFO: ERRO : "+e.getMessage());
			throw new IntegrationException("Sistema de busca de cep indispon�vel no momento.");
		} catch (BusinessException_Exception e) {
			LOGGER.error("INFO: CEP Inv�lido!");
			LOGGER.error("INFO: ERRO : "+e.getMessage());
			throw new BusinessException(new Message("msg.erro.cep.invalido", Message.ERROR_TYPE));
		}
	
	}
	
	public static String garanteZeros(String entrada, Integer tam) {
        int inicial = entrada.trim().length();
        StringBuffer ret = new StringBuffer(tam);
        if (entrada.length() > tam) {
              ret.append(entrada.substring(0, tam - 1));
        } else if(entrada.length() < tam) {
              for(int x = 0; x < (tam - inicial); x++ ) {
                    ret.append('0');
              }
              ret.append(entrada.trim());
        } else {
              ret.append(entrada);
        }
        return ret.toString().trim();
  }


	



	

	

}
