package br.com.bradseg.eedi.emissaoexpressaservicos.endereco.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.bradseg.eedi.emissaoexpressaservicos.support.ResultSetAdapter;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Strings;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.EnderecoVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.UnidadeFederativaVO;

/**
 * Mapeamento dos dados do endere�o
 * 
 * @author WDEV
 */
public class EnderecoRowMapper implements RowMapper<EnderecoVO> {

	public EnderecoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ResultSetAdapter resultSet = new ResultSetAdapter(rs);

		EnderecoVO endereco = new EnderecoVO();
		endereco.setCodigo(resultSet.getLong("CENDER_DNTAL_INDVD"));
		endereco.setLogradouro(Strings.maiusculas(resultSet.getString("ILOGDR_ENDER_COTAC")));
		endereco.setNumero(resultSet.getLong("NLOGDR_ENDER_COTAC"));
		endereco.setComplemento(Strings.maiusculas(resultSet.getString("RCOMPL_ENDER_COTAC")));
		endereco.setBairro(Strings.maiusculas(resultSet.getString("IBAIRO")));
		endereco.setCidade(Strings.maiusculas(resultSet.getString("ICIDDE")));
		endereco.setUnidadeFederativa(new UnidadeFederativaVO());
		endereco.getUnidadeFederativa().setSigla(Strings.maiusculas(resultSet.getString("CSGL_UF")));
		endereco.setCep(Strings.normalizarCep(resultSet.getString("CNRO_CEP")));

		return endereco;
	}

}