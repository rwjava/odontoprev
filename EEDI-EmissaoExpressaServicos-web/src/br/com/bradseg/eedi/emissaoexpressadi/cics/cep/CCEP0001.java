package br.com.bradseg.eedi.emissaoexpressadi.cics.cep;

import br.com.bradseg.bsad.framework.ctg.programapi.annotation.CicsGateway;
import br.com.bradseg.bsad.framework.ctg.programapi.field.FieldType;
import br.com.bradseg.bsad.framework.ctg.programapi.field.LongFieldType;
import br.com.bradseg.bsad.framework.ctg.programapi.field.StringFieldType;
import br.com.bradseg.bsad.framework.ctg.programapi.program.CTGProgramImpl;
import br.com.bradseg.bsad.framework.ctg.programapi.program.CommonAreaMetaData;

/**
 * Classe responsável pelo mapeamento da book do serviço CICS CCEP0001.
 * 
 * @author Arquitetura Bradesco Seguros e Previdência
 */
@CicsGateway
public class CCEP0001 extends CTGProgramImpl {

	private static final String PGM_NAME = "CCEP0001";
	private static final String TRAN_NAME = "EP01";
	private static final int COMM_LENGTH = 1328;

	private static final FieldType[] COMM_OUT = new FieldType[] { new StringFieldType("CCEP-CEP-UF", 2),
			new StringFieldType("CCEP-CEP-CIDADE", 72), new StringFieldType("CCEP-CEP-BAIRRO", 60),
			new StringFieldType("CCEP-CEP-BAIRRO-ABREV", 36), new StringFieldType("CCEP-CEP-TPO-LOGDR", 72),
			new StringFieldType("CCEP-CEP-TPO-LOGDR-ABREV", 15), new StringFieldType("CCEP-CEP-PATENTE", 72),
			new StringFieldType("CCEP-CEP-PREPOSICAO", 3), new StringFieldType("CCEP-CEP-LOGDR", 72),
			new StringFieldType("CCEP-CEP-LOGDR-ABREV", 36), new StringFieldType("CCEP-CEP-LOGDR-COMPLETO", 140),
			new StringFieldType("CCEP-CEP-DISTRITO", 72), new StringFieldType("CCEP-CEP-POVOADO", 72),
			new StringFieldType("CCEP-CEP-GUSUARIO", 72), new StringFieldType("CCEP-CEP-COMLP-LOGDR", 72),
			new StringFieldType("CCEP-CEP-CD-UF-IBGE", 2), new StringFieldType("CCEP-CEP-NOME-UF", 72),
			new StringFieldType("CCEP-CEP-CD-MUNICIPIO-IBGE", 5), new StringFieldType("FILLER", 202) };

	private static final FieldType[] COMM_AREA = new FieldType[] { new LongFieldType("CCEP-CEP-GSBS-ERR", 4),
			new LongFieldType("CCEP-CEP-GSBS-TIP-ERR", 1), new LongFieldType("CCEP-CEP-GSBS-SQLCODE", 8),
			new StringFieldType("FILLER", 7), new LongFieldType("CCEP-CEP-CD-RETORNO", 2),
			new StringFieldType("CCEP-CEP-SINAL-SQLCODE", 1), new LongFieldType("CCEP-CEP-SQLCODE", 4),
			new LongFieldType("CCEP-CEP-SQLERRML", 4), new StringFieldType("CCEP-CEP-SQLERRMC", 70),
			new StringFieldType("CCEP-CEP-TABELA-ERR", 18), new StringFieldType("CCEP-CEP-DESCRICAO-ERR", 52),
			new StringFieldType("CCEP-CEP-NROCEP", 8) };

	private static final CommonAreaMetaData COMM_AREA_IN = new CommonAreaMetaData(COMM_AREA);
	private static final CommonAreaMetaData COMM_AREA_OUT = new CommonAreaMetaData(COMM_AREA, COMM_OUT);

	/**
	 * Construtor para objetos da classe CCEP0001.
	 */
	public CCEP0001() {
		super(PGM_NAME, TRAN_NAME, COMM_LENGTH, COMM_AREA_IN, COMM_AREA_OUT);
	}
}
