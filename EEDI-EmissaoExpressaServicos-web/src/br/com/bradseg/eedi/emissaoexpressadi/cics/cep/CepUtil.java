package br.com.bradseg.eedi.emissaoexpressadi.cics.cep;

/**
 * Classe utilit�ria para ajudar no tratatmentot dos dados do programa CCEP0001.
 * 
 * @author EEDI
 */
public class CepUtil {

	/**
	 * M�todo respons�vel pelo retorno da constante indicadora de poss�veis erros ocorridos na chamada dos servi�os.
	 * 
	 * @param codErro - c�digo de erro.
	 * @return String - constante do erro.
	 */
	public static String retornaDescricaoErroServico(int codErro) {
		switch (codErro) {
		case 11:
			return "msg.erro.cics.ccep.sql";
		case 12:
			return "msg.erro.cics.ccep.cep-nao-encontrado";
		case 13:
			return "msg.erro.cics.ccep.linkage-com-tamanho-errado";
		case 14:
			return "msg.erro.cics.ccep.localidade-nao-encontrada";
		case 15:
			return "msg.erro.cics.ccep.logradouro-nao-encontrado";
		case 16:
			return "msg.erro.cics.ccep.bairro-nao-encontrado";
		case 17:
			return "msg.erro.cics.ccep.tipo-logradouro-nao-encontrado";
		case 18:
			return "msg.erro.cics.ccep.subdiv-logradouro-nao-encontrada";
		case 19:
			return "msg.erro.cics.ccep.cep-informado-igual-zero";
		case 20:
			return "msg.erro.cics.ccep.localidade-subordinada-codigo-subordinado-zero";
		case 21:
			return "msg.erro.cics.ccep.tipo-localidade-desconhecida";
		case 22:
			return "msg.erro.cics.ccep.cep-distrito-diferente-m";
		case 23:
			return "msg.erro.cics.ccep.cep-nao-pertence-municipio";
		case 33:
			return "msg.erro.cics.ccep.qtd-sol-logradouros-menor-maior";
		case 34:
			return "msg.erro.cics.ccep.numero-base-cep-zero";
		case 35:
			return "msg.erro.cics.ccep.numero-base-cep-nao-encontrado";
		case 36:
			return "msg.erro.cics.ccep.qtd-logradouros-menor-maior-30";
		case 37:
			return "msg.erro.cics.ccep.uf-nao-existe";
		case 38:
			return "msg.erro.cics.ccep.cidade-nao-existe";
		default:
			return "";
		}
	}
}
