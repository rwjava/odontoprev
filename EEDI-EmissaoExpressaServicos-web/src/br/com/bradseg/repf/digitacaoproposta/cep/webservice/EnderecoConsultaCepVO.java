
package br.com.bradseg.repf.digitacaoproposta.cep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enderecoConsultaCepVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="enderecoConsultaCepVO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservice.cep.digitacaoproposta.repf.bradseg.com.br/}enderecoVO">
 *       &lt;sequence>
 *         &lt;element name="numeroLogradouroFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroLogradouroInicial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enderecoConsultaCepVO", propOrder = {
    "numeroLogradouroFinal",
    "numeroLogradouroInicial"
})
public class EnderecoConsultaCepVO extends EnderecoVO
{

    protected String numeroLogradouroFinal;
    protected String numeroLogradouroInicial;

    /**
     * Gets the value of the numeroLogradouroFinal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLogradouroFinal() {
        return numeroLogradouroFinal;
    }

    /**
     * Sets the value of the numeroLogradouroFinal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLogradouroFinal(String value) {
        this.numeroLogradouroFinal = value;
    }

    /**
     * Gets the value of the numeroLogradouroInicial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLogradouroInicial() {
        return numeroLogradouroInicial;
    }

    /**
     * Sets the value of the numeroLogradouroInicial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLogradouroInicial(String value) {
        this.numeroLogradouroInicial = value;
    }

}
