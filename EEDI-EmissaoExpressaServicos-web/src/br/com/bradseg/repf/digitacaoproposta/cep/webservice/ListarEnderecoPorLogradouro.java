
package br.com.bradseg.repf.digitacaoproposta.cep.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listarEnderecoPorLogradouro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listarEnderecoPorLogradouro">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="endereco" type="{http://webservice.cep.digitacaoproposta.repf.bradseg.com.br/}enderecoConsultaCepVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listarEnderecoPorLogradouro", propOrder = {
    "endereco"
})
public class ListarEnderecoPorLogradouro {

    protected EnderecoConsultaCepVO endereco;

    /**
     * Gets the value of the endereco property.
     * 
     * @return
     *     possible object is
     *     {@link EnderecoConsultaCepVO }
     *     
     */
    public EnderecoConsultaCepVO getEndereco() {
        return endereco;
    }

    /**
     * Sets the value of the endereco property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnderecoConsultaCepVO }
     *     
     */
    public void setEndereco(EnderecoConsultaCepVO value) {
        this.endereco = value;
    }

}
