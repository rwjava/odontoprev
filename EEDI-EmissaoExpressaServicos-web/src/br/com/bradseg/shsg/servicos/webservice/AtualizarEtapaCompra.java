
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for atualizarEtapaCompra complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="atualizarEtapaCompra">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="atualizaEtapaCompra" type="{http://webservice.servicos.shsg.bradseg.com.br/}atualizaEtapaCompraRequestVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "atualizarEtapaCompra", propOrder = {
    "atualizaEtapaCompra"
})
public class AtualizarEtapaCompra {

    protected AtualizaEtapaCompraRequestVO atualizaEtapaCompra;

    /**
     * Gets the value of the atualizaEtapaCompra property.
     * 
     * @return
     *     possible object is
     *     {@link AtualizaEtapaCompraRequestVO }
     *     
     */
    public AtualizaEtapaCompraRequestVO getAtualizaEtapaCompra() {
        return atualizaEtapaCompra;
    }

    /**
     * Sets the value of the atualizaEtapaCompra property.
     * 
     * @param value
     *     allowed object is
     *     {@link AtualizaEtapaCompraRequestVO }
     *     
     */
    public void setAtualizaEtapaCompra(AtualizaEtapaCompraRequestVO value) {
        this.atualizaEtapaCompra = value;
    }

}
