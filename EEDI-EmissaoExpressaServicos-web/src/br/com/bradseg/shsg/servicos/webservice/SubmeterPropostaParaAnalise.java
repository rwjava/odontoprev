
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for submeterPropostaParaAnalise complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="submeterPropostaParaAnalise">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="submeterPropostaParaAnalise" type="{http://webservice.servicos.shsg.bradseg.com.br/}submeterPropostaParaAnaliseRequestVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submeterPropostaParaAnalise", propOrder = {
    "submeterPropostaParaAnalise"
})
public class SubmeterPropostaParaAnalise {

    protected SubmeterPropostaParaAnaliseRequestVO submeterPropostaParaAnalise;

    /**
     * Gets the value of the submeterPropostaParaAnalise property.
     * 
     * @return
     *     possible object is
     *     {@link SubmeterPropostaParaAnaliseRequestVO }
     *     
     */
    public SubmeterPropostaParaAnaliseRequestVO getSubmeterPropostaParaAnalise() {
        return submeterPropostaParaAnalise;
    }

    /**
     * Sets the value of the submeterPropostaParaAnalise property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmeterPropostaParaAnaliseRequestVO }
     *     
     */
    public void setSubmeterPropostaParaAnalise(SubmeterPropostaParaAnaliseRequestVO value) {
        this.submeterPropostaParaAnalise = value;
    }

}
