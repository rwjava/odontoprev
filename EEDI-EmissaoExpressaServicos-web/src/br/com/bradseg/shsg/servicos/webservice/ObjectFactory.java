
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.shsg.servicos.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Etapa_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "Etapa");
    private final static QName _CancelarContratacaoResponse_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "cancelarContratacaoResponse");
    private final static QName _NegarContratacao_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "negarContratacao");
    private final static QName _AtualizarEtapaCompraResponse_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "atualizarEtapaCompraResponse");
    private final static QName _ListarEtapasResponse_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "listarEtapasResponse");
    private final static QName _NegarCotacao_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "negarCotacao");
    private final static QName _AtualizarEtapaCompra_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "atualizarEtapaCompra");
    private final static QName _NegarContratacaoResponse_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "negarContratacaoResponse");
    private final static QName _ListarEtapas_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "listarEtapas");
    private final static QName _EfetuarContratacao_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "efetuarContratacao");
    private final static QName _NegarCotacaoResponse_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "negarCotacaoResponse");
    private final static QName _SubmeterPropostaParaAnaliseResponse_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "submeterPropostaParaAnaliseResponse");
    private final static QName _EfetuarContratacaoResponse_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "efetuarContratacaoResponse");
    private final static QName _CancelarContratacao_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "cancelarContratacao");
    private final static QName _SubmeterPropostaParaAnalise_QNAME = new QName("http://webservice.servicos.shsg.bradseg.com.br/", "submeterPropostaParaAnalise");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.shsg.servicos.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NegarCotacaoResponse }
     * 
     */
    public NegarCotacaoResponse createNegarCotacaoResponse() {
        return new NegarCotacaoResponse();
    }

    /**
     * Create an instance of {@link EfetuarContratacaoResponse }
     * 
     */
    public EfetuarContratacaoResponse createEfetuarContratacaoResponse() {
        return new EfetuarContratacaoResponse();
    }

    /**
     * Create an instance of {@link SubmeterPropostaParaAnaliseResponse }
     * 
     */
    public SubmeterPropostaParaAnaliseResponse createSubmeterPropostaParaAnaliseResponse() {
        return new SubmeterPropostaParaAnaliseResponse();
    }

    /**
     * Create an instance of {@link CancelarContratacao }
     * 
     */
    public CancelarContratacao createCancelarContratacao() {
        return new CancelarContratacao();
    }

    /**
     * Create an instance of {@link SubmeterPropostaParaAnalise }
     * 
     */
    public SubmeterPropostaParaAnalise createSubmeterPropostaParaAnalise() {
        return new SubmeterPropostaParaAnalise();
    }

    /**
     * Create an instance of {@link AtualizarEtapaCompraResponse }
     * 
     */
    public AtualizarEtapaCompraResponse createAtualizarEtapaCompraResponse() {
        return new AtualizarEtapaCompraResponse();
    }

    /**
     * Create an instance of {@link AtualizarEtapaCompra }
     * 
     */
    public AtualizarEtapaCompra createAtualizarEtapaCompra() {
        return new AtualizarEtapaCompra();
    }

    /**
     * Create an instance of {@link ListarEtapasResponse }
     * 
     */
    public ListarEtapasResponse createListarEtapasResponse() {
        return new ListarEtapasResponse();
    }

    /**
     * Create an instance of {@link NegarCotacao }
     * 
     */
    public NegarCotacao createNegarCotacao() {
        return new NegarCotacao();
    }

    /**
     * Create an instance of {@link EtapaVO }
     * 
     */
    public EtapaVO createEtapaVO() {
        return new EtapaVO();
    }

    /**
     * Create an instance of {@link CancelarContratacaoResponse }
     * 
     */
    public CancelarContratacaoResponse createCancelarContratacaoResponse() {
        return new CancelarContratacaoResponse();
    }

    /**
     * Create an instance of {@link NegarContratacao }
     * 
     */
    public NegarContratacao createNegarContratacao() {
        return new NegarContratacao();
    }

    /**
     * Create an instance of {@link EfetuarContratacao }
     * 
     */
    public EfetuarContratacao createEfetuarContratacao() {
        return new EfetuarContratacao();
    }

    /**
     * Create an instance of {@link ListarEtapas }
     * 
     */
    public ListarEtapas createListarEtapas() {
        return new ListarEtapas();
    }

    /**
     * Create an instance of {@link NegarContratacaoResponse }
     * 
     */
    public NegarContratacaoResponse createNegarContratacaoResponse() {
        return new NegarContratacaoResponse();
    }

    /**
     * Create an instance of {@link AtualizaEtapaCompraRequestVO }
     * 
     */
    public AtualizaEtapaCompraRequestVO createAtualizaEtapaCompraRequestVO() {
        return new AtualizaEtapaCompraRequestVO();
    }

    /**
     * Create an instance of {@link SubmeterPropostaParaAnaliseRequestVO }
     * 
     */
    public SubmeterPropostaParaAnaliseRequestVO createSubmeterPropostaParaAnaliseRequestVO() {
        return new SubmeterPropostaParaAnaliseRequestVO();
    }

    /**
     * Create an instance of {@link NegarContratacaoRequestVO }
     * 
     */
    public NegarContratacaoRequestVO createNegarContratacaoRequestVO() {
        return new NegarContratacaoRequestVO();
    }

    /**
     * Create an instance of {@link ResultadoVO }
     * 
     */
    public ResultadoVO createResultadoVO() {
        return new ResultadoVO();
    }

    /**
     * Create an instance of {@link ResultadoListarEtapasVO }
     * 
     */
    public ResultadoListarEtapasVO createResultadoListarEtapasVO() {
        return new ResultadoListarEtapasVO();
    }

    /**
     * Create an instance of {@link CancelarContratacaoRequestVO }
     * 
     */
    public CancelarContratacaoRequestVO createCancelarContratacaoRequestVO() {
        return new CancelarContratacaoRequestVO();
    }

    /**
     * Create an instance of {@link IdentificadorEtapaVO }
     * 
     */
    public IdentificadorEtapaVO createIdentificadorEtapaVO() {
        return new IdentificadorEtapaVO();
    }

    /**
     * Create an instance of {@link NegarCotacaoRequestVO }
     * 
     */
    public NegarCotacaoRequestVO createNegarCotacaoRequestVO() {
        return new NegarCotacaoRequestVO();
    }

    /**
     * Create an instance of {@link AtualizaEtapaCompraProdutoVO }
     * 
     */
    public AtualizaEtapaCompraProdutoVO createAtualizaEtapaCompraProdutoVO() {
        return new AtualizaEtapaCompraProdutoVO();
    }

    /**
     * Create an instance of {@link EfetuarContratacaoRequestVO }
     * 
     */
    public EfetuarContratacaoRequestVO createEfetuarContratacaoRequestVO() {
        return new EfetuarContratacaoRequestVO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EtapaVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "Etapa")
    public JAXBElement<EtapaVO> createEtapa(EtapaVO value) {
        return new JAXBElement<EtapaVO>(_Etapa_QNAME, EtapaVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarContratacaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "cancelarContratacaoResponse")
    public JAXBElement<CancelarContratacaoResponse> createCancelarContratacaoResponse(CancelarContratacaoResponse value) {
        return new JAXBElement<CancelarContratacaoResponse>(_CancelarContratacaoResponse_QNAME, CancelarContratacaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NegarContratacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "negarContratacao")
    public JAXBElement<NegarContratacao> createNegarContratacao(NegarContratacao value) {
        return new JAXBElement<NegarContratacao>(_NegarContratacao_QNAME, NegarContratacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtualizarEtapaCompraResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "atualizarEtapaCompraResponse")
    public JAXBElement<AtualizarEtapaCompraResponse> createAtualizarEtapaCompraResponse(AtualizarEtapaCompraResponse value) {
        return new JAXBElement<AtualizarEtapaCompraResponse>(_AtualizarEtapaCompraResponse_QNAME, AtualizarEtapaCompraResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEtapasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "listarEtapasResponse")
    public JAXBElement<ListarEtapasResponse> createListarEtapasResponse(ListarEtapasResponse value) {
        return new JAXBElement<ListarEtapasResponse>(_ListarEtapasResponse_QNAME, ListarEtapasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NegarCotacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "negarCotacao")
    public JAXBElement<NegarCotacao> createNegarCotacao(NegarCotacao value) {
        return new JAXBElement<NegarCotacao>(_NegarCotacao_QNAME, NegarCotacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AtualizarEtapaCompra }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "atualizarEtapaCompra")
    public JAXBElement<AtualizarEtapaCompra> createAtualizarEtapaCompra(AtualizarEtapaCompra value) {
        return new JAXBElement<AtualizarEtapaCompra>(_AtualizarEtapaCompra_QNAME, AtualizarEtapaCompra.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NegarContratacaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "negarContratacaoResponse")
    public JAXBElement<NegarContratacaoResponse> createNegarContratacaoResponse(NegarContratacaoResponse value) {
        return new JAXBElement<NegarContratacaoResponse>(_NegarContratacaoResponse_QNAME, NegarContratacaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarEtapas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "listarEtapas")
    public JAXBElement<ListarEtapas> createListarEtapas(ListarEtapas value) {
        return new JAXBElement<ListarEtapas>(_ListarEtapas_QNAME, ListarEtapas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EfetuarContratacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "efetuarContratacao")
    public JAXBElement<EfetuarContratacao> createEfetuarContratacao(EfetuarContratacao value) {
        return new JAXBElement<EfetuarContratacao>(_EfetuarContratacao_QNAME, EfetuarContratacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NegarCotacaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "negarCotacaoResponse")
    public JAXBElement<NegarCotacaoResponse> createNegarCotacaoResponse(NegarCotacaoResponse value) {
        return new JAXBElement<NegarCotacaoResponse>(_NegarCotacaoResponse_QNAME, NegarCotacaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmeterPropostaParaAnaliseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "submeterPropostaParaAnaliseResponse")
    public JAXBElement<SubmeterPropostaParaAnaliseResponse> createSubmeterPropostaParaAnaliseResponse(SubmeterPropostaParaAnaliseResponse value) {
        return new JAXBElement<SubmeterPropostaParaAnaliseResponse>(_SubmeterPropostaParaAnaliseResponse_QNAME, SubmeterPropostaParaAnaliseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EfetuarContratacaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "efetuarContratacaoResponse")
    public JAXBElement<EfetuarContratacaoResponse> createEfetuarContratacaoResponse(EfetuarContratacaoResponse value) {
        return new JAXBElement<EfetuarContratacaoResponse>(_EfetuarContratacaoResponse_QNAME, EfetuarContratacaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarContratacao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "cancelarContratacao")
    public JAXBElement<CancelarContratacao> createCancelarContratacao(CancelarContratacao value) {
        return new JAXBElement<CancelarContratacao>(_CancelarContratacao_QNAME, CancelarContratacao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmeterPropostaParaAnalise }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.servicos.shsg.bradseg.com.br/", name = "submeterPropostaParaAnalise")
    public JAXBElement<SubmeterPropostaParaAnalise> createSubmeterPropostaParaAnalise(SubmeterPropostaParaAnalise value) {
        return new JAXBElement<SubmeterPropostaParaAnalise>(_SubmeterPropostaParaAnalise_QNAME, SubmeterPropostaParaAnalise.class, null, value);
    }

}
