
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for atualizaEtapaCompraRequestVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="atualizaEtapaCompraRequestVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="produto" type="{http://webservice.servicos.shsg.bradseg.com.br/}atualizaEtapaCompraProdutoVO"/>
 *         &lt;element name="etapaAtual" type="{http://webservice.servicos.shsg.bradseg.com.br/}identificadorEtapaVO"/>
 *         &lt;element name="novaEtapa" type="{http://webservice.servicos.shsg.bradseg.com.br/}identificadorEtapaVO"/>
 *         &lt;element name="justificativa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "atualizaEtapaCompraRequestVO", propOrder = {
    "produto",
    "etapaAtual",
    "novaEtapa",
    "justificativa"
})
public class AtualizaEtapaCompraRequestVO {

    @XmlElement(required = true)
    protected AtualizaEtapaCompraProdutoVO produto;
    @XmlElement(required = true)
    protected IdentificadorEtapaVO etapaAtual;
    @XmlElement(required = true)
    protected IdentificadorEtapaVO novaEtapa;
    protected String justificativa;

    /**
     * Gets the value of the produto property.
     * 
     * @return
     *     possible object is
     *     {@link AtualizaEtapaCompraProdutoVO }
     *     
     */
    public AtualizaEtapaCompraProdutoVO getProduto() {
        return produto;
    }

    /**
     * Sets the value of the produto property.
     * 
     * @param value
     *     allowed object is
     *     {@link AtualizaEtapaCompraProdutoVO }
     *     
     */
    public void setProduto(AtualizaEtapaCompraProdutoVO value) {
        this.produto = value;
    }

    /**
     * Gets the value of the etapaAtual property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificadorEtapaVO }
     *     
     */
    public IdentificadorEtapaVO getEtapaAtual() {
        return etapaAtual;
    }

    /**
     * Sets the value of the etapaAtual property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificadorEtapaVO }
     *     
     */
    public void setEtapaAtual(IdentificadorEtapaVO value) {
        this.etapaAtual = value;
    }

    /**
     * Gets the value of the novaEtapa property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificadorEtapaVO }
     *     
     */
    public IdentificadorEtapaVO getNovaEtapa() {
        return novaEtapa;
    }

    /**
     * Sets the value of the novaEtapa property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificadorEtapaVO }
     *     
     */
    public void setNovaEtapa(IdentificadorEtapaVO value) {
        this.novaEtapa = value;
    }

    /**
     * Gets the value of the justificativa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJustificativa() {
        return justificativa;
    }

    /**
     * Sets the value of the justificativa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJustificativa(String value) {
        this.justificativa = value;
    }

}
