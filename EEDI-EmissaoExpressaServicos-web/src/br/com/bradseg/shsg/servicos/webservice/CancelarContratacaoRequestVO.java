
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cancelarContratacaoRequestVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cancelarContratacaoRequestVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="produto" type="{http://webservice.servicos.shsg.bradseg.com.br/}atualizaEtapaCompraProdutoVO"/>
 *         &lt;element name="chaveOrigem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancelarContratacaoRequestVO", propOrder = {
    "produto",
    "chaveOrigem"
})
public class CancelarContratacaoRequestVO {

    @XmlElement(required = true)
    protected AtualizaEtapaCompraProdutoVO produto;
    @XmlElement(required = true)
    protected String chaveOrigem;

    /**
     * Gets the value of the produto property.
     * 
     * @return
     *     possible object is
     *     {@link AtualizaEtapaCompraProdutoVO }
     *     
     */
    public AtualizaEtapaCompraProdutoVO getProduto() {
        return produto;
    }

    /**
     * Sets the value of the produto property.
     * 
     * @param value
     *     allowed object is
     *     {@link AtualizaEtapaCompraProdutoVO }
     *     
     */
    public void setProduto(AtualizaEtapaCompraProdutoVO value) {
        this.produto = value;
    }

    /**
     * Gets the value of the chaveOrigem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChaveOrigem() {
        return chaveOrigem;
    }

    /**
     * Sets the value of the chaveOrigem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChaveOrigem(String value) {
        this.chaveOrigem = value;
    }

}
