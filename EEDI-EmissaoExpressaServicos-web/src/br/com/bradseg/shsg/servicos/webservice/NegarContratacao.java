
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for negarContratacao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="negarContratacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="negarContratacao" type="{http://webservice.servicos.shsg.bradseg.com.br/}negarContratacaoRequestVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "negarContratacao", propOrder = {
    "negarContratacao"
})
public class NegarContratacao {

    protected NegarContratacaoRequestVO negarContratacao;

    /**
     * Gets the value of the negarContratacao property.
     * 
     * @return
     *     possible object is
     *     {@link NegarContratacaoRequestVO }
     *     
     */
    public NegarContratacaoRequestVO getNegarContratacao() {
        return negarContratacao;
    }

    /**
     * Sets the value of the negarContratacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link NegarContratacaoRequestVO }
     *     
     */
    public void setNegarContratacao(NegarContratacaoRequestVO value) {
        this.negarContratacao = value;
    }

}
