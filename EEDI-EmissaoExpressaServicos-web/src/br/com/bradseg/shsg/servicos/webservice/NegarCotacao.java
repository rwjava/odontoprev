
package br.com.bradseg.shsg.servicos.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for negarCotacao complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="negarCotacao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="negarCotacao" type="{http://webservice.servicos.shsg.bradseg.com.br/}negarCotacaoRequestVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "negarCotacao", propOrder = {
    "negarCotacao"
})
public class NegarCotacao {

    protected NegarCotacaoRequestVO negarCotacao;

    /**
     * Gets the value of the negarCotacao property.
     * 
     * @return
     *     possible object is
     *     {@link NegarCotacaoRequestVO }
     *     
     */
    public NegarCotacaoRequestVO getNegarCotacao() {
        return negarCotacao;
    }

    /**
     * Sets the value of the negarCotacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link NegarCotacaoRequestVO }
     *     
     */
    public void setNegarCotacao(NegarCotacaoRequestVO value) {
        this.negarCotacao = value;
    }

}
