package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ArrayOfCorrentistasPorFiltrosVO complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCorrentistasPorFiltrosVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorrentistasPorFiltrosVO" type="{http://vo.model.servicos.bucb.bradseg.com.br}CorrentistasPorFiltrosVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCorrentistasPorFiltrosVO", propOrder = { "correntistasPorFiltrosVO" })
public class ArrayOfCorrentistasPorFiltrosVO implements Serializable {

	private static final long serialVersionUID = 7408941744984614396L;
	@XmlElement(name = "CorrentistasPorFiltrosVO", nillable = true)
	protected List<CorrentistasPorFiltrosVO> correntistasPorFiltrosVO;

	/**
	 * Gets the value of the correntistasPorFiltrosVO property.
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
	 * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
	 * the correntistasPorFiltrosVO property.
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getCorrentistasPorFiltrosVO().add(newItem);
	 * </pre>
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link CorrentistasPorFiltrosVO }
	 */
	public List<CorrentistasPorFiltrosVO> getCorrentistasPorFiltrosVO() {
		if (correntistasPorFiltrosVO == null) {
			correntistasPorFiltrosVO = new ArrayList<CorrentistasPorFiltrosVO>();
		}
		return this.correntistasPorFiltrosVO;
	}

}
