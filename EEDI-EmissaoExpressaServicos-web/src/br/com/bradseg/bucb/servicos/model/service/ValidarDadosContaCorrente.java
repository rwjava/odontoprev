
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ValidarDadosContaCorrenteVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ipCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="validarDadosVO" type="{http://vo.model.servicos.bucb.bradseg.com.br}ValidarDadosContaCorrenteVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ipCliente",
    "userID",
    "validarDadosVO"
})
@XmlRootElement(name = "validarDadosContaCorrente")
public class ValidarDadosContaCorrente {

    @XmlElement(required = true, nillable = true)
    protected String ipCliente;
    @XmlElement(required = true, nillable = true)
    protected String userID;
    @XmlElement(required = true, nillable = true)
    protected ValidarDadosContaCorrenteVO validarDadosVO;

    /**
     * Gets the value of the ipCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpCliente() {
        return ipCliente;
    }

    /**
     * Sets the value of the ipCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpCliente(String value) {
        this.ipCliente = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the validarDadosVO property.
     * 
     * @return
     *     possible object is
     *     {@link ValidarDadosContaCorrenteVO }
     *     
     */
    public ValidarDadosContaCorrenteVO getValidarDadosVO() {
        return validarDadosVO;
    }

    /**
     * Sets the value of the validarDadosVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidarDadosContaCorrenteVO }
     *     
     */
    public void setValidarDadosVO(ValidarDadosContaCorrenteVO value) {
        this.validarDadosVO = value;
    }

}
