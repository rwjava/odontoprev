
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for ContaCorrenteIdBucVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContaCorrenteIdBucVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="agenciaDestino" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoPosto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoSegmento" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="conta" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="contaDestino" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="contaSalario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descricaoPosto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descricaoSegmento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvAgencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvConta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContaDestino" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="indicadorTragueamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="razao" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="situacao" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="tipoConta" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="titularidade" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContaCorrenteIdBucVO", propOrder = {
    "agencia",
    "agenciaDestino",
    "codigoPosto",
    "codigoSegmento",
    "conta",
    "contaDestino",
    "contaSalario",
    "descricaoPosto",
    "descricaoSegmento",
    "dvAgencia",
    "dvConta",
    "dvContaDestino",
    "indicadorTragueamento",
    "razao",
    "situacao",
    "tipoConta",
    "titularidade"
})
public class ContaCorrenteIdBucVO implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected long agencia;
    protected long agenciaDestino;
    protected long codigoPosto;
    protected long codigoSegmento;
    protected long conta;
    protected long contaDestino;
    @XmlElement(required = true, nillable = true)
    protected String contaSalario;
    @XmlElement(required = true, nillable = true)
    protected String descricaoPosto;
    @XmlElement(required = true, nillable = true)
    protected String descricaoSegmento;
    @XmlElement(required = true, nillable = true)
    protected String dvAgencia;
    @XmlElement(required = true, nillable = true)
    protected String dvConta;
    @XmlElement(required = true, nillable = true)
    protected String dvContaDestino;
    @XmlElement(required = true, nillable = true)
    protected String indicadorTragueamento;
    protected long razao;
    protected long situacao;
    protected long tipoConta;
    protected long titularidade;

    /**
     * Gets the value of the agencia property.
     * 
     */
    public long getAgencia() {
        return agencia;
    }

    /**
     * Sets the value of the agencia property.
     * 
     */
    public void setAgencia(long value) {
        this.agencia = value;
    }

    /**
     * Gets the value of the agenciaDestino property.
     * 
     */
    public long getAgenciaDestino() {
        return agenciaDestino;
    }

    /**
     * Sets the value of the agenciaDestino property.
     * 
     */
    public void setAgenciaDestino(long value) {
        this.agenciaDestino = value;
    }

    /**
     * Gets the value of the codigoPosto property.
     * 
     */
    public long getCodigoPosto() {
        return codigoPosto;
    }

    /**
     * Sets the value of the codigoPosto property.
     * 
     */
    public void setCodigoPosto(long value) {
        this.codigoPosto = value;
    }

    /**
     * Gets the value of the codigoSegmento property.
     * 
     */
    public long getCodigoSegmento() {
        return codigoSegmento;
    }

    /**
     * Sets the value of the codigoSegmento property.
     * 
     */
    public void setCodigoSegmento(long value) {
        this.codigoSegmento = value;
    }

    /**
     * Gets the value of the conta property.
     * 
     */
    public long getConta() {
        return conta;
    }

    /**
     * Sets the value of the conta property.
     * 
     */
    public void setConta(long value) {
        this.conta = value;
    }

    /**
     * Gets the value of the contaDestino property.
     * 
     */
    public long getContaDestino() {
        return contaDestino;
    }

    /**
     * Sets the value of the contaDestino property.
     * 
     */
    public void setContaDestino(long value) {
        this.contaDestino = value;
    }

    /**
     * Gets the value of the contaSalario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContaSalario() {
        return contaSalario;
    }

    /**
     * Sets the value of the contaSalario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContaSalario(String value) {
        this.contaSalario = value;
    }

    /**
     * Gets the value of the descricaoPosto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoPosto() {
        return descricaoPosto;
    }

    /**
     * Sets the value of the descricaoPosto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoPosto(String value) {
        this.descricaoPosto = value;
    }

    /**
     * Gets the value of the descricaoSegmento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescricaoSegmento() {
        return descricaoSegmento;
    }

    /**
     * Sets the value of the descricaoSegmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescricaoSegmento(String value) {
        this.descricaoSegmento = value;
    }

    /**
     * Gets the value of the dvAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvAgencia() {
        return dvAgencia;
    }

    /**
     * Sets the value of the dvAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvAgencia(String value) {
        this.dvAgencia = value;
    }

    /**
     * Gets the value of the dvConta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvConta() {
        return dvConta;
    }

    /**
     * Sets the value of the dvConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvConta(String value) {
        this.dvConta = value;
    }

    /**
     * Gets the value of the dvContaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaDestino() {
        return dvContaDestino;
    }

    /**
     * Sets the value of the dvContaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaDestino(String value) {
        this.dvContaDestino = value;
    }

    /**
     * Gets the value of the indicadorTragueamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicadorTragueamento() {
        return indicadorTragueamento;
    }

    /**
     * Sets the value of the indicadorTragueamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicadorTragueamento(String value) {
        this.indicadorTragueamento = value;
    }

    /**
     * Gets the value of the razao property.
     * 
     */
    public long getRazao() {
        return razao;
    }

    /**
     * Sets the value of the razao property.
     * 
     */
    public void setRazao(long value) {
        this.razao = value;
    }

    /**
     * Gets the value of the situacao property.
     * 
     */
    public long getSituacao() {
        return situacao;
    }

    /**
     * Sets the value of the situacao property.
     * 
     */
    public void setSituacao(long value) {
        this.situacao = value;
    }

    /**
     * Gets the value of the tipoConta property.
     * 
     */
    public long getTipoConta() {
        return tipoConta;
    }

    /**
     * Sets the value of the tipoConta property.
     * 
     */
    public void setTipoConta(long value) {
        this.tipoConta = value;
    }

    /**
     * Gets the value of the titularidade property.
     * 
     */
    public long getTitularidade() {
        return titularidade;
    }

    /**
     * Sets the value of the titularidade property.
     * 
     */
    public void setTitularidade(long value) {
        this.titularidade = value;
    }

}
