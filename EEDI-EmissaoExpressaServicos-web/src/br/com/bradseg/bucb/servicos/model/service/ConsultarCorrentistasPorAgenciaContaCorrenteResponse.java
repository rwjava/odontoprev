
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ContaCorrenteVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="consultarCorrentistasPorAgenciaContaCorrenteReturn" type="{http://vo.model.servicos.bucb.bradseg.com.br}ContaCorrenteVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarCorrentistasPorAgenciaContaCorrenteReturn"
})
@XmlRootElement(name = "consultarCorrentistasPorAgenciaContaCorrenteResponse")
public class ConsultarCorrentistasPorAgenciaContaCorrenteResponse {

    @XmlElement(required = true, nillable = true)
    protected ContaCorrenteVO consultarCorrentistasPorAgenciaContaCorrenteReturn;

    /**
     * Gets the value of the consultarCorrentistasPorAgenciaContaCorrenteReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ContaCorrenteVO }
     *     
     */
    public ContaCorrenteVO getConsultarCorrentistasPorAgenciaContaCorrenteReturn() {
        return consultarCorrentistasPorAgenciaContaCorrenteReturn;
    }

    /**
     * Sets the value of the consultarCorrentistasPorAgenciaContaCorrenteReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContaCorrenteVO }
     *     
     */
    public void setConsultarCorrentistasPorAgenciaContaCorrenteReturn(ContaCorrenteVO value) {
        this.consultarCorrentistasPorAgenciaContaCorrenteReturn = value;
    }

}
