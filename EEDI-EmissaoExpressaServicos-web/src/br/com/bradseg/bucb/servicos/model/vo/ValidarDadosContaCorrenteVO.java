
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidarDadosContaCorrenteVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidarDadosContaCorrenteVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cdagencia" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cdvCtaCorr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cdvValido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cpfCnpj" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="nrctaCorr" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidarDadosContaCorrenteVO", propOrder = {
    "cdagencia",
    "cdvCtaCorr",
    "cdvValido",
    "cpfCnpj",
    "nrctaCorr"
})
public class ValidarDadosContaCorrenteVO {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cdagencia;
    @XmlElement(required = true, nillable = true)
    protected String cdvCtaCorr;
    @XmlElement(required = true, nillable = true)
    protected String cdvValido;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cpfCnpj;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long nrctaCorr;

    /**
     * Gets the value of the cdagencia property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCdagencia() {
        return cdagencia;
    }

    /**
     * Sets the value of the cdagencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCdagencia(Long value) {
        this.cdagencia = value;
    }

    /**
     * Gets the value of the cdvCtaCorr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdvCtaCorr() {
        return cdvCtaCorr;
    }

    /**
     * Sets the value of the cdvCtaCorr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdvCtaCorr(String value) {
        this.cdvCtaCorr = value;
    }

    /**
     * Gets the value of the cdvValido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdvValido() {
        return cdvValido;
    }

    /**
     * Sets the value of the cdvValido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdvValido(String value) {
        this.cdvValido = value;
    }

    /**
     * Gets the value of the cpfCnpj property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Sets the value of the cpfCnpj property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCpfCnpj(Long value) {
        this.cpfCnpj = value;
    }

    /**
     * Gets the value of the nrctaCorr property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNrctaCorr() {
        return nrctaCorr;
    }

    /**
     * Sets the value of the nrctaCorr property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNrctaCorr(Long value) {
        this.nrctaCorr = value;
    }

}
