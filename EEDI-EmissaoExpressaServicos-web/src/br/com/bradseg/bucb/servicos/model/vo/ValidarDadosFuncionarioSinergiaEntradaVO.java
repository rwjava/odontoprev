
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidarDadosFuncionarioSinergiaEntradaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidarDadosFuncionarioSinergiaEntradaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="matricula" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="corretor" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidarDadosFuncionarioSinergiaEntradaVO", propOrder = {
    "matricula",
    "corretor"
})
public class ValidarDadosFuncionarioSinergiaEntradaVO {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long matricula;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long corretor;

    /**
     * Gets the value of the matricula property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMatricula() {
        return matricula;
    }

    /**
     * Sets the value of the matricula property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMatricula(Long value) {
        this.matricula = value;
    }

    /**
     * Gets the value of the corretor property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCorretor() {
        return corretor;
    }

    /**
     * Sets the value of the corretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCorretor(Long value) {
        this.corretor = value;
    }

}
