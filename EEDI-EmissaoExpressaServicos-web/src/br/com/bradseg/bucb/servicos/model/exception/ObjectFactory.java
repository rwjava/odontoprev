
package br.com.bradseg.bucb.servicos.model.exception;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.bradseg.bucb.servicos.model.exception package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WebServiceIntegrationException_QNAME = new QName("http" + "://exception.model.servicos.bucb.bradseg.com.br", "WebServiceIntegrationException");
    private final static QName _WebServiceBusinessException_QNAME = new QName("http" + "://exception.model.servicos.bucb.bradseg.com.br", "WebServiceBusinessException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.bradseg.bucb.servicos.model.exception
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WebServiceBusinessException }
     * 
     */
    public WebServiceBusinessException createWebServiceBusinessException() {
        return new WebServiceBusinessException();
    }

    /**
     * Create an instance of {@link WebServiceIntegrationException }
     * 
     */
    public WebServiceIntegrationException createWebServiceIntegrationException() {
        return new WebServiceIntegrationException();
    }

    /**
     * Create an instance of {@link ArrayOf143104033NillableString }
     * 
     */
    public ArrayOf143104033NillableString createArrayOf143104033NillableString() {
        return new ArrayOf143104033NillableString();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebServiceIntegrationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://exception.model.servicos.bucb.bradseg.com.br", name = "WebServiceIntegrationException")
    public JAXBElement<WebServiceIntegrationException> createWebServiceIntegrationException(WebServiceIntegrationException value) {
        return new JAXBElement<WebServiceIntegrationException>(_WebServiceIntegrationException_QNAME, WebServiceIntegrationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebServiceBusinessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http" + "://exception.model.servicos.bucb.bradseg.com.br", name = "WebServiceBusinessException")
    public JAXBElement<WebServiceBusinessException> createWebServiceBusinessException(WebServiceBusinessException value) {
        return new JAXBElement<WebServiceBusinessException>(_WebServiceBusinessException_QNAME, WebServiceBusinessException.class, null, value);
    }

}
