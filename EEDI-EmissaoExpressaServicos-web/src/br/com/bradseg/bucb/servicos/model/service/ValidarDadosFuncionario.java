
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ipCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="matricula" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="conta" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ipCliente",
    "userID",
    "matricula",
    "agencia",
    "conta"
})
@XmlRootElement(name = "validarDadosFuncionario")
public class ValidarDadosFuncionario {

    @XmlElement(required = true, nillable = true)
    protected String ipCliente;
    @XmlElement(required = true, nillable = true)
    protected String userID;
    protected long matricula;
    protected long agencia;
    protected long conta;

    /**
     * Gets the value of the ipCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIpCliente() {
        return ipCliente;
    }

    /**
     * Sets the value of the ipCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIpCliente(String value) {
        this.ipCliente = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the matricula property.
     * 
     */
    public long getMatricula() {
        return matricula;
    }

    /**
     * Sets the value of the matricula property.
     * 
     */
    public void setMatricula(long value) {
        this.matricula = value;
    }

    /**
     * Gets the value of the agencia property.
     * 
     */
    public long getAgencia() {
        return agencia;
    }

    /**
     * Sets the value of the agencia property.
     * 
     */
    public void setAgencia(long value) {
        this.agencia = value;
    }

    /**
     * Gets the value of the conta property.
     * 
     */
    public long getConta() {
        return conta;
    }

    /**
     * Sets the value of the conta property.
     * 
     */
    public void setConta(long value) {
        this.conta = value;
    }

}
