
package br.com.bradseg.bucb.servicos.model.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import br.com.bradseg.bucb.servicos.model.vo.ContaCorrenteFuncionarioIdBucVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="obterContaCorrentePorIdBucReturn" type="{http://vo.model.servicos.bucb.bradseg.com.br}ContaCorrenteFuncionarioIdBucVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "obterContaCorrentePorIdBucReturn"
})
@XmlRootElement(name = "obterContaCorrentePorIdBucResponse")
public class ObterContaCorrentePorIdBucResponse {

    @XmlElement(required = true, nillable = true)
    protected ContaCorrenteFuncionarioIdBucVO obterContaCorrentePorIdBucReturn;

    /**
     * Gets the value of the obterContaCorrentePorIdBucReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ContaCorrenteFuncionarioIdBucVO }
     *     
     */
    public ContaCorrenteFuncionarioIdBucVO getObterContaCorrentePorIdBucReturn() {
        return obterContaCorrentePorIdBucReturn;
    }

    /**
     * Sets the value of the obterContaCorrentePorIdBucReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContaCorrenteFuncionarioIdBucVO }
     *     
     */
    public void setObterContaCorrentePorIdBucReturn(ContaCorrenteFuncionarioIdBucVO value) {
        this.obterContaCorrentePorIdBucReturn = value;
    }

}
