
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PessoaContaCorrenteEntradaVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PessoaContaCorrenteEntradaVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoAgencia" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codigoTipoConta" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numeroContaCorrente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="titular" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PessoaContaCorrenteEntradaVO", propOrder = {
    "codigoAgencia",
    "codigoTipoConta",
    "numeroContaCorrente",
    "titular"
})
public class PessoaContaCorrenteEntradaVO {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoAgencia;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long codigoTipoConta;
    @XmlElement(required = true, nillable = true)
    protected String numeroContaCorrente;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long titular;

    /**
     * Gets the value of the codigoAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Sets the value of the codigoAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoAgencia(Long value) {
        this.codigoAgencia = value;
    }

    /**
     * Gets the value of the codigoTipoConta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCodigoTipoConta() {
        return codigoTipoConta;
    }

    /**
     * Sets the value of the codigoTipoConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCodigoTipoConta(Long value) {
        this.codigoTipoConta = value;
    }

    /**
     * Gets the value of the numeroContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    /**
     * Sets the value of the numeroContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroContaCorrente(String value) {
        this.numeroContaCorrente = value;
    }

    /**
     * Gets the value of the titular property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTitular() {
        return titular;
    }

    /**
     * Sets the value of the titular property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTitular(Long value) {
        this.titular = value;
    }

}
