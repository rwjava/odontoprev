
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidarDadosFuncionarioVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidarDadosFuncionarioVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="conta" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="matricula" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="agenciaSaida" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="contaSaida" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="contaSal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvAgenciaSaida" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContaSaida" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="indDiretor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="situacao" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cpf" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidarDadosFuncionarioVO", propOrder = {
    "agencia",
    "conta",
    "matricula",
    "agenciaSaida",
    "contaSaida",
    "contaSal",
    "dvAgenciaSaida",
    "dvContaSaida",
    "indDiretor",
    "nome",
    "situacao",
    "sucursal",
    "cpf"
})
public class ValidarDadosFuncionarioVO {

    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long agencia;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long conta;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long matricula;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long agenciaSaida;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long contaSaida;
    @XmlElement(required = true, nillable = true)
    protected String contaSal;
    @XmlElement(required = true, nillable = true)
    protected String dvAgenciaSaida;
    @XmlElement(required = true, nillable = true)
    protected String dvContaSaida;
    @XmlElement(required = true, nillable = true)
    protected String indDiretor;
    @XmlElement(required = true, nillable = true)
    protected String nome;
    @XmlElement(required = true, nillable = true)
    protected String situacao;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long sucursal;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long cpf;

    /**
     * Gets the value of the agencia property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgencia() {
        return agencia;
    }

    /**
     * Sets the value of the agencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgencia(Long value) {
        this.agencia = value;
    }

    /**
     * Gets the value of the conta property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getConta() {
        return conta;
    }

    /**
     * Sets the value of the conta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setConta(Long value) {
        this.conta = value;
    }

    /**
     * Gets the value of the matricula property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMatricula() {
        return matricula;
    }

    /**
     * Sets the value of the matricula property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMatricula(Long value) {
        this.matricula = value;
    }

    /**
     * Gets the value of the agenciaSaida property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgenciaSaida() {
        return agenciaSaida;
    }

    /**
     * Sets the value of the agenciaSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgenciaSaida(Long value) {
        this.agenciaSaida = value;
    }

    /**
     * Gets the value of the contaSaida property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContaSaida() {
        return contaSaida;
    }

    /**
     * Sets the value of the contaSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContaSaida(Long value) {
        this.contaSaida = value;
    }

    /**
     * Gets the value of the contaSal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContaSal() {
        return contaSal;
    }

    /**
     * Sets the value of the contaSal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContaSal(String value) {
        this.contaSal = value;
    }

    /**
     * Gets the value of the dvAgenciaSaida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvAgenciaSaida() {
        return dvAgenciaSaida;
    }

    /**
     * Sets the value of the dvAgenciaSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvAgenciaSaida(String value) {
        this.dvAgenciaSaida = value;
    }

    /**
     * Gets the value of the dvContaSaida property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaSaida() {
        return dvContaSaida;
    }

    /**
     * Sets the value of the dvContaSaida property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaSaida(String value) {
        this.dvContaSaida = value;
    }

    /**
     * Gets the value of the indDiretor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndDiretor() {
        return indDiretor;
    }

    /**
     * Sets the value of the indDiretor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndDiretor(String value) {
        this.indDiretor = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the situacao property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * Sets the value of the situacao property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSituacao(String value) {
        this.situacao = value;
    }

    /**
     * Gets the value of the sucursal property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSucursal() {
        return sucursal;
    }

    /**
     * Sets the value of the sucursal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSucursal(Long value) {
        this.sucursal = value;
    }

    /**
     * Gets the value of the cpf property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCpf() {
        return cpf;
    }

    /**
     * Sets the value of the cpf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCpf(Long value) {
        this.cpf = value;
    }

}
