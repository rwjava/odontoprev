
package br.com.bradseg.bucb.servicos.model.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContaCorrenteVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContaCorrenteVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoSegmento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoTipoConta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dvAgencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContaCorrente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoAgencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroContaCorrente" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="correntistaVO" type="{http://vo.model.servicos.bucb.bradseg.com.br}CorrentistaVO"/>
 *         &lt;element name="correntistaArrayVO" type="{http://vo.model.servicos.bucb.bradseg.com.br}ArrayOfCorrentistaVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContaCorrenteVO", propOrder = {
    "codigoSegmento",
    "codigoTipoConta",
    "dvAgencia",
    "dvContaCorrente",
    "codigoAgencia",
    "numeroContaCorrente",
    "correntistaVO",
    "correntistaArrayVO"
})
public class ContaCorrenteVO {

    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoSegmento;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoTipoConta;
    @XmlElement(required = true, nillable = true)
    protected String dvAgencia;
    @XmlElement(required = true, nillable = true)
    protected String dvContaCorrente;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoAgencia;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long numeroContaCorrente;
    @XmlElement(required = true, nillable = true)
    protected CorrentistaVO correntistaVO;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfCorrentistaVO correntistaArrayVO;

    /**
     * Gets the value of the codigoSegmento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoSegmento() {
        return codigoSegmento;
    }

    /**
     * Sets the value of the codigoSegmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoSegmento(Integer value) {
        this.codigoSegmento = value;
    }

    /**
     * Gets the value of the codigoTipoConta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoTipoConta() {
        return codigoTipoConta;
    }

    /**
     * Sets the value of the codigoTipoConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoTipoConta(Integer value) {
        this.codigoTipoConta = value;
    }

    /**
     * Gets the value of the dvAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvAgencia() {
        return dvAgencia;
    }

    /**
     * Sets the value of the dvAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvAgencia(String value) {
        this.dvAgencia = value;
    }

    /**
     * Gets the value of the dvContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaCorrente() {
        return dvContaCorrente;
    }

    /**
     * Sets the value of the dvContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaCorrente(String value) {
        this.dvContaCorrente = value;
    }

    /**
     * Gets the value of the codigoAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Sets the value of the codigoAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoAgencia(Integer value) {
        this.codigoAgencia = value;
    }

    /**
     * Gets the value of the numeroContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    /**
     * Sets the value of the numeroContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroContaCorrente(Long value) {
        this.numeroContaCorrente = value;
    }

    /**
     * Gets the value of the correntistaVO property.
     * 
     * @return
     *     possible object is
     *     {@link CorrentistaVO }
     *     
     */
    public CorrentistaVO getCorrentistaVO() {
        return correntistaVO;
    }

    /**
     * Sets the value of the correntistaVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrentistaVO }
     *     
     */
    public void setCorrentistaVO(CorrentistaVO value) {
        this.correntistaVO = value;
    }

    /**
     * Gets the value of the correntistaArrayVO property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCorrentistaVO }
     *     
     */
    public ArrayOfCorrentistaVO getCorrentistaArrayVO() {
        return correntistaArrayVO;
    }

    /**
     * Sets the value of the correntistaArrayVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCorrentistaVO }
     *     
     */
    public void setCorrentistaArrayVO(ArrayOfCorrentistaVO value) {
        this.correntistaArrayVO = value;
    }

}
