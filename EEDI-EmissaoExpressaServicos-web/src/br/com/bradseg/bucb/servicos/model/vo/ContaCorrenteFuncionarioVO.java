
package br.com.bradseg.bucb.servicos.model.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for ContaCorrenteFuncionarioVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContaCorrenteFuncionarioVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoSegmento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoTipoConta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="dvAgencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContaCorrente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoAgencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroContaCorrente" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="contaSalario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="titularidade" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContaCorrenteFuncionarioVO", propOrder = {
    "codigoSegmento",
    "codigoTipoConta",
    "dvAgencia",
    "dvContaCorrente",
    "codigoAgencia",
    "numeroContaCorrente",
    "contaSalario",
    "titularidade"
})
public class ContaCorrenteFuncionarioVO implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoSegmento;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoTipoConta;
    @XmlElement(required = true, nillable = true)
    protected String dvAgencia;
    @XmlElement(required = true, nillable = true)
    protected String dvContaCorrente;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer codigoAgencia;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long numeroContaCorrente;
    @XmlElement(required = true, nillable = true)
    protected String contaSalario;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer titularidade;

    /**
     * Gets the value of the codigoSegmento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoSegmento() {
        return codigoSegmento;
    }

    /**
     * Sets the value of the codigoSegmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoSegmento(Integer value) {
        this.codigoSegmento = value;
    }

    /**
     * Gets the value of the codigoTipoConta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoTipoConta() {
        return codigoTipoConta;
    }

    /**
     * Sets the value of the codigoTipoConta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoTipoConta(Integer value) {
        this.codigoTipoConta = value;
    }

    /**
     * Gets the value of the dvAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvAgencia() {
        return dvAgencia;
    }

    /**
     * Sets the value of the dvAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvAgencia(String value) {
        this.dvAgencia = value;
    }

    /**
     * Gets the value of the dvContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContaCorrente() {
        return dvContaCorrente;
    }

    /**
     * Sets the value of the dvContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContaCorrente(String value) {
        this.dvContaCorrente = value;
    }

    /**
     * Gets the value of the codigoAgencia property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodigoAgencia() {
        return codigoAgencia;
    }

    /**
     * Sets the value of the codigoAgencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodigoAgencia(Integer value) {
        this.codigoAgencia = value;
    }

    /**
     * Gets the value of the numeroContaCorrente property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    /**
     * Sets the value of the numeroContaCorrente property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroContaCorrente(Long value) {
        this.numeroContaCorrente = value;
    }

    /**
     * Gets the value of the contaSalario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContaSalario() {
        return contaSalario;
    }

    /**
     * Sets the value of the contaSalario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContaSalario(String value) {
        this.contaSalario = value;
    }

    /**
     * Gets the value of the titularidade property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTitularidade() {
        return titularidade;
    }

    /**
     * Sets the value of the titularidade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTitularidade(Integer value) {
        this.titularidade = value;
    }

}
