<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title><decorator:title default="EEDI-EmissaoExpressaServicos"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	
	<!-- Bootstrap styles -->
	<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/bootstrap.min.css'/>" />
	
	<!-- External styles -->
	<link type="text/css" rel="stylesheet" href="<s:url value='/includes/css/vendor/jquery-ui.min.css'/>" />
	
	<!-- WDEV styles -->
	<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/wdev-required.css'/>" />
	
	<!-- Bradesco styles -->
	<!-- link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/master.css'/>" /-->
	
	<!-- Personalizado styles -->
	<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/estilos.css'/>" />
	
	<!-- Font ionicons -->
	<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/ionicons.css'/>" />
	
	<!-- External plugins -->
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-1.9.1.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.inputmask.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-ui.custom.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.ui.datepicker-pt-BR.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/vendor/popper.min.js'/>"></script>
	
	<!-- WDEV plugins -->
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-datatypes.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-ajax.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-required.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/includes/js/wdev-inputcontrol.js'/>"></script>
	
	<!-- BOOTSTRAP plugins -->
	<script type="text/javascript" src="<s:url value='/includes/js/bootstrap.min.js'/>"></script>
	
	<script type="text/javascript" src="<s:url value='/includes/js/servicos.js'/>"></script>
	
	<decorator:head />
</head>
<body>
<table id="tabela_principal">
	<tr>
		<td>
			<span class="titulo"><decorator:getProperty property="meta.titulo" default="EEDI-EmissaoExpressaServicos"/></span> <br />
			<span class="subtitulo"><decorator:getProperty property="meta.subtitulo" /></span> <br />
			<s:if test="hasFieldErrors()">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_verm" id="msgErros">
					<tbody>
						<tr>
							<td><s:iterator value="fieldErrors">
								<li><s:property value="value[0]" /></li>
							</s:iterator></td>
						</tr>
					</tbody>
				</table>
			</s:if>
			<s:if test="hasActionErrors()">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_verm" id="msgErros">
					<tbody>
						<tr>
							<td><s:iterator value="actionErrors">
								<li><s:property /></li>
							</s:iterator></td>
						</tr>
					</tbody>
				</table>
			</s:if>
			<s:if test="hasActionMessages()">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabela_sucesso" id="msgSucesso">
					<tbody>
						<tr>
							<td><s:iterator value="actionMessages">
								<li><s:property /></li>
							</s:iterator></td>
						</tr>
					</tbody>
				</table>
			</s:if>
			 <div class="container-fluid bg-menu">
				 <div class="row">
					<div class="col-sm-12">
						<nav class="nav nav-pills flex-column flex-sm-row estilo-menu">
						  <a class="flex-sm-fill text-sm-center nav-link active" href="../EEDI-EmissaoExpressaServicos/inicio.do"><i class="icon ion-home"></i> HOME</a>
						  <a class="flex-sm-fill text-sm-center nav-link" href="../EEDI-EmissaoExpressaServicos/statusServicos.do"><i class="icon ion-connection-bars"></i> STATUS DOS SERVI�OS</a>
						  <a class="flex-sm-fill text-sm-center nav-link" href="#"><i class="icon ion-forward"></i> OUTROS</a>
						</nav>
					</div>
				</div>
			</div>
			<br><br>
			<decorator:body />
		</td>
	</tr>
</table>

<s:if test="hasActionMessages()">
	<script>
		$("#msgSucesso").fadeOut(7000);
	</script>
</s:if>
</body>
</html>
