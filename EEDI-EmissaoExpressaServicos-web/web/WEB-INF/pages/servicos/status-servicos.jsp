<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta name="titulo" content="EEDI-EmissaoExpressaServicos" />
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">				
				<br>
				<div id="accordion" role="tablist">
					<div class="card">
				    	<div class="card-header" role="tab" id="headingOne">
				      		<h6 class="mb-0">
				        	<a data-toggle="collapse" class="collapsed" id="primeiroServico" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				        		<table class="tabela_principal">
									<tr>
										<td class="td_collapse" width="450px" align="left"><i class="icon ion-clipboard icon-status"></i> MovimentoPropostaWebService</td>
										<td class="td_collapse" align="left"><i class="icon ion-link icon-status"></i> <a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td class="td_collapse" align="left"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
								</table>
				        	</a>
				      		</h6>
				    	</div>
				
				    	<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
				      		<div class="card-body">
								<table class="tabela_interna">
									<tr>
										<th width="150px">Nome</th>
										<th width="700px">Wsdl</th>
										<th width="50px">Status</th>
									</tr>
									<tr>
										<td align="center">listarMovimentoPropostaPorSequencial</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
								</table>
				      		</div>
				    	</div>
				  	</div>
				  	
				  	<br>
				  	<div class="card">
					    <div class="card-header" role="tab" id="headingTwo">
					    	<h6 class="mb-0">
					        	<a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					        		<table class="tabela_principal">
										<tr>
											<td class="td_collapse" width="450px" align="left"><i class="icon ion-clipboard icon-status"></i> Proposta100PorCentoCorretorWebService</td>
											<td class="td_collapse" align="left"><i class="icon ion-link icon-status"></i> <a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/Proposta100PorCentoCorretorWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/Proposta100PorCentoCorretorWebService?wsdl</a></td>
											<td class="td_collapse" align="left"><i class="icon ion-checkmark-circled"></i></td>
										</tr>
									</table>
					        	</a>
					      	</h6>
					    </div>
					    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
					    	<div class="card-body">
					    		<table class="tabela_interna">
									<tr>
										<th width="150px">Nome</th>
										<th width="500px">Wsdl</th>
										<th width="50px">Status</th>
									</tr>
									<tr>
										<td align="center">listarPlanosVigentePorCanal</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPropostasPorProponente</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPropostasAcompanhamento</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">gerarBoleto</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">salvarRascunho</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarParaRelatorioAcompanhamento</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">gerarPDFProposta</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPropostasPorBeneficiario</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">atualizarStatusDaPropostaNoShoppingDeSeguros</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">gerarRelatorioAcompanhamentoEEDI</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPorFiltro</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterTermoDeAdesao</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarCanaisVenda</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">salvarPropostaVazia</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">finalizar</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">consultarProposta</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPlanoDisponivelParaVenda</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">cancelarPropostaPorProtocolo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterCanalVendaCallCenter</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterPlanoPorCodigo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterPlanoAnsPorCodigo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPropostasIntranetPorFiltro</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterPropostaPorCodigo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterCanalVendaPorCodigo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterPorCodigo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">cancelarPropostaShoppingSeguros</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">gerarRelatorioAcompanhamento</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">finalizarProposta</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">cancelarPropostasDoSite100CorretorComDataDeValidadeExpirada</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">cancelar</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/MovimentoPropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
								</table>
					    	</div>
					    </div>
					</div>
				  	
				  	<br>
				  	<div class="card">
				    	<div class="card-header" role="tab" id="headingThree">
				      		<h6 class="mb-0">
				        	<a data-toggle="collapse" class="collapsed" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
				        		<table class="tabela_principal">
									<tr>
										<td class="td_collapse" width="450px" align="left"><i class="icon ion-clipboard icon-status"></i> PropostaWebService</td>
										<td class="td_collapse" align="left"><i class="icon ion-link icon-status"></i> <a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td class="td_collapse" align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
								</table>
				        	</a>
				      		</h6>
				    	</div>
				
				    	<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
				      		<div class="card-body">
								<table class="tabela_interna">
									<tr>
										<th width="150px">Nome</th>
										<th width="700px">Wsdl</th>
										<th width="50px">Status</th>
									</tr>
									<tr>
										<td align="center">listarPlanosVigentePorCanal</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPropostasPorProponente</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">gerarBoleto</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">salvarRascunho</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarParaRelatorioAcompanhamento</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">gerarPDFProposta</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPropostasPorBeneficiario</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">atualizarStatusDaPropostaNoShoppingDeSeguros</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">listarPorFiltro</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterTermoDeAdesao</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">finalizar</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">cancelarPropostaPorProtocolo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterCanalVendaCallCenter</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterPlanoPorCodigo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterPropostaPorCodigo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterPorCodigo</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">obterSituacaoProposta</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">cancelarPropostaShoppingSeguros</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">gerarRelatorioAcompanhamento</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
									<tr>
										<td align="center">cancelar</td>
										<td align="center"><a target="_blank" href="http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl">http://localhost:8094/EEDI-EmissaoExpressaServicos/service/PropostaWebService?wsdl</a></td>
										<td align="center"><i class="icon ion-checkmark-circled"></i></td>
									</tr>
								</table>
				      		</div>
				    	</div>
				  	</div>
				  	
				</div>
				<br>
				<div id="setTime"></div>
			</div>
		</div>
		<br>
	</div>
</body>
</html>