<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta name="titulo" content="EEDI-EmissaoExpressaServicos" />
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="card tela_consultaMovimento">
					<div class="card-header">
						<div class="card-title">
							<h1 id="titulo_home_servicos">Consuta servi�os com Tela</h1>
						</div>
					</div>
					<div class="card-body">
						<s:form action="consultaMovimento">
							<fieldset>
								<div class="form-group">
									<label for="sucursalCorretor" class="lbl_forms">SEQUENCIAL PROPOSTA: <span class="obrigatorio">*</span></label>
									<s:textfield name="form.sequencial" cssClass="input-group form-control" />
								</div>
								<br/>
								<div class="btn-servicos" id="btn1_servicos">
									<s:submit value="Consultar" cssClass="btn btn-primary"/>
								</div>
							</fieldset>	
						</s:form>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<s:if test="movimentosProposta != null && movimentosProposta.size > 0">
					<table class="tabela_interna">
						<tr>
							<th colspan="4">MOVIMENTO(S)</th>
						</tr>
						<tr>
							<th width="150px;">Sequencial</th>
							<th width="450px;">Data Movimenta��o</th>
							<th width="250px;">Situa��o</th>
							<th width="250px;">Canal</th>
						</tr>
						<s:iterator value="movimentosProposta" var="proposta" status="status">
							<tr>
								<td align="center">
									<s:if test="canal.codigo == 8">
										<a href="visualizarProposta.do?proposta.sequencial=<s:property value='sequencial'/>" >
											<s:property value="proposta.numeroSequencial"/>
										</a>
									</s:if>
									<s:else>
										<s:property value="proposta.numeroSequencial"/>
									</s:else>
								</td>
								<td align="center"><s:property value="dataInicio"/></td>
								<td align="center"><s:property value="situacao"/></td>
								<td align="center"><s:property value="canal.nome"/></td>
							</tr>
						</s:iterator>
					</table>
				</s:if><br/><br/>
			</div>
		</div>
	</div>
</body>
</html>