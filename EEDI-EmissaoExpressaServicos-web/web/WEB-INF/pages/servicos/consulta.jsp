<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
 
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<!-- Bradesco styles -->
		<link rel="stylesheet" type="text/css" href="<s:url value='/includes/css/css.css'/>" />
		<!-- External plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-ui.custom.min.js'/>"></script>
		<script type="text/javascript" src="<s:url value="/includes/js/vendor/jquery.ui.datepicker-pt-BR.js" />" ></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.inputmask.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.blockUI.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.scrollto.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery-maskmoney.js'/>"></script>
		
		<!-- WDEV plugins -->
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-datatypes.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-ajax.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-block.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-required.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/wdev-inputcontrol.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/includes/js/vendor/jquery.formatCurrency-1.4.0.min.js'/>"></script>


<link rel="stylesheet" type="text/css"
	href="<s:url value='/includes/css/bootstrap.min.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<s:url value='/includes/css/bootstrap-dialog.css'/>" />
<script type="text/javascript"
	src="<s:url value='/includes/js/bootstrap.min.js'/>"></script>
<script type="text/javascript"
	src="<s:url value='/includes/js/bootstrap-dialog.js'/>"></script>
	
	<style>


	.button-consulta{
	-webkit-box-shadow: inset 0px 1px 0px 0px #ffffff;
	box-shadow: inset 0px 1px 0px 0px #ffffff;
	background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background: -moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color: #ededed;
	-webkit-border-top-left-radius: 6px;
	-moz-border-radius-topleft: 6px;
	border-top-left-radius: 6px;
	-webkit-border-top-right-radius: 6px;
	-moz-border-radius-topright: 6px;
	border-top-right-radius: 6px;
	-webkit-border-bottom-right-radius: 6px;
	-moz-border-radius-bottomright: 6px;
	border-bottom-right-radius: 6px;
	-webkit-border-bottom-left-radius: 6px;
	-moz-border-radius-bottomleft: 6px;
	border-bottom-left-radius: 6px;
	text-indent: 0;
	border: 1px solid #dcdcdc;
	display: inline-block;
	color: #777777;
	font-family: "Open June", montserrat;
	font-size: 15px;
	font-weight: bold;
	font-style: normal;
	height: 50px;
	line-height: 50px;
	width: 100px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
	text-transform: uppercase;
	margin-left: 0;
	cursor: pointer;
	min-width: 156px;
	overflow: visible;
	padding-left: 3px;
	padding-right: 3px;
}
</style>
	
</head>

<body>
	<s:form id="form" action="servicos/consultar.do">
	    <div class="border-header"></div>
		<br/>  
	    <div>
	  		<s:textarea rows="20" cols="100" name="textoConsulta" id="textoConsulta" cssStyle="resize: both;"></s:textarea>
	<!-- 		<input type="button" name="enviar" id="enviar" value="Enviar" /> -->
	<!-- 		<div id="successMessages"></div> -->
	<!-- 		<div id="saida"></div> -->
	    </div>
	    <br/><br/>
	    <div align="center" style="width: 820px;">
	    	<s:submit type="button" name="enviar" id="enviar" value="Enviar" cssClass="button-consulta" />
	    </div>
	    <br/><br/><br/>
	    <s:if test="detalhes != null && detalhes.size > 1">
		    <table style="width: 820px;" id="tabela_interna" class="tituloInterno">
		    	<s:iterator value="detalhes" var="linha" status="status">			    	
			    	<s:if test = "#status.index == 0">
				    	<s:iterator value="linha" var="coluna">
					    	<th>
					    		<span><c:out value="${coluna}"/></span>
					    	</th>
				    	</s:iterator>
			    	</s:if>
					<s:else>	    	
			    	<tr>
			    		<s:iterator value="linha" var="coluna">
			    				<td ><c:out value="${coluna}"/></td>
			    		</s:iterator>
			    	</tr>
			    	</s:else>
			    </s:iterator>
		    </table>
		</s:if>
		<s:elseif test="detalhes.size == 0">
			<table style="width: 820px;">
				<tr>
					<td align='center'>
						<b>Nenhum registro encontrado.</b>
					</td>
				</tr>
			</table>
		</s:elseif>
	</s:form>
</body>
</html>