<%@ page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta name="titulo" content="EEDI-EmissaoExpressaServicos" />
</head>
<body>	
	<div class="container-fluid">
		<div class="row">
	    	<div class="col-sm-4">
				<div class="card card_principal">
					<div class="card-header">
						<div class="card-title">
							<h1>Consultar Corretor</h1>
						</div>
					</div>
					<div class="card-body">
						<s:form action="consultarCorretor">
							<fieldset>
								<div class="form-group">
									<label for="sucursalCorretor" class="lbl_forms">Sucursal: </label>
									<s:textfield name="form.corretor.sucursalSeguradora.codigo" cssClass="input-group form-control"  placeholder="" />
								</div>
								<div class="form-group">
									<label for="sucursalCorretor" class="lbl_forms">CPD Corretor: </label>
									<s:textfield name="form.corretor.cpd" cssClass="input-group form-control" aria-describedby="emailHelp" placeholder="" />
								</div>
								<br>
								<div class="btn-home" id="btn1_home">
									<s:submit value="Consultar" cssClass="btn btn-primary" />
								</div>
								
								<c:if test="${corretor != null}">
									<h2>Resultado</h2>
									CPD: ${corretor.cpd}<br/>
									Sucursal: ${corretor.sucursalSeguradora.descricao}
								</c:if>
							</fieldset>
						</s:form>
					</div>
				</div>
			</div>

		<br>

	    	<div class="col-sm-4">
				<div class="card card_principal">
					<div class="card-header">
						<div class="card-title">
							<h1>Validar cliente em lista de restri��o</h1>
						</div>
					</div>
					<div class="card-body">
						<s:form action="validarCliente">
							<fieldset>				
								<div class="form-group">
									<label for="sucursalCorretor" class="lbl_forms">CPF cliente: </label>
									<s:textfield name="form.cpf" cssClass="input-group form-control" placeholder="" />
								</div>
								<div class="form-group">
									<label for="sucursalCorretor" class="lbl_forms">CPF/CNPJ Corretor: </label>
									<s:textfield name="form.corretor.cpfCnpj" cssClass="input-group form-control" placeholder="" />
								</div>
								<div class="form-group">
									<label for="sucursalCorretor" class="lbl_forms">Sucursal: </label>
									<s:textfield name="form.corretor.sucursalSeguradora.codigo" cssClass="input-group form-control" placeholder="" />
								</div>
								<div class="btn-home" id="btn2_home">
									<s:submit value="Consultar" cssClass="btn btn-primary" />
								</div>
								
								<c:if test="${clienteEmListaRestricao != null}">
									<h2>Resultado: ${clienteEmListaRestricao}</h2>
								</c:if>
							</fieldset>
						</s:form>
					</div>
				</div>
			</div>

		<br>
			<div class="col-sm-4">
				<div class="card card_principal">
					<div class="card-header">
						<div class="card-title">
							<h1>Listar Corretores</h1>
						</div>
					</div>
					<div class="card-body">
						<s:form action="listarCorretores">
							<fieldset>
								<div class="form-group">
									<label for="sucursalCorretor" class="lbl_forms">CPF/CNPJ: </label>
									<s:textfield name="form.corretor.cpfCnpj" cssClass="input-group form-control" placeholder="" />
								</div>
								<br>
								<div class="btn-home" id="btn3_home">
									<s:submit value="Consultar" cssClass="btn btn-primary" />
								</div>
								<c:if test="${corretores != null}">
									<h2>Resultado</h2>
									<c:forEach var="corretor" items="corretores">
										CPD: ${corretor.cpd} <br/>
										Sucursal: ${corretor.sucursalSeguradora.descricao}
										
									</c:forEach>
								</c:if>
							</fieldset>	
						</s:form>
					</div>
				</div>
			</div>
		</div>
		
		
		<br><br><br>
		<div class="row">
			<div class="col-sm-12">
				<s:form action="consultaServicos">
					<fieldset>	
						<label>Deseja acessar outros servi�os?</label>
						<s:submit value="Acessar" cssClass="btn btn-primary" />
					</fieldset>	
				</s:form>
			</div>
		</div>
	</div>
</body>
</html>