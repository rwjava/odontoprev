$(document).ready(function() {

	var relogio = setInterval(myTimer, 1000);
	
	function myTimer() {
	    var d = new Date();
	    document.getElementById("setTime").innerHTML = d.toLocaleTimeString();
	}

	$('#headingOne').click(function(){
		var myVar = setInterval(statusServicos, 3000);
		
		function statusServicos(){	
			
			var i = 0;
			
			$.ajax({
				url: "statusServicos.do", 
				success: function(result){
					console.log(++i);
					if($('#primeiroServico').hasClass('collapsed')){
						clearInterval(myVar);
					}
				}
			});
		}
	});
});