package br.com.bradseg.eedi.emissaoexpressaservicos.corretorsucursal.facade;

import java.util.SortedSet;

import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CorretorVO;

/**
 * Serviço responsável por consultar as informações do corretor/sucursal
 * 
 * @author WDEV
 */
public interface CorretorSucursalServiceFacade {

	/**
	 * Retorna todos os corretores ordenados de acordo com o CPF ou CNPJ informado.
	 * 
	 * @param cpfCnpj CPF ou CNPJ
	 * @return lista de corretores/sucursais
	 */
	public SortedSet<CorretorVO> listarPorCpfCnpjUsuario(String cpfCnpj);

	/**
	 * Retorna as informações do corretor a partir do codigo da Sucursal e o seu cpd 
	 * 
	 * @param codigoSucursal Codigo da Sucursal
	 * @param cpdCorretor Codigo do CorretorVO
	 * @return CorretorVO
	 */
	public CorretorVO consultarCorretor(Integer codigoSucursal, Integer cpdCorretor);

	/**
	 * Detalha as informações do corretor
	 * 
	 * @param cpd Cpd do Corretor
	 * @return corretor com informações detalhadas
	 */
	public CorretorVO detalharInformacoesPessoaCorretor(Integer cpd);

}
