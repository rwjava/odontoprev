package br.com.bradseg.eedi.emissaoexpressaservicos.proposta.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.bradseg.eedi.emissaoexpressaservicos.proposta.dao.CanalVendaDAO;
import br.com.bradseg.eedi.emissaoexpressaservicos.util.Constantes;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.CanalVendaVO;
import br.com.bradseg.eedi.emissaoexpressaservicos.vo.Origem;

/**
 * Servicos para obter o canal de venda
 * 
 * @author WDEV
 */
@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CanalVendaServiceFacadeImpl implements CanalVendaServiceFacade {

	@Autowired
	private CanalVendaDAO canalVendaDAO;

	public CanalVendaVO obterCanalVendaCallCenter() {
		return obterPorCodigo(Constantes.CODIGO_CANAL_VENDA_CALL_CENTER);
	}

	public CanalVendaVO obterPorCodigo(Integer codigo) {
		return canalVendaDAO.consultarPorCodigo(codigo);
	}

	public CanalVendaVO definirCanalVenda(Origem origem) {
		if (origem == null) {
			// TODO Por enquanto como o call center nao envia origem, retorna canal de venda call center 
			return obterCanalVendaCallCenter();
		}

		switch (origem) {
		case CALL_CENTER:
			return obterCanalVendaCallCenter();
		case SITE_100_CORRETOR:
			return obterPorCodigo(1);
		case FAMILIA_BRADESCO:
			return obterPorCodigo(5);
		case INTERNET_BANKING:
			return obterPorCodigo(3);
		case WORKSITE_CALL:
			return obterPorCodigo(6);
		case APP_RODOBENS:
			return obterPorCodigo(9);
		case APP_BANCO:
			return obterPorCodigo(10);
		case APP_CORRETOR:
			return obterPorCodigo(11);	
		default:
			return null;
		}
	}
	
	public List<CanalVendaVO> listarCanaisVenda(){
		return canalVendaDAO.listarCanaisVenda();
	}

}