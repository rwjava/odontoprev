package br.com.bradseg.eedi.emissaoexpressaservicos.ejb.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SaudacaoDaoImpl implements SaudacaoDao {

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private JdbcTemplate jdbcTemplate;

	private SimpleJdbcInsert saudacaoJdbcInsert;

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);

		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
				dataSource);

		this.saudacaoJdbcInsert = new SimpleJdbcInsert(dataSource)
				.withTableName("SAUDACAO").usingGeneratedKeyColumns(
						"SAUDACAO_ID");
	}

	@Override
	public String getSaudacao(String nome) {
		try {
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("nome", nome);

			String mensagemModelo = namedParameterJdbcTemplate.queryForObject(
					"SELECT MSG FROM SAUDACAO WHERE NAME = :nome", params,
					String.class);

			return mensagemModelo;
		} catch (EmptyResultDataAccessException e) {
			return "NADA";
		}
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public Long insertSaudacao(String nome, String saudacao) {
		Map<String, Object> parameters = new HashMap<String, Object>(2);
		parameters.put("NAME", nome);
		parameters.put("MSG", saudacao);

		return saudacaoJdbcInsert.executeAndReturnKey(parameters).longValue();
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public Integer excluirSaudacao(String nome) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("nome", nome);

		return namedParameterJdbcTemplate.update(
				"DELETE FROM SAUDACAO WHERE NAME = :nome", params);
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public void clearTable() {
		namedParameterJdbcTemplate.update("DELETE FROM SAUDACAO",
				new HashMap<String, String>());
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public Integer updateSaudacao(String nome, String saudacao) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("msg", saudacao);
		params.addValue("nome", nome);

		return namedParameterJdbcTemplate.update(
				"UPDATE SAUDACAO SET MSG = :msg WHERE NAME = :nome", params);
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public void createTable() {
		jdbcTemplate
				.update("CREATE TABLE SAUDACAO (SAUDACAO_ID INT NOT NULL GENERATED ALWAYS AS IDENTITY CONSTRAINT SAUDACAO_PK PRIMARY KEY, NAME VARCHAR(50), MSG VARCHAR(20))");
	}
}