package br.com.bradseg.eedi.emissaoexpressaservicos.ejb.facade;

import java.text.MessageFormat;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.bradseg.bsad.framework.core.exception.BusinessException;
import br.com.bradseg.bsad.framework.core.exception.IntegrationException;
import br.com.bradseg.bsad.framework.core.message.Message;

/**
 * Descri��o do servi�o
 * 
 * @author Bradesco Seguros
 */
@Service
public class SaudacaoServiceFacadeImpl implements SaudacaoServiceFacade {

	/**
	 * Descri��o do m�todo
	 * 
	 * @param nome
	 *            do argumento
	 * @return String
	 */
	@Override
	public String consultarSaudacao(String nome) {
		Assert.hasText(nome, "O nome � obrigat�rio"); // Garantir que o nome
														// esteja preenchido.

		// Caso o nome informado seja BSAD,
		if (nome.equalsIgnoreCase("bsad")) {
			// Lan�ar uma IntegrationException apenas para exemplifica��o.
			throw new IntegrationException(new Message(
					"msg.erro.nome.proibido", nome));
		}

		// Caso o nome informado seja BSP,
		if (nome.equalsIgnoreCase("bsp")) {
			// Lan�ar uma BusinessException apenas para exemplifica��o.
			throw new BusinessException(new Message("msg.erro.nome.proibido",
					nome));
		}

		String modeloSaudacao = "Ol� {0}";
		String saudacao = MessageFormat.format(modeloSaudacao, nome);

		return saudacao;
	}
}