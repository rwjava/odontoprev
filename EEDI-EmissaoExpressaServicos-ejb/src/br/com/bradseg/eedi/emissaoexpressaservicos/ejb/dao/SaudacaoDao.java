package br.com.bradseg.eedi.emissaoexpressaservicos.ejb.dao;

public interface SaudacaoDao {

	public Long insertSaudacao(String nome, String saudacao);

	public Integer updateSaudacao(String nome, String saudacao);

	public String getSaudacao(String nome);

	public Integer excluirSaudacao(String nome);

	public void clearTable();

	public void createTable();
}