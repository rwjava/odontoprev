package br.com.bradseg.eedi.emissaoexpressaservicos.ejb;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

import br.com.bradseg.eedi.emissaoexpressaservicos.ejb.facade.SaudacaoServiceFacade;

/**
 * Documenta��o da classe que representa um EJB da funcionalidade.
 * 
 * @author Bradesco Seguros
 */
@Stateless
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class SaudacaoEJB implements SaudacaoEJBRemote {

	@Autowired
	@Qualifier("saudacaoFacade")
	private SaudacaoServiceFacade saudacaoFacade;

	/**
	 * Descri��o do m�todo
	 * 
	 * @param nome
	 *            do argumento
	 * @return String
	 */
	@Override
	public String consultarSaudacao(String nome) {
		return saudacaoFacade.consultarSaudacao(nome);
	}
}