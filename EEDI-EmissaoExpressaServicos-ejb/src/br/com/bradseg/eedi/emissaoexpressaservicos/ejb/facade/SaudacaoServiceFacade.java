package br.com.bradseg.eedi.emissaoexpressaservicos.ejb.facade;

/**
 * Descri��o do Servi�o da Funcionalidade
 * 
 * @author Bradesco Seguros
 */
public interface SaudacaoServiceFacade {

	/**
	 * Descri��o do m�todo
	 * 
	 * @param nome
	 *            do argumento
	 * @return String
	 */
	public String consultarSaudacao(String nome);
}